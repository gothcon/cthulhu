/*
SQLyog Community v11.2 (64 bit)
MySQL - 5.1.68-community : Database - gc2012
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gc2012` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `gc2012`;

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `description` text COLLATE utf8_swedish_ci,
  `image_id` int(11) DEFAULT NULL,
  `l` int(11) DEFAULT NULL,
  `r` int(11) DEFAULT NULL,
  `p` int(11) DEFAULT NULL,
  `available_from` datetime DEFAULT NULL,
  `available_to` datetime DEFAULT NULL,
  `type` enum('category','article','composite_article','package','slot_fee','entrance_fee') COLLATE utf8_swedish_ci NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Article must reference existing image` (`image_id`),
  CONSTRAINT `Article must reference existing image` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `articles` */

insert  into `articles`(`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`name`,`description`,`image_id`,`l`,`r`,`p`,`available_from`,`available_to`,`type`,`price`) values (1,'2016-09-18 08:25:34',NULL,'2016-09-18 09:41:00',NULL,0,'/',NULL,NULL,0,11,NULL,NULL,NULL,'category',NULL),(2,'2016-09-18 09:10:56','Joakim','2016-09-18 09:11:20',NULL,0,'Muggar','',NULL,1,4,1,NULL,NULL,'category','0.00'),(3,'2016-09-18 09:11:20','Joakim','2016-09-18 09:11:20',NULL,0,'Snygg mugg','',NULL,2,3,2,NULL,NULL,'article','50.00'),(4,'2016-09-18 09:38:31','Joakim','2016-09-18 09:39:14',NULL,0,'Inträden','',NULL,5,8,1,NULL,NULL,'category','0.00'),(5,'2016-09-18 09:39:14','Joakim','2016-09-18 09:39:14','Joakim',0,'Hela konventet','',NULL,6,7,4,NULL,NULL,'entrance_fee','300.00'),(6,'2016-09-18 09:41:00','Joakim','2016-09-18 09:41:00',NULL,0,'Arrangemangsavgifter','',NULL,9,10,1,NULL,NULL,'category','0.00');

/*Table structure for table `bookings` */

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `resource_id` int(11) NOT NULL,
  `description` text COLLATE utf8_swedish_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Booking must have an resource` (`resource_id`),
  CONSTRAINT `Booking must have an resource` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `bookings` */

insert  into `bookings`(`resource_id`,`description`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (1,'Sovning,Sovning to-fr',4,'2016-09-18 09:04:23','Joakim','2016-09-18 09:04:23',NULL,0);

/*Table structure for table `changelog` */

DROP TABLE IF EXISTS `changelog`;

CREATE TABLE `changelog` (
  `entity_class_name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `entity_name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `operation` enum('create','update','delete') COLLATE utf8_swedish_ci DEFAULT NULL,
  `serialized_entity_data` text COLLATE utf8_swedish_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_user` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `operation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `changelog` */

insert  into `changelog`(`entity_class_name`,`entity_id`,`entity_name`,`operation`,`serialized_entity_data`,`id`,`current_user`,`operation_time`) values ('ResourceType',1,'Created ResourceType 1','create','N;',1,'Joakim','2015-04-06 23:13:11'),('ResourceType',2,'Created ResourceType 2','create','N;',2,'Joakim','2015-04-06 23:13:26'),('ResourceType',3,'Created ResourceType 3','create','N;',3,'Joakim','2015-04-06 23:13:38'),('User',2,'Updated User 2','update','N;',4,'','2015-04-07 11:04:26'),('Booking',1,'Created Booking 1','create','N;',5,'Joakim','2015-04-07 11:44:13'),('EventOccationBooking',1,'Created EventOccationBooking 1','create','N;',6,'Joakim','2015-04-07 11:44:13'),('Booking',2,'Created Booking 2','create','N;',7,'Joakim','2015-04-07 11:44:13'),('EventOccationBooking',2,'Created EventOccationBooking 2','create','N;',8,'Joakim','2015-04-07 11:44:13'),('Booking',3,'Created Booking 3','create','N;',9,'Joakim','2015-04-07 13:49:36'),('EventOccationBooking',3,'Created EventOccationBooking 3','create','N;',10,'Joakim','2015-04-07 13:49:36'),('Booking',4,'Created Booking 4','create','N;',11,'Joakim','2015-04-07 13:49:36'),('EventOccationBooking',4,'Created EventOccationBooking 4','create','N;',12,'Joakim','2015-04-07 13:49:36'),('Booking',5,'Created Booking 5','create','N;',13,'Joakim','2015-04-07 13:49:36'),('EventOccationBooking',5,'Created EventOccationBooking 5','create','N;',14,'Joakim','2015-04-07 13:49:36'),('EventOccationBooking',3,'Deleted EventOccationBooking 3','delete','N;',15,'Joakim','2015-04-07 13:50:03'),('EventOccationBooking',1,'Deleted EventOccationBooking 1','delete','N;',16,'Joakim','2015-04-07 13:50:05'),('EventOccationBooking',4,'Deleted EventOccationBooking 4','delete','N;',17,'Joakim','2015-04-07 13:50:06'),('EventOccationBooking',5,'Deleted EventOccationBooking 5','delete','N;',18,'Joakim','2015-04-07 13:50:07'),('EventOccationBooking',2,'Deleted EventOccationBooking 2','delete','N;',19,'Joakim','2015-04-07 13:50:09'),('Booking',6,'Created Booking 6','create','N;',20,'Joakim','2015-04-07 13:50:15'),('EventOccationBooking',6,'Created EventOccationBooking 6','create','N;',21,'Joakim','2015-04-07 13:50:15'),('Booking',7,'Created Booking 7','create','N;',22,'Joakim','2015-04-07 13:50:15'),('EventOccationBooking',7,'Created EventOccationBooking 7','create','N;',23,'Joakim','2015-04-07 13:50:15'),('Booking',8,'Created Booking 8','create','N;',24,'Joakim','2015-04-07 13:51:18'),('EventOccationBooking',8,'Created EventOccationBooking 8','create','N;',25,'Joakim','2015-04-07 13:51:18'),('Booking',9,'Created Booking 9','create','N;',26,'Joakim','2015-04-07 13:52:56'),('EventOccationBooking',9,'Created EventOccationBooking 9','create','N;',27,'Joakim','2015-04-07 13:52:56'),('Booking',10,'Created Booking 10','create','N;',28,'Joakim','2015-04-07 13:52:56'),('EventOccationBooking',10,'Created EventOccationBooking 10','create','N;',29,'Joakim','2015-04-07 13:52:56'),('Booking',11,'Created Booking 11','create','N;',30,'Joakim','2015-04-07 13:52:56'),('EventOccationBooking',11,'Created EventOccationBooking 11','create','N;',31,'Joakim','2015-04-07 13:52:56'),('Booking',12,'Created Booking 12','create','N;',32,'Joakim','2015-04-07 13:52:56'),('EventOccationBooking',12,'Created EventOccationBooking 12','create','N;',33,'Joakim','2015-04-07 13:52:57'),('Booking',13,'Created Booking 13','create','N;',34,'Joakim','2015-04-07 13:52:57'),('EventOccationBooking',13,'Created EventOccationBooking 13','create','N;',35,'Joakim','2015-04-07 13:52:57'),('Booking',14,'Created Booking 14','create','N;',36,'Joakim','2015-04-07 13:53:19'),('EventOccationBooking',14,'Created EventOccationBooking 14','create','N;',37,'Joakim','2015-04-07 13:53:19'),('EventOccationBooking',14,'Deleted EventOccationBooking 14','delete','N;',38,'Joakim','2015-04-07 13:53:27'),('Booking',15,'Created Booking 15','create','N;',39,'Joakim','2015-04-07 13:53:40'),('EventOccationBooking',15,'Created EventOccationBooking 15','create','N;',40,'Joakim','2015-04-07 13:53:40'),('User',2,'Updated User 2','update','N;',41,'','2015-04-07 20:24:42'),('Booking',16,'Created Booking 16','create','N;',42,'Joakim','2015-04-07 20:46:50'),('EventOccationBooking',16,'Created EventOccationBooking 16','create','N;',43,'Joakim','2015-04-07 20:46:50'),('Booking',17,'Created Booking 17','create','N;',44,'Joakim','2015-04-07 20:46:50'),('EventOccationBooking',17,'Created EventOccationBooking 17','create','N;',45,'Joakim','2015-04-07 20:46:50'),('Booking',18,'Created Booking 18','create','N;',46,'Joakim','2015-04-07 20:46:50'),('EventOccationBooking',18,'Created EventOccationBooking 18','create','N;',47,'Joakim','2015-04-07 20:46:50'),('User',2,'Updated User 2','update','N;',48,'','2015-04-08 19:00:27'),('User',2,'Updated User 2','update','N;',49,'','2015-04-09 09:33:10'),('User',2,'Updated User 2','update','N;',50,'','2015-04-09 09:50:11'),('EventOccationBooking',17,'Deleted EventOccationBooking 17','delete','N;',51,'Joakim','2015-04-09 11:05:10'),('EventOccationBooking',1,'Deleted EventOccationBooking 1','delete','N;',52,'Joakim','2015-04-09 14:21:03'),('User',2,'Updated User 2','update','N;',53,'','2016-09-17 19:36:09'),('User',2,'Updated User 2','update','N;',54,'','2016-09-18 08:05:29'),('Order',1,'Created Order 1','create','N;',55,'Joakim','2016-09-18 08:20:08'),('Booking',4,'Created Booking 4','create','N;',56,'Joakim','2016-09-18 09:04:23'),('EventOccationBooking',2,'Created EventOccationBooking 2','create','N;',57,'Joakim','2016-09-18 09:04:23'),('Article',2,'Created Article 2','create','N;',58,'Joakim','2016-09-18 09:10:56'),('Article',3,'Created Article 3','create','N;',59,'Joakim','2016-09-18 09:11:20'),('Order',1,'Updated Order 1','update','N;',60,'Joakim','2016-09-18 09:19:35'),('OrderRow',1,'Created OrderRow 1','create','N;',61,'Joakim','2016-09-18 09:19:35'),('Order',1,'Updated Order 1','update','N;',62,'Joakim','2016-09-18 09:19:49'),('OrderRow',1,'Updated OrderRow 1','update','N;',63,'Joakim','2016-09-18 09:19:49'),('Article',4,'Created Article 4','create','N;',64,'Joakim','2016-09-18 09:38:31'),('Article',5,'Created Article 5','create','N;',65,'Joakim','2016-09-18 09:39:14'),('Article',6,'Created Article 6','create','N;',66,'Joakim','2016-09-18 09:41:00'),('Order',2,'Created Order 2','create','N;',67,'Joakim','2016-09-18 09:43:10'),('OrderRow',2,'Created OrderRow 2','create','N;',68,'Joakim','2016-09-18 09:43:10'),('OrderRow',2,'Deleted OrderRow 2','delete','N;',69,'Joakim','2016-09-18 09:43:43'),('Order',2,'Updated Order 2','update','N;',70,'Joakim','2016-09-18 09:43:43'),('Article',5,'Updated Article 5','update','N;',71,'Joakim','2016-09-18 09:46:54'),('Order',2,'Updated Order 2','update','N;',72,'Joakim','2016-09-18 09:47:15'),('OrderRow',3,'Created OrderRow 3','create','N;',73,'Joakim','2016-09-18 09:47:15'),('OrderRow',3,'Updated OrderRow 3','update','N;',74,'Joakim','2016-09-18 09:47:37'),('Transaction',1,'Created Transaction 1','create','N;',75,'Joakim','2016-09-18 09:47:37'),('OrderPayment',1,'Created OrderPayment 1','create','N;',76,'Joakim','2016-09-18 09:47:37'),('Order',2,'Updated Order 2','update','N;',77,'Joakim','2016-09-18 09:47:37'),('OrderRow',3,'Updated OrderRow 3','update','N;',78,'Joakim','2016-09-18 09:47:37'),('Image',1,'Created Image 1','create','N;',79,'Joakim','2016-09-18 09:53:54');

/*Table structure for table `event_bookings` */

DROP TABLE IF EXISTS `event_bookings`;

CREATE TABLE `event_bookings` (
  `booking_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Event booking must have a booking` (`booking_id`),
  KEY `Event booking must have an event` (`event_id`),
  CONSTRAINT `Event booking must have a booking` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Event booking must have an event` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `event_bookings` */

/*Table structure for table `event_occation_bookings` */

DROP TABLE IF EXISTS `event_occation_bookings`;

CREATE TABLE `event_occation_bookings` (
  `booking_id` int(11) NOT NULL,
  `event_occation_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Event occation booking must have an event occation` (`event_occation_id`),
  KEY `Event occation booking must have an booking` (`booking_id`),
  CONSTRAINT `Event occation booking must have an booking` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Event occation booking must have an event occation` FOREIGN KEY (`event_occation_id`) REFERENCES `event_occations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `event_occation_bookings` */

insert  into `event_occation_bookings`(`booking_id`,`event_occation_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (4,1,2,'2016-09-18 09:04:23','Joakim','2016-09-18 09:04:23',NULL,0);

/*Table structure for table `event_occations` */

DROP TABLE IF EXISTS `event_occations`;

CREATE TABLE `event_occations` (
  `timespan_id` int(11) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `main_booking_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Event occation must belong to an existing event` (`event_id`),
  KEY `Event occation must reference existing timespan or NULL` (`timespan_id`),
  KEY `Event occation must reference existing booking` (`main_booking_id`),
  CONSTRAINT `Event occation must belong to an existing event` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Event occation must reference existing booking` FOREIGN KEY (`main_booking_id`) REFERENCES `event_occation_bookings` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `Event occation must reference existing timespan or NULL` FOREIGN KEY (`timespan_id`) REFERENCES `timespans` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `event_occations` */

insert  into `event_occations`(`timespan_id`,`event_id`,`main_booking_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`name`,`is_hidden`) values (15,4,NULL,1,'2016-09-17 19:39:01','Joakim','2016-09-17 23:10:03','Joakim',0,'',0);

/*Table structure for table `event_types` */

DROP TABLE IF EXISTS `event_types`;

CREATE TABLE `event_types` (
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `event_types` */

insert  into `event_types`(`name`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values ('Brädspel',1,'2012-09-06 08:57:54',NULL,'2012-09-06 08:57:54',NULL,0),('Övriga',2,'2012-09-08 23:44:32',NULL,'2012-09-08 23:44:32',NULL,0);

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `eventtype_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` smallint(2) NOT NULL DEFAULT '0',
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `preamble` text COLLATE utf8_swedish_ci,
  `text` text COLLATE utf8_swedish_ci,
  `info` text COLLATE utf8_swedish_ci,
  `internal_info` text COLLATE utf8_swedish_ci,
  `schedule_name` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `visible_in_schedule` tinyint(2) DEFAULT '1',
  `visible_in_public_listing` tinyint(2) DEFAULT '1',
  `organizer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eventtype_id` (`eventtype_id`),
  CONSTRAINT `Event must be of an existing event type or be NULL` FOREIGN KEY (`eventtype_id`) REFERENCES `event_types` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `events` */

insert  into `events`(`name`,`eventtype_id`,`id`,`is_deleted`,`modified_at`,`modified_by`,`created_at`,`created_by`,`preamble`,`text`,`info`,`internal_info`,`schedule_name`,`visible_in_schedule`,`visible_in_public_listing`,`organizer_id`) values ('Advanced Civilization',1,1,0,'2015-04-06 14:50:18','Joakim','2015-04-06 14:23:27','Joakim','','','',NULL,'Adv. Civ',1,1,1),('Carcassonne',1,2,0,'2015-04-06 14:47:04',NULL,'2015-04-06 14:47:04','Joakim','','','',NULL,'Carcassonne',1,1,2),('Diplomacy',1,3,0,'2015-04-06 14:50:07',NULL,'2015-04-06 14:50:07','Joakim','','','',NULL,'Joakim',1,1,3),('Sovning',2,4,0,'2016-09-17 23:12:05','Joakim','2015-04-06 14:51:08','Joakim','','','',NULL,'Sovning',1,1,4),('Goblin Games Spelbutik',2,5,0,'2015-04-06 14:57:45',NULL,'2015-04-06 14:57:45','Joakim','','','',NULL,'Goblin Games Spelbutik',0,0,5);

/*Table structure for table `group_memberships` */

DROP TABLE IF EXISTS `group_memberships`;

CREATE TABLE `group_memberships` (
  `person_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_leader` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Only one membership per group and person` (`person_id`,`group_id`),
  UNIQUE KEY `Only one leader per group` (`group_id`,`is_leader`),
  CONSTRAINT `Groupmember group must exist` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Groupmember person must exist` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `group_memberships` */

insert  into `group_memberships`(`person_id`,`group_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`is_leader`) values (15310,1,1,'2015-04-06 14:37:26','Joakim','2015-04-06 14:37:26',NULL,0,NULL),(15312,2,2,'2015-04-06 14:39:08','Joakim','2015-04-06 14:39:08',NULL,0,NULL),(15334,3,4,'2015-04-06 14:46:23','Joakim','2015-04-06 14:46:23',NULL,0,NULL);

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_swedish_ci DEFAULT NULL,
  `salt` varchar(12) COLLATE utf8_swedish_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8_swedish_ci,
  `internal_note` text COLLATE utf8_swedish_ci,
  `leader_membership_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Leader membership must exist` (`leader_membership_id`),
  CONSTRAINT `Leader membership must exist` FOREIGN KEY (`leader_membership_id`) REFERENCES `group_memberships` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `groups` */

insert  into `groups`(`name`,`email_address`,`password`,`salt`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`note`,`internal_note`,`leader_membership_id`) values ('Joakims Grupp','',NULL,NULL,1,'2015-04-06 14:37:18','Joakim','2015-04-06 14:37:33','Joakim',0,NULL,NULL,1),('Ninas grupp','',NULL,NULL,2,'2015-04-06 14:38:59','Joakim','2015-04-06 14:38:59',NULL,0,NULL,NULL,NULL),('Martins grupp','',NULL,NULL,3,'2015-04-06 14:46:14','Joakim','2015-04-06 14:46:14',NULL,0,NULL,NULL,NULL);

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `path` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `content` longblob,
  `thumbnail_small` blob,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `images` */

insert  into `images`(`path`,`name`,`content`,`thumbnail_small`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values ('/','Linus1.jpg','����\0JFIF\0\0`\0`\0\0��\0C\0		\n\r\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			\r\r2!!22222222222222222222222222222222222222222222222222��\0�\0\"\0��\0\0\0\0\0\0\0\0\0\0\0	\n��\0�\0\0\0}\0!1AQa\"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0	\n��\0�\0\0w\0!1AQaq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�FcM�i�M�H�h�i���n>�o?�S(��4��2�\0~�F��E2�4\0���I@�iw�����Ѽ�)i��Ɨy�KH4��7�J\0~�\0�(s��Tt�����o>ߕFM�>������\r\0JӃ��4���y�\0\"��>ߕD\r;�\0��?�S�����C�PyP���M7�`M2��b����\n3�>[+�b�$�DO��\r�j�q�O?���qgkv�7�β�������Əq�B�>�Ò����T��v��!s(�y�-Z���.8X�;��;֔���yuw1��D�&\r�<���5�ݾ�6��1�8�R��s�����О�Z��,4˨�n���4}�>���j�%հ���74J�Ce���\0�}6�\"�\Z�@�d�t�Ees:+G��p@�۷�Q{ɵ(�N�,A�;�Ilm�6K��0ze�Ƞ�x]���n^��i:��N�:Z�w9}6�?7v�m˻���\n��#�*n��r��\'�2��?���[���EZ����X&����i��95Bx�6,�&,�3@Q�)�\0�R��\0|Q<�,q�gn�jV��H̍o(Aԕ�:�\0�ſ��\0A5�}�R����6���7�m����T���hwJf�T�����m:L!h\\Hz.�Mi_]�$�m�n,���Lւ_�,��$齭�3���_ր0e����̒��{�t�nt���\"�`J��}�Lօ�t�<�w�M!M�������Xi��nْ2�1�v�E��\Ze�x��o_���QEiq0�P��v������P�p���r�*���sZ�\\%�v��F޸��\0S�ϼ�6}�M��N1��kK��ah\\H�tc�ҥ3��E����?&y�oL��>x�;$�6L�\0y��6�\0,�tv��J�1�>�c�Qf�d_���,�<�*�3��=y뚧q\0����#��r�=�1J:�8u�\n��M�#�1@�]Mh���e�x���^}(\0�)y��P��.)\0\n)�R�S[ʰ�-\nI�\n�㧿֙�\0���R��ъ�i�4�6#�O�@�S�F(H\'��e�&����>��fwgc�c�}�\0�)qF(�b��1L��N�\0ʚ�Q�#D���85-\0�N1��QE�ZQ֒�u�SIJz�f��KE(�\0\nP(�\0\0S��\np��.)qJ\07��6�S]�#�rO~���h�v�ϟ(��A��2���(���ZX�y(H��ǀ�S].��f��IX�������\nʑ�T��������j2#�kc��3�>���Y���Eu4_Aѭ�R�����l/��˛{kd3��p#B���[F���L��NAnp}�Mo�$�d,����~��J�[��%��}�hڧ��s��\0���r���sk ��N���+���Y�p�LH�B�2F1��\Z�MN�I;m���I�9q�]H���F�]\n����w,�^�n�+/Bˊ˼��*^�u�&68c���UH�7�km&*y �&�\"2�B1L�VA)6Իi6�<Q�~�1@�T���)i��m\03��;�\0R\"�N\"��)iqK@)@��\0\0S��\0 ���/�I��Kt?;�����r#Nr�z(��Ic���]�D0=��ֲ�S�E��!� HbU�mP0Xuj�c��a���B�O1�X�w�j�=˶��{�?��F�gJ�:�;H�t�xf���?Z����X�yf��\0=*h	h����Q�Pܟ)v��{��Z��-\"����r�����iF-!b9��S�\Ze�c�i�eE-±;O�<���\0�Uߠ�Ԏ�-�԰椚\\l8�(�se����.�;���(��m[H۱���I�E^�ler�{�\0u���>!�GcVRA�}��a�{�\"�^�˜�cV����qj�Ҕq�c�Z���H���S�·0��9F��)�i��eR��G9��mXhʱr:�\n�Q�C��V�\"�V���.�6�\",Q��eh<Q�jM���3���\0Rъp\0N�)�P\0J�Y���\'\0\nh��^�ͼk�%��3Оԥ.Uv4��h%��zrۯ���J}O��QGi<���U��3N�O\nI4�h��\\u�?��96�gtceb��3�v���j��>=$U��+�$k�֡�\"�Ϧ�R�vR����z�.e}ǡ�}*�.� �p~��$q��O[�m\nȟ61�\'$Z�UϘO\'�Mj-�#�y���N��6�� 1���;����������jܫ�4����*��}s����4U�bX������ 7I�I ~��Q�?�=Э���Sdؑ�����7��\'�|��z��/���k�����a �4$c � �5尖�G�_��=k,�m��~�\r�t ����W����}���+���Y�V6w3�I���0��ȋb���Pt��0(�@��Ȥ�\"��M��� �Gf�2)n���.�ZB�p����Z�ʶV�{��80(ۂ\0Fy��\0:����9�����FVϖp:�UN��?2[wU�qҝ�w(-܂ֵ��K�ėW�L���M����uB�O1m\\2�\n0C���N�rB����sM�4�HLl<����~�kP����\0�߶�pq��Z<E.��P�nbȠ����`�@RGZ�mF���\0��o��z~B�llP�t�\'���Y�*=��tw줐��Cc5ɉ����cws-�o ��6�T���7`X��T�*TLp���L?q�ׅ?Rs�+��Ei#&8׼���*�d����\\�,;`t���\"C�\0}+�ZEx-d�LF������a4`;��@�Ek�l��N1SƞL34�	 �HU\r��	h6�3�H�c�F�&�1!z{U��e����޺��)o���v�6������U���i��+��}2h�P�̃�[T�e�� ���0:��][��K��U��~EV��Tg�ݜ�TdE�\\�8��q6�w�.�[#4\0f��)�i����̭�q�tV�.\r̗��BnϒA1P�Ȧ�C�����iqR�]}��@��#�R6Ӧ�\"�0]��c�n�`������#=>n?J�\Z3r��.�j��s��(M�x%��3��q����x<�x%���*��&�Txd�`��g��z�M���y�&�+�\"���$�*���aQ���Q2�q�Di��E4�\0#��G\Z�s�֥k+����Hu;jƌ?�m��\0A5�}�P��k����ߒ}��Pci��X%�t�PIU_��z���$�&�ć���֍���̴Xe�ܪ܏L�佷Y��]�o��s���@WzP)إ�1H��EJ�HV��\"�2�bF.�W�Q*�ʴ�<9�f��r�\0���jN��`:	@�tb�i������Jz�o?�y��sg]h��|�H}���V�>d�\'���\Z�N���U�XhƧ�\'����4{pORI�h�F�F~nj{����ib��|�mr8@��ᶷn��n\n��b�Oӊ-��m���Ԡl��S���J�s$���&`t���E��T֣!���,@�X����;WJ�\Z G9�B�.��}1��Z�32Rm�8���&���_��h�[Y��p�e꒫���5V�/�;ZBq�\0Vu��d��%}�G�:{��A,���?�R� ��@) 1�Pά;���TϘ��ȫ���\'�[(�9�ִ�2��c/-]+��J��<�E0���Q@	�[̲�p���5�wgnY�M<�B(:1ȥ4��(��(�S��@���\n�E\0*�N����LQS��A�恝d\n�@�W�=�1Jۈ=�d�G�@G�i����8�.N��mKt��y�,�+c���\0�&�d�>��u�Ҧ��vd�\0����c��	\"�<n�*(P�q�U��U�8ִQ�IVILv��ЌV3je� ֦��yEC\r�t��z锝ø���Zf������Xc���o�9\'�k>�T��3�W\r�*d��ޥhjC���A<���+�9�����Fr\Z��B�j��\rXe��Ƕ�Xs�z\0j�0��Y���	�q\"�$LT�|��S3,���5�q�Z�$ �ˁS��~�r{�Ж��~x��#��Q�A��槸��3g\0tJY�2^Z����]G��F­*}�z���$��-F�V]*=��V)Q�V��(�Zn9;-0�E\0Q�4�)qN����QL*�\0�V�Q�F�je�k*�\0������v�5�c\'�Y{�����g�^d�i4zT���_���x�5������3Ұ�\r}�9�tv2)�$_�䏭Jܾ����T#�������+\0@O�����P\n�E*�V�X��8��_x��B�~X��Z�u8u+I`�éo�������A��;�ۥ{#NT�j	JM�#��H���#��l�X�ǹ�(�q�?\Z���#{��xoQ�H�ѣؿ��[�:*�����-R��$zM���G\'ڪj��X��\\/S�����T��ˡvl¸�s�@�qҲ_X���bY�ܜ�Py�?��|6y�[����k:�\0�\Z��ʺ�@a���@6�ۺ�{��Ҷ� �1������k���B�^T��EVӜ<x�qZ_�7����~���aQYh���7�x��*�W��|��N�n�qQ.�z\n]Q�/_B)����E�D�/Z�����=׊u�m�\\�O4�^���eVJ���0�aLEV¼U�Z���\n�_�U�SM���\0e�x�)E\n�i��Z\0�G5:��EX�f�\r�����oQS���c�Rp�ƞ�A��ks��é�wms\0 ���=�\0:�M5�ta�ԹL�Ii7z\Z��7�r�U�\0\n���m/�S)t蒁Î��կ��K\n�\0��V�kz5����U���J�o�\0�\n��#:t�Ue����9�FsTV`$أq���BU�$d��\Zf�,���8�]-L�1c��4>�y� �=�R��H�U��[��}R4��f;��PUċ[�s��U�2Rm�>Rkf��r�5JMk�KH�h�h����L&s�+��$Q{��.����\n~���*�f�hh��qYr?��_�:n*?:�9j_3����B7��j;D����Vh�l<uZH}+��2�*�$^�Y�\nl)�U�J��\0B�\rBS��j��c/�>��=iE/�$�fb�#9$ڧ6wG�I�z��~��BN?ty�EZ�akor��G2Ȅ*��$��1ɥM��\r.�H\\cږyL�P����q�M}rKZ�3p���n��~�\\΋2+I <g���x\'�Pd��<\ny}N�U���# <0#� �\0	���Z�Η$��\n��`�թ5{9���d�.��Ri5f4�wD�Φ��ȗVS�1:ݏj��6���a\"6��+;Q�bk�2�G*�v�I8P=¦�����k`\Z@�yy�~n�k�T�wGT*�+3�����n:�V՝	�d��U�L��3���z��3�g�a֕��e;��u�)9�ڋX�k�z���ű$�tn��c��U�p�����\n#�&a�pG�V���ZYv�W���TaÃ��<�F�P�*H�d�&�A�\'$V��Գ�l��:��*�\'B��*l����TX������R%��Ed�m���5�kvb�y�Jd����b�&��0�Ybà+��7�(�95�_���=�� \Z�uk��`/,~^�����)Yjy�zm�a��v��>��v�%��0����E�xZ��ח	&�?v<WN���;����y5�N������F����c�U�����ǜp+~Hɺ}�y���銣v��I��4�Ǩ�jfd��Ŷ�-�g�S��Q Cnn�k]����h~\\��$;B��p6��OA�@`��GFuBUz�ª�^շ2y��n�`�#0��T.\"`oVb9���3�*\"�7�V\Z�c�~4���Fi���Q�hf��[gf���.�q�)c�I֭D���-]�sQD�~�h=�G��[(g\0I\Z��TV�+N$P8��K3E���g��>���\'[�s��0��]����^F~��|�x0���+T��ݓ�7��p9c��ED�ШJ���YVD9U���#��gF��\0���~�}k���( ���-mQ���}k���.�4�zi��IU�<W\\��G\'4-\n�8_�{{;[�FY!e\\ �H�����ze���k��J�c��X��݅j�cou��k&�ð�ى�N���W#� �/\r��+��V\"�\r��w�ޱo5)-&�%��đ�8Oo�[����a�)�Ϩ����,�}i)���M]����Ŭ�G�r�k�����;�I��c�6�Z�6��:�&�H-�P��wk�f�N\"M�r��\n~���[��4��	��Y���Ʈ���;G_���TP�\0P0\0�[R�S����(��P�փ���թ�gȵU�<���ȸ�\n������p?��54�VN)�Vld���r\Z�)��H\n�U��Z��	���@�lҨ��Q@�Ƽ�؅CU�Z\0���\n�]���+N՛\0V�-�hpΖ�<�\")f8��n<[���y����=�Q��vP��0,��t(9���-d�w1�\"�f�\n�U:	��Z$f��r��t-N\rV��3bB�lb�c�̜�u��V&��&�i�C�^O��k~����I�&���Թ��`��H�Y�fޝQW-�.A�4Ls\'�Vu`}*�\0Ui���Ks�ʹ%�ֽ�VEܪ�d����Y��!��2�I�PujG��}����msM��g��2�Zi���W��D�n�*Y��݅3)`�@�������?&�3RĖ(6��|я�+����t�	H�e]NA�:r��8T����^D�\'Q��(��\"�)��%�I@ҭS�V��JQ@�NAW�NJ\0����a���5>a��x�5,iHG�R��\0L�azUpi�f����*�7h����u3��^��>y�b�+�ߐ��qP����]��y�	W�o���E�(	�\'?ʳ��{˰U���\0t|�����Q�W]<-8l��ROqň���x�[�\n�>�w>�땏�Q�{��\\��j�f�/���4x�a�����*��P]N�$/\'#N�G�J�-Z�v\\��T6k���U����b:�W�z�!�5��6R��sRx���^=G��w8v(v��l��R��fa�^������q�f��A��\\�\0����L���}��3-�,I�ex��;{��#�}�u-�YA�Bq^m��{|̇+�dw��iR�dc^�,�W�޼�W��j\Z�E\'�lN^�~����NJ�k�t�����;���x�O׭����0�&����jE�����U�	^9�2�^�����k��\0�C�ןW	(�Q�j��:ɐ�H��vJ�D`��!��5Ru��3c\"Q֨�Z�	{�9j��WT��Aҡ)��^aP���ր9s֤N��T�(�犈�<Ӥm��z\n��k��RJ���g%Y^V��H��Z�3�ֻQ�/T�z��e� f������[@�(�����:��$��zW࡟I��b�&O�{2m׷c��/݌��F>�,�)A⟰�#��:�W;@�5��ڤQ��qHEqQҪ\\��+E�Re�ڐ�d��jA\n�Z�����L�7ABAc��k�$�/��~5�6�U\'�:�?��-���NK��z�^r#ڽ�9Q����F�uO��\\~T��N=sJ8 �ףc�p4�æj#�);�hGI�x��Eq&kB~h���z\Z�MF�V�[�I7!�;��5�d�v�=Y�ѯVx�h�!<0��,FKގ�Ш֌�)��k6hȭ(.࿴����F�#����i�5��t�2)�J���Бj��4���Qm��֭����>�̶�s��$,��\"�����Q������^v[[5���&��\n�������[��\"�̆�=�Ǆ`��x��ZR�<�I����qiuA���$��I�Ԋ\n@�HpkF3��%���9Q��E}ۍg�K�;UW����\0�{��&��nN�y889���*�>[/��t�:��k[��~�W��9�j�\\FRvgV-91f�����H�f���H�J:U��v��i㸅�s�� G����Uʫ�\0�p�4�·��*)���zb�W�xJu�����ԧ�^��\"�{0_�g���\Z\0�R�5:Ԙ�N�ʬNg�⮣,�\\T�����z�����sE��@�Al���P��ݧ>��S?)��/cOwR��Yxl�d3%�t��y4����늶̶�Hd�6G\0���K���h�$;�c�<ⵥFU%�\'QA]�w���5$y����v��h�vS�0V��xu/�5�&�\0�$��J�u���c`|�P�<f���9 ��*���v4X]d�<�r*6��Y��mʻ�q�z��7�,��ݵ[+���­�:4ZE��I���V��;_e�C��a�#Ң�s�ֵ���sG,2ǰ ]��J`r0j��+IU�ԯO��$sҚ����A8<~\"�H#�LgM�t�w^D���)���O��ƻ��|���Gz�ؔ���WZ3F4��YG���zW����߉�)��t-H�POaP�k0R�\'\n;�,����\0�jL�W2M%��ws��y�A�%���/���mo\"��\'�9�[�G��X���=*U�-�i����h�U9�*�����a\Z3z-dLk��˱�w�B\\簥N@�����,G����?A��*�2�T1#G+��;ӗ�P���&p3KHGP\"����6�\'���^�ʊ��:_\"�&�|~U��ֱH�Pk�s�gs��5��mzԢ���t!�̇*pH�ѻ=(8�cI���\\���M��8Ag`\0�?�?��e{=9�e�=�����Нir�ΥH�]�x��f)^�Ɂe��u���:���Vwb˜�O,i�����)�Ҿ��\Zc��s˩VU؇��b���J�2�Mo0�yh�M�Tn=�3҅t����ڎ�i��<P]3ӭV���j�qP�3ۥg4R��Bs߁W,.\Z�����ⳣ�ԞV1���*r3I�8ن���Xn�$w0�7)ǭB�Y�=Iɮ{���������&};��n��ԃ��Y�uq�y���i�Z��u�aB��k2C�U����=\0A����gZ)�9*��t�.i#��&�\0Z�ޕGط3�J:R�B�)GZ�G���/zN�� �)�����\r���NXX��2>�厣<�<\Z�����3���[r=EyY�+�Mt;p�ה�Ej~x�X^�yn�!�*�<׆zx\"��`�@?1����k�\Z����.���2����uQ���WPn���oK�j��\\8�����xr:���a{v��Y�����Q�i�F\'�99J�w�v=i� N3JO�i)ݨJ>����P \'\0�j�\0:k���\"�L`FA�?��R�5ZRH�����d΍S�ӑ���@��*�9Ǹ��2֝q�MB�·?N�ޖ�r לg�i�&}6\'$.��W�����:(>��hS�DZ�O�>�盜E�e��橯3/֬\\��G��澊傉���œ�֕\'�Cr��3���v朥ʛ�����$D|���z՘m�2���zS�P�G�����b?�n;Vn1���;��a���$�xi������)�\"ƥ��Q��+���n����6�̾t�����k$�~��ʷ�X7 4l��u��S�1�E�ӆ�UK�:@�,[-�*�v1�\Z��P��i�$v:�,���1���i�i,a��\Z�IP?t�qڛڄ-�&>�^%L\rX��\\�!��Wl��u$��V5�x�W����a|��P;�\Z�[Z�H�,�c��ڳ����5Յ��2S��e[��D88�ң�x�]#��擿�Ո^�v���@�4�8�=y�MrPk��֥jdT�C\Z�-�ӓQ�CN^Alry��82�hjÊ�~�5\nq�XP�C�[���԰����Wt�<�Ag��.2�56]\'ihoƞ��Z���7�s޼C��.:�2���S�-U��m>޻q_E\'es�+�����w~8⤒?1H\r�I�PZ�y�8���U���<\nΓ����*I\'�7f%��d8z\Z�j��fuf� t4뉌�\0���Z���6��?;u�g)T��u좼��%��(8�ڮ�3Xˬj>A��U������S��{��$)���\0֧M�i=\\���*���8��=OsO�9��7[8�	X�7ִ���&p$a���V.��J/qǖ����6U�i��bH��ZD�Aɧ��V�ޔt��a`�������0�8�ɩ���l;ird�7%U�#�%U���Y��\'�B�\'7�Y�I_@w��V��棛����⢘t���C�p��C��u<T�|�������.�4�Zc��;T�ɴ`u�Uy�je��C�6i���u\\v`���r,�yS������S(�F��W�\\ta�S���ֲ��7م\'�8�;V�}G־rq哏c�;��a��q�G\"�:��7�X�9�Z\\9Q������8�п���-�p��\n3�Ҡ�K�U�0u��0+{A8����?�� akot��̲!\n���?Nԧ�`�h���{o/�W��,�j�{o;��Mܨ^El�W_5��~!���ջ����;�I$�vJ���{TԋT\\b�.�9v���Ӝ����zկ�fƆC���=}*��R�L�����c0�#G݃����Gj.,��xʪ� �H$H�jjv�᭑r��f<z-�L�M���RG#�Q[��^_�&h#�8��\0�[�v�ex�s�I,1��9�+P�Y��h��<�@~�[�kI{�Z[���d��|�L�C!Uo�J��4E+�\'R��V ��Ro:�O��w�p\r��­I0GS&峑	$�>��Y���/�N	�Wky��g1�t\'�j���I��3�5�x�-�S��1���1������eyD���=�U�Z۫���#�Ed�����(�U\'su\"����d�C��q4ϴy��Gr���ڲ�f�����g�hZ��������N���~7�Tz\n�nEwҷ*���8�W	\Zc�O{+��y�@���ҭh��Ԅ��\0���y���{C�|��~���)4�#0�r}�	�\r)�T/�ǽS��e�\"18���ּ�L�Xe��+r=3V��n�Z3̻;�����Km!��d=��1�U��Je�ֶ�r��Ñ�����Z��Β��9Q�������$н���d\"��e�c��ަR}$e��x��y�zSR��T�p�����)}H^�B!ny���GP����!�W���pH�����ʱU,/7l�;�����kK��ah�H�uq֧�H�V3�8����f�XM\Z���*��>w7��iv�:�o<�4�N��TC��|]�۬R�K��g�z��5F��B�<�v#\'a�٢�cCG��g���WAQ��J�O*�&=�f���=k��F�n��T]�c��Yw9j�l�e`���ܝW{|��S|�W�(mv`��̶�d���]��<\Z��g�2�7�]Ǡ��j(O�n����g�x����7�&�s�v܂7c�+V�$B�Ǥ�V�����G�,�1�����	랕�\0.��_�>���+�1��0�ޣ$~�M�m#O���]h_[ݩ[{]��m����\n��4��#���,\ZA%�µ̉�a����+ͧ9ǭ��}��&x���j\ZE��m�����pq�EF��q$L����\Zޯ�Y�v�qe��&�0w+�X��(+��E�m5$��q_#�1�h�3�x\"��E{\'��SN��?7��2����τ�9��s���;�[�+����H&^�\"�\"��[$�C7f���Bn7���W�R���&	 ����g/l��p�����K=в��\'��o:N�1\r�3\\j�\'h�g�չ-lP��q8ބ1.;U����r�t�\0�Ҷ��{K��5氄���Y���W@<=�c{�_`�so�o��N\n\r�\'+�6�k#���UK�8U<�+{H�{��\0xk����il��ڌ�:yq�?-�*�\n��kV��\0\\��\nܱ۹|��9���;�{S��@��]|��h\n�>+���0��9Q���Η|��|_��}��]����:�T��19�{�.e�	W�^���fb�����E��fvo��zl̹�;�8=���?�-G�����;��犸����K��3����u�i��\r�O����\0��A���{We:Ъ��e(8�8�*��9�R�H��pH����E6.��$��:Q�r�g֝��C\Z�����(\\\nђqϷj4��7�HS�g�\ru�2�����?Z�ۆ�:\Z������s�\'r�\0Z�qмS�oE�����U۞��,�!!x��s&eby櫰�+�Bз�D�Nj�Q����;�1Z8#��Z��I��]���/W��b��;�{ 2�8�x����#�(S���N3V�������x���rI,�!:���Kq�r������<w-�.-l��l�ݱ��J�Q�X�`r�+����OF���N�o&�X�\0t���:�G����j-��|EԒ�L:n�o\r���2Z\\F?զ���9�@ �לE�jZ\\ڍ����B�$t�q�G\\{�.�&ԯw|�;��2Ą�Q�9�q������V1,��p{6�۷�U)}V�KVė��ϟ�4,�,:)v���w��~<E�ZmEme����6g\'��9��kz���6�}ٗJ�B=k�5i�N+tC����j\ZE���4F�Iđ\\c�̽;��ox	$��l�d�ֱ���X!3M��G�JY� \'\0�A�}���^ڋ�]2�x	 IE��JƥM_��Шͮ���Gø���T�=$��\0�S\'�>�17��Nz�0���ϥ��7�qc2K��$���\\c�U{���8�w�����Tv\"��=���j�{�X�����?����l�`В2�p�c�������}Fjҥ-é�xJ��j�Y�v�\nc�4��HǦi�\0�X���W��l5�\00�]����O�:lϨ5f�[6`��;�3/���n����z����T���z�\ZN1�\0渝c^�յ������H18�����}kN���/�#f�;�����Չ�j�ڝɺ�.滻+�I!���@�\nڎ�$���2�d�gM~�1Do�y�6�\0��4m�Z6$>�]4�JR���RZ���D\\\nd�ƥ�q������\'�&�v��j����w�z�J���M^\"��k}\rUa�P��⹫G�r������','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAABACAYAAABsrOVnAAAgAElEQVRoBQFAKr/VASUmIf/+/v4A////AAAAAAAAAAAA////AAAAAAAAAAAAAAAAAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAD/AQAAAAEAAAAAAAEC/wABAQEAAAAAAAAAAAABAQEAAAAAAAAA/wACAgIAAAABAAAAAAAAAAAA////AP///wACAgIAAgIBAAICAgAAAAAAAgICAAAAAAABAQEAAgICAAT+/v4A/v7+AP///wD///8AAAAAAP///wAAAAAAAAAAAAABAAABAAEAAAAAAAAAAAABAQEAAAAAAP///wAAAAAAAAAAAAEBAQAA/wEAAAABAAEBAAABAAAAAQEBAAAAAAABAQEA/v7+AAAA/wAAAAAA/v7/AAAAAAAAAAAAAQEBAP///wAAAAAAAAD/AP//AQD9/f0AAgICAP39/QABAQEAAAAAAAEBAQAE/v7+AP7+/gAAAAAAAAAAAP///wAAAAAA////AAEBAQABAAEAAAAAAAAAAAAAAAAA////AAAAAAAAAAAAAAAAAAAAAAD///8AAQAAAAAAAAD//wAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///gAAAAAAAAAAAP//AQAAAAAAAAAAAP7+AAACAgAAAAABAP7+/wACAgEA/v7/AAEBAAAAAAAABP7+/gD///8AAAAAAAcHBwAEBAQACQkJAPf39wD8/PwABQUFAAwMDADz8/MAAAAAAAEBAQABAQEAAQEBAPn5+QADAwMA9/f3AAABAAAAAP8AAAAAAAAAAQD//v8AAAEAAAYGBgAHBwcABwcHAPn5+QD5+fkACAgHAAkJCQDy8vMABQUFAPv7+wAEBAQAAgICAPj4+AAGBgYA9/f3AAEBAQAAAAEA////AAT///8A/v7+AAAAAAAaGhoAAAAAAAUFBQD4+PgACAgIAAAAAAD///8ADg4OAAEBAQDv7+8AAQEBAPv7+wATExMA7u7uAP39/QD7+/sAAAAAAAICAgAAAAAAAAEAAAAAAAAUFBQABgYGAA8PDwDr6+sAHx8fAPPy9AAJCgoADg4QAP7+/QD29vYA/Pz8APb29gAaGhoA9vb2AP39/QD5+fkAAQEBAAEBAQAEAAAAAAAAAAAAAAAA8vLyAPX19QD7+/sAAAAAAAMDAwADAwIA////AAAAAQACAgEA+/v7AAAAAQAAAP8A/v79AP7+/QD///8AAQEAAAAAAAAAAP8AAAABAAEBAAABAQEA+fn4APX19QDn5+YA/v7+AO3t7AAEBAYA++f6AAECAQADAwMA+vr6AAAAAAD///8A7+/vAPv7+gD///4AAQEBAAEBAQAAAAAAARwdGP8AAAAA////AAEBAQAAAAAAAQEAAAAAAQAAAAAAAgIBAAAAAAABAQEAAQEBAAAAAAAAAAAAAAAAAAICAgAAAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAEBAQAAAAAA////AAAAAAD//wAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8AAQEBAAEBAAAAAAAAAQEBAAAAAQABAQEAAAAAAAQBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAQABAQEAAAAAAAEBAQABAQEAAAAAAAICAgABAQEAAAAAAP///wABAQEAAQEBAAEBAQAAAAAAAQEBAAICAgD9/f0AAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAQEAAAAAAQAAAAAAAAAAAAAAAAAAAAAA////AAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAAAAEAQEAAP7//wAAAP8AAAAAAAIBAgAAAQAAAQEAAAEBAQABAQEAAgICAAEBAQAAAAAAAQEBAAAAAQACAQAAAQEBAAQDAAAeHBQAIR8dAAwIAwAfGxIA6e71ANDV3gDd4egA/v4BAAEA/wD/AAIAAAAAAP///wAAAAEA////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAABAQEAAR4fGf////8AAAAAAAAAAAABAQEAAQEBAAEBAQABAQEAAgICAAEBAQACAgIAAAAAAAEBAQAC//8AAgP/AB0bFQAjFg0AFAoDAPXy8gAECQ8AAwcNAA4MCQD/BQkA6e7yAN/k6QDj5+oA+fv8AP8AAAAAAAAAAAABAP///wAAAAAA////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEdHhj/////AAEBAQABAQEAAQEBAAEBAQABAQEAAQEBAAICAgABAQEAAgECAAAB/wABAgMAEwsHACojFAANBvwAAPn5APr3+AD4+v0A+v4DAAEAAgD+//4ABgkIAAEFBAAMCwcABAcMAODo8wDc4u0A/wH+AAAAAQD///8AAP8AAP///wD///8A/wAAAAAAAAABAQAAAAAAAAAAAQAAAP8AAAAAAAAAAAAEAQEBAAAAAAABAQEAAQEBAAEBAAABAQEAAQEBAAEBAQABAQEAAQEBAAABAAADAAUAGxcIACodEwAA/fMAAf4EAPr7+wD7+/cAAP4CAPPy8wALCAYA9vj5AAH7BQDx9/wA9AoDAAwJCQAUAwkAByQOANri9AD///8A////AAD/AQD//wAA/v7+AP4A/wAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAABAEBAQAAAAAAAQEBAAEBAQACAgIAAAAAAAEBAQACAgIAAQEBAAICAgACAgMAFQ4FAC0eDgD69u8ABQL/APLx8gAJCAUABgMAAOvw8wAQCQcA9vn5AAgFAwAB/vwAAv/+AAH9AgD/+/wA/f30ABII+QAhFw0A4+36AP///wD//gAA//8AAP7+/gD+//8AAAEAAP///wAAAAAAAAAAAAAAAAAAAAAA////AAQCAgIA////AAEBAQABAQEAAAAAAAEBAQABAQEAAQECAAICAwACAgEACQcHADElDgD78fIABgQAAPbx9QD/+wAABAgAAPv9/QAHBggABgYBAPz9AQAHBAEA/wABAAcDAgAC/wAA/Pv7AAgKBgD89f0AFgj4APcMCADt9gAAAQH5AP7/AwACAgIA/wD/AAD/AAAAAAAAAAAAAP7//gAAAAAAAAD/AP///wAEAAAAAAEBAQAAAAAAAAAAAAEBAQABAQEAAgICAAICAQADA/4AAwIFADAnFgAE/OcABQUJAAD/9wAEAwAABQMDAAP//gD6/PwACwcHAAQF/wD++wAADAgGAPj8+gD6A/4AAwIBAP///QAB/v4A/f/8AAD4/QAeFQwA9ub9APv8CAAB//4A////AP7+/wABAgAA////AP///wADAQMAAAD+AP8AAAD//wAABAAAAAAAAAAAAQEBAAEBAQACAgIAAQEBAAICAgADAgIAAgEFAAsLAQAhFAgA9OwDAAYHBAD+/QEA//wDAAQEBQAB/fkADwsMAAAEAwADAfwACAECAAD/AAAABQIA9wD8AAcFBAD+/v4AAwEBAAH+AAD9+fsAAvbxABwXEwDj6/4A/v39AP7+/gABAf4AAAAAAAAAAAD///8AAAAAAP8AAQD+/v0AAQEBAAQDAwQABwcHAPn5+QABAQEAAgICAAICAgACAgIAAwICAAMCAQAuJx4AAPzsAAkHBgAGBAAA/PwIAAD9AwAKBf8A/vwEAAIFCQANCgsA/P74APgH+QD//QMA/gIAAPv68QAGCgUA/PwCAAMBAgD7/f4A+vr9AAT5/AAKCfYAAwQMAAEAAAAAAAAAAQEBAP7+/gD///8AAAAAAAAAAAAIBwkA+fn5AP7+/gAEJSUlAAEBAQD7+/sAAwMDAPr6+gAFBQUA/v7+AAYGBgD7+fQAJBoQAADy9AD7BQEAAwICAAMB/gAHCwkAAf75ABUaGwAFCQwADAwQAAEE/wDx8fIABAIFAPj8+QAHDvUACwkGAPz8/QD7/PsA8/T0APj4+gAAAgMA8/37AAkGAAD3/gIA/v7+AP///wABAQEA////AA0NDQAODg4ACgoKAPX19QAFBQUABPT09AD9/f0AAgIBAAEBAQD39/gAAgICAAMCAgAKCwwAGBQNAA0E/QD4BAUA/fn7AAD//wAPDAsA+vf8AAUEBAANEA8A/wEEAAH+/AAFAQAACQQBAAkKBAD46+4ACgX+AAYF/QAEA/8A/fr2AP758gAIBAUA+f4BAAkDBAAMCAMA+/wCAAAAAAD8/f0A/v7+AAEAAAAEBQQAAAAAAO3u7gD08/MADAwMAATp6ekAAQEBAAICAQABAQEAAwMDAAQDBAD/AwMA+vn4AAkGAwD38/AA///9AAL+/wD///0ADAsTAPgC/AAICQMAAQH8AAD//gAAAAMABQMEAAYFAwACBAEABgMBAPn9/AAFAwYAAQICAPwFAwALCQcA/Pz/APT39AD7AQUACAgPAAMFAwD+AP8AAgD/AAABAAAAAQAA8vPzAPDx8QD//v8AAQAAAO/v7wAEAwMDAAEBAQAAAAAAAwMCAAMDAwADAwIABAQEAAD//gAUDggA/Pv1AAcHBgD5/fQAAAABAAUICwD9APwACw0HAAYFAAD+/gIABgMJAAD//gAIBwsAAwkGAA0REgD///kAChAKAO/u7wAFBAUAERERAPPz9QAI/vwAAf4DAAMDAQAGBgYAAAAAAAEBAQD8/PwAAAAAAP7+/gADAwMA/gD+AP/+/gD///8ABAICAgABAQAAAQEBAAQEBAADAwMAAwICAAMDAwAGBgUABQEBAP79/wADAQMAAwEAAPn4+QACBQMABwEHAAMGBAD/AfsA9vHyAPsBAgACBPsA/v79AAgEDwADAggACAgEAP38/QD18fIA///+AA4RDQAF/P8ADgQDAPH2+QAB//4ABQMFAAAF/gAAAP8AAAAAAAAAAAABAQEA/v7+AAEBAAD/AQAAAAABAAQDAwIAAAAAAAICAgACAgIAAgICAAMDBAADAwMACAgGAAoD/gAEAAEA+Pf3AAICAQAEBAQA/PwBAAAAAQAA/wYA+/XzAO3m5gD+++gABQYDAAMDAAAC8e8ACAkSAAgLDgDs7eYA+/n5APj4+gD9/v0ABgkPAAgIBAD3/AEAAPj9AAMEAQD+/gIABAQEAAAAAQABAQEA/v7+APz8/AABAQEAAP39AP///wAEAgICAAEBAQACAgIAAgICAAICAgADAgIAAwMDAAD+/gAE+vcA//77APb3+gAF//sACwsNAAD/BwALCwkABwUPAPDz9gD8/wUA9vn0ABEJFgADBwMA+vXxAAgHEgD8/AAA9PT0APb39gAD/wUA6OToABQXBQD6Af4A9/j5AP4AAAAGBgIAAgEIAP/6+gD//wAAAgICAP39/QACAgIA/Pz8AAEBAQD+/v4ABAIAAQABAAEAAgABAAQCAwAEAwQABAQEAAMDAwABAQEA+/r5AAT8/gAE/vwA//wAAAsJEQAHAgMADA0VAO3m7QDa3esAzNv1AAcLBwA7FA4AHiQwAAP/8QAGCwsA3uHjAPX8CQD6AQEAEBITAP/u8QD3+PEA8/v8APb5/QD6BgQACAoIAAEC/gD/AQAAAgICAPz8/AABAQEA/f39AAEBAQABAQEA/Pz8AAQCAgIAAQEBAAMDAwACAgIAAQEBAAICAgACAgIA/wABAOzx+QAYCwYAAf8DAAIDAQAICAgADhIYAAcGCQAgHx8AGSAgACUq7wDu7PEAExEKAAUPDgALAQ0A+PfzAO/s8QDo8P8AytroABkW+wAU+QMACvwFAPz7/gD6+/gAAwkCAAQEBAD+/f0ABQUGAAAAAQAAAAAAAAAAAAEBAQD8/PwAAwMDAAEBAQAEAgICAAEBAQABAQEAAAAAAAICAgAAAAAAAQEBAAIDBQDv+AEACv/zAA8JCgD49/YA+/v7AAMEBwAKCQsAAvf4APvz7wAVCv8AEyEVAP0CAwAB+PAA+wD9APfw9AAG/wEAJRwWADMtLQAAAAAAAfnwAPL48wDo1dMAAAECAAIEBgACAwMAAf7+AAICAgADAgMA/wP/AAEAAAD9/f0AAAAAAAAAAAABAQEABAEBAQACAgIAAgICAAICAgAAAAAAAgICAAICAgACAgIA8gMRAAr76wAE//YABgMDAPLz9QD9/PwAAP3+AAEHBgAF+gYA/ff4AAQHAQADCQcA/vv9APv2+AAEAv0ABAP8APz68wAHAhEADQwMAP8AEgAA8P8A9fj1AAIGBgASERwAAgYIAP//AQD8APwAAP7/AP/+/gAA/wMAAQABAAICAwADAwMA+/v7AAQDAwMAAQEBAAEBAQABAQEAAgICAAEBAQABAQEABAMDAAYJDQAEChIAGgz3AO7r5gATFRgAAAAAAAAAAAAIBwgAAvsMAP79AQACBwkA/Pj2APf7+QAQDhAABQgKAAfq7AD79/UAAvz0AAkCBwD3/P0A9/f6APbz+AAJFQwAEhUdAP3+/gAFBQUA+fn5AAICAgABAQEA+/v7APz8/AAAAP8AAgMCAAEBAQAEAAAAAP///wAAAQEA/wAAAP7//wAAAgEAAgMCAAQFBAAHBgMA9fr6ABkiJwAA/vwA/AICAP//AQD9+/wA/QT9APn5+QDz9vcADQkKAP79/wAB/QAA8vb7APn9/QAAAAQAAv4BAA4NEAAJCQwA/fXyAPb19QD/AQEADhgeAAgLDQD7+/kAAgMDAAQFBQACAwIAAQIBAAICAgADAwMA/f79AP7+/gAA/wAABAAAAAAAAAAAAQEBABAREQAJCQkADw8OAP7+/gAGBQQABgYGAAEFBwDO6vwA9vUEAB4KAAAFCgsA/AMEAPD3+AD19vgABwsPAP7/BwAEBQYABwb+APP28QDs6usA/Pj9ABMSGwARFB0ADREOAP7r5gD+AAIAFiMlAC0zPwAEChIA//77APb29gD9/fwAC/oLAPf29gD9/P0A8PDwAPv7+wD8/PwAAP8AAAQBAQEAAQEBAAAAAAAMDAwAAwMDAPz8/AABAQEACgsMAAkJCAAEAwEA9Pf0AOz5/wAG7/UAKv0AAP0A/gD/AAEABQ4QAPz4/gD8+v0ABwQDAPr6+wAB/AEAEg8UAAH7AAAGCQoA7/PzAAMEBwDr8PQA+AkLACo4SAAXGyAA+/36APz7BgD9/f0A+voAAPr6+gAODg4A9vX2AAwMDADx8fEAAAAAAP///wAEAAIBAP8AAAAAAP8A7e3sAPv7+wAKCAkACgoKAPz6+wANDQ0AEBAQAAUCCQAgIisA0cG0ADQqKAAVDA8AAwYDAP38+QDj3uMA8ezxABUXFgAJDQsA+fXzAO/39wDx7fIAAwz0ABcZ+QAIBwIA6OjsACErOgAIDg4A+/z7APr6+wD5+fkA/v79AP///wACAgMA+vv8APX39gD4+vkA/wD/AP7+/gD///8AAv7+/gD+/v4A////APj4+AADAwMABQUFAAUFBQAICAgACwsLAAoKCgAQEBEA3uLfAOTv6QDe5eIA+P78AP4EAwAJDA4AKy8rAC8wKAAB9/IA+eHiAP7m6AD219wA6tnYAAwGAgAgICAA9/T3AAcYHgASGhoABwcHAAUFBQAFBQUAAwMDAAICAgD//wAA/v7+APr6+QD///8A/v7+AP//AAABAQEAAQEBAAT///8A////AAICAgABAQEABQUFAAUFBQAHBwcABgYGAAQEBAAEBQUACg0GAC4vMwANCgsA+f8BAPD38wAUDAsABwQFAP/++wAHEQ8AITMwAPv4+QD28u0AABkRAAr69wAHDQsA7/HzANTh4wAtRE8ACAYJAPsBAQAAAAAA////APz8/AD7+/sAAQEAAAEBAAD///8A////AP///wAAAP8AAAAAAAABAQAEAAAAAAAAAAACAgIAAwMDAAMDAwACAgIAAgICAAEBAQAAAgEA/wD/AAD+BwABAgAACBAeAO3z6QD/BQMA8vLyAAMA/wAIBAYAAwYBAP37/AD9AP0A/gUHAAELCgD//fwA9Pf7AN7T1wD+DRAAERERAP0A/wAAAAUA////AP39/QAFBQUABQUFAAQEBAACAgIAAgICAAICAgACAgIAAAEBAP8BAAACAwIAAgAAAAADAwMABAQEAAYGBgADAwMAAgICAAAAAAABAQEAAAAAAAAAAAAAAP0A2Oj2AKqs2AD+9/EAAf0CAAUHBgDw9PMA+Pz6AAMEAwAGBwYABgUGAAQEBAAEAwQA/P7/AO/t6ADt7egAydTsAPD2/gAJBgQACQkJAAcHBwAEBAQAAgICAAICAgADAwMAAwMDAAICAgACAgIAAgICAAICAgABAQEAAAAAAAIDAwMAAgICAAUEBAAEBQUAAgUEAAEBAQD///sA/wAAAAD//gDj6/wAucXrAMHN8gAoHf8AMSgXAAgHBQACBAMABAUGAOvw7gD09vUA/fz9APv9/AD5/PsA7vPyAPL17wAIAwAAGBgSANzf+ACAjKsA2ODwAAEBAgABAAEAAAMAAP///wACAgIAAQEBAAEBAQACAgIAAgICAAICAgACAgIAAgICAAICAgAEAwMDAAICAgADBQgAAv8BAAT++wD8AQQA6fUIAPLuCgDz9gQA+fr+AAD+BgD+APUAOTMSAAUGCAAICwoA/v7/AAwNDQD8/foA6vHvAAQEBQD6+fkA+PT6AAAE/wAEBgIADgwLANfbAQD7AAgA5+8DAIWVuwBQRSkAQjskAAcDCQD7BvsA////APj4+AABAQEAAAAAAAICAgAFBQUA/v7+AAAAAAD7+/sABAMDAwABAQEAAQL8AP8A/ADy/BoA6vQbAPj7DQD8/v4AAfv8AP8CAwABAwAACvnzAC0kFAABAQMABAQEAPr6+wAHBgYACwsMAAcHCAAGBwcA+/r6APUL9AAB7/sAAQMVAP3/AgDV4vYA+wgFAAkABgD7+AcArbnhAMjS6gBgUS4AFxIJAP7+/gACAgIAAAAAAAEBAQABAQEAAgICAAMDAwAFBQUAAQEBAAIAAAAAAQEBAAD9AADz/RMA5/UjAPb5DAD6+f4A//wDAP3+AAACBAcAAQMJADgmDwAGBQAACwgKAAcDBgAGAgQAAwIBAAwKCwANDAsACggGAAIB/wD//PkADQ0OABYXFgC70P8A4/YIAAQBBQAAAQQAAQMBAP8ABADn6vsAj53NAMbN4wACAgUABQYEAAQEBgACAQUAAQEBAAAAAAABAQEAAQEBAAMDAwAEAAAAAAAAAAD/A/0A9vsfAPj7BAD+/fwA/f0AAAUFBAD+//8AAQABAAIBAgAfHwsADQoHAAUFCgD+/v4ABwcHAAEBAQAICAgAAf//AP39/AAHAwUACgYLAAAAAwD7+PUAGRMMAAbi/AD+AAEAAAAEAAAABAD+/f8A/f39APr8AACoteEA5ervAEM4IAAKCvwA/Pz6AP3+BgD///8AAQEBAAEBAQD8/PwABAAAAAABAwEA/gAMAPT8CAD9AQIAAf4GAAD/+wAA//8AAwMEAP//AAAA/wIABwQAAAcFDwAEBgMAAQIBAAQDAwADAgMAAgICAAH//wD9/P8ABgUFAP0GCQD8/AEABQsHAPH36gD+BQoABgIDAAAA/gD//v0A/wD9AAABAgAC/P4A/AD+AMPK7AARCwQAKiMPAAMEBAAFBQUA/f3+AAAAAAD+/v8A/v7/AAQMDAwADBAMAOjuEgADBAcACgIHAAIG/wD+/gQA+/v8AAYFCgD6/f8A/wD9AAH9AgABBAEAAgMFAAQEBAACAgEAAgIBAAIAAwACAwIA/gQDAAT/BgAKBAkAAP/+AOTs+gDZ5f8ACwgCAAcGCQABAwEA9/j0AAIBAwD29fkA/gADAAD//wD5+/8AtcTnAA8OBQAQDf8ABQUGAAMDBAD+/v4A+vr6APX19QAEGBkZAAABAgANACgABPoHAPD0+wABAgIA+/v4ABQUFQDx8fIA/gIAAP39AAD5+fsABAQIAAEBAAACAgIAAAAAAAUHCwACAgIAAwMDAP8C+gAFCAIA+/r8APH6/gC91eUACxMcAAD6+AAB/wUA+Pn2ABkWFAD2+PkAAAADAPf19gD+/v4A//8AAPX2CgCzwOYAVkcaAAwKCAACAgIABAQEAO7u7gAVFRUABO3r6wD4AQwA4vH1APz47gD//wgA/gIEAAD//gAA//gAAAIGAPz8+AD///8A7u7xAP4BAQABAgAAAAEBAAICAgD//v4A/P38APn5/QD8/v8A/f77APXy9wCXtesAzNz0AAYHDwD3+voAAgD4AAD//QACA/kA+vz9AP7+BAABAgcA/vz6AP79+QAAAQUA6PEFANLX7QA1LhMA//0DAPX26gD6+v0A9PT0AAT//v4A8fkMAPb4/QD9+PwABAT9AAAAAwABAAEA9vf5AAACAQD//wEA/vz3AP369gAB//8AAAIDAP8AAAACAQEA/v38AP7+/gD///8AAf34APP2/QCPsO4A0N72AAMDCQD2+PsAAAABAP4BAAD//wEA8fP3AAEBAgD///8AAQEEAAUECAD5/PYA//r1AAAEFADV3/YAKSX3AP/4/QD7/vwA/f3+AP///wACAwUEAPT4DwD//v8A////AP///gAAAAAAAAAAAP///wD/AAAAAAAAAP0ACADo7vgA+AD8AAAB/wAAAAAAAAAAAP7+/gABAQEA/f7/AOPt+gCNq+oA0eD6AAYDCQAB/wEAAQEBAAEBAQACAQQAAQEEAAIBBAD///8A/v7+AAAAAQAA/wIABAQHAP0C+wAA/fgA7/EFANna8wAA//0A/v//AP///wD///8AAvr8AQD1/AgAAf//AAEBAQD+/v8A/gAAAP8CAQD+AP8AAP//AAAAAAABAfsA3OoNAOzr6gD//gIA/wD/AP39/gD/AP4A/v0AANRKPD0AAApLSURBVM/b6QB8n+oA1+QDAAQCBQD/AQMAAAEDAAIBAwADAgQA/wIBAAACAgD+AQAAAP8BAP///AAAAf4AAAQEAAQECAACAwUA/gAAAPsBAADT4PkA+v/9AAD8AQD+/v4AAAAAAAL2+wsA/P4DAAD/AAD///8AAQEAAAEBAAABAQEAAQEBAAAAAAAAAAAAAgEHAO/9DADHzd0A+/v4AP/+/wD9/vwA/PoAAO/16QC0xu4A7vYEAAAA+gAAAAAAAAAAAAEBAQD///8AAAAAAAEAAAD///8A/v7+AP3+/QAAAAEAAAAAAAMDAgD+/v4AAP8CAAIBBQAC/wUA8vIFAO3x+AD9/vgA/f39AP39/QAC9/oLAP38AwD//v4AAAAAAAABBAAB/v8AAPzvAAH+9wD/AAIAAAD9AAEBBAD+CBEA1d74ANjX2QD9/vwA/AD7APf3+gDv8QAA2d3sAAL/9AD+//4AAgICAAEBAgD/AAAAAAECAAABAQD/AAAAAAABAAAAAQD9/PsAAQECAAEBAQD9/f8A/v7+AAIBBAD+/gAAAQACAPv/AgDn6/sA+/v7AP39/QD+/v4ABPf7CwD+/f0AAAIAAAAA/wD//f0A/PnYAAADCgAFCSAA/PnjAAQFGgAB++EA/gD+AOL1HQDD0dYA+wH/ABsTHQD4+fAA0uf+AO/4CQAHCwEAAQIBAP7+AAAAAAAAAAACAAACAQD+AAAA//8AAP///wAA//4AAAH9AAEBBgD+/gAA/Pv9AAEAAAAAAAIAAQEAAAAAAwD+AgIA5ur9AB0V/gACAQUAAQEBAAT2/Q4AAP35AAIECgD9//wAAf3zAAIDGwADCxsA+vTQAAkHFgDz88QADAgxAAL74gD5CxQAz+AXANbUwAAOEAcABg8zANjqAAAFCAoA//8DAAD+AgABAgAAAAAAAAAA/wD+/gAAAgIBAAAAAAABAQEA/v8AAAICAwABAQEA+/r6AAEBAAD+//4A/v7+AAQEAQACAQIA/v8GAO7xAgD29/4ABgT6AAcHBwAE/v4EAAAB9wD/AQAAAAIDAP/9/AABAwQA+/vcAAL68QD8+/gABwEHAPf+5gAHBxcAAAAFAPUJIAD3BRwA7f4AAPzs+gD+Af0ABQEIAP8CAQABAP8A/v/9AP4C/wAB/gYAAQH/AP/+AAAAAAAAAf8AAP/8+AACAwMAAQABAAAB/wABAAQA/v38AAABAwACAwcA/QH/AP7+AgD7AAYA8fX2ABAJAgAAAfgAAv8A/gAB//4ABPvgAP3x0QABBRIA/f7uAP7qxgD9//4A+frjAAAOIQAI/hEAAQIKAAACBAAB//0AAAEJAAEBAwABAQMAAgEFAAACAwAA/fUA//7/AP700gAC/gUAAAH7AAAAAQAAAAEAAAABAAIBAgAA//wABAQEAP//AAABAQEAAAH9APz9/gACAwQA//8CAP7//wABAAEAAAAPAOvvBwD+//sA//4AAAIAAQAAA/4BAPv5+QALDxkAAf3/AAL48QD/BhYAAgUJAAEGHQAB8+EA+/viAPLtuQAE+/EAAQAAAP///gD+//8A//4BAAQDBgAAAQAA/vnlAP7+9AAABRIAAP//AAMABAABAQEAAgICAAECAQAAAAAAAQIBAAMCAwAAAAAAAQEBAPj58wAEAwcAAgEDAAEBAAAB/wAAAv/+APz/AADt8wgA/wD6AP8AAwACAfz6AAEEAgAADSUA+f4HAPr01QAKCQkABhE6AAD89AAEAv4AAQXzAAUHGQAQDjEA9/TZAAL96gD/APUAAP8DAAED+gD99d0A+/71AP0CBwD++OUA/f//AP8E/gD+AgMAAAIAAAD+/gAHBwAA/gT+AAEABQD9AgEABgkBAPz69AAGAgQABAMHAP3//wD//v8AAv//AAAB/wD///wA8/cLAPn5BwAAAP4ABPPy6QAA/wEACQcdAB4gLAAEACUABQgIAP3+AAD/8BcAAAMVAAME8AD0+e8ADQ0vAPTvAQAJBAQA8PfiAAgPHwD7/BEA5u3PAAUA/QAAAgAA+/nnAAoLHgD9AQUAAv8CABYVDAAHBfoADQoPAPb4+wAC/PYA/wMJAAUE+QDyDP4AAwMHAPn5/AAAAAYA/f0BAAME/wD8/gMA6unpAPv+AgDv9QUADw36AAQC/wUA9fbqAAMHBwAEAhIA+vf/AP/++AD8/f0A/vweAP/32wAIDj8AAQf1APL1/QDv6+8AAAT0AAQBEAD5988A/gYXAAwLCQDw78oABwknAP387QD8A/wABAX/AAH9AgADAAEAAPwDAO3u+AD0+PgACAX7AAUJAwDs7/EAAf7/AAYD+gDt7fIAAgkRAPr6+gANDQYAAP38AAwMCwDy9/MA8PUFAAH//wAE/wT7APv8+AD7+PkA4uXsAAD7AgCAd0cA3N3gAPkBCQC2s+UA6N++AAQGBQD6/QQA/gIEAATyAwD3BOIAAv38AP334wAEEjMAAQopAAMKEgAICA4AA/n5AAH66QAAAgUA5ukDAAAFDgABAAUAAf//APT39gAD8/wA+/j4AP7++gD79/QAAAIHAAT/+QD/AP4A9PT5AP39/wD///8AAAD/APDyAgD+AgIABAoJHQACEgwA/vwAAP4ABAAREQoAExQNAP37HgD2+PwAhnllALnH2AC4wuYABvz/AAP25QDwAecACAQnAAAODAAgIg0AKSkGAA4CHAADBQMABgQDAP0A/gDn5uwA5uXjAPT28wD19gMA9voOAAMKJAD/Aw8A/P35AP8AAAADAwMA+P39AAUEAgAAAAEA////AP39/gABAQIA////AAP/BADz+f8A8PMBAAMSHz6A+/nyAPf15wALDRIA+PgBAMvO3wDv8O8ALS4ZAPf6+wAjGwwARTwlAOvz+ADm8AkACP4GAP8B7gAEARIAHR4VAA8QBgD59e8A+vz5APP69wD18fIA8fLvAAQGAgAcHBoANDIpADs2IAAiHQkA2eHxAPj5+gD/+vUA/vz9AP///gADAwgAAgIFAAMEBAABAAEAAAAAAP8AAAAB//4A/AAAAAsG9gAE+fjtAPb18gAAAPwA/Pn3AAMGCwDt8PcA7O/9AMnRvgA/OB4AOjAfACMmDwDl4O4AST8gAMDLAQD/8x4A/wL2ANrd6ADj4PYA//7nAPv28gAJB/cAExITABoYFgAVFBEAFhYUAAECAgABAQAA9fLoAPjs1gDx7u8ABQgXAAgMIgAAAgcAAQQGAAABAQAA/wAA/v4AAAEBAAABAAEA/wD9AAIB/wDh5vwABAD8/gAEBQoA/Pz8APr6+wAKBwAA/wEFAAEC/gACAgQAxMznAMnP7AAeHg0ABAQMAGBWKgANBwAAAwP+AK683AASER0A//3/AAMEKgDv8fkA9f34APLz7ADc6vIAAPznAAAC5wD///8AAAIDAAcEEgD7/BQA/fzqAAD68gAKAwEABAQNAAAEAwAB//8AAP8AAAICAQAAAAAA////AAABCAAB//YA8vkDALWFPTclMfKgAAAAAElFTkSuQmCC',1,'2016-09-18 09:53:54','Joakim','2016-09-18 09:53:54',NULL,0);

/*Table structure for table `news_articles` */

DROP TABLE IF EXISTS `news_articles`;

CREATE TABLE `news_articles` (
  `title` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `preamble` text COLLATE utf8_swedish_ci,
  `text` text COLLATE utf8_swedish_ci,
  `publish_at` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `unpublish_at` datetime DEFAULT '2999-12-31 23:59:59',
  `author` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_internal` tinyint(1) DEFAULT '1',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `news_articles` */

insert  into `news_articles`(`title`,`preamble`,`text`,`publish_at`,`unpublish_at`,`author`,`is_internal`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values ('Ett första inlägg','<p>Detta &auml;r ingressen f&ouml;r inl&auml;gget.&nbsp;<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a quam dolor, id varius risus. Donec libero elit, ornare et suscipit vitae, varius ac neque. Nam id purus tortor, sit amet suscipit sem. Duis tempus nisl nec turpis mollis interdum. Nulla facilisi. Aenean pulvinar lobortis felis at mattis.</span></p>','<p><span>Detta &auml;r texten som f&ouml;ljer efter inl&auml;gget. Integer quam augue, consequat ornare ultricies sit amet, porta ac lorem. Morbi in eros dui, ac suscipit erat. Sed ut ante ac mi venenatis auctor. Nulla scelerisque sagittis rutrum. Cras ut tellus neque. Aliquam scelerisque leo suscipit felis faucibus imperdiet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </span></p>\r\n<ul>\r\n<li>Cras quis quam nulla, id varius enim.</li>\r\n<li>Donec neque ante, laoreet sed sollicitudin et, vehicula at nisl.</li>\r\n<li>Maecenas faucibus mattis erat eget blandit.</li>\r\n</ul>\r\n<p><span>Praesent sed enim augue, ut auctor nunc. Pellentesque neque purus, fringilla nec dapibus a, aliquam ac nibh. In ut placerat tellus. Vestibulum ut nisl lorem, vitae accumsan tellus. Sed vitae scelerisque arcu. Ut aliquam vestibulum nibh, at congue odio ullamcorper quis.</span></p>','2012-09-06 08:54:56',NULL,'Joakim Molin',0,1,'2012-09-06 08:54:56',NULL,'2012-09-06 08:54:56',NULL,0),('Magic inställt på grund av regn','<p>Aliquam a quam dolor, id varius risus. Donec libero elit, ornare et suscipit vitae, varius ac neque. Nam id purus tortor, sit amet suscipit sem. Duis tempus nisl nec turpis mollis interdum. Nulla facilisi. Aenean pulvinar lobortis felis at mattis.</p>','<p>Praesent sed enim augue, ut auctor nunc. Pellentesque neque purus, fringilla nec dapibus a, aliquam ac nibh. In ut placerat tellus. Vestibulum ut nisl lorem, vitae accumsan tellus. Sed vitae scelerisque arcu. Ut aliquam vestibulum nibh, at congue odio ullamcorper quis.</p>','2012-11-07 15:13:58',NULL,'Kortspelsansvarig',0,2,'2012-11-07 15:13:58',NULL,'2012-11-07 15:13:58',NULL,0);

/*Table structure for table `order_payments` */

DROP TABLE IF EXISTS `order_payments`;

CREATE TABLE `order_payments` (
  `transaction_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Order payment must belong to an existing order` (`order_id`),
  KEY `Order payment must reference an existing transaction` (`transaction_id`),
  CONSTRAINT `Order payment must belong to an existing order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Order payment must reference an existing transaction` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `order_payments` */

insert  into `order_payments`(`transaction_id`,`order_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (1,2,1,'2016-09-18 09:47:37','Joakim','2016-09-18 09:47:37',NULL,0);

/*Table structure for table `order_rows` */

DROP TABLE IF EXISTS `order_rows`;

CREATE TABLE `order_rows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_swedish_ci DEFAULT NULL,
  `count` int(11) NOT NULL,
  `price_per_each` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `status` enum('delivered','not_delivered') COLLATE utf8_swedish_ci DEFAULT NULL,
  `price_model` enum('normal','custom') COLLATE utf8_swedish_ci DEFAULT NULL,
  `note` text COLLATE utf8_swedish_ci,
  PRIMARY KEY (`id`),
  KEY `Order row must belong to an existing order` (`order_id`),
  KEY `Order row must reference existing article or be NULL` (`article_id`),
  CONSTRAINT `Order row must belong to an existing order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Order row must reference existing article or be NULL` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `order_rows` */

insert  into `order_rows`(`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`order_id`,`article_id`,`name`,`count`,`price_per_each`,`total`,`status`,`price_model`,`note`) values (1,'2016-09-18 09:19:35','Joakim','2016-09-18 09:19:49','Joakim',0,1,3,'Snygg mugg',1,'50.00','50.00','not_delivered','normal',''),(3,'2016-09-18 09:47:15','Joakim','2016-09-18 09:47:15','Joakim',0,2,5,'Hela konventet',1,'300.00','300.00','delivered','normal','');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('cart','submitted','partially_paid','paid','cancelled') COLLATE utf8_swedish_ci DEFAULT NULL,
  `dibs_secret` varchar(32) COLLATE utf8_swedish_ci DEFAULT NULL,
  `preferred_payment_method` varchar(32) COLLATE utf8_swedish_ci DEFAULT NULL,
  `total` decimal(11,2) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `has_been_submitted` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Order must reference existing person or NULL` (`person_id`),
  CONSTRAINT `Order must reference existing person or NULL` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`status`,`dibs_secret`,`preferred_payment_method`,`total`,`note`,`submitted_at`,`person_id`,`has_been_submitted`) values (1,'2016-09-18 08:20:08','Joakim','2016-09-18 08:20:08','Joakim',0,'submitted',NULL,NULL,'50.00','','2016-09-18 08:20:08',15391,0),(2,'2016-09-18 09:43:10','Joakim','2016-09-18 09:43:10','Joakim',0,'paid',NULL,NULL,'300.00','','2016-09-18 09:43:10',15310,0);

/*Table structure for table `organizers` */

DROP TABLE IF EXISTS `organizers`;

CREATE TABLE `organizers` (
  `person_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Organizers must reference an existing event` (`event_id`),
  KEY `Organizers must reference an existing person or NULL` (`person_id`),
  KEY `Organizer must reference an existing group or NULL` (`group_id`),
  CONSTRAINT `Organizer must reference an existing group or NULL` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `Organizers must reference an existing event` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Organizers must reference an existing person or NULL` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `organizers` */

insert  into `organizers`(`person_id`,`group_id`,`event_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (15310,NULL,NULL,1,'2015-04-06 14:23:27','Joakim','2015-04-06 14:23:27',NULL,0),(NULL,3,NULL,2,'2015-04-06 14:47:04','Joakim','2015-04-06 14:47:04',NULL,0),(NULL,2,NULL,3,'2015-04-06 14:50:07','Joakim','2015-04-06 14:50:07',NULL,0),(NULL,1,NULL,4,'2015-04-06 14:51:08','Joakim','2015-04-06 14:51:08',NULL,0),(NULL,NULL,NULL,5,'2015-04-06 14:57:45','Joakim','2015-04-06 14:57:45',NULL,0);

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `first_name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `email_is_public` tinyint(2) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `phone_number` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `identification` varchar(16) COLLATE utf8_swedish_ci DEFAULT NULL,
  `street_address` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8_swedish_ci DEFAULT NULL,
  `city` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `country` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `foreign_address` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `note` text COLLATE utf8_swedish_ci,
  `internal_note` text COLLATE utf8_swedish_ci,
  `is_outdated` smallint(1) DEFAULT '0',
  `xp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Identification must be unique` (`identification`)
) ENGINE=InnoDB AUTO_INCREMENT=15392 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `people` */

insert  into `people`(`first_name`,`last_name`,`email_address`,`email_is_public`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`phone_number`,`identification`,`street_address`,`zip`,`city`,`country`,`foreign_address`,`note`,`internal_note`,`is_outdated`,`xp`) values ('Joakim','Molin','joakim.molin@mogul.com',0,15310,'2014-09-07 01:00:21','Joakim','2014-10-15 23:41:03','Joakim',0,'0734 330045','7709221977','Wärnsköldsgatan 1a','41471','Göteborg',NULL,'',NULL,'Ingen åtkomst',0,0),('Nina','Ekeblad','dalbeke70@hotmail.com',0,15312,'2012-09-12 22:21:17',NULL,'2014-10-14 23:28:19','Joakim',0,NULL,'7005254803','Wärnsköldsgatan 1a','41471','Göteborg',NULL,'','','Ingen åtkomst',0,0),('Linus','Ekeblad','linus.ekeblad@hotmail.com',0,15316,'2012-09-16 22:53:39',NULL,'2014-09-21 11:21:22','Joakim',0,'0733 322704','1102227731','Wärnsköldsgatan 1a','41471','Göteborg',NULL,'',NULL,'Ingen åtkomst',0,0),('Karin','Karlsson','irmelin.drake@hotmail.com',0,15333,'2012-11-06 18:05:29',NULL,'2012-11-06 18:05:29',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,0,0),('Martin','Stahrberg','m.stahrberg@telia.se',0,15334,'2012-11-06 19:54:14',NULL,'2012-11-06 19:54:14',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,0,0),('Joakim','Karlsson','joakim.molin@hotmail.com',0,15361,'2012-11-26 09:33:25',NULL,'2015-04-05 09:47:41','Joakim',0,'014217216','7709221976','Föreningsgatan 21','59551','Mjölby',NULL,'',NULL,'Ingen åtkomst',0,0),('Assi','Molin','assimolin@hotmail.com',NULL,15385,'2014-09-18 20:19:20','Joakim','2014-09-18 20:21:59','Joakim',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Ingen åtkomst',NULL,NULL),('Kalle','Anka','joakim.ekeblad@hotmail.com',NULL,15386,'2014-10-12 22:31:06','Joakim','2014-10-12 22:31:06',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Musse','Pigg','m.pigg@ankeborg.se',NULL,15387,'2014-12-04 22:23:45','Joakim','2014-12-04 22:23:45',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Kajsa','Anka','kajsa.anka@ankeborg.com',0,15388,'2015-03-11 16:51:30','Joakim','2015-03-11 16:51:30',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Jan','Långben','jannel@ankeborg.se',NULL,15389,'2015-03-15 10:26:10','Joakim','2015-03-15 10:26:10',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Irene','Antonsson','irene.antonsson@jcamaltider.se',0,15390,'2015-03-20 20:38:49','Joakim','2015-03-20 20:38:49',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('Ann-Sofie','Karlsson','assi@hotmail.com',0,15391,'2015-04-05 09:45:37','Joakim','2015-04-05 11:47:25','Joakim',0,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `resource_types` */

DROP TABLE IF EXISTS `resource_types`;

CREATE TABLE `resource_types` (
  `name` varchar(63) COLLATE utf8_swedish_ci NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `resource_types` */

insert  into `resource_types`(`name`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values ('Aula',1,'2015-04-06 23:13:11','Joakim','2015-04-06 23:13:11',NULL,0),('Klassrum',2,'2015-04-06 23:13:26','Joakim','2015-04-06 23:13:26',NULL,0),('Gymnastiksal',3,'2015-04-06 23:13:38','Joakim','2015-04-06 23:13:38',NULL,0);

/*Table structure for table `resources` */

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `name` varchar(63) COLLATE utf8_swedish_ci NOT NULL,
  `resource_type_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `available` tinyint(4) DEFAULT '1',
  `description` text COLLATE utf8_swedish_ci,
  PRIMARY KEY (`id`),
  KEY `A resource must be of an specific type or NULL` (`resource_type_id`),
  CONSTRAINT `A resource must be of an specific type or NULL` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_types` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `resources` */

insert  into `resources`(`name`,`resource_type_id`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`available`,`description`) values ('Södra Aula',1,1,'2015-04-06 23:26:07','Joakim','2015-04-06 23:26:07',NULL,0,1,''),('N102',2,2,'2015-04-07 13:54:38','Joakim','2015-04-07 13:54:38',NULL,0,1,'');

/*Table structure for table `sent_emails` */

DROP TABLE IF EXISTS `sent_emails`;

CREATE TABLE `sent_emails` (
  `subject` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `message` text COLLATE utf8_swedish_ci,
  `from` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `to` text COLLATE utf8_swedish_ci,
  `cc` text COLLATE utf8_swedish_ci,
  `bcc` text COLLATE utf8_swedish_ci,
  `as_html` tinyint(4) DEFAULT NULL,
  `no_of_recipients` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `sent_emails` */

/*Table structure for table `signup_slot_articles` */

DROP TABLE IF EXISTS `signup_slot_articles`;

CREATE TABLE `signup_slot_articles` (
  `signup_slot_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Only_one_article_per_slot` (`signup_slot_id`),
  KEY `A signup slot article must be an article` (`article_id`),
  CONSTRAINT `A signup slot article must be an article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `A signup slot article must belong to a signup slot` FOREIGN KEY (`signup_slot_id`) REFERENCES `signup_slots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `signup_slot_articles` */

/*Table structure for table `signup_slots` */

DROP TABLE IF EXISTS `signup_slots`;

CREATE TABLE `signup_slots` (
  `event_occation_id` int(11) NOT NULL,
  `slot_type_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `maximum_signup_count` int(11) DEFAULT NULL,
  `maximum_spare_signup_count` int(11) DEFAULT NULL,
  `requires_approval` tinyint(2) DEFAULT '0',
  `is_hidden` smallint(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `A signupslot must be connected to an existing event occation` (`event_occation_id`),
  KEY `A signup slot must be of an existing type` (`slot_type_id`),
  CONSTRAINT `A signup slot must be of an existing type` FOREIGN KEY (`slot_type_id`) REFERENCES `slot_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `A signupslot must be connected to an existing event occation` FOREIGN KEY (`event_occation_id`) REFERENCES `event_occations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `signup_slots` */

/*Table structure for table `signups` */

DROP TABLE IF EXISTS `signups`;

CREATE TABLE `signups` (
  `signup_slot_id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_approved` tinyint(1) DEFAULT '0',
  `is_paid` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Dont_signup_person_twice` (`signup_slot_id`,`person_id`),
  UNIQUE KEY `Dont_signup_group_twice` (`signup_slot_id`,`group_id`),
  KEY `The signup group must exist` (`group_id`),
  KEY `The signed up person must exist` (`person_id`),
  CONSTRAINT `The signed up person must exist` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `The signup group must exist` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `The signup slot referenced by the signup must exist` FOREIGN KEY (`signup_slot_id`) REFERENCES `signup_slots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `signups` */

/*Table structure for table `sleeping_resources` */

DROP TABLE IF EXISTS `sleeping_resources`;

CREATE TABLE `sleeping_resources` (
  `resource_id` int(11) NOT NULL,
  `no_of_slots` int(11) DEFAULT '0',
  `is_available` tinyint(4) NOT NULL DEFAULT '1',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Sleeping resource must have a resource` (`resource_id`),
  CONSTRAINT `Sleeping resource must have a resource` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `sleeping_resources` */

insert  into `sleeping_resources`(`resource_id`,`no_of_slots`,`is_available`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (1,100,1,1,'2015-04-06 23:26:07','Joakim','2015-04-06 23:26:25','Joakim',0),(2,25,1,2,'2015-04-07 13:54:38','Joakim','2015-04-07 13:54:38',NULL,0);

/*Table structure for table `sleeping_slot_bookings` */

DROP TABLE IF EXISTS `sleeping_slot_bookings`;

CREATE TABLE `sleeping_slot_bookings` (
  `sleeping_resource_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Sleeper person must exist` (`person_id`),
  KEY `SleepingSlotBooking must reference existing SleepingResource` (`sleeping_resource_id`),
  CONSTRAINT `Sleeper person must exist` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `SleepingSlotBooking must reference existing SleepingResource` FOREIGN KEY (`sleeping_resource_id`) REFERENCES `sleeping_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `sleeping_slot_bookings` */

/*Table structure for table `slot_types` */

DROP TABLE IF EXISTS `slot_types`;

CREATE TABLE `slot_types` (
  `cardinality` smallint(6) NOT NULL DEFAULT '1',
  `name` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `slot_types` */

insert  into `slot_types`(`cardinality`,`name`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`) values (1,'Spelare',1,'2012-09-06 20:14:41',NULL,'2012-09-06 20:14:41',NULL,0),(5,'Lag om 5 personer',2,'2012-09-09 22:10:36',NULL,'2012-09-09 22:10:36',NULL,0),(1,'Spelledare',3,'2012-09-14 21:11:06',NULL,'2012-09-14 21:11:06',NULL,0),(1,'Arbetare',4,'2012-10-12 09:57:50',NULL,'2012-10-12 09:57:50',NULL,0),(4,'Lag om 4 personer',5,'2015-04-05 11:37:30','Joakim','2015-04-05 11:37:30',NULL,0),(5,'Lag om 5 personer (Mutant)',6,'2015-04-05 11:37:54','Joakim','2015-04-05 11:37:54',NULL,0),(1,'Spelledare (Mutant)',7,'2015-04-05 11:42:45','Joakim','2015-04-05 11:42:45',NULL,0);

/*Table structure for table `timed_bookings` */

DROP TABLE IF EXISTS `timed_bookings`;

CREATE TABLE `timed_bookings` (
  `booking_id` int(11) NOT NULL,
  `starts_at` datetime DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Timed booking must have a booking` (`booking_id`),
  CONSTRAINT `Timed booking must have a booking` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `timed_bookings` */

/*Table structure for table `timespans` */

DROP TABLE IF EXISTS `timespans`;

CREATE TABLE `timespans` (
  `starts_at` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `ends_at` datetime NOT NULL DEFAULT '9999-12-31 23:59:59',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_schedule` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_master_timespan` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `timespans` */

insert  into `timespans`(`starts_at`,`ends_at`,`is_public`,`show_in_schedule`,`name`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`is_master_timespan`) values ('2015-04-02 08:00:00','2015-04-02 12:00:00',1,1,'To fm',1,'2015-04-06 14:24:09','Joakim','2015-04-06 14:29:55','Joakim',0,0),('2015-04-02 13:00:00','2015-04-02 17:00:00',1,1,'To em',2,'2015-04-06 14:24:38','Joakim','2015-04-06 14:28:18','Joakim',0,0),('2015-04-02 18:00:00','2015-04-02 22:00:00',1,1,'To kväll',3,'2015-04-06 14:24:59','Joakim','2015-04-06 14:28:25','Joakim',0,0),('2015-04-02 23:00:00','2015-04-03 03:00:00',1,1,'To natt',4,'2015-04-06 14:27:31','Joakim','2015-04-06 14:28:32','Joakim',0,0),('2015-04-03 08:00:00','2015-04-03 12:00:00',1,1,'Fr fm',5,'2015-04-06 14:29:12','Joakim','2015-04-06 14:30:19','Joakim',0,0),('2015-04-03 13:00:00','2015-04-03 17:00:00',1,1,'Fr em',6,'2015-04-06 14:29:35','Joakim','2015-04-06 14:30:10','Joakim',0,0),('2015-04-03 18:00:00','2015-04-03 22:00:00',1,1,'Fr kväll',7,'2015-04-06 14:30:40','Joakim','2015-04-06 14:30:40',NULL,0,0),('2015-04-03 23:00:00','2015-04-04 03:00:00',1,1,'Fr natt',8,'2015-04-06 14:30:57','Joakim','2015-04-06 14:30:57',NULL,0,0),('2015-04-04 08:00:00','2015-04-04 12:00:00',1,1,'Lö fm',9,'2015-04-06 14:34:49','Joakim','2015-04-06 14:34:49',NULL,0,0),('2015-04-04 13:00:00','2015-04-04 17:00:00',1,1,'Lö em',10,'2015-04-06 14:35:09','Joakim','2015-04-06 14:35:09',NULL,0,0),('2015-04-04 18:00:00','2015-04-04 22:00:00',1,1,'Lö kväll',11,'2015-04-06 14:35:26','Joakim','2015-04-06 14:35:26',NULL,0,0),('2015-04-04 23:00:00','2015-04-05 03:00:00',1,1,'Lö natt',12,'2015-04-06 14:35:41','Joakim','2015-04-06 14:35:41',NULL,0,0),('2015-04-05 08:00:00','2015-04-05 12:00:00',1,1,'Sö fm',13,'2015-04-06 14:36:02','Joakim','2015-04-06 14:36:02',NULL,0,0),('2014-04-02 08:00:00','2014-04-05 12:00:00',1,0,'Hela konventet',14,'2015-04-06 14:36:33','Joakim','2015-04-09 14:15:45','Joakim',0,0),('2015-04-02 23:00:00','2015-04-03 07:00:00',1,0,'Sovning to-fr',15,'2015-04-06 14:52:15','Joakim','2015-04-09 14:13:31','Joakim',0,0);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `amount` decimal(10,2) DEFAULT NULL,
  `payment_method` enum('cash','cc','wire_transfer','payment_service') COLLATE utf8_swedish_ci NOT NULL,
  `note` text COLLATE utf8_swedish_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `transactions` */

insert  into `transactions`(`amount`,`payment_method`,`note`,`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`cancelled`) values ('300.00','cc','Betalning för order # 2',1,'2016-09-18 09:47:37','Joakim','2016-09-18 09:47:37',NULL,0,0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(63) COLLATE utf8_swedish_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `username` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_swedish_ci DEFAULT NULL,
  `reset_password_token` varchar(32) COLLATE utf8_swedish_ci DEFAULT 'NULL',
  `salt` varchar(16) COLLATE utf8_swedish_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `is_active` tinyint(2) DEFAULT '0',
  `most_recent_login` datetime DEFAULT NULL,
  `old_password` varchar(64) COLLATE utf8_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Username must be unique or null` (`username`),
  KEY `User must reference an existing person or NULL` (`person_id`),
  CONSTRAINT `User must reference an existing person or NULL` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`created_at`,`created_by`,`modified_at`,`modified_by`,`is_deleted`,`username`,`password`,`reset_password_token`,`salt`,`level`,`person_id`,`is_active`,`most_recent_login`,`old_password`) values (2,'2012-09-06 00:11:59',NULL,'2014-10-16 00:17:02',NULL,0,'Joakim','78727242309aca737746d0a7f8ad6527','','X3bShaeThkMR',100,15310,1,'2016-09-18 08:05:29',''),(4,'2012-09-13 10:10:21',NULL,'2012-11-06 20:23:16',NULL,0,'Nina','b3f41c99ff6cf325bef0f44d6de8ea52','','to6dU2iiT2o3',100,15312,1,'2014-11-02 14:41:09',''),(7,'2012-09-16 22:53:39',NULL,'2014-09-18 21:20:05',NULL,0,'Linus','7f72a6647f4d8646c48707b481799aaf','','Wjf0bo0bLFe1',30,15316,1,'2015-02-09 22:45:08',''),(11,'2012-11-06 19:54:14',NULL,'2012-11-06 19:54:14','Joakim',0,'Martin','8961df469878b38e0e07e3902d0c8f02','','5Kd0KVp3RK3#',20,15334,1,'2012-11-23 14:49:13',''),(12,'2012-11-06 20:27:27',NULL,'2014-09-07 17:46:52','',0,'Karin','fd0f96334528895012d9fe99f0b81eac','','Cj?L1RkcSUcD',30,15333,1,'2014-09-07 17:47:04',''),(13,'2012-11-23 20:24:06','Joakim','2012-11-23 20:24:06',NULL,0,'Niklas','f157f8a2e76a289780fb9bd657a6c756','','LCkVhDXgFc#B',0,NULL,0,'0000-00-00 00:00:00',''),(15,'2012-11-26 09:33:25',NULL,'2015-03-29 20:20:25','Joakim',0,'Joakim77','10374946106b3cda1a93e9d94d0ae86b','','09zbXfofMKet',0,15361,1,'2015-03-11 17:58:03',''),(16,'2014-09-07 19:07:44','Joakim','2014-09-07 19:07:44',NULL,0,'Hey','870d25c462464f1062cc52d9404cdd5b',NULL,'RB!z9g56rUmd',1,NULL,1,NULL,NULL),(17,'2014-09-09 22:51:44','Joakim','2014-09-09 22:51:46','Joakim',0,'Anna',NULL,NULL,NULL,0,NULL,1,NULL,NULL),(18,'2015-03-11 16:54:33','Joakim','2015-03-11 16:54:33',NULL,0,'',NULL,NULL,NULL,1,15388,0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
