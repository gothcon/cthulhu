<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventOccationBooking
 */
class EventOccationBooking
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\EventOccation
     */
    private $eventOccation;

    /**
     * @var \Entities\Booking
     */
    private $booking;

	
	function __construct(){
		$this->booking = new Booking();
		$this->booking->setEventOccationBooking($this);
	}

	/**
	 * 
	 * @param Resource $resource
	 * $return EventOccationBooking
	 */
	public function setResource($resource){
		$this->booking->setResource($resource);
		return $this;
	}
	
	/**
	 * 
	 * @return Resource
	 */
	public function getResource(){
		return $this->booking->getResource();
	}
	
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EventOccationBooking
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return EventOccationBooking
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return EventOccationBooking
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return EventOccationBooking
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return EventOccationBooking
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventOccation
     *
     * @param \Entities\EventOccation $eventOccation
     * @return EventOccationBooking
     */
    public function setEventOccation(\Entities\EventOccation $eventOccation = null)
    {
        $this->eventOccation = $eventOccation;
		
		if(is_object($eventOccation)){
			$eventOccation = \GothConEntityManager::getInstance()->getRepository("Entities\\EventOccation")->find($eventOccation->getId());
		}
		
        return $this;
    }

    /**
     * Get eventOccation
     *
     * @return \Entities\EventOccation 
     */
    public function getEventOccation()
    {
        return $this->eventOccation;
    }

    /**
     * Set booking
     *
     * @param \Entities\Booking $booking
     * @return EventOccationBooking
     */
    public function setBooking(\Entities\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \Entities\Booking 
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
