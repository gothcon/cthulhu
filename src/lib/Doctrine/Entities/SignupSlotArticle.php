<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignupSlotArticle
 */
class SignupSlotArticle
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\SignupSlot
     */
    private $signupSlot;

    /**
     * @var \Entities\Article
     */
    private $article;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SignupSlotArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return SignupSlotArticle
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return SignupSlotArticle
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return SignupSlotArticle
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return SignupSlotArticle
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set signupSlot
     *
     * @param \Entities\SignupSlot $signupSlot
     * @return SignupSlotArticle
     */
    public function setSignupSlot(\Entities\SignupSlot $signupSlot = null)
    {
        $this->signupSlot = $signupSlot;

        return $this;
    }

    /**
     * Get signupSlot
     *
     * @return \Entities\SignupSlot 
     */
    public function getSignupSlot()
    {
        return $this->signupSlot;
    }

    /**
     * Set article
     *
     * @param \Entities\Article $article
     * @return SignupSlotArticle
     */
    public function setArticle(\Entities\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Entities\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
