<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
class Event
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $isDeleted;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var string
     */
    private $preamble;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $info;

    /**
     * @var string
     */
    private $internalInfo;

    /**
     * @var string
     */
    private $scheduleName;

    /**
     * @var boolean
     */
    private $visibleInSchedule;

    /**
     * @var boolean
     */
    private $visibleInPublicListing;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Organizer
     */
    private $organizer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventOccations;

    /**
     * @var \Entities\EventType
     */
    private $eventype;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->eventOccations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isDeleted
     *
     * @param integer $isDeleted
     * @return Event
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return integer 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Event
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Event
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Event
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set preamble
     *
     * @param string $preamble
     * @return Event
     */
    public function setPreamble($preamble)
    {
        $this->preamble = $preamble;

        return $this;
    }

    /**
     * Get preamble
     *
     * @return string 
     */
    public function getPreamble()
    {
        return $this->preamble;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Event
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return Event
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set internalInfo
     *
     * @param string $internalInfo
     * @return Event
     */
    public function setInternalInfo($internalInfo)
    {
        $this->internalInfo = $internalInfo;

        return $this;
    }

    /**
     * Get internalInfo
     *
     * @return string 
     */
    public function getInternalInfo()
    {
        return $this->internalInfo;
    }

    /**
     * Set scheduleName
     *
     * @param string $scheduleName
     * @return Event
     */
    public function setScheduleName($scheduleName)
    {
        $this->scheduleName = $scheduleName;

        return $this;
    }

    /**
     * Get scheduleName
     *
     * @return string 
     */
    public function getScheduleName()
    {
        return $this->scheduleName;
    }

    /**
     * Set visibleInSchedule
     *
     * @param boolean $visibleInSchedule
     * @return Event
     */
    public function setVisibleInSchedule($visibleInSchedule)
    {
        $this->visibleInSchedule = $visibleInSchedule;

        return $this;
    }

    /**
     * Get visibleInSchedule
     *
     * @return boolean 
     */
    public function getVisibleInSchedule()
    {
        return $this->visibleInSchedule;
    }

    /**
     * Set visibleInPublicListing
     *
     * @param boolean $visibleInPublicListing
     * @return Event
     */
    public function setVisibleInPublicListing($visibleInPublicListing)
    {
        $this->visibleInPublicListing = $visibleInPublicListing;

        return $this;
    }

    /**
     * Get visibleInPublicListing
     *
     * @return boolean 
     */
    public function getVisibleInPublicListing()
    {
        return $this->visibleInPublicListing;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizer
     *
     * @param \Entities\Organizer $organizer
     * @return Event
     */
    public function setOrganizer(\Entities\Organizer $organizer = null)
    {
        $this->organizer = $organizer;

        return $this;
    }

    /**
     * Get organizer
     *
     * @return \Entities\Organizer 
     */
    public function getOrganizer()
    {
        return $this->organizer;
    }

    /**
     * Add eventOccations
     *
     * @param \Entities\EventOccation $eventOccations
     * @return Event
     */
    public function addEventOccation(\Entities\EventOccation $eventOccations)
    {
        $this->eventOccations[] = $eventOccations;

        return $this;
    }

    /**
     * Remove eventOccations
     *
     * @param \Entities\EventOccation $eventOccations
     */
    public function removeEventOccation(\Entities\EventOccation $eventOccations)
    {
        $this->eventOccations->removeElement($eventOccations);
    }

    /**
     * Get eventOccations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventOccations()
    {
        return $this->eventOccations;
    }

    /**
     * Set eventype
     *
     * @param \Entities\EventType $eventype
     * @return Event
     */
    public function setEventype(\Entities\EventType $eventype = null)
    {
        $this->eventype = $eventype;

        return $this;
    }

    /**
     * Get eventype
     *
     * @return \Entities\EventType 
     */
    public function getEventype()
    {
        return $this->eventype;
    }
    /**
     * @var \Entities\EventType
     */
    private $eventtype;


    /**
     * Set eventtype
     *
     * @param \Entities\EventType $eventtype
     * @return Event
     */
    public function setEventtype(\Entities\EventType $eventtype = null)
    {
        $this->eventtype = $eventtype;

        return $this;
    }

    /**
     * Get eventtype
     *
     * @return \Entities\EventType 
     */
    public function getEventtype()
    {
        return $this->eventtype;
    }
}
