<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventOccation
 */
class EventOccation
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $isHidden;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signupSlots;

    /**
     * @var \Entities\Timespan
     */
    private $timespan;

    /**
     * @var \Entities\EventOccationBooking
     */
    private $mainBooking;

    /**
     * @var \Entities\Event
     */
    private $event;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signupSlots = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EventOccation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return EventOccation
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return EventOccation
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return EventOccation
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return EventOccation
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EventOccation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isHidden
     *
     * @param boolean $isHidden
     * @return EventOccation
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return boolean 
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add signupSlots
     *
     * @param \Entities\SignupSlot $signupSlots
     * @return EventOccation
     */
    public function addSignupSlot(\Entities\SignupSlot $signupSlots)
    {
        $this->signupSlots[] = $signupSlots;

        return $this;
    }

    /**
     * Remove signupSlots
     *
     * @param \Entities\SignupSlot $signupSlots
     */
    public function removeSignupSlot(\Entities\SignupSlot $signupSlots)
    {
        $this->signupSlots->removeElement($signupSlots);
    }

    /**
     * Get signupSlots
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSignupSlots()
    {
        return $this->signupSlots;
    }

    /**
     * Set timespan
     *
     * @param \Entities\Timespan $timespan
     * @return EventOccation
     */
    public function setTimespan(\Entities\Timespan $timespan = null)
    {
        $this->timespan = $timespan;

        return $this;
    }

    /**
     * Get timespan
     *
     * @return \Entities\Timespan 
     */
    public function getTimespan()
    {
        return $this->timespan;
    }

    /**
     * Set mainBooking
     *
     * @param \Entities\EventOccationBooking $mainBooking
     * @return EventOccation
     */
    public function setMainBooking(\Entities\EventOccationBooking $mainBooking = null)
    {
        $this->mainBooking = $mainBooking;

        return $this;
    }

    /**
     * Get mainBooking
     *
     * @return \Entities\EventOccationBooking 
     */
    public function getMainBooking()
    {
        return $this->mainBooking;
    }

    /**
     * Set event
     *
     * @param \Entities\Event $event
     * @return EventOccation
     */
    public function setEvent(\Entities\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Entities\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventOccationBookings;


    /**
     * Add eventOccationBookings
     *
     * @param \Entities\EventOccationBooking $eventOccationBookings
     * @return EventOccation
     */
    public function addEventOccationBooking(\Entities\EventOccationBooking $eventOccationBookings)
    {
        $this->eventOccationBookings[] = $eventOccationBookings;

        return $this;
    }

    /**
     * Remove eventOccationBookings
     *
     * @param \Entities\EventOccationBooking $eventOccationBookings
     */
    public function removeEventOccationBooking(\Entities\EventOccationBooking $eventOccationBookings)
    {
        $this->eventOccationBookings->removeElement($eventOccationBookings);
    }

    /**
     * Get eventOccationBookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventOccationBookings()
    {
        return $this->eventOccationBookings;
    }
}
