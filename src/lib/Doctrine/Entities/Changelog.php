<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changelog
 */
class Changelog
{
    /**
     * @var string
     */
    private $entityClassName;

    /**
     * @var integer
     */
    private $entityId;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var string
     */
    private $operation;

    /**
     * @var string
     */
    private $serializedEntityData;

    /**
     * @var string
     */
    private $currentUser;

    /**
     * @var \DateTime
     */
    private $operationTime;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set entityClassName
     *
     * @param string $entityClassName
     * @return Changelog
     */
    public function setEntityClassName($entityClassName)
    {
        $this->entityClassName = $entityClassName;

        return $this;
    }

    /**
     * Get entityClassName
     *
     * @return string 
     */
    public function getEntityClassName()
    {
        return $this->entityClassName;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     * @return Changelog
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set entityName
     *
     * @param string $entityName
     * @return Changelog
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * Get entityName
     *
     * @return string 
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * Set operation
     *
     * @param string $operation
     * @return Changelog
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation
     *
     * @return string 
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set serializedEntityData
     *
     * @param string $serializedEntityData
     * @return Changelog
     */
    public function setSerializedEntityData($serializedEntityData)
    {
        $this->serializedEntityData = $serializedEntityData;

        return $this;
    }

    /**
     * Get serializedEntityData
     *
     * @return string 
     */
    public function getSerializedEntityData()
    {
        return $this->serializedEntityData;
    }

    /**
     * Set currentUser
     *
     * @param string $currentUser
     * @return Changelog
     */
    public function setCurrentUser($currentUser)
    {
        $this->currentUser = $currentUser;

        return $this;
    }

    /**
     * Get currentUser
     *
     * @return string 
     */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    /**
     * Set operationTime
     *
     * @param \DateTime $operationTime
     * @return Changelog
     */
    public function setOperationTime($operationTime)
    {
        $this->operationTime = $operationTime;

        return $this;
    }

    /**
     * Get operationTime
     *
     * @return \DateTime 
     */
    public function getOperationTime()
    {
        return $this->operationTime;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
