<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderRow
 */
class OrderRow
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $count;

    /**
     * @var string
     */
    private $pricePerEach;

    /**
     * @var string
     */
    private $total;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $priceModel;

    /**
     * @var string
     */
    private $note;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Article
     */
    private $article;

    /**
     * @var \Entities\Order
     */
    private $order;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OrderRow
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return OrderRow
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return OrderRow
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return OrderRow
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return OrderRow
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OrderRow
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return OrderRow
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set pricePerEach
     *
     * @param string $pricePerEach
     * @return OrderRow
     */
    public function setPricePerEach($pricePerEach)
    {
        $this->pricePerEach = $pricePerEach;

        return $this;
    }

    /**
     * Get pricePerEach
     *
     * @return string 
     */
    public function getPricePerEach()
    {
        return $this->pricePerEach;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return OrderRow
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return OrderRow
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set priceModel
     *
     * @param string $priceModel
     * @return OrderRow
     */
    public function setPriceModel($priceModel)
    {
        $this->priceModel = $priceModel;

        return $this;
    }

    /**
     * Get priceModel
     *
     * @return string 
     */
    public function getPriceModel()
    {
        return $this->priceModel;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return OrderRow
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param \Entities\Article $article
     * @return OrderRow
     */
    public function setArticle(\Entities\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Entities\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set order
     *
     * @param \Entities\Order $order
     * @return OrderRow
     */
    public function setOrder(\Entities\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Entities\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
