<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Signup
 */
class Signup
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var boolean
     */
    private $isApproved;

    /**
     * @var boolean
     */
    private $isPaid;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\SignupSlot
     */
    private $signupSlot;

    /**
     * @var \Entities\Group
     */
    private $group;

    /**
     * @var \Entities\Person
     */
    private $person;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Signup
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Signup
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Signup
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Signup
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Signup
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     * @return Signup
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean 
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * Set isPaid
     *
     * @param boolean $isPaid
     * @return Signup
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return boolean 
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set signupSlot
     *
     * @param \Entities\SignupSlot $signupSlot
     * @return Signup
     */
    public function setSignupSlot(\Entities\SignupSlot $signupSlot = null)
    {
        $this->signupSlot = $signupSlot;

        return $this;
    }

    /**
     * Get signupSlot
     *
     * @return \Entities\SignupSlot 
     */
    public function getSignupSlot()
    {
        return $this->signupSlot;
    }

    /**
     * Set group
     *
     * @param \Entities\Group $group
     * @return Signup
     */
    public function setGroup(\Entities\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Entities\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set person
     *
     * @param \Entities\Person $person
     * @return Signup
     */
    public function setPerson(\Entities\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Entities\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}
