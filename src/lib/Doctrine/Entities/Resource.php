<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resource
 */
class Resource
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var boolean
     */
    private $available;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\ResourceType
     */
    private $resourceType;


    /**
     * Set name
     *
     * @param string $name
     * @return Resource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Resource
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Resource
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Resource
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Resource
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Resource
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set available
     *
     * @param boolean $available
     * @return Resource
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return boolean 
     */
    public function getAvailable()
    {
        return $this->available == null ? false : $this->available;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resourceType
     *
     * @param \Entities\ResourceType $resourceType
     * @return Resource
     */
    public function setResourceType(\Entities\ResourceType $resourceType = null)
    {
        $this->resourceType = $resourceType;

        return $this;
    }

    /**
     * Get resourceType
     *
     * @return \Entities\ResourceType 
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }
    /**
     * @var \Entities\SleepingResource
     */
    private $sleepingResource;


    /**
     * Set sleepingResource
     *
     * @param \Entities\SleepingResource $sleepingResource
     * @return Resource
     */
    public function setSleepingResource(\Entities\SleepingResource $sleepingResource = null)
    {
        $this->sleepingResource = $sleepingResource;

        return $this;
    }

    /**
     * Get sleepingResource
     *
     * @return \Entities\SleepingResource 
     */
    public function getSleepingResource()
    {
        return $this->sleepingResource;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $resourceBookings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resourceBookings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add resourceBookings
     *
     * @param \Entities\Booking $resourceBookings
     * @return Resource
     */
    public function addResourceBooking(\Entities\Booking $resourceBookings)
    {
        $this->resourceBookings[] = $resourceBookings;

        return $this;
    }

    /**
     * Remove resourceBookings
     *
     * @param \Entities\Booking $resourceBookings
     */
    public function removeResourceBooking(\Entities\Booking $resourceBookings)
    {
        $this->resourceBookings->removeElement($resourceBookings);
    }

    /**
     * Get resourceBookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResourceBookings()
    {
        return $this->resourceBookings;
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     * @return Resource
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
