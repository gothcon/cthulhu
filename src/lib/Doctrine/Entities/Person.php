<?php

namespace Entities;
use UserAccessor;
use UserLevel;

/**
 * Person
 */
class Person
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var boolean
     */
    private $emailIsPublic;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $identification;

    /**
     * @var string
     */
    private $streetAddress;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $foreignAddress;

    /**
     * @var string
     */
    private $note;

    /**
     * @var string
     */
    private $internalNote;

    /**
     * @var integer
     */
    private $isOutdated;

    /**
     * @var integer
     */
    private $xp;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return Person
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set emailIsPublic
     *
     * @param boolean $emailIsPublic
     * @return Person
     */
    public function setEmailIsPublic($emailIsPublic)
    {
        $this->emailIsPublic = $emailIsPublic;

        return $this;
    }

    /**
     * Get emailIsPublic
     *
     * @return boolean 
     */
    public function getEmailIsPublic()
    {
        return $this->emailIsPublic;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Person
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Person
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Person
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
  
    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Person
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Person
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Person
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set identification
     *
     * @param string $identification
     * @return Person
     */
    public function setIdentification($identification)
    {
		$identification = str_replace(array(" ","-"),"",$identification);
		if(strlen($identification)){
			if(!preg_match("/^[0-9]{6}[0-9]{4}$/",$identification)){
				throw new \Exception("Incorrect format");
			}
		}
		
        $this->identification = $identification;

        return $this;
    }

    /**
     * Get identification
     *
     * @return string 
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Set streetAddress
     *
     * @param string $streetAddress
     * @return Person
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;

        return $this;
    }

    /**
     * Get streetAddress
     *
     * @return string 
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Person
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Person
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Person
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set foreignAddress
     *
     * @param string $foreignAddress
     * @return Person
     */
    public function setForeignAddress($foreignAddress)
    {
        $this->foreignAddress = $foreignAddress;

        return $this;
    }

    /**
     * Get foreignAddress
     *
     * @return string 
     */
    public function getForeignAddress()
    {
        return $this->foreignAddress;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Person
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set internalNote
     *
     * @param string $internalNote
     * @return Person
     */
    public function setInternalNote($internalNote)
    {
        if(UserAccessor::currentUserHasAccess(UserLevel::STAFF))
            $this->internalNote = $internalNote;
        else
            throw new Exception("Cannot set internal note, user has no access");
        return $this;
    }

    /**
     * Get internalNote
     *
     * @return string 
     */
    public function getInternalNote()
    {
        if(UserAccessor::currentUserHasAccess(UserLevel::STAFF))
            return $this->internalNote;
        else
            return "Ingen åtkomst";
    }

    /**
     * Set isOutdated
     *
     * @param integer $isOutdated
     * @return Person
     */
    public function setIsOutdated($isOutdated)
    {
        $this->isOutdated = $isOutdated;

        return $this;
    }

    /**
     * Get isOutdated
     *
     * @return integer 
     */
    public function getIsOutdated()
    {
        return $this->isOutdated;
    }

    /**
     * Set xp
     *
     * @param integer $xp
     * @return Person
     */
    public function setXp($xp)
    {
        $this->xp = $xp;

        return $this;
    }

    /**
     * Get xp
     *
     * @return integer 
     */
    public function getXp()
    {
        return $this->xp;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Entities\User $user
     * @return Person
     */
    public function setUser(\Entities\User $user = null)
    {
        $this->user = $user;
        if($user != null)
            $user->setPerson($this);
        return $this;
    }

    /**
     * Get user
     *
     * @return \Entities\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add signups
     *
     * @param \Entities\Signup $signups
     * @return Person
     */
    public function addSignup(\Entities\Signup $signups)
    {
        $this->signups[] = $signups;

        return $this;
    }

    /**
     * Remove signups
     *
     * @param \Entities\Signup $signups
     */
    public function removeSignup(\Entities\Signup $signups)
    {
        $this->signups->removeElement($signups);
    }

    /**
     * Get signups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSignups()
    {
        return $this->signups;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupMemberships;


    /**
     * Add groupMemberships
     *
     * @param \Entities\Signup $groupMemberships
     * @return Person
     */
    public function addGroupMembership(\Entities\Signup $groupMemberships)
    {
        $this->groupMemberships[] = $groupMemberships;

        return $this;
    }

    /**
     * Remove groupMemberships
     *
     * @param \Entities\Signup $groupMemberships
     */
    public function removeGroupMembership(\Entities\Signup $groupMemberships)
    {
        $this->groupMemberships->removeElement($groupMemberships);
    }

    /**
     * Get groupMemberships
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupMemberships()
    {
        return $this->groupMemberships;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
     public function onPreUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $eventArgs)
    {
		$person = $eventArgs->getEntity();

		if($eventArgs->hasChangedField("identification")){
			$personIdentification = $person->getIdentification();
			if($personIdentification != ""){
				$person = \Repository\PersonRepository::personByIdentification($personIdentification);
				if($person != null){
					$person = $person[0];
					throw new \Exception("Personnummret är registrerat på ". $person->getId() .". " . $person->getFirstName(). " " .$person->getLastName());
				}
			}
        }
		
		if($eventArgs->hasChangedField("emailAddress") && strlen($person->getEmailAddress())){
			$tempPerson = \Repository\PersonRepository::personByEmailAddress($person->getEmailAddress());
			
			if($tempPerson != null && $person->getId() != $tempPerson->getId()){
				throw new \Exception("Epostadressen är redan registrerat på ". $tempPerson->getId() .". " . $tempPerson->getFirstName(). " " .$tempPerson->getLastName());
			}
		}
        
        if($eventArgs->hasChangedField("level") || $eventArgs->hasChangedField("username")){
            if(! \UserAccessor::currentUserHasAccess(\UserLevel::STAFF)){
                throw new Exception("User is not authorized to update level or username");
            }
        }
        
    }
    /**
     * @var \Entities\SleepingSlotBooking
     */
    private $sleepingSlotBooking;


    /**
     * Set sleepingSlotBooking
     *
     * @param \Entities\SleepingSlotBooking $sleepingSlotBooking
     * @return Person
     */
    public function setSleepingSlotBooking($sleepingSlotBooking = null)
    {
        $this->sleepingSlotBooking = $sleepingSlotBooking;

        return $this;
    }

    /**
     * Get sleepingSlotBooking
     *
     * @return \Entities\SleepingSlotBooking 
     */
    public function getSleepingSlotBooking()
    {
        return $this->sleepingSlotBooking;
    }
	
	public function __toString(){
		// die(print_r($this));
		return json_encode($this);
	}
	
}
