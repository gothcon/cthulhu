<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsArticle
 */
class NewsArticle
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $preamble;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $publishAt;

    /**
     * @var \DateTime
     */
    private $unpublishAt;

    /**
     * @var string
     */
    private $author;

    /**
     * @var boolean
     */
    private $isInternal;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set title
     *
     * @param string $title
     * @return NewsArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set preamble
     *
     * @param string $preamble
     * @return NewsArticle
     */
    public function setPreamble($preamble)
    {
        $this->preamble = $preamble;

        return $this;
    }

    /**
     * Get preamble
     *
     * @return string 
     */
    public function getPreamble()
    {
        return $this->preamble;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return NewsArticle
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set publishAt
     *
     * @param \DateTime $publishAt
     * @return NewsArticle
     */
    public function setPublishAt($publishAt)
    {
        $this->publishAt = $publishAt;

        return $this;
    }

    /**
     * Get publishAt
     *
     * @return \DateTime 
     */
    public function getPublishAt()
    {
        return $this->publishAt;
    }

    /**
     * Set unpublishAt
     *
     * @param \DateTime $unpublishAt
     * @return NewsArticle
     */
    public function setUnpublishAt($unpublishAt)
    {
        $this->unpublishAt = $unpublishAt;

        return $this;
    }

    /**
     * Get unpublishAt
     *
     * @return \DateTime 
     */
    public function getUnpublishAt()
    {
        return $this->unpublishAt;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return NewsArticle
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set isInternal
     *
     * @param boolean $isInternal
     * @return NewsArticle
     */
    public function setIsInternal($isInternal)
    {
        $this->isInternal = $isInternal;

        return $this;
    }

    /**
     * Get isInternal
     *
     * @return boolean 
     */
    public function getIsInternal()
    {
        return $this->isInternal;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return NewsArticle
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return NewsArticle
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return NewsArticle
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return NewsArticle
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
