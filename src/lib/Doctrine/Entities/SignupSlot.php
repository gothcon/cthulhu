<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignupSlot
 */
class SignupSlot
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $maximumSignupCount;

    /**
     * @var integer
     */
    private $maximumSpareSignupCount;

    /**
     * @var boolean
     */
    private $requiresApproval;

    /**
     * @var integer
     */
    private $isHidden;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signups;

    /**
     * @var \Entities\EventOccation
     */
    private $eventOccation;

    /**
     * @var \Entities\SlotType
     */
    private $slotType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SignupSlot
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return SignupSlot
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return SignupSlot
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return SignupSlot
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return SignupSlot
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set maximumSignupCount
     *
     * @param integer $maximumSignupCount
     * @return SignupSlot
     */
    public function setMaximumSignupCount($maximumSignupCount)
    {
		if($maximumSignupCount == ""){
			$maximumSignupCount = null;
		}
        $this->maximumSignupCount = $maximumSignupCount;

        return $this;
    }

    /**
     * Get maximumSignupCount
     *
     * @return integer 
     */
    public function getMaximumSignupCount()
    {
        return $this->maximumSignupCount;
    }

    /**
     * Set maximumSpareSignupCount
     *
     * @param integer $maximumSpareSignupCount
     * @return SignupSlot
     */
    public function setMaximumSpareSignupCount($maximumSpareSignupCount)
    {
		if($maximumSpareSignupCount == ""){
			$maximumSpareSignupCount = null;
		}
        $this->maximumSpareSignupCount = $maximumSpareSignupCount;

        return $this;
    }

    /**
     * Get maximumSpareSignupCount
     *
     * @return integer 
     */
    public function getMaximumSpareSignupCount()
    {
        return $this->maximumSpareSignupCount;
    }

    /**
     * Set requiresApproval
     *
     * @param boolean $requiresApproval
     * @return SignupSlot
     */
    public function setRequiresApproval($requiresApproval)
    {
        $this->requiresApproval = $requiresApproval;

        return $this;
    }

    /**
     * Get requiresApproval
     *
     * @return boolean 
     */
    public function getRequiresApproval()
    {
        return $this->requiresApproval;
    }

    /**
     * Set isHidden
     *
     * @param integer $isHidden
     * @return SignupSlot
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return integer 
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add signups
     *
     * @param \Entities\Signup $signups
     * @return SignupSlot
     */
    public function addSignup(\Entities\Signup $signups)
    {
        $this->signups[] = $signups;

        return $this;
    }

    /**
     * Remove signups
     *
     * @param \Entities\Signup $signups
     */
    public function removeSignup(\Entities\Signup $signups)
    {
        $this->signups->removeElement($signups);
    }

    /**
     * Get signups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSignups()
    {
        return $this->signups;
    }

    /**
     * Set eventOccation
     *
     * @param \Entities\EventOccation $eventOccation
     * @return SignupSlot
     */
    public function setEventOccation(\Entities\EventOccation $eventOccation = null)
    {
        $this->eventOccation = $eventOccation;

        return $this;
    }

    /**
     * Get eventOccation
     *
     * @return \Entities\EventOccation 
     */
    public function getEventOccation()
    {
        return $this->eventOccation;
    }

    /**
     * Set slotType
     *
     * @param \Entities\SlotType $slotType
     * @return SignupSlot
     */
    public function setSlotType(\Entities\SlotType $slotType = null)
    {
        $this->slotType = $slotType;

        return $this;
    }

    /**
     * Get slotType
     *
     * @return \Entities\SlotType 
     */
    public function getSlotType()
    {
        return $this->slotType;
    }
    /**
     * @var \Entities\SignupSlotArticle
     */
    private $signupSlotArticle;


    /**
     * Set signupSlotArticle
     *
     * @param \Entities\SignupSlotArticle $signupSlotArticle
     * @return SignupSlot
     */
    public function setSignupSlotArticle(\Entities\SignupSlotArticle $signupSlotArticle = null)
    {
        $this->signupSlotArticle = $signupSlotArticle;

        return $this;
    }

    /**
     * Get signupSlotArticle
     *
     * @return \Entities\SignupSlotArticle 
     */
    public function getSignupSlotArticle()
    {
        return $this->signupSlotArticle;
    }
}
