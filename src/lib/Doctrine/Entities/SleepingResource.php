<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * SleepingResource
 */
class SleepingResource
{
    /**
     * @var boolean
     */
    private $isAvailable;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Resource
     */
    private $resource;

    /**
     * Set isAvailable
     *
     * @param boolean $isAvailable
     * @return SleepingResource
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * Get isAvailable
     *
     * @return boolean 
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SleepingResource
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return SleepingResource
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return SleepingResource
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return SleepingResource
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return SleepingResource
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resource
     *
     * @param \Entities\Resource $resource
     * @return SleepingResource
     */
    public function setResource(\Entities\Resource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \Entities\Resource 
     */
    public function getResource()
    {
        return $this->resource;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sleepingSlotBookings;

    /**
     * Add sleepingSlotBookings
     *
     * @param \Entities\SleepingSlotBooking $sleepingSlotBookings
     * @return SleepingResource
     */
    public function addSleepingSlotBooking(\Entities\SleepingSlotBooking $sleepingSlotBookings)
    {
        $this->sleepingSlotBookings[] = $sleepingSlotBookings;

        return $this;
    }

    /**
     * Remove sleepingSlotBookings
     *
     * @param \Entities\SleepingSlotBooking $sleepingSlotBookings
     */
    public function removeSleepingSlotBooking(\Entities\SleepingSlotBooking $sleepingSlotBookings)
    {
        $this->sleepingSlotBookings->removeElement($sleepingSlotBookings);
    }

    /**
     * Get sleepingSlotBookings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSleepingSlotBookings()
    {
        return $this->sleepingSlotBookings;
    }
    /**
     * @var integer
     */
    private $noOfSlots;


    /**
     * Set noOfSlots
     *
     * @param integer $noOfSlots
     * @return SleepingResource
     */
    public function setNoOfSlots($noOfSlots)
    {
        $this->noOfSlots = $noOfSlots;

        return $this;
    }

    /**
     * Get noOfSlots
     *
     * @return integer 
     */
    public function getNoOfSlots()
    {
        return $this->noOfSlots;
    }
    /**
     * @var string
     */
    private $availableEventOccations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sleepingSlotBookings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set availableEventOccations
     *
     * @param string $availableEventOccations
     * @return SleepingResource
     */
    public function setAvailableEventOccations($availableEventOccations)
    {
        $this->availableEventOccations = $availableEventOccations;

        return $this;
    }

    /**
     * Get availableEventOccations
     *
     * @return string 
     */
    public function getAvailableEventOccations()
    {
        return $this->availableEventOccations;
    }
}
