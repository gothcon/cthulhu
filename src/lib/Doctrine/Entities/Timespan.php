<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timespan
 */
class Timespan
{
    /**
     * @var \DateTime
     */
    private $startsAt;

    /**
     * @var \DateTime
     */
    private $endsAt;

    /**
     * @var boolean
     */
    private $isPublic;

    /**
     * @var boolean
     */
    private $showInSchedule;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set startsAt
     *
     * @param \DateTime $startsAt
     * @return Timespan
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    /**
     * Get startsAt
     *
     * @return \DateTime 
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * Set endsAt
     *
     * @param \DateTime $endsAt
     * @return Timespan
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    /**
     * Get endsAt
     *
     * @return \DateTime 
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     * @return Timespan
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }
	
	 /**
     *  Get isCustom
     *
     * @return boolean 
     */
    public function isCustom()
    {
        return !$this->isPublic;
    }

    /**
     * Set showInSchedule
     *
     * @param boolean $showInSchedule
     * @return Timespan
     */
    public function setShowInSchedule($showInSchedule)
    {
        $this->showInSchedule = $showInSchedule;

        return $this;
    }

    /**
     * Get showInSchedule
     *
     * @return boolean 
     */
    public function getShowInSchedule()
    {
        return $this->showInSchedule;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Timespan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Timespan
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Timespan
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Timespan
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Timespan
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Timespan
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventOccation;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->eventOccation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add eventOccation
     *
     * @param \Entities\EventOccation $eventOccation
     * @return Timespan
     */
    public function addEventOccation(\Entities\EventOccation $eventOccation)
    {
        $this->eventOccation[] = $eventOccation;

        return $this;
    }

    /**
     * Remove eventOccation
     *
     * @param \Entities\EventOccation $eventOccation
     */
    public function removeEventOccation(\Entities\EventOccation $eventOccation)
    {
        $this->eventOccation->removeElement($eventOccation);
    }

    /**
     * Get eventOccation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventOccation()
    {
        return $this->eventOccation;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventOccations;


    /**
     * Get eventOccations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEventOccations()
    {
        return $this->eventOccations;
    }
}
