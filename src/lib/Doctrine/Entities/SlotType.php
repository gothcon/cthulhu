<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * SlotType
 */
class SlotType
{
    /**
     * @var integer
     */
    private $cardinality;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signupSlots;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signupSlots = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set cardinality
     *
     * @param integer $cardinality
     * @return SlotType
     */
    public function setCardinality($cardinality)
    {
        $this->cardinality = $cardinality;

        return $this;
    }

    /**
     * Get cardinality
     *
     * @return integer 
     */
    public function getCardinality()
    {
        return $this->cardinality;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SlotType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SlotType
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return SlotType
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return SlotType
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return SlotType
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return SlotType
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add signupSlots
     *
     * @param \Entities\SignupSlot $signupSlots
     * @return SlotType
     */
    public function addSignupSlot(\Entities\SignupSlot $signupSlots)
    {
        $this->signupSlots[] = $signupSlots;

        return $this;
    }

    /**
     * Remove signupSlots
     *
     * @param \Entities\SignupSlot $signupSlots
     */
    public function removeSignupSlot(\Entities\SignupSlot $signupSlots)
    {
        $this->signupSlots->removeElement($signupSlots);
    }

    /**
     * Get signupSlots
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSignupSlots()
    {
        return $this->signupSlots;
    }
}
