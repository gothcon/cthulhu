<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $resetPasswordToken;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var integer
     */
    private $level;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \DateTime
     */
    private $mostRecentLogin;

    /**
     * @var string
     */
    private $oldPassword;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Person
     */
    private $person;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return User
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return User
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return User
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        if($username == ""){
        }
        
        if(\UserAccessor::currentUserHasAccess(\UserLevel::ADMIN)){
            $this->username = $username;
        }
        else if($this->username == ""){  
            $this->username = $username;
        }else{
            throw new Exception("User not authorized to set usernname", $code, $previous);
        }
        
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if($password != ""){
            $this->salt = static::createSalt();
            $this->password = static::encryptPassword($password,$this->salt); 
        }
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return "";
        // return $this->password;
    }

    /**
     * Set resetPasswordToken
     *
     * @param string $resetPasswordToken
     * @return User
     */
    public function setResetPasswordToken($resetPasswordToken)
    {
        $this->resetPasswordToken = $resetPasswordToken;
        return $this;
    }

    /**
     * Get resetPasswordToken
     *
     * @return string 
     */
    public function getResetPasswordToken()
    {
        return "";
        // return $this->resetPasswordToken;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    private function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return "";
        // return $this->salt;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return User
     */
    public function setLevel($level)
    {
        if(\UserAccessor::currentUserHasAccess(\UserLevel::ADMIN)){
            $this->level = $level;
        }
        else if(\UserAccessor::currentUserHasAccess(\UserLevel::STAFF) && $level < \UserLevel::STAFF){
            $this->level = $level;
        }
        else{
            return $this;
        }
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set mostRecentLogin
     *
     * @param \DateTime $mostRecentLogin
     * @return User
     */
    public function setMostRecentLogin($mostRecentLogin)
    {
        $this->mostRecentLogin = $mostRecentLogin;

        return $this;
    }

    /**
     * Get mostRecentLogin
     *
     * @return \DateTime 
     */
    public function getMostRecentLogin()
    {
        return $this->mostRecentLogin;
    }

    /**
     * Set oldPassword
     *
     * @param string $oldPassword
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get oldPassword
     *
     * @return string 
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \Entities\Person $person
     * @return User
     */
    public function setPerson(\Entities\Person $person = null)
    {
        $this->person = $person;
        return $this;
    }

    /**
     * Get person
     *
     * @return \Entities\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(\Doctrine\ORM\Event\PreUpdateEventArgs $eventArgs)
    {
        if($eventArgs->hasChangedField("password") || $eventArgs->hasChangedField("salt")){
            switch(true){
                case \UserAccessor::currentUserHasAccess(\UserLevel::STAFF) :
                case \UserAccessor::getCurrentUser()->id == $this->id :
                    // everything went fine
                    break;
                default:
                    // reset to the old values
                    $this->salt = $eventArgs->getOldValue("salt");
                    $this->password = $eventArgs->getOldValue("password");
                    break;
            }
        }
        
        if($eventArgs->hasChangedField("level") || $eventArgs->hasChangedField("username")){
            if(! \UserAccessor::currentUserHasAccess(\UserLevel::STAFF)){
                throw new Exception("User is not authorized to update level or username");
            }
        }
        // Add your code here
    }

    static public function generateRandomString($length = 12 ,$allchar = "abcdefghijkmnoprstuzABCDFKLMRSTUVWX1235690#!?"){
            mt_srand ((double) microtime() * 1000000);
            $allCharCount=strlen($allchar);
            $string = "";
            for($i = 0; $i < $length;$i++)
            {
                    $string .= $allchar[mt_rand(0,$allCharCount-1)];
            }
            return $string;
    }

    static public function createSalt(){
            return static::generateRandomString(12);
    }

    static public function encryptPassword($password,$salt){
            $hash = $salt . $password;
            for($i=0; $i < 1000; $i++){
                    $hash = md5($hash);
            }
            return $hash;
    }

}
