<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 */
class Order
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $dibsSecret;

    /**
     * @var string
     */
    private $preferredPaymentMethod;

    /**
     * @var string
     */
    private $total;

    /**
     * @var string
     */
    private $note;

    /**
     * @var \DateTime
     */
    private $submittedAt;

    /**
     * @var integer
     */
    private $hasBeenSubmitted;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Person
     */
    private $person;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Order
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Order
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Order
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Order
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dibsSecret
     *
     * @param string $dibsSecret
     * @return Order
     */
    public function setDibsSecret($dibsSecret)
    {
        $this->dibsSecret = $dibsSecret;

        return $this;
    }

    /**
     * Get dibsSecret
     *
     * @return string 
     */
    public function getDibsSecret()
    {
        return $this->dibsSecret;
    }

    /**
     * Set preferredPaymentMethod
     *
     * @param string $preferredPaymentMethod
     * @return Order
     */
    public function setPreferredPaymentMethod($preferredPaymentMethod)
    {
        $this->preferredPaymentMethod = $preferredPaymentMethod;

        return $this;
    }

    /**
     * Get preferredPaymentMethod
     *
     * @return string 
     */
    public function getPreferredPaymentMethod()
    {
        return $this->preferredPaymentMethod;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Order
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Order
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set submittedAt
     *
     * @param \DateTime $submittedAt
     * @return Order
     */
    public function setSubmittedAt($submittedAt)
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    /**
     * Get submittedAt
     *
     * @return \DateTime 
     */
    public function getSubmittedAt()
    {
        return $this->submittedAt;
    }

    /**
     * Set hasBeenSubmitted
     *
     * @param integer $hasBeenSubmitted
     * @return Order
     */
    public function setHasBeenSubmitted($hasBeenSubmitted)
    {
        $this->hasBeenSubmitted = $hasBeenSubmitted;

        return $this;
    }

    /**
     * Get hasBeenSubmitted
     *
     * @return integer 
     */
    public function getHasBeenSubmitted()
    {
        return $this->hasBeenSubmitted;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \Entities\Person $person
     * @return Order
     */
    public function setPerson(\Entities\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Entities\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}
