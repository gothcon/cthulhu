<?php

namespace Entities;
use UserAccessor;
use UserLevel;
use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 */
class Group
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $note;

    /**
     * @var string
     */
    private $internalNote;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $signups;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupMembers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->signups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupMembers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return Group
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Group
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Group
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Group
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Group
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Group
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return Group
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Group
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Group
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set internalNote
     *
     * @param string $internalNote
     * @return Group
     */
    public function setInternalNote($internalNote)
    {
		if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
	        $this->internalNote = $internalNote;
		}
        else{
			throw new Exception("Could not set internal note, Access denied");
		}
        return $this;
    }

    /**
     * Get internalNote
     *
     * @return string 
     */
    public function getInternalNote()
    {
		 if(UserAccessor::currentUserHasAccess(UserLevel::STAFF))
            return $this->internalNote;
        else
            return null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add signups
     *
     * @param \Entities\Signup $signups
     * @return Group
     */
    public function addSignup(\Entities\Signup $signups)
    {
        $this->signups[] = $signups;

        return $this;
    }

    /**
     * Remove signups
     *
     * @param \Entities\Signup $signups
     */
    public function removeSignup(\Entities\Signup $signups)
    {
        $this->signups->removeElement($signups);
    }

    /**
     * Get signups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSignups()
    {
        return $this->signups;
    }

    /**
     * Add groupMembers
     *
     * @param \Entities\GroupMembership $groupMembers
     * @return Group
     */
    public function addGroupMember(\Entities\GroupMembership $groupMembers)
    {
        $this->groupMembers[] = $groupMembers;

        return $this;
    }

    /**
     * Remove groupMembers
     *
     * @param \Entities\GroupMembership $groupMembers
     */
    public function removeGroupMember(\Entities\GroupMembership $groupMembers)
    {
        $this->groupMembers->removeElement($groupMembers);
    }

    /**
     * Get groupMembers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupMembers()
    {
        return $this->groupMembers;
    }
//    /**
//     * @var \Entities\GroupMembership
//     */
//    private $leader_membership;
//
//
//    /**
//     * Set leader_membership
//     *
//     * @param \Entities\GroupMembership $leaderMembership
//     * @return Group
//     */
//    public function setLeaderMembership(\Entities\GroupMembership $leaderMembership = null)
//    {
//        $this->leader_membership = $leaderMembership;
//
//        return $this;
//    }
//
//    /**
//     * Get leader_membership
//     *
//     * @return \Entities\GroupMembership 
//     */
//    public function getLeaderMembership()
//    {
//        return $this->leader_membership;
//    }
//    /**
//     * @var int
//     */

	private $leaderMembershipId;


    /**
     * Set leaderMembershipId
     *
     * @param int $leaderMembershipId
     * @return Group
     */
    public function setLeaderMembershipId($leaderMembershipId)
    {
        $this->leaderMembershipId = $leaderMembershipId;

        return $this;
    }

    /**
     * Get leaderMembershipId
     *
     * @return int
     */
    public function getLeaderMembershipId()
    {
        return $this->leaderMembershipId;
    }
}
