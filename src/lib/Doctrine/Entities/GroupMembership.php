<?php

namespace Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupMembership
 */
class GroupMembership
{
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $modifiedAt;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isDeleted;

    /**
     * @var boolean
     */
    private $isLeader;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Entities\Person
     */
    private $person;

    /**
     * @var \Entities\Group
     */
    private $group;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GroupMembership
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return GroupMembership
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return GroupMembership
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     * @return GroupMembership
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string 
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return GroupMembership
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isLeader
     *
     * @param boolean $isLeader
     * @return GroupMembership
     */
    public function setIsLeader($isLeader)
    {
        $this->isLeader = $isLeader;

        return $this;
    }

    /**
     * Get isLeader
     *
     * @return boolean 
     */
    public function getIsLeader()
    {
        return $this->isLeader;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \Entities\Person $person
     * @return GroupMembership
     */
    public function setPerson(\Entities\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Entities\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set group
     *
     * @param \Entities\Group $group
     * @return GroupMembership
     */
    public function setGroup(\Entities\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Entities\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }
}
