<?php

setlocale(LC_ALL, "swedish");
date_default_timezone_set("Europe/Stockholm");

require_once __DIR__ . "/../vendor/autoload.php";



use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
$config = Setup::createXMLMetadataConfiguration(
       // Setup::createAnnotationMetadataConfiguration(
               array(
                       __DIR__ .'/Metadata'
               ), 
               false
       );

// $config->setEntityNamespaces(array('' => "Entities\\"));
$config->setProxyDir(__DIR__ . "/Proxies");
$config->setProxyNamespace("Proxies");

$entityManager = EntityManager::create(
	array(
		'driver'   => 'pdo_mysql',
		'user'     => 'root',
		'password' => 'elcSmxKU',
		'dbname'   => 'gc2012',
	), 
	$config
);
$entityManager->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');


return new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($entityManager->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager, array(
		__DIR__ .'/Metadata/People.dcm.xml'
	),null,null,false)
));

?>