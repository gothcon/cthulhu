<?php
//            $c = new \Doctrine\Common\Cache\WinCacheCache();
//
//            print_r($c->getStats());
//die();
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GothConEntityManager
 *
 * @author Joakim
 */
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Event\Listeners\MysqlSessionInit;

class GothConEntityEventSubscriber implements \Doctrine\Common\EventSubscriber{
    
    public function prePersist(Doctrine\ORM\Event\LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        $rc = new ReflectionClass(get_class($entity));
        if($rc->hasMethod("setCreatedAt")){
            $entity->setCreatedAt(new DateTime());
        }
        if($rc->hasMethod("setCreatedBy")){
            $entity->setCreatedBy(UserAccessor::getCurrentUser()->username);
        }
        if($rc->hasMethod("setIsDeleted")){
            $entity->setIsDeleted(0);
        }
    }
    
    public function preUpdate(Doctrine\ORM\Event\LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        $rc = new ReflectionClass(get_class($entity));
        if($rc->hasMethod("setModifiedAt")){
            $entity->setModifiedAt(new DateTime());
        }
        if($rc->hasMethod("setModifiedBy")){
            $entity->setModifiedBy(UserAccessor::getCurrentUser()->username);
        }
    }
    
    public function getSubscribedEvents() {
        return array('prePersist', 'preUpdate');
    }
}

class GothConEntityManager{
    
    /**
     *
     * @var Doctrine\ORM\EntityManager
     */
    private static $_instance = null;
    
	/**
	 * 
	 * @param int $personId
	 * @return \Entities\Person
	 */
	public static function createPersonalReference($personId){
		return static::getInstance()->getReference("\\Entities\\Person", $personId);
	}

	/**
	 * 
	 * @param int $eventId
	 * @return \Entities\Event
	 */
	public static function createEventReference($eventId){
		return static::getInstance()->getReference("\\Entities\\Event", $eventId);
	}

	/**
	 * 
	 * @param int $eventOccationId
	 * @return \Entities\Event
	 */
	public static function createEventOccationReference($eventOccationId){
		return static::getInstance()->getReference("\\Entities\\EventOccation", $eventOccationId);
	}
	
	/**
	 * 
	 * @param int $groupId
	 * @return \Entities\Group
	 */
	public static function createGroupReference($groupId){
		return static::getInstance()->getReference("\\Entities\\Group", $groupId);
	}
	
	/**
	 * 
	 * @param int $eventTypeId
	 * @return \Entities\EventType
	 */
	public static function createEventTypeReference($eventTypeId){
		return static::getInstance()->getReference("\\Entities\\EventType", $eventTypeId);
	}
	
	/**
	 * 
	 * @param int $personId
	 * @return \Entities\Resource
	 */
	public static function createResourceReference($resourceId){
		return static::getInstance()->getReference("\\Entities\\Resource", $resourceId);
	}
	
	/**
	 * 
	 * @param int $id
	 * @return \Entities\SlotType
	 */
	public static function createSlotTypeReference($id){
		return static::getInstance()->getReference("\\Entities\\SlotType", $id);
	}
	/**
	 * 
	 * @param int $id
	 * @return \Entities\SignupSlot
	 */
	public static function createSignupSlotReference($id){
		return static::getInstance()->getReference("\\Entities\\SignupSlot", $id);
	}
	
    /**
     * 
     * @return Doctrine\ORM\EntityManager
     */
    public static function getInstance(){
        if(null === static::$_instance){
            $paths = array(__DIR__ .'/Metadata');
            $isDevMode = true;
            // the connection configuration
            $dbParams = array(
                'host'     => Application::getConfigurationValue('dbHost'),
                'driver'   => 'pdo_mysql',
                'user'     => Application::getConfigurationValue('dbUsername'),
                'password' => Application::getConfigurationValue('dbPassword'),
                'dbname'   => Application::getConfigurationValue('dbDatabase'),
            );
            $config = Setup::createXMLMetadataConfiguration($paths, $isDevMode);
            // $config->setQueryCacheImpl(new \Doctrine\Common\Cache\WinCacheCache());
            // $config->setResultCacheImpl(new \Doctrine\Common\Cache\WinCacheCache());
            // $config->setMetadataCacheImpl(new \Doctrine\Common\Cache\WinCacheCache());
            // $config->setHydrationCacheImpl(new \Doctrine\Common\Cache\WinCacheCache());
            $config->setProxyDir(__DIR__ . "/Proxies");
            $config->setProxyNamespace("Proxies");
			$config->setAutoGenerateProxyClasses(false);
            static::$_instance = EntityManager::create($dbParams, $config);
            static::$_instance->getEventManager()->addEventSubscriber(new GothConEntityEventSubscriber());
			static::$_instance->getEventManager()->addEventSubscriber(new MysqlSessionInit('utf8', 'utf8_unicode_ci'));
        }
        return static::$_instance;
    }
}