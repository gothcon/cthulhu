<?php
namespace Repository;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
use \DateTime;
use \DateInterval;
class UserRepository extends GothConRepository {
	/**
	 * 
	 * @param \DateTime $mostRecentLoginLimit
	 * @return int
	 */
   static function GetUserLogonCount($mostRecentLoginLimit = null){
		if($mostRecentLoginLimit == null){
			$mostRecentLoginLimit = new DateTime();
			$mostRecentLoginLimit->sub(new DateInterval('P7D'));
		}
		$em = \GothConEntityManager::getInstance();
		
		return $em->createQuery("SELECT count(u) FROM Entities\\User u WHERE u.mostRecentLogin >= :mostRecentLoginLimit")
			->setParameter("mostRecentLoginLimit", $mostRecentLoginLimit)
			->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
   }
}
