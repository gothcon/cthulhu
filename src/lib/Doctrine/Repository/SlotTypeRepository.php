<?php
namespace Repository;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventOccationRepository
 *
 * @author Joakim
 */
class SlotTypeRepository extends GothConRepository {
	public static function all(){
		
		$query = static::getGCEntityManager()->createQuery(""
			. "select partial st.{name,id, cardinality}, partial ss.{id}, partial s.{id}, partial g.{id, name}, partial p.{firstName, lastName,id} from Entities\\SlotType st "
			. "LEFT JOIN st.signupSlots ss "
			. "LEFT JOIN ss.signups s "
			. "LEFT JOIN s.group g "
			. "LEFT JOIN s.person p "
			. "");
		
		return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}	
	
}
