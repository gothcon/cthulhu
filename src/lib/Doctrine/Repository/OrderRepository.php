<?php
namespace Repository;
use Doctrine\ORM\EntityRepository;
use GothConEntityManager;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class OrderRepository extends \Repository\GothConRepository {
    
    /**
     * @author Joakim Ekeblad
     * @return Entities\Order[]
     */
	public static function getPersonalOrders($personId){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("SELECT partial o.{submittedAt,note,status,total,id} FROM Entities\\Order o WHERE o.person = :person")
			->setParameter("person", GothConEntityManager::createPersonalReference($personId))
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
    
}
