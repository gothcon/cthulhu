<?php
namespace Repository;
use GothConEntityManager;
require_once __DIR__ . "/../GothConEntityManager.php";
/**
 * Description of AccommodationResourceRepository
 *
 * @author Joakim
 */
class AccommodationResourceRepository extends \Repository\GothConRepository {
	//put your code here
	public static function getPersonalSleepingResorces($sleepingEventId,$personId,$includeUnavailable=false,$includeFull=false){

		$em = GothConEntityManager::getInstance();
		
		$sleepingResourcesStats = $em->createQuery(
			  "SELECT "
			. "r.id as resource_id, sr.noOfSlots, sr.noOfSlots - count(ssb.id) as availableSlots "
			. "FROM Entities\\SleepingResource sr "
			. "LEFT JOIN sr.sleepingSlotBookings ssb "
			. "LEFT JOIN sr.resource r "
			. "GROUP BY sr "
			)->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
		
		$indexedStatsArray = array();
		foreach($sleepingResourcesStats as $sleepingResourceStats){
			$indexedStatsArray[$sleepingResourceStats['resource_id']] = $sleepingResourceStats;
		}

		$res = $em->createQuery(
			"SELECT "
			. "partial e.{id,name}, partial eo.{id,name}, partial t.{startsAt,endsAt,name,id}, "
			. "partial eob.{id},partial b.{id,description}, partial r.{id,name}, partial sr.{id, noOfSlots} "
			. "FROM Entities\\sleepingResource sr "
			. "LEFT JOIN sr.resource r "
			. "LEFT JOIN r.resourceBookings b "
			. "LEFT JOIN b.eventOccationBooking eob "
			. "LEFT JOIN eob.eventOccation eo "
			. "LEFT JOIN eo.timespan t "
			. "LEFT JOIN eo.event e "
			. "WHERE e.id = :id "
			. "ORDER BY r.name, t.startsAt, t.endsAt")->setParameter("id", $sleepingEventId)->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

		$personalSleepingRes = $em->createQuery(""
			. "SELECT "
			. "partial ssb.{id}, "
			. "identity(ssb.sleepingResource) as resource_id "
			. "from Entities\\SleepingSlotBooking ssb "
			. "WHERE ssb.person = :person")
			->setParameter("person",  GothConEntityManager::createPersonalReference($personId))
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
		
		if(count($personalSleepingRes) > 0){
			$personalSleepingRes = $personalSleepingRes[0]['resource_id'];
		}else{
			$personalSleepingRes = -1;
		}
		
		foreach($res as &$sleepingResource){
			$resource = new \stdClass();
			$resource->name = $sleepingResource['resource']['name'];
			$resource->sleepingResourceId = $sleepingResource['id'];
			$resource->eventOccations = array();
			$resource->stats = $indexedStatsArray[$sleepingResource['resource']['id']];
			$resource->isSelected = ($personalSleepingRes == $sleepingResource['id']);
			foreach($sleepingResource['resource']['resourceBookings'] as $resourceBooking){
				array_push($resource->eventOccations,$resourceBooking['eventOccationBooking']['eventOccation']['timespan']);
			}
			$sleepingResource = $resource;
		}
		unset($sleepingResource);
		return $res;
	}
	
	/**
	 * Hmmmm 
	 * @param int $personId
	 *
	 * 	 
	 * @return SleepingSlotBooking
	 */
	public static function getPersonalAccommodation($personId){
		$em = GothConEntityManager::getInstance();
		$booking = $em->createQuery("SELECT ssb FROM Entities\\SleepingSlotBooking ssb WHERE ssb.person = :person")
					->setParameter("person", GothConEntityManager::createPersonalReference($personId))->getResult();
		if(count($booking)){
			return $booking[0];
		}
		else
		{
			return null;
		}
	}

	public static function savePersonalAccommodation($personId, $sleepingResourceId = 0){
		$booking = static::getPersonalAccommodation($personId);
		
		$em = GothConEntityManager::getInstance();
		
		if($sleepingResourceId <= 0 && $booking != null){
			static::delete($booking);
			return;
		}
		
		if($booking == null){
			$booking = new \Entities\SleepingSlotBooking();
			$booking->setPerson(GothConEntityManager::createPersonalReference($personId));
		}
		$booking->setSleepingResource($em->getReference ("Entities\\SleepingResource", $sleepingResourceId));
		static::save($booking);
		return $booking;
	}
	
	public static function addAccommodations($resourceId, array $peopleIds){
		$em = static::getGCEntityManager();
		$sleepingResources = $em->createQuery(""
			. "SELECT r,sr FROM Entities\\Resource r "
			. "LEFT JOIN r.sleepingResource sr "
			. "WHERE r.id = :resourceId ")
			->setParameter("resourceId",$resourceId)
			->getResult();
		
		$resource = $sleepingResources[0];
		
		$currentBookings = $em->createQuery(""
			. "SELECT p, ssb FROM Entities\\Person p INDEX BY p "
			. "INNER JOIN p.sleepingSlotBooking ssb "
			. "LEFT JOIN ssb.sleepingResource sr "
			. "WHERE p.id IN (:peopleIds)")
			->setParameter("peopleIds",$peopleIds)
			->getResult();
		
		$bookingsToSave = array();
		
		foreach($peopleIds as $key => $personId){
			if(isset($currentBookings[$personId])){
				$currentBookings[$personId]
					->getSleepingSlotBooking()
					->setSleepingResource($resource->getSleepingResource());
			}else{
				$booking = new \Entities\SleepingSlotBooking();
				$booking->setPerson(GothConEntityManager::createPersonalReference($personId));
				$booking->setSleepingResource($resource->getSleepingResource());
				$em->persist($booking);
			}
		}
		
		$em->flush();
		
		return $em->createQuery(""
			. "SELECT partial p.{id, firstName,lastName}, partial ssb.{id} FROM Entities\\SleepingSlotBooking ssb  "
			. "LEFT JOIN ssb.person p "
			. "WHERE ssb.sleepingResource = :sleepingResource ")
			->setParameter("sleepingResource",$resource->getSleepingResource())
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
		
	}
	
	public static function delete($entityOrId){
		if(is_numeric($entityOrId)){
			$entityOrId = static::createReference($entityOrId);
		}
		return parent::delete($entityOrId);
	}
	
	public static function createReference($sleepingSlotBookingId){
		return \GothConEntityManager::getInstance()->getReference("Entities\\SleepingSlotBooking", $sleepingSlotBookingId);
	}
	
	
}
