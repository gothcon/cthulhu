<?php
namespace Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use GothConEntityManager;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class EventTypeRepository extends GothConRepository {
	public static function getAll(){
		$em = GothConEntityManager::getInstance();
		return $em->createQuery("SELECT et from \Entities\EventType et ORDER BY et.name")
		->getResult(Query::HYDRATE_ARRAY);
	}	
	public static function get($id){
		$em = GothConEntityManager::getInstance();
		return $em->createQuery("SELECT et from \Entities\EventType et WHERE et.id = :id")
		->setParameter("id",$id)
		->getResult(Query::HYDRATE_OBJECT)[0];
	}	
}
