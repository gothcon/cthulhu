<?php
namespace Repository;
use GothConEntityManager;
use Doctrine\ORM\Query;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class ResourceRepository extends GothConRepository {
    
    /**
     * @author Joakim Ekeblad
     * @return Entities\Resource[]
     */
	public static function getSleepingOccations($sleepingEventId,$resourceId){
		$em = \GothConEntityManager::getInstance();
		$sleepingEventOccations = $em->createQuery("select partial eo.{id,name}, partial t.{startsAt,endsAt,id,name} "
			. "FROM entities\\eventOccation eo INDEX BY eo.id "
			. "LEFT JOIN eo.timespan t "
			. "WHERE eo.event = :event ")
			->setParameter("event", GothConEntityManager::createEventReference($sleepingEventId))
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
		
		$bookedSleepingOccations = $em->createQuery(""
			. "SELECT identity(eob.eventOccation) as eventOccationId " 
			. "FROM entities\\Booking b "
			. "LEFT JOIN b.eventOccationBooking eob "
			. "WHERE eob.eventOccation in (:sleepingEventOccations) and b.resource = :resource ")
			->setParameter("sleepingEventOccations", array_keys( $sleepingEventOccations) )
			->setParameter("resource" , \GothConEntityManager::createResourceReference($resourceId))
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
		
		foreach($bookedSleepingOccations as $bookedSleepingOccation){
			$eventOccationId = $bookedSleepingOccation['eventOccationId'];
			$sleepingEventOccations[$eventOccationId]['selected'] = true;
		}
		
		return $sleepingEventOccations;
		
	}
	
	public static function delete($entityOrId){
		if(is_numeric($entityOrId)){
			$entityOrId = static::createReference($entityOrId);
		}
		return parent::delete($entityOrId);
	}
	
	public static function get($id){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("Select r, rt from Entities\\Resource r LEFT JOIN r.resourceType rt "
			. "WHERE r.id = :id")
			->setParameter("id" , $id)->getResult()[0];
	}
	
	    /**
     * @param int $id
     * @return Entities\Resource
     */
    public static function resource($id = null){
        $em = GothConEntityManager::getInstance();
        if($id == null){
            $resource = new \Entities\Resource();
            $em->persist($resource);
        }else{
            $resource = static::get($id);
        }
        return $resource;
    }
	
	public static function all(){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery(""
			. "SELECT partial r.{id,name,available}, "
			. "partial rt.{id,name}, "
			. "partial sr.{id,noOfSlots,isAvailable}, "
			. "count(ssb.id) as sleepingSlotBookings "
			. "FROM Entities\\Resource r "
			. "LEFT JOIN r.resourceType rt "
			. "LEFT JOIN r.sleepingResource sr "
			. "LEFT JOIN sr.sleepingSlotBookings ssb "
			. "GROUP BY sr.id")
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
	
	public static function types(){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery(""
			. "SELECT partial rt.{id,name} "
			. "FROM Entities\\ResourceType rt ")->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
	
	/**
	 * @param int $sleepingEventId
	 * @return Entities\\EventOccation[]
	 */
	public static function sleepingOccations($sleepingEventId){
		$em = \GothConEntityManager::getInstance();
		$event = $em->getReference("Entities\\Event", $sleepingEventId);
		
		return $em->createQuery(""
			. "SELECT partial t.{id,startsAt,endsAt,name}, partial eo.{id,name} "
			. "FROM Entities\\EventOccation eo "
			. "LEFT JOIN eo.timespan t "
			. "WHERE eo.event = :e ")
			->setParameters(array('e' => $event))
			->getResult();
	}
	
	private static function array_unique_elements ($array1, $array2)
	{
	  array_merge(array_diff_key($array1, $array2), array_diff_key($array2, $array1));
	}
	
	public static function setSleepingOccations($sleepingEventId, $resourceId, $newSelectionData){
		$currentSelectionData = static::getSleepingOccations($sleepingEventId, $resourceId);
		
		$uniqueElements = static::array_unique_elements((array)$newSelectionData, (array)$currentSelectionData);
		
		if(count($uniqueElements) > 0){
			throw new Exception("Möjliga sovtillfällen har ändrats, var vänlig ladda om sidan");
		}
		
		foreach($newSelectionData as $eventOccationId => $eventOccation){
		
			$currentlySelected = isset($currentSelectionData[$eventOccationId]['selected']) && $currentSelectionData[$eventOccationId]['selected'];
			$updatedSelection = isset($eventOccation->selected) && $eventOccation->selected;
			if($currentlySelected != $updatedSelection){
				if($updatedSelection){
					// add event occation booking selection
					$eventOccationBooking = new \Entities\EventOccationBooking();
					$eventOccationBooking->setResource(static::createReference($resourceId));
					$eventOccationBooking->setEventOccation(EventOccationRepository::getEventOccation($eventOccationId));
					static::getGCEntityManager()->persist($eventOccationBooking);
				}
				else{
					// delete event occation booking
					$booking = EventOccationRepository::getEventOccationResourceBooking($resourceId, $eventOccationId);
					static::getGCEntityManager()->remove($booking);
				}
			}
		}
		static::getGCEntityManager()->flush();
	}
	
	public static function getResourceType($resourceTypeId){
		
		$em = \GothConEntityManager::getInstance();
		
		return $em->createQuery(""
			. "SELECT rt "
			. "FROM Entities\\ResourceType rt "
			. "WHERE rt.id = :id")
			->setParameters(array('id' => $resourceTypeId))
			->getResult()[0];
	}
	
	public static function getEventOccationBookings($resourceId){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("SELECT "
			. "partial b.{id}, "
			. "partial eob.{id}, "
			. "partial eo.{id,name},  "
			. "partial e.{id,name} , "
			. "partial t.{id,startsAt,endsAt} "
			. "FROM Entities\\Booking b "
			. "LEFT JOIN b.eventOccationBooking eob "
			. "LEFT JOIN eob.eventOccation eo "
			. "LEFT JOIN eo.event e "
			. "LEFT JOIN eo.timespan t "
			. "WHERE b.resource = :resource AND eo.id != 'NULL'"
			. "ORDER BY t.startsAt ASC, t.endsAt ASC")
			->setParameter("resource", static::createReference($resourceId))
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
	
	public static function getBookingsSchedule(){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("SELECT "
			. "partial r.{id, name}, "
			. "partial rt.{id,name}, "
			. "partial b.{id}, "
			. "partial eob.{id}, "
			. "partial eo.{id,name},  "
			. "partial e.{id,name} , "
			. "partial t.{id,startsAt,endsAt} "
			. "FROM Entities\\Booking b "
			. "LEFT JOIN b.resource r "
			. "LEFT JOIN r.resourceType rt "
			. "LEFT JOIN b.eventOccationBooking eob "
			. "LEFT JOIN eob.eventOccation eo "
			. "LEFT JOIN eo.event e "
			. "LEFT JOIN eo.timespan t "
			. "WHERE eo.id != 'NULL'"
			. "ORDER BY t.startsAt ASC, t.endsAt ASC")
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
	
	public static function createReference($resourceId){
		return \GothConEntityManager::getInstance()->getReference("Entities\\Resource", $resourceId);
	}
	
	public static function getScheduleData(){
		$conn = GothConEntityManager::getInstance()->getConnection();
		$query = "SELECT 
					b.id AS bookingId,
					r.name AS resource,
					r.id AS resourceId,
					rt.name AS resourceType,
					e.name AS event,
					e.id AS eventId,
					eo.name AS eventOccation,
					t.starts_at AS startsAt,
					t.ends_at AS endsAt
				FROM resources r
				LEFT JOIN resource_types rt ON rt.id = r.resource_type_id
				LEFT JOIN bookings b ON b.resource_id = r.id
				LEFT JOIN event_occation_bookings eob ON eob.booking_id = b.id
				LEFT JOIN event_occations eo ON eo.id = eob.event_occation_id
				LEFT JOIN timespans t ON t.id = eo.timespan_id
				LEFT JOIN EVENTS e ON e.id = eo.event_id
				ORDER BY resource ASC, event ASC, startsAt, endsAt";
		$resultSet = $conn->executeQuery($query);
		
		$resources = array();
		$currentResource = null;
		$currentEvent = null;

		while($row = $resultSet->fetch(\PDO::FETCH_ASSOC)){

			if(!is_null($currentResource) && $currentResource['id'] != $row["resourceId"]){
				unset($currentResource);
				$currentResource = null;
			}

			if($currentResource == null){
				$resources[] = array(
					'name' => $row['resource'],
					'id' => $row['resourceId'],
					'events' => array()
				);
				$currentResource = &$resources[count($resources)-1];
				unset($currentEvent);
				$currentEvent = null;
			}
			

			if(!is_null($currentEvent) && $currentEvent['id'] != $row["eventId"]){
				unset($currentEvent);
				$currentEvent = null;
			}
			
			if( $currentEvent == null && $row['eventId'] > 0){
				$currentResource['events'][] = array(
					'name' => $row['event'],
					'id' => $row['eventId'],
					'bookings' => array()
				);
				$currentEvent = &$currentResource['events'][count($currentResource['events'])-1];
			}
			
			if($row['bookingId'] > 0){
				$currentEvent['bookings'][] = array(
					'name' => $row['eventOccation'],
					'startsAt' => $row['startsAt'],
					'endsAt' => $row['endsAt'],
					'bookingId' => $row['bookingId']
				);
			}
		}
		
		return $resources;
		
	}	
	
	public static function getScheduleDataForResource($resourceId){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("SELECT "
			. "partial r.{id, name}, "
			. "partial b.{id}, "
			. "partial eob.{id}, "
			. "partial eo.{id,name},  "
			. "partial e.{id,name} , "
			. "partial et.{id,name} , "
			. "partial t.{id,name,startsAt,endsAt} "
			. "FROM Entities\\eventType et "
			. "LEFT JOIN et.events e "
			. "LEFT JOIN e.eventOccations eo "
			. "LEFT JOIN eo.timespan t "
			. "LEFT JOIN eo.eventOccationBookings eob "
			. "LEFT JOIN eob.booking b "
			. "LEFT JOIN b.resource r "
			. "WHERE eo.id != 'NULL' and r.id = :resourceId "
			. "ORDER BY t.startsAt ASC, t.endsAt ASC")
			->setParameter("resourceId", $resourceId)
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}
	
	

}