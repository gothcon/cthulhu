<?php
namespace Repository;
use Entities\Person;
use \UserAccessor;
use \UserLevel;
use Doctrine\ORM\EntityRepository;
use GothConEntityManager;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class SignupRepository extends GothConRepository {
    
    /**
     * @param Signup $signup
     */
    static function save($signup){
        parent::save($signup);
    }
	
	static function createAndSavePersonalSignup($personID, $signupSlotID){
		$em = GothConEntityManager::getInstance();
		$signup = new \Entities\Signup;
		$signup->setPerson($em->getReference("\\Entities\\Person", $personID));
		$signup->setSignupSlot($em->getReference("\\Entities\\SignupSlot", $signupSlotID));
		static::save($signup);
		return $signup;
	}
    
	static function createAndSaveGroupSignup($groupID, $signupSlotID){
		$em = GothConEntityManager::getInstance();
		$signup = new \Entities\Signup;
		$signup->setGroup($em->getReference("\\Entities\\Group", $groupID));
		$signup->setSignupSlot($em->getReference("\\Entities\\SignupSlot", $signupSlotID));
		static::save($signup);
		return $signup;
	}
	
    /**
     * @param Signup $signup
     */
    static function delete($signup){
        if(!UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
            header("HTTP/1.0 401 No access");
            exit;
        }
        return parent::delete($signup);
    }
	
	static function deleteById($id){
		$em = GothConEntityManager::getInstance();
		return static::delete($em->getReference("\\Entities\\Signup", $id));
	}
    
    /**
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    static function all($limit = null,$offset= null){
       $em = GothConEntityManager::getInstance();
       return $em->getRepository("\Entities\Person")->findBy(array(), null, $limit, $offset);
    }
    
    /**
     * 
     * @param int $personId
     */
    static function getByPerson($personId,$signupId = null){
        $em = GothConEntityManager::getInstance();

        $sql = "
            SELECT 
                    signup_id,
                    is_signup_owner,
                    signup_order_number FROM(	
                            SELECT 
                                    personalSignup.id AS signup_id,
                                    personalSignup.signup_slot_id,
                                    personalSignup.person_id,
                                    NULL AS group_id,
                                    1 AS is_signup_owner,
                                    COUNT(allSignups.id) AS signup_order_number
                            FROM signups personalSignup
                            JOIN signups allSignups
                            WHERE personalSignup.signup_slot_id = allSignups.signup_slot_id
                            AND personalSignup.id >= allSignups.id
                            AND personalSignup.person_id = :id
                            GROUP BY signup_slot_id

                            UNION

                            SELECT 		
                                    groupSignups.signup_id,
                                    groupSignups.signup_slot_id,
                                    NULL AS person_id,
                                    groupSignups.group_id,
                                    groupSignups.is_signup_owner, 
                                    COUNT(allSignups.id) AS signup_order_number 
                            FROM(
                                    SELECT 
                                            s.id AS signup_id, 
                                            s.signup_slot_id, 
                                            s.group_id AS group_id,
                                            gm.id = g.leader_membership_id AS is_signup_owner 
                                    FROM group_memberships gm 
                                    INNER JOIN groups g ON g.id = gm.group_id
                                    INNER JOIN signups s ON s.group_id = g.id
                                    WHERE gm.person_id = :id
                            ) groupSignups
                            JOIN signups allSignups
                            WHERE groupSignups.signup_slot_id = allSignups.signup_slot_id
                            AND groupSignups.signup_id >= allSignups.id
                            GROUP BY groupSignups.group_id, signup_slot_id
                    ) s";
        
        $res  = $em->getConnection()->executeQuery($sql, array("id" => $personId))->fetchAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $signupsStatus = array();            
        $signupIds = array();
        
        foreach($res as $signupState){
            array_push($signupIds,$signupState["signup_id"]);
            $signupsStatus[$signupState["signup_id"]] = $signupState; 
        }
        
        $signups = $em->createQuery("SELECT "
        . "partial s.{id,isPaid,isApproved}, "
        . "partial g.{name,id}, "
        . "partial ss.{id,requiresApproval}, "
        . "partial st.{id,name}, "
        . "partial eo.{id,name}, "
        . "partial t.{id,startsAt, endsAt, name}, "
        . "partial e.{id,name}, "
        . "partial et.{id,name}, "
        . "partial ssa.{id}, "
        . "partial g.{id,name}, "
        . "partial p.{id,firstName,lastName} "
        . "FROM Entities\Signup s "
        . "LEFT JOIN s.group g "
        . "LEFT JOIN s.person p "
        . "LEFT JOIN s.signupSlot ss "
        . "LEFT JOIN ss.slotType st "
        . "LEFT JOIN ss.eventOccation eo "
        . "LEFT JOIN eo.timespan t "
        . "LEFT JOIN eo.event e "
        . "LEFT JOIN e.eventtype et "
        . "LEFT JOIN ss.signupSlotArticle ssa "
        . "WHERE s.id IN(:signupIds) "
		. "ORDER BY t.startsAt")
            ->setParameter("signupIds", $signupId == null ? $signupIds : array($signupId))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        foreach($signups as &$signup){
            $signup["signupOwner"] = $signupsStatus[$signup["id"]]["is_signup_owner"];
            $signup["signupOrderNumber"] = $signupsStatus[$signup["id"]]["signup_order_number"];
        }
        unset($signup);
        return $signups;
    }
	static function getByGroup($personId,$signupId = null){
        $em = GothConEntityManager::getInstance();

        $sql = "
            SELECT 
                    signup_id,
                    is_signup_owner,
                    signup_order_number FROM(	
                            SELECT 		
                                    groupSignups.signup_id,
                                    groupSignups.signup_slot_id,
                                    groupSignups.group_id,
                                    groupSignups.is_signup_owner, 
                                    COUNT(allSignups.id) AS signup_order_number 
                            FROM(
                                    SELECT 
                                            s.id AS signup_id, 
                                            s.signup_slot_id, 
                                            s.group_id AS group_id,
                                            1 AS is_signup_owner 
                                    FROM
										signups s 
                                    WHERE s.group_id = :id
                            ) groupSignups
                            JOIN signups allSignups
                            WHERE groupSignups.signup_slot_id = allSignups.signup_slot_id
                            AND groupSignups.signup_id >= allSignups.id
                            GROUP BY groupSignups.group_id, signup_slot_id
                    ) s";
        
        $res  = $em->getConnection()->executeQuery($sql, array("id" => $personId))->fetchAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $signupsStatus = array();            
        $signupIds = array();
        
        foreach($res as $signupState){
            array_push($signupIds,$signupState["signup_id"]);
            $signupsStatus[$signupState["signup_id"]] = $signupState; 
        }
        
        $signups = $em->createQuery("SELECT "
        . "partial s.{id,isPaid,isApproved}, "
        . "partial g.{name,id}, "
        . "partial ss.{id,requiresApproval}, "
        . "partial st.{id,name}, "
        . "partial eo.{id,name}, "
        . "partial t.{id,startsAt, endsAt, name}, "
        . "partial e.{id,name}, "
        . "partial et.{id,name}, "
        . "partial ssa.{id} "
        . "FROM Entities\Signup s "
        . "LEFT JOIN s.group g "
        . "LEFT JOIN s.signupSlot ss "
        . "LEFT JOIN ss.slotType st "
        . "LEFT JOIN ss.eventOccation eo "
        . "LEFT JOIN eo.timespan t "
        . "LEFT JOIN eo.event e "
        . "LEFT JOIN e.eventtype et "
        . "LEFT JOIN ss.signupSlotArticle ssa "
        . "WHERE s.id IN(:signupIds) "
		. "ORDER BY t.startsAt")
            ->setParameter("signupIds", $signupId == null ? $signupIds : array($signupId))->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        foreach($signups as &$signup){
            $signup["signupOwner"] = $signupsStatus[$signup["id"]]["is_signup_owner"];
            $signup["signupOrderNumber"] = $signupsStatus[$signup["id"]]["signup_order_number"];
        }
        unset($signup);
        return $signups;
    }
}
