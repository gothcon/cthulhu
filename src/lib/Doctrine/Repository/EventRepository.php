<?php
namespace Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use GothConEntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class EventRepository extends \Repository\GothConRepository {
    
    /**
     * @author Joakim Ekeblad
     * @return Event
     */
    private static function _getSignupSlots($eventId = null,$personId = null,$groupId = null){
        $em = GothConEntityManager::getInstance();
        $queryString = "SELECT "
            . "partial et.{id,name}, "
            . "partial e.{id,name}, "
            . "partial eo.{name,id}, "
            . "partial st.{name,id}, "
            . "partial ss.{id}, "
            . "partial t.{id,startsAt,endsAt,name} "
            . "FROM "
            . "\Entities\EventType et "
            . "INNER JOIN et.events e "
            . "INNER JOIN e.eventOccations eo "
            . "INNER JOIN eo.signupSlots ss "
            . "INNER JOIN eo.timespan t "
            . "INNER JOIN ss.slotType st ";
        
        if($eventId){
            $queryString .= "WHERE e.id = :eventId";
            $signupSlots = $em->createQuery($queryString)->setParameter("eventId",$eventId)->getResult(Query::HYDRATE_ARRAY);
        }
        else if($personId){
            $personalSignups = $em->createQuery("SELECT partial ss.{id}, partial s.{id}  FROM \\Entities\\SignupSlot ss INNER JOIN ss.signups s WHERE Identity(s.person) = :id")->setParameter("id", $personId)->getResult(Query::HYDRATE_ARRAY);
			if(count($personalSignups) == 0){
				$queryString .= "WHERE st.cardinality = 1 ";
				$signupSlots = $em->createQuery($queryString)->getResult(Query::HYDRATE_ARRAY);
			}else{
				$queryString .= "WHERE ss.id NOT IN(:personalSignups) AND st.cardinality = 1";
				$signupSlots = $em->createQuery($queryString)->setParameter("personalSignups",$personalSignups)->getResult(Query::HYDRATE_ARRAY);
			}
        }
        else if($groupId){
            $groupSignups = $em->createQuery("SELECT partial ss.{id}, partial s.{id}  FROM \\Entities\\SignupSlot ss INNER JOIN ss.signups s WHERE Identity(s.group) = :id")->setParameter("id", $groupId)->getResult(Query::HYDRATE_ARRAY);
			if(count($groupSignups) == 0){
				$queryString .= "WHERE st.cardinality > 1 ";
				$signupSlots = $em->createQuery($queryString)->getResult(Query::HYDRATE_ARRAY);
			}else{
				$queryString .= "WHERE st.cardinality > 1 AND ss.id NOT IN(:groupSignups)";
				$signupSlots = $em->createQuery($queryString)->setParameter("groupSignups",$groupSignups)->getResult(Query::HYDRATE_ARRAY);
			}
        }else{
            $signupSlots = $em->createQuery($queryString)->getResult(Query::HYDRATE_ARRAY);
        }
        
        return $signupSlots;
        
        
        
    }
	
    public static function getSignupSlots(){
        return static::_getSignupSlots();
    }
	
    public static function getPersonalSignupSlots($personId){
        return static::_getSignupSlots(null,$personId,null);
    }

    public static function getGroupSignupSlots($groupId){
        return static::_getSignupSlots(null,null,$groupId);
    }

    public static function getEventSignupSlots($eventId){
        return static::_getSignupSlots($eventId,null,null);
    }
	
	public static function getEventOccations($eventId){
		return GothConEntityManager::getInstance()
			->createQuery("SELECT partial eo.{id}, t FROM Entities\\EventOccation eo LEFT JOIN eo.timespan t WHERE eo.event = :event")
			->setParameter("event",GothConEntityManager::createEventReference($eventId))
			->getResult(Query::HYDRATE_ARRAY);
	}
    
	public static function getResourceEventOccations($eventId,$resourceId){
		return GothConEntityManager::getInstance()
			->createQuery("SELECT eo,t FROM Entities\\EventOccation eo LEFT JOIN eo.timespan t "
				. "LEFT JOIN eo.eventOccationBooking eob "
				. "WHERE eo.event = :event && eob.resource = :resource INDEX BY eo")
			->setParameter("event",GothConEntityManager::createEventReference($eventId))
			->setParameter("resource",GothConEntityManager::createResourceReference($resourceId))
			->getResult(Query::HYDRATE_ARRAY);
	}
	
	public static function getEvent($eventId,$asObject=true){
		$query = GothConEntityManager::getInstance()
			->createQuery("SELECT e, "
				. "partial et.{name,id}, "
				. "partial o.{id}, "
				. "partial op.{id, firstName, lastName,emailAddress}, "
				. "partial og.{id,name,emailAddress}, "
				. "eo, "
				. "t, "
				. "partial eob.{id,booking}, "
				. "partial b.{id,resource},"
				. "partial r.{id,name},"
				. "partial rt.{id,name},"
				. "partial ss.{id,maximumSignupCount, maximumSpareSignupCount,requiresApproval,isHidden }, "
				. "s, "
				. "partial sp.{id, firstName, lastName,emailAddress}, "
				. "partial sg.{id,name,emailAddress}, "
				. "partial st.{id,name,cardinality} "
				. "FROM \\Entities\\Event e "
				. "LEFT JOIN e.organizer o "
				. "LEFT JOIN o.person op "
				. "LEFT JOIN o.group og "
				. "LEFT JOIN e.eventtype et "
				. "LEFT JOIN e.eventOccations eo "
				. "LEFT JOIN eo.eventOccationBookings eob "
				. "LEFT JOIN eob.booking b "
				. "LEFT JOIN b.resource r "
				. "LEFT JOIN r.resourceType rt "
				. "LEFT JOIN eo.timespan t "
				. "LEFT JOIN eo.signupSlots ss "
				. "LEFT JOIN ss.slotType st "
				. "LEFT JOIN ss.signups s "
				. "LEFT JOIN s.person sp "
				. "LEFT JOIN s.group sg "
				. "WHERE e.id = :eventId "
				. "ORDER BY t.startsAt, t.endsAt ")
			->setParameter("eventId",$eventId);

		if($asObject){
			$events = $query->getResult(Query::HYDRATE_OBJECT);
			
		}else{
			$events = $query->getResult(Query::HYDRATE_ARRAY);
		}
		return $events[0];
	}

	public static function getAll(){
		$em = GothConEntityManager::getInstance();
		$query = $em->createQuery("SELECT "
			. "PARTIAL e.{id,name,visibleInSchedule, visibleInPublicListing}, "
			. "PARTIAL et.{id,name}, "
			. "PARTIAL o.{id}, "
			. "PARTIAL p.{id,firstName,lastName,emailAddress}, "
			. "PARTIAL g.{id,name,emailAddress} "
			. "FROM \\Entities\\Event e "
			. "LEFT JOIN e.eventtype et "
			. "LEFT JOIN e.organizer o "
			. "LEFT JOIN o.person p "
			. "LEFT JOIN o.group g ");
		$result = $query->getResult(Query::HYDRATE_ARRAY);
		return $result;
	}
	
	public static function getResourceBookings(){
		$conn = GothConEntityManager::getInstance()->getConnection();
		$query = "SELECT 
					b.id AS bookingId,
					r.name AS resource,
					r.id AS resourceId,
					rt.name AS resourceType,
					e.name AS event,
					e.id AS eventId,
					eo.name AS eventOccation,
					t.starts_at AS startsAt,
					t.ends_at AS endsAt
				FROM resources r
				LEFT JOIN resource_types rt ON rt.id = r.resource_type_id
				LEFT JOIN bookings b ON b.resource_id = r.id
				LEFT JOIN event_occation_bookings eob ON eob.booking_id = b.id
				LEFT JOIN event_occations eo ON eo.id = eob.event_occation_id
				LEFT JOIN timespans t ON t.id = eo.timespan_id
				LEFT JOIN events e ON e.id = eo.event_id
				ORDER BY event ASC, resource ASC, startsAt, endsAt";
		$resultSet = $conn->executeQuery($query);
		
		$events = array();
		$currentEvent = null;
		$currentResource = null;

		while($row = $resultSet->fetch(\PDO::FETCH_ASSOC)){

			if(!is_null($currentEvent) && $currentEvent['id'] != $row["eventId"]){
				unset($currentEvent);
				$currentEvent = null;
			}
			
			if(!is_null($currentResource) && $currentResource['id'] != $row["resourceId"]){
				unset($currentResource);
				$currentResource = null;
			}
			
			if( $currentEvent == null){
				$events[] = array(
					'name' => $row['event'],
					'id' => $row['eventId'],
					'resources' => array()
				);
				$currentEvent = &$events[count($events)-1];
				unset($currentResource);
				$currentResource = null;
			}
			


			if($currentResource == null){
				$currentEvent["resources"][] = array(
					'name' => $row['resource'],
					'id' => $row['resourceId'],
					'bookings' => array()
				);
				$currentResource = &$currentEvent["resources"][count($currentEvent["resources"])-1];
			}
			
			if($row['bookingId'] > 0){
				$currentResource['bookings'][] = array(
					'name' => $row['eventOccation'],
					'startsAt' => $row['startsAt'],
					'endsAt' => $row['endsAt'],
					'bookingId' => $row['bookingId']
				);
			}
		}
		
		return $events;
		
	}
	
	
}
