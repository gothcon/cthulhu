<?php
namespace Repository;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
use \DateTime;
use \DateInterval;
class StatisticsRepository extends GothConRepository {

	static function GetArrivalCount(){
		$em = \GothConEntityManager::getInstance();
		
		return $em->createQuery("SELECT count( distinct p) FROM Entities\\Article a "
			. " LEFT JOIN a.orderRow r "
			. " LEFT JOIN r.order o"
			. " LEFT JOIN o.person p "
			. " WHERE a.type = 'entrance_fee' AND r.status = 'delivered'")
			
			->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
	}
   
	static function GetArrivalCountByGender(){
		$em = \GothConEntityManager::getInstance();
		$sql= "SELECT DISTINCT people.* FROM articles
				LEFT JOIN order_rows ON order_rows.article_id = articles.id
				LEFT JOIN orders ON orders.id = order_rows.order_id
				LEFT JOIN people ON people.id = orders.person_id
				WHERE articles.type = 'entrance_fee'
				AND ! isnull(people.id)
				AND order_rows.status = 'delivered'";
		$rsm = new \Doctrine\ORM\Query\ResultSetMappingBuilder($em);
		$rsm->addRootEntityFromClassMetadata('Entities\Person', 'p');
		
		$res = $em->createNativeQuery($sql, $rsm)->getResult();
		
		$result = new \stdClass();
		$result->maleCount = 0;
		$result->femaleCount = 0;
		$result->totalCount = 0;
		$result->unknownCount = 0;
		foreach($res as $person){
			$result->totalCount++;
			$personnummer = $person->getIdentification();
			if(strlen($personnummer) == 10){
				
				if($personnummer{8} % 2 == 0){
					$result->femaleCount++;
				}else{
					$result->maleCount++;
				}
			}else{
				$result->unknownCount++;
			}
		}
		return $result;
    }
	
	static function GetApproximatedTotalArrivalCount(){
		$em = \GothConEntityManager::getInstance();
		$sql= "SELECT count(DISTINCT people.id) as totalCount FROM articles
				LEFT JOIN order_rows ON order_rows.article_id = articles.id
				LEFT JOIN orders ON orders.id = order_rows.order_id
				LEFT JOIN people ON people.id = orders.person_id
				WHERE orders.status in ('partially_paid','paid')
				AND ! isnull(people.id)
				";
		$rsm = new \Doctrine\ORM\Query\ResultSetMappingBuilder($em);
		$rsm->addScalarResult("totalCount", "totalCount");
		return $em->createNativeQuery($sql, $rsm)->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
    }
	
	static function GetUnarrivedCount(){
		$em = \GothConEntityManager::getInstance();
		$sql= "SELECT count(DISTINCT people.id) as totalCount FROM articles
				LEFT JOIN order_rows ON order_rows.article_id = articles.id
				LEFT JOIN orders ON orders.id = order_rows.order_id
				LEFT JOIN people ON people.id = orders.person_id
				WHERE orders.status in ('partially_paid','paid')
				AND ! isnull(people.id)
				AND order_rows.status = 'not_delivered'";
		$rsm = new \Doctrine\ORM\Query\ResultSetMappingBuilder($em);
		$rsm->addScalarResult("totalCount", "unarrivedCount");
		return $em->createNativeQuery($sql, $rsm)->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
    }
	
	static function GetArticleSalesAmounts(){
		$em = \GothConEntityManager::getInstance();
		$sql= "SELECT "
			. "article_category.name AS category_name, article_category.id as category_id, " 
			. "articles.name AS article_name, articles.id as article_id, "
			. "SUM(article_order_row.total) AS order_total, "
			. "COUNT(article_order_row.id) AS orderrowcount "
			. "FROM articles AS article_category "
			. "LEFT JOIN articles ON articles.p = article_category.id "
			. "LEFT JOIN order_rows AS article_order_row ON article_order_row.article_id = articles.id "
			. "LEFT JOIN orders ON orders.id = article_order_row.order_id "
			. "WHERE (orders.status = 'paid' OR orders.status = 'partially_paid') "
			. "AND articles.type != \"category\" "
			. "GROUP BY article_id "
			. "HAVING orderrowcount > 0";
		$rsm = new \Doctrine\ORM\Query\ResultSetMappingBuilder($em);
		$rsm->addScalarResult("category_id","category_id");
		$rsm->addScalarResult("category_name","category_name");
		$rsm->addScalarResult("article_name","article_name");
		$rsm->addScalarResult("article_id","article_id");
		$rsm->addScalarResult("order_total","order_total");
		$rows = $em->createNativeQuery($sql, $rsm)->getResult();
		$currentCategory = null;
		$result = array();
		foreach($rows as $row){
			if($currentCategory == null || $currentCategory->id != $row["category_id"]){
				if($currentCategory != null){
					$result[] = $currentCategory;
				}
				$currentCategory = new ArticleStatisticsCategory($row["category_name"],$row["category_id"]);
			}
			$currentCategory->total += $row["order_total"];
			$currentCategory->articles[] = new ArticleStatisticsArticle($row["article_name"],$row["article_id"],$row["order_total"]);
		}
		if($currentCategory != null){
			$result[] = $currentCategory;
		}
		return $result;
	}	
}


class ArticleStatisticsCategory{
	public $name ="";
	public $id = 0;
	public $articles = array();
	public $total = 0;
	public function __construct($name,$id){
		$this->name = $name;
		$this->id = $id;
	}
}	

class ArticleStatisticsArticle{
	public $name = "";
	public $id = 0;
	public $order_total = 0;
	public function __construct($name,$id,$order_total){
		$this->name = $name;
		$this->id = $id;
		$this->order_total = $order_total;
	}
}