<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Repository;

use Doctrine\ORM\Query;
use GothConEntityManager;

/**
 * Description of GroupRepository
 *
 * @author Joakim
 */
class GroupRepository  extends GothConRepository {
	//put your code here
	
	public static function getAllGroups(){
		$em = GothConEntityManager::getInstance();
		$query = $em->createQuery("select partial g.{id,name,emailAddress} from \\Entities\\Group g");
		$result = $query->getResult(Query::HYDRATE_ARRAY);
		return $result;
	}
	
	public static function getAvailableGroups($personId){
		$em = GothConEntityManager::getInstance();
		
		$groupMemberships = $em->createQuery("select partial g.{id} from \\Entities\\Group g "
				. "INNER JOIN g.groupMembers gm "
                . "WHERE gm.person = :person ")
			->setParameter("person", $em->getReference("\\Entities\\Person", $personId))
			->getResult(Query::HYDRATE_ARRAY);
		
		foreach($groupMemberships as &$group){
			$group = $em->getReference("\\Entities\\Group",$group["id"]);
		}
			
		
		
		
		$query = $em->createQuery("select partial g.{id,name,emailAddress} from \\Entities\\Group g");
			
		if(count($groupMemberships) > 0){
			$queryString = $query->getDQL();
			$queryString .= " WHERE g not in(:groups)";
			$query->setDQL($queryString)->setParameter("groups",$groupMemberships);
		}

		$result = $query->getResult(Query::HYDRATE_ARRAY);

		return $result;
    }
	
	public static function addMembership($personId,$groupId){
		
		$em = GothConEntityManager::getInstance();

		$membership = new \Entities\GroupMembership();
		$membership->setGroup($em->getReference("\\Entities\\Group",$groupId));
		$membership->setPerson($em->getReference("\\Entities\\Person",$personId));
		
		parent::save($membership);
		
		$membership = $em->createQuery(
			  "select gm,g,partial p.{id,firstName,lastName,emailAddress} from \\Entities\\GroupMembership gm "
			. "LEFT JOIN gm.group g " 
			. "LEFT JOIN gm.person p " 
			. "WHERE gm = :gm")
			->setParameter("gm",$membership)->getResult(Query::HYDRATE_ARRAY);

		return $membership[0];
	}
	
	public static function removeMembership($membershipId){
		$em = GothConEntityManager::getInstance();
		$membership = $em->getReference("\\Entities\\GroupMembership",$membershipId);
		return parent::delete($membership);
	}
	
	/**
	 * 
	 * @param type $groupId
	 * @return \Entities\Group
	 */
	public static function getGroup($groupId){
		$em = GothConEntityManager::getInstance();
		$group = $em->createQuery("select g,gm,p from \\Entities\\Group g "
			. "LEFT JOIN g.groupMembers gm "
			. "LEFT JOIN gm.person p "
            . "WHERE g.id = :id ")
			->setParameter("id", $groupId)
			->getResult(Query::HYDRATE_OBJECT);
		return $group[0];
	}
	
	
	/**
	 * 
	 * @param int $membershipId
	 * @return \Entities\GroupMembership
	 */
	public static function createGroupMembershipReference($membershipId){
		$em = GothConEntityManager::getInstance();
		return $em->getReference("\\Entities\\GroupMembership", $membershipId);
	}
}
