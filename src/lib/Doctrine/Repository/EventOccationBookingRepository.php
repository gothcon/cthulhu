<?php
namespace Repository;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */

class EventOccationBookingRepository extends GothConRepository {
	/**
	 * 
	 * @param integer $eventOccationBookingId
	 * @return array
	 */
   public static function get($eventOccationBookingId){
		$em = \GothConEntityManager::getInstance();
		return $em->createQuery("SELECT "
			. "partial b.{id}, "
			. "partial eob.{id}, "
			. "partial eo.{id,name},  "
			. "partial e.{id,name} , "
			. "partial t.{id,startsAt,endsAt},"
			. "partial r.{id,name}, "
			. "partial rt.{id,name} "
			. "FROM Entities\\EventOccationBooking eob "
			. "LEFT JOIN eob.booking b "
			. "LEFT JOIN b.resource r "
			. "LEFT JOIN r.resourceType rt "
			. "LEFT JOIN eob.eventOccation eo "
			. "LEFT JOIN eo.event e "
			. "LEFT JOIN eo.timespan t "
			. "WHERE eob.id = :eventOccationBookingId "
			. "ORDER BY t.startsAt ASC, t.endsAt ASC")
			->setParameter("eventOccationBookingId", $eventOccationBookingId)
			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];
	}
}
