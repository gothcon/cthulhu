<?php
namespace Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use GothConEntityManager;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class TimespanRepository extends GothConRepository {
	public static function getPublic(){
		$em = GothConEntityManager::getInstance();
		return $em->createQuery("SELECT t from \Entities\Timespan t WHERE t.isPublic = 1 ORDER BY t.startsAt")
		->getResult(Query::HYDRATE_ARRAY);
	}
	
	public static function getPublicScheduleTimespans(){
		$em = GothConEntityManager::getInstance();
		return $em->createQuery("SELECT t from \Entities\Timespan t WHERE t.isPublic = 1 AND t.showInSchedule=1 ORDER BY t.startsAt")
		->getResult(Query::HYDRATE_ARRAY);
	}
	
	public static function get($id){
		$em = GothConEntityManager::getInstance();
		$res = $em->createQuery("SELECT t, partial eo.{id,name}, partial e.{id,name}, partial et.{id,name} from \Entities\Timespan t "
			. "LEFT JOIN t.eventOccations eo "
			. "LEFT JOIN eo.event e "
			. "LEFT JOIN e.eventtype et "
			. "WHERE t.id = :id ")->setParameter("id",$id)->getResult(Query::HYDRATE_OBJECT);
		return $res[0];
	}
	
	public static function getAsObject($id){
		$em = GothConEntityManager::getInstance();
		$res = $em->createQuery("SELECT t, partial eo.{id,name}, partial e.{id,name}, partial et.{id,name} from \Entities\Timespan t "
			. "LEFT JOIN t.eventOccations eo "
			. "LEFT JOIN eo.event e "
			. "LEFT JOIN e.eventtype et "
			. "WHERE t.id = :id ")->setParameter("id",$id)->getResult(Query::HYDRATE_OBJECT);
		return $res[0];
	}
	
	public static function getScheduleData(){
		$em = GothConEntityManager::getInstance();
		$res = $em->createQuery("SELECT t, partial eo.{id,name}, partial e.{id,name}, partial et.{id,name} from \Entities\EventType et "
			. "LEFT JOIN et.events e "
			. "LEFT JOIN e.eventOccations eo "
			. "LEFT JOIN eo.timespan t ORDER BY et.name, e.name, t.startsAt")->getResult(Query::HYDRATE_ARRAY);
		return $res;
		
	}
}
