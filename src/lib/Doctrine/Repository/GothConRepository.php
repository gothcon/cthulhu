<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Repository;
use GothConEntityManager;
/**
 * Description of GothConRepository
 *
 * @author Joakim
 */

class GothConRepository extends \Doctrine\ORM\EntityRepository {
    
	public static function getGCEntityManager(){
		return GothConEntityManager::getInstance();
	}
	
    private static $_eventManager = null;
    
    /**
     * 
     * @return \Doctrine\Common\EventManager
     */
    public static function getEventManager(){
        if(static::$_eventManager == null){
            static::$_eventManager = new \Doctrine\Common\EventManager();
        }
        return static::$_eventManager;
    }
    
    public static function registerEventSubscriber($subscriber){
        static::getEventManager()->addEventSubscriber($subscriber);
    }
    
    public static function removeEventSubscriber($subscriber){
        static::getEventManager()->removeEventSubscriber($subscriber);
    }
    
    /**
     * 
     * @param mixed $entity
     * @param string $propertyName
     */
    protected static function revertPropertyChange($entity,$propertyName){
        $em = static::getGCEntityManager();
        $unitOfWork = $em->getUnitOfWork();
        $class = $em->getClassMetadata(get_class($entity));
        $unitOfWork->computeChangeSet($class,$entity);
        $changeset = $unitOfWork->getEntityChangeSet($entity);
        if(isset($changeset[$propertyName])){
            $entity->setInternalNote($changeset[$propertyName][0]);
        }
    }
    
    /**
     * @param mixed $entity
     */
    static function save($entity,$flush=true){
        $em = static::getGCEntityManager();
        $em->persist($entity); 
		if($flush){
			$em->flush();
		}
    }
	
	/**
     * @param mixed $entity
     */
    static function persist($entity){
        $em = static::getGCEntityManager();
        $em->persist($entity); 
    }
    
    /**
     * @param mixed $entity
     */
    static function delete($entity){
        $em = static::getGCEntityManager();
        $em->remove($entity);
        $em->flush();
    }
}
