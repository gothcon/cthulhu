<?php
namespace Repository;

use Doctrine\ORM\Query;
use GothConEntityManager;
use \stdClass;

require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EventOccationRepository
 *
 * @author Joakim
 */
class EventOccationRepository extends GothConRepository {
	public static function getEventOccation($eventOccationId){
		return static::getGCEntityManager()->getRepository("Entities\\EventOccation")->find($eventOccationId);
	}	
	
	public static function createReference($eventOccationId){
		return static::getGCEntityManager()->getReference("Entities\\EventOccation",$eventOccationId);
	}
	
	public static function getEventOccationResourceBooking($resourceId, $eventOccationId){
		
		$q = static::getGCEntityManager()->createQuery(""
			. "SELECT eb from Entities\\EventOccationBooking eb "
			. "LEFT JOIN eb.booking b "
			. "WHERE "
			. "		eb.eventOccation = :eventOccation "
			. "AND "
			. "		b.resource = :resource");
			$q->setParameter("eventOccation", static::createReference($eventOccationId));
			$q->setParameter("resource", ResourceRepository::createReference($resourceId));
		$result = $q->getResult();
		if(count($result) == 0){
			return null;
		}
		return $result[0];
	}
	
	/**
	 * @param type $eventOccationId
	 * @param type $asObject
	 * @return \Entities\EventOccation
	 */
	public static function getAsObject($eventOccationId){
		return static::get($eventOccationId,true);
	}
	
	/**
	 * @param type $eventOccationId
	 * @param type $asObject
	 * @return Array
	 */
	public static function getAsArray($eventOccationId){
		return static::get($eventOccationId,false);
	}

	
	public static function get($eventOccationId,$asObject = false){
		$query = GothConEntityManager::getInstance()
			->createQuery("SELECT eo, "
				. "t, "
				. "partial eob.{id,booking}, "
				. "partial b.{id,resource},"
				. "partial r.{id,name},"
				. "partial rt.{id,name},"
				. "partial ss.{id,maximumSignupCount, maximumSpareSignupCount,requiresApproval,isHidden }, "
				. "s, "
				. "partial sp.{id, firstName, lastName,emailAddress}, "
				. "partial sg.{id,name,emailAddress}, "
				. "partial st.{id,name,cardinality} "
				. "FROM \\Entities\\EventOccation eo "
				. "LEFT JOIN eo.eventOccationBookings eob "
				. "LEFT JOIN eob.booking b "
				. "LEFT JOIN b.resource r "
				. "LEFT JOIN r.resourceType rt "
				. "LEFT JOIN eo.timespan t "
				. "LEFT JOIN eo.signupSlots ss "
				. "LEFT JOIN ss.slotType st "
				. "LEFT JOIN ss.signups s "
				. "LEFT JOIN s.person sp "
				. "LEFT JOIN s.group sg "
				. "WHERE eo.id = :eventOccationId ")
			->setParameter("eventOccationId",$eventOccationId);
		
		if($asObject){
			$eventOccations = $query->getResult(Query::HYDRATE_OBJECT);
			
		}else{
			$eventOccations = $query->getResult(Query::HYDRATE_ARRAY);
		}
		return count($eventOccations) > 0 ? $eventOccations[0]: null;
	}
	
//	/**
//	 * 
//	 * @param type $resourceId
//	 * @return array[\Entitites\Event]
//	 */
//	public static function getEventOccationsForResource($resourceId){
//
//		
//		$bookings = GothConEntityManager::getInstance()->createQuery("SELECT "
//			. "partial b.{id}, "
//			. "partial eob.{id}, "
//			. "partial eo.{id,name},  "
//			. "partial e.{id,name} , "
//			. "partial t.{id,startsAt,endsAt} "
//			. "FROM Entities\\Booking b "
//			. "LEFT JOIN b.eventOccationBooking eob "
//			. "LEFT JOIN eob.eventOccation eo "
//			. "LEFT JOIN eo.event e "
//			. "LEFT JOIN eo.timespan t "
//			. "WHERE b.resource = :resource AND eo.id != 'NULL'"
//			. "ORDER BY t.startsAt ASC, t.endsAt ASC")
//			->setParameter("resource", static::createReference($resourceId))
//			->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
//		
//		
//		
//		
//		
//		GothConEntityManager::getInstance()
//			->createQuery("SELECT eo.id "
//				. "FROM \\Entities\\EventOccationBooking eob indexed by eob.eventOccationBookingId "
//				. "LEFT JOIN eventOccationBookings eob "
//				. "LEFT JOIN eob.booking b "
//				. "WHERE b.resourceId = :resourceId "
//			)
//			->setParameter("resourceId",$resourceId)
//			->getResult(Query::HYDRATE_ARRAY);
//		
//		
//		
//		$events = GothConEntityManager::getInstance()
//			->createQuery("SELECT partial e.{id,name}, partial eo.{id,name}, t "
//				. "FROM \\Entities\\Event e "
//				. "LEFT JOIN e.eventOccations eo "
//				. "LEFT JOIN eo.timespan t "
//				. "WHERE count(eo) > 0 ")
//			->getResult(Query::HYDRATE_ARRAY);
//
//		$data = array();
//		
//		
//		
//		foreach($events as $key => $event){
//			$event = array();
//			$event['eventOccations'] = array(); 
//			foreach($event[$key]->eventOccations as $key => $eventOccation){
//				 
//			
//			}
//		}
//		
//		
//
//	}
//	
	public static function getBookingTreeForResource($resourceId){
		
		$conn = GothConEntityManager::getInstance()->getConnection();
		$query = "
			SELECT `event_id`, `event`,  `event_occation_id`,`event_occation`,`occation_starts_at`, `occation_ends_at`, timespan_name as timespan, COUNT(is_overlapping) = 0 AS ok_to_book, COUNT(is_same_occation) = 1 AS already_booked FROM
			(
				SELECT *,
					CASE
						WHEN event_occation_id = booking_occation_id THEN NULL
						WHEN occation_starts_at > booking_starts_at AND occation_starts_at < booking_ends_at THEN 1
						WHEN occation_ends_at > booking_starts_at AND occation_ends_at < booking_ends_at THEN 1
						WHEN occation_starts_at <= booking_starts_at && occation_ends_at >= booking_ends_at THEN 1
						WHEN booking_starts_at <= occation_starts_at && booking_ends_at >= occation_ends_at THEN 1
						ELSE NULL
					END 
					AS is_overlapping,
					CASE 
						WHEN event_occation_id = booking_occation_id THEN 1
						ELSE NULL
					END 
					AS is_same_occation	

				FROM 
				(
					SELECT 
						e.name AS event, e.id AS event_id,
						eo.id AS event_occation_id, 
						eo.name AS event_occation, 
						t.starts_at AS occation_starts_at,
						t.ends_at AS occation_ends_at,
						t.name AS timespan_name,
						t.id AS timespan_id
					FROM event_occations eo 
					LEFT JOIN `events` e ON e.id = eo.event_id
					LEFT JOIN `timespans` t ON t.id = eo.timespan_id
				 ) occations
				 JOIN 
				 (
					-- Timed bookings
					SELECT 
						b.resource_id,
						starts_at AS booking_starts_at,
						ends_at AS booking_ends_at,
						booking_id,
						NULL AS booking_occation_id,
						NULL AS booking_event_id
					FROM timed_bookings tb
					LEFT JOIN bookings b ON b.id = tb.booking_id
					UNION
					-- Event bookings
					SELECT
						b.resource_id,
						'1900-01-01 00:00:00', 
						'9999-12-31 23:59:59',
						booking_id,
						NULL,
						eb.event_id
					FROM event_bookings eb
					LEFT JOIN bookings b ON b.id = eb.booking_id
					UNION
					-- Event occation Bookings
					SELECT 
						b.resource_id,
						t.starts_at,
						t.ends_at, 
						eob.booking_id,
						eob.event_occation_id,
						eo.event_id
					FROM event_occation_bookings eob
					LEFT JOIN bookings b ON b.id = eob.booking_id
					LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
					LEFT JOIN timespans t ON t.id = eo.timespan_id
					UNION
					SELECT 
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL
				)
				bookings 
				WHERE bookings.resource_id= :resourceId
				OR ISNULL(bookings.resource_id)
			) event_occations
			GROUP BY event_occation_id
			ORDER BY `event`, occation_starts_at ASC
		";
				
		$params = array("resourceId" => $resourceId);
		
		$eventList = array();
		
		$resultSet = $conn->executeQuery($query,$params);
		
		$currentEvent = null;
		
		while($row = $resultSet->fetch(\PDO::FETCH_ASSOC)){
			if(!is_null($currentEvent) && $currentEvent['id'] != $row["event_id"]){
				$eventList[] = $currentEvent;
				$currentEvent = null;
			}
			
			
			if(is_null($currentEvent)){
				$currentEvent = array();
				$currentEvent['eventOccations'] =  array();
				$currentEvent['name'] = $row["event"];
				$currentEvent['id'] = $row["event_id"];
			}
			
			$timespan = array();
			$timespan['startsAt'] = array("date" => $row['occation_starts_at']);
			$timespan['endsAt'] = array("date" => $row['occation_ends_at']);
			$timespan['name'] = $row["timespan"];
			
			$eventOccation = array();
			$eventOccation['name'] = $row["event_occation"];
			$eventOccation['id'] = $row["event_occation_id"];
			$eventOccation['ok_to_book'] = $row['ok_to_book'];
			$eventOccation['already_booked'] = $row['already_booked'];
			$eventOccation['timespan'] = $timespan;

			$currentEvent['eventOccations'][] = $eventOccation;
		}
		
		$eventList[] = $currentEvent;
		
		return $eventList;
		
		
	}
	
	
}
