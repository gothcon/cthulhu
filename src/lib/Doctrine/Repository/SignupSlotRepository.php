<?php
namespace Repository;
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class SignupSlotRepository extends \Repository\GothConRepository {
	public static function get($slotId, $asObject = true){
		$query = static::getGCEntityManager()->createQuery(""
			. "select ss, st , s, partial g.{id,name}, partial p.{id,firstName,lastName} "
			. "FROM Entities\\SignupSlot ss "
			. "LEFT JOIN ss.slotType st "
			. "LEFT JOIN ss.signups s "
			. "LEFT JOIN s.group g "
			. "LEFT JOIN s.person p "
			. "WHERE ss.id = :id")
			->setParameter("id",$slotId);
		if($asObject){
			return $query->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);
		}
		return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}	

	/**
	 * @param type $slotId
	 * @return \Entities\SignupSlot
	 */
	public static function getAsObject($slotId){
		return static::get($slotId,true)[0];
	}	
	
	/**
	 * 
	 * @param int $slotId
	 * @return Array
	 */
	public static function getAsArray($slotId){
		return static::get($slotId,false)[0];
	}	
}
