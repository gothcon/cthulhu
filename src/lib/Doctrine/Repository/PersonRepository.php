<?php
namespace Repository;
use Entities\Person;
use \UserAccessor;
use \UserLevel;
use \Doctrine\ORM\Query;
use Doctrine\ORM\EntityRepository;
use GothConEntityManager;
 
require_once __DIR__ . "/../GothConEntityManager.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of EventTypeRepository
 *
 * @author Joakim
 */
class PersonRepository extends GothConRepository {
    
    /**
     * @param Person $person
     */
    public static function save($person){
		
		$personnummer = $person->getIdentification();
		
        if(!UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
           static::revertPropertyChange($person, 'internalNote');
        }
        parent::save($person);
    }
    
    /**
     * @param Person $person
     */
    public static function delete($person){
		return parent::delete($person);
    }
    
    /**
     * @return Person[]
     */
    public static function all(){
        $em = GothConEntityManager::getInstance();
        $query = $em->createQuery('SELECT partial p.{firstName,lastName,id,emailAddress} FROM \\Entities\\Person p');
        $query->useResultCache(true);
        $res = $query->getResult(Query::HYDRATE_ARRAY);
        return $res;
    }
    
    /**
     * @param int $id
     * @return Person
     */
    public static function person($id = null, $fetchAsArray = false){
        $em = GothConEntityManager::getInstance();
        if($id == null){
            $person = new Person();
            $em->persist($person);
        }else{
            $query = $em->createQuery("select p,u,gm,g from \\Entities\\Person p "
                . "LEFT JOIN p.groupMemberships gm "
                . "LEFT JOIN p.user u "
                . "LEFT JOIN gm.group g "
                . " WHERE p.id = :id")
                    ->setParameter("id",$id)
                    ->useResultCache(false);
            if($fetchAsArray){
                $people = $query->getResult(Query::HYDRATE_ARRAY);
            }else{
                $people = $query->getResult();
            }
            if(count($people)){
                $person = $people[0];
            }else{
                $person = null;
            }
        }
        return $person;
    }
    
	/**
	 * 
	 * @param string $identification
	 * @return Entities\Person
	 */
	public static function personByIdentification($identification){
		$em = GothConEntityManager::getInstance();
		return $em->getRepository("Entities\\Person")->findBy(array("identification" => $identification));
	}
	/**
	 * 
	 * @param string $email
	 * @return Entities\Person
	 */
	public static function personByEmailAddress($email){
		$em = GothConEntityManager::getInstance();
		$res = $em->getRepository("Entities\\Person")->findBy(array("emailAddress" => $email));
		return count($res) > 0 ? $res[0] : null;
	}
	
    /**
     * @return Person
     */
    public static function create(){
        return static::person();
    }

}
