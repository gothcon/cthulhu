<?php
	require_once("lib/Routers/Router.php");
	class PublicRouter extends Router{
		protected function getControllerDirectory(){
			return parent::getControllerDirectory()."/PublicControllers";
		}	
	}
