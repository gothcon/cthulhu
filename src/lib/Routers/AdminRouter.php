<?php
	require_once("lib/Routers/Router.php");
	require_once("lib/SessionAccessors/UserAccessor.php");
	class adminRouter extends Router{
		
		public function handleRequest($relativeUrl) {
			parent::handleRequest($relativeUrl);
		}
		
		protected function userHasAccessToController($controllerName){
			switch($controllerName){
				default:
					return UserAccessor::getCurrentUser()->level >= UserLevel::WORKER;
			}
		}
		
		protected function getControllerDirectory(){
			return parent::getControllerDirectory()."/AdminControllers";
		}	
		
	}