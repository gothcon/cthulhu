<?php
	require_once("lib/Routers/Router.php");
	require_once("lib/SessionAccessors/UserAccessor.php");
	class OrganizerRouter extends Router{
		
		public function handleRequest($relativeUrl) {
			parent::handleRequest($relativeUrl);
		}
		
		protected function userHasAccessToController($controllerName){
			return UserAccessor::getCurrentUser()->level >= 20;
		}
		
		protected function getControllerDirectory(){
			return parent::getControllerDirectory() . "/OrganizerControllers";
		}	
		
	}