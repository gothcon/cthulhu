<?php
	
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."../Models/Model.php");
	require_once("lib/ClassLib/Translator.php");
	require_once("lib/ClassLib/InternalLinkBuilder.php");
	
	class Router{
	
		static $rewrittenUrl;
		static $getParameters;
		
		/**
		 *
		 * @var InternalLinkBuilder 
		 */
		protected $internalLinkBuilder;
		
		protected $loginControllerName;
		protected $urlBase;
		protected $useFriendlyUrls;
		static $defaultController = "Index";

		/**
		 * @var SimpleXMLElement
		 */
		private $configurationData;
		
		/**
		 * @param SimpleXMLElement $configurationData
		 * @param bool $useFriendlyUrls
		 * @param string $applicationPath
		 */
		public function __construct($configurationData,$useFriendlyUrls,$routerBasePath){
			$this->configurationData = $configurationData;
			$attributes = $configurationData->attributes();
			$this->loginControllerName = (string)$attributes["loginController"];
			
			$this->urlBase = $routerBasePath;
			$this->useFriendlyUrls = $useFriendlyUrls;
			
			$controllerNameTranslations = array();
			
			$nodes = $configurationData->xpath("./localizedControllerNames/".Translator::getLang());
			if(count($nodes)){
				foreach($nodes[0] as $key => $node){
					$controllerNameTranslations[$key] = (string)$node;
				}
			}
			$this->internalLinkBuilder = new InternalLinkBuilder($this->urlBase,$controllerNameTranslations,$this->useFriendlyUrls);
			
		}
		
		public function handleRequest($relativeUrl){
			$segments = $this->splitUrl($relativeUrl);
			                        
			switch(count($segments)){
				case 0:
					$controller = $this->getDefaultController();
					$controllerName = strtolower(get_class($controller));
					$controllerOffset  = strpos($controllerName,"controller");
					$controllerName = substr($controllerName,0,$controllerOffset);
					$remainder = "";
				break;
				case 1:
					$locallizedControllerName = $segments[0];
					$controllerName = $this->getControllerName($locallizedControllerName,  Translator::getLang());
					
					$controller = $this->getController($controllerName);
					
					$remainder = "";

				break;
				default:
					$locallizedControllerName = $segments[0];
					$controllerName = $this->getControllerName($locallizedControllerName,  Translator::getLang());
					$controller = $this->getController($controllerName);
					$remainder = $segments[1];
			}

			if(is_null($controller)){
				$controller = $this->getController("index");
				$controllerName = "index";
				$remainder = "/error/404";
			}

			$controller->handleRequest($remainder,$controllerName);

		}
		

		private $controllerNamesTranslator = null;

		private function getControllerName($localizedName,$lang){
			if($this->controllerNamesTranslator == null){
				$this->controllerNamesTranslator = array();
				$xpathExpression = "./localizedControllerNames/".$lang;
				$localizedControllerNamesLists = $this->configurationData->xpath($xpathExpression);
				if(count($localizedControllerNamesLists)){
					$aliases = (array)$localizedControllerNamesLists[0];
					foreach($aliases as $key => $value){
						if($value)
							$this->controllerNamesTranslator[$value] = $key;
					}
				}
			}
			if(key_exists($localizedName, $this->controllerNamesTranslator)){
				return $this->controllerNamesTranslator[$localizedName];
			}
			return $localizedName;
		}
		
		protected function userHasAccessToController($controllerName){
			return true;
		}
		
		protected function getDefaultController(){
			$nodes = $this->configurationData->xpath("./controller[@default='true']");
			
			if(count($nodes) == 0){
				$nodes = $this->configurationData->xpath("./controller");
			}
			
			if(count($nodes)){
				$attributes = $nodes[0]->attributes();
				$controllerClassName = (string)$attributes["class"];
				if($this->userHasAccessToController($controllerClassName)){
					include($this->getControllerDirectory()."/".$controllerClassName.".php");
					$controller = new $controllerClassName($this->internalLinkBuilder);
					return $controller;
				}else{
					$controllerFileName = $this->getControllerDirectory()."/{$this->loginControllerName}.php";
					require_once($controllerFileName);
					$loginControllerName = $this->loginControllerName;
					$controller = new $loginControllerName($this->internalLinkBuilder);
					return $controller;
				}
			}
			die("No controller found - aborting");
		}
		
		protected function getController($controllerName){
			if($this->userHasAccessToController($controllerName)){
				
				$xpathExpression = "./controller[@name='{$controllerName}']";
				$controllerData = $this->configurationData->xpath($xpathExpression);
				if(count($controllerData) == 0){
					// no controller found
					return null;
				}
				$attributes = $controllerData[0]->attributes();
				$controllerClassName = (string)$attributes["class"];
				
				include($this->getControllerDirectory()."/".$controllerClassName.".php");
				
				$controller = new $controllerClassName($this->internalLinkBuilder);
				return $controller;
			}else{
				$controllerFileName = $this->getControllerDirectory()."/{$this->loginControllerName}.php";
				require_once($controllerFileName);
				$controller = new $this->loginControllerName($this->internalLinkBuilder);
				return $controller;
			}
		}
		
		protected function splitUrl($url){
			if(strlen($url) == 0)
				return array();
			if($url{strlen($url)-1} == "/")
				$url = substr($url,0,strlen($url)-1);
			if(strlen($url) > 0 && $url{0} == "/")
				$url = substr($url,1,strlen($url)-1);
			else if(strlen($url) == 0)
				return array();
			$urlSegments = explode("/",$url,2);
			return $urlSegments;
		}
		
		protected function getControllerDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR ."../Controllers";
		}	
		/**
		 * @param SimpleXMLElement $simpleXMLElement
		 */
		public function setMappingData($simpleXMLElement){
			$this->mappingData = $simpleXMLElement;
		}
		
		protected function translateController($controllerName){
			$controllerName = strtolower($controllerName);
			if(isset($this->controllerMappings[$controllerName]))
				return $this->controllerMappings[$controllerName];
			else{
				header("Status: 404 Not Found"); 
				echo "<h1>Page was not found!</h1>";
				exit();
			}
		}
	}
	
	
