<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/HtmlView.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Models/JsonModel.php");
	class JsonView extends View{
		public function init(){
		}
		
		public function render(){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-Type: application/json; charset=utf-8');
            echo json_encode($this->getModel()->getJsonData());
		}
}