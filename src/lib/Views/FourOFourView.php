<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/HtmlView.php");
	class FourOFourView extends HtmlView{
		
		public function init() {
			header("Status: 404 Not Found");
		}
		
		public function render(){?>
		<h2>Sidan kunde inte hittas</h2>
		<p>
			Tyvärr kunde vi inte hitta den sidan du sökte. Detta beror antagligen på att du skrivit in en felaktig adress, eller på ett sidfel.
		</p>
		<?php }
	}