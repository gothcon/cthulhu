<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class IndexView extends HtmlView{
	/**
	 *
	 * @var Person
	 */
	var $person;
	var $signupList;
	var $groupList;
	var $orderList;
	
	public function init(){
		$this->person				= $this->model->getPerson();
		$this->signupList			= $this->model->getSignupList();
		$this->groupList			= $this->model->getGroupMembershipList();
		$this->orderList			= $this->model->getOrderList();
		$this->user					= $this->model->getUser();
		$this->userLevelSelectData	= $this->model->getUserLevelSelectData();
		$this->returnUrl			= urlencode($this->internalLink("person","{$this->person->id}/#Anmalningar_och_lag",false));
		$this->sleepingSlotBooking  = $this->model->getSleepingSlotBooking();
		$this->availableSleepingResources = $this->model->getAvailableSleepingResources();
	}
	
	public function render(){ 
		$r=1;
		?>
		<div class="section" ng-app="GothConPublic">
			<div class="sectionContent">
				<h4 class="sectionHeader">Detaljer</h4>
				<form method="post" action="<?php $this->internalLink("person", "{$this->person->id}/".CONTROLLER_ACTION_SAVE);?>" class="personForm">
					<fieldset>
						<span class="formBox">
							<?php Html::Label("Förnamn","first_name");?>
							<?php Html::Text("first_name",$this->person->first_name); ?>
							<?php Html::Label("Efternamn","last_name");?>
							<?php Html::Text("last_name",$this->person->last_name); ?>
							<?php Html::Label("Epostadress","email_address");?>
							<span class="data"><?php psafe($this->person->email_address); ?><br/>(ändras under användaruppgifter)</span>
							
							<?php Html::Label("Personnummer","identification");?>
							<?php Html::Text("identification",$this->person->identification); ?>
							
							<?php Html::Label("Gatuadress","street_address");?>
							<?php Html::Text("street_address",$this->person->street_address); ?>

							<?php Html::Label("Postnummer","zip");?>
							<?php Html::Text("zip",$this->person->zip); ?>

							<?php Html::Label("Ort","city");?>
							<?php Html::Text("city",$this->person->city); ?>

							<?php Html::Label("Epostadress &auml;r privat","email_is_public");?>
							<span class="buttonBox">
								<?php Html::Radio("email_is_public",0, !$this->person->email_is_public);?>Ja
								<?php Html::Radio("email_is_public",1, $this->person->email_is_public);?> Nej
							</span>
							<?php Html::Label("Telefonnummer","phone_number");?>
							<?php Html::Text("phone_number",$this->person->phone_number); ?>
						</span>
						<span class="formBox">
							<?php Html::Label("&Ouml;vrig information","note");?>
							<?php Html::Text("note",$this->person->note,true); ?>
						</span>
						<span class="clear"></span>
						<div>
						<?php Html::SubmitButton("Spara");?>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
		<div class="section">
			<h3 class="sectionHeader"><?php echo $this->translate("people/signupsAndGroupMemberships");?></h3>
			<div class="sectionBox">
				<h4>Anmälningar</h4>
				<table class="signupList">
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<th>När</th>
						<th>Vad</th>
						<th>Anmälningstyp</th>
						<th>Not.</th>
						<th class="buttonColumn"></th>
					</tr>
				<?php foreach($this->signupList as $signup){ 
					$timespan = new Timespan();
					$timespan->starts_at = $signup->starts_at;
					$timespan->ends_at = $signup->ends_at;
					
					$status = array();
					
					if($signup->article_id){
						$status[] = $signup->is_paid ? $this->translate("signup/statuses/paid"): $this->translate("signup/statuses/notPaid") ;
					}
					if($signup->requires_approval){
						$status[] = $signup->is_approved ? $this->translate("signup/statuses/approved"): $this->translate("signup/statuses/notApproved") ;
					}
					if(is_numeric($signup->maximum_signup_count) && $signup->maximum_signup_count >= 0 && $signup->signup_order_number > $signup->maximum_signup_count){
						$status[]  = sprintf($this->translate("signup/backupSpotX"), $signup->signup_order_number - $signup->maximum_signup_count);
					}
					if(count($status)){
						$statusAsString = strtolower(implode(", ", $status));
						$statusAsString{0} = strtoupper($statusAsString{0});
					}else{
						$statusAsString = "";
					}
				?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><?php psafe($timespan);?></td>
						<td><?php psafe($signup->event_name)?></td>
						<td><?php echo safe($signup->slot_type_name) . ($signup->group_id > 0 ? " : <span class='teamName'>" . safe($signup->group_name) ."</span>" : "");?></td>
						<td><?php echo safe($statusAsString) ?></td>
						<td class="buttonColumn">
							<?php 
								if($signup->is_signup_owner)
									Html::DeleteButton($this->internalLink("signup", num($signup->id)."/".CONTROLLER_ACTION_DELETE,false),"Ta bort","OBS!!!\\nDetta kommer att ta bort anmälningen\\nKlicka OK för att ta bort anmälningen", array("class"=>"deleteButton"));
							?>
						</td>
					</tr>
				<?php } ?>
				</table>
				<span class="clear"></span>
			</div>
			<div class="sectionBox">
				<h4>Lagmedlemskap</h4>
				<form method="post" action="<?php $this->internalLink("person", CONTROLLER_ACTION_CREATE_GROUP);?>">
				<table id="groupMembershipTable">
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<th>lagnamn</th>
						<th>lagets epostadress</th>
						<th>lagledare?</th>
						<th class="buttonColumn"></th>
					</tr>
				<?php foreach($this->groupList as $membership){	?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->internalLink("group",$membership->group_id); ?>"><?php psafe($membership->getGroup()->name)?></a></td>
						<td><?php psafe($membership->getGroup()->email_address)?></td>
						<td><?php echo $membership->id == $membership->getGroup()->leader_membership_id ? "Ja" : "Nej";?></td>
						<td class="buttonColumn">
							<?php 
								Html::LinkButton($this->translate("groupMembership/leave"), $this->internalLink("person", CONTROLLER_ACTION_LEAVE_GROUP."?group_id=".num($membership->group_id), false));
							?>
						</td>
					</tr>
				<?php } ?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><?php Html::Text("name", "");?></td>
						<td><?php Html::Text("email_address", "");?></td>
						<td></td>
						<td class="buttonColumn">
							<?php 
								Html::SubmitButton("nytt lag");?>
						</td>
					</tr>
				</table>
				</form>
				<span class="clear"></span>
				<?php Html::LinkButton("Gå med i lag", $this->internalLink("group","",false));?>
			</div> 
		</div>
		<div class="section">
			<h3 class="sectionHeader">Användaruppgifter</h3>
			<form method="post" action="<?php $this->internalLink("person", num($this->person->id)."/".CONTROLLER_ACTION_SAVE_USER. "/#".Html::CreateHash($this->translate("people/userDetails")));?>"  class="userForm">
				<fieldset>
					<span class="formBox">
						<label for="username">anv&auml;ndarnamn:</label>
						<?php Html::Text("username",$this->user->username); ?>
						<?php Html::Label($this->translate("user/emailAddress").":","email_address");?>
						<?php Html::Text("email_address",$this->person->email_address); ?>
						<label for="password">nytt l&ouml;sen:</label>
						<?php Html::Password("password"); ?>
						<label for="repeat_password">upprepa nytt l&ouml;sen:</label>
						<?php Html::Password("repeat_password"); ?>
						<span class="clear"></span>
						<?php Html::Hidden("is_active","1"); ?>
						<label for="level">anv&auml;ndarnivå:</label>
						<span class="data"><?php psafe($this->translate("user/userLevels/".  UserLevel::toString($this->user->level))); ?></span>
					</span>
				</fieldset>
				<hr/>
				<label for="current_password">ditt nuvarande l&ouml;sen:</label>
				<?php Html::Password("current_password"); ?>
				
				<?php Html::SubmitButton("Spara",array("style" => "float:right"));?>
				
			</form>
		</div>
		<div class="section">
			<div class="sectionContent">
				<h3 class="sectionHeader"><?php echo Translator::translate("sleepingResource/sleepingResource");?></h3>
				<p class="sleepingSlotBookingInfo">
				Du har för närvarande <?php echo ($this->sleepingSlotBooking ? " bokat <em>".safe($this->sleepingSlotBooking->getSleepingResource()->getResource()->name). "</em> som sovplats" : "inte bokat någon sovplats")?>
				</p>
				<div class="sleepingResourcesContainer">
					<h5>Välj din sovplats</h5>
					<form method="post" action="<?php $this->internalLink("person", CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING);?>" class="userForm">
						<ul class="sleepingResources">
							<li>
								<h6><?php Html::Radio("sleeping_resource_id", -1, !$this->sleepingSlotBooking );?> Sover inte på konventet</h6>
							</li>
							<?php  foreach($this->availableSleepingResources as $resource){ ?>	
							<li>
								<h6><?php Html::Radio("sleeping_resource_id", num($resource->sleeping_resource_id),num($resource->personal_booking_id)  > 0);?> <?php psafe($resource->resource);?></h6>
								<ul>

									<?php foreach($resource->eventOccations as $occation){ ?>
									<li><?php psafe($occation->name);?></li>
									<?php }?>
								</ul>
							</li>
						<?php } ?>
						</ul>
						<span class="clear"></span>
						<?php Html::SubmitButton($this->translate("/general/update"));?>
					</form>	
				</div>
			</div>
		</div>
		<div class="section">
			<h3 class="sectionHeader">Beställningar</h3>
			<?php $r=0;	?>
			<table class="resourceList">
				<tr class="even">
					<th class="idCol">Orderid</th>
					<th class="nameCol">Orderdatum</th>
					<th class="typeCol">Summa</th>
					<th>Status</th>
					<?php  foreach($this->orderList as $order){ ?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td><a href="<?php $this->internalLink("order",$order->id);?>"><?php pnum($order->id)?></a></td>
							<td><a href="<?php $this->internalLink("order",$order->id);?>"><?php psafe($order->submitted_at);?></a></td>
							<td><a href="<?php $this->internalLink("order",$order->id);?>"><?php psafe($order->total);?></a></td>
							<td><a href="<?php $this->internalLink("order",$order->id);?>"><?php psafe($this->translate("order/orderStatus/".$order->status));?></a></td>
						</tr>
					<?php } ?>
			</table>
		</div>
		<?php
	}
}

?>
