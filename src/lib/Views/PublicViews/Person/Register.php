<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class RegisterView extends HtmlView{
	public function init(){
		$this->person = $this->getModel()->getPerson();
	}
	
public function render() {?>
		<div class="section">
			<div class="sectionContent">
				<h2>Registrera dig på GothCon.se</h2>
				<form method="post" action="<?php $this->internalLink("person", CONTROLLER_ACTION_NEW);?>" class="personForm registrationForm">
					<h3>Personuppgifter</h3>
					<fieldset>
						<span class="formBox">
							<?php Html::Label("Förnamn","first_name");?>
							<?php Html::Text("first_name",$this->person->first_name); ?>
							<?php Html::ReqFieldIndicator($this->person->propertyIsInvalid("first_name"));?>
							
							<?php Html::Label("Efternamn","last_name");?>
							<?php Html::Text("last_name",$this->person->last_name,false); ?>
							<?php Html::ReqFieldIndicator($this->person->propertyIsInvalid("last_name"));?>
							
							<?php Html::Label("Epostadress","email_address");?>
							<?php Html::Text("email_address",$this->person->email_address); ?>
							<?php Html::ReqFieldIndicator($this->person->propertyIsInvalid("email_address"));?>
							
							<?php Html::Label("Personnummer","identification");?>
							<?php Html::Text("identification",$this->person->identification); ?>
							
							<?php Html::Label("Gatuadress","street_address");?>
							<?php Html::Text("street_address",$this->person->street_address); ?>

							<?php Html::Label("Postnummer","zip");?>
							<?php Html::Text("zip",$this->person->zip); ?>

							<?php Html::Label("Ort","city");?>
							<?php Html::Text("city",$this->person->city); ?>

							<?php Html::Label("Epostadress &auml;r privat","email_is_public");?>
							<span class="buttonBox">
								<?php Html::Radio("email_is_public",0, !$this->person->email_is_public);?>Ja
								<?php Html::Radio("email_is_public",1, $this->person->email_is_public);?> Nej
							</span>
							<?php Html::Label("Telefonnummer","phone_number");?>
							<?php Html::Text("phone_number",$this->person->phone_number); ?>
							<label for="username"><?php echo $this->translate("user/username")?></label>
							<?php Html::Text("username"); ?>
							<label for="password"><?php echo $this->translate("user/password")?></label>
							<?php Html::Password("password"); ?>
							<label for="repeat_password"><?php echo $this->translate("user/repeatPassword")?></label>
							<?php Html::Password("repeat_password"); ?>
							<span class="clear"></span>
						</span>
						<span class="formBox">
							<?php Html::Label("&Ouml;vrig information","note");?>
							<?php Html::Text("note",$this->person->note,true); ?>
						</span>
						<span class="clear"></span>
						<div>
						<?php Html::SubmitButton("Registrera");?>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
<?php }

}

?>
