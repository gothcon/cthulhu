<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->newsArticles = $this->model->getNewsArticles();
		}
		public function render(){
			$r=0;
			?>
			<ul class="newsArticles">
				<?php foreach($this->newsArticles as $newsArticle){?>
				<li class="html newsContainer <?php echo $r++ % 2 ? "even" : "odd";?>">
					<h2><?php psafe($newsArticle->title);?></h2>
					<div class="author"><?php psafe($newsArticle->publish_at." av ".$newsArticle->author);?></div>
					<div class="preamble"><?php phtml($newsArticle->preamble);?></div>
					<div class="text"><a href="<?php $this->internalLink("news",$newsArticle->id) ?>">Läs hela nyheten...</a></div>
				</li>
				<?php } ?>
			</ul>
			
			
		<?php }
	}