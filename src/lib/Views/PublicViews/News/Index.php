<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class IndexView extends HtmlView{
		public function init(){
			$this->article = $this->model->getNewsArticle();
		}
		public function render(){ ?>
		<div class="newsContainer">
			<h2><?php psafe($this->article->title);?></h2>
			<div class="author"><?php psafe($this->article->publish_at." av ".$this->article->author);?></div>
			<div class="preamble"><?php phtml($this->article->preamble);?></div>
			<div class="text"><?php phtml($this->article->text);?></div>
		</div>
		<?php }
	}