<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		
		/** @var Article **/
		protected $currentCategory;
		
		/** @var array */
		protected $articles;

		/** @var Article[] */
		protected $categories;

		/** @var Article */
		protected $article;
		
		/** @var array */
		protected $path;
		
		/** @var Person */
		protected $currentCustomer;

		/** @var Order */
		protected $cart;

		/** @var array */
		protected $cartItems;

		/** @var bool */
		protected $isLoggedIn;
		
		public function init(){
			// die(print_r($this->model));
			$this->articles = $this->model->getArticles();
			$this->categories = $this->model->getCategories();
			$this->article = $this->model->getArticle();
			$this->path = $this->model->getArticlePath();
			$this->currentCustomer = $this->model->getCurrentCustomer();
			$this->cart = $this->model->getCurrentCart();
			$this->cartItems = $this->cart->getOrderRows();
			$this->isLoggedIn = $this->currentCustomer->id > 0;
		}
		public function render(){?>

		
			<?php if($this->isLoggedIn){?>
			<div id="cart">
				<h3 class="shadedText">Aktuell order</h3>
				<div id="tableContainer">
					<?php $r=0;	?>
					<table id="cartTable">
					<tr class="even">
						<th class="idCol">Art.id</th>
						<th class="nameCol">Namn</th>
						<th class="countCol">Antal</th>
						<th class="pricePerEachCol">Pris per styck</th>
						<th class="totalPriceCol">Totalpris</th>
						<th class="buttonsCol"></th>
					</tr>
					<?php  foreach($this->cartItems as $row){ ?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td class="idCol"><?php psafe($row->article_id);?></td>
							<td class="nameCol"><?php psafe($row->name);?></td>
							<td class="countCol"><?php psafe($row->count);?></td>
							<td class="pricePerEachCol"><?php echo number_format($row->price_per_each,2)?></td>
							<td class="totalPriceCol"><?php echo number_format($row->total,2)?></td>
							<td class="buttonsCol">
								<?php 
								$p = (fromGet('p',false) && is_numeric(fromGet('p') )? "?p=".fromGet('p') : "");
								$link = $this->internalLink(PublicArticleController::getControllerName(), "{$row->article_id}/".CONTROLLER_ACTION_REMOVE_FROM_CART . $p , false);
								Html::DeleteButton($link,$this->translate("general/delete"),$this->translate("articles/deleteArticleWarning"),array("class" => "deleteButton"));
								?>
							</td>
						</tr>
					<?php } ?>
					</table>
				</div>
				<span class="clear"></span>
				<div id="status">
					Antal artiklar i varukorgen: <?php pnum($this->cart->getNumberOfArticles())?> <span id="cartTotal"><span id="cartTotalLabel">Total ordersumma:</span> <?php echo number_format($this->cart->total,2);?></span>
				</div>
				<?php if(count($this->cart->getOrderRows())){?>
				<div id="buttons" style="float:right">
					<?php Html::LinkButton("Gå till \"kassan\"",$this->internalLink("checkout","?step=1", false ) ,array("style" => "float:left; margin-right:14px;"));?>
				</div>
				<?php } ?>
				<span class="clear"></span>
			</div>
			<script type="text/javascript">
				var texts = [];
				texts[true] = "Visa artiklar";
				texts[false] = "Dölj artiklar";
				var cartIsHidden = true;
				var $textSpan = $("<span>"+texts[cartIsHidden] +"<span>");
				$status = $("#status");
				$statusLabel = $("<span id='statusLabel'></span>").html($("#status").html());
				$status.html("").append($statusLabel);
				var $button = $("<a class='editButton buttonLink' href='<?php $this->internalLink("Order");?>' style='float:right; margin-right:10px'></a>").append($textSpan).append($("<span class='end'/>"));
				var $cart = $("#cart #tableContainer");
				
				$button.click(function(event){
					event.preventDefault();
					event.stopPropagation();
					if(cartIsHidden){
						$cart.slideDown();
					}
					else{
						$cart.slideUp();
					}
					cartIsHidden = !cartIsHidden;
					$textSpan.html(texts[cartIsHidden]);
				});
				$cart.hide();
				$("#buttons").append($button);
			</script>
			<?php } ?>
			
			<div id="articlePath">
				<?php 
					$first = true;
					foreach( $this->path as $key => $article){ 
					if($first){?>
						<a href="<?php $this->internalLink("article");?>">Start</a>
					<?php
						$first = false;
					}else{ ?>
						/<a href="<?php $this->internalLink("article","?p=".num($article->id));?>"><?php psafe($article->name);?></a>
				<?php } 
					}
				?>
				
			</div>
			<?php if($this->article->parent){ ?>
			<div class="categoryDescription">
				<h3><?php psafe($this->article->name)?></h3>
				<?php phtml($this->article->description);?>
			</div>
			<?php } ?>
			<?php if(count($this->categories)){ ?>
				<div class="categoriesContainer">
					<?php foreach($this->categories as $category){
						$image = ( $category->getImage()->name ? Application::getFilesRootUrl().$category->getImage()->path.$category->getImage()->name : Application::getFilesRootUrl()."/missingImage_134x134.png");
						$tn = ( $category->getImage()->name ? Application::getFilesRootUrl().$category->getImage()->path.$category->getImage()->name : Application::getFilesRootUrl()."/missingImage_32x32.png");
						// $tn = $category->getImage()->name ? $category->getImage()->thumbnail_small : "" ;
						?>
					<a class="categoryBox" href="<?php echo $this->internalLink("article","?p=".num($category->id),false) ;?>">
						<img src ="<?php psafe($image);?>">
						<span class="categoryName"><?php phtml($category->name);?>
					</a>	
					<?php } ?>
					<span class="clear"></span>
				</div>
			<?php }
			if(count($this->articles)){ ?>
				<h3 class="shadedText">Artiklar</h3>
				<table class="articles">
					<tr>
						<th class="nameCol">Artikelnamn</th>
						<th class="priceCol">Pris</th>
						<th class="addToOrderCol">Lägg till order</th>
					</tr>
					<?php foreach($this->articles as $article){ 
						// $articleLink =  $this->internalLink("article",num($article->id),false);
						$tn = ( $article->getImage()->name ? Application::getFilesRootUrl().$article->getImage()->path.$article->getImage()->getName(32,32) : Application::getFilesRootUrl()."/missingImage_32x32.png");
						?>
					<tr>
						<td class="nameCol"><img src="<?php echo $tn;?>"/> <?php psafe($article->name)?></td>
						<td class="priceCol"><?php echo number_format($article->price,2);?></td>
						<td class="addToOrderCol">
							<?php 
							if($this->isLoggedIn){
								$p = (fromGet('p',false) && is_numeric(fromGet('p') )? "?p=".fromGet('p') : "");
							?>
							<form class="addToCartForm" action="<?php $this->internalLink("article", num($article->id)."/".CONTROLLER_ACTION_ADD_TO_CART . $p );?>" method="post">
								<?php Html::Text("antal","1");?>
								<?php Html::SubmitButton("Lägg till order")?>
							</form>
							<?php } ?>
						</td>
					</tr>
					<?php } ?>
				</table>
				<script type="text/javascript">
					/*
					$(".addToCartForm").ajaxForm({preventBuiltInCallback:true,returnDataType:"html",saveCallback:function($form,data){
						var $response = $(data);
						var $cart = $($response.find("#cartTable"));
						var $currentCart = $("#cartTable");
						$currentCart.html($cart.html());
						var $status = $($response.find("#status"));
						var $currentStatus = $("#statusLabel");
						var $statusButton = $($currentStatus.find(".buttonLink"));
						$currentStatus.html($status.html());
						$currentStatus.append($statusButton);
						$currentCart.find("a.deleteButton").ajaxDelete(options);
					}});

					var deleteCallback = function(data){
						var $response = $(data);
						var $cart = $($response.find("#cartTable"));
						var $currentCart = $("#cartTable");

						$currentCart.html($cart.html());
						var $status = $($response.find("#status"));
						var $currentStatus = $("#statusLabel");
						var $statusButton = $($currentStatus.find(".buttonLink"));
						$currentStatus.html($status.html());
						$currentStatus.append($statusButton);
						$currentCart.find("a.deleteButton").ajaxDelete(options)
					}

					var options = {
						returnDataType:"html",
						deleteCallback:deleteCallback,
						deleteQuestion: "Are you sure?"
					}
				
					$("#cartTable a.deleteButton").ajaxDelete(
						options
					);
					*/
				</script>
		<?php } 
		}
	}