<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");


class ListingView extends HtmlListView{
	public function init(){
		$this->types = $this->model->getEventtypeList();
	}

	public function render(){
		?>
		<div class="row">
			<h1>Arrangemang på konventet</h1>
			Välj arrangemangstyp
			<?php if(!empty($this->types)){?>
			<ul class="event-type-list list-unstyled">
				<?php foreach($this->types  as $type){?>
				<li><a href="<?php $this->internalLink("eventType", num($type->id))?>"><?php psafe($type->name);?></a></li>
				<?php }	?>
			</ul>
			
			Välj format
			<ul class="event-sub-menu list-unstyled" >
				<li><a href="<?php $this->internalLink("pdf","?file=schema");?>" target="_blank">Schema (pdf)</a></li>
				<li><a href="<?php $this->internalLink("pdf","?file=folder");?>" target="_blank">Folder (pdf)</a></li>
			</ul>
			<?php
			}
			?>
		</div>
		<?php
	}
}
?>
