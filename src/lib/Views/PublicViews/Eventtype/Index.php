<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");

class IndexView extends HtmlListView{
	
	/** @var TreeItem_EventType[] */
	private $types;
	
	public function init(){
		$this->types = $this->model->getEventtypeList();
		$this->currentEventtype = $this->model->getCurrentEventtype();
		$this->eventTypes = $this->model->loadEventTree();
		$this->signupSlots = $this->model->getSignupSlotList();
		$this->userGroups = $this->model->getUserGroups();
	}
	
	public function GetGroupsDropdownData(){
		$data = array();
		foreach($this->userGroups as $group){
			$data[] = array("label" => safe($group["g_name"]),"value" => num($group["g_id"]));
		}
		return $data;
	}
	
	public function createEventLink($event){
		return $this->internalLink("event","{$event->id}. {$event->name}",false);
	}
	
	public function createAddSignupFormAction(){
		$returnUrl = urlencode($this->internalLink("eventType", $this->currentEventtype->id,false));
		return $this->internalLink("signup", CONTROLLER_ACTION_NEW."?returnUrl={$returnUrl}", false);
	}
	
	public function printAddSignupFormAction(){
		echo $this->createAddSignupFormAction();
	}

	public function render(){?>
		<div class="row">
			<div class="col-lg-2">
				<h3>Arrangemangstyper</h3>
				<ul class="event-type-list list-unstyled">
					<?php foreach($this->types  as $type){?>
					<li><a href="<?php $this->internalLink("eventType", num($type->id))?>"><?php psafe($type->name);?></a></li>
					<?php }	?>
				</ul>
			</div>
			<div class="events col-lg-10">
				<h2><?php echo $this->currentEventtype->name; ?></h2>
			<?php
			foreach($this->eventTypes as $type){?>
				<?php foreach($type->events as $event){ 
					$signupIsPossible = false;
					?>
					<article>
						<h3><a href="<?php echo $this->createEventLink($event);?>"><?php psafe($event->name)?></a></h3>
						<div class="preamble">
						<?php phtml($event->preamble)?>
						</div>
						<div class="eventText">
						<?php phtml($event->text)?>
						</div>
						<?php if(strlen($event->info) || count($event->event_occations)){ ?>
						<div class="event-info">
							<?php if(strlen($event->info)){?><div class="infoText"><?php phtml($event->info)?></div><?php } ?>
							<?php if (count($event->event_occations)){ 

							?>
							<div>
								<table class="event-occations">
									<tr><th>Tider</th><th>Anmälningsbart för</th></tr>
									<?php foreach($event->event_occations as $eventOccation){ 

										?>
									<tr>	
										<td><?php echo psafe($eventOccation->timespan_as_string)?></td>
										<td>
											<?php foreach($eventOccation->signup_slots as $signupSlot){
												if($signupSlot->slot_status != "notAvailable" && $signupSlot->slot_status != "alreadySignedUp" )
													$signupIsPossible = true;
												?>
											<?php psafe($signupSlot->slot_type_name);?> <?php echo ( $signupSlot->slot_status != "slotAvailable" ? "(".$this->translate("signupSlot/status/".$signupSlot->slot_status).")": "");?><br/>
											<?php } ?>
										</td>
									</tr>
									<?php }?>

								</table>
							</div>
						<?php	}?>	
							<?php if($signupIsPossible && UserAccessor::getCurrentUser()->id > 0){ Html::LinkButton("Anmäl mig",$this->createEventLink($event)); }?>
						</div>
						<?php } ?>
						<div class="center">***</div>
					</article>
				<?php } ?>
				<script>
					$(".teamSignup").each(function(key,obj){
						var $existingTeamForm = $(obj);
						var $teamDropDown = $(obj).find("select");
						var $newTeamForm = $(obj).siblings(".newTeamSignup");
						var $newTeamInput = $newTeamForm.find("[type=text]");

						$newTeamInput.bind("keydown",function(event){
							if(event.keyCode == 27){
								event.preventDefault();
								event.stopPropagation();
								$newTeamForm.hide();
								$existingTeamForm.show();
								$teamDropDown.val($($teamDropDown.find("option")[0]).attr("value"));
							}
						});

						$newTeamInput.bind("blur",function(event){
							if($newTeamInput.val() == "" || $newTeamInput.val() == $newTeamInput[0].defaultValue){
								event.preventDefault();
								event.stopPropagation();
								$newTeamForm.hide();
								$existingTeamForm.show();
								$teamDropDown.val($($teamDropDown.find("option")[0]).attr("value"));
							}
						});

						var onTeamChange = function(){
							if($teamDropDown.val() == -1){
								$existingTeamForm.hide();
								$newTeamForm.show();
								$newTeamInput.focus();
							}
						};
						$teamDropDown.append("<option value='-1'>"+ $.translate("signup/newTeam")+ "</option>");
						$teamDropDown.bind("change",onTeamChange);
						$newTeamForm.hide();
					});
				</script>
				<script>
					//$(".eventOccationList ul").hide();
				</script>
			<?php } ?>
			</div>
		</div>
		<script>
			$("#eventTypeList a").each(function(key,obj){
				var $obj = $(obj);
				if(document.location.pathname.indexOf($obj.attr("href")) === 0 ){
					$obj.addClass("active");
				}
			});
		</script>
<?php
	}
}
?>
