<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class FinalStepView extends HtmlView{
	
	/**
	 *
	 * @var Order
	 */
	protected $cart;
	
	public function init(){
		$this->backLink = $this->internalLink("checkout","?step=2",false);
		$this->cart = PublicCartAccessor::getCurrentCart();
		$this->cartItems = $this->cart->getOrderRows();
		$this->cartStatus = Translator::translate("articles/cartArticleCount").": " . $this->cart->getNumberOfArticles();

                $secondMD5String = md5(
                        Application::GetConfigurationValue("DibsSecret2") 
                        ."merchant=". Application::GetConfigurationValue("DibsMerchantNumber") 
                        ."&orderid=". $this->cart->id  
                        ."&currency=". Application::GetConfigurationValue("DibsCurrency")
                        ."&amount=". $this->cart->total
                );
                
                $this->DibsMD5 = md5( Application::GetConfigurationValue("DibsSecret1") . $secondMD5String );
	}
	public function render(){
		Html::LinkButton($this->translate("checkout/back"), $this->backLink,array("class"=>"previousCheckoutStepButton"));?>
		<div id="cart">
			<table id="cartTable">
			<tr class="even">
				<th class="idCol"><?php echo $this->translate("articles/articleId");?></th>
				<th class="nameCol"><?php echo $this->translate("general/name");?></th>
				<th class="countCol"><?php echo $this->translate("general/count");?></th>
				<th class="pricePerEachCol"><?php echo $this->translate("articles/pricePerEach");?></th>
				<th class="totalPriceCol"><?php echo $this->translate("articles/totalPrice");?></th>
			</tr>
			<?php  foreach($this->cartItems as $row){ 
                                $r = 0;
                            ?>
				<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
					<td class="idCol"><?php pnum($row->article_id);?></td>
					<td class="nameCol"><?php psafe($row->name)?></td>
					<td class="countCol"><?php pnum($row->count)?></td>
					<td class="pricePerEachCol"><?php echo number_format($row->price_per_each,2);?></td>
					<td class="totalPriceCol"><?php echo number_format($row->total,2);?></td>
				</tr>
				<?php if(strlen($row->note)){?>
				<tr class="noteRow">
					<td class="noteLabelCol"><?php echo $this->translate("general/note");?></td>
					<td class="noteCol" colspan="4"><?php echo psafe($row->note);?></td>
				</tr>

			<?php 
				}
			}
			?>
			</table>				
			<span class="clear"></span>
			<div id="status">
				<?php echo $this->cartStatus;?>
				<span id="cartTotal"><span id="cartTotalLabel"><?php echo $this->translate("articles/orderTotal");?></span> <?php echo number_format($this->cart->total,2);?></span>
			</div>
			<h4 class="orderNoteLabel"><?php echo $this->translate("order/orderNote");?></h4>
			<div id="orderNote">
				<?php psafe($this->cart->note);?>
			</div>
		</div>
		<div class="payment">
                    <?php if($this->cart->preferred_payment_method == "wire_transfer"){ ?>
                    <h3>Betalningsinstruktioner: <?php psafe($this->translate("checkout/paymentMethods/".$this->cart->preferredPaymentMethod."/label"));?></h3>
                    <p class="paymentInstructions">
                            Betala till mottagarkonto : <strong><?php echo Application::getConfigurationValue("plusgirokonto"); ?></strong><br/>
                            Ange namn samt beställningsnr <strong>Goth2012#<?php echo $this->cart->id;?></strong><br/>
                            Klicka på "slutför" nedan för att slutföra beställningen.
                    </p>
                    <form method="post">
                    <?php Html::SubmitButton($this->translate("checkout/finalize"),array("class" => "nextCheckoutStepButton"));?>
                    </form>
                    <?php }else if($this->cart->preferred_payment_method == "dibs"){ ?> 
                        <h3>Betalningsinstruktioner: <?php psafe($this->translate("checkout/paymentMethods/".$this->cart->preferred_payment_method."/label"));?></h3>
                        <p class="paymentInstructions">
                                Betalningen sker via DIBS. Klicka på knappen nedan för att betala och slutföra beställningen
                        </p>
                        <form method="post" action="<?php echo Application::GetConfigurationValue("DibsPaymentUrl",true)?>">
                            <?php Html::SubmitButton($this->translate("checkout/finalize"),array("class" => "nextCheckoutStepButton"));?>
                            <?php Html::Hidden("merchant", Application::GetConfigurationValue("DibsMerchantNumber")) ?>
                            <?php Html::Hidden("amount", $this->cart->total * 100) ?>
                            <?php Html::Hidden("accepturl",$this->internalLink("dibsSuccess","",false));?>
                            <?php Html::Hidden("orderid", $this->cart->id) ?>
                            <?php Html::Hidden("currency", Application::GetConfigurationValue("DibsCurrency")) ?>
                            <?php Html::Hidden("aquirerinfo", "") ?>
                            <?php Html::Hidden("lang", Application::GetConfigurationValue("DibsLang")) ?>
                            <?php Html::Hidden("voucher", Application::GetConfigurationValue("DibsAllowVouchers")) ?>
                            <?php Html::Hidden("uniqueoid", $this->cart->id) ?>
                            <?php Html::Hidden("ticketrule", Application::GetConfigurationValue("DibsTickeRule")) ?>
                            <?php if(Application::GetConfigurationValue("DibsTestMode") != "false" ){ 
                               Html::Hidden("test","true");
                            } ?>
                            <?php Html::Hidden("paytype", Application::GetConfigurationValue("DibsPayType")) ?>
                            <?php Html::Hidden("ordertext", "Payment for order #" . $this->cart->id) ?>
                            <?php Html::Hidden("md5key",$this->DibsMD5);?>
                            <?php Html::Hidden("cancelurl",$this->internalLink("dibsCancel","",false));?>
                            <?php Html::Hidden("orderid", $this->cart->id); ?>
                        </form>
                    <?php } ?>
                </div>		
		<?php
	}
}

?>
