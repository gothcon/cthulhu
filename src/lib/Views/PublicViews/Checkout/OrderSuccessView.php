<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class OrderSuccessView extends HtmlView{
	public function init(){
	
	}
	public function render(){?>
		<p>För att få se betalningsinstruktionerna igen, var vänlig välj din order under <a href="<?php $this->internalLink("person","#Bestallningar");?>">"Din sida > Beställningar"</a> där du hittar alla dina ordrar. </p>
		<?php
	}
}

?>
