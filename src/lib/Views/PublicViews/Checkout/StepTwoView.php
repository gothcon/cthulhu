<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class StepTwoView extends HtmlView{
	public function init(){
		$this->cart = $this->model->getCart();
		$this->cartItems = $this->cart->getOrderRows();
		$this->cartStatus = Translator::translate("articles/cartArticleCount").": " . $this->cart->getNumberOfArticles();
		$this->cartTotal = Translator::translate("order/orderTotal").": ". number_format($this->cart->total,2);
		$this->nextLink = $this->internalLink("checkout","?step=final",false);
		$this->backLink = $this->internalLink("checkout","?step=1",false);
	}
	public function render(){
		Html::LinkButton($this->translate("checkout/back"), $this->backLink,array("class"=>"previousCheckoutStepButton"));
		?>
		<p class="stepDescription"></p>
		<?php $r=0;	?>
		<div id="cart">
			<table id="cartTable">
			<tr class="even">
				<th class="idCol"><?php echo $this->translate("articles/articleId");?></th>
				<th class="countCol"><?php echo $this->translate("general/count");?></th>
				<th class="nameCol"><?php echo $this->translate("general/name");?></th>
				<th class="noteCol"><?php echo $this->translate("general/note");?></th>
				<th class="pricePerEachCol"><?php echo $this->translate("articles/pricePerEach");?></th>
				<th class="totalPriceCol"><?php echo $this->translate("articles/totalPrice");?></th>
			</tr>
			<?php  foreach($this->cartItems as $row){ ?>
				<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
					<td class="idCol"><?php pnum($row->article_id);?></td>
					<td class="countCol"><?php pnum($row->count)?></td>
					<td class="nameCol"><?php psafe($row->name)?></td>
					<td class="noteCol"><?php echo psafe($row->note);?></td>
					<td class="pricePerEachCol"><?php echo number_format($row->price_per_each,2);?></td>
					<td class="totalPriceCol"><?php echo number_format($row->total,2);?></td>
				</tr>
			<?php }
			?>
			</table>				
			<span class="clear"></span>
			<div id="status">
				<?php echo $this->cartStatus;?>
				<span id="cartTotal"><span id="cartTotalLabel"><?php echo $this->translate("articles/orderTotal");?></span> <?php echo number_format($this->cart->total,2);?></span>
			</div>
			<h4 class="orderNoteLabel"><?php echo $this->translate("order/orderNote");?></h4>
			<div id="orderNote">
				<?php psafe($this->cart->note);?>
			</div>
		</div>
		<form class="paymentMethodForm" method="post">
		<h3><?php echo $this->translate("checkout/choosePaymentMethod");?></h3>
		<fieldset>
			<?php Html::Radio("payment_method", "wire_transfer", true);?> 
			<?php Html::Label($this->translate("checkout/paymentMethods/wire_transfer/label"),"payment_method");?>
			<div class="information">
				<?php echo $this->translate("checkout/paymentMethods/wire_transfer/information");?>
			</div>
		</fieldset>

		<fieldset>
			<?php Html::Radio("payment_method", "dibs");?> 
			<?php Html::Label($this->translate("checkout/paymentMethods/card/label"),"payment_method");?>
			<div class="information"><?php echo $this->translate("checkout/paymentMethods/card/information");?></div>
			<?php /*
			<div class="additionalFields">
			<?php Html::Label($this->translate("checkout/paymentMethods/card/cardNumber"),"card_number");?>
			<?php Html::Text("cart_number","");?><br/>
			<?php Html::Label($this->translate("checkout/paymentMethods/card/cardCVC"),"cvc");?>
			<?php Html::Text("cvc","");?>
			</div>
			 */ ?>
		</fieldset>

		<span class="clear"></span>
		<?php Html::SubmitButton($this->translate("checkout/next"),array("class" => "nextCheckoutStepButton")); ?>
		</form>
		<?php
		
	}
}

?>
