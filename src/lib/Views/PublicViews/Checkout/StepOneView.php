<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class StepOneView extends HtmlView{
	public function getDeleteFromCartLink($articleId){
		return sprintf($this->deleteFromCartLinkTemplate,num($articleId));
	}
	
	public function init(){
		$this->cart = $this->model->getCart();
		$this->cartItems = $this->cart->getOrderRows();
		$this->cartStatus = Translator::translate("articles/cartArticleCount").": " . $this->cart->getNumberOfArticles();
		$this->cartTotal = Translator::translate("order/orderTotal").": ". number_format($this->cart->total,2);
		$this->deleteFromCartLinkTemplate = $this->internalLink("checkout", "%s/".CONTROLLER_ACTION_REMOVE_FROM_CART . (isset($_GET['p']) ? "?p={$_GET['p']}" : ""), false);
		$this->nextLink = $this->internalLink("checkout","?step=2",false);
		
	}
	public function render(){?>
		<p class="stepDescription"><?php echo $this->translate("checkout/step1Description");?></p>
		<?php $r=0;	?>
		<form action="<?php $this->internalLink("checkout", "?step=1");?>" method="post">
			<div id="cart">
				<table id="cartTable">
					<tr class="even">
						<th class="idCol"><?php echo $this->translate("articles/articleId");?></th>
						<th class="countCol"><?php echo $this->translate("general/count");?></th>
						<th class="nameCol"><?php echo $this->translate("general/name");?></th>
						<th class="noteLabelCol"><?php echo $this->translate("general/note");?></th>
						<th class="pricePerEachCol"><?php echo $this->translate("articles/pricePerEach");?></th>
						<th class="totalPriceCol"><?php echo $this->translate("articles/totalPrice");?></th>
						<th class="buttonsCol"></th>
					</tr>
					<?php  foreach($this->cartItems as $row){ ?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td class="idCol"><?php pnum($row->article_id);?></td>
							<td class="countCol"><?php Html::Text("count[{$row->article_id}]", "{$row->count}", false);?></td>
							<td class="nameCol"><?php psafe($row->name)?></td>
							<td class="noteCol"><?php Html::Text("note[".num($row->article_id)."]", "{$row->note}", true, array("rows" => "1"));?></td>
							<td class="pricePerEachCol"><?php echo number_format($row->price_per_each,2);?></td>
							<td class="totalPriceCol"><?php echo number_format($row->total,2);?></td>
							<td class="buttonsCol"><?php 
								Html::DeleteButton($this->getDeleteFromCartLink($row->article_id),$this->translate("general/delete"),$this->translate("articles/removeFromCartWarning"));
							?>
							</td>
						</tr>
					<?php } ?>
				</table>
				<span class="clear"></span>
				<div id="status">
					<?php echo $this->cartStatus;?>
					<span id="cartTotal"><span id="cartTotalLabel"><?php echo $this->translate("articles/orderTotal");?></span> <?php echo number_format($this->cart->total,2);?></span>
				</div>
				<?php Html::Label($this->translate("order/orderNote"), "orderNote", array("class" => "orderNoteLabel"))?>
				<?php Html::Text("orderNote", $this->cart->note, true, array("rows"=> "4","class" => "orderNote"))?>
				<?php Html::SubmitButton($this->translate("general/update"), array("class" => "updateCartButton"));?>
				<span class="clear"></span>
			</div>
		</form>
		<?php
		Html::LinkButton($this->translate("checkout/next"), $this->nextLink,array("class" => "nextCheckoutStepButton"));
	}
}

?>
