<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class HelpView extends HtmlView{
        private $currentUser;
	public function init(){
            $this->currentUser = UserAccessor::getCurrentUser(); 
	}
	public function render(){?>
		<div class="help">
			<h3>Hur gör jag för att anmäla mig till konventet?</h3>
			<p>
				Använd anmälningsguiden. Du startar den genom att klicka <a href="<?php $this->internalLink("wizard", "?step=0");?>">Här</a> 
			</p>
                        <?php if($this->currentUser->id == 0){?>
			<h3>Jag har glömt mitt lösenord</h3>
			<p>
				Klicka <a class="forgotPasswordLink" href="<?php $this->internalLink("user",CONTROLLER_ACTION_FORGOT_PASSWORD);?>">HÄR</a> för att få ett nytt.
			</p>
                        <?php } ?>
		</div>	
	<?php }
}

?>
