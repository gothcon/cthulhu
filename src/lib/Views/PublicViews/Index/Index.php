<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");;
class IndexView extends HtmlView{
	
	/**
	 *
	 * @var Order
	 */
	protected $cart;
	/**
	 *
	 * @var OrderRow[]
	 */
	protected $cartItems;
	
	public function init(){
		$this->newsArticles = $this->getModel()->getNews();
		$this->cart = $this->getModel()->getCart();
		$this->cartItems = $this->cart->getOrderRows();
	}
	public function render(){

		$r=0;
			?>
			<div class="row">
				<div class="col-lg-10">
					<h2 class="articleHeading">Senaste nytt från GothCon</h2>
					<?php foreach($this->newsArticles as $newsArticle){?>
					<article>
						<h3><?php psafe($newsArticle->title);?></h3>
						<div class="author"><?php psafe($newsArticle->publish_at." av ".$newsArticle->author);?></div>
						<div class="preamble"><?php phtml($newsArticle->preamble);?></div>
						<div class="text"><a href="<?php $this->internalLink("news",$newsArticle->id) ?>">Läs hela nyheten...</a></div>
					</article>
					<?php } ?>
				</div>
				<div class="col-lg-2 cart-container">
					<h3>I din varukorg just nu:</h3>
					<table class="cart">
							<tr><th>Vad</th><th>antal</th><th>kostnad</th></tr>
					<?php foreach ($this->cartItems as $row) {?>
						<tr><td class="nameCol"><?php echo $row->name;?></td><td class="countCol">x <?php echo $row->count?></td><td class="totalPriceCol"><?php echo $row->total;?>:-</td></tr>
					<?php }	?>
					</table>
					<span class="cartTotal">Totalt: <?php echo $this->cart->total;?>:-</span>
					<a href="<?php $this->internalLink("webshop") ?>" class="shop-link">Till shoppen</a>
					<?php if(UserAccessor::getCurrentUser()->person_id != 0){?>
					<a href="<?php $this->internalLink("checkout")?>" class="checkoutLink">Till kassan</a>
					<?php } ?>
				</div>
			</div>
		<?php }
}

?>
