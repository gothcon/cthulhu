<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		
		public function init(){
			$this->groupList = $this->model->getGroupList();
		}
		public function render(){
			?>
			<table id="groupList">
				<tr>
					<th class="idColumn"><?php echo $this->translate("group/id");?></th>
					<th><?php echo $this->translate("group/name");?></th>
					<th><?php echo $this->translate("group/emailAddress");?></th>
					<th><?php echo $this->translate("groupMembership/leader");?></th>
					<th></th>
				</tr>
				<?php foreach($this->groupList as $group){
					
					?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><?php pnum($group->id)?></td>
						<td><a href='<?php $this->internalLink("group",$group->id)?>'><?php psafe($group->name)?></a></td>
						<td><?php psafe($group->email_address)?></td>
						<td><?php psafe($group->leader)?></td>
						<td>
							<?php if(!$group->is_member){
								Html::LinkButton($this->translate("groupMembership/join"), $this->internalLink("group",CONTROLLER_ACTION_JOIN_GROUP."?group_id=".num($group->id),false));
							}else{ 
								echo $this->translate("groupMembership/alreadyMember");
							} ?>
						</td>
					</tr>
				<?php } ?>
			</table>
		<?php }
	
	}
	