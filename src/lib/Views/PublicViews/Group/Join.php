<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class JoinView extends HtmlView{
	public function init(){
		$this->groupList = $this->getModel()->getGroupList();
	}
	public function render(){?>
		
<form action="<?php $this->internalLink("group",CONTROLLER_ACTION_JOIN_GROUP)?>" method="post">
		<fieldset>
			<span class="formBox">
				<?php Html::Label("Grupp", "group_id")?>
				<?php Html::Select("group_id",$this->groupList,fromGet("group_id",-1))?>
				<?php Html::Label("Lösenord","password")?>
				<?php Html::Password("password")?>
			</span>
		</fieldset>
		<?php Html::SubmitButton("Gå med")?>
	</form>
	<?php }
}

?>
