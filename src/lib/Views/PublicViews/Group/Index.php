<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		/** @var User */
		var $currentUser;
		
		/** @var Group */
		var $group;
		
		/** @var GroupMembership[] */
		var $members;
		
		/** @var Signup[] */
		var $signupList;
		
		/** @var GroupMembership */
		var $leader;
		
		var $userIsLeader = false;
		var $userIsMember = false;
		
		public function init(){
			$this->group = $this->model->getGroup();
			$this->members = $this->model->getGroupMembers();
			$this->signupList = $this->model->getSignupList();
			$this->leader = $this->group->getLeaderMembership();
			$this->userIsLeader = $this->model->getIsLeader();
			$this->userIsMember = $this->model->getIsMember();
			$this->currentUser = UserAccessor::getCurrentUser();
		}
		public function render(){?>
		<?php if($this->userIsLeader){ ?>
			<form method="post" id="groupForm" action="<?php $this->internalLink("group","{$this->group->id}");?>">
				<div class="section">
					<h4 class="sectionHeader"><?php echo $this->translate("general/details");?></h4>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("group/name"),"name");?>
							<?php Html::Text("name",$this->group->name); ?>
							<?php Html::Label($this->translate("group/emailAddress"),"email_address");?>
							<?php Html::Text("email_address",$this->group->email_address); ?>
							<?php Html::Label($this->translate("groupMembership/leader"),"");?>
							<?php Html::Text("leader_person_id",$this->group->leader_membership_id ? $this->leader->getPerson() :"",false,array("disabled" => "disabled","id" => "leaderDisplay"));?>
							<?php Html::Label($this->translate("group/newPassword"),"password");?>
							<?php Html::Password("password"); ?>
							<?php Html::Label($this->translate("group/repeatNewPassword"),"repeat_password");?>
							<?php Html::Password("repeat_password"); ?>

						</span>
						<span class="formBox">
							<?php Html::Label($this->translate("group/otherInformation"),"note");?>
							<?php Html::Text("note",$this->group->note,true); ?>
						</span>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</form>
		<?php }else{ ?>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("general/details");?></h4>
				<fieldset>
					<span class="formBox">
						<?php Html::Label($this->translate("group/name"),"name");?>
						<span class="data"><?php psafe($this->group->name)?></span>
						<?php Html::Label($this->translate("group/emailAddress"),"email_address");?>
						<span class="data"><?php psafe($this->group->email_address)?></span>
						<?php Html::Label($this->translate("groupMembership/leader"),"");?>
						<span class="data"><?php psafe($this->group->leader_membership_id ? $this->leader->getPerson() :"")?></span>
					</span>
					<span class="formBox">
						<?php Html::Label($this->translate("group/otherInformation"),"note");?>
						<?php Html::Text("note",$this->group->note,true); ?>
					</span>
				</fieldset>
			</div>
		<?php }	?>
		<?php if($this->group->id >0){ ?>
		<div class="section">
			<h4 class="sectionHeader"><?php echo $this->translate("signup/signups")?></h4>
			<?php $r = 0;?>
			<table class="signupList">
				<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
					<th><?php echo $this->translate("people/signup/when")?></th>
					<th><?php echo $this->translate("people/signup/what")?></th>
					<th><?php echo $this->translate("people/signup/type")?></th>
					<th><?php echo $this->translate("people/signup/note")?></th>
					<th></th>
				</tr>
				<?php foreach($this->signupList as $signup){ 
					$eventOccation = $signup->getSignupSlot()->getEventOccation();
					$event = $eventOccation->getEvent();
					$type = $signup->getSignupSlot()->getSlotType();
					$signupSlot = $signup->getSignupSlot();
					$eventList = $this->internalLink("event","{$event->id}",false);
					$returnUrl = urlencode($this->internalLink("group",$this->group->id."/#".$this->translate("group/signups"),false));
					$deleteSignupLink = $this->internalLink("group",$this->group->id."/".CONTROLLER_ACTION_DELETE_GROUPSIGNUP."?signup_id=".$signup->id,false);
					$note = "";
					if($signupSlot->requires_approval && !$signup->is_approved){
						$note = $this->translate("people/signup/notApproved");
					}
					if($signupSlot->article_id  && !$signup->is_paid){
						if($note){
							$note .= ", ".strtolower($this->translate("people/signup/notPaid"));
						}
						else{
							$note = $this->translate("people/signup/notPaid");
						}
					}
					if($note){
						$note .= ".";
					}
				?>
				<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
					<td><a href="<?php echo $eventList;?>"><?php psafe($eventOccation->getTimespan())?></a></td>
					<td><a href="<?php echo $eventList;?>"><?php psafe($event->name)?></a></td>
					<td><?php psafe($type->name)?></td>
					<td class="noteCol">
						<?php psafe($note) ?>
					</td>	
					<td class="buttonCol"><?php Html::DeleteButton($deleteSignupLink,$this->translate("general/delete"),$this->translate("signup/deleteSignupWarning"), array("class"=>"deleteButton"));?></td>
				</tr>
			<?php } ?>	
			</table>
		</div>
		<div class="section memberships">
			<h4 class="sectionHeader"><?php echo $this->translate("group/members");?></h4>
			<form action="<?php $this->internalLink("group", $this->group->id."/".CONTROLLER_ACTION_UPDATE_LEADER);?>" method="post" id="groupLeaderForm">
				<table id="personList">
					<tr>
						<th class="idColumn"><?php echo $this->translate("people/id");?></th>
						<th><?php echo $this->translate("people/firstName");?></th>
						<th><?php echo $this->translate("people/lastName");?></th>
						<th><?php echo $this->translate("people/emailAddress");?></th>
						<th></th>
					</tr>
					<?php foreach($this->members as $member){
						$person = $member->getPerson();
						$deleteMembershipLink = $this->internalLink("group", num($this->group->id)."/".CONTROLLER_ACTION_DELETE_MEMBERSHIP."?membership_id={$member->id}", false);
					?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td><?php psafe($member->id)?></a></td>
							<td><?php psafe($person->first_name)?></td>
							<td><?php psafe($person->last_name)?></td>
							<td><?php psafe($person->email_address)?></td>
							<td class="buttonCol"><?php 
								if($this->userIsLeader || $person->id == $this->currentUser->person_id){
									$label = $person->id == $this->currentUser->person_id ? $this->translate("groupMembership/leave") : $this->translate("general/delete");
									Html::DeleteButton($deleteMembershipLink,$label,$this->translate("general/areYouSure"), array("class"=>"deleteButton"));
								}
							?></td>
						</tr>
					<?php } ?>
				</table>
			</form>
		</div>
		<?php if($this->userIsLeader){ ?>
		<div class="section">
			<div class="sectionContent">
				<h4 class="sectionHeader"><?php echo $this->translate("group/delete");?></h4>
				<?php 
					$deleteGroupLink = $this->internalLink("group",num($this->group->id)."/".CONTROLLER_ACTION_DELETE,false);
					Html::DeleteButton($deleteGroupLink,$this->translate("group/delete"));?>
			</div>
		</div>
		<?php } ?>
		<script type="text/javascript">
			(function(){
				var personUrlTemplate		= "<?php $this->internalLink("person", "{:person_id}?format=json");?>";
				var deleteUrlTemplate		= "<?php $this->internalLink("group", "/".$this->group->id."/".CONTROLLER_ACTION_DELETE_MEMBERSHIP."?membership_id={:membership_id}?format=json");?>";
				var deleteUrlTemplate_Ajax 	= "<?php $this->internalLink("groupMembership", "{:membership_id}/".CONTROLLER_ACTION_DELETE."?format=json");?>";
				var searchUrl				= "<?php $this->internalLink("search", "?r=people&format=json");?>";
				var createMembershipUrl		= "<?php $this->internalLink("groupMembership", CONTROLLER_ACTION_NEW."?format=json");?>";
				var group_id				= <?php echo $this->group->id;?>;
				var $membershipTable		= $("#personList");
				var entityNodeSelector		= "tr";
				
				function ajaxifyDeleteLink($links){
					$links.each(function(key,link){
						
						var $link = $(link);
						
						var onDeleteSuccess	= function(data){
							$.log(data.message);
							var $row = $link.closest(entityNodeSelector);
							$row.remove();
							
						}
						
						var pattern = /membership_id=(\d+)/g;
						var membershipId = pattern.exec($link.attr("href"))[1];
						var deleteUrl = deleteUrlTemplate_Ajax.replace("{:membership_id}",membershipId);
						
						$link.attr("href",deleteUrl);
						$link.prop("onclick",null);
						
						$link.click(function(event){
							event.preventDefault();
							event.stopPropagation();
							if(confirm("<?php echo $this->translate("groupMembership/deleteWarning")?>")){
								$.ajax({
									url			: deleteUrl,
									dataType	: 'json',
									success		: onDeleteSuccess,
									type		:"post"
								});
							}else{
							}
							return false;
						});

					});
				}
				
				function onGroupMembershipSaveCallback(response){
					if(response.status === 0){
						for(var key in response.data){
							var membership = response.data[key];
							var deleteUrl = deleteUrlTemplate.replace("{:membership_id}",membership.id);
							var $deleteButton = $("<a href='"+deleteUrl+"' class='deleteButton buttonLink'><?php echo $this->translate("general/delete");?><span class='end'></span></a>");
							var $leaderRadioButton = $("<input type='radio' value='"+membership.id+"' name='leader_membership_id'/>");
							var personUrl = personUrlTemplate.replace("{:person_id}",membership.person.id);
							var $tableRow = $("<tr>").appendTo($membershipTable);
							$("<td>").append($("<a href='"+personUrl+"'>"+membership.person.id+"</a>")).appendTo($tableRow);
							$("<td>").append($("<a href='"+personUrl+"'>"+membership.person.first_name+"</a>")).appendTo($tableRow);
							$("<td>").append($("<a href='"+personUrl+"'>"+membership.person.last_name+"</a>")).appendTo($tableRow);
							$("<td>").append($("<a href='"+personUrl+"'>"+membership.person.email_address+"</a>")).appendTo($tableRow);
							$("<td>").append($leaderRadioButton).appendTo($tableRow);
							$("<td>").append($deleteButton).appendTo($tableRow);
							ajaxifyDeleteLink($deleteButton);
							$leaderRadioButton.autoSubmitOnEvent("click");
						}
						$.log(response.message,"info");
					}else{
						$.log(response.message,"error");
					}
				};
				
				ajaxifyDeleteLink($(".memberships .deleteButton"));
				
				$(".memberships").groupMembershipSignup({
					createMembershipUrl			: createMembershipUrl,
					deleteMembershipTemplate	: deleteUrlTemplate,
					searchUrl					: searchUrl,
					group_id					: group_id,
					onSaveCallback				: onGroupMembershipSaveCallback
				});
				
			})()
		</script>
		<span class="clear"></span>
		<?php }
		}
	}