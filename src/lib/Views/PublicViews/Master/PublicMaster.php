<?php 

	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."../../HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."../../../SessionAccessors/UserAccessor.php");
	
	class PublicMaster extends HtmlView{
		
		var $innerView;
		var $model;
		var $currentUser;
		var $currentUsername;
		var $hideTitle;
		
		var $errors = array();
		var $warnings = array();
		var $messages = array();
		
		function __construct($innerView,$hideTitle = false){
			$this->setView($innerView);
			$this->hideTitle = $hideTitle;
		}
		
		function isCurrentPageUrl($url){
			$comparisonString = substr($_SERVER["REQUEST_URI"] , - strlen($url));
			$matches = array();
			$pattern = "/.*:\/\/[^\/]*(\/.*)/";
			if(preg_match($pattern, $url,$matches)){
				return strtolower($_SERVER["REQUEST_URI"]) == strtolower($matches[1]);
			}
			return false;
		}
		
		function setView($innerView){
			$this->innerView = $innerView;
			$this->model = $innerView->getModel();
			$this->setInternalLinkBuilder($this->innerView->getInternalLinkBuilder());
			$this->currentUser = UserAccessor::getCurrentUser(); 
			$this->currentUsername = $this->currentUser->id > 0 ? ("Inloggad som: " .$this->currentUser->username) : "Ej inloggad";
			if(is_a($this->model, "HtmlModel"))
				$this->errors = $this->model->getValidationErrors();
		}
		
		/**
		 *
		 * @var Wizard
		 */
		protected $wizard;
		
		
		function init(){
			$this->myPageUrl = $this->internalLink("person",'',false);
			$this->innerView->init();
			$this->wizard = WizardAccessor::getWizard();
			$this->model->init("startdate","2013-05-04");
			$this->errors = MessageAccessor::getErrorMessages();
			$this->messages = MessageAccessor::getInformationMessages();
			$this->warnings = MessageAccessor::getWarningMessages();

			MessageAccessor::clearAllMessages();
			
		}
		
		function render(){
			$rootUrl =  Application::getConfigurationValue("SiteRoot",true).Application::getConfigurationValue("ApplicationPath",true);
			?>
			<!DOCTYPE html>
				<html lang="en">
				<head>
					<title><?php echo strip_tags($this->model->getTitle());?></title>
					<meta http-equiv="content-type" content="text/html; charset=UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<base href="<?php echo Application::getApplicationUrl();?>/">
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/normalize.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/gothcon-public.css'/>
					<link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog.min.css'/>
					<link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog-theme-default.min.css'/>
					<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
					<!-- Latest compiled and minified CSS -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
					<!-- Optional theme -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
					<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
					<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>						
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.js'></script>
					<!-- Latest compiled and minified JavaScript -->
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/lang/lang.php'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/gothcon_jq_plugins.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.0/moment-with-locales.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/gothcon.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/angular-moment/0.8.0/angular-moment.min.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog.min.js'></script>
					<script type='text/javascript'>
						angular.module("GothCon").config(function(urlsProvider){
							urlsProvider.setApplicationUrl("/<?php echo Application::getApplicationPath();?>");
							// urlsProvider.setTemplatesUrl("resources/javascripts/angular/app/templates/");
							// urlsProvider.setDirectivesUrl("resources/javascripts/angular/app/directives/");
						});
					</script>
					<script type="text/javascript">
						<?php 
						$year = substr($this->model->get("startdate"),0,4);
						$month = substr($this->model->get("startdate"),5,2);
						$day = substr($this->model->get("startdate"),8,2);
						?>
						var defaultDate = new Date("<?php echo "$year,$month,$day";?>");
						$.datepicker.setDefaults({"defaultDate":defaultDate, "dateFormat":'yy-mm-dd', "gotoCurrent": true});
					</script>
				</head>
				<body>
					<div id="wrapper" class="<?php echo get_called_class();?> container-fluid" >
						<div id="ajaxStatusBox"></div>
						<span class="clear"></span>
						<header>
							<nav>
								<a title="Start" href="<?php $this->internalLink("index");?>">
									Start
								</a>
<?php if($this->currentUser->id){?>	
								<a title="Jag"<?php if($this->isCurrentPageUrl($this->myPageUrl)){echo " class=\"active\"";}?> id="yourPageMenuLink" href="<?php echo $this->myPageUrl;?>">Jag</a>
<?php }?>
								<a<?php if($this->isCurrentPageUrl($this->internalLink("eventType","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("eventType");?>" class="<?php if(0) echo "";?>">Arrangemang</a>
								<a<?php if($this->isCurrentPageUrl($this->internalLink("webshop","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("webshop");?>">Shoppen</a>
<?php if($this->currentUser->level == UserLevel::VISITOR){?>	
									<a<?php if($this->isCurrentPageUrl($this->internalLink("person",CONTROLLER_ACTION_NEW,false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("person",CONTROLLER_ACTION_NEW);?>">Registrera dig</a>
<?php }
								else if($this->currentUser->level >= UserLevel::WORKER){?>
									<a href="<?php echo $rootUrl;?>/admin/">Administrera konvent</a>
<?php }	else if($this->currentUser->level >= UserLevel::ORGANIZER){ ?>	
									<a href="<?php echo $rootUrl;?>/arrangor/">Dina arrangemang</a>
<?php }	?>
								<a <?php if($this->isCurrentPageUrl($this->internalLink("help","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("help");?>">HJÄLP!</a>
							</nav>
							<script type='text/javascript'>
								$("nav a").each(function(key,obj){
									var $obj = $(obj);
									if(document.location.pathname.indexOf($obj.attr("href")) === 0 ){
										$obj.addClass("active");
									}
								});
							</script>
							<div id="user-info-box">
								<?php if($this->currentUser->id > 0){?>
								Inloggad som <?php psafe($this->currentUser->username)?> <a href="<?php $this->internalLink("user",CONTROLLER_ACTION_LOGOUT);?>">Logga ut</a>
								<?php }else{?>
									<form action="<?php $this->internalLink("user", CONTROLLER_ACTION_LOGIN."?return_url=".$_SERVER['REQUEST_URI']) ?>" method="post">
										<label>Användarnamn<br/>
										<?php Html::Text("username"); ?>
										</label>
										<label>Lösenord<br/>
										<?php Html::Password("password"); ?>
										</label>
										<?php Html::SubmitButton($this->translate("user/login"));?>
									</form>
							<?php } ?>
							</div>
						</header>
						<div class="content-wrapper">
							<?php $this->innerView->render(); ?>
						</div>
					</div>
				</body>
			</html>
		<?php }
	}
