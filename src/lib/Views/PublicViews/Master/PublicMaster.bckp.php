<?php 

	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."../../HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."../../../SessionAccessors/UserAccessor.php");
	
	class PublicMaster extends HtmlView{
		
		var $innerView;
		var $model;
		var $currentUser;
		var $currentUsername;
		var $hideTitle;
		
		var $errors = array();
		var $warnings = array();
		var $messages = array();
		
		function __construct($innerView,$hideTitle = false){
			$this->setView($innerView);
			$this->hideTitle = $hideTitle;
		}
		
		function isCurrentPageUrl($url){
			$comparisonString = substr($_SERVER["REQUEST_URI"] , - strlen($url));
			$matches = array();
			$pattern = "/.*:\/\/[^\/]*(\/.*)/";
			if(preg_match($pattern, $url,$matches)){
				return strtolower($_SERVER["REQUEST_URI"]) == strtolower($matches[1]);
			}
			return false;
		}
		
		function setView($innerView){
			$this->innerView = $innerView;
			$this->model = $innerView->getModel();
			$this->setInternalLinkBuilder($this->innerView->getInternalLinkBuilder());
			$this->currentUser = UserAccessor::getCurrentUser(); 
			$this->currentUsername = $this->currentUser->id > 0 ? ("Inloggad som: " .$this->currentUser->username) : "Ej inloggad";
			if(is_a($this->model, "HtmlModel"))
				$this->errors = $this->model->getValidationErrors();
		}
		
		/**
		 *
		 * @var Wizard
		 */
		protected $wizard;
		
		
		function init(){
			$this->myPageUrl = $this->internalLink("person",'',false);
			$this->innerView->init();
			$this->wizard = WizardAccessor::getWizard();
			$this->model->init("startdate","2013-05-04");
			$this->errors = MessageAccessor::getErrorMessages();
			$this->messages = MessageAccessor::getInformationMessages();
			$this->warnings = MessageAccessor::getWarningMessages();

			MessageAccessor::clearAllMessages();
			
		}
		
		function render(){
			$rootUrl =  Application::getConfigurationValue("SiteRoot",true).Application::getConfigurationValue("ApplicationPath",true);
			
			echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<title><?php echo strip_tags($this->model->getTitle());?></title>
					<meta http-equiv="content-type" content="text/html; charset=UTF-8">
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/reset.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/public.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/datepicker.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/jQueryUI.css'/>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/lang/lang.php'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.tabbedContainer.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxDelete.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxSelector.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.logger.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxForm.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxSearch.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQueryUI.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.timepicker.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/datepicker-sv.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/DOMWindow.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/cufon.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/SF_Old_Republic_Cufon.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/common.js'></script>
				</head>
				<body>
					<div id="wrapper" class="<?php echo get_called_class();?>">
						<script type="text/javascript">
						<?php 
							$year = substr($this->model->get("startdate"),0,4);
							$month = substr($this->model->get("startdate"),5,2);
							$day = substr($this->model->get("startdate"),8,2);
						?>
							var defaultDate = new Date("<?php echo "$year,$month,$day";?>");
							$.datepicker.setDefaults({"defaultDate":defaultDate, "dateFormat":'yy-mm-dd', "gotoCurrent": true});
						</script>
						<div id="ajaxStatusBox"></div>
						<div id="mainMenuBackground">
						</div>
						<span class="clear"></span>
						<div id="mainContent">
							<div id="mainMenuWrapper">
								<a href="<?php $this->internalLink("index");?>">
								<img id="logo" src="<?php echo $rootUrl;?>/resources/images/logo_lila.png" alt="<?php echo $this->model->getTitle();?>"/>
								</a>
								<ul id="mainMenu">
									<?php if($this->currentUser->id){?>	
									<li>
										<a<?php if($this->isCurrentPageUrl($this->myPageUrl)){echo " class=\"active\"";}?> id="yourPageMenuLink" href="<?php echo $this->myPageUrl;?>">Jag</a>
									</li>
									<?php }?>
									<li>
										<a<?php if($this->isCurrentPageUrl($this->internalLink("eventType","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("eventType");?>" class="<?php if(0) echo "";?>">Arrangemang</a></li>
									<li>
										<a<?php if($this->isCurrentPageUrl($this->internalLink("webshop","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("webshop");?>">Shoppen</a>
									</li>
									<?php if($this->currentUser->level == UserLevel::VISITOR){?>	
									<li>
										<a<?php if($this->isCurrentPageUrl($this->internalLink("person",CONTROLLER_ACTION_NEW,false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("person",CONTROLLER_ACTION_NEW);?>">Registrera dig</a>
									</li>
									<?php }
									else if($this->currentUser->level >= UserLevel::WORKER){
									?>
									<li>
										<a href="<?php echo $rootUrl;?>/admin/">Administrera konvent</a>
									</li>
									<?php }
									else if($this->currentUser->level >= UserLevel::ORGANIZER){
									?>	
									<li>
										<a href="<?php echo $rootUrl;?>/arrangor/">Dina arrangemang</a>
									</li>
									<?php }
									?>
									<li style="float:right" class="helpListItem">
										<a<?php if($this->isCurrentPageUrl($this->internalLink("help","",false))){echo " class=\"active\"";}?> href="<?php $this->internalLink("help");?>">HJÄLP!</a>
									</li>
								</ul>
								<script type='text/javascript'>
									$("#mainMenu a").each(function(key,obj){
										var $obj = $(obj);
										if(document.location.pathname.indexOf($obj.attr("href")) === 0 ){
											$obj.addClass("active");
										}
									});
								</script>
							</div>
							
							<div id="userInfoBox">
								<?php if($this->currentUser->id > 0){?>
								Inloggad som <?php psafe($this->currentUser->username)?> <a href="<?php $this->internalLink("user",CONTROLLER_ACTION_LOGOUT);?>">Logga ut</a>
								<?php }else{?>
									<form action="<?php $this->internalLink("user", CONTROLLER_ACTION_LOGIN."?return_url=".$_SERVER['REQUEST_URI']) ?>" method="post">
										<?php Html::Label("användarnamn", "username",array("class" => "usernameLabel"));?>
										<?php Html::Label("lösenord", "password",array("class" => "passwordLabel"));?>
										<br/>
										<?php Html::Text("username"); ?>
										<?php Html::Password("password"); ?>
										<?php Html::SubmitButton($this->translate("user/login"));?>
									</form>
							<?php } ?>
							</div>
							<div id="mainContentTop"></div>
							<?php if($this->wizard){ ?>
							<div class="wizardContainer">
								<h2><a href="<?php echo $this->wizard->targetUrl;?>"><?php psafe($this->wizard->title)?></a></h2>
								<div class="wizardContentContainer">
									<?php phtml($this->wizard->body)?>
								</div>
								<div class="previousStep navigationContainer"><a href="<?php echo $this->wizard->previousStepUrl;?>"><?php echo $this->wizard->previousStepLabel;?></a></div>
								<div class="nextStep navigationContainer"><a href="<?php echo $this->wizard->nextStepUrl;?>"><?php echo $this->wizard->nextStepLabel;?></a></div>
								<div class="closeWizard navigationContainer"><a href="<?php echo $this->wizard->closeWizardUrl;?>"><?php echo $this->wizard->closeWizardLabel;?></a></div>
							</div>
							<?php } ?>
							<?php if(count($this->errors)){ ?>
								<ul class="errorList">
									<?php foreach($this->errors as $error){ ?>
									<li>
									<?php
										echo $error;
									?>
									</li>
									<?php } ?>
								</ul>
								
							<?php } ?>
							<?php if(count($this->messages)){ ?>
								<ul class="messageList">
									<?php foreach($this->messages as $message){ ?>
									<li>
									<?php
										echo $message;
									?>
									</li>
									<?php } ?>
								</ul>
							<?php } ?>
							<?php if(count($this->warnings)){ ?>
								<ul class="warningsList">
									<?php foreach($this->warnings as $message){ ?>
									<li>
									<?php
										echo $message;
									?>
									</li>
									<?php } ?>
								</ul>
							<?php } ?>
								<span class="clear"></span>
							<?php if(!$this->hideTitle){?>
							<h1><?php echo $this->model->getTitle();?></h1>
							<?php } ?>
							<div id="tabbedContainer">
								<?php $this->innerView->render(); ?>
								<span class="clear"></span>
							</div>
							<script type="text/javascript">
								$("#tabbedContainer").tabbedContainer();
								$(".submitButton").each(function(key,obj){
									var $submitButton = $(obj);
									var $replacement = $("<a href='#' class='buttonLink'>" + $submitButton.val() + "<span class='end'></span></a>").insertAfter($submitButton);
									$replacement.addClass($submitButton.attr("class")).attr("style", $submitButton.attr("style"));
									$replacement.bind("click",function(event){
										event.preventDefault();
										event.stopPropagation();
										$submitButton.trigger("click");
									});
									$submitButton.css({visibility:"hidden", position:"absolute"});
								});
							</script>
						</div>
					</div>
				</body>
			</html>
		<?php }
	}
