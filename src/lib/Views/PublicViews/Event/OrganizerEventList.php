<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");
	class ListingView extends HtmlListView{
		
		public function init(){

		}
		
		public function createEventLink($event){
			$eventName = str_replace(" ","-", num($event->event_id) ."._" . safe($event->event_name) );
			return $this->internalLink("event" , $eventName ,false);
		}
		
		public function createOrganizerLabel($event){
			if($event->type == 'person'){
				$organizer = "<a href='".$this->internalLink("person",$event->entity_id,false)."'>{$event->organizer_name}</a>";
			}
			else if($event->type=='group'){
				$organizer = "<a href='".$this->internalLink("group",$event->entity_id,false)."'>{$event->organizer_name}</a>";
			}
			else{ 
				$organizer = $this->translate("event/unknownOrganizer");
			}
			return $organizer;
		}
		
		public function createEventTypeLink($event){
			return $this->internalLink("eventType","{$event->event_type_id}/".CONTROLLER_ACTION_EDIT,false);
		}
		
		public function createVisibleInPublicListingLabel($event){
			return $event->visible_in_public_listing == 1 ? $this->translate("general/yes") :$this->translate("general/no");
		}
		
		public function createVisibleInScheduleLabel($event){
			 return $event->visible_in_schedule == 1 ? $this->translate("general/yes") :$this->translate("general/no");
		}
		
		public function render(){?>
			<table>
				<tr class="even">
					<th class="idCol">
						<a href="?<?php $this->printHeaderLink("id")?>"><?php echo $this->translate("event/id") ?></a>
					</th>
					<th>
						<a href="?<?php $this->printHeaderLink("name")?>"><?php echo $this->translate("event/eventName")?></a>
					</th>
					<th>
						<a href="?<?php $this->printHeaderLink("type")?>"><?php echo $this->translate("event/eventType")?></a>
					</th>
					<th><?php echo $this->translate("event/visibleInPublicListing");?></th>
					<th><?php echo $this->translate("event/visibleInSchedule");?></th>
					<th>
						<a href="?<?php $this->printHeaderLink("organizer")?>"><?php echo $this->translate("event/organizer")?></a>
					</th>
				</tr>
			<?php
				$listing = $this->model->getEventList();
				$r=0;
				foreach($listing as $event){?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php echo $this->createEventLink($event);?>"><?php pnum($event->event_id)?></a></td>
						<td><a href="<?php echo $this->createEventLink($event);?>"><?php psafe($event->event_name)?></a></td>
						<td><a href="<?php echo $this->createEventTypeLink($event);?>"><?php psafe($event->event_type_name)?></a></td>
						<td><?php echo $this->createVisibleInPublicListingLabel($event);?></td>
						<td><?php echo $this->createVisibleInScheduleLabel($event) ?></td>
						<td><?php echo $this->createOrganizerLabel($event);?></td>
					</tr>
				<?php }?>
			</table>
		<?php }
	}