<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");



class IndexView extends HtmlView{

	/** @var TreeItem_Event */
	public $currentEvent;
	
	/**
	 * 
	 * @return IndexModel
	 */
	public function getModel() {
		return parent::getModel();
	}
	
	public function init(){
		$this->types = $this->getModel()->getEventTypeList();
		$this->currentEvent = $this->model->getCurrentEvent();
		$this->userGroups = $this->model->getUserGroups();
	}

	public function GetGroupsDropdownData(){
		$data = array();
		foreach($this->userGroups as $group){
			$data[] = array("label" => $group["g_name"],"value" => $group["g_id"]);
		}
		return $data;
	}
	
	public function createAddSignupFormAction(){
		$returnUrl = urlencode($this->internalLink("event", $this->currentEvent->id,false));
		return $this->internalLink("signup", CONTROLLER_ACTION_NEW."?returnUrl={$returnUrl}", false);
	}
	
	public function printAddSignupFormAction(){
		echo $this->createAddSignupFormAction();
	}
	
	public function render(){?>
			<h3><?php psafe($this->currentEvent->name)?></h3>
					<div class="preamble">
					<?php phtml($this->currentEvent->preamble)?>
					</div>
					<div class="eventText">
					<?php phtml($this->currentEvent->text);?>
					</div>
					
					<div class="eventInfo">
						<?php if (count($this->currentEvent->event_occations) || strlen($this->currentEvent->info)){ ?>
						<div class="eventOccationBox">
							<?php if(strlen($this->currentEvent->info)){?><div class="infoText"><?php phtml($this->currentEvent->info)?></div><?php } ?>
							<?php if (count($this->currentEvent->event_occations)){ ?>
								<div class="eventOccationBox">
									<table>
										<tr><th>Tider</th><th colspan="2">Anmälningsbart för</th></tr>
										<?php foreach($this->currentEvent->event_occations as $eventOccation){ ?>
										<tr>	
											<td><?php echo psafe($eventOccation->timespan_as_string)?></td>
											<td>
												<?php foreach($eventOccation->signup_slots as $signupSlot){?>
												<div class="slotRow">
												<?php	psafe($signupSlot->slot_type_name);?> <?php echo ( $signupSlot->slot_status != "slotAvailable" ? "(".$this->translate("signupSlot/status/".$signupSlot->slot_status).")": "");?>
													<?php echo $signupSlot->cost > 0 ? "(Kostnad: ".$signupSlot->cost ."kr)" : "";?>
												<?php	if(($signupSlot->slot_status == "slotAvailable" || $signupSlot->slot_status == "spareSlotAvailable") && UserAccessor::getCurrentUser()->id > 0){?>
												<?php		if($signupSlot->slot_type_cardinality == 1){ ?>
																<form method ="post" action="<?php $this->printAddSignupFormAction() ?>" class="personSignup">
												<?php				Html::Hidden("signup_slot_id",$signupSlot->id);?>
												<?php				Html::SubmitButton($this->translate("signup/signUp"), array("class" => "right"));?>
																</form>
												<?php		}else if(count($this->GetGroupsDropdownData())){?>
																<form method ="post" action="<?php $this->printAddSignupFormAction() ?>" class="teamSignup">
												<?php				Html::Hidden("signup_slot_id",$signupSlot->id); ?>
												<?php				Html::SubmitButton($this->translate("signup/signUp"), array("class" => "right")); ?>
												<?php				Html::Select("group_id", $this->GetGroupsDropdownData(),"",array("class"=>"right", "style" => "margin-left:10px"));?>
																</form>
												<?php		}
															if($signupSlot->slot_type_cardinality > 1){ ?>
																<form method ="post" action="<?php $this->printAddSignupFormAction() ?>" class="newTeamSignup">
												<?php				Html::Hidden("signup_slot_id",$signupSlot->id);?>
												<?php				Html::SubmitButton($this->translate("signup/signUp"), array("class" => "right")); ?>
												<?php				Html::Text("group_name","nytt lagnamn...",false,array("style"=>"float:right;")); ?>
																</form>
												<?php		}?>
												<?php	}?>
												<span class="clear"></span>
												</div>
												<?php } ?>
											</td>
										</tr>
										<?php }?>
									</table>
								</div>
							<?php	}?>	
							<script>
								$(".teamSignup").each(function(key,obj){
									var $existingTeamForm = $(obj);
									var $teamDropDown = $(obj).find("select");
									var $newTeamForm = $(obj).siblings(".newTeamSignup");
									var $newTeamInput = $newTeamForm.find("[type=text]");

									$newTeamInput.bind("keydown",function(event){
										if(event.keyCode == 27){
											event.preventDefault();
											event.stopPropagation();
											$newTeamForm.hide();
											$existingTeamForm.show();
											$teamDropDown.val($($teamDropDown.find("option")[0]).attr("value"));
										}
									});

									$newTeamInput.bind("blur",function(event){
										if($newTeamInput.val() == "" || $newTeamInput.val() == $newTeamInput[0].defaultValue){
											event.preventDefault();
											event.stopPropagation();
											$newTeamForm.hide();
											$existingTeamForm.show();
											$teamDropDown.val($($teamDropDown.find("option")[0]).attr("value"));
										}
									});

									var onTeamChange = function(){
										if($teamDropDown.val() == -1){
											$existingTeamForm.hide();
											$newTeamForm.show();
											$newTeamInput.focus();
										}
									};
									$teamDropDown.append("<option value='-1'>"+ $.translate("signup/newTeam")+ "</option>");
									$teamDropDown.bind("change",onTeamChange);
									$newTeamForm.hide();
								});
							</script>
						</div>
					<?php	}?>	
						</div>	
				
	<?php }	
}
?>
