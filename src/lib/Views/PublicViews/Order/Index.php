<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		public function init(){
			$this->order = $this->model->getOrder();
			$this->orderRows = $this->order->getOrderRows();
			$this->orderRowStatusValues = $this->model->getOrderRowStatusValues();
			$this->orderStatusValues = $this->model->getOrderStatusValues();
			$this->orderPayments = $this->model->getOrderPayments();
			$this->remainingSum = $this->model->getRemainingSum();
		}
		public function render(){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/details")?></h4>
					<dl class="orderData">
							<dt><?php echo $this->translate("order/orderTotal")?>:</dt>
								<dd><?php psafe(number_format($this->order->total,2))?>:-</dd>
							<dt><?php echo $this->translate("order/remaining")?>:</dt>
								<dd><?php psafe(number_format($this->remainingSum,2))?>:-</dd>
						<dt><?php echo $this->translate("order/date")?>:</dt>
							<dd><?php psafe($this->order->submitted_at)?></dd>
						<dt><?php echo $this->translate("order/status")?>:</dt>
							<dd><?php psafe($this->translate("order/orderStatus/".$this->order->status))?></dd>
						<dt><?php echo $this->translate("order/deliveryStatus")?>:</dt>
							<dd><?php psafe($this->translate("order/orderStatus/".$this->order->delivery_status))?></dd>
					</dl>
					<span class="clear"></span>
				</div>
			</div>
			<div class="section">
				<h4 class="sectionHeader">Betalningar och betalningsinstruktioner</h4>
				<?php $r=0;	
				$totalSum = 0;
				?>
				<table class="transactionList">
					<tr class="even">
						<th><?php echo $this->translate("transaction/transactionId");?></th>
						<th><?php echo $this->translate("transaction/time");?></th>
						<th><?php echo $this->translate("transaction/paymentMethod");?></th>
						<th><?php echo $this->translate("transaction/amount");?></th>
					<?php  foreach($this->orderPayments as $orderPayment){ 
						$transaction = $orderPayment->transaction;
						$totalSum += $transaction->total;
						?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd"; ?>">
							<td><?php psafe($transaction->id)?></td>
							<td><?php psafe($transaction->created_at)?></td>
							<td><?php psafe($this->translate("transaction/paymentMethods/".$transaction->payment_method))?></td>
							<td><?php psafe($transaction->amount)?></td>
						</tr>
					<?php } ?>
				</table>
				<div class="payment">
					<h3><?php psafe($this->translate("checkout/paymentMethods/".$this->order->preferredPaymentMethod."/label"),"payment_method");?></h3>
					<p class="paymentInstructions">
						Betala till mottagarkonto: <strong><?php echo Application::getConfigurationValue("plusgirokonto"); ?></strong><br/>
						Ange namn samt beställningsnr: <strong>Goth2012#<?php echo $this->order->id;?></strong><br/>
					</p>
				</div>	
			</div>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("order/comment")?></h4>
				<p class="orderNote">
					<?php psafe($this->order->note); ?>
				</p>
			</div>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("order/orderRows");?></h4>
				<?php $r=0;	?>

				<table class="orderRows">
					<tr class="even">
						<th class="articleIdCol"><?php echo $this->translate("order/orderRow/articleId");?></th>
						<th class="nameCol"><?php echo $this->translate("order/orderRow/articleName");?></th>
						<th class="countCol"><?php echo $this->translate("order/orderRow/articleCount");?></th>
						<th class="pricePerEachCol"><?php echo $this->translate("order/orderRow/pricePerEach");?></th>
						<th class="totalPriceCol"><?php echo $this->translate("order/orderRow/totalPrice");?></th>
						<th class="deliveryStatusCol"><?php echo $this->translate("order/orderRow/deliveryStatus");?></th>
						<th class="orderRowNoteCol"><?php echo $this->translate("order/orderRow/note");?></th>
					<?php  foreach($this->orderRows as $row){ 
						$class = $r++ % 2 ? "even" : "odd";
							if($row->status == "cancelled"){
								$class .=" cancelled";
							}
							?>
						<tr class="<?php echo $class?>">
							<td class="articleIdCol"><?php psafe($row->article_id)?></td>
							<td class="nameCol"><?php psafe($row->name)?></td>
							<td class="countCol"><?php pnum($row->count)?></td>
							<td class="pricePerEachCol"><?php psafe(number_format($row->price_per_each,2))?></td>
							<td class="totalPriceCol"><?php psafe(number_format($row->total,2))?></td>
							<td class="deliveryStatusCol"><?php phtml($this->translate("order/orderRowStatus/{$row->status}"))?></td>
							<td class="orderRowNoteCol"><?php phtml($row->note)?></td>
						</tr>
					<?php } ?>
				</table>

			</div>
		<?php }
	}