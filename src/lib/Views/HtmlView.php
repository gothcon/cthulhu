<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlView
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."View.php");
abstract class HtmlView extends View{
	
	private $linkBuilder;
	
	public function setInternalLinkBuilder($linkBuilder){
		$this->linkBuilder = $linkBuilder;
	}

	public function getInternalLinkBuilder(){
		return $this->linkBuilder;
	}
	
	public function internalLink($ControllerName,$parameters="",$output=true){
		//TODO: FIX THIS
		if(!$this->linkBuilder){
			die("no link builder set");
			// $link = "/{$ControllerName}/{$parameters}";
		}else{
			$link = $this->linkBuilder->internalLink($ControllerName,$parameters);
		}
		if($output){
			echo $link;
			return true;
		}else{
			return $link;
		}
	}
	
	/** @return HtmlModel */
	public function getModel() {
		return parent::getModel();
	}
	
}

?>
