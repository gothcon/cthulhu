<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomTCPDF
 *
 * @author Joakim
 */
require_once("lib/tcpdf/tcpdf.php");
class TCPDFView extends TCPDF implements IView{
	protected $model;
	
	public function setModel(&$model){
		$this->model = $model;
	}
	
	public function getModel(){
		return $this->model;
	}
	
	public function translate($string,$lang="sv_SE"){
		return Translator::translate($string,$lang);
	}
	
	public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
		$this->SetMargins(10, 20);
		$this->setFooterMargin(6);
		$this->setHeaderMargin(6);
		
		// $this->AddFont('papyrus');
		$headerFont = array("DejaVuSans",'',11);
		$footerFont = array("DejaVuSans",'',9);
		$this->AddFont("DejaVuSerif");
		$this->AddFont("DejaVuSans");
		$this->SetHeaderFont($headerFont);
		$this->SetFooterFont($footerFont);
		
		$this->SetDisplayMode(DisplayMode::ZOOM_FULLPAGE, DisplayMode::LAYOUT_SINGLE_PAGE, DisplayMode::MODE_USE_NONE);
		//$this->AddFont("DejaVuSerif","-Italic");
		//$this->AddFont("DejaVuSerif","-Bold");
		$this->SetCompression(false);
	}
	
	public function init(){
		$this->SetFieldData(Field::LEFT_HEADER_FIELD,  "GothCon 2013 - ".$this->getModel()->getTitle());
		$this->SetFieldData(Field::CENTER_HEADER_FIELD, "");
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, "");
	}
	
	public function render(){
		$this->Output();
	}
	public function HideHeader(){
		$this->setPrintHeader(false);
	}
	
	public function ShowHeader(){
		$this->setPrintHeader(true);
	}
	
	public function HideFooter(){
		$this->setPrintFooter(false);
	}
	
	public function ShowFooter(){
		$this->setPrintFooter(true);
	}
	
	public function Footer() {
		$cur_y = $this->y;
		$this->SetTextColorArray($this->footer_text_color);
		//set style for cell border
		$line_width = (0.85 / $this->k);
		$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
		
		$w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
		if (empty($this->pagegroups)) {
			$pagenumtxt = $w_page.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
		} else {
			$pagenumtxt = $w_page.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
		}
		$this->SetY($cur_y);
		//Print page number
		if ($this->getRTL()) {
			$this->SetX($this->original_rMargin);
			$this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
		} else {
			$this->SetX($this->original_lMargin);
			$this->Cell(0, 0, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
		}
	}
	
	protected function getAvailableWidth(){
		return $this->getPageWidth() - ($this->lMargin + $this->rMargin);
	}
	
		
	protected function getAvailableHeight(){
		return $this->getPageHeight() - ($this->tMargin + $this->bMargin);
	}
	
	private $_fieldData = array(
		Field::LEFT_HEADER_FIELD => "",
		Field::CENTER_HEADER_FIELD => "",
		Field::RIGHT_HEADER_FIELD => "",
		//	
		//Field::LEFT_FOOTER_FIELD => "",
		//Field::CENTER_FOOTER_FIELD => "",
		//Field::RIGHT_FOOTER_FIELD => ""
	);
	
	public function SetFieldData($field,$data){
		switch($field){
			case Field::LEFT_HEADER_FIELD:
			case Field::CENTER_HEADER_FIELD:
			case Field::RIGHT_HEADER_FIELD:
			case Field::LEFT_FOOTER_FIELD:
			case Field::CENTER_FOOTER_FIELD:
			case Field::RIGHT_FOOTER_FIELD:
				break;
			default:
				die("Unknown field");
		}
		$this->_fieldData[$field] = $data;
	}
	
	
	private function AdaptHeaderCellsToAvailableWidth(&$leftCellWidth,&$centerCellWidth,&$rightCellWidth,$availableSpace,$cellSpacing){
		$availableSpace -= ($cellSpacing*2);
		
		/* Scaling the cells according to required space */
//		$outerCellsMaxWidth = max($leftCellWidth,$rightCellWidth);
//		if($outerCellsMaxWidth * 2 + $centerCellWidth){
//			$k =  $availableSpace / ($outerCellsMaxWidth * 2 + $centerCellWidth);
//			$leftCellWidth = $outerCellsMaxWidth*$k;
//			$rightCellWidth = $outerCellsMaxWidth * $k;
//			$centerCellWidth *= $k;
//		}
		
		/* never scale middle cell and stretch/compress left and right cell */
		$leftCellWidth = ($leftCellWidth > (($availableSpace - $centerCellWidth)/2)) ? (($availableSpace - $centerCellWidth)/2) : $leftCellWidth;
		$rightCellWidth = ($rightCellWidth > (($availableSpace - $centerCellWidth)/2)) ? (($availableSpace - $centerCellWidth)/2) : $rightCellWidth;
	}
	
	public function Header() {

		if ($this->header_xobjid < 0) {
			// start a new XObject Template
			$this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
			$headerfont = $this->getHeaderFont();
			
			$fields = array(Field::LEFT_HEADER_FIELD,  Field::CENTER_HEADER_FIELD, Field::RIGHT_HEADER_FIELD);
			
			$this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);

			// set X
			$this->x = $this->original_lMargin;
			// set Y
			$this->y = $this->header_margin;
			// cell height
			$cell_height = round(($this->cell_height_ratio * $headerfont[2]) / $this->k, 2);
			// cell width
			$cw = $this->getPageWidth() - $this->original_lMargin - $this->original_rMargin;
			
			// half the available width 
			$strwidth_left_field	= $this->getStringWidth($this->_fieldData[Field::LEFT_HEADER_FIELD]);
			$strwidth_center_field	= $this->GetStringWidth($this->_fieldData[Field::CENTER_HEADER_FIELD]);
			$strwidth_right_field	= $this->GetStringWidth($this->_fieldData[Field::RIGHT_HEADER_FIELD]);
			$cellSpacing = 4;
			
			// die($this->getPageWidth());
			
			$this->AdaptHeaderCellsToAvailableWidth($strwidth_left_field,$strwidth_center_field,$strwidth_right_field,$cw,$cellSpacing);
			
			// set background cell color
			$this->SetFillColor(230);
			
			// print background cell
			$this->Cell($cw, $cell_height, '', 0, 1, '' , true);
			
			$this->SetTextColorArray($this->header_text_color);
			
			if ($this->rtl) {
				$this->x = $this->w - $this->original_rMargin;
			} else {
				$this->x = $this->original_lMargin;
			}
			
			for($i = 0; $i < 3 ; $i++){
				// setting y
				$this->y = $this->header_margin;
			
				$fieldToPrint = $fields[$i];
				$stringToPrint = $this->_fieldData[$fieldToPrint];
				
				$this->y = $this->header_margin;
				$y = $this->header_margin;
				switch($i){
					case 0:
						$this->SetX($this->original_lMargin);
						$this->Cell($strwidth_left_field, 0, $stringToPrint,0, 0,Alignment::LEFT,false,'',1);
						break;
					case 1:
						$this->SetX(($this->getPageWidth() - $strwidth_center_field)/ 2);
						$this->Cell($strwidth_center_field, 0, $stringToPrint, 0, 0,Alignment::LEFT,true,'',1);
						break;
					case 2:
						$this->SetX(0 - $strwidth_right_field - $this->original_rMargin);
						$this->Cell($strwidth_right_field, 0, $stringToPrint, 0, 0,Alignment::LEFT,false,'',1);
						break;
				}
				
			}
			if ($this->rtl) {
				$this->SetX($this->original_rMargin);
			} else {
				$this->SetX($this->original_lMargin);
			}
			$this->endTemplate();
		}
		// print header template
		$x = 0;
		$dx = 0;
		if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
			// adjust margins for booklet mode
			$dx = ($this->original_lMargin - $this->original_rMargin);
		}
		if ($this->rtl) {
			$x = $this->w + $dx;
		} else {
			$x = 0 + $dx;
		}
		$this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
		if ($this->header_xobj_autoreset) {
			// reset header xobject template at each page
			$this->header_xobjid = -1;
		}
	}
	
	public function MoveRight($distance){
		$this->SetX($this->getX() + $distance,false);
	}

	public function MoveLeft($distance){
		$this->SetX($this->getX() - $distance);
	}
	
}

class Field{
	const LEFT_HEADER_FIELD		= 1; 
	const CENTER_HEADER_FIELD	= 2; 
	const RIGHT_HEADER_FIELD	= 3; 
	// const LEFT_FOOTER_FIELD		= 4; 
	// const CENTER_FOOTER_FIELD	= 5; 
	// const RIGHT_FOOTER_FIELD	= 6; 
}

class SpecialFieldValue{
	const PAGENUMBER = 0;
}

class FontStyle{
	const NORMAL = '';
	const BOLD = 'B';
	const UNDERLINED = 'U';
	const ITALIC = 'I';
	const BOLD_ITALIC = 'BI';
}

class Margin{
	const LEFT_MARGIN			= 'left';
	const RIGHT_MARGIN			= 'right';
	const TOP_MARGIN			= 'top';
	const BOTTOM_MARGIN			= 'bottom';
	const HEADER_MARGIN			= 'header';
	const FOOTER_MARGIN			= 'footer';
	const CELL_PADDING			= 'cell';
	const CELL_PADDING_LEFT		= 'padding_left';
	const CELL_PADDING_RIGHT	= 'padding_right';
	const CELL_PADDING_TOP		= 'padding_top';
	const CELL_PADDING_BOTTOM	= 'padding_bottom';
}

class Alignment{
	const LEFT = 'L';
	const CENTER = 'C';
	const RIGHT = 'R';
	const JUSTIFY = 'J';
	const TOP = 'T';
	const BOTTOM = 'B';
}

class DisplayMode{
	const ZOOM_FULLPAGE				= 'fullpage';
	const ZOOM_FULLWIDTH			= 'fullwidth';
	const ZOOM_REAL					= 'real';
	const ZOOM_DEFAULT				= 'default';
	const LAYOUT_DEFAULT			= 'default';
	const LAYOUT_SINGLE_PAGE		= 'SinglePage';
	const LAYOUT_ONE_COLUMN			= 'OneColumn';
	const LAYOUT_TWO_COLUMN_LEFT	= 'TwoColumnLeft';
	const LAYOUT_TWO_COLUMN_RIGHT	= 'TwoColumnRight';
	const LAYOUT_TWO_PAGE_LEFT		= 'TwoPageLeft';
	const LAYOUT_TWO_PAGE_RIGHT		= 'TwoPageRight';
	const MODE_USE_NONE				= 'UseNone';
	const MODE_USE_THUMBS			= 'UseThumbs';
	const MODE_USE_OUTLINES			= 'UseOutlines';
	const MODE_USE_FULLSCREEN		= 'FullScreen';
	const MODE_USE_OC				= 'UserOC';
	const MODE_USE_ATTACHMENT		= '';
}
?>
