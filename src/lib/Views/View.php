<?php 
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."/IView.php");
	require_once("lib/ClassLib/Global.php");
	
	abstract class View implements IView{

	protected $model;



	public function setModel(&$model){
		$this->model = $model;
	}
	
	public function getModel(){
		return $this->model;
	}
	
	public function translate($string,$lang="sv_SE"){
		return Translator::translate($string,$lang);
	}
	
	
	
	
}


	