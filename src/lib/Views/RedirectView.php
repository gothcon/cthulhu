<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."HtmlView.php");

class RedirectView extends View{
	
	protected $redirectDestination;

	public function __construct($redirect_to = ""){
		$this->redirectDestination = $redirect_to;
	}
	
	public function setRedirectDestination($redirectDestination){
		$this->redirectDestination = $redirectDestination;
	}
	
	public function init(){
		
		
	}
	public function render(){
		header("location:".$this->redirectDestination);
	}
}