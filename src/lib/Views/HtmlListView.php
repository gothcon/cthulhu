<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlListView
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."HtmlView.php");

abstract class HtmlListView extends HtmlView{
	public function getHeaderLink($property,$parameter_prefix="sort_"){
		$sortProperties = $this->model->getSortParameters();
		$sortProperty = isset($sortProperties["property"]) ? $sortProperties["property"] :"";
		$sortDirection = isset($sortProperties["direction"]) ? $sortProperties["direction"] :"";
		$sortOffset = isset($sortProperties["offset"]) ? $sortProperties["offset"] :"";
		$sortCount = isset($sortProperties["count"]) ? $sortProperties["count"] :"";

		$headerLink = "{$parameter_prefix}property={$property}";
			if($sortProperty == $property)
				$headerLink.= "&{$parameter_prefix}direction=". ($sortDirection == "asc" ? "desc" : "asc");
		return $headerLink;
	}
	
	public function printHeaderLink($property,$parameter_prefix="sort_"){
		echo $this->getHeaderLink($property, $parameter_prefix);
	}
	
}

?>
