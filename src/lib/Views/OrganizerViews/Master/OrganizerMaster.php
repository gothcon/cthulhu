<?php 

	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");
	require_once("lib/ClassLib/Html.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../SessionAccessors/UserAccessor.php");
	
	
	class OrganizerMaster extends HtmlView{
		
		function __construct($innerView){
			$this->innerView = $innerView;
			$this->model = $innerView->getModel();
			$this->setInternalLinkBuilder($this->innerView->getInternalLinkBuilder());
			$this->currentUser = UserAccessor::getCurrentUser(); 
			$this->currentUsername = $this->currentUser->id > 0 ? ("Inloggad som: " .$this->currentUser->username) : "Ej inloggad";
		}
		
		protected $showWizard = false;
		protected $wizardTitle = "";
		protected $wizardContent = "";
		
		public function showWizard($title,$content){
			$this->showWizard = true;
			$this->wizardTitle = $title;
			$this->wizardContent = $content;
		}
		
		public function hideWizard(){
			$this->showWizard = false;
		}
		
		function init(){
			$this->myPageUrl = $this->internalLink("person", $this->currentUser->person_id,false);
			$this->innerView->init();
			$this->model->init("startdate","2013-05-04");
		}
		
		function render(){
			$rootUrl =  Application::getConfigurationValue("SiteRoot",true).Application::getConfigurationValue("ApplicationPath",true);
			
			echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<title><?php echo $this->model->getTitle();?></title>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/reset.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/public.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/datepicker.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/jQueryUI.css'/>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/lang/lang.php'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.tabbedContainer.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxDelete.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxSelector.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.logger.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxForm.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.ajaxSearch.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQueryUI.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.timepicker.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/datepicker-sv.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/DOMWindow.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/cufon.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/SF_Old_Republic_Cufon.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/common.js'></script>
				</head>
				<body>
					<div id="wrapper" class="<?php echo get_called_class();?>">
						<script type="text/javascript">
						<?php 
							$year = substr($this->model->get("startdate"),0,4);
							$month = substr($this->model->get("startdate"),5,2);
							$day = substr($this->model->get("startdate"),8,2);
						?>
							var defaultDate = new Date("<?php echo "$year,$month,$day";?>");
							$.datepicker.setDefaults({"defaultDate":defaultDate, "dateFormat":'yy-mm-dd', "gotoCurrent": true});
						</script>
						<div id="ajaxStatusBox"></div>
						<div id="mainMenuBackground">
						</div>
						<span class="clear"></span>
						<div id="mainContent">
							<div id="mainMenuWrapper">
								<a href="<?php  echo $rootUrl ;?>">
								<img id="logo" src="<?php echo $rootUrl;?>/resources/images/logo_organizer.png" alt="<?php echo $this->model->getTitle();?>"/>
								</a>
								<ul id="mainMenu">
									<li><a id="yourPageMenuLink" href="<?php $this->internalLink("event");?>">Dina arrangemang</a></li>
									<li><a href="<?php echo $rootUrl;?>/">Till anmälningssidan</a></li>
									<?php if(!$this->currentUser->id){?>	
									<li><a href="<?php $this->internalLink("user",CONTROLLER_ACTION_LOGIN);?>">Logga in</a></li>
									<?php } ?>
								</ul>
								<script type='text/javascript'>
									$("#mainMenu a").each(function(key,obj){
										var $obj = $(obj);
										if(document.location.pathname.indexOf($obj.attr("href")) === 0 ){
											$obj.addClass("active");
										}
									});
								</script>
							</div>
							
							<div id="userInfoBox">
								<?php if($this->currentUser->id > 0){?>
								Inloggad som <?php psafe($this->currentUser->username)?> <a href="<?php $this->internalLink("user",CONTROLLER_ACTION_LOGOUT);?>">Logga ut</a>
								<?php }else{?>
									<form action="<?php $this->internalLink("user", CONTROLLER_ACTION_LOGIN."?return_url=".$_SERVER['REQUEST_URI']) ?>" method="post">
										<?php Html::Label("användarnamn", "username",array("class" => "usernameLabel"));?>
										<?php Html::Label("lösenord", "password",array("class" => "passwordLabel"));?>
										<br/>
										<?php Html::Text("username"); ?>
										<?php Html::Password("password"); ?>
										<?php Html::SubmitButton($this->translate("user/login"));?>
									</form>
							<?php } ?>
							</div>
							
							<div id="mainContentTop"></div>
							<?php if($this->showWizard){ ?>
							<div class="wizardContainer">
								<h2><?php echo $this->wizardTitle?></h2>
								<div class="wizardContentContainer">
									<?php echo $this->wizardContent?>
								</div>
							</div>
							<?php } ?>
							<?php if(0){ ?>
								<ul class="messageList">
									<li>
										<pre>
									<?php
										print_r($this->model);
										// print_r($_POST);
										// print_r($_SERVER);
									?>
									</pre>
									</li>
								</ul>
								
							<?php } ?>
								<span class="clear"></span>
							<h1 class="organizerPageTitle"><?php Html::PrintText($this->model->getTitle());?></h1>
							<div id="tabbedContainer">
								<?php $this->innerView->render(); ?>
								<span class="clear"></span>
							</div>
							<script type="text/javascript">
								$("#tabbedContainer").tabbedContainer();
								$(".submitButton").each(function(key,obj){
									var $submitButton = $(obj);
									var $replacement = $("<a href='#' class='buttonLink'>" + $submitButton.val() + "<span class='end'></span></a>").insertAfter($submitButton);
									$replacement.addClass($submitButton.attr("class")).attr("style", $submitButton.attr("style"));
									$replacement.bind("click",function(event){
										event.preventDefault();
										event.stopPropagation();
										$submitButton.trigger("click");
									});
									$submitButton.css({visibility:"hidden", position:"absolute"});
								});
							</script>
						</div>
					</div>
				</body>
			</html>
		<?php }
	}