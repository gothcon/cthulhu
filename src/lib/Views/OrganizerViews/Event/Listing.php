<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Models/HtmlModel.php");
	class ListingView extends HtmlListView{

		protected function getEventLink($event){
			return $this->internalLink("event",str_replace(" ","-","{$event->event_id}._{$event->event_name}"),false);
		}
		
		public function init(){
		}
		
		public function render(){?>
					<table>
						<tr class="even">
							<th class="idCol"><?php psafe($this->translate("event/id")) ?></th>
							<th><?php psafe($this->translate("event/eventName"))?></th>
							<th><?php psafe($this->translate("event/eventType"))?></th>
							<th><?php psafe($this->translate("event/visibleInPublicListing"))?></th>
							<th><?php psafe($this->translate("event/visibleInSchedule"))?></th>
							<th><?php echo $this->translate("event/organizer")?></th>
						</tr>
						<?php /**
						<tr class="even">
							<th class="idCol"><a href="?<?php $this->printHeaderLink("id");?>"><?php psafe($this->translate("event/id")) ?></a></th>
							<th><a href="?<?php $this->printHeaderLink("name");?>"><?php psafe($this->translate("event/eventName"))?></a></th>
							<th><a href="?<?php $this->printHeaderLink("type");?>"><?php psafe($this->translate("event/eventType"))?></a></th>
							<th><?php psafe($this->translate("event/visibleInPublicListing"))?></th>
							<th><?php psafe($this->translate("event/visibleInSchedule"))?></th>
							<th><a href="?<?php $this->printHeaderLink("organizer");?>"><?php echo $this->translate("event/organizer")?></a></th>
						</tr>
						<?php
						 * **/
						$listing = $this->model->getEventList();
						$r=0;
						foreach($listing as $event){
							if($event->type == 'person' || $event->type=='group'){
								$organizer = $event->organizer_name;
							}
							else{ 
								$organizer = $this->translate("event/unknownOrganizer");
							}
							?>
							<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
								<td><a href="<?php echo $this->getEventLink($event);?>"><?php psafe($event->event_id);?></a></td>
								<td><a href="<?php echo $this->getEventLink($event);?>"><?php psafe($event->event_name);?></a></td>
								<td><?php psafe($event->event_type_name)?></td>
								<td><?php psafe($event->visible_in_public_listing == 1 ? $this->translate("general/yes") :$this->translate("general/no"))?></td>
								<td><?php psafe($event->visible_in_schedule == 1 ? $this->translate("general/yes") :$this->translate("general/no"))?></td>
								<td><?php psafe($organizer)?></td>
							</tr>
						<?php }?>
					</table>
		<?php }
	}