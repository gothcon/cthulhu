<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		private $returnPath;
		private $eventFormAction;
		private $deleteEventLink;
		private $eventOccationFormActionTemplate;
		private $deleteEventOccationLinkTemplate;
		private $newEventOccationFormAction;
		private $signupSlotFormActionTemplate;
		private $deleteSignupSlotLinkTemplate;
		private $newSignupSlotFormAction;
		private $searchUrl;
		private $resourceLinkTemplate;
		private $deleteBookingLinkTemplate;
		
		
		public function init(){

			$this->event = $this->model->getEvent();
			$this->occations = $this->model->getOccations();
			$this->timespanSelectList = $this->model->getTimespanSelectList();
			$this->timespan = $this->model->getCurrentTimespan();
			$this->occation = $this->model->getCurrentOccation();
			$this->slotTypeSelectList = $this->model->getSlottypeSelectList();
			$this->signupSlots = $this->model->getSignupSlots();
			$this->signups = $this->model->getSignups();
			$this->organizer = $this->event->getOrganizer();
			
			$this->organizerName = $this->organizer == null ? $this->translate("event/noOrganizerChosen") : $this->organizer->getName();
			$this->resourceBookings = $this->model->getResourceBookings();

			$this->eventFormAction = $this->internalLink("event",num($this->event->id)."/".CONTROLLER_ACTION_SAVE,false);
			
		}
		
		public function getSignupFormAction($signupId){
			return $this->internalLink("event",num($this->event->id)."/".CONTROLLER_ACTION_UPDATE_SIGNUP."?signup_id=".num($signupId),false);
		}
		
		public function getEventOccationFormAction($eventOccationId){
			return $this->internalLink("event",num($this->event->id)."/".CONTROLLER_ACTION_UPDATE_EVENT_OCCATION."?event_occation_id=".num($eventOccationId),false);
		}

		public function getDeleteSignupLink($signupId){
			return $this->internalLink("event",num($this->event->id)."/".CONTROLLER_ACTION_DELETE_SIGNUP."?signup_id=".num($signupId),false);
		}
		
		public function getDeleteEventOccationLink($eventOccationId){
			 return $this->internalLink("event",num($this->event->id)."/".CONTROLLER_ACTION_DELETE_EVENT_OCCATION."?event_occation_id=".num($eventOccationId),false);
		}
		
		public function getVisibleInScheduleLabel(){
			return $this->event->visible_in_schedule  ? $this->translate("general/yes"): $this->translate("general/no");
		}
		
		public function getVisibleInPublicListingLabel(){
			return $this->event->visible_in_public_listing ? $this->translate("general/yes") : $this->translate("general/no");
		}
		
		public function render(){
			$eventTypes = $this->model->getEventTypeSelectList();
			?>
			<form method="post" action="<?php echo $this->eventFormAction?>" class="eventForm">
				<div class="section">
					<h2 class="sectionHeader"><?php echo $this->translate("general/details");?></h2>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("event/eventName"),"name");?>
							<span class="data"><?php psafe($this->event->name)?></span>
							<?php Html::Label($this->translate("event/eventType"),"eventtype_id");?>
							<span class="data"><?php psafe($this->event->getEventType()->name)?></span>
							<?php Html::Label($this->translate("event/organizer"),"organizer_id"); ?>
							<span class="data" style="float:left;"><?php psafe($this->organizerName)?></span>
							<?php Html::Label($this->translate("event/scheduleName"),"schedule_name");?>
							<span class="data"><?php psafe($this->event->schedule_name); ?></span>
							<?php Html::Label($this->translate("event/visibleInSchedule"),"visible_in_schedule");?>
							<span class="data"><?php echo $this->getVisibleInScheduleLabel() ;?>.</span>
							<?php Html::Label($this->translate("event/visibleInPublicListing"),"visible_in_public_listing");?>
							<span class="data"><?php  echo $this->getVisibleInPublicListingLabel();?>.</span>
						</span>
					</fieldset>
				</div>
				<div class="section">
					<h2 class="sectionHeader"><?php echo $this->translate("event/folderInfo");?></h2>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/preamble"),"preamble");?>
						<?php Html::Text("preamble",$this->event->preamble,true,array("class" =>"tiny")); ?>
					</div>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/text"),"text");?>
						<?php Html::Text("text",$this->event->text,true,array("class" =>"tiny")); ?>
					</div>
					<span class="clear"></span>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/info"),"info");?>
						<?php Html::Text("info",$this->event->info,true,array("class" =>"tiny")); ?>
					</div>
					<span class="clear"></span>
					<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</form>
			<?php if($this->event->id > 0){ ?>
			<div class="section">
				<h2 class="sectionHeader"><?php echo $this->translate("event/eventOccationsAndSignups");?></h2>
				<ul id="eventOccationList">
					<?php $counter = 0;
					foreach($this->occations as $eventOccation){ ?>
					<li class="pass">
						<h3 class="passnamn"><?php psafe($eventOccation->getName()); ?></h3>

<!-- anmälnings- och bokningssektion -->

<!-- Resursbokningsblock -->
							<div class="resourceBookingsContainer">
								<h4><?php echo $this->translate("resources/bookedResources");?></h4>
								<span class="clear"></span>
								<table class="signupTable">
									<tr>
										<th class="nameCol"><?php echo $this->translate("resources/name");?></th>
										<th class="typeCol"><?php echo $this->translate("resources/type");?></th>
									</tr>
									<?php foreach($this->resourceBookings as $resourceBooking){
										if($eventOccation->id != $resourceBooking->event_occation_id)
											continue;
										$resource = $resourceBooking->getResource();
										$resourceType = $resource->resourceType;
										$booking = $resourceBooking->getBooking();
									?>
									<tr>
										<td class="nameCol"><?php psafe($resource->name)?></td>
										<td class="typeCol"><?php psafe($resourceType->name)?></td>
										<td class="isMainResourceCol"><?php echo ($resourceBooking->id == $eventOccation->main_booking_id)?$this->translate("resources/isMainResource"):"";?></td>
									</tr>
									<?php } ?>
								</table>
								<hr/>
							</div>
<!-- Anmälningstypsblock -->
							<?php 
								$counter = 0;
								foreach($this->signupSlots as $signupSlot){ 
								if($signupSlot->event_occation_id != $eventOccation->id)
									continue;
								
								?>
								<div class="signupTypeContainer">
									<h4><?php psafe($signupSlot->getSlotType()->name);?></h4>
									<span class="clear"></span>
									<table class="signupTable">
										<tr>
											<th class="nameCol"><?php echo $this->translate("signup/name");?></th>
											<th class="approvalCol"><?php echo $this->translate("signup/approved");?></th>
											<th class="approvalCol"><?php echo $this->translate("signup/paid");?></th>
											<th class="buttonCol"></th>
										</tr>
										<?php foreach($this->signups as $signup){
											if($signup->signup_slot_id != $signupSlot->id)
												continue;
											$name = !$signup->person_id ? ( $signup->group->name) : ($signup->person->first_name ." ".$signup->person->last_name);
										?>
										<tr>
											<td><?php psafe($name)?></td>
											<td class="approvalCol">
												<?php if($signupSlot->requires_approval){?>
												<form method="post" action="<?php echo $this->getSignupFormAction($signup->id);?>" class="signupForm ajaxForm">
													<?php 
													Html::Checkbox("is_approved",1,$signup->is_approved == 1,array("style" => "float:none; display: inline"));
													Html::SubmitButton($this->translate("general/update"));
													?>
												</form>
												<?php
												}else{
													echo " - ";
												}?>
											</td>
											<td class="approvalCol">
												<?php if($signupSlot->signup_slot_article->id)
													echo $signup->is_paid == 1 ? $this->translate("general/yes"):$this->translate("general/no") ;
												?>
											</td>	
											<td><?php Html::DeleteButton($this->getDeleteSignupLink($signup->id),$this->translate("general/delete"),$this->translate("signup/deleteSignupWarning"));?></td>
										</tr>
										<?php } ?>
									</table>
									<span class="clear"></span>
									<hr/>
								</div>
								<?php if($counter++ % 3 == 2){?>		
									<span class="clear"></span>
								<?php } ?>	
							<?php } ?>

						<span class="clear"></span>
					</li>
					<?php } ?>

				</ul>
				<?php Html::LinkButton("Anmälningsöversikt (pdf)", $this->internalLink("pdf", "?file=overview&event_id={$this->event->id}", false),array("target" => "_blank"));?>
				<span class="clear"></span>
			</div>
	
			<?php } ?>
			<script type="text/javascript">
				
				$(".eventForms").ajaxForm({
					saveCallback:function(data){
						var name = data.data.name;
						$("#mainContentTop h1").html("<?php echo $this->translate("event/event")?>: " + name);
					}
				});

				$(".signupForm").each(function(key,obj){
					var $obj = $(obj);
					$obj.find(".submitButton").hide();
					var onChange = (function(){
						return function(event){
							$obj.find(".submitButton").click();
						}
					})();
					$obj.find("input").change(onChange);
					$obj.ajaxForm({
						saveCallback:function(data){
							console.log(data);
						}
					});
				})
			</script>
		<?php }
	}