<?php
	require_once("lib/Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		public function init(){
			$this->eventType = $this->model->getEventType();
			
		}
		public function render(){
			?>
				<div class="section">
					<h2 class="sectionHeader"><?php echo $this->translate("general/details");?></h2>
					<form method="post">
					<fieldset>
						<span class="leftBox">
						<?php Html::Label($this->translate("eventType/name"),"name");?>
						<?php Html::Text("name",$this->eventType->name); ?>
						</span>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
					</form>
				</div>
				<?php if($this->eventType->id && UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
				<div class="section">
					<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("general/delete");?></h2>
					<?php Html::DeleteButton($this->internalLink("eventType",$this->eventType->id."/".CONTROLLER_ACTION_DELETE,false),$this->translate("general/delete"),$this->translate("general/deleteWarning"),array("class" => "deleteButton"));?>
					</div>
				</div>
				<?php }?>

		<?php }
	}