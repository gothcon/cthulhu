<?php
namespace lib\Views\AdminViews\ArticleStatistics;
// use \lib\Models\AdminModels\ArticleStatistics\IndexModel;
use \HtmlView;

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
require_once("lib/ClassLib/Html.php");
class IndexView extends HtmlView{
	
	/** @return \lib\Models\AdminModels\ArticleStatistics\IndexModel */
	public function getModel() {
		return parent::getModel();
	}
	
	public function init() {
		$this->data = $this->getModel()->getStatistics();
	}

	/** @var \lib\Models\AdminModels\ArticleStatistics\IndexModel */
	protected $data;
	
	public function render() { ?>
		<div>
			<table class="category-order-amounts">
				<tr>
					<th>Vad</th>
					<th>Summa</th>
				</tr>
				<?php 
				$total = 0;
				foreach($this->data as $category){?>
					<tr class="category-header">
						<td><?php psafe($category->name);?></td>
						<td></td>
					</tr>
					<?php 
					$total += $category->total;
					foreach($category->articles as $article){
					?>
						<tr>
							<td><?php psafe($article->id);?>. <?php psafe($article->name);?></td>
							<td><?php psafe($article->order_total);?></td>
						</tr>
					<?php } ?> 
					<tr class="category-total"><td><?php psafe($category->name);?> totalt</td><td><?php psafe(number_format($category->total,2));?></td></tr>
				<?php }?>
				<tr class="order-total"><td>Totalt för alla kategorier</td><td><?php psafe(number_format($total,2));?></td></tr>
			</table>
        </div>
		<?php
	}
}
