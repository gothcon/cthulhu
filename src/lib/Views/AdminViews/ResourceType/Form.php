<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		/**
		 * @var ResourceType
		 */
		protected $resourceType;
		protected $resourceList;
		protected $sleepingStatistics;
		public function init(){
			$this->resourceType= $this->model->getResourceType();
			$this->resourceList = $this->model->getResourceList();
			$this->sleepingStatistics = $this->getModel()->getSleepingStatistics();
		}
		public function render(){
			?>
				<div class="section">
						<h2 class="sectionHeader"><?php echo $this->translate("general/details");?></h2>
						<form method="post">
						<fieldset>
							<span class="leftBox">
								<?php Html::Label($this->translate("resourceType/name"),"name");?>
								<?php Html::Text("name",$this->resourceType->name); ?>
							</span>
						</fieldset>
						<?php Html::SubmitButton($this->translate("general/save"));?>
						</form>
				</div>
				<?php if($this->resourceType->id > 0){ ?>
				<div class="section">
					<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("resourceType/resourcesOfThisType");?></h2>
					
					<?php $r=0;	?>
					<table class="resourceList">
						<tr class="even">
							<th class="idCol"><?php echo $this->translate("resources/id");?></th>
							<th class="nameCol"><?php echo $this->translate("resources/name");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/isAvailable");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/availableForSleeping");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/noOfSleepingSlots");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/noOfFreeSleepingSlots");?></th>
						<?php foreach($this->resourceList as $resource){
							$resourceLink = $this->internalLink("resource",$resource->id,false);
							$class = $r++ % 2 ? "even" : "odd";
							if(!$resource->available)
								$class .=" unavailable";
						?>
							<tr class="<?php echo $class;?>">
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->id)?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->name)?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->available ? $this->translate("general/yes") : $this->translate("general/no"))?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->sleeping_event_occation_id ? $this->translate("general/yes") : $this->translate("general/no"))?></a></td>
								<td><?php psafe($resource->sleeping_event_occation_id ? $this->sleepingStatistics[$resource->id]->total : " - ")?></td>
								<td><?php psafe($resource->sleeping_event_occation_id ? $this->sleepingStatistics[$resource->id]->available : " - ")?></td>
							</tr>
						<?php } ?>
					</table>
					
					
					
					</div>
				</div>
				<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
					<div class="section">
						<div class="sectionContent">
						<h2 class="sectionHeader"><?php echo $this->translate("general/delete");?></h2>
						<?php Html::DeleteButton($this->internalLink("resourceType", "{$this->resourceType->id}/".CONTROLLER_ACTION_DELETE,false),$this->translate("general/areYouSure"),$this->translate("resourceType/deleteWarning"),array("class" => "deleteButton"));?>
						</div>
					</div>
				<?php }?>	
			<?php }
		}
	}