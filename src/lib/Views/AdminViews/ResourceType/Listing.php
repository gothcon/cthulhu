<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->resourceTypeList = $this->model->getResourceTypeList();
			// $this->currentResource = $this->model->getCurrentResource();
		}
		public function render(){
			$r=0;
			?>
			<table class="resourceTypeList">
				<tr class="even">
					<th class="nameCol"><?php echo $this->translate("resourceType/name");?></th>
					<th class="nameCol"><?php echo $this->translate("resourceType/existingCount");?></th>
					<th class="nameCol"><?php echo $this->translate("resourceType/availableCount");?></th>
					<th class="buttonsCol"></th>
				<?php foreach($this->resourceTypeList as $resourceType){?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->internalLink("resourceType", num($resourceType->id)."/".CONTROLLER_ACTION_EDIT) ?>"><?php psafe($resourceType->name)?></a></td>
						<td><a href="<?php $this->internalLink("resourceType", num($resourceType->id)."/".CONTROLLER_ACTION_EDIT) ?>"><?php psafe($resourceType->resource_count);?></a></td>
						<td><a href="<?php $this->internalLink("resourceType", num($resourceType->id)."/".CONTROLLER_ACTION_EDIT) ?>"><?php psafe($resourceType->available_resource_count);?></a></td>
						<td>
							<?php Html::LinkButton($this->translate("general/edit"),$this->internalLink("resourceType", num($resourceType->id)."/".CONTROLLER_ACTION_EDIT,false),array("class" => "editButton"))?>
						</td>
					</tr>
				<?php } ?>
			</table>
			<script type="text/javascript">
				(function($){
					var defaultOptions = {
						saveUrl : "<?php $this->internalLink("resourceType", "{0}/".CONTROLLER_ACTION_SAVE."?format=json") ?>",
						newUrl : "<?php $this->internalLink("resourceType", CONTROLLER_ACTION_NEW."?format=json") ?>",
						deleteUrl : "<?php $this->internalLink("resourceType", "{0}/".CONTROLLER_ACTION_DELETE."format=json")?>"
					}	
					$.fn.resourceTypeList = function(optionsParam){
						var options = $.extend({},defaultOptions,optionsParam);
						var $this = $(this);

						if($this.length < 1){
							return $this;
						}
						
						function doAjaxSave(requestUrl,resourceTypeName,callback){
							$.ajax({
								  url: requestUrl,
								  dataType: 'json',
								  type : 'POST',
								  data: { "name" : resourceTypeName},
								  success: callback
							});
						}
						
						function doAjaxDelete(requestUrl,resourceTypeId,callback){
							$.ajax({
								  url: requestUrl,
								  dataType: 'json',
								  type : 'POST',
								  data: { "id" : resourceTypeId},
								  success: callback
							});
						}
						
						
						function createSaveButtonCallback($inputField,onSaveSuccessfull,operation,typeId){
							return function(event){
								if(operation == "new"){
									saveUrl = options.newUrl;
								}else{
									saveUrl = options.saveUrl.replace("{0}",typeId);
								}
								doAjaxSave(saveUrl,$inputField.val(),onSaveSuccessfull);
								event.stopPropagation();
								event.preventDefault();
								return false;
							};
						}
						
						function transformRows($table){
							$table.find("tr").each(function(key,row){
								if(key == 0){
									return true;
								}
								$tr = $(row);
								$idCell = $($tr.children()[0]);
								$nameCell = $($tr.children()[1]);
								$buttonCell = $($tr.children()[2]);
								
								var $idLink = $($idCell.find("a")[0]);
								var id = $idLink.text();
								var $idSpan = $("<span>"+id+"</span>");
								$idLink.replaceWith($idSpan);
								
								var $nameLink = $($nameCell.find("a")[0]);
								var name = $nameLink.text();
								var $nameSpan = $("<span>"+name+"</span>");
								$nameLink.replaceWith($nameSpan);
								
								var $editButton = $($buttonCell.find(".editButton")[0]).removeProp("click");
								var $deleteButton = $($buttonCell.find(".deleteButton")[0]).hide().removeProp("click");
								var $saveButton = createSaveButton().hide().insertAfter($editButton);
								var $abortButton = createAbortButton().insertAfter($saveButton).hide();
								var $nameInput = createNameInput(name).appendTo($nameCell).hide();

								var leaveEditState = function(){
									$deleteButton.hide();
									$saveButton.hide();
									$abortButton.hide();
									$editButton.show();
									$nameInput.hide();
									$nameSpan.show();
								}

								var enterEditState = function(){
									$editButton.hide();
									$deleteButton.show();
									$abortButton.show();
									$saveButton.show();
									$nameSpan.hide();
									$nameInput.show();
								}
								
								$abortButton.click(function(event){
									event.stopPropagation();
									event.preventDefault();
									leaveEditState();
									return false;
								});
								
								$editButton.click(function(event){
									event.stopPropagation();
									event.preventDefault();
									enterEditState();
									return false;
								});
								
								
								$deleteButton.click(createDeleteButtonClick($tr,id));
									$saveButton.click(createSaveButtonClick($nameInput,$nameSpan,$idSpan,"save",function(data){
									$nameSpan.text(data.data.name);
									$idSpan.text(data.data.id);
									leaveEditState();
									log(data.message);
								}));
								
							});
						}
						
						function createSaveButton(label){
							label = label || $.translate("general/save"); 
							var $saveButton = $("<a href='#spara' class='buttonLink saveButton'>" + label + "<span class='end'></span></a>");
							return $saveButton;
						}						
						
						function createAbortButton(){
							label = $.translate("general/abort"); 
							var $button = $("<a href='#avbryt' class='buttonLink abortButton'>" + label + "<span class='end'></span></a>");
							return $button;
						}
						
						function createDeleteButton(){
							var $deleteButton = $("<a href='#' class='buttonLink deleteButton'>"+ $.translate("general/delete")+"<span class='end'></span></a>");
							return $deleteButton;
						}
						
						function createNameInput(name){
							name = name || "";
							return $("<input type='text' name='name' class='nameInput' value='"+name+"'/>")
						}
						
						function createSaveButtonClick($inputField,$nameSpan,$idSpan,operation,ajaxCallback){
							operation = operation || "save";
							return function(event){
								if(operation == "new"){
									saveUrl = options.newUrl;
								}else{
									saveUrl = options.saveUrl.replace("{0}",$idSpan.text());
								}
								doAjaxSave(saveUrl,$inputField.val(),ajaxCallback);
								event.stopPropagation();
								event.preventDefault();
								return false;
							};
						}
						
						function createNewButtonClick($table){
							return function(event){
								var $ajaxCallback = function(data){
									// create row
									$tr = $("<tr>");
									$td1 = $("<td>");
									$td2 = $("<td>");
									$td3 = $("<td>");
									$table.append($tr.append($td1).append($td2).append($td3));
									
									// create the input field and name textspan
									$nameInput = $("<input type='text' value='"+ data.data.name +"'>");
									$nameSpan = $("<span>"+data.data.name+"</span>");
									// create the save button
									$saveButton = createSaveButton($.translate("general/save"));
									$saveButton.click(createSaveButtonClick($nameInput));
									// create the delete button
									$deleteButton = createDeleteButton()
									// create the click callback
									$deleteButtonClick = createDeleteButtonClick($tr,id);
									$table.append($tr.append($td1).append($td2.append($td2input)).append($td3.append($td2submit)));
									$table.append("<tr></tr>");
								}
							};
						}
						
						function log(string){
							console.log(string);
						}
						
						function createDeleteButtonClick($tr,typeId){
							var ajaxCallback = function(data){
								$tr.remove();
								log(data.data.message);
							}
							var deleteUrl = options.deleteUrl.replace("{0}",typeId);
							
							return function(event){
								doAjaxDelete(deleteUrl,typeId,ajaxCallback);
								event.stopPropagation();
								event.preventDefault();
								return false;
							};
						}
						
						$this.each(function(key,obj){
							var $table = $(obj);
							// transformRows($table);
							/*
							$table.find(".editButton").each(function(key,button){
								var $editButton = $(button).hide();
								var $deleteButton = $editButton.siblings(".deleteButton").hide();
								var $saveButton = $("<a href='#' class='submitButtonLink saveButton'>spara<span class='end'></span></a>");
								var $linkCell = $($tr.children()[1]);
								var $idCell = $($tr.children()[0]);
								var $idlink = $($idCell.find("a"));
								var $namelink = $($linkCell.find("a"));
								var id = $idlink.text();
								var name = $namelink.text();
								var $namespan = $("<span>"+name + "</span>");
								$namelink.replaceWith($namespan);
								var $inputField = $("<input type='text' name='name' value='"+name+"'/>").appendTo($linkCell).hide();
								var $idField = $("<input type='hidden' name='id' value='"+name+"'/>").appendTo($linkCell);
								$inputField.insertAfter($namelink);
								$saveButton.insertBefore($deleteButton);
								$editButton.prop("click",undefined);
								var callback = createSaveButtonCallback($inputField,function(){
									$namespan.text($inputField.val());
								},"save",id);
								$saveButton.click(callback);
							});
							*/
						});
					};
				})(jQuery);
				$(".resourceTypeList").resourceTypeList();
			</script>
			<?php Html::LinkButton($this->translate("resourceType/new"),$this->internalLink("resourceType", CONTROLLER_ACTION_NEW,false))?>
		<?php }
	}