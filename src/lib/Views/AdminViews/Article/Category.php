<?php
	require_once("lib/Views/HtmlView.php");
	
	class CategoryView extends HtmlView{	
		/**
		 *
		 * @var Article
		 */
		private $category;
		private $orderingStatistics;
		public function printCategoryLink($id){
			return $this->internalLink("article","?p=".pnum($id),false);
		}
		
		public function init(){
			$this->category = $this->getModel()->getArticle();
			$this->deleteCategoryLink = $this->internalLink(ArticleController::getControllerName(),num($this->category->id)."/".CONTROLLER_ACTION_DELETE,false);
			$this->startLink = $this->internalLink(ArticleController::getControllerName(),'',false);
			$this->path = $this->category->loadPath();
			$this->orderingStatistics = $this->getModel()->loadArticleOrderingStatistics();
		}
		public function render(){?>
			<div>
				<?php 
					$first = true;
					$count =  count($this->path);
					$i = 1;
					foreach( $this->path as $key => $category){ 
						$last = $i++ == $count;
					if($first){?>
						<a href="<?php echo $this->startLink;?>">Start</a>
					<?php
						$first = false;
					}else if($last){
						echo "/".$category->name;
					}
					else{ ?>
						/<a href="<?php $this->printCategoryLink($category->id);?>"><?php psafe($category->name);?></a>
				<?php } 
					}
				?>
			</div>
			<div class="section">
				<form method="post">
					<span class="clear"></span>
					<h4 class="sectionHeader"><?php psafe($this->translate("general/details"))?></h4>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("general/name"),"name");?>
							<?php Html::Text("name",$this->category->name); ?>
							<?php  if($this->category->image_id){?>
								<img src="<?php echo $this->category->getImage()->thumbnail_small;?>"/>
								<img src ="<?php echo Application::getFilesRootUrl(). $this->category->getImage()->path. $this->category->getImage()->name;?>">
								<span class="clear"></span>
							<?php } ?>
							<?php Html::Label($this->translate("general/image"),"bild");?> 
							<?php Html::Text("image_id",$this->category->image_id);?>
							<?php Html::Label($this->translate("general/type"),"type");?>
							<?php Html::Select("type",$this->model->getTypes() ,ARTICLE_TYPE_CATEGORY,array("disabled" => "disabled")); ?>
							<?php Html::Hidden("type",ARTICLE_TYPE_CATEGORY);?>
							<?php Html::Label($this->translate("articles/availableFromDate"), "starts_at"); ?>
							<?php Html::Text("available_from_date",$this->category->available_from_date,false,array("id" => "startdatum","class" => "datum"));?>
							<?php Html::Label($this->translate("articles/availableFromTime"), "available_from_time"); ?>
							<?php Html::Text("available_from_time",$this->category->available_from_time,false,array("id" => "start_tid","class" => "tid"));?>
							<?php Html::Label($this->translate("articles/availableToDate"), "available_to_date"); ?>
							<?php Html::Text("available_to_date",$this->category->available_to_date,false,array("id" => "slutdatum","class" => "datum"));?>
							<?php Html::Label($this->translate("articles/availableToTime"), "available_to_time"); ?>
							<?php Html::Text("available_to_time",$this->category->available_to_time,false,array("id" => "slut_tid","class" => "tid"));?>
							<div class="tinyContainer">
								<?php Html::Label($this->translate("general/description"),"description") ;?>
								<?php Html::Text("description",$this->category->description,true,array("class" =>"tiny")); ?>
							</div>
						</span>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
				</form>
			</div>
			<div class="section">
				<span class="clear"></span>
				<h4 class="sectionHeader"><?php echo $this->translate("articles/orderStatistics")?></h4>
				<table>
					<tr>
						<th>Artikel</th>
						<th><?php echo $this->translate("articles/numberOrdered"); ?></th>
						<th><?php echo $this->translate("articles/numberPaid"); ?></th>
						<th><?php echo $this->translate("articles/numberDelivered"); ?></th>
					</tr>
				<?php 
				
					$no_ordered_total = 0;
					$no_paid_total = 0;
					$no_delivered_total = 0;
					foreach($this->orderingStatistics as $statistics){
						$no_ordered_total += $statistics->no_ordered;
						$no_paid_total += $statistics->no_paid;
						$no_delivered_total += $statistics->no_delivered;	
					?>
					<tr>
						<td><?php psafe($statistics->article_id);?>. <?php psafe($statistics->article_name);?></td>
						<td><?php psafe($statistics->no_ordered);?></td>
						<td><?php psafe($statistics->no_paid)?></td>
						<td><?php psafe($statistics->no_delivered)?></td>
					</tr>
				<?php } ?>
					<tr class="totalRow">
						<td>Totalt:</td>
						<td><?php psafe($no_ordered_total)?></td>
						<td><?php psafe($no_paid_total)?></td>
						<td><?php psafe($no_delivered_total)?></td>
					</tr>
				</table>
			</div>
			<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/delete");?></h4>
					<?php Html::DeleteButton($this->deleteCategoryLink,$this->translate("articles/deleteArticle"),$this->translate("general/areYouSure"));?>
				</div>
			</div>
			<?php } ?>
			<script type="text/javascript">
				var $active = null;
				$(".specifics").hide();
				$("select").each(function(key,obj){
					var $this = $(obj);
					$this.change(function(event){
						$active.hide();
						$active = $("#"+$this.val()).show();
					});
					$active = $("#"+$this.val()).show();
				});
			</script>
		<?php }
	}