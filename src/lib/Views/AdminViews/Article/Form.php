<?php
	require_once("lib/Views/HtmlView.php");
	
	class FormView extends HtmlView{	
		/**
		 *
		 * @var Article
		 */
		private $article;
		/**
		 *
		 * @var TreeItem_ArticleOrderingStatistics
		 */
		protected $statistics;
		
		/**
		 *
		 * @var TreeItem_OrderRow
		 */
		protected $orderRows;
		
		
		public function printCategoryLink($id){
			echo $this->internalLink("article","?p=".$id,false);
		}
		
		public function printPersonLink($personId){
			echo $this->internalLink("person",$personId,false);
		}
		
		public function printOrderLink($orderId){
			echo $this->internalLink("order",$orderId,false);
		}
		
		public function init(){
			$this->article = $this->getModel()->getArticle();
			$statisticsArray = $this->getModel()->loadArticleOrderingStatistics();
			if($this->article->id > 0){
				$this->statistics = $statisticsArray[0];
				$this->orderRows = $this->statistics->getOrderRows();
				$this->deleteArticleLink = $this->internalLink(ArticleController::getControllerName(),num($this->article->id)."/".CONTROLLER_ACTION_DELETE,false);
			}
			$this->path = $this->article->loadPath();
			$this->articleControllerLink = $this->internalLink("article","",false);
		}
		public function render(){?>
			<div id="articlePath">
				<?php 
					$first = true;
					$count =  count($this->path);
					$i = 1;
					foreach( $this->path as $key => $category){ 
						$last = $i++ == $count;
					if($first){?>
						<a href="<?php echo $this->articleControllerLink;?>">Start</a>
					<?php
						$first = false;
					}else if($last){
						psafe("/".$category->name);
					}
					else{ ?>
						/<a href="<?php $this->printCategoryLink($category->id);?>"><?php psafe($category->name)?></a>
				<?php } 
					}
				?>
			</div>
			<div class="section">
			<form method="post">
			
			<span class="clear"></span>
				<h4 class="sectionHeader"><?php echo $this->translate("general/details")?></h4>
				<fieldset>
					<span class="formBox">
						<?php Html::Label($this->translate("general/name"),"name");?>
						<?php Html::Text("name",$this->article->name); ?>
						<?php if($this->article->image_id > 0){ ?>
						<span class="clear"></span>
						<img src ="<?php echo Application::getFilesRootUrl().$this->article->getImage()->path.$this->article->getImage()->name;?>">
						<span class="clear"></span>
						<?php } ?>
						<?php Html::Label($this->translate("general/image"),"bild");?> 
						<?php Html::Text("image_id",$this->article->image_id);?>
						<?php Html::Label($this->translate("general/type"),"type");?>
						<?php Html::Select("type",$this->model->getTypes() ,$this->article->type); ?>
						<?php Html::Label($this->translate("articles/availableFromDate"), "starts_at"); ?>
						<?php Html::Text("available_from_date",$this->article->available_from_date,false,array("id" => "startdatum","class" => "datum"));?>
						<?php Html::Label($this->translate("articles/availableFromTime"), "available_from_time"); ?>
						<?php Html::Text("available_from_time",$this->article->available_from_time,false,array("id" => "start_tid","class" => "tid"));?>
						<?php Html::Label($this->translate("articles/availableToDate"), "available_to_date"); ?>
						<?php Html::Text("available_to_date",$this->article->available_to_date,false,array("id" => "slutdatum","class" => "datum"));?>
						<?php Html::Label($this->translate("articles/availableToTime"), "available_to_time"); ?>
						<?php Html::Text("available_to_time",$this->article->available_to_time,false,array("id" => "slut_tid","class" => "tid"));?>
						<div class="tinyContainer">
							<?php Html::Label($this->translate("general/description"),"description") ;?>
							<?php Html::Text("description",$this->article->description,true,array("class" =>"tiny")); ?>
						</div>
					</span>
				</fieldset>
				<div id="article" class="specifics">
					<h5><?php echo $this->translate("articles/articleSpecifics")?></h5>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("articles/price"),"article_price");?>
							<?php Html::Text("article_price",$this->article->price); ?>
						</span>
					</fieldset>
				</div>
				<div id="category" class="specifics">
					
				</div>
				<div id="entrance_fee" class="specifics">
					<h5><?php echo $this->translate("articles/entranceSpecifics")?></h5>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("articles/price"),"entrance_fee_price");?>
							<?php Html::Text("entrance_fee_price",$this->article->price); ?>
						</span>
					</fieldset>
				</div>
				<div id="slot_fee" class="specifics">
					<h5><?php echo $this->translate("articles/slotfeeSpecifics")?></h5>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("articles/price"),"slot_fee_price");?>
							<?php Html::Text("slot_fee_price",$this->article->price); ?>
						</span>
					</fieldset>
				</div>
			<?php Html::SubmitButton($this->translate("general/save"));?>
				</form>
			</div>
			<?php if($this->article->type != "category" && $this->article->id > 0){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("articles/statisticsAndCustomers")?></h4>
					<div class="articleOrderRowStatistics">
						<h5><?php echo $this->translate("articles/orderStatistics")?></h5>
						<dl class="orderData">
							<dt><?php echo $this->translate("articles/numberOrdered"); ?>:</dt>
								<dd><?php psafe($this->statistics->no_ordered);?></dd>
							<dt><?php echo $this->translate("articles/numberPaid"); ?>:</dt>
								<dd><?php psafe($this->statistics->no_paid)?></dd>
							<dt><?php echo $this->translate("articles/numberDelivered"); ?>:</dt>
								<dd><?php psafe($this->statistics->no_delivered)?></dd>
						</dl>
					</div>
					<div class="articleCustomerList">
					
						<h5><?php echo $this->translate("articles/customer")?></h5>
						<?php $r=0;	?>

						<table class="articleCustomerList">
							<tr class="even">
								<th class="nameCol"><?php echo $this->translate("general/name"); ?></th>
								<th class="typeCol"><?php echo $this->translate("general/count"); ?></th>
								<th><?php echo $this->translate("articles/paymentStatuses/paid"); ?></th>
								<th><?php echo $this->translate("articles/paymentStatuses/submitted"); ?></th>
								<th><?php echo $this->translate("general/note"); ?></th>
							</tr>
						<?php  foreach($this->orderRows as $row){ ?>
							<tr class="<?php echo $r++ % 2 ? "even" : "odd" ?>">
								<td><a href="<?php $this->printPersonLink($row->customer_id);?>"><?php psafe($row->customer)?></a></td>
								<td><?php pnum($row->article_count)?></td>
								<td><a href="<?php $this->printOrderLink($row->order_id);?>"><?php echo $row->is_paid ? $this->translate("general/yes"): $this->translate("general/no") ;?></a></td>
								<td><a href="<?php $this->printOrderLink($row->order_id);?>"><?php echo $row->is_delivered ? $this->translate("general/yes"): $this->translate("general/no") ;?></a></td>
								<td><?php echo $row->note;?></td>
							</tr>
						<?php 


						}?>
						</table>
					</div>
					<span class="clear"></span>
				</div>
			</div>
			<?php } ?>


			<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF) && $this->article->id > 0){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/delete");?></h4>
					<?php Html::DeleteButton($this->deleteArticleLink,$this->translate("articles/deleteArticle"),$this->translate("general/areYouSure"));?>
				</div>
			</div>
			<?php } ?>
			<script type="text/javascript">
				var $active = null;
				$(".specifics").hide();
				$("select").each(function(key,obj){
					var $this = $(obj);
					$this.change(function(event){
						$active.hide();
						$active = $("#"+$this.val()).show();
					});
					$active = $("#"+$this.val()).show();
				});
			</script>
		<?php }
	}