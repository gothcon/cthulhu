<?php
	require_once("lib/Views/HtmlListView.php");
	require_once("lib/Controllers/AdminControllers/OrderController.php");
	class ListingView extends HtmlListView{
		
		
		public function createRemoveFromCartLink($article_id){
			return $this->internalLink("article", num($article_id)."/".CONTROLLER_ACTION_REMOVE_FROM_CART . (isset($_GET['p']) ? "?p=".num($_GET['p']) : ""), false);
		}
		public function createAddToCartLink($article_id){
			return $this->internalLink("article", num($article_id)."/".CONTROLLER_ACTION_ADD_TO_CART .	(isset($_GET['p']) ? "?p=".num($_GET['p']) : ""), false);
		}
		public function createArticleLink($article_id){
			return $this->internalLink("article", num($article_id)."/".CONTROLLER_ACTION_EDIT ,false);
		}		
		
		public function createCategoryLink($article_id){
			return $this->internalLink("article", "?p=".num($article_id),false);
		}
		/** @var Article[] **/
		public $articles;
		/** @var Article[] **/
		public $categories;
		/** @var Article **/
		public $article;
		/** @var Article[] **/
		public $path;
		/** @var Person **/
		public $currentCustomer;
		/** @var Order **/
		public $cart;
		/** @var OrderRow[] **/
		public $cartItems;
	
		public $cartInfo;
		public $newOrderLink;
		public $orderControllerLink;
		public $articleControllerLink;
		public $newArticleLink;
		public $articleLinkTemplate;
		public $articleListLinkTemplate;
		
		
		public function init(){
			$this->articles = $this->model->getArticles();
			$this->categories = $this->model->getCategories();
			$this->article = $this->model->getArticle();
			$this->path = $this->model->getArticlePath();
			$this->currentCustomer = $this->model->getCurrentCustomer();
			$this->cart = $this->model->getCurrentCart();
			$this->cartItems = $this->cart->getOrderRows();
			
			$this->cartInfo = $this->translate("articles/cartArticleCount").": ".num($this->cart->getNumberOfArticles()).", ";
			$this->cartInfo .= $this->translate("articles/orderTotal").": ".number_format($this->cart->total,2).", ";
			$this->cartInfo .= $this->translate("articles/customer").": ".safe($this->currentCustomer);
			
			$this->newOrderLink = $this->internalLink(OrderController::getControllerName(), CONTROLLER_ACTION_CREATE_ORDER_FROM_CART, false );
			$this->orderControllerLink = $this->internalLink(OrderController::getControllerName(),"",false);
			$this->articleControllerLink = $this->internalLink("article","",false);
			$this->newArticleLink = $this->internalLink("article",CONTROLLER_ACTION_NEW."?p=".num($this->article->id),false);
			
			$this->articleLinkTemplate = $this->internalLink("article","%s/".$this->translate("actions/edit"),false);
			$this->articleListLinkTemplate = $this->internalLink("article","?p=%s",false);
		}
		public function render(){
			if($this->currentCustomer->id){
			?>
			<div id="cart">
				<h3 class="shadedText"><?php echo $this->translate("articles/currentOrder");?></h3>
				<div id="tableContainer">
					<?php $r=0;	?>
					<table id="cartTable">
						<tr class="even">
							<th class="idCol"><?php echo $this->translate("articles/articleId");?></th>
							<th class="nameCol"><?php echo $this->translate("articles/articleName");?></th>
							<th class="typeCol"><?php echo $this->translate("articles/articleCount");?></th>
							<th><?php echo $this->translate("articles/pricePerEach");?></th>
							<th><?php echo $this->translate("articles/totalPrice");?></th>
							<th class="buttonsCol"></th>
						</tr>
						<?php  foreach($this->cartItems as $row){ ?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td><?php pnum($row->article_id)?></td>
							<td><?php psafe($row->name)?></td>
							<td><?php psafe($row->count)?></td>
							<td><?php echo number_format($row->price_per_each,2);?></td>
							<td><?php echo number_format($row->total,2);?></td>
							<td><?php 
								Html::DeleteButton($this->createRemoveFromCartLink($row->article_id),$this->translate("general/delete"),$this->translate("articles/removeFromCartWarning"));
							?></td>
						</tr>
						<?php } ?>
					</table>
				</div>
				<span class="clear"></span>
				<div id="status" style="float:left">
					 <?php psafe($this->cartInfo) ?>
				</div>
				<div id="buttons" style="float:right">
					<?php Html::LinkButton($this->translate("articles/submitOrder"), $this->newOrderLink,array("style" => "float:left; margin-right:14px;"));?>
				</div>
				<span class="clear"></span>
			</div>
			<script type="text/javascript">
				var texts = [];
				texts[true] = "<?php echo $this->translate("articles/showArticles");?>";
				texts[false] = "<?php echo $this->translate("articles/hideArticles");?>";
				var cartIsHidden = true;
				var $textSpan = $("<span>"+texts[cartIsHidden] +"<span>");
				$status = $("#status");
				$statusLabel = $("<span id='statusLabel'></span>").html($("#status").html());
				$status.html("").append($statusLabel);
				var $button = $("<a class='editButton buttonLink' href='<?php echo $this->orderControllerLink ;?>' style='float:right; margin-right:10px'></a>").append($textSpan).append($("<span class='end'/>"));
				var $cart = $("#cart #tableContainer");
				
				$button.click(function(event){
					console.log($cart);
					event.preventDefault();
					event.stopPropagation();
					if(cartIsHidden){
						$cart.slideDown();
					}
					else{
						$cart.slideUp();
					}
					cartIsHidden = !cartIsHidden;
					$textSpan.html(texts[cartIsHidden]);
				});
				$cart.hide();
				$("#buttons").append($button);
			</script>	
			<?php } ?>
			<div id="articlePath">
				<?php 
					$count = 0;
					foreach( $this->path as $key => $category){ 
					if($count == 0){?>
						<a href="<?php echo $this->articleControllerLink;?>">Start</a>
					<?php
						$first = false;
					}
					else if($count + 1 == count($this->path)){ ?>
						/<a href="<?php echo $this->createArticleLink($category->id);?>"><?php psafe($category->name)?></a>
					<?php }
					else{ ?>
						/<a href="<?php echo $this->createCategoryLink($category->id);?>"><?php psafe($category->name)?></a>
					<?php } 
						$count++;
					}
				?>
			</div>
			<span class="clear"></span>
			<div class="articleMainContent">
			
			
			<div class="subCategoriesContainer">
				<h3 class="shadedText"><?php echo $this->translate("articles/subCategories"); ?></h3>
				<table class="articleTable">
					<tr>
						<th class="thumbCol"></th>
						<th class="idCol"><?php echo $this->translate("articles/categoryId"); ?></th>
						<th><?php echo $this->translate("articles/categoryName"); ?></th>
						
					</tr>
					<?php foreach($this->categories as $category){
						$image = ( $category->getImage()->getName(32,32) ? Application::getFilesRootUrl().$category->getImage()->path.$category->getImage()->getName(32,32) : Application::getFilesRootUrl()."/missingImage_32x32.png");
						?>
					<tr>
						<td><img src ="<?php psafe($image);?>"></td>
						<td><a href="<?php echo $this->createCategoryLink($category->id);?>"><?php pnum($category->id)?></a></td>
						<td><a href="<?php echo $this->createCategoryLink($category->id);?>"><?php psafe($category->name);?></a></td>
					</tr>
					<?php } ?>
				</table>
				<?php Html::LinkButton($this->translate("articles/newCategory"),$this->newArticleLink ,array("class" => "editButton"))?>
			</div>
			<div class="articlesContainer">
				<h3 class="shadedText"><?php echo $this->translate("articles/articles");?></h3>
				<table class="articleTable">
					<tr>
						<th class="thumbCol"></th>
						<th class="idCol"><?php echo $this->translate("articles/articleId");?></th>
						<th><?php echo $this->translate("articles/articleName");?></th>
						<th><?php echo $this->translate("articles/pricePerEach");?></th>
						<th class="buttonCol"></th>
						<?php if($this->currentCustomer->id){ ?>
						<th class="orderCol" style="text-align:center"><?php echo $this->translate("articles/addToOrder");?></th>
						<?php } ?>
					</tr>
					<?php foreach($this->articles as $article){ 
						$image = ( $article->getImage()->getName(32,32) ? Application::getFilesRootUrl().$article->getImage()->path.$article->getImage()->getName(32,32) : Application::getFilesRootUrl()."/missingImage_32x32.png");
					?>
					<tr>
						<td><img src ="<?php psafe($image);?>"></td>
						<td><a href="<?php echo $this->createArticleLink($article->id)?>"><?php pnum($article->id)?></a></td>
						<td><a href="<?php echo $this->createArticleLink($article->id)?>"><?php psafe($article->name)?></a>
						<td><a href="<?php echo $this->createArticleLink($article->id)?>"><?php echo number_format($article->price,2);?></a>
						</td>
						<td>
						<?php Html::LinkButton($this->translate("articles/information"),$this->internalLink("article", "{$article->id}",false),array("class" => "editButton"))?>
						</td>
						<?php if($this->currentCustomer->id){ ?>
						<td>
							<form class="addToCartForm" action="<?php echo $this->createAddToCartLink($article->id);?>" method="post">
								<?php Html::Text($this->translate("articles/count"),"1");?>
								<?php Html::SubmitButton($this->translate("articles/addToOrder"))?>
							</form>
						</td>
						<?php } ?>
					</tr>
					<?php } ?>
				</table>
				<?php Html::LinkButton($this->translate("articles/newArticle"),$this->newArticleLink ,array("class" => "editButton"))?>
			</div>
				<script type="text/javascript">
					$(".addToCartForm").ajaxForm({preventBuiltInCallback:true,returnDataType:"html",saveCallback:function($form,data){
						var $response = $(data);
						
						$("#cartTable").html($response.find("#cartTable").html());
						$("#status").replaceWith($response.find("#status"));
						
						$response.find("#infoList li").each(function(key,obj){
							var message = $(obj).html();
							$.logger.log(message,"info");
						});
						$response.find("#errorList li").each(function(key,obj){
							var message = $(obj).html();
							$.logger.log(message,"error");
						});
						$response.find("#warningList li").each(function(key,obj){
							var message = $(obj).html();
							$.logger.log(message,"warning");
						});
						
						var $status = $($response.find("#status"));
						var $currentStatus = $("#statusLabel");
						var $statusButton = $($currentStatus.find(".buttonLink"));
						$currentStatus.html($status.html());
						$currentStatus.append($statusButton);
					}});
				</script>
		
			</div>
		
		<?php 
		}
	}