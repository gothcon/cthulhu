<?php
	require_once("lib/Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		protected $slotTypeList;
		protected $eventList;
		
		public function init(){
			$this->emailAddresses = EmailSelectionAccessor::getSelectedEmailAddresses();
			$this->slotTypeList = $this->getModel()->getSlotTypeList();
			$this->eventList = $this->getModel()->getEventList();
			$this->entranceFeeList = $this->getModel()->getArticleList();
			// $this->signupSlotTree = $this->getModel()->getEventOccationTree();
		}
		public function render(){?>
		<form method="post"> 
			
			<div class="newsLetterContent">
			<h3>Ämne</h3>
			<?php Html::Text("subject","",false,array("class" => "subjectInput"));?>
			<h3>Meddelande</h3>
			<?php Html::Text("message","",true,array("class" => "tiny"));?>
			</div>
			<?php if(count($this->emailAddresses) > 0){ ?>
			<div class="recipients">
				<h3>
					<?php echo $this->translate("emailer/recipients");?>
				</h3>
				<div class="recipientList">
				<?php foreach($this->emailAddresses as $address){?>
					<span class="emailAddress"><?php echo htmlentities($address) ?></span>
				<?php } ?>
				</div>
				<span class="clear"></span>
					<?php Html::SubmitButton($this->translate("emailer/clear"), array("name" => "clear", "class"=> "clearRecipientsButton"));?>
			</div>
			<?php } ?>
			<div class="recipientSelect">
				<h3><?php echo $this->translate("emailer/selectRecipients");?></h3>

				<ul class="selectionList">
					<li>
						<?php Html::Radio("mainSelection", "all")?>
						<h4>Alla</h4>
					</li>
					<li>
						<?php Html::Radio("mainSelection", "organizers")?>	
						<h4>Arrangörer</h4>						
						<ul class="filterList">
							<li>
								<?php Html::Checkbox("organizers_filter_event");?>
								<h5>Av specifikt arrangemang</h5>
								<?php Html::Select("organizers_filter_event_id", $this->eventList)?>
							</li>
						</ul>
					</li>
					<li>
						<?php Html::Radio("mainSelection", "attenders")?> 
						<h4>Anmälda</h4>
						<ul class="filterList">
							<li>
								<?php Html::Checkbox("signup_filter_event",1);?>
								<h5>Till specifikt arrangemang</h5>
								<?php Html::Select("signup_filter_event_id", $this->eventList)?>
							</li>
							<li>
								<?php Html::Checkbox("signup_filter_slot_type",1);?>
								<h5>Med anmälning av specfik typ</h5>
								<?php Html::Select("signup_filter_slot_type_id", $this->slotTypeList)?>
							</li>
							<li>
								<?php Html::Checkbox("signup_filter_status",1);?>
								<h5>Med specifik anmälningsstatus</h5>
								<?php Html::Radio("signup_filter_valid",1)?> Endast godkända
								<?php Html::Radio("signup_filter_valid",0)?> Endast icke godkända
							</li>
							<li>
								<?php Html::Checkbox("signup_filter_payment",1);?>
								<h5>Med specifik betalningsstatus</h5>
								<?php Html::Radio("signup_filter_paid",0)?> har EJ betalt
								<?php Html::Radio("signup_filter_paid",1)?> har betalt
							</li>
						</ul>
					</li>
					<li>
						<?php Html::Radio("mainSelection", "entrance_fee")?> 
							<h4>Inträdesbeställare</h4><span class="explanation">Dvs de som har ett inträde i någon av sina beställningar eller i varukorgen.</span>
						<ul class="filterList">
							<li>
								<?php Html::Checkbox("entrance_fee_filter",1);?>
								<h5>Av speciell typ</h5>
								<?php Html::Select("entrance_fee_id", $this->entranceFeeList)?>
							</li>
							<li>
								<?php Html::Checkbox("entrance_fee_payment_filter",1);?>
								<h5>Med specifik betalningsstatus</h5>
								<?php Html::Radio("entrance_fee_paid",0)?> har EJ betalt
								<?php Html::Radio("entrance_fee_paid",1)?> har betalt
							</li>
						</ul>
					</li>
					<li>
						<?php Html::Radio("mainSelection", "unpaid_orders")?> 
						<h4>Beställare med obetalda ordrar</h4><span class="explanation">Dvs de med varor i obetalade beställningar eller i varukorgen.</span>
					</li>
					<li>
						<?php Html::Radio("mainSelection", "manual")?> 
						<h4>Manuellt</h4><span class="explanation">Separera fler med komma (,).</span>
						<span class="clear"></span>
						<ul class="filterList">
							<li class="noPadding"><?php Html::Text("manual_addresses","",true);?></li>
						</ul>
					</li>
				</ul>
				<script type="text/javascript">
					var $active;
					$("ul.selectionList > li").each(function(key,obj){

						var $filterList = $(obj).find("ul.filterList");
						var $checkBox = $(obj).find("input[name=mainSelection]");
						if($checkBox.attr("checked") == "checked")
							$active = $filterList;
						else
							$filterList.hide();
						$checkBox.bind("click",function(event){
							if($active)
								$active.hide();
							console.log($active);
							console.log($filterList);
							if(!$active || $active != $filterList){
								$active = $filterList;
								$active.show();
							}
						});
					});
				</script>
				<span class="clear"></span>
				<?php Html::SubmitButton($this->translate("general/add"), array("name" => "add", "class"=> "addRecipientButton"));?>
			</div>
			<span class="clear"></span>
			<?php Html::SubmitButton($this->translate("general/send"), array("name" => "send","class" => "sendNewsletterButton"));?>
		</form>	
		<?php }
	}