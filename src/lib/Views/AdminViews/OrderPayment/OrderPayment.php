<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../HtmlView.php");
	require_once("lib/ClassLib/Translator.php");
	class OrderPaymentView extends HtmlView{
		public function init(){
			$this->mostRecentOrderPayments = $this->getModel()->getMostRecentTransactions();
			$this->paymentMethods = array(
					array("value" => "cc" , "label" => $this->translate("transaction/paymentMethods/cc")),
					array("value" => "cash" , "label" => $this->translate("transaction/paymentMethods/cash")),
					array("value" => "wire_transfer" , "label" => $this->translate("transaction/paymentMethods/wire_transfer"))
			);
		}
		public function render(){?>
			<form method="post" id="paymentForm">			
				<table class="orderPaymentRegisterForm">
					<thead>
						<tr>
							<th class="idCol"><?php Translator::out("transaction/transactionId");?></th>
							<th class="orderIdCol"><?php Translator::out("orderPayment/orderId");?></th>
							<th class="amountCol"><?php Translator::out("transaction/amount");?></th>
							<th class="timeCol"><?php Translator::out("transaction/time");?></th>
							<th class="paymentMethodCol"><?php Translator::out("transaction/paymentMethod");?></th>
							<th></th>
						</tr>
						<tr>
							<td></td>
							<td><?php Html::Text("reference", "");?></td>
							<td><?php Html::Text("amount");?></td>
							<td></td>
							<td><?php Html::Select("payment_method",$this->paymentMethods,"wire_transfer");?></td>
						</tr>
					</thead>
					<tbody>
						<?php foreach($this->mostRecentOrderPayments as $orderPayment){
							$link = $this->internalLink("orderPayment", $orderPayment->id."/".CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT,false);
							?>
							<tr<?php if($orderPayment->transaction->cancelled == 1){echo " class=\"cancelled\"";}?>>
								<td><?php psafe($orderPayment->transaction->id)?></td>
								<td><?php psafe($orderPayment->order->id)?></td>
								<td><?php psafe($orderPayment->transaction->amount)?></td>
								<td><?php psafe($orderPayment->transaction->created_at)?></td>
								<td><?php echo $this->translate("transaction/paymentMethods/".$orderPayment->payment_method); ?></td>
								<td><?php if($orderPayment->transaction->cancelled == 0){Html::LinkButton("Makulera",$link);}?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			<?php Html::SubmitButton(Translator::translate("orderPayment/send"));?>
			</form>
<script type="text/javascript">
	
	$("#paymentForm").ajaxForm({
		saveCallback:function(form,responseData){
			if(responseData.status == 0){
				var payment = responseData.data;
				var paymentMethod = $("option[value="+payment.transaction.payment_method+"]").html();
				$row = $("<tr></tr>");
				$("<td>"+payment.transaction.id+"</td>").appendTo($row);
				$("<td>"+payment.order.id+"</td>").appendTo($row);
				$("<td>"+payment.transaction.amount+"</td>").appendTo($row);
				$("<td>"+payment.created_at+"</td>").appendTo($row);
				$("<td>"+paymentMethod+"</td>").appendTo($row);
				$("table.orderPaymentRegisterForm tbody").prepend($row);
				$("input[name=reference],input[name=amount] ").val("");
				$("input[name=reference]").focus();
				
			}
		},
		preventBuiltInCallback:false
	})
	
	
</script>


		<?php }
	}