<?php
namespace lib\Views\AdminViews\UserStatistics;
use \lib\Models\AdminModels\UserStatistics\IndexModel;
use \HtmlView;

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
require_once("lib/ClassLib/Html.php");
class IndexView extends HtmlView{
	
	/** @return \lib\Models\AdminModels\UserStatistics\IndexModel */
	public function getModel() {
		return parent::getModel();
	}
	
	public function init() {
		$this->data = $this->getModel();
	}

	/** @var \lib\Models\AdminModels\UserStatistics\IndexModel */
	protected $data;
	
	public function render() { ?>
		<h2>Användarstatistik</h2>
		<div class="sectionContent">
			<fieldset class="wideFormBox">
			<table>
				<tr>
					<th>Vad</th>
					<th>Antal</th>
				</tr>
				<tr>
					<td>Antal inloggade den senaste veckan:</td>
					<td><?php echo $this->data->getStatistics()->loginCount; ?></td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<tr>
					<td>Antal anlända:</td>
					<td><?php echo $this->data->getStatistics()->arrivalCount->totalCount; ?></td>
				</tr>
				<tr>
					<td>Antal icke anlända:</td>
					<td><?php echo $this->data->getStatistics()->unarrivedCount; ?></td>
				</tr>
				<!-- 
				<tr>
					<td>Beräknat antal föranmälda:</td>
					<td><?php echo $this->data->getStatistics()->approximatedTotalArrivalCount; ?></td>
				</tr>
				-->
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<tr>
					<td>Antal anlända (tjejer):</td>
					<td><?php echo $this->data->getStatistics()->arrivalCount->femaleCount; ?></td>
				</tr>
				<tr>
					<td>Antal anlända (killar):</td>
					<td><?php echo $this->data->getStatistics()->arrivalCount->maleCount; ?></td>
				</tr>
				<tr>
					<td>Antal anlända (okänt):</td>
					<td><?php echo $this->data->getStatistics()->arrivalCount->unknownCount; ?></td>
				</tr>
				
			</table>
			</fieldset>
        </div>
		<?php
	}
}
