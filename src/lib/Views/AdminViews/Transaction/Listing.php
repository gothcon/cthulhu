<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->list = $this->getModel()->getList();
			$this->filterDefault = new stdClass();
			$this->filterDefault->from = fromGet("from",strftime("%Y-%m-%d %H:%M:%S",time()-(24*60*60)));
			$this->filterDefault->to = fromGet("to",strftime("%Y-%m-%d %H:%M:%S",time()));
			$this->filterDefault->paymentMethod = fromGet("payment_method","");
			$this->paymentMethods = $this->model->getPaymentMethods();
			
			$this->groupTotal = array();
			$this->grandTotal = 0;

			if($this->filterDefault->paymentMethod != ""){
				$this->groupTotal[$this->filterDefault->paymentMethod] = 0;
			}
		}
		public function render(){?>
			<form class="filterForm">
				<?php Html::Label($this->translate("transaction/from"), "from");?>
				<?php Html::Text("from",$this->filterDefault->from);?>
				<?php Html::Label($this->translate("transaction/to"), "to");?>
				<?php Html::Text("to",$this->filterDefault->to);?>
				<?php Html::Label($this->translate("transaction/paymentMethod"), "payment_method");?>
				<?php Html::Select("payment_method",$this->paymentMethods,$this->filterDefault->paymentMethod);?>
				<?php Html::SubmitButton($this->translate("general/search"))?>
			</form>
			<script>
				$(".filterForm [name=from],.filterForm [name=to]").timepicker();
			</script>
			<table class="transactions">
				<col class="idCol"/>
				<col class="timeCol"/>
				<col class="paymentMethodCol"/>
				<col class="noteCol"/>
				<col class="amountCol"/>
				<tr>
					<th><?php echo $this->translate("general/id");?></th>
					<th><?php echo $this->translate("general/when");?></th>
					<th><?php echo $this->translate("transaction/paymentMethod");?></th>
					<th><?php echo $this->translate("general/note");?></th>
					<th class="amountCol"><?php echo $this->translate("transaction/amount");?></th>
				</tr>
				<?php 
				
				foreach($this->list as $item){
					$viewLink = $this->internalLink("transaction",num($item->id),false);
					$class = " class=\"cancelled\"";
					if(!$item->cancelled){ 
						$this->grandTotal += $item->amount;
						if(!isset($this->groupTotal[$item->payment_method])){
							$this->groupTotal[$item->payment_method] = 0;
						}
						$this->groupTotal[$item->payment_method] += $item->amount;
						$class = "";
					}
				?>
				<tr<?php echo $class;?>>
					<td><a href="<?php echo $viewLink;?>"><?php psafe($item->id)?><a></td>
					<td><a href="<?php echo $viewLink;?>"><?php psafe($item->created_at)?><a></td>
					<td><a href="<?php echo $viewLink;?>"><?php psafe($this->translate("transaction/paymentMethods/".$item->payment_method))?><a></td>
					<td><a href="<?php echo $viewLink;?>"><?php psafe($item->note)?><a></td>
					<td class="amountCol"><a href="<?php echo $viewLink;?>"><?php psafe($item->amount)?><a></td>
				</tr>
				<?php }?>
				<table class="groupedTransactionTotals">
					<tr><th class="paymentMethodCol"><?php echo $this->translate("transaction/paymentMethodSums");?></th><th class="amountCol"><?php echo $this->translate("transaction/amount");?></th></tr>
				<?php foreach($this->groupTotal as $type => $total){ ?>
					<tr>
						<td class="paymentMethodCol"><?php echo $this->translate("transaction/paymentMethods/".$type) ?></td>
						<td class="amountCol"><?php psafe(number_format($total,2,",","")) ?></td>
					</tr>
				<?php } ?>
					<tr class="totalRow">
						<td class="paymentMethodCol"><?php echo $this->translate("transaction/total") ?></td>
						<td class="amountCol"><?php psafe(number_format($this->grandTotal,2,",","")); ?></td>
					</tr>
				</table>
			</table>
		<?php }
	}