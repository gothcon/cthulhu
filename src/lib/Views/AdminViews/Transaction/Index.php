<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		/** @var Transaction **/
		protected $transaction;


		public function init(){
			$this->transaction = $this->getModel()->getTransaction();
			$this->cancelTransactionLink = $this->internalLink("transaction", $this->transaction->id."/".CONTROLLER_ACTION_CANCEL_TRANSACTION, false);
		}
		public function render(){?>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("general/details");?></h4>
				<dl class="transactionData">
					<dt><?php echo $this->translate("general/id")?>:</dt>
						<dd><?php psafe($this->transaction->id)?></dd>
					<dt><?php echo $this->translate("transaction/time")?>:</dt>
						<dd><?php psafe($this->transaction->created_at)?></dd>
					<dt><?php echo $this->translate("transaction/amount")?>:</dt>
						<dd><?php psafe(number_format($this->transaction->amount,2))?>:-</dd>
					<dt><?php echo $this->translate("transaction/registeredBy")?>:</dt>
						<dd><?php psafe($this->transaction->created_by)?></dd>
					<dt><?php echo $this->translate("transaction/updatedBy")?>:</dt>
						<dd><?php psafe($this->transaction->modified_by)?></dd>
					<dt><?php echo $this->translate("transaction/paymentMethod")?>:</dt>
						<dd><?php psafe($this->translate("transaction/paymentMethods/".$this->transaction->payment_method))?></dd>
					<dt><?php echo $this->translate("general/note")?>:</dt>
						<dd><?php psafe($this->transaction->note)?></dd>
				</dl>
			</div>
			<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("orderPayment/cancel");?></h4>
					<?php Html::DeleteButton($this->cancelTransactionLink,$this->translate("transaction/cancel"),$this->translate("transaction/cancelTransactionWarning"),array("class" => "deleteButton"));?>
				</div>
			</div>
			<?php }?>
		<?php }
	}