<?php
	require_once("lib/Views/HtmlView.php");
	class FormView extends HtmlView{
		public function init(){
			$this->image = $this->getModel()->getFile();
		}
		public function render(){?>
			<form method="post" enctype="multipart/form-data">
				<fieldset class="formBox">
				<?php if($this->image->id > 0){ ?>
					<span class="clear"></span>
					<img src ="<?php echo Application::getFilesRootUrl().$this->image->path.$this->image->name;?>">
					<img src="<?php echo $this->image->thumbnail_small;?>"/>
					<span class="clear"></span>
				<?php } ?>
				<?php Html::FileUpload("file",array("value" => "välj fil"));?>
				<?php Html::Label("nytt filnamn (frivilligt)", "new_filename");?>
				<?php Html::Text("name",$this->image->name);?>
				<?php Html::Label("katalog","path");?>
				<?php Html::Text("path",$this->image->path);?>
					<span class="clear"></span>
				<?php Html::SubmitButton("skicka");?>
				</fieldset>
			</form>
		<?php }
	}