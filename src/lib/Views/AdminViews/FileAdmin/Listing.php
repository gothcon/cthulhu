<?php
	require_once("lib/Views/HtmlListView.php");
	class ListingView extends HtmlListView{
		public function init(){
			$this->list = $this->getModel()->getList();
		}
		public function render(){?>
			<table class="files">

				<tr>
					<th><?php echo $this->translate("general/id");?></th>
					<th class="nameCol"><?php echo $this->translate("general/name");?></th>
					<th><?php echo $this->translate("filehandling/path");?></th>
					<th></th>
				</tr>
				<?php foreach($this->list as $item){
					$viewLink = $this->internalLink("fileAdmin",$item->id,false);
					$editLink = "#";
					$deleteLink = "#";
					?>
				<tr>
					<td><?php pnum($item->id)?></td>
					<td><a href="<?php echo $viewLink;?>"><img src="<?php echo $item->thumbnail_small;?>"/> <?php psafe($item->name)?><a></td>
					<td><?php psafe($item->path)?></td>
					<td></td>
				</tr>
				<?php }?>
			</table>
			<?php Html::LinkButton($this->translate("filehandling/new"), $this->internalLink("fileAdmin", CONTROLLER_ACTION_NEW,false));?>
		<?php }
	}