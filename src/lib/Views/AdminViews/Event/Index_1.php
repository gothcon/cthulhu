<?php
	require_once("lib/Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		private $returnPath;
		private $eventFormAction;
		private $deleteEventLink;
		private $deleteEventOccationLinkTemplate;
		private $newEventOccationFormAction;

		private $newSignupSlotFormAction;
		private $searchUrl;
		private $resourceLinkTemplate;
		private $deleteBookingLinkTemplate;
		
		
		public function init(){
			$this->event = $this->model->getEvent();
			$this->occations = $this->model->getOccations();
			$this->timespanSelectList = $this->model->getTimespanSelectList();
			$this->timespan = $this->model->getCurrentTimespan();
			$this->occation = $this->model->getCurrentOccation();
			$this->slotTypeSelectList = $this->model->getSlottypeSelectList();
			$this->signupSlots = $this->model->getSignupSlots();
			$this->signups = $this->model->getSignups();
			$this->organizer = $this->event->getOrganizer();
			$this->organizerLink = "";
			if($this->organizer != null){
				$orgType = $this->organizer->getOrganizerType();
				$this->organizerName = $this->organizer->getName();
				if($orgType == OrganizerType::$GROUP){
					$this->organizerLink = $this->internalLink("group",$this->organizer->group->id,false);
				}
				if($orgType == OrganizerType::$PERSON){
					$this->organizerLink = $this->internalLink("person",$this->organizer->person->id,false);
				}
			
			}else{
				$this->organizerLink = "";
				$this->organizerName = $this->translate("event/noOrganizerChosen");
				
			}
			
			$this->resourceBookings = $this->model->getResourceBookings();

			$this->returnPath							= urlencode($this->internalLink("event", "{$this->event->id}/#".Html::CreateHash($this->translate("event/eventOccationsAndSignups")), false));

			$this->eventFormAction						= $this->internalLink("event","{$this->event->id}/".CONTROLLER_ACTION_SAVE,false);
			$this->deleteEventLink						= $this->internalLink("event","{$this->event->id}/".CONTROLLER_ACTION_DELETE,false);
			
			$this->eventOccationFormAction				= $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_UPDATE_EVENT_OCCATION,false);
			$this->deleteEventOccationLinkTemplate		= $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_DELETE_EVENT_OCCATION,false);
			$this->newEventOccationFormAction			= $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_CREATE_EVENT_OCCATION,false);

			$this->newSignupSlotFormAction				= $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_CREATE_SIGNUP_SLOT,false);
			
			$this->signupFormActionTemplate				= $this->internalLink("signup",":signupId/".CONTROLLER_ACTION_SAVE.		"?returnPath={$this->returnPath}",false);
			$this->deleteSignupLinkTemplate				= $this->internalLink("signup",":signupId/".CONTROLLER_ACTION_DELETE.	"?returnPath={$this->returnPath}",false);
			
			$this->searchUrl							= $this->internalLink("search", "?format=json",false);
			
			$this->addResourceBookingLinkTemplate		= $this->internalLink("resource", CONTROLLER_ACTION_NEW."?event_occation_id=%s",false);
			$this->resourceLinkTemplate					= $this->internalLink("resource",":resourceId#Bokningar",false);
			
		}
		
		public function getSignupSlotFormAction($slotId){
			return $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_UPDATE_SIGNUP_SLOT."?signup_slot_id={$slotId}",false);
		}

		public function getDeleteSignupSlotLink($slotId){
			return $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_DELETE_SIGNUP_SLOT."?signup_slot_id={$slotId}",false);
		}
		
		public function getDeleteEventOccationLink($eventOccationId){
			return $this->internalLink("event",$this->event->id."/".CONTROLLER_ACTION_DELETE_EVENT_OCCATION."?event_occation_id={$eventOccationId}",false);
		}
		
		public function getResourceLink($resourceId){
			 return $this->internalLink("resource",num($resourceId)."#Bokningar",false);
		}
		
		public function getDeleteResourceBookingLink($bookingId){
			return $this->internalLink("eventOccationBooking", num($bookingId)."/".CONTROLLER_ACTION_DELETE,false);
		}
		
		public function render(){
			$eventTypes = $this->model->getEventTypeSelectList();
			?>
			<form method="post" action="<?php echo $this->eventFormAction?>" class="eventForm">
				<div class="section">
					<h2 class="sectionHeader"><?php echo $this->translate("general/details");?></h2>
						<fieldset>
							<span class="formBox">
								<?php Html::Label($this->translate("event/eventName"),"name");?>
								<?php Html::Text("name",$this->event->name); ?>
								<?php Html::Label($this->translate("event/eventType"),"eventtype_id");?>
								<?php Html::Select("eventtype_id",$eventTypes ,$this->event->eventtype_id); ?>
								<?php Html::Label($this->translate("event/organizer"),"organizer_id"); ?>
								<div id="organizer" style="float:left;"><?php 
									if($this->organizerLink){?><a href="<?php echo $this->organizerLink;?>"><?php }
										psafe($this->organizerName);
									if($this->organizerLink){?></a><?php }?></div>
								<input type="button" value="<?php echo $this->translate("general/choose");?>" id="selectOrganizer"/>
								<?php Html::Hidden("organizer_id",num($this->organizer->id));?>
								<?php Html::Hidden("organizer_group_id",num($this->organizer->group_id));?>
								<?php Html::Hidden("organizer_person_id",num($this->organizer->person_id));?>
							</span>
							<span class="formBox">
								<?php Html::Label($this->translate("general/internalInfo"),"internal_note");?>
								<?php Html::Text("internal_note",$this->event->internal_note,true); ?>
							</span>
						</fieldset>
						<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
				<div class="section">
					<h2 class="sectionHeader"><?php echo $this->translate("event/folderInfo");?></h2>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/preamble"),"preamble");?>
						<?php Html::Text("preamble",$this->event->preamble,true,array("class" =>"tiny")); ?>
					</div>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/text"),"text");?>
						<?php Html::Text("text",$this->event->text,true,array("class" =>"tiny")); ?>
					</div>
					<span class="clear"></span>
					<div class="tinyContainer">
						<?php Html::Label($this->translate("event/info"),"info");?>
						<?php Html::Text("info",$this->event->info,true,array("class" =>"tiny")); ?>
					</div>
					<span class="clear"></span>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("event/scheduleName"),"schedule_name");?>
							<?php Html::Text("schedule_name",$this->event->schedule_name,false ); ?>

							<?php Html::Label($this->translate("event/visibleInSchedule"),"visible_in_schedule");?>
							<span class="buttonBox">
								<?php Html::Radio("visible_in_schedule",1,$this->event->visible_in_schedule == 1);?> <?php echo $this->translate("general/yes");?>,
								<?php Html::Radio("visible_in_schedule",0,$this->event->visible_in_schedule != 1);?> <?php echo $this->translate("general/no");?>.	
							</span>
							<?php Html::Label($this->translate("event/visibleInPublicListing"),"visible_in_public_listing");?>
							<span class="buttonBox">
								<?php Html::Radio("visible_in_public_listing",1,$this->event->visible_in_public_listing == 1);?> <?php echo $this->translate("general/yes");?>,
								<?php Html::Radio("visible_in_public_listing",0,$this->event->visible_in_public_listing != 1);?> <?php echo $this->translate("general/no");?>.	
							</span>
						</span>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</form>
			<script type="text/javascript">
				$("#selectOrganizer").ajaxSelector({
					callback : function (data){
						if(data.length > 0){
							var $organizer = data[0];
							$("#organizer").html($organizer.id + ". " + $organizer.label + " (" + $organizer.type + ")");
							if($organizer.type == "person"){
								$("input[name=organizer_person_id]").val($organizer.id);
								$("input[name=organizer_group_id]").val(0);
							}else{
								$("input[name=organizer_group_id]").val($organizer.id);
								$("input[name=organizer_person_id]").val(0);
							}
						}
						else{
								$("input[name=organizer_person_id]").val(0);
								$("#organizer").html("<?php echo $this->translate("event/noOrganizerChosen");?>");
								$("input[name=organizer_group_id]").val(0);
						}
					},
					multiselect : false,
					serviceUrl: "<?php echo $this->searchUrl?>"
				});
			</script>
			<?php if($this->event->id > 0){ ?>
			<div class="section">
				<h2 class="sectionHeader"><?php echo $this->translate("event/eventOccationsAndSignups");?></h2>
				<ul id="eventOccationList">
					<?php foreach($this->occations as $eventOccation){ ?>
					<li class="pass">
						<h3 class="passnamn"><?php psafe($eventOccation->getName()) ?></h3>
<!-- Detaljformulärssektionen -->
						<div class="sektion">
							<div class="newEventOccationContainer">
								<h4><?php echo $this->translate("general/details");?></h4>
								<form action="<?php echo $this->eventOccationFormAction;?>" method="post" class="occationForm">
									<?php Html::Label($this->translate("timespan/timespan"), "timespan_id"); ?>
									<?php
										$adaptedTimespanList = $this->timespanSelectList;
										$adaptedTimespanList[] = array("label" => $eventOccation->getTimespan()->__toString(),"value" => $eventOccation->timespan_id);?>
									<?php Html::Select("timespan_id",$this->timespanSelectList /*$adaptedTimespanList*/,$eventOccation->timespan_id); ?>
									<div class="anpassat">
										<?php Html::Label($this->translate("timespan/startsAtDate"), "timespan_starts_at"); ?>
										<?php Html::Text("timespan_starts_at_date",$eventOccation->timespan->starts_at_date,false,array("id" => "startdatum","class" => "datum"));?>
										<?php Html::Label($this->translate("timespan/startsAtTime"), "timespan_starts_at_time"); ?>
										<?php Html::Text("timespan_starts_at_time",$eventOccation->timespan->starts_at_time,false,array("id" => "start_tid","class" => "tid"));?>
										<?php Html::Label($this->translate("timespan/endsAtDate"), "timespan_ends_at_date"); ?>
										<?php Html::Text("timespan_ends_at_date",$eventOccation->timespan->ends_at_date,false,array("id" => "slutdatum","class" => "datum"));?>
										<?php Html::Label($this->translate("timespan/endsAtTime"), "timespan_ends_at_time"); ?>
										<?php Html::Text("timespan_ends_at_time",$eventOccation->timespan->ends_at_time,false,array("id" => "slut_tid","class" => "tid"));?>
										<?php Html::Label($this->translate("timespan/name"), "timespan_name"); ?>
										<?php Html::Text("timespan_name",$eventOccation->timespan->name,false,array("class" => "intervallnamn"));?>
										<?php Html::Hidden("timespan_is_public",0)?>
									</div>
									<?php Html::Label($this->translate("eventOccation/name"), "event_occation_name"); ?>
									<?php Html::Text("event_occation_name",$eventOccation->name,false,array("class" => "passnamn"));?>
									<?php Html::Label($this->translate("eventOccation/isHidden"),"event_occation_is_hidden"); ?>
									<span class="buttonBox">
										<?php Html::Radio("event_occation_is_hidden",1,$eventOccation->is_hidden == 1,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
										<?php Html::Radio("event_occation_is_hidden",0,$eventOccation->is_hidden != 1,array("style" => "float:none; display: inline")); echo $this->translate("general/no")."."; ?>
									</span>
									<?php Html::Hidden("event_occation_id",$eventOccation->id);?>
									<span class="clear"></span>
									<div class="left">
										<?php Html::SubmitButton($this->translate("general/save"));?>
									</div>
									<div class="right">
										<?php Html::DeleteButton($this->getDeleteEventOccationLink($eventOccation->id),$this->translate("general/delete"),$this->translate("eventOccation/deleteEventOccationWarning"));?>
									</div>
								</form>
							</div>
							<span class="clear"></span>
						</div>
<!-- anmälnings- och bokningssektion -->
						<div class="sektion">
<!-- ny anmälningstypsblock -->
							<div class="newSignupTypeContainer">
								<form action="<?php echo $this->newSignupSlotFormAction ;?>" method="post" class="newSignupSlot">
									<h4><?php echo $this->translate("signupSlot/new");?></h4>
									<span class="clear"></span>
									<fieldset class="formFieldset">
										<span class="formBox">
											<?php Html::Hidden("event_occation_id",$eventOccation->id);?>
											<?php Html::Hidden("event_id",$eventOccation->event_id);?>
											<?php Html::Label($this->translate("signupSlot/type"),"slot_type_id"); ?>
											<?php Html::Select("slot_type_id",$this->slotTypeSelectList,0); ?>
											<?php Html::Label($this->translate("signupSlot/requiresApproval"),"requires_approval"); ?>
											<span class="buttonBox">
												<?php Html::Radio("requires_approval",1,false,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
												<?php Html::Radio("requires_approval",0,true,array("style" => "float:none; display: inline")); echo $this->translate("general/no").","; ?>
											</span>
											<?php Html::Label($this->translate("signupSlot/isHidden"),"is_hidden"); ?>
											<span class="buttonBox">
												<?php Html::Radio("is_hidden",1,false,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
												<?php Html::Radio("is_hidden",0,true,array("style" => "float:none; display: inline")); echo $this->translate("general/no").","; ?>
											</span>
											<?php Html::Label($this->translate("signupSlot/noOfSlots"), "maximum_signup_count"); ?>
											<?php Html::Text("maximum_signup_count","");?>
											<?php Html::Label($this->translate("signupSlot/noOfSpareSlots"), "maximum_signup_count"); ?>
											<?php Html::Text("maximum_spare_signup_count","");?>
											<?php Html::Label($this->translate("signupSlot/fee"), "article_price"); ?>
											<?php Html::Text("article_price","0");?>
										</span>
										<span class="clear"></span>
										<?php Html::SubmitButton($this->translate("general/save"));?>
									</fieldset>
								</form>
							</div>
							<span class="clear"></span>
<!-- Resursbokningsblock -->
							<div class="resourceBookingsContainer">
								<h4><?php echo $this->translate("resources/bookedResources");?></h4>
								<span class="clear"></span>
								<form action="<?php echo $this->eventOccationFormAction; ?>" method="post" class="mainResourceForm">
									<?php Html::Radio("event_occation_main_resource_booking_id",0, true, array("style" => "display:none"));?>
									<?php Html::Hidden("event_occation_id",$eventOccation->id);?>
								<table class="signupTable">
									<tr>
										<th class="nameCol"><?php echo $this->translate("resources/name");?></th>
										<th class="typeCol"><?php echo $this->translate("resources/type");?></th>
										<th class="isMainResourceCol"><?php echo $this->translate("resources/isMainResource");?></th>
										<th class="buttonCol"></th>
									</tr>
									<?php foreach($this->resourceBookings as $resourceBooking){
										if($eventOccation->id != $resourceBooking->event_occation_id)
											continue;
										$resource = $resourceBooking->getResource();
										$resourceType = $resource->resourceType;
										$booking = $resourceBooking->getBooking();
									?>
									<tr>
										<td class="nameCol"><a href="<?php echo $this->getResourceLink($resource->id)?>"><?php psafe($resource->name)?></a></td>
										<td class="typeCol"><?php psafe($resourceType->name)?></td>
										<td class="isMainResourceCol"><?php Html::Radio("event_occation_main_resource_booking_id", $resourceBooking->id, $resourceBooking->id == $eventOccation->main_booking_id);?></td>
										<td class="buttonCol"><?php 
											Html::DeleteButton($this->getDeleteResourceBookingLink($booking->id),$this->translate("general/delete"),$this->translate("resource/deleteBookingWarning"));?>
										</td>
									</tr>
									<?php } ?>
								</table>
									
								<hr/>
								<?php Html::SubmitButton($this->translate("resources/updateMainResource"), array("class" => "right"));?>
								<?php Html::LinkButton($this->translate("resourceBooking/addResourceBooking"),sprintf($this->addResourceBookingLinkTemplate,$eventOccation->id),array("class" => "resursSok"));?>
								</form>
							</div>
<!-- Anmälningstypsblock -->
							<?php foreach($this->signupSlots as $signupSlot){ 
								if($signupSlot->event_occation_id != $eventOccation->id)
									continue;
								$searchRepository = $signupSlot->getSlotType()->cardinality > 1 ? "groups" : "people" ;
								?>
								<div class="signupTypeContainer">
									<h4><?php echo $signupSlot->getSlotType()->name;?></h4>
									<span class="clear"></span>
									<form class="signupSlotForm" style="display:block" method="post" action="<?php echo $this->getSignupSlotFormAction($signupSlot->id);?>">
										<fieldset class="formFieldset">
											<span class="formBox">
											<?php Html::Hidden("id",$signupSlot->id);?>
											<?php Html::Hidden("event_occation_id",$eventOccation->id);?>
											<?php Html::Label($this->translate("signupSlot/type"),"slot_type_id"); ?>
											<?php Html::Select("slot_type_id",$this->slotTypeSelectList,$signupSlot->slot_type_id); ?>
											<?php Html::Label($this->translate("signupSlot/requiresApproval"),"requires_approval"); ?>
											<span class="buttonBox">
												<?php Html::Radio("requires_approval",1,$signupSlot->requires_approval == 1,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
												<?php Html::Radio("requires_approval",0,$signupSlot->requires_approval != 1,array("style" => "float:none; display: inline")); echo $this->translate("general/no")."."; ?>
											</span>
											<?php Html::Label($this->translate("signupSlot/isHidden"),"is_hidden"); ?>
											<span class="buttonBox">
												<?php Html::Radio("is_hidden",1,$signupSlot->is_hidden == 1,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
												<?php Html::Radio("is_hidden",0,$signupSlot->is_hidden != 1,array("style" => "float:none; display: inline")); echo $this->translate("general/no")."."; ?>
											</span>
											<?php Html::Label($this->translate("signupSlot/noOfSlots"), "maximum_signup_count"); ?>
											<?php Html::Text("maximum_signup_count",$signupSlot->maximum_signup_count);?>
											<?php Html::Label($this->translate("signupSlot/noOfSpareSlots"), "maximum_spare_signup_count"); ?>
											<?php Html::Text("maximum_spare_signup_count",$signupSlot->maximum_spare_signup_count);?>
											<?php Html::Label($this->translate("signupSlot/fee"), "article_price"); ?>
											<?php Html::Text("article_price",$signupSlot->getArticle()->price);?>
											</span>
										</span>
										</fieldset>
										<div class="clear">
										<?php Html::SubmitButton($this->translate("general/save"));?>
										<?php Html::DeleteButton($this->getDeleteSignupSlotLink($signupSlot->id),$this->translate("general/delete"),$this->translate("signupSlot/deleteSignupSlotWarning"),array("style" => "float:right;"));?>
										</div>
									</form>
									<?php Html::Hidden("signup_slot_id",$signupSlot->id);?>
									<?php Html::Hidden("searchRepository",$searchRepository);?>
									<span class="clear"></span>
									<table class="signupTable">
										<tr>
											<th class="nameCol"><?php echo $this->translate("signup/name");?></th>
											<th class="approvalCol"><?php echo $this->translate("signup/approved");?></th>
											<th class="approvalCol"><?php echo $this->translate("signup/paid");?></th>
											<th class="buttonCol"></th>
										</tr>
										<?php foreach($this->signups as $signup){
											if($signup->signup_slot_id != $signupSlot->id)
												continue;
											$name = !$signup->person_id ? ( $signup->group_id.". ".$signup->group->name) : ($signup->person_id.". ".$signup->person->first_name ." ".$signup->person->last_name);
											$link = $this->internalLink(!$signup->person_id ? "group" : "person", !$signup->person_id ? $signup->group_id."#Anmalningar" : $signup->person_id."#Anmalningar_och_lagmedlemskap", false);
										?>
										<tr>
											<td><a href="<?php echo $link;?>"><?php echo $name?></a></td>
											<td class="approvalCol">
												<?php if($signupSlot->requires_approval){?>
												<form method="post" action="<?php echo str_replace(":signupId" ,$signup->id,$this->signupFormActionTemplate);?>" class="signupForm ajaxForm">
													<?php 
													Html::Checkbox("is_approved",1,$signup->is_approved == 1,array("style" => "float:none; display: inline"));
													Html::SubmitButton($this->translate("general/update"));
													?>
												</form>
												<?php
												}else{
													echo " - ";
												}?>
											</td>
											<td class="approvalCol">
												<?php if($signupSlot->signup_slot_article->id){?>
													<form method="post" action="<?php echo str_replace(":signupId" ,$signup->id,$this->signupFormActionTemplate);?>" class="signupForm ajaxForm">
													<?php 
													Html::Checkbox("is_paid",1,$signup->is_paid == 1,array("style" => "float:none; display: inline"));
													Html::SubmitButton($this->translate("general/update"));
													?>
													</form>
													<?php
												}else{
													echo " - ";
												}?>
											</td>	
											<td><?php Html::DeleteButton(str_replace(":signupId",$signup->id,$this->deleteSignupLinkTemplate),$this->translate("general/delete"),$this->translate("signup/deleteSignupWarning"));?></td>
										</tr>
										<?php } ?>
									</table>
									<span class="clear"></span>
									<hr/>
									<?php Html::LinkButton($this->translate("signup/addSignup"),"#",array("class"=>"addSignupButton"));?>
								</div>
							<?php } ?>

						</div>
						<span class="clear"></span>
					</li>
					<?php } ?>
					<li class="pass">
<!-- Nytt pass-formulär  -->
						<?php $eventOccation = $this->model->getCurrentOccation();?>
						<h3 class="passnamn"><?php echo $this->translate("eventOccation/new");?></h3>
						<div class="sektion">
							<div class="newEventOccationContainer">
								<h4><?php echo $this->translate("general/details");?></h4>
								<form action="<?php echo $this->newEventOccationFormAction;?>" method="post" class="newEventOccationForm">
									<?php Html::Hidden("event_occation_event_id",$this->event->id)?>
									<?php Html::Hidden("event_occation_id",0)?>
									<?php Html::Label($this->translate("eventOccation/eventOccation"), "timespan_id"); ?>
									<?php Html::Select("timespan_id",$this->timespanSelectList,$this->occation->timespan_id); ?>
									<div class="anpassat">
										<?php Html::Label($this->translate("timespan/startsAtDate"), "timespan_starts_at"); ?>
										<?php Html::Text("timespan_starts_at_date",$this->timespan->starts_at_date,false,array("id" => "startdatum","class" => "datum"));?>
										<?php Html::Label($this->translate("timespan/startsAtTime"), "timespan_starts_at_time"); ?>
										<?php Html::Text("timespan_starts_at_time",$this->timespan->starts_at_time,false,array("id" => "start_tid","class" => "tid"));?>
										<?php Html::Label($this->translate("timespan/endsAtDate"), "timespan_ends_at_date"); ?>
										<?php Html::Text("timespan_ends_at_date",$this->timespan->ends_at_date,false,array("id" => "slutdatum","class" => "datum"));?>
										<?php Html::Label($this->translate("timespan/endsAtTime"), "timespan_ends_at_time"); ?>
										<?php Html::Text("timespan_ends_at_time",$this->timespan->ends_at_time,false,array("id" => "slut_tid","class" => "tid"));?>
										<?php Html::Label($this->translate("timespan/name"), "timespan_name"); ?>
										<?php Html::Text("timespan_name",$this->timespan->name,false,array("class" => "intervallnamn"));?>
										<?php Html::Hidden("timespan_is_public",0)?>
									</div>
									<?php Html::Label($this->translate("eventOccation/name"), "event_occation_name"); ?>
									<?php Html::Text("event_occation_name",$this->occation->name,false,array("class" => "passnamn"));?>
									<?php Html::Label($this->translate("eventOccation/isHidden"),"is_hidden"); ?>
									<span class="buttonBox">
										<?php Html::Radio("is_hidden",1,$this->occation->is_hidden == 1,array("style" => "float:none; display: inline")); echo $this->translate("general/yes").","; ?>
										<?php Html::Radio("is_hidden",0,$this->occation->is_hidden != 1,array("style" => "float:none; display: inline")); echo $this->translate("general/no")."."; ?>
									</span>

									<span class="clear"></span>
									<?php Html::SubmitButton($this->translate("general/create"));?>
									
								</form>
							</div>
							<span class="clear"></span>
						</div>
					</li>
				</ul>
				
				<span class="clear"></span>
			</div>
				<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
				<div class="section">
					<div class="sectionContent">
						<h4 class="sectionHeader"><?php echo $this->translate("general/delete");?></h4>
						<?php Html::DeleteButton($this->deleteEventLink,$this->translate("general/delete"),$this->translate("event/deleteEventWarning"),array("class" => "deleteButton"));?>
					</div>
				</div>			
				<?php } ?>
			<?php } ?>
			<script type="text/javascript">
				$(".signupTypeContainer").signupSlotForm();
				$(".newSignupTypeContainer").newSlotForm();
				$("li.pass").eventOccationForm();
				$(".eventForms").ajaxForm({
					saveCallback:function(data){
						var name = data.data.name;
						$("#mainContentTop h1").html("<?php echo $this->translate("event/event")?>: " + name);
					}
				});
				$(".resursSok").each(function(key,obj){
					var $obj = $(obj);
					var $table = $obj.siblings("table");
					var onSaveCallback = function(data){
						var resourceBooking = data.data;
						var resource = resourceBooking.booking.resource;
						var deleteUrl = resourceBookingOptions.deleteUrlTemplate.replace("{:resource_booking_id}",resourceBooking.booking.id);
						var resourceLink = "<?php $this->internalLink("resource","{:resource_id}");?>".replace("{:resource_id}",resource.id);
						var $nameCell = $("<td></td>").append($("<a href='"+resourceLink+"'>"+resource.name+"</a>"));
						var $deleteButton = $("<a href='" +deleteUrl + "' class='buttonLink deleteButton'>"+ "<?php echo $this->translate("general/delete")?>" +"<span class='end'></span></a>");
						$deleteButton.appendTo($("<td></td>"));
						var $resourceTypeCell = $("<td>"+resource.resource_type.name+"</td>");
						var $mainResourceRadioButton = $("<input class='radio' value='"+resourceBooking.id+"' name='event_occation_main_resource_booking_id' type='radio'>")
						var $inputCell = $("<td class='isMainResourceCol'></td>").append($mainResourceRadioButton);
						$("<tr></tr>").append($nameCell).append($resourceTypeCell).append($inputCell).append($("<td></td>").append($deleteButton)).appendTo($table);
						$deleteButton.ajaxDeleteResourceBooking({"deleteUrlTemplate" : resourceBookingOptions.deleteUrlTemplate});						
						$mainResourceRadioButton.autoSubmitOnEvent("click");
					};

					var resourceBookingOptions = {
						"serviceUrl" : "<?php $this->internalLink("search","?format=json&r=resources");?>",
						"ajaxSaveUrl" : "<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?format=json");?>",
						"deleteUrlTemplate" : "<?php $this->internalLink("eventOccationBooking","{:resource_booking_id}/".CONTROLLER_ACTION_DELETE."?format=json");?>",
						"onSaveCallback" : onSaveCallback
					};
					$obj.resourceBooker(resourceBookingOptions);
				});
				$(".signupTypeContainer").eventSignup({
					saveUrlTemplate : "<?php $this->internalLink("signup",CONTROLLER_ACTION_NEW."?format=json");?>",
					deleteUrlTemplate : "<?php $this->internalLink("signup","{:signup_id}/".CONTROLLER_ACTION_DELETE."?format=json");?>",
					searchUrlTemplate : "<?php $this->internalLink("search","?r={:repository}&format=json");?>",
					deleteButtonSelector : ".deleteButton"
				});
				$(".mainResourceForm").each(function(key,obj){
					var $obj = $(obj);
					$obj.find("input[type=radio]").autoSubmitOnEvent("click");
					$obj.ajaxForm({
						saveCallback:function(data){
							// console.log(data);
						}
					});
					$obj.find(".submitButton").hide();
				})
			</script>
		<?php }
	}