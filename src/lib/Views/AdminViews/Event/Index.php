<?php
	require_once("lib/Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		public function init(){
		}

		public function render(){?>
			<div>
				<div gc-loader></div>
				<div gc-tabs class="tabs-container"></div>
				<event class="tabbed-view"></event>
			</div>
		<?php }
	}