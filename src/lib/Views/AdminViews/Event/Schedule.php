<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("lib/Views/HtmlView.php");
require_once("lib/Database/ListItems/ListItem_EventOccationBooking.php");
class ScheduleView extends HtmlView{
	var $resourceSchedules;
	
	/** @var TreeItem_EventType[] */
	var $eventTree;
	
	public function init() {

		$this->eventTree = $this->getModel()->getResourceSchedules();
		
		$this->starts_at = strtotime($this->getModel()->getMasterTimespan()->starts_at);
		$this->ends_at = strtotime($this->getModel()->getMasterTimespan()->ends_at);
		
		$this->offset = 0;
		$this->master_start_hour = $this->getModel()->getMasterTimespan()->starts_at;
		$this->hour_width = 18;
		$this->total_width = (($this->ends_at - $this->starts_at) / 3600) * $this->hour_width ; 
	}
	public function render() {?>
	<div id="scheduleListContainer" style="width:<?php echo $this->total_width+200;?>px;">
		<ul id="dateList">
		<?php
			$j= 0;
			$dateBoxes = array();
			$current_time = $this->starts_at;
			$dateBox = new stdClass();
			$dateBox->offset = 0;
			$dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
			$dateBox->length_in_hours = 0;
			$currentDayOfMonth = strftime("%d",$current_time);
			while($current_time < $this->ends_at){
				if(strftime("%d",$current_time) != $currentDayOfMonth){
					$currentDayOfMonth = strftime("%d",$current_time);
					$dateBoxes[] = $dateBox;
					$new_offset = $dateBox->offset + $dateBox->length_in_hours;
					$dateBox = new stdClass();
					$dateBox->offset = $new_offset;
					$dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
					$dateBox->length_in_hours = 0;
				}
				$dateBox->length_in_hours++;
				$current_time += 3600;
			}
			$dateBoxes[] = $dateBox;
				
			foreach($dateBoxes as $dateBox){
				$width = $dateBox->length_in_hours*$this->hour_width;
				$label = $dateBox->label;
				$offset = $dateBox->offset * $this->hour_width;
				?>
				<li class="<?php echo $j++ %2 ?"even" :"odd";?>" style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
					<span class="dateLabel"><?php echo $label;?></span>
				</li>
				<?php
			}
			?>
		</ul>
		<ul id="hourList" style="width:<?php echo $this->total_width; ?>px;">
			<?php 
			$i = 0;
			for($currentTime = $this->starts_at; $currentTime < $this->ends_at; $currentTime += 3600){ 
				$width = $this->hour_width;
				$left = $this->hour_width*(($currentTime - $this->starts_at) / 3600);
				?>
			<li class="<?php echo $i++ %2 ?"even" :"odd";?>" style="width:<?php echo $width;?>px; left:<?php echo $left  ;?>px">
			<?php echo strftime("%H",$currentTime);?>
			</li>
			<?php } ?>
		</ul>
		<ul id="eventScheduleList">
		<?php
		foreach ($this->eventTree as $type) {

		foreach($type->events as $event){?>
			<li>
				<div class="resourceLabel">
					<a name="<?php echo $event->id;?>" href="<?php $this->internalLink("event", "{$event->id}");?>">
					<?php echo "{$event->name}";?>
					</a>
						<ul class="occationList">
							<?php foreach($event->event_occations as $booking){ 
								$offset = (((strtotime($booking->starts_at) - $this->starts_at)/3600) * $this->hour_width) ;
								$width = (((strtotime($booking->ends_at) - strtotime($booking->starts_at))/3600) * $this->hour_width-2);
							?>
							<li style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
								<a onClick="return false" href="<?php $this->internalLink("eventOccation", "{$booking->id}");?>"><?php echo $booking->starts_at ." ".$booking->ends_at;?></a>
							</li>
							<?php }?>
						</ul>

				</div>
				<ul class="eventList">
					<?php 
					$resourceList = array();
					foreach($event->event_occations as $occation){
						foreach($occation->booked_resources as $booking){
							if(!isset($resourceList[$booking->resource_name]))
								$resourceList[$booking->resource_name] = array();
							$eventOccationBooking = new ListItem_EventOccationBooking();
							$eventOccationBooking->booking_id = $booking->booking_id;
							$eventOccationBooking->resource_name = $booking->resource_name;
							$eventOccationBooking->starts_at = $occation->starts_at;
							$eventOccationBooking->ends_at = $occation->ends_at;
							$eventOccationBooking->event_occation_name = $eventOccationBooking->event_occation_name;
							$resourceList[$booking->resource_name][] = $eventOccationBooking;
						}
					}

					foreach($resourceList as $resourceName => $bookings){ 
					?>
					<li>
						<div class="eventLabel">
						<?php echo $resourceName; ?>
						</div>
						<ul class="bookingList">
							
							<?php foreach($bookings as $booking){ 
								
								
								$offset = (((strtotime($booking->starts_at) - $this->starts_at)/3600) * $this->hour_width)  ;
								$width = (((strtotime($booking->ends_at) - strtotime($booking->starts_at))/3600) * $this->hour_width-2);
								$returnUrl = urlencode($this->internalLink("event", "?format=schedule#{$event->id}",false));
								$deleteLink = $this->internalLink("eventOccationBooking", "{$booking->booking_id}/".CONTROLLER_ACTION_DELETE."?returnPath={$returnUrl}",false);
							?>
							<li style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
								<a onClick="return confirm('<?php echo $this->translate("resourceBooking/deleteWarning");?>');"  href="<?php echo $deleteLink; ?>"><?php echo $booking->starts_at ." ".$booking->ends_at;?></a>
							</li>
							<?php } ?>
						</ul>
						<span class="clear"></span>
					</li>
					
				<?php }?>
				</ul>
				<span class="clear"></span>
			</li>
		<?php }
		}
		?>
		</ul>
		<script>
			(function(){
				
				var $resourceTableBody;
				var $resourceTable;
				var $resourceTableHead;
				var $form;
				var $dialogue;
				var $okButton;
				var $cancelButton;
				
				function createStructure(){
					$dialogue = $("<div id='lbDialogue'></div>").appendTo($("body"));
					$form = $("<form method='post' action='<?php $this->internalLink("booking",CONTROLLER_ACTION_NEW."?multiple=true&event_id={:event_id}");?>'></form>").appendTo($dialogue);
					var $scrollContainer = $("<div class='scrollContainer'></div>").appendTo($form);
					$resourceTable = $("<table class='resourceList tablesorter'></table>").appendTo($scrollContainer);
					$resourceTableHead = $("<thead>").appendTo($resourceTable).append(createTableHeaderRow());
					$resourceTableBody = $("<tbody>").appendTo($resourceTable);
					$resourceTable.tablesorter({
						headers: {
							0:{ sorter:false},
							1:{ sorter:false}
						}
					});
					$okButton = $("<input class='ajaxSelector_okButton submitButton' type='submit' value='ok'>").appendTo($form);
					$cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>").appendTo($form);
					$("<span class='clear'></span>").appendTo($dialogue);
					
					$okButton.click(function(event){
						$dialogue.closeDOMWindow();
					});
					
					$cancelButton.click(function(event){
						$dialogue.closeDOMWindow();
					});
				}
				
				function createTableHeaderRow(){
					var $row = $("<tr>");
					$("<th class='bookingCheckboxHeader'>Boka</th>").appendTo($row);
					$("<th class='mainResourceRadioHeader'>Samlingsplats</th>").appendTo($row);
					$("<th class='resourceHeader'>Platsnamn</th>").appendTo($row);
					$("<th>Platstyp</th>").appendTo($row);
					return $row;
				}
				
				function init(){
					createStructure();
					$(".resourceLabel > a").each(function(key,obj){
						var $resourceLink = $(obj);
						var resourceLinkClickHandler = createResourceClickHandler($resourceLink);
						$resourceLink.click(resourceLinkClickHandler);
					});
					
					$(".occationList > li > a").each(function(key,obj){
						var $occationLink = $(obj);
						var linkClickHandler = createOccationLinkClickHandler($occationLink);
						$occationLink.click(linkClickHandler);
					});
				}
				
				function createOccationLinkClickHandler($occationLink){
					var $eventList = $($occationLink.closest(".resourceLabel").siblings(".eventList"));
					var onSaveCallback = createOnSaveCallback($eventList);
					return function(event){
						$form.ajaxForm({"preventBuiltInCallback" : true, "saveCallback" : onSaveCallback });
						event.preventDefault();
						event.stopPropagation();
						var pattern = /\/(\d+)(\/)?#?/g
						var eventOccationId = pattern.exec($occationLink.attr("href"))[1];
						$form.attr("action","<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?multiple=true&event_occation_id={:event_occation_id}");?>".replace("{:event_occation_id}",eventOccationId));
						fillDialogue(eventOccationId, "occationId");
						$occationLink.openDOMWindow({modal : true, width:500, height:450, windowSourceID : "#lbDialogue" });
					}
				}
				
				function createResourceClickHandler($resourceLink){
					var $eventList = $($resourceLink.closest("li").find(".eventList"));
					var onSaveCallback = createOnSaveCallback($eventList);
					return function(event){
						$form.ajaxForm({"preventBuiltInCallback" : true, "saveCallback" : onSaveCallback });
						event.preventDefault();
						event.stopPropagation();
						var pattern = /\/(\d+)(\/)?#?/g
						var eventId = pattern.exec($resourceLink.attr("href"))[1];
						$form.attr("action","<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?multiple=true&event_id={:event_id}");?>".replace("{:event_id}",eventId));
						fillDialogue(eventId,"eventId");
						$resourceLink.openDOMWindow({modal : true, width:400, height:450, windowSourceID : "#lbDialogue" });
					}
				}
				
				function createOnSaveCallback($eventList){
					return function(form,data){
						var resourceSchedule = data.data.resourceSchedule;
						var masterTimespan = data.data.masterTimespan;
						var startsAt = new Date(masterTimespan.starts_at);

						$eventList.html("");

						for(resourceName in resourceSchedule){
							var bookings = resourceSchedule[resourceName];
							var $listItem = $("<li></li>").appendTo($eventList);
							// $("<span class='clear'></span>").appendTo($eventList);
							$label = $("<div class='eventLabel'>"+resourceName+"</div>").appendTo($listItem);
							var $bookingList = $("<ul class='bookingList'></ul>").appendTo($listItem);
							for(bookingKey in bookings){
								var booking = bookings[bookingKey];
								var bookingStartsAt = new Date(booking.starts_at);
								var bookingEndsAt = new Date(booking.ends_at);
								var width = ((((bookingEndsAt / 1000)- (bookingStartsAt/1000)) / 3600) * 18) - 2;
								var offset = (((bookingStartsAt / 1000)- (startsAt/1000)) / 3600) * 18;

								$link = $("<a href='#'>"+booking.booking_id+"</a>");
								$innerListItem = $("<li style='width:"+width+"px; left:"+offset+"px'></li>").append($link).appendTo($bookingList);
								$link.click(createDeleteClickHandler($innerListItem,booking.booking_id));
							}
							$("<span class='clear'></span>").appendTo($listItem);
						}
					}
				}

				function createResourceListItem(resource){
					var attributeString = "";
					console.log(resource);
					var cssClass = ( resource.is_available == "0"  || resource.is_already_booked == "1")? " unavailable" : "";
					// console.log("alreadyBooked = " + resource.alreadyBooked);
					if(resource.is_already_booked == 1){
						attributeString = "disabled='disabled' checked='checked' ";
					}
					var $row = $("<tr class='eventOccation"+cssClass+"'>");
					$("<td><input "+ attributeString +"type='checkbox' name='resource[]' value='"+ resource.resource_id +"'/></td>").appendTo($row);
					$("<td><input type='radio' name='mainResourceId' value='"+ resource.resource_id +"'/></td>").appendTo($row);
					$("<td>"+resource.resource+ "</td>").appendTo($row);
					$("<td>"+ resource.resource_type +"</td>").appendTo($row);
					return $row;
				}

				function fillDialogue(identifier,type){
					var url;
					console.log(type);
					if(type == "occationId"){
						url = "<?php $this->internalLink("eventOccationBooking","?format=json&eventOccationId=")?>"+ identifier;
					}else{
						url = "<?php $this->internalLink("eventOccationBooking","?format=json&eventId=")?>"+ identifier;
					}
					$.ajax({
						url: url,
						dataType: 'json',
						success: function(data){
							$resourceTableBody.html("");
							var resourceList = data.data;
							for(resourceKey in resourceList){
								var resource = resourceList[resourceKey];
								createResourceListItem(resource).appendTo($resourceTableBody);
							}
							$resourceTable.trigger("update");
						}
					});	
				}
				
				init();

				$(".bookingList li").each(function(key,obj){
					var $listItem = $(obj);
					$listItem.find("a").click(createDeleteClickHandler($listItem));
				});

				function createDeleteClickHandler($listItem,id){
					
					var $link = $listItem.find("a");
					$link.removeProp("onclick");
					
					if(!id){
						var pattern = /\/(\d+)\/<?php echo $this->translate("actions/delete"); ?>/g
						var id = pattern.exec($link.attr("href"))[1];
					}
					
					var callback = function(){
						if($listItem.siblings().length == 0){
							$listItem.parent().closest("li").remove();
						}else{
							$listItem.remove();
						}
					}
					
					return function(event){
						event.stopPropagation();
						event.preventDefault();
						if(confirm($.translate("resourceBooking/deleteWarning"))){
							var url = "<?php $this->internalLink("booking", "%s/" .CONTROLLER_ACTION_DELETE."?format=json");?>".replace("%s", id);							
							$.ajax({
							  url: url,
							  success: callback,
							  type:"get"
							});
						}
					};
				}

			})();
		</script>
	</div>
<?php
	}
}