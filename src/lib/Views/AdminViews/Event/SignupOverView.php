<?php
	require_once("lib/Views/HtmlView.php");
	class SignupOverView extends HtmlView{
		/** @var TreeItem_EventType[] */
		var $eventTree;
		
		public function printEventLink($id){
			$this->internalLink("event", $id);
		}
		
		public function printEventOccationLink($id){
			$this->internalLink("event", "{$id}/#" . Html::CreateHash(Translator::translate("event/eventOccationsAndSignups")));
		}
		
		public function createSignupSlotLink($signupSlotId){
			return $this->internalLink("signupSlot", $signupSlotId,false);
		}
		
		public function init(){
			$this->eventTree = $this->getModel()->getEventTree();
		}
		public function render(){?>
			<ul>
				<?php foreach($this->eventTree as $eventType){ ?>
					<?php foreach($eventType->events as $event){ ?>
					<li>
						<h2><a href="<?php $this->printEventLink($event->id);?>"><?php phtml($event->name);?></a></h2>
						<div class="signupTableContainer" style="margin-left:20px">
							<table>
							<?php 
								$eventOccations = array();
								$signupSlotTypes = array();

								foreach($event->event_occations as $eventOccation){
									foreach($eventOccation->signup_slots as $slot){
										if(!isset($signupSlotTypes[$slot->slot_type_id])){
											$signupSlotTypes[$slot->slot_type_id] = $slot->slot_type_name;
										}
									}
								}?>
								<thead>
									<tr>
										<th style="width:300px">Arrangemangspass</th>
										<th style="width:200px">Inbokade platser</th>
										<?php foreach($signupSlotTypes as $slotType){ phtml("<th class='signupCountCol'>{$slotType}</th>"); }?>
									</tr>
								</thead>
								<tbody>
									<?php foreach($event->event_occations as $eventOccation){ 
										$signups_per_slot = array();
										$signupSlotIdPerSlot = array();
										foreach($eventOccation->signup_slots as $slot){
											$signups_per_slot[$slot->slot_type_id] = count($slot->signups);
											$signupSlotIdPerSlot[$slot->slot_type_id] = $slot->id;
										}
									?>
									<tr>
										<td><a href="<?php $this->printEventOccationLink($event->id)?>"><?php psafe($eventOccation->full_name);?></a></td>
										<td style="width:200px"><?php psafe(count($eventOccation->booked_resources));?></td>
										<?php foreach($signupSlotTypes as $slot_type_id => $slotType){ ?>
										<td class="signupCountCol" style="width:200px"><?php phtml(isset($signups_per_slot[$slot_type_id]) ? "<!-- a class='signupSlotLink' href='".$this->createSignupSlotLink($signupSlotIdPerSlot[$slot_type_id])."' -->".$signups_per_slot[$slot_type_id]."<!--/a-->" :"-")?></td>
										<?php }?>
										<td class="status"></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</li>
					<?php }?>
				<?php }?>
			</ul>
			<script>
				$(".signupSlotLink").each(function(key,obj){
					/*
					var $link = $(obj);
					$link.addClass("addButton");
					$("<span class='signupCount'>"+$link.html()+"</span>").insertBefore($link);
					var onClick = function(event){
						event.preventDefault();
						event.stopPropagation();
						var signupSlotId = $link.attr("href");
						$.log(signupSlotId);
						return false;
					}
					$link.bind("click",onClick);
					*/
				});
				
					
			</script>
	<?php }
	}