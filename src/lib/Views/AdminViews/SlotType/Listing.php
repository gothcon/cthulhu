<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->slotTypeList = $this->model->getSlotTypeList();
		}	
		public function render(){
			?>
			<table id="slotTypeList">
				<tr>
					<th class="idColumn"><?php echo $this->translate("slotType/id");?></th>
					<th class="nameCol"><?php echo $this->translate("slotType/name");?></th>
					<th class="nameCol">Antal anmälningar</th>
					<th class="nameCol">Antal anmälda personer/grupper</th>
					<th></th>
				</tr>
				<?php foreach($this->slotTypeList as $slotType){ 
					$r=0;?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->internalLink("slotType", num($slotType->id)."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($slotType->id)?></a></a></td>
						<td><a href="<?php $this->internalLink("slotType", num($slotType->id)."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($slotType->name)?></a></a></td>
						<td><a href="<?php $this->internalLink("slotType", num($slotType->id)."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($slotType->signupCount)?></a></a></td>
						<td><a href="<?php $this->internalLink("slotType", num($slotType->id)."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($slotType->uniqueSignupCount)?></a></a></td>
						<td></td>
					</tr>
				<?php } ?>
			</table>
			<?php Html::LinkButton($this->translate("slotType/new"),$this->internalLink("slotType", CONTROLLER_ACTION_NEW,false));?>
		<?php }
	}