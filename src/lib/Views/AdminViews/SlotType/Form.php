<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		public function init(){
			$this->slotType = $this->model->getSlotType();
		}
		public function render(){
			?>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("general/details")?></h4>
				<form method="post">
				<fieldset>
					<span class="formBox">
						<?php Html::Label($this->translate("slotType/name"),"name");?>
						<?php Html::Text("name",$this->slotType->name); ?>
						<?php Html::Label($this->translate("slotType/cardinality"),"cardinality");?>
						<?php Html::Text("cardinality",$this->slotType->cardinality); ?>
					</span>
				</fieldset>
				<?php Html::SubmitButton($this->translate("general/save"));?>
				</form>
			</div>
			<?php if($this->slotType->id && UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/delete")?></h4>
					<?php Html::DeleteButton($this->internalLink("article","{$this->slotType->id}/".CONTROLLER_ACTION_DELETE,false),$this->translate("slotType/delete"),$this->translate("slotType/deleteWarning"));?>
				</div>
			</div>
				
		<?php 
			}
		}
	}