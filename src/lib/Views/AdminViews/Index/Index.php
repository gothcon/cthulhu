<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	
	class IndexView extends HtmlView{
		public function init(){
			$this->newsArticles = $this->model->getNewsArticles();
			
		}
		public function render(){?>
		<div id="nyheter" class="sectionContent">
		<?php 
			$r=0;
			?>
			<ul class="news-list">
				<?php foreach($this->newsArticles as $newsArticle){?>
				<li class="html">
					<h2><?php psafe($newsArticle->title);?></h2>
					<div class="author"><?php psafe("{$newsArticle->publish_at} av {$newsArticle->author}");?></div>
					<div class="preamble"><?php phtml($newsArticle->preamble)?></div>
					<div class="text"><?php phtml($newsArticle->text);?></div>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php }
	}