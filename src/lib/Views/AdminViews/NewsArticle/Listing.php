<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->newsArticles = $this->model->getNewsArticles();
		}
		public function render(){
			
			$r=0;
			?>
			<table class="newsArticles">
				<tr class="even">
					<th class="idCol"><?php echo $this->translate("news/id");?></th>
					<th class="titleCol"><?php echo $this->translate("news/title");?></th>
					<th class="authorCol"><?php echo $this->translate("news/author");?></th>
					<th class="isPublishedCol"><?php echo $this->translate("news/isPublished");?></th>
					<th class="isInternalCol"><?php echo $this->translate("news/internal");?></th>
					<th class="buttonCol"></th>
				</tr>
				<?php foreach($this->newsArticles as $newsArticle){?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($newsArticle->id)?></a></td>
						<td><a href="<?php $this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($newsArticle->title)?></a></td>
						<td><a href="<?php $this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_EDIT)?>"><?php psafe($newsArticle->author)?></a></td>
						<td><a href="<?php $this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_EDIT)?>"><?php echo $newsArticle->isPublished()? "ja, ".safe($newsArticle->publish_at) : "nej";?></a></td>
						<td><a href="<?php $this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_EDIT)?>"><?php echo $newsArticle->is_internal ? "ja" : "nej";?></a></td>
						<td>
							<?php Html::DeleteButton($this->internalLink("news", $newsArticle->id."/".CONTROLLER_ACTION_DELETE,false),$this->translate("general/delete"),$this->translate("news/deleteWarning"),array("class" => "deleteButton"));?>
						</td>
					</tr>
				<?php } ?>
			</table>
			<?php Html::LinkButton($this->translate("news/newArticle"),$this->internalLink("news", CONTROLLER_ACTION_NEW,false))?>
			<script type="text/javascript">
				$(".eventTypeList").eventTypeList();
			</script>
			
		<?php }
	}