<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		public function init(){
			$this->newsArticle = $this->getModel()->getNewsArticle();
		}
		public function render(){
			?>
			<form method="post">
			<div class="section">
				<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("general/details");?></h2>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("news/title"),"title");?>
							<?php Html::Text("title",$this->newsArticle->title); ?>

							<?php Html::Label($this->translate("news/author"),"author");?>
							<?php Html::Text("author",$this->newsArticle->author); ?>
							</span>
					</fieldset>
						<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</div>
			<div class="section">
				<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("news/content");?></h2>
					<fieldset>
						<span class="clear"></span>
						<?php Html::Label($this->translate("news/preamble"),"name");?>
						<?php Html::Text("preamble",$this->newsArticle->preamble,true,array("class" => "tiny")); ?>
						<?php Html::Label($this->translate("news/text"),"name");?>
						<?php Html::Text("text",$this->newsArticle->text,true,array("class" => "tiny")); ?>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</div>
			<div class="section">
				<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("news/publishing");?></h2>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("news/startPublishDate"),"publish_at_date");?>
							<?php Html::Text("publish_at_date",$this->newsArticle->publish_at_date,false,array("id" => "startdatum")); ?>
							<?php Html::Label($this->translate("news/startPublishTime"),"publish_at_time");?>
							<?php Html::Text("publish_at_time",$this->newsArticle->publish_at_time); ?>
							<?php Html::Label($this->translate("news/endPublishDate"),"unpublish_at_date");?>
							<?php Html::Text("unpublish_at_date",$this->newsArticle->unpublish_at_date,false,array("id" => "slutdatum")); ?>
							<?php Html::Label($this->translate("news/endPublishTime"),"unpublish_at_time");?>
							<?php Html::Text("unpublish_at_time",$this->newsArticle->unpublish_at_time); ?>
							<?php Html::Label($this->translate("news/internal"),"is_internal");?>
							<span class="buttonBox">
								<?php Html::Radio("is_internal",1, $this->newsArticle->is_internal);?> <?php echo $this->translate("general/yes");?>
								<?php Html::Radio("is_internal",0, !$this->newsArticle->is_internal);?><?php echo $this->translate("general/no");?>
							</span>
						</span>
					</fieldset>
						<?php Html::SubmitButton($this->translate("general/save"));?>
				</div>
			</div>
			</form>
			<?php if($this->newsArticle->id > 0 && UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>			
			<div class="section">
				<div class="sectionContent">
					<h2 class="sectionHeader"><?php echo $this->translate("general/delete");?></h2>
					<?php Html::DeleteButton($this->internalLink("news", $this->newsArticle->id."/".CONTROLLER_ACTION_DELETE,false),$this->translate("general/delete"),$this->translate("news/deleteWarning"),array("class" => "deleteButton"));?>

				</div>
			</div>
			<?php } ?>
		<?php }
	}