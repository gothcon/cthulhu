<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		public function init(){
			
			$this->resource = $this->model->getResource();
			$this->events = $this->model->getEvents();
			$this->eventOccations = $this->model->getEventOccations();
			
			array_unshift($this->events,array("value"=>"0","label"=>"Välj arrangemang"));
			array_unshift($this->eventOccations,array("value"=>"0","label"=>"Välj arrangemangspass"));
			$this->eventOccationAttributes = count($this->eventOccations) > 1 ? array() : array("disabled" => "disabled");
			$this->eventAttributes = array();
			$this->eventAttributes["onchange"] = "$(this).closest(\"form\").submit()";
			
		}
		public function render(){
		?>
			<div class="section">
				<h4 class="sectionHeader">Detaljer</h4>
				<form method="post">
					<fieldset>
						<span class="leftBox">
							<?php Html::Radio("bokningstyp","arrangemangspass",true); ?>Arrangemangspass
							<span class="clear"></span>
							<?php Html::Label("Arrangemang","event_id");?>
							<?php Html::Select("event_id",$this->events,-1,$this->eventAttributes); ?>
							<div class="hideIfJavascript">
							<span class="clear"></span>
							<?php Html::Label("&nbsp;","event_id");?>
							<?php Html::SubmitButton("Uppdatera arrangemangspass");?>
							<span class="clear"></span>
							</div>
							<?php Html::Label("Arrangemangspass","event_occation_id");?>
							<?php Html::Select("event_occation_id",$this->eventOccations, "" , $this->eventOccationAttributes ); ?>
							<span class="clear"></span>
							<hr/>
							<?php Html::Radio("bokningstyp","standardpass"); ?>Standardpass
							<span class="clear"></span>
							<?php Html::Label("Tidsrymd","timespan_id");?>
							<?php Html::Select("timespan_id",array(array("value"=>"0","label"=>"Välj pass"))); ?>
							<span class="clear"></span>
							<?php Html::Label("Beskrivning","note");?>
							<?php Html::Text("note",""); ?>
						</span>
					</fieldset>
					<?php Html::SubmitButton("Spara");?>
				</form>
				
				
				
			</div>
			
			
		<?php }
	}