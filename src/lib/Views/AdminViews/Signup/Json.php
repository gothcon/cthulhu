<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class JsonView extends View{
		public function init(){
			$this->signups = $this->model->getSignups();
			
		}
		
		public function render(){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');
			echo json_encode($this->signups);
		}
	}