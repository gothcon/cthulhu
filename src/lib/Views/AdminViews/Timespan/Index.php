<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		public function init(){}
		public function render(){?>
			<div>
				<div gc-loader></div>
				<div gc-tabs class="tabs-container"></div>
				<timespan class="tabbed-view"></timespan>
			</div>
		<?php }
	}