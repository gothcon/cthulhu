<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		public function init(){
			$this->timespan = $this->model->getTimespan();
			$this->formAction = $this->timespan->id > 0 ? $this->internalLink("timespan",num($this->timespan->id)."/".CONTROLLER_ACTION_SAVE,false) : $this->internalLink("timespan",CONTROLLER_ACTION_NEW,false); 
			$this->submitButtonLabel = num($this->timespan->id) > 0 ? $this->translate("general/update") : $this->translate("general/save");
		}
		public function render(){
			?>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("general/details");?></h4>
						<form method="post" action="<?php echo $this->formAction;?>">
						<fieldset>
							<span class="formBox">
								<?php Html::Label($this->translate("timespan/name"),"name");?>
								<?php Html::Text("name",$this->timespan->name); ?>
								<?php Html::Label($this->translate("timespan/startsAtDate"), "starts_at"); ?>
								<?php Html::Text("starts_at_date",$this->timespan->starts_at_date,false,array("id" => "startdatum","class" => "datum"));?>
								<?php Html::Label($this->translate("timespan/startsAtTime"), "starts_at_time"); ?>
								<?php Html::Text("starts_at_time",$this->timespan->starts_at_time,false,array("id" => "start_tid","class" => "tid"));?>
								<?php Html::Label($this->translate("timespan/endsAtDate"), "ends_at_date"); ?>
								<?php Html::Text("ends_at_date",$this->timespan->ends_at_date,false,array("id" => "slutdatum","class" => "datum"));?>
								<?php Html::Label($this->translate("timespan/endsAtTime"), "ends_at_time"); ?>
								<?php Html::Text("ends_at_time",$this->timespan->ends_at_time,false,array("id" => "slut_tid","class" => "tid"));?>
								<?php Html::Label($this->translate("timespan/visibleInSchedule"),"show_in_schedule"); ?>
								<span class="buttonBox">
									<?php Html::Radio("show_in_schedule",1,$this->timespan->show_in_schedule == 1,array("style" => "float:none; display: inline")); echo $this->translate("general/yes"); ?>
									<?php Html::Radio("show_in_schedule",0,$this->timespan->show_in_schedule != 1,array("style" => "float:none; display: inline")); echo $this->translate("general/no")."."; ?>
								</span>
								<?php Html::Hidden("id",$this->timespan->id);?>
								<?php Html::Hidden("is_public",true);?>
							</span>
						</fieldset>
						<?php Html::SubmitButton($this->submitButtonLabel);?>
						</form>
			</div>
			<?php if($this->timespan->id > 0 && UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/delete");?></h4>
					<?php Html::DeleteButton($this->internalLink("timespan","{$this->timespan->id}/".CONTROLLER_ACTION_DELETE,false),$this->translate("timespan/delete"),$this->translate("timespan/deleteWarning"));?>
				</div>
			</div>			
			<?php } ?>
		<?php }
	}