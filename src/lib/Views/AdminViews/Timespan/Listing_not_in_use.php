<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->timespanList = $this->model->getTimespanList();
		}
		
		public function printTimespanEditLink($timespanId){
			return $this->internalLink("timespan", num($timespanId)."/".CONTROLLER_ACTION_EDIT);
		}
		
		public function render(){
			?>
			
			<table id="timespanList">
				<tr>
					<th class="idCol"><?php echo $this->translate("timespan/id");?></th>
					<th class="nameCol"><?php echo $this->translate("timespan/name");?></th>
					<th><?php echo $this->translate("timespan/startsAt");?></th>
					<th><?php echo $this->translate("timespan/endsAt");?></th>
				</tr>
				<?php foreach($this->timespanList as $timespan){
					$r=0;?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td class="idCol"><a href="<?php $this->printTimespanEditLink($timespan->id) ?>"><?php psafe($timespan->id)?></a></></td>
						<td><a href="<?php $this->printTimespanEditLink($timespan->id) ?>"><?php psafe($timespan->name)?></a></td>
						<td><a href="<?php $this->printTimespanEditLink($timespan->id) ?>"><?php psafe($timespan->starts_at)?></a></td>
						<td><a href="<?php $this->printTimespanEditLink($timespan->id) ?>"><?php psafe($timespan->ends_at)?></a></td>
					</tr>
				<?php } ?>
			</table>
			<?php Html::LinkButton($this->translate("timespan/new"),$this->internalLink("timespan", CONTROLLER_ACTION_NEW,false));
		}
	}