<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class JsonView extends View{
		public function init(){
			$this->personList = $this->model->getPersonList();
			for($i = 0; $i < count($this->personList); $i++){
				$person = $this->personList[$i];
				$this->personList[$i] = (object)array();
				$this->personList[$i]->first_name = utf8_encode($person->first_name);
				$this->personList[$i]->last_name = utf8_encode($person->last_name);
				$this->personList[$i]->id = utf8_encode($person->id);
			}
		}
		
		/** @return JsonModel */
		public function getModel() {
			return parent::getModel();
		}
		
		public function render(){
			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json');
			echo json_encode($this->personList);
		}
	}