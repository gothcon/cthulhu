<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
require_once("lib/ClassLib/Html.php");
class FormView extends HtmlView{

    /** @var Person */
    private $person;

    /** @var User */
    private $user;


    public function init(){

    }
    public function render(){?>
		<div>
			<div id="initial-person-data" style="display:none"><?php echo $this->getModel()->getPerson(); ?></div>
			<div gc-loader></div>
			<div gc-tabs class="tabs-container"></div>
			<people class="tabbed-view"></people>
		</div>
	<?php }
}