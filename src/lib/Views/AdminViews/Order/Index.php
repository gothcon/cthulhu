<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		public function init(){
			$this->order = $this->model->getOrder();
			$this->orderRows = $this->order->getOrderRows();
			$this->orderRowStatusValues = $this->model->getOrderRowStatusValues();
			$this->orderStatusValues = $this->model->getOrderStatusValues();
			$this->orderPayments = $this->model->getOrderPayments();
			$this->remainingSum = $this->model->getRemainingSum();
		}
		public function render(){?>
			<form method="post">			
				<div class="section">
					<div class="sectionContent">
						<h4 class="sectionHeader"><?php echo $this->translate("general/details")?></h4>
						<dl class="orderData">
							<dt><?php echo $this->translate("order/orderTotal")?>:</dt>
								<dd><?php psafe(number_format($this->order->total,2))?>:-</dd>
							<dt><?php echo $this->translate("order/remaining")?>:</dt>
								<dd><?php psafe(number_format($this->remainingSum,2))?>:-</dd>
							<dt><?php echo $this->translate("order/date")?>:</dt>
								<dd><?php psafe($this->order->submitted_at)?></dd>
							<dt><?php echo $this->translate("order/status")?>:</dt>
								<dd><?php Html::Select("status",$this->orderStatusValues,$this->order->status);?></dd>
							<dt><?php echo $this->translate("order/deliveryStatus")?>:</dt>
							<dd><?php psafe($this->translate("order/orderStatus/".$this->order->delivery_status))?></dd>
							<dt><?php echo $this->translate("order/customer")?>:</dt>
								<dd><a href="<?php $this->internalLink("person", num($this->order->getPerson()->id)."#Bestallningar");?>" id="customerLink"><?php psafe($this->order->getPerson()->first_name." ".$this->order->getPerson()->last_name ." (".$this->order->getPerson()->id.")")?></a></dd>
						</dl>
						<span class="clear"></span>
						<?php Html::SubmitButton(strlen($this->order->comment) ? $this->translate("general/update") : $this->translate("general/save"));?>
					</div>
				</div>
				<script type="text/javascript">
					var $customerLink = $("#customerLink");
					var pattern = /\/(\d+)#/g;
					$input = $("<input type='text' class='text' id='clientSelector' value='"+$customerLink.html()+"'/>");
					$customerLink.replaceWith($input);
					var $hiddenCustomerId = $("<input type=\"hidden\" name=\"person_id\" value=\""+ pattern.exec($customerLink.attr("href"))[1] +"\"/>");
					$hiddenCustomerId.insertAfter($input);
					
					
				$input.suggestionInput({
					callback : function (newId){
						if(newId > 0){
							$hiddenCustomerId.val(newId);
						}
						else if(newId == 0){
							$hiddenCustomerId.val(0);
						}
					},
					serviceUrl: "<?php $this->internalLink("search", "?format=json&r=people");?>"
				});
				</script>
				<div class="section">
					<h4 class="sectionHeader"><?php echo $this->translate("orderPayment/payments");?></h4>
					<?php $r=0;	
					$totalSum = 0;
					?>
					<table class="transactionList">
						<tr class="even">
							<th><?php echo $this->translate("transaction/transactionId");?></th>
							<th><?php echo $this->translate("transaction/time");?></th>
							<th><?php echo $this->translate("transaction/paymentMethod");?></th>
							<th><?php echo $this->translate("transaction/amount");?></th>
							<th></th>
						<?php  foreach($this->orderPayments as $orderPayment){ 
							$transaction = $orderPayment->transaction;
							$totalSum += $transaction->total;
							$link = $this->internalLink("order", $orderPayment->id."/".CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT,false);
							?>
							<tr class="<?php echo ($r++ % 2 ? "even" : "odd") . ($orderPayment->transaction->cancelled == 1 ? " cancelled" :""); ?>">
								<td><?php psafe($transaction->id)?></td>
								<td><?php psafe($transaction->created_at)?></td>
								<td><?php psafe($this->translate("transaction/paymentMethods/".$transaction->payment_method))?></td>
								<td><?php psafe($transaction->amount)?></td>
								<td><?php if($orderPayment->transaction->cancelled == 0){Html::LinkButton("Makulera",$link);}?></td>
							</tr>
						<?php } ?>
					</table>
				</div>
				<div class="section">
					<h4 class="sectionHeader"><?php echo $this->translate("order/comment")?></h4>
					<fieldset>
						<span class="formBox">
							<?php Html::Text("note",$this->order->note,true, array( "style"=> "width:700px"));?>
						</span>
					</fieldset>
					<?php Html::SubmitButton(strlen($this->order->comment) ? $this->translate("general/update") : $this->translate("general/save"));?>
				</div>
				<div class="section">
					<h4 class="sectionHeader"><?php echo $this->translate("order/orderRows");?></h4>
					<?php $r=0;	?>

					<table class="resourceList">
						<tr class="even">
							<th class="idCol"><?php echo $this->translate("order/orderRow/articleId");?></th>
							<th class="nameCol"><?php echo $this->translate("articles/articleName");?></th>
							<th class="typeCol"><?php echo $this->translate("articles/articleCount");?></th>
							<th><?php echo $this->translate("articles/pricePerEach");?></th>
							<th><?php echo $this->translate("articles/totalPrice");?></th>
							<th><?php echo $this->translate("order/orderRow/deliveryStatus");?></th>
							<th><?php echo $this->translate("order/orderRow/note");?></th>
						<?php  foreach($this->orderRows as $row){ 
							$class = $r++ % 2 ? "even" : "odd";
								if($row->status == "cancelled"){
									$class .=" cancelled";
								}
								?>
							<tr class="<?php echo $class?>">
								<td><?php psafe($row->article_id)?></td>
								<td><?php psafe($row->name)?></td>
								<td><?php Html::Text("row_count[{$row->article_id}]",$row->count);?></td>
								<td><?php Html::Text("row_pricePerEach[{$row->article_id}]",number_format($row->price_per_each,2));?></td>
								<td><?php psafe(number_format($row->total,2))?></td>
								<td><?php Html::Select("row_status[{$row->article_id}]",$this->orderRowStatusValues,$row->status);?></td>
								<td><?php Html::Text("row_note[{$row->article_id}]",$row->note,false,array("style" => "width:300px;"));?></td>
							</tr>
						<?php } ?>
					</table>
					<?php Html::SubmitButton($this->translate("general/save")); ?>
				</div>
			</form>
		<?php }
	}