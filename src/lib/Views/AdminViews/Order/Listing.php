<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->orders = $this->model->getOrders();
		}
		public function render(){?>
			<?php $r=0;	?>
			<table class="resourceList">
				<tr class="even">
					<th class="idCol"><?php echo $this->translate("order/id");?></th>
					<th><?php echo $this->translate("order/date");?></th>
					<th class="nameCol"><?php echo $this->translate("order/customer");?></th>
					<th class="typeCol"><?php echo $this->translate("order/orderTotal");?></th>
					<th><?php echo $this->translate("order/status");?></th>
					<th class="buttonsCol"></th>
				<?php  foreach($this->orders as $order){ ?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->printOrderLink($order->id) ?>"><?php psafe($order->id)?></a></td>
						<td><a href="<?php $this->printOrderLink($order->id) ?>"><?php psafe($order->submitted_at)?></a></td>
						<td><a href="<?php $this->printPersonLink($order->getPerson()->id)?>"><?php psafe($order->getPerson());?></a></td>
						<td><a href="<?php $this->printOrderLink($order->id) ?>"><?php psafe($order->total)?>:-</a></td>
						<td><a href="<?php $this->printOrderLink($order->id) ?>"><?php psafe($this->translate("order/orderStatus/".$order->status))?></a></td>
						<td>
							<?php Html::LinkButton($this->translate("general/edit"),$this->internalLink("order", num($order->id),false),array("class" => "editButton"))?>
						</td>
					</tr>
				<?php } ?>
			</table>
		<?php }
		
		public function printOrderLink($orderId){
			$this->internalLink("order", num($orderId));
		}
		public function printPersonLink($personId){
			$this->internalLink("person", num($personId));
		}
	}