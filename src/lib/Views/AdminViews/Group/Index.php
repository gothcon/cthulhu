<?php
	require_once("lib/Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		public function init(){

			// $this->groupdata = $this->getModel()->getGroup();
		}
		public function render(){?>
			<div>
				<div gc-loader></div>
				<div gc-tabs class="tabs-container"></div>
				<group class="tabbed-view"></group>
			</div>
		<?php }
	}