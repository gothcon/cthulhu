<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("lib/Views/HtmlView.php");
class ListingView extends HtmlView{
	var $resourceSchedules;
	/**
	 *
	 * @var SlotType[] 
	 */
	var $slotTypes;
	public function init() {
		$this->resourceSchedules = $this->getModel()->getResourceSchedules();
		
		$this->scheduleTimespans = Timespan::loadVisibleInSchedule();
		$this->slotTypes = SlotType::loadList();
		
	}
	public function render() {?>
	<div id="eventOccationOverview">
		<ul id="eventList">
		<?php
		$i = 0;
		foreach ($this->resourceSchedules as $evenType){
			foreach($evenType->events as $event){
				$cssClass = ($i++ % 2 ? "odd" : "even");
				?>
				<li<?php echo " class='{$cssClass}'";?>>
					<div class="eventName">
						<h2>
							<a name="<?php echo $event->id;?>" href="<?php $this->internalLink("event", "{$event->id}");?>">
							<?php echo "{$event->name}";?>
							</a>
						</h2>
					</div>
					<ul class="occationList">
						<?php foreach($event->event_occations as $occation){ 	
							$occationLink = $this->internalLink("eventOccation",$occation->id,false);
							$deleteLink = $this->internalLink("eventOccation",$occation->id."/".CONTROLLER_ACTION_DELETE,false);
							?>
						<li>
							<a  class="occationLink" href="<?php echo $occationLink; ?>"><?php echo $occation->starts_at ." - ".$occation->ends_at;?><?php echo strlen($occation->name) ? ", ".$occation->name : "";?></a>
							<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){ Html::DeleteButton($deleteLink); }?>
						</li>
						<?php } ?>
					</ul>
					<span class="clear"></span>
				</li>
			<?php }
		}
		?>
		</ul>
		<script>
			(function($){
			// set some calculation short-cuts 
			// Global private vari
			var msecPerSecond = 1000;
			var msecPerMinute = msecPerSecond * 60;
			var msecPerHour = msecPerMinute * 60;
			var msecPerDay = msecPerHour * 24;
			
			var masterInterval = getMasterInterval();
			var start = masterInterval[0];
			var end = masterInterval[1];
			var standardTimespans; //  = getScheduleTimespans();
			
			var options = {
				formCreateAction			: '<?php $this->internalLink("eventOccation",CONTROLLER_ACTION_NEW);?>',
				formSaveActionTemplate		: '<?php $this->internalLink("eventOccation","{:id}/".CONTROLLER_ACTION_SAVE);?>',
				labelWidth					: 220,
				loadOccationUrlTemplate		: '<?php $this->internalLink("eventOccation", "{:id}?format=json");?>',
				loadPublicTimespansUrl		: '<?php $this->internalLink("timespan", "?format=json");?>',
				deleteOccationUrlTemplate	: '<?php $this->internalLink("eventOccation", "{:id}/".CONTROLLER_ACTION_DELETE."?format=json");?>'
			}
			
			// get the number of hours in the schedule
			var hourCount = Math.ceil((end.getTime() - start.getTime()) / msecPerHour);

			var hourWidth = Math.floor((window.screen.availWidth - 20 - options.labelWidth) / hourCount);
			
			var $overviewContainer = $("#eventOccationOverview").css({"width" : ((hourWidth * hourCount) + options.labelWidth) });
			var $mainLightBoxContainer;
			var $loader;
			init();

			function init(){
				$("ul.occationList").hide();
				$mainLightBoxContainer = createLightBoxStructure();
				$loader = createLoaderStructure().appendTo($overviewContainer).css({ left:options.labelWidth+"px", right: "0px", "bottom" : "0px"})
				showLoader();
				loadTimespans(function(timespans){
					standardTimespans = timespans;
					createTimescale();
					// update the form
					var $timespanSelect = $mainLightBoxContainer.find("#timespanSelect");
					for(key in standardTimespans){
						var timespan = standardTimespans[key];
						$("<option value='"+timespan.id+"'>"+timespan.label+"</option>").appendTo($timespanSelect);
					}
					modifyEventList();
					$("ul.occationList").show();
					hideLoader();
				});
			}
		
			function getMasterInterval(){
				// (start rounded down to nearest hour, end rounded up to nearest hour)
				var start = new Date("2013-05-01 18:00:00");
				var end = new Date("2013-05-05 12:00:00");
				start.setTime( (Math.floor(start.getTime() / msecPerHour)-1) * msecPerHour);
				end.setTime( (Math.ceil(end.getTime() / msecPerHour)+1) * msecPerHour);
				return [start,end];
			}	
			
			function createTimescale(){
				// create the date and hour scale nodeds
				var $eventList = $("#eventList");
				
				var $hourList = $("<ul class='hourList'>").insertBefore($eventList).css({"width":(hourWidth * hourCount)-1, "left" : options.labelWidth});
				var $dateList = $("<ul class='dateList'>").insertBefore($eventList).css({"width":(hourWidth * hourCount)-1, "left" : options.labelWidth});
				var $timespanList = $("<ul class='mainTimespanList'>").insertBefore($eventList).css({"left" : options.labelWidth +1});
				// add the standard timespans 
				for(var key in standardTimespans){
					var timespan = standardTimespans[key];
					var offset = (timespan.offset * hourWidth);
					var width =  (timespan.width*hourWidth);
					$("<li>"+timespan.label+"</li>").css({left: offset , "width" : width}).appendTo($timespanList);
				}

				// populate the hour bar
				var leftOffset = 0;
				var cssClass;
				var startHour = start.getUTCHours();
				for(var i = 0; i < hourCount; i++){
					cssClass = (i % 2 ? "odd" : "even");
					$("<li>"+ (startHour % 24) +"</li>").css({"width": hourWidth, "left" : leftOffset}).addClass(cssClass).appendTo($hourList);
					leftOffset += hourWidth;
					startHour++;
				}
				
				// populate the date bar
				var firstMidnight = new Date((Math.ceil(start.getTime() / msecPerDay)) * msecPerDay);
				var currentTime = new Date(start.getTime());
				var leftOffset = 0;
				while(currentTime.getTime() < end.getTime()){
					// label = currentTime.getUTCDay() + "/" + currentTime.getUTCMonth();
					var label = DateToSwedish(currentTime.toDateString());
					if(currentTime < firstMidnight){
						dateLength = (firstMidnight.getTime() - currentTime.getTime()) / msecPerHour;
						currentTime.setTime(firstMidnight.getTime());
					}else if (currentTime.getTime() + msecPerDay > end.getTime()){
						var dateLength = (end.getTime() - currentTime.getTime()) / msecPerHour;
						currentTime.setTime(end.getTime());
					}else{
						dateLength = 24;
						currentTime.setTime(currentTime.getTime() + msecPerDay);
					}
					var width = hourWidth * dateLength;


					$("<li >"+ label +"</li>").css({"width": width, "left" : leftOffset }).appendTo($dateList);
					leftOffset += width;
				}
			}
			
			function DateToSwedish(date){
				var days = { "Mon" : "må","Tue" : "ti", "Wed" : "on", "Thu" : "to", "Fri" : "fr", "Sat" : "lö" , "Sun" : "sö" };
				var months = {"Jan": "01", "Feb":"02", "Mar":"03", "Apr":"04", "May":"05", "Jun":"06", "Jul":"07", "Aug":"08", "Sep":"09", "Oct":"10", "Nov":"11", "Dec":"12"};
				var regexp = /([a-zA-Z]+) ([a-zA-Z]+) (\d\d) (\d\d)/g;
				var res = regexp.exec(date);
				var day = res[1];
				// var month = res[2];
				var day = res[3];
				// var year = res[4];
				return days[res[1]] + " " + day +"/" + months[res[2]];
			}
 
			function addClickHandlerToTimespan($timespanNode,timespanId,eventId){
				var clickHandler = (function(){
					return function(event){
						event.stopPropagation();
						event.preventDefault();
						$("#mainFormContainer h2").html("Skapa nytt pass");
						$mainLightBoxContainer.find("form")[0].reset();
						$mainLightBoxContainer.find("input[name='event_occation_id']").val(0);
						$mainLightBoxContainer.find("#eventSelect").val(eventId);
						$mainLightBoxContainer.find("#deleteOccationButton").hide();
						$("#eventOccationForm").attr("action",options.formCreateAction).ajaxForm({saveCallback :onCreateCallback});
						var $timespanSelect = $mainLightBoxContainer.find("#timespanSelect");
						$timespanSelect.find("option.custom").remove();
						$timespanSelect.val(timespanId);
						$.openDOMWindow({modal : true, width:500, height: 350, windowSourceID : "#lbContainer" });
						console.log($timespanSelect.val());
					};
				}());
				$timespanNode.bind("click",clickHandler);
			}
			
			function loadTimespans(onLoadCallback){
				$.ajax({
					method: "get",
					url : options.loadPublicTimespansUrl,
					success : function(data){
						var standardTimespans = [];
						var noOfTimespans = data.data.length;
						for(var i = 0; i < noOfTimespans;i++){
							var span_start = new Date(data.data[i].startsAt);
							var span_end = new Date(data.data[i].endsAt);
							var span_label = data.data[i].name;
							standardTimespans[i] = {
								offset: (span_start.getTime() - start.getTime()) / msecPerHour , 
								width: (span_end.getTime() - span_start.getTime()) / msecPerHour,
								label: span_label,
								id : data.data[i].id
							};
						}
						
						onLoadCallback(standardTimespans);
					},
					dataType : "json"
				});
			}

			function modifyEventList(){
				$("#eventList > li").each(function(key,obj){
					var $li = $(obj);
					var $timespanList = $("<ul class='publicTimespans'>").appendTo($li).css({"left" : options.labelWidth+1});;
					var $link = $li.find(".eventName a");

					var pattern = /\/(\d+)/g;
					var res = pattern.exec($link.attr("href"));
					// add the standard timespans 
					for(key in standardTimespans){
						var timespan = standardTimespans[key];
						var offset = (timespan.offset * hourWidth);
						var width =  (timespan.width*hourWidth); // compensate for border and padding right
						var $timespan = $("<li>"+timespan.label+"</li>").css({left: offset , "width" : width}).appendTo($timespanList);
						// $timespan.bind("mouseover",function(event){$timespan.addClass("active")}).bind("mouseout",function(event){$timespan.removeClass("active")});
						addClickHandlerToTimespan($timespan, timespan.id, res[1]);
					}
					
				 $li.data("event_id",res[1]);

					// add the occations 
					var $occationList = $li.find(".occationList").css({"left" : options.labelWidth +1});
					$occationList.find("li").each(function(key, obj){
						var $obj = $(obj);
						var $link = $obj.find("a.occationLink");
						var occation_id = /\/(\d+)$/g.exec($link.attr("href"))[1];
						var pattern = /(\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d) - (\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d)(?:, (.*))?/g;
						var res = pattern.exec($link.html());
						var span_start = new Date(res[1]);
						var span_end = new Date(res[2]);
						var $eventOccation = createEventOccation(span_start,span_end,res[3])
						
						$obj.replaceWith($eventOccation);
						
						var callback = function(data){
							populateForm(data, $eventOccation);
							hideLoader();
						}
						
						$eventOccation.click(function(event){
							event.stopPropagation();
							event.preventDefault();
							showLoader();
							$.openDOMWindow({modal : true, width:500, height: 350, windowSourceID : "#lbContainer" });
							getEventOccationData(occation_id, callback);
						});
					});
				});
			}
			
			function populateForm(data,$clickedEventOccation){
				
				var occation = data.data;
				var $mainFormContainer = $("#mainFormContainer");
				$mainFormContainer.find("h2").html("Uppdatera pass");
				var $form = $("#eventOccationForm");
				$form[0].reset();
				var $timespanSelect = $form.find("select[name=event_occation_timespan_id]");
				// remove any old custom timespans
				$timespanSelect.find("option.custom").remove();

				// add the event occation timespan if it is not already in the select 
				if(($timespanSelect.find("option[value="+ occation.timespan_id +"]")).length == 0){
					$timespanSelect.append("<option class='custom' value='"+ occation.timespan_id +"'>"+ occation.timespan +"</option>");
				}
				
				$timespanSelect.val(occation.timespan_id);
				$form.find("select[name='event_occation_event_id']").val(occation.event_id);
				$form.find("input[name='event_occation_id']").val(occation.id);
				$form.find("input[name='event_occation_name']").val(occation.name);
				$form.find("input[name='event_occation_is_hidden']").each(function(key,obj){
					var $radio = $(obj);
					if($radio.val() == occation.is_hidden){
						$radio.trigger("click");
						return false;
					}
				});
				var $deleteButton = $mainFormContainer.find("#deleteOccationButton");
				
				for(key in occation.signup_slots){
					var slot = occation.signup_slots[key];
					var slot_type_id = slot.slot_type_id;
					var slot_type_identifier					= "input[name='slot_type["						+ slot_type_id + "]']";
					var maximum_signup_count_identifier			= "input[name='maximum_signup_count["			+ slot_type_id + "]']";
					var maximum_spare_signup_count_identifier	= "input[name='maximum_spare_signup_count["		+ slot_type_id + "]']";
					var requires_approval_identifier			= "input[name='requires_approval["				+ slot_type_id + "]']";
					var is_hidden_identifier					= "input[name='is_hidden["						+ slot_type_id + "]']";
					var price_identifier						= "input[name='price["							+ slot_type_id + "]']";
					
					$form.find(slot_type_identifier).trigger("click");
					$form.find(maximum_signup_count_identifier).val(slot.maximum_signup_count);
					$form.find(maximum_spare_signup_count_identifier).val(slot.maximum_spare_signup_count);
					if(Number(slot.requires_approval))
						$form.find(requires_approval_identifier).trigger("click");
					if(Number(slot.is_hidden))
						$form.find(is_hidden_identifier).trigger("click");
					$form.find(price_identifier).val(slot.price);
				}
				
				$deleteButton.unbind("click.deleteOccation").bind("click.deleteOccation",function(event){
					var url = options.deleteOccationUrlTemplate.replace("{:id}", occation.id);
					event.preventDefault();
					if(confirm("are U sure?")){
						$.ajax({
							  url: url,
							  dataType: 'json',
							  success: function(data){
								$clickedEventOccation.remove();
								$.closeDOMWindow({functionCallOnClose : true, functionCallAfterClose :  function(){
									$form[0].reset();
								}});
							  },
							  error:function(){
								$.logger.log("something went wrong!!!");
							  },
							  type: "get"
						});
					}
					$deleteButton.hide();
				}).show();
				
				var callback = function($form,data){
					$clickedEventOccation.remove();
					onUpdateCallback($form,data);
				}
				$form.attr("action",options.formSaveActionTemplate.replace("{:id}",occation.id)).ajaxForm({saveCallback :callback});
			}
			
			function getEventOccationData(eventOccationId,callback){
				var url = options.loadOccationUrlTemplate.replace("{:id}", eventOccationId);
				$.ajax({
					method: "post",
					url : url,
					success : callback,
					dataType : "json"
				});
			}
			
			function createEventOccation(span_start, span_end,label){
				var offset= (span_start.getTime() - start.getTime()) / msecPerHour;
				var width = (span_end.getTime() - span_start.getTime()) / msecPerHour;
				label =  label || "";
				offset *= hourWidth;
				width =  (width*hourWidth);
				return $("<li>"+label+"</li>").css({left: offset , "width" : width});
			}
			
			function getSlotTypes(){
				var slotTypes = [];
					<?php foreach ($this->slotTypes as $key => $slotType){?>
						slotTypes[<?php echo $key;?>] = { id :<?php echo $slotType->id;?> , name : '<?php echo $slotType->name;?>' }
				<?php } ?>
				return slotTypes;
			}
			
			function createFormStructure($overviewContainer){
				var $form = $("<form id='eventOccationForm' method='post' action='"+ options.eventOccationFormAction + "'>");
				$("<h3>Passdetaljer</h3>").appendTo($form);
				
				// timespan select
				$("<label for='event_occation_timespan_id'>Pass</label>").appendTo($form);
				var $timespanSelect = $("<select id='timespanSelect' name='event_occation_timespan_id'>").appendTo($form);
				$("<span class='clear'>").appendTo($form);
				
				// event select
				$("<label for='event_occation_event_id'>Arrangemang</label>").appendTo($form);
				var $eventSelect = $("<select id='eventSelect' name='event_occation_event_id'>").appendTo($form);
				$("#eventList > li").each(function(key,obj){
					var $link = $(obj).find(".eventName a");
					var pattern = /\/(\d+)/g;
					var res = pattern.exec($link.attr("href"));
					$("<option value='"+ res[1] +"'>"+$link.html()+"</option>").appendTo($eventSelect);
				});
				$("<span class='clear'>").appendTo($form);
				
				// name input
				$("<label for='event_occation_name'>Passnamn</label>").appendTo($form);
				$("<input type='text' name='event_occation_name' class='text'>").appendTo($form);
				$("<span class='clear'>").appendTo($form);
				
				// visibility...
				$("<label for='name'>Dolt?</label>").appendTo($form);
				var $visibilityButtonBox = $("<div class='buttonBox'>").appendTo($form);
				$("<span>Ja</span>").appendTo($visibilityButtonBox);
				$("<input type='radio' value='1' name='event_occation_is_hidden'/>").appendTo($visibilityButtonBox);
				$("<span>Nej</span>").appendTo($visibilityButtonBox);
				$("<input type='radio' value='0' name='event_occation_is_hidden'/>").attr("checked","checked").appendTo($visibilityButtonBox);
				$("<span class='clear'>").appendTo($form);

				$("<input id='occationId' type='hidden' value='0' name='event_occation_id'/>").appendTo($form);

				// signup slot header
				$("<h3>Lägg till anmälningsmöjligheter</h3>").appendTo($form);
				// signup slots
				var $signupSlotTableContainer = $("<div>").appendTo($form);
				var $signupSlotTable = $("<table><tr><th>Anmälningsmöjlighet</th><th>Platser</th><th>Reservplatser</th><th>Kräver godkännande</th><th>Dold</th><th>Kostnad</th></tr></table>").appendTo($signupSlotTableContainer);
				var slotTypes = getSlotTypes();
				for (key in slotTypes){
					var slotType = slotTypes[key];
					$("<tr>" +
						"<td><input class='checkbox' type='checkbox'		name='slot_type["+ slotType.id +"]' value='"+ slotType.id +"'/>"+ slotType.name +"</td>"+
						"<td><input class='text' type='text'				name='maximum_signup_count["+ slotType.id +"]'/></td>" + 
						"<td><input type='text' class='text'				name='maximum_spare_signup_count["+ slotType.id +"]'/></td>" +
						"<td><input type='checkbox' class='text' value='1'	name='requires_approval["+ slotType.id +"]'/></td>" +
						"<td><input type='checkbox' class='text' value='1'	name='is_hidden["+ slotType.id +"]'/></td>" +
						"<td><input type='text' class='text'				name='price["+ slotType.id +"]'/></td>" +
					"</tr>").appendTo($signupSlotTable);
				}
				// ok / cancel buttons
				$("<input class='okButton' type='submit' value='ok'>").appendTo($form);
				$("<input class='cancelButton' type='button' value='cancel'>").appendTo($form);
				
				return $form;
			}
			
			function showLoader(){
				$loader.show();
			}
			
			function hideLoader(){
				$loader.hide();
			}
			
			function onCreateCallback(form,data){
				var eventOccation = data.data;	
				$overviewContainer.find("#eventList > li").each(function(key,obj){
					var $obj = $(obj);
					if($obj.data("event_id") != eventOccation.event_id)
						return true;
					var starts_at = new Date(eventOccation.timespan.starts_at);
					var ends_at = new Date(eventOccation.timespan.ends_at);
					var $li = createEventOccation(starts_at, ends_at, eventOccation.name);
					
					var callback = function(data){
						populateForm(data, $li);
						hideLoader();
					}

					$li.click(function(event){
						showLoader();
						$.openDOMWindow({modal : true, width:500, height: 350, windowSourceID : "#lbContainer" });
						getEventOccationData(eventOccation.id, callback);
					});
					
					
					$obj.find(".occationList").append($li);
					$.closeDOMWindow();
				});
			} 
			
			function onUpdateCallback(form,data){
				var eventOccation = data.data;	

				$overviewContainer.find("#eventList > li").each(function(key,obj){
					var $obj = $(obj);
					if($obj.data("event_id") != eventOccation.event_id)
						return true;
					var starts_at = new Date(eventOccation.timespan.starts_at);
					var ends_at = new Date(eventOccation.timespan.ends_at);
					var $li = createEventOccation(starts_at, ends_at, eventOccation.name);
					var callback = function(data){
						populateForm(data, $li);
						hideLoader();
					}

					$li.click(function(event){
						showLoader();
						$.openDOMWindow({modal : true, width:500, height: 350, windowSourceID : "#lbContainer" });
						getEventOccationData(eventOccation.id, callback);
					});
					
					$obj.find(".occationList").append($li);
					$("#deleteOccationButton").hide();
					$.closeDOMWindow();
				});
			}
			
			function createLightBoxStructure(){
				$("<span class='clear'>").appendTo("body");
				var $lbContainer = $("<div id='lbContainer'>").appendTo("body").hide();

				var $mainContainer = $("<div id='mainFormContainer'><h2>Skapa nytt pass</h2></div>").appendTo($lbContainer);
				createLoaderStructure().appendTo($mainContainer).hide()
				var $form = createFormStructure().appendTo($mainContainer);
				$("<input type='button' id='deleteOccationButton' value='delete'>").appendTo($mainContainer).hide();
				$form.ajaxForm({saveCallback :onCreateCallback});
				$form.find(".cancelButton").click(function(){
					$.closeDOMWindow({functionCallOnClose : true, functionCallAfterClose :  function(){
				}});
				});
				return $lbContainer;
			}
			
			function createLoaderStructure(){
				$loader = $("<div class='loader'></div>");
				$("<div class='overlay'></div>").appendTo($loader);
				$("<div class='spinner'></div>").appendTo($loader);
				return $loader;
			}
		}(jQuery));

		</script>
	</div>
<?php
	}
}