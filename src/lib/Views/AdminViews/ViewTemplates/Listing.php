<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->list = $this->getModel()->getList();
		}
		public function render(){?>
			<table>
				<col class="idCol"/>
				<col class="nameCol"/>
				<col class="buttonCol"/>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th></th>
				</tr>
				<?php foreach($this->list as $item){
					$viewLink = "#";
					$editLink = "#";
					$deleteLink = "#";
					?>
				<tr>
					<td><a href="<?php echo $viewLink;?>"><?php echo $item->id;?><a></td>
					<td><a href="<?php echo $viewLink;?>"><?php echo $item->name;?><a></td>
					<td><?php Html::LinkButton($this->translate("general/edit"), $editLink);?><?php Html::LinkButton($this->translate("general/delete"), $deleteLink);?></td>
				</tr>
				<?php }?>
			</table>
		<?php }
	}