<?php 

	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../SessionAccessors/UserAccessor.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Controllers/AdminControllers/EventController.php");
	class AngularAdminMaster extends HtmlView{
		
		function getInnerView(){
            return $this->innerView;
		}
		
		function __construct($innerView){
			$this->innerView = $innerView;
			$this->model = $innerView->getModel();
			$this->setInternalLinkBuilder($this->innerView->getInternalLinkBuilder());
			$this->currentUsername = UserAccessor::getCurrentUser()->id > 0 ? (sprintf($this->translate("user/loggedInAs"),UserAccessor::getCurrentUser()->username)) : $this->translate("user/notLoggedIn");
			$this->errors = MessageAccessor::getErrorMessages();
			$this->warnings = MessageAccessor::getWarningMessages();
			$this->messages = MessageAccessor::getInformationMessages();
			MessageAccessor::clearAllMessages();
		}
	 
		function init(){
            $this->innerView->init();
            $this->model->init("startdate","2013-05-04");
		}
		
		function render(){
			$rootUrl =  Application::GetConfigurationValue("SiteRoot").Application::GetConfigurationValue("ApplicationPath");
                        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<title><?php echo strip_tags($this->model->getTitle());?></title>
					<meta http-equiv="content-type" content="text/html; charset=UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<base href="<?php echo Application::getApplicationUrl();?>/administrator/">
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/reset.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/angularDefault.css'/>
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/javascripts/tablesorter/style.css'/>
					<link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog.min.css'/>
					<link rel='stylesheet' type='text/css' href='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog-theme-default.min.css'/>
					<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
					<!-- Latest compiled and minified CSS -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
					<link rel='stylesheet' type='text/css' href='<?php echo $rootUrl;?>/resources/styles/datetimepicker.css'/>
					<!-- Optional theme -->
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
					<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
					<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>						
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/jQuery.js'></script>
					<!-- Latest compiled and minified JavaScript -->
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/lang/lang.php'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/gothcon_jq_plugins.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.0/moment-with-locales.js'></script>
					<script type='text/javascript' src='<?php echo $rootUrl;?>/resources/javascripts/gothcon.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/angular-moment/0.8.0/angular-moment.min.js'></script>
					<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/ng-dialog/0.1.6/ng-dialog.min.js'></script>
					<script type='text/javascript'>
						angular.module("GothCon").config(function(urlsProvider){
							urlsProvider.setApplicationUrl("/<?php echo Application::getApplicationPath();?>");
							// urlsProvider.setTemplatesUrl("resources/javascripts/angular/app/templates/");
							// urlsProvider.setDirectivesUrl("resources/javascripts/angular/app/directives/");
						});
					</script>
					<script type="text/javascript">
						<?php 
						$year = substr($this->model->get("startdate"),0,4);
						$month = substr($this->model->get("startdate"),5,2);
						$day = substr($this->model->get("startdate"),8,2);
						?>
						var defaultDate = new Date("<?php echo "$year,$month,$day";?>");
						$.datepicker.setDefaults({"defaultDate":defaultDate, "dateFormat":'yy-mm-dd', "gotoCurrent": true});
					</script>
				</head>
				<body ng-app="GothCon">
					<div id="wrapper" class="<?php echo get_called_class();?>">
						<div id="mainContent">
							<span class="clear"></span>
							<ul id="errorList"<?php echo count($this->errors) ? "":" style='display:none'";?>>
								<?php foreach($this->errors as $error){ ?>
								<li>
								<?php
									echo $error;
								?>
								</li>
								<?php } ?>
							</ul>
							<ul id="infoList"<?php echo count($this->messages) ? "":" style='display:none'";?>>
								<?php foreach($this->messages as $message){ ?>
								<li>
								<?php
									echo $message;
								?>
								</li>
								<?php } ?>
							</ul>
							<ul id="warningList"<?php echo count($this->warnings) ? "":" style='display:none'";?>>
								<?php foreach($this->warnings as $warning){ ?>
								<li>
								<?php
									echo $warning;
								?>
								</li>
								<?php } ?>
							</ul>							
							<div id="contentView">
                                <?php $this->innerView->render(); ?>
							</div>
						</div>
						<div id="mainMenuContainer" ng-app="menuApp">
						<ul id="mainMenu" gc-menu>
							<li class="searchContainer" >
								<form method="GET" action="<?php $this->internalLink("search");?>"> 
									<input name="q" type="text" /><?php Html::SubmitButton($this->translate("mainMenu/search"));?>
									<a target="_self" href="<?php $this->internalLink("search","?mode=advanced");?>"><?php echo $this->translate("mainMenu/advancedSearch")?></a>
                                </form>
							</li>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/personsAndGroups/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("person",CONTROLLER_ACTION_NEW);?>"><?php echo $this->translate("mainMenu/personsAndGroups/newPerson")?></a></li>
									<li><a href="<?php $this->InternalLink("group",CONTROLLER_ACTION_NEW);?>"><?php echo $this->translate("mainMenu/personsAndGroups/newGroup")?></a></li>
									<li><a href="<?php $this->InternalLink("person");?>"><?php echo $this->translate("mainMenu/personsAndGroups/personList")?></a></li>
									<li><a href="<?php $this->InternalLink("group");?>"><?php echo $this->translate("mainMenu/personsAndGroups/groupList")?></a></li>
									<li><a href="<?php $this->InternalLink("userstatistics");?>">Person och användarstatistik</a></li>								
								</ul>
							</li>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/event/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("event",CONTROLLER_ACTION_NEW);?>"><?php echo $this->translate("mainMenu/event/newEvent")?></a></li>
									<li><a href="<?php $this->InternalLink("event");?>"><?php echo $this->translate("mainMenu/event/eventList")?></a></li>
									<li><a href="<?php $this->InternalLink("timespan");?>"><?php echo $this->translate("mainMenu/event/timespanList")?></a></li>
									<li><a href="<?php $this->InternalLink("eventOccation");?>"><?php echo $this->translate("mainMenu/event/eventOccations")?></a></li>
									<li><a href="<?php $this->InternalLink("event","bookingSchedule");?>"><?php echo $this->translate("mainMenu/event/eventOverview")?></a></li>
									<li><a href="<?php $this->InternalLink("eventtype");?>"><?php echo $this->translate("mainMenu/event/eventTypes")?></a></li>
									<li><a href="<?php $this->InternalLink("slotType");?>"><?php echo $this->translate("mainMenu/event/slotTypes")?></a></li>
									<li><a href="<?php $this->InternalLink("event",CONTROLLER_ACTION_SHOW_SIGNUP_OVERVIEW);?>"><?php echo $this->translate("mainMenu/event/signupOverview")?></a></li>
								</ul>
							</li>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/resources/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("resource");?>"><?php echo $this->translate("mainMenu/resources/resources")?></a></li>
									<li><a href="<?php $this->InternalLink("resource",CONTROLLER_ACTION_NEW);?>"><?php echo $this->translate("mainMenu/resources/newResource")?></a></li>
									<li><a href="<?php $this->InternalLink("resource","schedule");?>">Bokningsöversikt</a></li>
									<li><a href="<?php $this->InternalLink("resourceType");?>"><?php echo $this->translate("mainMenu/resources/resourceTypes")?></a></li>
								</ul>
							</li>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/orders/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("order");?>"><?php echo $this->translate("mainMenu/orders/submittedOrders")?></a></li>
									<li><a href="<?php $this->InternalLink("article");?>"><?php echo $this->translate("mainMenu/orders/articles")?></a></li>
									<li><a href="<?php $this->InternalLink("transaction");?>"><?php echo $this->translate("mainMenu/orders/transactions")?></a></li>
									<li><a href="<?php $this->InternalLink("articlestatistics");?>"><?php echo $this->translate("mainMenu/orders/articlestatistics")?></a></li>
									<li><a href="<?php $this->InternalLink("orderPayment",CONTROLLER_ACTION_REGISTER);?>"><?php echo $this->translate("mainMenu/orders/registerPayment")?></a></li>
								</ul>
							</li>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/pdf/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("pdf","?file=schema");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/schedule")?></a></li>
									<li><a href="<?php $this->InternalLink("pdf","?file=folder");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/folder")?></a></li>
									<li><a href="<?php $this->InternalLink("pdf","?file=overview");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/overview")?></a></li>
									<li><a href="<?php $this->InternalLink("pdf","?file=resourceSchedules");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/resourceSchedules")?></a></li>
									<li><a href="<?php $this->InternalLink("pdf","?file=articleOrders");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/orderedArticles")?></a></li>
									<li><a href="<?php $this->InternalLink("pdf","?file=sleeperList");?>" target="_blank"><?php echo $this->translate("mainMenu/pdf/sleeperList")?></a></li>
								</ul>
							</li>
							<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){ ?>
							<li>
								<span class="header"><?php echo $this->translate("mainMenu/newsAndNewsletters/heading")?></span>
								<ul>
									<li><a href="<?php $this->InternalLink("news");?>"><?php echo $this->translate("mainMenu/newsAndNewsletters/newsList")?></a></li>
									<li><a href="<?php $this->InternalLink("news",CONTROLLER_ACTION_NEW);?>"><?php echo $this->translate("mainMenu/newsAndNewsletters/newNewsArticle")?></a></li>
									<li><a href="<?php $this->InternalLink("emailer");?>"><?php echo $this->translate("mainMenu/newsAndNewsletters/newsletter")?></a></li>
								</ul>
							</li>
							<?php } ?>
							<li>
								<a href="<?php $this->InternalLink("fileAdmin");?>"><?php echo $this->translate("mainMenu/filehandling/fileList")?></a>
							</li>
							<li class="receptionLink">
								<a href="<?php $this->InternalLink("reception");?>"><?php echo $this->translate("mainMenu/reception/register")?></a>
							</li>
							<li class="loginStatusContainer">
								<?php psafe($this->currentUsername) ?><br/>
								<a href="<?php $this->InternalLink("user",CONTROLLER_ACTION_LOGOUT);?>">
									<?php echo $this->translate("mainMenu/logout")?>
								</a>
							</li>
							
						</ul>
						</div>
						<script type="text/javascript">
							$("#mainMenuContainer").insertBefore("#mainContent");
							$("#mainMenuContainer").append("<span class='clear'></span>");
						</script>
						<div id="ajaxStatusBox"></div>
						<span class="clear"></span>
					</div>
				</body>
			</html>
		<?php }
	}
