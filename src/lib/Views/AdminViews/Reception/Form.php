<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class FormView extends HtmlView{
		public function init(){
			$this->person = $this->model->getPerson();
			$this->signupList = $this->model->getSignupList();
			$this->groupList = $this->model->getGroupMembershipList();
			$this->orderList = $this->model->getOrderList();
			$this->sleepingSlotBooking  = $this->model->getSleepingSlotBooking();
			$this->availableSleepingResources = $this->model->getAvailableSleepingResources();
			$this->entranceFeeArticles = $this->getModel()->getEntranceFeeArticles();
		}
		public function render(){
			?>
			<!-- identification data -->
			<div class="section">
				<div class="sectionContent">
				<h4 class="sectionHeader"><?php echo $this->translate("reception/person")?></h4>
				<dl>
					<dt><?php echo $this->translate("general/name");?></dt>
					<dd><?php echo $this->person->first_name." ".$this->person->last_name . "(".$this->person->identification.")" ;?></dd>
					<dt><?php echo $this->translate("people/otherInformation");?></dt>
					<dd><?php echo $this->person->note;?></dd>
				</dl>
				<?php Html::LinkButton($this->translate("general/details"), $this->internalLink("person", $this->person->id, false) );?>
				</div>
			</div>
			<!-- sleeping resource booking -->
			<div class="section">
				<h4 class="sectionHeader"><?php echo Translator::translate("sleepingResource/sleepingResource");?></h4>
				<p class="sleepingSlotBookingInfo">
				För närvarande är <?php echo $this->sleepingSlotBooking ? " <em>".$this->sleepingSlotBooking->getSleepingResource()->getResource()->name. "</em> bokad som sovplats" : "ingen sovplats bokad"; ?>
				</p>

				<div class="sleepingResourcesContainer">
					<h5>Välj ny sovplats</h5>
					<form method="post" action="<?php $this->internalLink("person", "{$this->person->id}/".CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING);?>" class="userForm">
						<ul class="sleepingResources">
							<li>
								<h6><?php Html::Radio("sleeping_resource_id", -1, !$this->sleepingSlotBooking );?> Sover inte på konventet</h6>
							</li>
							<?php  foreach($this->availableSleepingResources as $resource){ ?>	
							<li>
								<h6><?php Html::Radio("sleeping_resource_id", $resource->sleeping_resource_id,$resource->personal_booking_id  > 0);?> <?php psafe($resource->resource);?></h6>
								<ul>

									<?php foreach($resource->eventOccations as $occation){ ?>
									<li><?php psafe($occation->name);?></li>
									<?php }?>
								</ul>
							</li>
						<?php } ?>
						</ul>
						<span class="clear"></span>
						<?php Html::SubmitButton($this->translate("/general/update"));?>
					</form>	
				</div>
			</div>			
			<!-- orders -->
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("people/submittedOrders")?></h4>
				<?php $r=0;	?>
				<table class="resourceList">
					<tr class="even">
						<th class="idCol"><?php echo $this->translate("order/id");?></th>
						<th><?php echo $this->translate("order/date");?></th>
						<th class="typeCol"><?php echo $this->translate("order/orderTotal");?></th>
						<th><?php echo $this->translate("order/status");?></th>
						<th class="buttonsCol"></th>
						<?php  foreach($this->orderList as $order){ ?>
							<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
								<td><a href="<?php $this->internalLink("order", "{$order->id}") ?>"><?php psafe($order->id)?></a></td>
								<td><a href="<?php $this->internalLink("order", "{$order->id}") ?>"><?php psafe($order->submitted_at)?></a></td>
								<td><a href="<?php $this->internalLink("order", "{$order->id}") ?>"><?php psafe($order->total)?>:-</a></td>
								<td><a href="<?php $this->internalLink("order", "{$order->id}") ?>"><?php psafe($this->translate("order/orderStatus/".$order->status))?></a></td>
								<td>
									<?php Html::LinkButton($this->translate("general/edit"),$this->internalLink("order", "{$order->id}",false),array("class" => "editButton"))?>
								</td>
							</tr>
						<?php } ?>
				</table>
				<?php Html::LinkButton($this->translate("order/startNewOrder"),$this->internalLink("order", CONTROLLER_ACTION_NEW."/?p=".$this->person->id,false));?>
			</div>
		<?php 
		}
	}