<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	require_once("lib/ClassLib/Html.php");
	class IndexView extends HtmlView{
		
		
		protected $person;
		protected $sleepingSlotBooking;
		protected $hasSelectedEntranceFee = false;
		protected $amountToPay = 0;
		protected $isOk = false;
		protected $paymentMethods;
		protected $notes;
		/** @var OrderRow[] */
		protected $articlesToReceive = array();
		
		public function init(){
			$this->person = $this->model->getPerson();
			
			$this->sleepingSlotBooking  = $this->model->getSleepingSlotBooking();
			$this->hasSelectedEntranceFee = $this->model->entranceFeeSelected();
			$this->amountToPay = $this->model->getRemainingAmount();
			$this->articlesToReceive = $this->model->getArticlesToBeDelivered();
			
			$this->paymentMethods = array(
					array("value" => "cash" , "label" => $this->translate("transaction/paymentMethods/cash")),
					array("value" => "cc" , "label" => $this->translate("transaction/paymentMethods/cc"))					
			);
			
			$this->notes = $this->getModel()->getNotes();
			
			
			
		}
		public function render(){
			$r = 0;
			?>
			<!-- identification data -->
			<form method="post">
				<?php 
					if(count($this->articlesToReceive) == 0){ Html::Hidden("PreOrdersAreOk", 1); } 
					if($this->hasSelectedEntranceFee){ Html::Hidden("EntranceFeeIsOk", 1); }
					if($this->amountToPay == 0){ Html::Hidden("PaymentIsOk", 1); } 
				?>
				<div class="sectionContent reception">
					<div class="importantCheckoutInformation">
						<h3>Att kolla vid ankomstregistrering</h3>
						<table>
							<tr>
								<th class="descriptionCol">Vad?</th>
								<th class="statusCol">Status</th>
								<th class="checkedCol">Check!</th>
							</tr>
							<tr>
								<td class="descriptionCol">Identitet</td>
								<td class="statusCol"><a href="<?php $this->internalLink("person", $this->person->id);?>"><?php echo $this->person->first_name." ".$this->person->last_name . " (".$this->person->identification.")" ;?></a></td>
								<td class="checkedCol"><?php Html::Checkbox("IdentityIsOk", 1)?></td>
							</tr>
							
							<?php if(!$this->hasSelectedEntranceFee){?>
							<tr class="error">
								<td class="descriptionCol">Välj inträde</td>
								<td class="statusCol">
									<?php 
										Html::Select("entrance_fee_article_id",$this->model->getEntranceFeeArticles());
										Html::SubmitButton("uppdatera",array("name" => "update"));
									?>
								</td>
								<td class="checkedCol"><?php Html::Checkbox("EntranceIsOk", 1,false,array("disabled" => "disabled"))?></td>
							</tr>
							<?php }else if($this->amountToPay  > 0){ ?>
							<tr class="error">
								<td class="descriptionCol">Betalning</td>
								<td class="statusCol<?php if($this->amountToPay > 0){ echo " error";}?>">
									<?php echo "Att betala: ".number_format($this->amountToPay,2) ."kr";?>
									<?php if($this->amountToPay > 0){?>
										<div class="paymentMethodBox">Betalningsmetod: <?php Html::Select("payment_method",$this->paymentMethods) ?></div>
										<?php Html::Hidden("amount",$this->amountToPay);?>
									<?php } ?>
								</td>
								<td class="checkedCol"><?php Html::Checkbox("PaymentIsOk", 1,false)?></td>
							</tr>
							<?php } ?>
							
							<?php if(count($this->articlesToReceive)){ ?>
							<tr>
								<td class="descriptionCol">Förbeställning<br/><?php Html::LinkButton($this->translate("order/editCurrent"),$this->internalLink("order", CONTROLLER_ACTION_NEW."/?p=".$this->person->id,false));?></td>
								<td class="orderTableCell">
									<table class="orderRows">
										<tr>
											<th class="articleCol">Vad</th>
											<th class="noteCol">Notering</th>
											<th class="deliveredCol">Utlämnad</th>
										</tr>
									<?php foreach($this->articlesToReceive as $orderRow){
										$attributes = $orderRow->status == OrderRow::STATUS_DELIVERED ? array("disabled" => "disabled") : array();
										$remember = $orderRow->status == OrderRow::STATUS_DELIVERED ? false : true;
										$link = $this->internalLink("order",$orderRow->order_id,false)."#".Html::CreateHash($this->translate("order/orderRows"));
										?>
										<tr>
											<td class="articleCol"><a href='<?php echo $link;?>'><?php psafe($orderRow->count)?> x <?php phtml("{$orderRow->name} ({$orderRow->article_id})".($orderRow->status == OrderRow::STATUS_DELIVERED ? " <em>Redan uthämtad</em>" :""))?></a></td>
											<td class="noteCol"><?php psafe($orderRow->note)?></td>
											<td class="deliveredCol"><?php Html::Checkbox("orderRow[".$orderRow->id."]",$orderRow->id,true,$attributes,$remember)?></td>
										</tr>
									<?php }  ?>
									</table>
								</td>
								<td class="checkedCol"><?php Html::Checkbox("PreOrdersAreOk", 1)?></td>
							</tr>
							<?php } ?>
							<tr>
								<td class="descriptionCol">Sovplats</td>
								<td class="statusCol"><?php psafe( $this->sleepingSlotBooking ? $this->sleepingSlotBooking->getSleepingResource()->getResource()->name : "sover ej på konventet")?></td>
								<td class="checkedCol"><?php Html::Checkbox("SleepingIsOk", 1)?></td>
							</tr>
							<tr>
								<td class="descriptionCol">Övrigt</td>
								<td class="statusCol">
									<ul>
									<?php foreach ($this->notes as $note) { ?>
									<li><?php echo $note; ?></li>
									<?php } ?>
									</ul>
								</td>
								<td class="checkedCol"><?php Html::Checkbox("OtherIsOk", 1,count($this->notes) == 0 )?></td>
							</tr>
						</table>

						<?php
							$attributes = ($this->hasSelectedEntranceFee) ? array() : array("disabled" => "disabled");
							Html::SubmitButton("REGISTRERA ANKOMST",$attributes);
						?>
						<span class="clear"></span>
					</div>
				</div>
			</form>
		<?php 
		}
	}