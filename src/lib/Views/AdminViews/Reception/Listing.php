<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		public function init(){
			$this->personList = $this->model->getPersonList();
		}
		public function render(){
			
			?>
		<div class="receptionList">
			<form>
				<?php Html::Label($this->translate("people/identification"), "identification");?>
				<?php Html::Text("identification");?>
				<?php Html::Label($this->translate("people/firstName"), "first_name");?>
				<?php Html::Text("first_name");?>
				<?php Html::Label($this->translate("people/lastName"), "last_name");?>
				<?php Html::Text("last_name");?>
				<span class="clear"></span>
				<?php Html::SubmitButton($this->translate("general/search")); ?>
			</form>
			<table id="personList">
				<tr>
					<th class="idColumn"><?php echo $this->translate("people/id")?></th>
					<th><?php echo $this->translate("people/firstName")?></th>
					<th><?php echo $this->translate("people/lastName")?></th>
					<th><?php echo $this->translate("people/identification")?></th>
				</tr>
				<?php $r=0;
					foreach($this->personList as $person){?>
					<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
						<td><a href="<?php $this->internalLink("reception", $person->id);?>"><?php echo $person->id;?></a></td>
						<td><a href="<?php $this->internalLink("reception", $person->id);?>"><?php echo $person->first_name;?></a></td>
						<td><a href="<?php $this->internalLink("reception", $person->id);?>"><?php echo $person->last_name;?></a></td>
						<td><a href="<?php $this->internalLink("reception", $person->id);?>"><?php echo $person->identification;?></a></td>
					</tr>
				<?php } ?>
			</table>
		</div>
		<?php }
	}