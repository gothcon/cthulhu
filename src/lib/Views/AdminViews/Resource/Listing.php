 <?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlListView.php");;
	class ListingView extends HtmlListView{
		
		protected $sleepingStatistics;
		
		public function init(){
		}
		public function render(){?>

					<?php $r=0;	?>ddd
					<table class="resourceList">
						<tr class="even">
							<th class="idCol"><a href="?<?php $this->printHeaderLink("id");?>"><?php echo $this->translate("resources/id");?></a></th>
							<th class="nameCol"><a href="?<?php $this->printHeaderLink("name")?>"><?php echo $this->translate("resources/name");?></a></th>
							<th class="typeCol"><a href="?<?php $this->printHeaderLink("type")?>"><?php echo $this->translate("resources/type");?></a></th>
							<th class="typeCol"><a href="?<?php $this->printHeaderLink("available")?>"><?php echo $this->translate("resources/isAvailable");?></a></th>
							<th class="typeCol"><?php echo $this->translate("resources/availableForSleeping");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/noOfSleepingSlots");?></th>
							<th class="typeCol"><?php echo $this->translate("resources/noOfFreeSleepingSlots");?></th>
						<?php foreach($this->resourceList as $resource){
							$resourceType = $resource->resourceType;
							$resourceLink = $this->internalLink("resource",$resource->id,false);
							$resourceTypeLink = $this->internalLink("resourceType",$resourceType->id,false);
							$availableForSleeping = isset($this->sleepingStatistics[$resource->id]) && $this->sleepingStatistics[$resource->id]->is_available;
							$noOfSleepingSlots = isset($this->sleepingStatistics[$resource->id]) ? $this->sleepingStatistics[$resource->id]->no_of_slots : "-";
							$noOfFreeSleepingSlots = isset($this->sleepingStatistics[$resource->id]) ? $this->sleepingStatistics[$resource->id]->free_slots : "-";
							
						?>
							<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->id)?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php psafe($resource->name)?></a></td>
								<td><a href="<?php echo $resourceTypeLink; ?>"><?php psafe($resourceType->name);?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php echo $resource->available ? $this->translate("general/yes") : $this->translate("general/no");?></a></td>
								<td><a href="<?php echo $resourceLink; ?>"><?php echo $availableForSleeping  ? $this->translate("general/yes") : $this->translate("general/no");?></a></td>
								<td><?php psafe($noOfSleepingSlots)?></td>
								<td><?php psafe($noOfFreeSleepingSlots)?></td>
							</tr>
						<?php } ?>
					</table>
								<script type="text/javascript">
				(function($){
					var defaultOptions = {
						saveUrl : "<?php $this->internalLink("resource","{0}/save?format=json");?>",
						newUrl : "<?php $this->internalLink("resource","new?format=json");?>",
						deleteUrl : "<?php $this->internalLink("resource","{0}/delete?format=json");?>",
						typeListUrl : "<?php $this->internalLink("resource","?format=json");?>"
					}	
					
					$.fn.resourceList = function(optionsParam){
						var $this = $(this);
						if($this.length < 1){
							return $this;
						}
						
						var options = $.extend({},defaultOptions,optionsParam);
						var resourceTypes = [];
						var typeIdToNameMapping = [];
						
						function doAjaxSave(requestUrl,data,callback){
							$.ajax({
								  url: requestUrl,
								  dataType: 'json',
								  type : 'POST',
								  data: data,
								  success: callback
							});
						}
						
						function doAjaxDelete(requestUrl,resourceId,callback){
							$.ajax({
								  url: requestUrl,
								  dataType: 'json',
								  type : 'POST',
								  data: { "id" : resourceId},
								  success: callback
							});
						}
						
						function transformRows($table){
							$table.find("tr").each(function(key,row){
								if(key == 0){
									return true;
								}
								$tr = $(row);
								$idCell = $($tr.children()[0]);
								$nameCell = $($tr.children()[1]);
								$typeCell = $($tr.children()[2]);
								$buttonCell = $($tr.children()[3]);
								
								var $idLink = $($idCell.find("a")[0]);
								var id = $idLink.text();
								var $idSpan = $("<span class='idSpan'>"+id+"</span>");
								$idLink.replaceWith($idSpan);
								
								var $nameLink = $($nameCell.find("a")[0]);
								var name = $nameLink.text();
								var $nameSpan = $("<span class='nameSpan'>"+name+"</span>");
								$nameLink.replaceWith($nameSpan);
								
								var $typeLink = $($typeCell.find("a")[0]);
								var type = $typeLink.text();
								var $typeSpan = $("<span class='typeSpan'>"+type+"</span>");
								$typeLink.replaceWith($typeSpan);
								
								
								
								var $editButton = $($buttonCell.find(".editButton")[0]).removeProp("click");
								var $deleteButton = $($buttonCell.find(".deleteButton")[0]).hide().removeProp("click");
								var $saveButton = createSaveButton().hide().insertAfter($editButton);
								var $abortButton = createAbortButton().insertAfter($saveButton).hide();
								var $nameInput = createNameInput(name).appendTo($nameCell).hide();
								var $typeSelect = createTypeSelect(resourceTypes,type).appendTo($typeCell).hide();
								var leaveEditState = function(){
									
									$deleteButton.hide();
									$abortButton.hide();
									$saveButton.hide();
									$editButton.show();
									
									$nameInput.hide();
									$nameSpan.show();
									
									$typeSelect.hide();
									$typeSpan.show();
									
								}

								var enterEditState = function(){
									$editButton.hide();
									$abortButton.show();
									$saveButton.show();
									$deleteButton.show();

									$nameSpan.hide();
									$nameInput.show();

									$typeSelect.show();
									$typeSpan.hide();

								}
								
								$abortButton.click(function(event){
									event.stopPropagation();
									event.preventDefault();
									leaveEditState();
									return false;
								});
								
								$editButton.click(function(event){
									event.stopPropagation();
									event.preventDefault();
									enterEditState();
									return false;
								});
								
								$deleteButton.click(createDeleteButtonClick($tr,id));
								
								$saveButton.click(createSaveButtonClick($tr,$.translate("general/save"),function(data){
									$nameSpan.text(data.data.name);
									$idSpan.text(data.data.id);
									$typeSpan.text(getTypeFromId(data.data.resource_type_id).name);
									leaveEditState();
									log(data.message);
								}));
								
								
								var onRowMouseOver = function(){
									$nameSpan.hide();
								}
							
							});
						}
						
						function createSaveButton(label){
							label = label || $.translate("general/save"); 
							var $saveButton = $("<a href='#spara' class='submitButtonLink saveButton'>" + label + "<span class='end'></span></a>");
							return $saveButton;
						}						
						
						function createAbortButton(){
							label = $.translate("general/abort"); 
							var $button = $("<a href='#avbryt' class='submitButtonLink abortButton'>" + label + "<span class='end'></span></a>");
							return $button;
						}
						
						function createDeleteButton(){
							var $deleteButton = $("<a href='#' class='submitButtonLink deleteButton'>"+ $.translate("general/delete")+"<span class='end'></span></a>");
							return $deleteButton;
						}
						
						function createNameInput(name){
							name = name || "";
							return $("<input type='text' name='name' class='nameInput' value='"+name+"'/>")
						}
						
						function createTypeSelect(typeList,selectedValue){
							var $select = $("<select class='typeSelect'></select>");
							for(key in typeList){
								$option = $("<option value='"+ typeList[key].id +"'>"+ typeList[key].name+"</option>");
								if(selectedValue == typeList[key].name){
									$option.attr("selected","selected");
								}
								$select.append($option);
							}
							return $select;
						}
						
						function getData($tr){
							var id = $($tr.find(".idSpan")[0]).text();
							var name = $($tr.find(".nameInput")[0]).val();
							var type = $($tr.find(".typeSelect")[0]).val();
							var data =  {"id" : id, "name" : name, "resource_type_id" : type};
							return data;
						}
						
						function createSaveButtonClick($tr,operation,ajaxCallback){
							var id = $($tr.find(".idSpan")).text();
							operation = operation || "save";
							return function(event){
								if(operation == "new"){
									saveUrl = options.newUrl;
								}else{
									saveUrl = options.saveUrl.replace("{0}",id);
								}
								doAjaxSave(saveUrl,getData($tr),ajaxCallback);
								event.stopPropagation();
								event.preventDefault();
								return false;
							};
						}
						
						function createNewButtonClick($table){
							return function(event){
								var $ajaxCallback = function(data){
									// create row
									$tr = $("<tr>");
									$td1 = $("<td>");
									$td2 = $("<td>");
									$td3 = $("<td>");
									$table.append($tr.append($td1).append($td2).append($td3));
									
									// create the input field and name textspan
									$nameInput = $("<input type='text' value='"+ data.data.name +"'>");
									$nameSpan = $("<span>"+data.data.name+"</span>");
									// create the save button
									$saveButton = createSaveButton($.translate("general/save"));
									$saveButton.click(createSaveButtonClick($nameInput));
									// create the delete button
									$deleteButton = createDeleteButton()
									// create the click callback
									$deleteButtonClick = createDeleteButtonClick($tr,id);
									$table.append($tr.append($td1).append($td2.append($td2input)).append($td3.append($td2submit)));
									$table.append("<tr></tr>");
								}
							};
						}
						
						function createDeleteButtonClick($tr,typeId){
							var ajaxCallback = function(data){
								$tr.remove();
								log(data.data.message);
							}
							var deleteUrl = options.deleteUrl.replace("{0}",typeId);
							
							return function(event){
								doAjaxDelete(deleteUrl,typeId,ajaxCallback);
								event.stopPropagation();
								event.preventDefault();
								return false;
							};
						}
						
						function log(string){
							console.log(string);
						}

						function getTypeFromId(id){
							return resourceTypes[typeIdToNameMapping[id]];
						}
						
						function init(data){
							resourceTypes = data.data;
							
							for(key in resourceTypes){
								var id = resourceTypes[key].id;
								typeIdToNameMapping[id] = key;
							}
							$this.each(function(key,obj){
								var $table = $(obj);
								transformRows($table);
							});
						}

						$.ajax({
							  url: options.typeListUrl,
							  dataType: 'json',
							  type : 'POST',
							  success: init
						});
					};
				})(jQuery);
				// $(".resourceList").resourceList();
			</script>

		<?php }
	}