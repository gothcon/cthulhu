<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		
		/**
		 *
		 * @var Resource
		 */
		protected $resource;
		/**
		 *
		 * @var Entities\SleepingResource;
		 */
		protected $sleepingResource;
		
		/**
		 *
		 * @var array
		 */
		protected $bedBookings;
		
		protected $sleepingOccations;
		
		protected $resourceSchedules;
		protected $starts_at;
		protected $ends_at;
		protected $offset;
		protected $master_start_hour;
		protected $hour_width;
		protected $total_width;

		private function initResourceSchedule(){
			$this->resourceSchedules = $this->getModel()->getResourceSchedules();
			$this->starts_at = strtotime($this->getModel()->getMasterTimespan()->starts_at);
			$this->ends_at = strtotime($this->getModel()->getMasterTimespan()->ends_at);
			$this->offset = 0;
			$this->master_start_hour = $this->getModel()->getMasterTimespan()->starts_at;
			$this->hour_width = 18;
			$this->total_width = (($this->ends_at - $this->starts_at) / 3600) * $this->hour_width ; 

		}
		public function init(){
			$this->resource = $this->model->getResource();
			$this->resourceTypes = $this->model->getTypes();
			$this->sleepingResource = $this->resource->getSleepingResource();
			
			if($this->sleepingResource != null){
				$this->bedBookings = $this->sleepingResource->getSleepingSlotBookings();
			}
			
			$this->resourceBookings = $this->model->getBookings();
			
			$this->formAction = $this->internalLink("resource",num($this->resource->getId()) ? num($this->resource->getId())."/".CONTROLLER_ACTION_SAVE : CONTROLLER_ACTION_NEW,false);
			$this->newSignupLink = $this->internalLink("sleepingSlotBookings",CONTROLLER_ACTION_NEW ."?format=json",false);
			$this->peopleSearchUrl = $this->internalLink("sleepingSlotBookings",CONTROLLER_ACTION_LISTING."?format=json",false);
			$this->sleepingOccations = $this->getModel()->getSleepingOccations();
                        
            $this->initResourceSchedule();
		}
		
		public function getDeleteSignupLink($signupId){
			return $this->internalLink("sleepingSlotBookings",num($signupId)."/".CONTROLLER_ACTION_DELETE,false);
		}
		
		public function renderDeleteResourceButton(){
			$link = $this->internalLink("resource",num($this->resource->getId())."/".CONTROLLER_ACTION_DELETE,false);
			Html::DeleteButton($link,$this->translate("general/delete"),$this->translate("resources/deleteWarning"),array("class" => "deleteButton"));
		}
		
		public function printPersonLink($personId){
			$this->internalLink("person",num($personId));
		}
		
		
		public function render(){?>
			
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("general/details");?></h4>
					<form method="post" action="<?php echo $this->formAction  ?>" id="resourceForm">
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("resources/name"),"name");?>
							<?php Html::Text("name",$this->resource->getName()); ?>
							<?php Html::Label($this->translate("resources/type"),"type");?>
							<?php Html::Select("resource_type_id",$this->resourceTypes, $this->resource->getResourceType()->getId()); ?>

							<?php Html::Label($this->translate("resources/useableForSleeping"),"sleeping_is_possible");?>
							<span class="buttonBox">
								<?php Html::Radio("sleeping_is_possible",1, $this->resource->getSleepingResource() != null );?> <?php echo $this->translate("general/yes");?>
								<?php Html::Radio("sleeping_is_possible",0, $this->resource->getSleepingResource() == null);?> <?php echo $this->translate("general/no");?>
							</span>	
							<?php Html::Label($this->translate("resources/isAvailable"),"available");?>
							<span class="buttonBox">
								<?php Html::Radio("available",1, $this->resource->getAvailable());?> <?php echo $this->translate("general/yes");?>
								<?php Html::Radio("available",0, !$this->resource->getAvailable());?> <?php echo $this->translate("general/no");?>
							</span>	
						</span>
					</fieldset>
					<?php Html::SubmitButton($this->translate("general/save"));?>
					</form>
			</div>
			<script>
				//$("#resourceForm").ajaxForm({
				//	"saveCallback" : function($form,data){
				//		var resource = data.data;
				//		$("#mainContentTop h1").html( resource.id + ". " + resource.name );
				//	}
				//});
			</script>
			<?php if($this->resource->getId() > 0){ ?>
			<?php if($this->sleepingResource != null){ ?>
			<div class="section">
				<h3 class="sectionHeader"><?php echo $this->translate("resources/sleeping");?></h3>
				<div style="float:left">
					<h4><?php echo $this->translate("sleepingResource/details");?></h4>
					<form method="post" action="<?php echo $this->formAction  ?>" id="sleepingForm">
					<?php Html::Hidden("sleeping_resource_id",$this->sleepingResource->getId());?>
					<fieldset>
						<span class="formBox">
							<?php Html::Label($this->translate("resources/noOfSleepingSlots"),"maximum_signup_count");?>
							<?php Html::Text("maximum_signup_count",$this->sleepingResource->getNoOfSlots()); ?>
							<?php Html::Label($this->translate("resources/isAvailable"),"is_hidden");?>
							<span class="buttonBox">
								<?php Html::Radio("is_available",1, $this->sleepingResource->getIsAvailable());?><?php echo $this->translate("general/yes");?>
								<?php Html::Radio("is_available",0, !$this->sleepingResource->getIsAvailable());?><?php echo $this->translate("general/no");?>
							</span>	
							<span class="clear"></span>
						</span>
					</fieldset>
					<h4><?php echo $this->translate("resources/bookedSleepingOccations");?></h4>
					<table>
						<thead>
							<tr>
								<th><?php echo $this->translate("resources/isBooked");?></th>
								<th><?php echo $this->translate("resources/when");?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($this->sleepingOccations as $sleepingOccation){
								$timespan = new Timespan();
								$timespan->starts_at = $sleepingOccation->starts_at;
								$timespan->ends_at = $sleepingOccation->ends_at;
								$timespan->name = $sleepingOccation->timespan;
								$label = $timespan->getSmartIntervalString();
							?>
							<tr>
								<td><?php Html::Checkbox("sleeping_occations[$sleepingOccation->event_occation_id]", $sleepingOccation->event_occation_id,$sleepingOccation->booking_id > 0)?></td>
								<td><?php psafe($label) ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<?php Html::SubmitButton($this->translate("general/save"));?>
					</form>
				</div>
				<?php $r=0;	?>
				<div  class="sleeperListContainer" style="width:400px; float:left; margin-left:10px;">
					<h4><?php echo $this->translate("sleepingResource/sleepingBookings");?></h4>
					<table>
						<colgroup>
							<col class="idCol">
							<col class="firstNameCol"/>
							<col class="lastNameCol"/>
							<col class="buttonsCol"/>
						</colgroup>
						<tr class="even">
							<th><?php echo $this->translate("general/id");?></th>
							<th><?php echo $this->translate("people/firstName");?></th>
							<th><?php echo $this->translate("people/lastName");?></th>
							<th></th>
						</tr>
						<?php foreach($this->bedBookings as $bedBooking){
							$person = $bedBooking->getPerson();
						?>
							<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
								<td><a href="<?php $this->printPersonLink($person->getId()) ?>"><?php psafe($person->getId())?></a></td>
								<td><a href="<?php $this->printPersonLink($person->getId()) ?>"><?php psafe($person->getFirstName())?></a></td>
								<td><a href="<?php $this->printPersonLink($person->getId()) ?>"><?php psafe($person->getLastName())?></a></td>
								<td><?php Html::DeleteButton( $this->getDeleteSignupLink($bedBooking->getId()),$this->translate("general/delete"),$this->translate("general/areYouSure"));?></td>
							</tr>
						<?php } ?>
					</table>
					<?php Html::LinkButton($this->translate("resources/addSleepers"),"#",array("class"=>"addSignupButton"));?>
				</div>
				<?php Html::Hidden("sleeping_resource_id",$this->sleepingResource->getId());?>
				<?php Html::Hidden("searchRepository","people");?>
				
				<script type="text/javascript">
					$(".sleeperListContainer").each(function(index,container){
						var personLinkTemplate = "<?php $this->internalLink("person","id={:id}");?>";
						var $container = $(container);
						var $trigger = $container.find(".addSignupButton");
						var $signupTable = $container.find("table");
						var sleeping_resource_id = <?php echo $this->sleepingResource->getId();?>;
						
						var dataUrl = "<?php echo $this->peopleSearchUrl?>";
						
						function onSaveSignup(response){
							for(var key in response.data){
								createRow(response.data[key]).appendTo($signupTable);
							}
						}	
						
						function createRow(slotBooking){
							var personLink = personLinkTemplate.replace("{:id}",slotBooking.person.id);
							var $tableRow = $("<tr></tr>");
							$("<td>").append($("<a href='" + personLink +"'>"+slotBooking.person.id +"</a>")).appendTo($tableRow);
							$("<td>").append($("<a href='" + personLink +"'>"+slotBooking.person.first_name +"</a>")).appendTo($tableRow);
							$("<td>").append($("<a href='" + personLink +"'>"+slotBooking.person.last_name +"</a>")).appendTo($tableRow);
							var $deleteButton = $("<a href='<?php $this->internalLink("sleepingSlotBookings	","%id/".CONTROLLER_ACTION_DELETE)?>' class='buttonLink'>Radera<span class='end'></span></a>".replace("%id",slotBooking.id))
							$deleteButton.bind("click",function(event){
								if(!confirm("<?php echo $this->translate("general/areYouSure")?>")){
									event.preventDefault();
									event.stopPropagation();
									return false;
								}
							})
							$("<td>").append($deleteButton).appendTo($tableRow);
							return $tableRow;
						}
						
						function onSelect(data){
							var entityIDs = [];
							var i =0;
							for(var key in data){
								entityIDs[i++] = data[key].id;
							}

							$.ajax({
								url: "<?php echo $this->newSignupLink;?>",
								  dataType: 'json',
								  type : 'POST',
								  data: {"sleeping_resource_id" : sleeping_resource_id , "entityIDs" : entityIDs},
								  success: onSaveSignup
							});
						}
						$trigger.ajaxSelector({"serviceUrl" : dataUrl, "callback" : onSelect, "multiSelect" : false, populateOnStart:true, hideSearchField: true});
					});
				</script>
			</div>
			<?php } ?>
						<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("resources/bookings");?></h4>
					<?php $r=0;	?>
				<table>
					<thead>
					<tr class="even">
						<th class="idCol"><?php echo $this->translate("resources/bookingId");?></th>
						<th class="timeCol"><?php echo $this->translate("resources/when");?></th>
						<th class="nameCol"><?php echo $this->translate("resources/name");?></th>
						<th class="typeCol"><?php echo $this->translate("resources/bookingType");?></th>
						<th class="buttonsCol"></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($this->resourceBookings as $booking){
						$bookingLink = $this->internalLink("booking", $booking->id, false);
						$timespan = new Timespan();
						$timespan->starts_at = $booking->getStart();
						$timespan->ends_at = $booking->getEnd();
						$timespanLabel = $this->getTimespanLabel($timespan);
						?>
						<tr class="<?php echo $r++ % 2 ? "even" : "odd";?>">
							<td><a href="<?php echo $bookingLink;?>"><?php psafe($booking->getBooking()->id)?></a></td>
							<td><a href="<?php echo $bookingLink;?>"><?php psafe($timespanLabel)?></a></td>
							<td><a href="<?php echo $bookingLink;?>"><?php psafe($booking->getDescription())?></a></td>
							<td><a href="<?php echo $bookingLink;?>"><?php psafe($this->translate("booking/types/{$booking->getType()}"))?></a></td>
							<td><?php Html::LinkButton($this->translate("general/delete"),$this->getDeleteBookingLink($booking->getBooking()->id),array("class" => "editButton"))?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>	
                		<?php Html::LinkButton($this->translate("resources/addBooking") ,$this->internalLink("booking",CONTROLLER_ACTION_NEW."?resource_id={$this->resource->getId()}", false),array("class" => "editButton", "id" => "addBookingBtn"))?>				
                                <script type="text/javascript">
                                    (function(){
                                        
                                        var $occationList;
                                        var $form;
                                        var $dialogue;
                                        var $okButton;
                                        var $cancelButton;
                                        var resourceId = "<?php psafe($this->resource->getId())?>";
                                        
                                        function createStructure(){
                                                $dialogue = $("<div id='lbDialogue'></div>").appendTo($("body"));
                                                $form = $("<form method='post' action='<?php $this->internalLink("resourceBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>'></form>").appendTo($dialogue);
                                                $occationList = $("<ul class='occationList'></ul>").appendTo($form);
                                                $okButton = $("<input class='ajaxSelector_okButton submitButton' type='submit' value='ok'>").appendTo($form);
                                                $cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>").appendTo($form);
                                                $("<span class='clear'></span>").appendTo($dialogue);

                                                $okButton.click(function(event){
                                                        $dialogue.closeDOMWindow();
                                                });
                                                $cancelButton.click(function(event){
                                                        $dialogue.closeDOMWindow();
                                                });
                                        }

                                        function init(){
                                            createStructure();
                                            $("#addBookingBtn").each(function(key,obj){
                                                $addBookingBtn = $(obj);
                                                $addBookingBtn.click(createResourceClickHandler());
                                            });
                                        }

                                        function createResourceClickHandler(){
                                            var onSaveCallback = createOnSaveCallback();
                                            return function(event){
                                                $form.ajaxForm({"preventBuiltInCallback" : true, "saveCallback" : onSaveCallback });
                                                event.preventDefault();
                                                event.stopPropagation();
                                                $form.attr("action","<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>".replace("{:resource_id}",resourceId));
                                                fillDialogue(resourceId);
                                                $.openDOMWindow({modal : true, width:400, height:450, windowSourceID : "#lbDialogue" });
                                            };
                                        }
				
                                        function createOnSaveCallback(){
                                            return function(form,data){
                                                console.log(data);
                                                for(key in data.data.resourceSchedule.schedule[0].events){
							var event = schedule.events[key];
							var bookings = event.bookings;
							for(bookingKey in bookings){
								var booking = bookings[bookingKey];
								var bookingStartsAt = new Date(booking.starts_at);
								var bookingEndsAt = new Date(booking.ends_at);
								var width = ((((bookingEndsAt / 1000)- (bookingStartsAt/1000)) / 3600) * 18) - 1;
								var offset = (((bookingStartsAt / 1000)- (startsAt/1000)) / 3600) * 18;
								$link = $("<a href='#'>"+booking.id+"</a>");
								console.log(booking);
								
								$listItem = $("<li style='width:"+width+"px; left:"+offset+"px'></li>").append($link).appendTo($bookingList);
								$link.click(createDeleteClickHandler($listItem,booking.booking_id));
							}
						}
                                            }
                                        }
				
                                        function createEventListItem(event){
                                            var $listItem = $("<li class='event'> <input type='checkbox'/> "+event.name+"</li>"); 
                                            return $listItem;
                                        }

                                        function createEventOccationListItem(occation){
                                                var cssClass = occation.ok_to_book === 0 ? " colliding" : ""; 
                                                return $("<li class='eventOccation"+cssClass+"'> <input "+ ( occation.already_booked==="1" ? "disabled='disabled' checked='checked'" : "") +" type='checkbox' name='eventOccation[]' value='"+ occation.id +"'/> "+occation.timespan+ (occation.name ? ", " +occation.name : "") +"</li>");
                                        }

                                        function fillDialogue(resourceId){

                                                $.ajax({
                                                          url: "<?php $this->internalLink("eventOccation","?format=json&resourceId=")?>"+ resourceId,
                                                          dataType: 'json',
                                                          success: function(data){

                                                                $occationList.html("");
                                                                var tree = data.data;
                                                                for(eventKey in tree){
                                                                    var event = tree[eventKey];
                                                                    var eventListItem = createEventListItem(event).appendTo($occationList);
                                                                    var occationListItems = [];
                                                                    for(occationKey in event.eventOccations){
                                                                            var occation = event.eventOccations[occationKey];
                                                                            var eventOccationListItem = createEventOccationListItem(occation,event).appendTo($occationList);
                                                                            occationListItems[occationListItems.length] = eventOccationListItem.find("input")[0];
                                                                    }
                                                                    var onClick = (function(occationListItems){
                                                                            return function(event){
                                                                                    if($(event.target).attr("checked")=== "checked")
                                                                                            $(occationListItems).attr("checked","checked");
                                                                                    else
                                                                                            $(occationListItems).removeAttr("checked");
                                                                            };
                                                                    })(occationListItems);

                                                                    var $checkBox = eventListItem.find("input");
                                                                    $checkBox.click(onClick);
                                                                }
                                                          }
                                                });	
                                        }

                                        init();        
                                    }());
                                </script>
				
			</div>
			<div class="section">
				<h4 class="sectionHeader"><?php echo $this->translate("resources/bookings");?></h4>
                                <div id="scheduleListContainer" style="width:<?php echo $this->total_width+200;?>px;">
                                        <ul id="dateList">
                                        <?php
                                                $j= 0;
                                                $dateBoxes = array();
                                                $current_time = $this->starts_at;
                                                $dateBox = new stdClass();
                                                $dateBox->offset = 0;
                                                $dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
                                                $dateBox->length_in_hours = 0;
                                                $currentDayOfMonth = strftime("%d",$current_time);
                                                while($current_time < $this->ends_at){
                                                        if(strftime("%d",$current_time) != $currentDayOfMonth){
                                                                $currentDayOfMonth = strftime("%d",$current_time);
                                                                $dateBoxes[] = $dateBox;
                                                                $new_offset = $dateBox->offset + $dateBox->length_in_hours;
                                                                $dateBox = new stdClass();
                                                                $dateBox->offset = $new_offset;
                                                                $dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
                                                                $dateBox->length_in_hours = 0;
                                                        }
                                                        $dateBox->length_in_hours++;
                                                        $current_time += 3600;
                                                }
                                                $dateBoxes[] = $dateBox;

                                                foreach($dateBoxes as $dateBox){
                                                        $width = $dateBox->length_in_hours*$this->hour_width;
                                                        $label = $dateBox->label;
                                                        $offset = $dateBox->offset * $this->hour_width;
                                                        ?>
                                                        <li class="<?php echo $j++ %2 ?"even" :"odd";?>" style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
                                                                <span class="dateLabel"><?php echo $label;?></span>
                                                        </li>
                                                        <?php
                                                }
                                                ?>
                                        </ul>
                                        <ul id="hourList">
                                                <?php 
                                                $i = 0;
                                                for($currentTime = $this->starts_at; $currentTime < $this->ends_at; $currentTime += 3600){ 
                                                        $width = $this->hour_width;
                                                        $left = $this->hour_width*(($currentTime - $this->starts_at) / 3600);
                                                        ?>
                                                <li class="<?php echo $i++ %2 ?"even" :"odd";?>" style="width:<?php echo $width;?>px; left:<?php echo $left  ;?>px">
                                                <?php psafe(strftime("%H",$currentTime))?>
                                                </li>
                                                <?php } ?>
                                        </ul>
                                        <ul id="scheduleList">
                                        <?php
                                        foreach($this->resourceSchedules as $resource){?>
                                                <li>

                                                        <div class="resourceLabel<?php echo $resource->available ? "" :" unavailable";?>"><a name="<?php psafe($resource->id);?>" href="<?php $this->internalLink("resource", num($resource->id)."#Bokningar");?>"><?php psafe("{$resource->name}, {$resource->type}")?></a></div>
                                                        <ul class="eventList">
                                                        <?php foreach($resource->events as $event){?>
                                                                <li>
                                                                        <div class="eventLabel">
                                                                        <?php echo $event->name; ?>
                                                                        </div>
                                                                        <ul class="bookingList">
                                                                                <?php foreach($event->bookings as $booking){ 

                                                                                        $offset = (((strtotime($booking->starts_at) - $this->starts_at)/3600) * $this->hour_width)  ;
                                                                                        $width = (((strtotime($booking->ends_at) - strtotime($booking->starts_at))/3600) * $this->hour_width-2);
                                                                                        $returnUrl = urlencode($this->internalLink("resource", "?format=schedule#".num($resource->id),false));
                                                                                ?>
                                                                                <li style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
                                                                                        <a onClick="return confirm('<?php echo $this->translate("resourceBooking/deleteWarning");?>');"  href="<?php $this->internalLink("resourceBooking", num($booking->booking_id)."/".CONTROLLER_ACTION_DELETE."?returnPath={$returnUrl}");?>"><?php psafe($booking->starts_at ." ".$booking->ends_at)?></a>
                                                                                </li>
                                                                                <?php } ?>
                                                                        </ul>
                                                                        <span class="clear"></span>
                                                                </li>

                                                        <?php }?>
                                                        </ul>
                                                        <span class="clear"></span>
                                                </li>
                                        <?php }
                                        ?>
                                        </ul>
                                        <script>
                                                (function(){

                                                        var $occationList;
                                                        var $form;
                                                        var $dialogue;
                                                        var $okButton;
                                                        var $cancelButton;

                                                        function createStructure(){
                                                                $dialogue = $("<div id='lbDialogue'></div>").appendTo($("body"));
                                                                $form = $("<form method='post' action='<?php $this->internalLink("resourceBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>'></form>").appendTo($dialogue);
                                                                $occationList = $("<ul class='occationList'></ul>").appendTo($form);
                                                                $okButton = $("<input class='ajaxSelector_okButton submitButton' type='submit' value='ok'>").appendTo($form);
                                                                $cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>").appendTo($form);
                                                                $("<span class='clear'></span>").appendTo($dialogue);

                                                                $okButton.click(function(event){
                                                                        $dialogue.closeDOMWindow();
                                                                });
                                                                $cancelButton.click(function(event){
                                                                        $dialogue.closeDOMWindow();
                                                                });
                                                        }

                                                        function init(){
                                                            createStructure();
                                                            $(".resourceLabel a").each(function(key,obj){
                                                                    $resourceLink = $(obj);
                                                                    $resourceLink.click(createResourceClickHandler($resourceLink));
                                                            });
                                                        }

                                                        function createResourceClickHandler($resourceLink){
                                                                var $eventList = $($resourceLink.closest("li").find(".eventList"));
                                                                var onSaveCallback = createOnSaveCallback($eventList);
                                                                return function(event){
                                                                        $form.ajaxForm({"preventBuiltInCallback" : true, "saveCallback" : onSaveCallback });
                                                                        event.preventDefault();
                                                                        event.stopPropagation();
                                                                        var pattern = /\/(\d+)(\/)?#/g
                                                                        var resourceId = pattern.exec($resourceLink.attr("href"))[1];
                                                                        $form.attr("action","<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>".replace("{:resource_id}",resourceId));
                                                                        fillDialogue(resourceId);
                                                                        $resourceLink.openDOMWindow({modal : true, width:400, height:450, windowSourceID : "#lbDialogue" });
                                                                }
                                                        }

                                                        function createOnSaveCallback($eventList){
                                                                return function(form,data){
                                                                        console.log(data);
                                                                        var schedule = data.data.resourceSchedule[0];
                                                                        var masterTimespan = data.data.masterTimespan;
                                                                        var startsAt = new Date(masterTimespan.starts_at);
                                                                        // var endsAt = new Date(masterTimespan.ends_at);
                                                                        // var lengthInHours = ((endsAt / 1000)- (startsAt/1000)) / 3600;
                                                                        $eventList.html("");
                                                                        for(key in schedule.events){
                                                                                var event = schedule.events[key];
                                                                                var bookings = event.bookings;
                                                                                console.log(event);
                                                                                $listItem = $("<li></li>").appendTo($eventList);
                                                                                $("<span class='clear'></span>").appendTo($eventList);
                                                                                $label = $("<div class='eventLabel'>"+event.name+"</div>").appendTo($listItem);
                                                                                $bookingList = $("<ul class='bookingList'></ul>").appendTo($listItem);
                                                                                for(bookingKey in bookings){
                                                                                        var booking = bookings[bookingKey];
                                                                                        var bookingStartsAt = new Date(booking.starts_at);
                                                                                        var bookingEndsAt = new Date(booking.ends_at);
                                                                                        var width = ((((bookingEndsAt / 1000)- (bookingStartsAt/1000)) / 3600) * 18) - 1;
                                                                                        var offset = (((bookingStartsAt / 1000)- (startsAt/1000)) / 3600) * 18;
                                                                                        $link = $("<a href='#'>"+booking.id+"</a>");
                                                                                        console.log(booking);

                                                                                        $listItem = $("<li style='width:"+width+"px; left:"+offset+"px'></li>").append($link).appendTo($bookingList);
                                                                                        $link.click(createDeleteClickHandler($listItem,booking.booking_id));
                                                                                }
                                                                        }
                                                                        $("<span class='clear'></span>").appendTo($listItem);

                                                                }
                                                        }

                                                        function createEventListItem(event){
                                                                var $listItem = $("<li class='event'> <input type='checkbox'/> "+event.name+"</li>"); 
                                                                return $listItem;
                                                        }

                                                        function createEventOccationListItem(occation){
                                                                var cssClass = occation.ok_to_book == 0 ? " colliding" : ""; 
                                                                return $("<li class='eventOccation"+cssClass+"'> <input "+ ( occation.already_booked==1 ? "disabled='disabled' checked='checked'" : "") +" type='checkbox' name='eventOccation[]' value='"+ occation.id +"'/> "+occation.timespan+ (occation.name ? ", " +occation.name : "") +"</li>");
                                                        }

                                                        function fillDialogue(resourceId){

                                                                $.ajax({
                                                                          url: "<?php $this->internalLink("eventOccation","?format=json&resourceId=")?>"+ resourceId,
                                                                          dataType: 'json',
                                                                          success: function(data){

                                                                                $occationList.html("");
                                                                                var tree = data.data;
                                                                                for(eventKey in tree){
                                                                                    var event = tree[eventKey];
                                                                                    var eventListItem = createEventListItem(event).appendTo($occationList);
                                                                                    var occationListItems = [];
                                                                                    for(occationKey in event.eventOccations){
                                                                                            var occation = event.eventOccations[occationKey];
                                                                                            var eventOccationListItem = createEventOccationListItem(occation,event).appendTo($occationList);
                                                                                            occationListItems[occationListItems.length] = eventOccationListItem.find("input")[0];
                                                                                    }
                                                                                    var onClick = (function(occationListItems){
                                                                                            return function(event){
                                                                                                    if($(event.target).attr("checked")=== "checked")
                                                                                                            $(occationListItems).attr("checked","checked");
                                                                                                    else
                                                                                                            $(occationListItems).removeAttr("checked");
                                                                                            };
                                                                                    })(occationListItems);

                                                                                    var $checkBox = eventListItem.find("input");
                                                                                    $checkBox.click(onClick);
                                                                                }
                                                                          }
                                                                });	
                                                        }

                                                        init();

                                                        $(".bookingList li").each(function(key,obj){
                                                                var $listItem = $(obj);
                                                                $listItem.find("a").click(createDeleteClickHandler($listItem));
                                                        });

                                                        function createDeleteClickHandler($listItem,id){

                                                                var $link = $listItem.find("a");
                                                                $link.removeProp("onclick");

                                                                if(!id){
                                                                        var pattern = /\/(\d+)\/<?php echo $this->translate("actions/delete"); ?>/g
                                                                        var id = pattern.exec($link.attr("href"))[1];
                                                                }

                                                                var callback = function(){
                                                                        if($listItem.siblings().length == 0){
                                                                                $listItem.parent().closest("li").remove();
                                                                        }else{
                                                                                $listItem.remove();
                                                                        }
                                                                }

                                                                return function(event){
                                                                        event.stopPropagation();
                                                                        event.preventDefault();
                                                                        if(confirm($.translate("resourceBooking/deleteWarning"))){
                                                                                var url = "<?php $this->internalLink("booking", "%s/" .CONTROLLER_ACTION_DELETE."?format=json");?>".replace("%s", id);							
                                                                                $.ajax({
                                                                                  url: url,
                                                                                  success: callback,
                                                                                  type:"get"
                                                                                });
                                                                        }
                                                                };
                                                        }

                                                })();
                                        </script>
                                </div>
                        </div>
			<?php if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){?>
			<div class="section">
				<div class="sectionContent">
					<h4 class="sectionHeader"><?php echo $this->translate("general/delete");?></h4>
					<?php $this->renderDeleteResourceButton($this->resource->getId()) ?>
				</div>
			</div>
			<?php } ?>
		<?php }
		}
		
                
                
                
		public function getDeleteBookingLink($bookingId){
			 return $this->internalLink("resource", num($this->resource->getId())."/".CONTROLLER_ACTION_DELETE_BOOKING."?booking_id=".num($bookingId), false);
		}
		
		
		/**
		 * @param Timespan $timespan
		 */
		public function getTimespanLabel($timespan){
			
			if($timespan->starts_at == "1900-01-01 00:00:00" && $timespan->ends_at == "9999-12-31 23:59:59"){
				return $this->translate("general/entireConvention");
			}
			return (string) $timespan;
		}
	}