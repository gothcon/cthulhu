<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
class ScheduleView extends HtmlView{
	var $resourceSchedules;
	public function init() {
		$this->resourceSchedules = $this->getModel()->getResourceSchedules();
		
		$this->starts_at = strtotime($this->getModel()->getMasterTimespan()->starts_at);
		$this->ends_at = strtotime($this->getModel()->getMasterTimespan()->ends_at);
		
		$this->offset = 0;
		$this->master_start_hour = $this->getModel()->getMasterTimespan()->starts_at;
		$this->hour_width = 18;
		$this->total_width = (($this->ends_at - $this->starts_at) / 3600) * $this->hour_width ; 
	}
	public function render() {?>
            <div id="scheduleListContainer" style="width:<?php echo $this->total_width+200;?>px;">
                    <ul id="dateList">
                    <?php
                            $j= 0;
                            $dateBoxes = array();
                            $current_time = $this->starts_at;
                            $dateBox = new stdClass();
                            $dateBox->offset = 0;
                            $dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
                            $dateBox->length_in_hours = 0;
                            $currentDayOfMonth = strftime("%d",$current_time);
                            while($current_time < $this->ends_at){
                                    if(strftime("%d",$current_time) != $currentDayOfMonth){
                                            $currentDayOfMonth = strftime("%d",$current_time);
                                            $dateBoxes[] = $dateBox;
                                            $new_offset = $dateBox->offset + $dateBox->length_in_hours;
                                            $dateBox = new stdClass();
                                            $dateBox->offset = $new_offset;
                                            $dateBox->label = utf8_encode(strftime('%a %d/%m',$current_time));
                                            $dateBox->length_in_hours = 0;
                                    }
                                    $dateBox->length_in_hours++;
                                    $current_time += 3600;
                            }
                            $dateBoxes[] = $dateBox;

                            foreach($dateBoxes as $dateBox){
                                    $width = $dateBox->length_in_hours*$this->hour_width;
                                    $label = $dateBox->label;
                                    $offset = $dateBox->offset * $this->hour_width;
                                    ?>
                                    <li class="<?php echo $j++ %2 ?"even" :"odd";?>" style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
                                            <span class="dateLabel"><?php echo $label;?></span>
                                    </li>
                                    <?php
                            }
                            ?>
                    </ul>
                    <ul id="hourList">
                            <?php 
                            $i = 0;
                            for($currentTime = $this->starts_at; $currentTime < $this->ends_at; $currentTime += 3600){ 
                                    $width = $this->hour_width;
                                    $left = $this->hour_width*(($currentTime - $this->starts_at) / 3600);
                                    ?>
                            <li class="<?php echo $i++ %2 ?"even" :"odd";?>" style="width:<?php echo $width;?>px; left:<?php echo $left  ;?>px">
                            <?php psafe(strftime("%H",$currentTime))?>
                            </li>
                            <?php } ?>
                    </ul>
                    <ul id="scheduleList">
                    <?php
                    foreach($this->resourceSchedules as $resource){?>
                            <li>

                                    <div class="resourceLabel<?php echo $resource->available ? "" :" unavailable";?>"><a name="<?php psafe($resource->id);?>" href="<?php $this->internalLink("resource", num($resource->id)."#Bokningar");?>"><?php psafe("{$resource->name}, {$resource->type}")?></a></div>
                                    <ul class="eventList">
                                    <?php foreach($resource->events as $event){?>
                                            <li>
                                                    <div class="eventLabel">
                                                    <?php echo $event->name; ?>
                                                    </div>
                                                    <ul class="bookingList">
                                                            <?php foreach($event->bookings as $booking){ 

                                                                    $offset = (((strtotime($booking->starts_at) - $this->starts_at)/3600) * $this->hour_width)  ;
                                                                    $width = (((strtotime($booking->ends_at) - strtotime($booking->starts_at))/3600) * $this->hour_width-2);
                                                                    $returnUrl = urlencode($this->internalLink("resource", "?format=schedule#".num($resource->id),false));
                                                            ?>
                                                            <li style="left:<?php echo $offset;?>px; width:<?php echo $width;?>px;">
                                                                    <a onClick="return confirm('<?php echo $this->translate("resourceBooking/deleteWarning");?>');"  href="<?php $this->internalLink("resourceBooking", num($booking->booking_id)."/".CONTROLLER_ACTION_DELETE."?returnPath={$returnUrl}");?>"><?php psafe($booking->starts_at ." ".$booking->ends_at)?></a>
                                                            </li>
                                                            <?php } ?>
                                                    </ul>
                                                    <span class="clear"></span>
                                            </li>

                                    <?php }?>
                                    </ul>
                                    <span class="clear"></span>
                            </li>
                    <?php }
                    ?>
                    </ul>
                    <script>
                            (function(){

                                    var $occationList;
                                    var $form;
                                    var $dialogue;
                                    var $okButton;
                                    var $cancelButton;

                                    function createStructure(){
                                            $dialogue = $("<div id='lbDialogue'></div>").appendTo($("body"));
                                            $form = $("<form method='post' action='<?php $this->internalLink("resourceBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>'></form>").appendTo($dialogue);
                                            $occationList = $("<ul class='occationList'></ul>").appendTo($form);
                                            $okButton = $("<input class='ajaxSelector_okButton submitButton' type='submit' value='ok'>").appendTo($form);
                                            $cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>").appendTo($form);
                                            $("<span class='clear'></span>").appendTo($dialogue);

                                            $okButton.click(function(event){
                                                    $dialogue.closeDOMWindow();
                                            });
                                            $cancelButton.click(function(event){
                                                    $dialogue.closeDOMWindow();
                                            });
                                    }

                                    function init(){
                                        createStructure();
                                        $(".resourceLabel a").each(function(key,obj){
                                                $resourceLink = $(obj);
                                                $resourceLink.click(createResourceClickHandler($resourceLink));
                                        });
                                    }

                                    function createResourceClickHandler($resourceLink){
                                            var $eventList = $($resourceLink.closest("li").find(".eventList"));
                                            var onSaveCallback = createOnSaveCallback($eventList);
                                            return function(event){
                                                    $form.ajaxForm({"preventBuiltInCallback" : true, "saveCallback" : onSaveCallback });
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    var pattern = /\/(\d+)(\/)?#/g
                                                    var resourceId = pattern.exec($resourceLink.attr("href"))[1];
                                                    $form.attr("action","<?php $this->internalLink("eventOccationBooking",CONTROLLER_ACTION_NEW."?multiple=true&resource_id={:resource_id}");?>".replace("{:resource_id}",resourceId));
                                                    fillDialogue(resourceId);
                                                    $resourceLink.openDOMWindow({modal : true, width:400, height:450, windowSourceID : "#lbDialogue" });
                                            }
                                    }

                                    function createOnSaveCallback($eventList){
                                            return function(form,data){
                                                    console.log(data);
                                                    var schedule = data.data.resourceSchedule[0];
                                                    var masterTimespan = data.data.masterTimespan;
                                                    var startsAt = new Date(masterTimespan.starts_at);
                                                    // var endsAt = new Date(masterTimespan.ends_at);
                                                    // var lengthInHours = ((endsAt / 1000)- (startsAt/1000)) / 3600;
                                                    $eventList.html("");
                                                    for(key in schedule.events){
                                                            var event = schedule.events[key];
                                                            var bookings = event.bookings;
                                                            console.log(event);
                                                            $listItem = $("<li></li>").appendTo($eventList);
                                                            $("<span class='clear'></span>").appendTo($eventList);
                                                            $label = $("<div class='eventLabel'>"+event.name+"</div>").appendTo($listItem);
                                                            $bookingList = $("<ul class='bookingList'></ul>").appendTo($listItem);
                                                            for(bookingKey in bookings){
                                                                    var booking = bookings[bookingKey];
                                                                    var bookingStartsAt = new Date(booking.starts_at);
                                                                    var bookingEndsAt = new Date(booking.ends_at);
                                                                    var width = ((((bookingEndsAt / 1000)- (bookingStartsAt/1000)) / 3600) * 18) - 1;
                                                                    var offset = (((bookingStartsAt / 1000)- (startsAt/1000)) / 3600) * 18;
                                                                    $link = $("<a href='#'>"+booking.id+"</a>");
                                                                    console.log(booking);

                                                                    $listItem = $("<li style='width:"+width+"px; left:"+offset+"px'></li>").append($link).appendTo($bookingList);
                                                                    $link.click(createDeleteClickHandler($listItem,booking.booking_id));
                                                            }
                                                    }
                                                    $("<span class='clear'></span>").appendTo($listItem);

                                            }
                                    }

                                    function createEventListItem(event){
                                            var $listItem = $("<li class='event'> <input type='checkbox'/> "+event.name+"</li>"); 
                                            return $listItem;
                                    }

                                    function createEventOccationListItem(occation){
                                            var cssClass = occation.ok_to_book == 0 ? " colliding" : ""; 
                                            return $("<li class='eventOccation"+cssClass+"'> <input "+ ( occation.already_booked==1 ? "disabled='disabled' checked='checked'" : "") +" type='checkbox' name='eventOccation[]' value='"+ occation.id +"'/> "+occation.timespan+ (occation.name ? ", " +occation.name : "") +"</li>");
                                    }

                                    function fillDialogue(resourceId){

                                            $.ajax({
                                                      url: "<?php $this->internalLink("eventOccation","?format=json&resourceId=")?>"+ resourceId,
                                                      dataType: 'json',
                                                      success: function(data){

                                                            $occationList.html("");
                                                            var tree = data.data;
                                                            for(eventKey in tree){
                                                                var event = tree[eventKey];
                                                                var eventListItem = createEventListItem(event).appendTo($occationList);
                                                                var occationListItems = [];
                                                                for(occationKey in event.eventOccations){
                                                                        var occation = event.eventOccations[occationKey];
                                                                        var eventOccationListItem = createEventOccationListItem(occation,event).appendTo($occationList);
                                                                        occationListItems[occationListItems.length] = eventOccationListItem.find("input")[0];
                                                                }
                                                                var onClick = (function(occationListItems){
                                                                        return function(event){
                                                                                if($(event.target).attr("checked")=== "checked")
                                                                                        $(occationListItems).attr("checked","checked");
                                                                                else
                                                                                        $(occationListItems).removeAttr("checked");
                                                                        };
                                                                })(occationListItems);

                                                                var $checkBox = eventListItem.find("input");
                                                                $checkBox.click(onClick);
                                                            }
                                                      }
                                            });	
                                    }

                                    init();

                                    $(".bookingList li").each(function(key,obj){
                                            var $listItem = $(obj);
                                            $listItem.find("a").click(createDeleteClickHandler($listItem));
                                    });

                                    function createDeleteClickHandler($listItem,id){

                                            var $link = $listItem.find("a");
                                            $link.removeProp("onclick");

                                            if(!id){
                                                    var pattern = /\/(\d+)\/<?php echo $this->translate("actions/delete"); ?>/g
                                                    var id = pattern.exec($link.attr("href"))[1];
                                            }

                                            var callback = function(){
                                                    if($listItem.siblings().length == 0){
                                                            $listItem.parent().closest("li").remove();
                                                    }else{
                                                            $listItem.remove();
                                                    }
                                            }

                                            return function(event){
                                                    event.stopPropagation();
                                                    event.preventDefault();
                                                    if(confirm($.translate("resourceBooking/deleteWarning"))){
                                                            var url = "<?php $this->internalLink("booking", "%s/" .CONTROLLER_ACTION_DELETE."?format=json");?>".replace("%s", id);							
                                                            $.ajax({
                                                              url: url,
                                                              success: callback,
                                                              type:"get"
                                                            });
                                                    }
                                            };
                                    }

                            })();
                    </script>
            </div>
        <?php
	}
}