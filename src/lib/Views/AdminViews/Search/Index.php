<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
	class IndexView extends HtmlView{
		public function init(){
			
		}
		
		public function getEventUrl($eventId){
			return $this->internalLink("event", num($eventId), false);
		}
		
		public function getPersonLink($personId){
			return $this->internalLink("person", num($personId), false);
		}

		public function getResourceLink($resourceId){
			return $this->internalLink("resource", num($resourceId), false);
		}
		
		public function getGroupLink($groupId){
			return $this->internalLink("group", num($groupId), false);
		}
		
		public function render(){?>
			<div class="searchFormContainer">
					<form method="get" class="searchForm">
						<?php Html::Text("q",$this->getModel()->getSearchTerm());?>
						<?php Html::SubmitButton($this->translate("search/search"));?>
						<h3><?php echo $this->translate("search/limitSearchAreas");?></h3>
						<ul>
							<li><?php Html::Checkbox("r[0]", "people", fromRequest(array("r",0),false),array(),false); echo $this->translate("search/people");?></li>
							<li><?php Html::Checkbox("r[1]", "events", fromRequest(array("r",1),false),array(),false); echo $this->translate("search/events");?></li>
							<li><?php Html::Checkbox("r[2]", "resources", fromRequest(array("r",2),false),array(),false); echo $this->translate("search/resources");?></li>
							<li><?php Html::Checkbox("r[3]", "groups", fromRequest(array("r",3),false),array(),false); echo $this->translate("search/groups");?></li>
						</ul>
					</form>	
				<?php if(is_array($this->model->getSearchResult())){ ?>
					<h2><?php printf($this->translate("search/results"),count($this->model->getSearchResult()))?></h2>
					<?php if(count($this->model->getSearchResult())){
						$searchResult = $this->getModel()->getSearchResult();
					?>
					<ul class="searchResult">	
						<?php foreach($searchResult as $resultItem){
							switch($resultItem["type"]){
								case "event":
									$link = $this->getEventUrl($resultItem["id"]);
									$type = $this->translate("search/types/event");
									break;
								case "person":
									$link = $this->getPersonLink($resultItem["id"]);
									$type = $this->translate("search/types/person");
									$resultItem["label"] = preg_replace('/\(([0-9]{6})([0-9]{4})\)/', '(${1}-XXXX)', $resultItem["label"]);
									break;
								case "group":
									$link = $this->getGroupLink($resultItem["id"]);
									$type = $this->translate("search/types/group");
									break;
								default:
									// resource
									$link = $this->getResourceLink($resultItem["id"]);
									$type = $this->translate("search/types/resource")." ".$resultItem["type"];

									break;
							}
							?>
							<li><h3><a class="label" href="<?php echo $link;?>"><?php psafe($resultItem["label"])?></a></h3>
								<span class="meta"><?php psafe($type)?></span>
							</li>
							<?php 
						}?>
						
					</ul>
					<?php
					}else{?> 
						Inga matchningar funna.
					<?php } 
				}?>
			</div>
		<?php }
	}