<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../../Views/HtmlView.php");
require_once("lib/ClassLib/Html.php");
class IndexView extends HtmlView{
	
	
	public function init() {
		
	}

	public function render() { ?>
		<div ng-app="Statistics" ng-controller="StatisticsAppController">
            <div gc-tabs class="tabs-container"></div>
            <div gc-loader></div>
			<gc-statistics></gc-statistics>
            <span class="clear"></span>
        </div>
		<?php
	}

}
