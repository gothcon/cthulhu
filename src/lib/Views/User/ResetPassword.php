<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../HtmlView.php");
	class ResetPasswordView extends HtmlView{
		public function init(){
			
		}
		public function render(){?>
			<div class="resetPasswordContainer">
				<h2>Nollställ lösenord</h2>
				<h3>Jag minns mitt användarnamn och vill nollställa det</h3>
				<form method="post">
					<p>Ange användarnamn</p>
					<?php Html::Text("username");?>
					<?php Html::SubmitButton("Skicka");?>
					<p class="errorText"><?php psafe($this->model->getUsernameErrorMessage())?></p>
					<p class="successText"><?php psafe($this->model->getUsernameSuccessMessage())?></p>
				</form>
				<h3>Jag minns inte mitt användarnamn och vill få det mailat till mig</h3>
				<form method="post">
					<p>Ange den epostadress du angett vid registreringen</p>
					<?php Html::Text("email_address");?>
					<?php Html::SubmitButton("Skicka");?>
					<p class="errorText"><?php psafe($this->model->getEmailAddressErrorMessage());?></p>
					<p class="successText"><?php psafe($this->model->getEmailAddressSuccessMessage());?></p>
				</form>
				<p class="contactInfo">Om du inte minns ditt användarnamn och inte kommer ihåg/kommer åt brevlådan för den epostadress du angivit, var vänlig maila anmälningsansvarig</p>
			</div>
		<?php }
	}