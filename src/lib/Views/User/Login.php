<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../HtmlView.php");
	class LoginView extends HtmlView{
                private $loginFormUrl;
		public function init(){
			$this->errorMessage = $this->model->getErrorMessage();
                        $this->loginFormUrl = $this->internalLink("user", CONTROLLER_ACTION_LOGIN."?return_url={$this->getModel()->getReturnUrl()}",false);
		}
		public function render(){?>
			<div class="loginForm" >
				<form action="<?php echo $this->loginFormUrl;  ?>" method="post">
					<fieldset class="formBox">
						<label for="username"><?php echo $this->translate("user/username");?></label>
						<?php Html::Text("username"); ?>
						<label for="password"><?php echo $this->translate("user/password");?></label>
						<?php Html::Password("password"); ?>
                                                <div> <span class="errorText"><?php phtml($this->errorMessage);?></span></div>
						<?php Html::SubmitButton($this->translate("user/login"));?>
					</fieldset>
				</form>
				<span class="clear"></span>
				<a class="forgotPasswordLink" href="<?php $this->internalLink("user",CONTROLLER_ACTION_FORGOT_PASSWORD);?>">Glömt lösenord?</a>
			</div>
		<?php }
	}