<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../HtmlView.php");
	class SetNewPasswordView extends HtmlView{
		public function init(){
		}
		public function render(){?>
			<div class="resetPasswordContainer">
				<h2>Skriv in nytt lösenord</h2>
				<form method="post">
					<fieldset class="formBox">
					<label for="password"><?php echo $this->translate("user/newPassword")?></label>
					<?php Html::Password("password"); ?>
					<label for="repeat_password"><?php echo $this->translate("user/repeatNewPassword")?></label>
					<?php Html::Password("repeat_password"); ?>
					<?php Html::SubmitButton("Skicka");?>
					</fieldset>
					<p class="errorText"><?php psafe($this->model->getPasswordErrorMessage())?></p>
					<p class="successText"><?php psafe($this->model->getPasswordSuccessMessage())?></p>
				</form>
			</div>
		<?php }
	}