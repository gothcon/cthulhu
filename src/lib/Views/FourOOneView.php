<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/HtmlView.php");
	class FourOOneView extends HtmlView{
		
		public function init() {
			header("Status: 401 Unauthorized");
		}
		
		public function render(){?>
		<h3>Du har inte rätt att komma åt denna sida.</h3>
		
		<?php }
	}