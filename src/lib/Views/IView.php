<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IView
 *
 * @author Joakim
 */
interface IView {
	function init();
	function render();
	public function setModel(&$model);
	public function getModel();
	public function translate($string,$lang="sv_SE");
}

?>
