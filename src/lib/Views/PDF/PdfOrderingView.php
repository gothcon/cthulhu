<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
class PdfOrderingView extends TCPDFView{
	
	protected $starts_at;
	protected $ends_at;
	protected $scheduleRowHeight = 10;
	protected $minimumFontSize = 2;
	
	/** @var TreeItem_ArticleOrderingStatistics[] */
	protected $articleTree;

	public function init() {
		$this->setHeaderTemplateAutoreset(true);
		$this->articleTree = $this->model->getArticles();
		$this->AddFont("DejaVuSans");
		$this->SetCellPadding(1);
		parent::init();
	}
	
	public function render() {
		$this->HideHeader();
		$this->HideFooter();
		$this->AddPage();
		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
		$this->SetY(20);
		$title = $this->getModel()->getTitle();
		$this->Cell(0, $this->getPageHeight() - 81, $title,0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
		$this->Bookmark($title);
		$this->ShowFooter();
		$this->ShowHeader();
		
		// sammanfattningssida
		$this->SetFieldData(Field::CENTER_HEADER_FIELD, "Sammanfattning");
		$this->AddPage();
		$this->Bookmark("Sammanfattning",1);
		
		$this->SetFont("DejaVuSans", FontStyle::UNDERLINED, 10);

		$cellWidth_articleName = $this->w - 100 - $this->lMargin - $this->rMargin;
		
		$this->Cell(20, 0, "Art.id", 0, 0,  Alignment::LEFT);
		$this->Cell($cellWidth_articleName, 0, "Artikel", 0, 0,  Alignment::LEFT,false,'',1);
		$this->Cell(40, 0, "Antal beställda", 0, 0,  Alignment::LEFT);
		$this->Cell(40, 0, "Antal betalade", 0, 1,  Alignment::LEFT);

		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 9);
		$i=0;
		foreach($this->articleTree as $article){
			$this->SetFillColor($i++%2?255:240);
			$this->Cell(20, 0,$article->article_id, 0, 0,  Alignment::LEFT,true);
			$this->Cell($cellWidth_articleName, 0, $article->article_name, 0, 0,  Alignment::LEFT,true,'',1);
			$this->Cell(40, 0, $article->no_ordered, 0, 0,  Alignment::LEFT,true);
			$this->Cell(40, 0, $article->no_paid, 0, 1,  Alignment::LEFT,true);
		}
		
		foreach($this->articleTree as $article){
			$this->SetFieldData(Field::CENTER_HEADER_FIELD, $article->article_name);
			$this->AddPage();
			$this->Bookmark($article->article_name,1);
			$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
			
			$this->Cell(0, $this->GetX(), $article->article_name,0,1,  Alignment::LEFT,false,'',1);
			$orderRows = $article->getOrderRows();
			$this->SetFont("DejaVuSerif",  '' ,9);
			$this->SetFillColor(255);
			
			$this->SetFont("DejaVuSans", FontStyle::NORMAL, 14);

			$label = "Antal beställda: ";
			$labelWidth = $this->GetStringWidth($label);
			$this->Cell( $labelWidth, 0, $label, 0, 0,  Alignment::LEFT);
			$this->Cell(0, 0, $article->no_ordered, 0, 1,  Alignment::LEFT);
			
			$label = "Antal betalda: ";
			$labelWidth = $this->GetStringWidth($label);
			$this->Cell( $labelWidth, 0, $label, 0, 0,  Alignment::LEFT);
			$this->Cell(0, 0, $article->no_paid, 0, 1,  Alignment::LEFT);

			$this->SetFont("DejaVuSans", FontStyle::UNDERLINED, 10);
			
			$this->Cell(15, 0, "antal", 0, 0,  Alignment::LEFT);
			$this->Cell(50, 0, "beställare", 0, 0,  Alignment::LEFT);
			$this->Cell(70, 0, "notering", 0, 0,  Alignment::LEFT);
			$this->Cell(0, 0, "betald?", 0, 1,  Alignment::LEFT);

			$this->SetFont("DejaVuSans", FontStyle::NORMAL, 9);
			$i=0; 
			
			
			
				
			foreach($orderRows as $orderRow){
				$this->SetFillColor( $i++ % 2 ? 255 : 240 );

				$x = $this->GetX();
				$y = $this->GetY();					
				$multicellWidth = 70;
				$start_page = $this->getPage();
				// print the multicell
				$this->startTransaction();
				$this->MultiCell($multicellWidth, 0, $orderRow->note, false, Alignment::LEFT,true,2,$x+65);
				if($this->getPage() != $start_page){
					$this->rollbackTransaction(true);
					$this->AddPage();
					$y = $this->GetY();					
					$this->MultiCell($multicellWidth, 0, $orderRow->note, false, Alignment::LEFT,true,2,$x+65);
				}else{
					$this->commitTransaction();
				}


				$yAfterMultiCell = $this->getY();
				$xAfterMultiCell = $this->getX();
				$cellHeight = $yAfterMultiCell - $y;
				$this->SetY($y);
				$this->Cell(15, $cellHeight, $orderRow->article_count, 0, 0,  Alignment::LEFT,true);
				$this->Cell(50, $cellHeight, $orderRow->customer, 0, 0,  Alignment::LEFT,true);
				$this->SetX($xAfterMultiCell);
				$this->Cell(0, $cellHeight, ($orderRow->is_paid ? "Ja" : "Nej"), 0, 1,  Alignment::LEFT,true);
			}
		
		}
		parent::render();
	}
	
	/**
	 * 
	 * @param TreeItem_Event $event
	 */
	protected function renderArticlePage($event){
		// update header
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $event->name);
		
		// add new page and bookmark it
		$this->AddPage();
		$this->Bookmark($event->name,2);
		
		// output event name and add bookmarks
		$this->SetFont("DejaVuSans",'',24);
		$this->Cell(0,0,$event->name,0,1);
		$this->SetFont("DejaVuSerif",  '' ,9);
		
		$this->SetTextColor(80);
		if(strlen($event->preamble)){
			$this->SetFont("DejaVuSerif", FontStyle::ITALIC ,9);
			$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $event->preamble,0,1);
			$this->Ln(3);
			$this->Cell(0,0,"***",0,1,  Alignment::CENTER);
			$this->Ln(3);
		}
		$this->setTextColor(0);
		if(strlen($event->text)){
			$this->SetFont("DejaVuSerif",  '' ,9);
			$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $event->text,0,1);
			$this->Ln(5);
		}
		
		$info = strlen($event->info) ? $event->info ."<br/>" : "";
		$info.= "<table><thead><tr><th><b>Tider</b></th><th><b>Samlingssal</b></th><th><b>Anmälningsbart för</b></th></thead></tr>";
		// event occation list
		foreach($event->event_occations as $eventOccation){
			$info .= "<tr><td>{$eventOccation->timespan_as_string}</td><td>".(strlen($eventOccation->main_resource)?$eventOccation->main_resource:" - ")."</td>";
				$slotTypes = array();
				foreach($eventOccation->signup_slots as $slot){
					$slotTypes[] = $slot->slot_type_name;
				}
			$types = implode(", ", $slotTypes);
			$info .= "<td>". (strlen($types) ? $types : "Ingen föranmälning") . "</td></tr>";
		}
		$info .= "</table>";
		$this->SetFont("DejaVuSerif",  '' ,9);
		$this->SetFillColor(230);
		$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $info,1,1,true);
		$this->Ln(5);
	}
}



?>
