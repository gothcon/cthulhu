<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
class PdfResourceScheduleView extends TCPDFView{
	
	protected $starts_at;
	protected $ends_at;
	
	/** @var PdfResourceItem[] */
	protected $resources;
	protected $title;

	public function init() {
		$this->setHeaderTemplateAutoreset(true);
		$this->title = $this->getModel()->getTitle();
		$this->resources = $this->model->GetResources();
		$masterTimespan = $this->model->getMasterTimeSpan();
		$this->starts_at = $masterTimespan->starts_at;
		$this->ends_at = $masterTimespan->ends_at;
		$this->AddFont("DejaVuSans");
		parent::init();
	}
	
	public function render() {
		$this->HideHeader();
		$this->HideFooter();
		$this->AddPage();
		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
		$this->SetY(20);
		$this->Cell(0, $this->getPageHeight() - 81, $this->title,0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
		$this->Bookmark( $this->title);
		$this->ShowHeader();
		foreach($this->resources as $resource){
			$this->renderResourcePage($resource);
		}
		parent::render();
	}
	
	/**
	 * 
	 * @param PdfResourceItem $resource
	 */
	protected function renderResourcePage($resource){
		// update header
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $resource->name);
		// add new page and bookmark it
		$this->AddPage();
		$this->ShowFooter();
		$this->Bookmark($resource->name,1);
		
		// output event name and add bookmarks
		$this->SetFont("DejaVuSans",'',24);
		$this->Cell(0,0,$resource->name,0,1);
		$this->SetFont("DejaVuSerif",  '' ,9);
		$this->SetTextColor(80);
		
		$master_timespan_start = new DateTime(substr($this->starts_at,0,10));
		$master_timespan_end = new DateTime(substr($this->ends_at,0,10));

		while($master_timespan_start <= $master_timespan_end){
			// extrahera det datum som 
			$day_start = new DateTime($master_timespan_start->format("Y-m-d 00:00:00"));
			$day_end = new DateTime($master_timespan_start->format("Y-m-d 00:00:00"));
			$day_end->modify("+1 day");
			
			$day = new Day();
			$count = 0;
			$day->nameOfDay = $master_timespan_start->format("l");
			
			
			
			foreach($resource->bookings as $booking)
				{
				$booking_start	= new DateTime($booking->starts_at);
				$booking_end	= new DateTime($booking->ends_at);
				if($booking_start > $day_end || $booking_end < $day_start){
					$timespanDescription = "";
					// not this day
					continue;
				}else{
					
					if($booking_start < $day_start && $booking_end > $day_end){
						// hela dagen
						$timespanDescription = "Hela dagen";
					}else if($booking_start >= $day_start && $booking_end <= $day_end){
						// booking starts and ends on this day
						$timespanDescription = $booking_start->format("H:i") ." - ". $booking_end->format("H:i");
					}else if($booking_start < $day_start && $booking_end <= $day_end){
						$timespanDescription = "Fram till ". $booking_end->format("H:i");
					}else if($booking_end > $day_end && $booking_start > $day_start){
						$timespanDescription = "Från och med " . $booking_start->format("H:i");
					}
				}
				
				$booking->timespan_description = $timespanDescription;		
				$day->bookings[$count] = $booking;
				$count++;
			}
			
			$this->SetFont("DejaVuSans", FontStyle::BOLD ,16);
			$this->Ln();
			$this->Cell(0,0,$this->dayNameToSwedish($day->nameOfDay),0,1);
			$this->SetFont("DejaVuSerif",  '' ,10);
			if(count($day->bookings) == 0){
				$this->Cell(0,0,"Inget inbokat",0,1);
			}
			foreach($day->bookings as $booking){
				$timespan = new Timespan();
				$timespan->starts_at = $booking->starts_at;
				$timespan->ends_at = $booking->ends_at;
				$timespan->name = $booking->timespan_description;
				$this->Cell(90,0,$booking->description,0,0,  Alignment::LEFT,false,'',1);
				$this->Cell(0,0,$booking->timespan_description,0,1,  Alignment::LEFT,false,'',1);
				$this->Ln(2);
			}
			$master_timespan_start->modify("+ 1 day");
		}
	}
	
	protected function dayNameToSwedish($day){
		$day = strtolower($day);
		switch($day){
			case "monday":
				return "Måndag";
				break;
			case "tuesday":
				return "Tisdag";
				break;
			case "wednesday":
				return "Onsdag";
				break;
			case "thursday":
				return "Torsdag";
				break;
			case "friday":
				return "Fredag";
				break;
			case "saturday":
				return "Lördag";
				break;
			case "sunday":
				return "Söndag";
				break;
		}
	}
	
}



class Day{
	public $nameOfDay;
	/** @var PDFResourceBookingItem[] */
	public $bookings = array();
}



?>
