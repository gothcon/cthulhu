<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
class PdfEventOverView extends TCPDFView{
	

	
	protected $starts_at;
	protected $ends_at;
	protected $scheduleRowHeight = 10;
	protected $minimumFontSize = 2;
	/**
	 *
	 * @var TreeItem_EventType[]
	 */
	protected $eventTree;

	public function init() {
		$this->setHeaderTemplateAutoreset(true);
		$this->eventTree = $this->model->getEventTree();
		$this->AddFont("DejaVuSans");
		parent::init();
	}
	
	public function render() {
		$this->HideHeader();
		$this->HideFooter();
		$this->AddPage();
		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
		$this->SetY(20);
		$this->Cell(0, $this->getPageHeight() - 81, "Arrangemangssammanfattning",0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
		$this->Bookmark("Arrangemangssammanfattning");
		$this->ShowFooter();
		$this->ShowHeader();
		foreach($this->eventTree as $eventType){
			foreach($eventType->events as $event){
				$this->renderEventDetailsPage($event);
			}
		}
		parent::render();
	}
	
	protected function renderEventDetailsPage($event){
		// update header
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $event->name);
		
		// add new page and bookmark it
		$this->AddPage();
		$this->Bookmark($event->name,1);
		
		// output event name and add bookmarks
		$this->SetFont("DejaVuSans",'',24);
		$this->Cell(0,0,$event->name,0,1);
		$this->SetFont("DejaVuSerif",'',14);
		$this->Bookmark("Passdetaljer",2);
		$this->Ln(4);
		$this->Cell(0, 0, "Arrangör: ". $event->organizer,0,1);
		$this->Ln(4);
		$output = "";
		// event occation list
		foreach($event->event_occations as $eventOccation){
			$output .= "{$eventOccation->full_name}\nSamlingssal: ".(strlen($eventOccation->main_resource)?$eventOccation->main_resource:" - ")."\n\n";
		}
		$this->MultiCell(0,0, $output, 0 ,1);
		
		// event occation detail pages
		foreach($event->event_occations as $eventOccation){
			$this->renderEventOccationPage($eventOccation,$event);
		}
	}
	/**
	 * 
	 * @param TreeItem_EventOccation $eventOccation
	 * @param TreeItem_Event $event
	 */
	protected function renderEventOccationPage($eventOccation,$event){
		// echo "Top margin is" .$this->tMargin." at the beginning of event occation page\n";
		// create the signup and resource booking column headers
		$columnCount =  count($eventOccation->signup_slots) +1;
		$columnWidth = ($this->getAvailableWidth()) / $columnCount;
		
		$columnHeaders = array("Bokade resurser");
		foreach($eventOccation->signup_slots as $signupSlot){
			$columnHeaders[] = $signupSlot->slot_type_name;
		}
		
		// update the header information
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $event->name." - ".$eventOccation->name);

		// add a page and bookmark it
		$this->AddPage();
		$this->Bookmark($eventOccation->full_name,2);

		// set the top information font
		$this->SetFont("DejaVuSans", "", 24);
		
		// print event name
		$this->Cell(0,0,$event->name,0,1);
		
		// print main resource information
		$this->SetFont("DejaVuSerif", "", 14);
		$this->MultiCell(0,0,$eventOccation->full_name."\nSamlingssal: ".(strlen($eventOccation->main_resource)?$eventOccation->main_resource:" - "),0,1);
		$this->Ln(8);
		
		// print column headers
		$this->SetFont("DejaVuSans", FontStyle::BOLD.FontStyle::UNDERLINED,12);
		foreach($columnHeaders as $columnHeader){
			$this->Cell($columnWidth, 0, $columnHeader, 0, 0);
		}
		$this->Ln();
		
		// set this as top margin, so that multicells that flows over to the next page leaves room for the headings
		$margins = $this->getMargins();
		$initialTopMargin =  $margins[Margin::TOP_MARGIN]; 
		$this->SetTopMargin($this->getY());

		$this->SetFont("DejaVuSerif", "",10);
		$output = "";
		$i = 1;

		foreach($eventOccation->booked_resources as $resourceBooking){
			$output .= $i++.". ".$resourceBooking->resource_name."\n";
		}
		$this->MultiCell($columnWidth, 0, $output, 0,  Alignment::LEFT, false, 0);
		foreach($eventOccation->signup_slots as $signupSlot){
			$count = 0;
			$output = "";
			foreach($signupSlot->signups as $signup){
				$count++;
				$output .= $count.". ".$signup->signee_name."\n";
			}
			$this->MultiCell($columnWidth, 0, $output, 0, Alignment::LEFT, false, 0);
		}
		// check if this is the last page

		// reset the top margin
		$this->SetTopMargin($initialTopMargin);
		
		
		// print event occation information on any added pages
		while($this->numpages > $this->page){
			// reset the top and bottom margin on that particular page
			$this->pagedim[$this->page+1]['tm'] = $initialTopMargin;
			$this->setPage($this->page+1,true);
			
			$this->SetTopMargin($initialTopMargin);
			// set the top information font
			$this->SetFont("DejaVuSans", "", 24);

			// print event name
			$this->Cell(0,0,$event->name,0,1);

			// print main resource information
			$this->SetFontSize(14);
			$this->MultiCell(0,0,$eventOccation->name." (forts.)\nSamlingssal: ".(strlen($eventOccation->main_resource)?$eventOccation->main_resource:" - "),0,1);
			$this->Ln(8);

			// print column headers
			$this->SetFont("DejaVuSans", "B",12);
			foreach($columnHeaders as $columnHeader){
				$this->Cell($columnWidth, 0, $columnHeader, 0, 0);
			}
		}
	
		// echo "Top margin is" .$this->tMargin." at the end of event occation page\n";

	}
	

}



?>
