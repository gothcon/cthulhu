<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
class PdfSleeperListView extends TCPDFView{
	
	
	/** @var PdfResourceItem[] */
	protected $resources;
	protected $title;

	public function init() {
		$this->setHeaderTemplateAutoreset(true);
		$this->title = $this->getModel()->getTitle();
		$this->resources = $this->model->GetResources();
		
		$this->AddFont("DejaVuSans");
		parent::init();
	}
	
	public function render() {
		$this->HideHeader();
		$this->HideFooter();
		$this->AddPage();
		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
		$this->SetY(20);
		$this->Cell(0, $this->getPageHeight() - 81, $this->title,0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
		$this->Bookmark( $this->title);
		$this->ShowHeader();
		foreach($this->resources as $resource){
			$this->renderResourcePage($resource);
		}
		parent::render();
	}
	
	/**
	 * 
	 * @param PdfSleepingResource $resource
	 */
	protected function renderResourcePage($resource){
		// update header
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $resource->name);
		// add new page and bookmark it
		$this->AddPage();
		$this->ShowFooter();
		$this->Bookmark($resource->name,1);
		
		// output event name and add bookmarks
		$this->SetFont("DejaVuSans",'',24);
		$this->Cell(0,0,$resource->name,0,1);
		$this->SetFont("DejaVuSans",'',13);
		$this->Cell(0,0,"Inbokade sovande",0,1);
		$this->SetFont("DejaVuSerif",  '' ,9);
		
		$this->SetTextColor(80);
		$i = max(count($resource->sleepers), $resource->no_of_slots);
		$this->SetCellPadding(1);
		for($j = 0; $j < $i; $j++){
			if($j >= $resource->no_of_slots){
				$this->SetFont("DejaVuSerif", FontStyle::ITALIC,"9");
			}
			$sleeper = isset($resource->sleepers[$j]) ? $resource->sleepers[$j]: "";
			$this->Cell(10, 0, ($j+1).".", 1 , 0, Alignment::RIGHT, false, '', 1);
			$this->Cell(0,  0, $sleeper, 1 , 1, '', false, '', 1);
		}
		
	}
	
	protected function dayNameToSwedish($day){
		$day = strtolower($day);
		switch($day){
			case "monday":
				return "Måndag";
				break;
			case "tuesday":
				return "Tisdag";
				break;
			case "wednesday":
				return "Onsdag";
				break;
			case "thursday":
				return "Torsdag";
				break;
			case "friday":
				return "Fredag";
				break;
			case "saturday":
				return "Lördag";
				break;
			case "sunday":
				return "Söndag";
				break;
		}
	}
	
}
?>
