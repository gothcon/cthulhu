<?php



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");
class PdfScheduleView extends TCPDFView{
	
	protected $starts_at;
	protected $ends_at;
	protected $scheduleRowHeight = 10;
	protected $minimumFontSize = 2;
	protected $defaultFont = "DejaVuSans" ;
	protected $defaultFontSize = 7;

	/** @var TreeItem_EventType */
	protected $eventTree;

	public function init() {
		parent::init();
		$this->eventTree = $this->getModel()->GetEventTree();
		$this->setPageFormat("A4",'L');	
		$this->hideFooter();
		$this->hideHeader();
		// use hours and use some margin
		$this->starts_at	= floor(strtotime($this->getModel()->getMasterTimespan()->starts_at)/ 3600) * 3600;
		$this->ends_at		= ceil(	strtotime($this->getModel()->getMasterTimespan()->ends_at)	/ 3600) * 3600;
		$this->SetLeftMargin(5);
		$this->SetRightMargin(5);
		$this->SetTopMargin(5);
		$this->SetAutoPageBreak("on", 5);
		$this->SetLineWidth(0.05);
		$this->setCellPaddings(0.4,0.5,0.4);
	}
	
	public function drawBackground(){
		
		$availablePageWidth = $this->getAvailableWidth();
		$hourCount = ($this->ends_at - $this->starts_at) / 3600;
		$hourColumnWidth = ($availablePageWidth / $hourCount);
		$availablePageHeight = $this->getAvailableHeight()-5;
		$this->Ln(5);

		$i = 0;
		$dateBoxWidth = 0;
		$dateString = strftime('%a %d/%m',$this->starts_at);
		
		for($currentTime = $this->starts_at; $currentTime < $this->ends_at; $currentTime += 3600){
			$this->SetDrawColor(220);
			$this->setCellPaddings(0.4,0.5,0.4);
			$hour = strftime("%H",$currentTime);
			
			if($i++%2 == 0){
				$this->SetFillColor(255);
			}else{
				$this->SetFillColor(240);
			}
			$this->Cell($hourColumnWidth, $availablePageHeight, $hour,1,0,  Alignment::CENTER,true,'',1,false, Alignment::TOP, Alignment::TOP);
			
			$dateBoxWidth += $hourColumnWidth;
			
			if($hour == "00" || $currentTime + 3600 == $this->ends_at){
				$this->SetDrawColor(50);
				$this->setCellPaddings(0.7,0.5,0.7);
				$this->SetFillColor(255);
				$currentX = $this->GetX();
				$currentY = $this->GetY();
				$this->SetX($currentX - $dateBoxWidth);
				// echo ($currentX - $dateBoxWidth."\n");
				// echo ($dateString."\n");
				$this->SetY($currentY - 5,false);
				$this->Cell($dateBoxWidth, 5, $dateString, 1, 0, Alignment::LEFT, true);
				$dateBoxWidth = 0;
				$dateString = utf8_encode(strftime('%a %d/%m',$currentTime));
				$this->setXY($currentX,$currentY);
			}
		}
		$this->SetDrawColor(50);
		$this->setY($this->tMargin);
		$this->Ln(5);
		$this->Rect($this->GetX(), $this->GetY(), $availablePageWidth, $availablePageHeight, '' ,'');
		
		$this->Ln(5);
		
		$timespans = $this->getModel()->getVisibleTimespans();
		$cellHeight = $availablePageHeight - 5;
		
		$this->setCellPaddings(0.4,0.5,0.4);
		$this->SetFillColor(255, 204, 0);
		foreach($timespans as $timespan){
			$timespan_starts_at = strtotime($timespan->starts_at);
			$timespan_ends_at	= strtotime($timespan->ends_at);
			$timespan_width		= (($timespan_ends_at - $timespan_starts_at)/3600) * $hourColumnWidth;
			$timespan_x_offset	= ($timespan_starts_at - $this->starts_at)/3600 * $hourColumnWidth;
			$label = strtoupper($timespan->name);
			$this->SetX($timespan_x_offset + $this->lMargin);
			$this->Cell($timespan_width, $cellHeight, $label,1,0,  Alignment::LEFT,true,'',1,false, Alignment::TOP, Alignment::TOP);
		}
		
	}
	
	public function drawEventOccations(){
		$this->SetY(20);
		$this->SetFillColor(255);
		$hourCount = ($this->ends_at - $this->starts_at) / 3600;
		$hourColumnWidth = ($this->getAvailableWidth() / $hourCount);
		$eventTypeSections = array();
		
		foreach($this->eventTree as $eventType){
			$eventTypeSection = new EventTypeSection($eventType->name);
			foreach($eventType->events as $event){
				$eventTypeSection->addEvent($event);
			}
			$eventTypeSections[$eventType->id] = $eventTypeSection;
		}
		
		foreach($eventTypeSections as $eventTypeSection){
			foreach($eventTypeSection->rows as $row){
				foreach($row->rowOccations as $block){
					$block_starts_at = strtotime($block->starts_at);
					$block_ends_at	= strtotime($block->ends_at);
					$block_width	= (($block_ends_at - $block_starts_at)/3600) * $hourColumnWidth;
					$block_x_offset	= ($block_starts_at - $this->starts_at)/3600 * $hourColumnWidth;
					$label = $block->label;
					$this->SetX($block_x_offset + $this->lMargin);
					$this->Cell($block_width, 0, $label,1,0,  Alignment::LEFT,true,'',1,false, Alignment::TOP, Alignment::TOP);
				}
				$this->Ln();
				$this->Ln(0.8);
			}
			$this->Ln(2);
		}
	}
	
	public function render() {
		$this->AddPage();
		$this->SetFont($this->defaultFont, '', 9);
		$this->drawBackground();
		$this->SetFont($this->defaultFont, '', 8);
		$this->drawEventOccations();
//		$this->pdf->SetFont($this->defaultFont, '', $this->defaultFontSize);
		parent::render();
	}

}


class EventTypeSection{
	var $eventType;
	/** @var type */
	var $rows = array();
	
	/** @param string $eventType*/
	public function __construct($eventType){
		$this->eventType = $eventType;
	}
	
	public function addEvent($event){
		$addedSuccessfully = false;
		foreach($this->rows as $row){
			$addedSuccessfully = $row->addEventOccations($event);
			
			if($addedSuccessfully)
				break;
		}
		if(!$addedSuccessfully){
			$row = new EventTypeRow();
			$row->addEventOccations($event);
			$this->rows[] = $row;
		}
	}
}

class EventTypeRow{
	
	var $rowOccations = array();
	
	/**
	 * 
	 * @param TreeItem_Event $event
	 * @return boolean
	 */
	public function addEventOccations($event){
		
		foreach($event->event_occations as $eventOccation){
			$timespan = new Timespan();
			$timespan->starts_at = $eventOccation->starts_at;
			$timespan->ends_at = $eventOccation->ends_at;
			foreach($this->rowOccations as $rowOccation){
				if($timespan->overlaps($rowOccation->timespan)){
					return false;
				}
			}
		}
		
		foreach($event->event_occations as $eventOccation){
			$occationLabel = strlen(trim($event->schedule_name)) != 0 ? $event->schedule_name: $event->name;
			$this->rowOccations[] = new RowOccation($eventOccation->starts_at , $eventOccation->ends_at, $occationLabel);
		}
		
		return true;
		
	}
}

/**
 * @property-read string $starts_at 
 * @property-read string $ends_at 
 */
class RowOccation{
	var $label;
	var $timespan;
	
	public function __construct($starts_at,$ends_at,$label){
		$this->timespan = new Timespan();
		$this->timespan->starts_at = $starts_at;
		$this->timespan->ends_at = $ends_at;
		$this->label = $label;
	}
	
	public function __get($name){
		if($name == "starts_at")
			return $this->timespan->starts_at;
		if($name == "ends_at")
			return $this->timespan->ends_at;
		else 
			die("property not found");
	}
}

?>
