<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../IView.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../TCPDFView.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDFSchemaView
 *
 * @author Joakim
 */
class PdfFolderView extends TCPDFView{
	
	protected $starts_at;
	protected $ends_at;
	protected $scheduleRowHeight = 10;
	protected $minimumFontSize = 2;
	
	/** @var TreeItem_EventType[] */
	protected $eventTree;

	public function init() {
		$this->setHeaderTemplateAutoreset(true);
		$this->eventTree = $this->model->getEventTree();
		$this->AddFont("DejaVuSans");
		parent::init();
	}
	
	public function render() {
		$this->HideHeader();
		$this->HideFooter();
		$this->AddPage();
		$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
		$this->SetY(20);
		$this->Cell(0, $this->getPageHeight() - 81, "Folder",0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
		$this->Bookmark("Folder");
		$this->ShowFooter();
		$this->ShowHeader();
		foreach($this->eventTree as $eventType){
			$this->SetFieldData(Field::CENTER_HEADER_FIELD, $eventType->name);
			$this->AddPage();
			$this->SetFont("DejaVuSans", FontStyle::NORMAL, 24);
			$this->SetY(20);
			$this->Cell(0, $this->getPageHeight() - 81, $eventType->name,0,1,  Alignment::RIGHT,false,'',0,false,  Alignment::TOP,  Alignment::BOTTOM);
			$this->Bookmark($eventType->name,1);
			foreach($eventType->events as $event){
				$this->renderEventDetailsPage($event);
			}
		}
		parent::render();
	}
	
	/**
	 * 
	 * @param TreeItem_Event $event
	 */
	protected function renderEventDetailsPage($event){
		// update header
		$this->SetFieldData(Field::RIGHT_HEADER_FIELD, $event->name);
		
		// add new page and bookmark it
		$this->AddPage();
		$this->Bookmark($event->name,2);
		
		// output event name and add bookmarks
		$this->SetFont("DejaVuSans",'',24);
		$this->Cell(0,0,$event->name,0,1);
		$this->SetFont("DejaVuSerif",  '' ,9);
		
		$this->SetTextColor(80);
		if(strlen($event->preamble)){
			$this->SetFont("DejaVuSerif", FontStyle::ITALIC ,9);
			$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $event->preamble,0,1);
			$this->Ln(3);
			$this->Cell(0,0,"***",0,1,  Alignment::CENTER);
			$this->Ln(3);
		}
		$this->setTextColor(0);
		if(strlen($event->text)){
			$this->SetFont("DejaVuSerif",  '' ,9);
			$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $event->text,0,1);
			$this->Ln(5);
		}
		
		$info = strlen($event->info) ? $event->info ."<br/>" : "";
		$info.= "<table><thead><tr><th><b>Tider</b></th><th><b>Samlingssal</b></th><th><b>Anmälningsbart för</b></th></thead></tr>";
		// event occation list
		foreach($event->event_occations as $eventOccation){
			$info .= "<tr><td>{$eventOccation->timespan_as_string}</td><td>".(strlen($eventOccation->main_resource)?$eventOccation->main_resource:" - ")."</td>";
				$slotTypes = array();
				foreach($eventOccation->signup_slots as $slot){
					$slotTypes[] = $slot->slot_type_name;
				}
			$types = implode(", ", $slotTypes);
			$info .= "<td>". (strlen($types) ? $types : "Ingen föranmälning") . "</td></tr>";
		}
		$info .= "</table>";
		$this->SetFont("DejaVuSerif",  '' ,9);
		$this->SetFillColor(230);
		$this->writeHTMLCell(0, 0, $this->GetX(),$this->GetY(), $info,1,1,true);
		$this->Ln(5);
		
		
	}
	
}



?>
