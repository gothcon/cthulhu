<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PdfView
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/HtmlView.php");
require_once("lib/FPDF/CustomTfpdf.php");

class PdfView extends View{
	
	/**
	 *
	 * @var PDFModel
	 */
	protected $model;

	/**
	 *
	 * @var CustomTfpdf
	 */
	protected $pdf;
	protected $pageWidth = 842;
	protected $pageHeight = 595;
	protected $topMargin = 20;
	protected $bottomMargin = 20;
	protected $leftMargin = 20;
	protected $rightMargin = 20;
	protected $defaultFont = "DejaVuSans";
	protected $defaultFontSize = 12;
	protected $hideHeader = false;
	protected $hideFooter = false;
	protected $lineThickness =0.5;
	
	protected function getAvailablePageWidth(){
		return $this->pageWidth - $this->leftMargin - $this->rightMargin - ($this->lineThickness * 2);
	}
	
	protected function getAvailablePageHeight(){
		return $this->pageHeight - $this->topMargin - $this->bottomMargin - ($this->lineThickness * 2);
	}
	
	public function init(){
		$this->pageWidth=$this->getModel()->getPageWidth();
		$this->pageHeight=$this->getModel()->getPageHeight();
		$this->pdf = new CustomTfpdf('L','pt',array($this->pageHeight,$this->pageWidth));
		$this->pdf->SetCreator($this->getModel()->getCreator());
		$this->pdf->SetAuthor($this->getModel()->getAuthor());
		$this->pdf->SetTitle($this->getModel()->getTitle());
	    $this->pdf->SetMargins($this->leftMargin,$this->topMargin,$this->rightMargin);
		$this->pdf->SetLineWidth($this->lineThickness);
		$this->pdf->SetFontSize($this->defaultFontSize);
		$this->pdf->AddFont("DejaVuSans", '', '', true);
		
	}
	
	public function render(){
		$this->pdf->Close();
		$title = $this->getModel()->getTitle();
		header("Cache-Control: no-cache, must-revalidate");
		header("Expires: Sat, 01 Dec 1970 00:00:00 GMT");
		$this->pdf->Output("{$title}.pdf","I");
	}
	
	protected $pagesHaveBeenCreated = false;
	
	public function addPage(){
		if($this->pagesHaveBeenCreated){
			if(!$this->hideFooter){
				$this->addFooter();
			}
		}else{
			$this->pagesHaveBeenCreated = true;
		}
		$this->pdf->addPage();
	    $this->pdf->SetXY($this->leftMargin,$this->topMargin);
		if(!$this->hideHeader){
			$this->addHeader();
		}
	}
	
	protected function resetXY(){
	    $this->pdf->SetXY($this->leftMargin,$this->topMargin);
	}
	
	public function addHeader(){
	}
	
	public function addFooter(){
	}
}