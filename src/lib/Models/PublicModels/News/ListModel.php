<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{	
	public function __construct(){
		parent::__construct();
		$this->init("newsArticles",array());
	}

	public function getNewsArticles(){
		return $this->get("newsArticles");
	}
	
	public function setNewsArticles($value){
		$this->set("newsArticles",$value);
	}
	
}		

