<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("newsArticle",new NewsArticle());
	}

	public function getNewsArticle(){
		return $this->get("newsArticle");
	}
	
	public function setNewsArticle($value){
		$this->set("newsArticle",$value);
	}
}		

