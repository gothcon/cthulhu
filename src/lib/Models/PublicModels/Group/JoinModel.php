<?php 
require_once("lib/Models/HtmlListModel.php");

Class JoinModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("groupList",array());
	}

	public function getGroupList(){
		return $this->get("groupList");
	}
	
	public function setGroupList($value){
		$this->set("groupList",$value);
	}
	
}	

