<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("group",new Group());
		$this->init("groupmembers",array());
		$this->init("signups",array());
		$this->init("isMember",false);
		$this->init("isLeader",false);
	}

	public function getGroup(){
		return $this->get("group");
	}
	
	public function setGroup($value){
		$this->set("group",$value);
	}

	public function getIsMember(){
		return $this->get("isMember");
	}
	
	public function setIsMember($value){
		$this->set("isMember",$value === true);
	}

	public function getIsLeader(){
		return $this->get("isLeader");
	}
	
	public function setIsLeader($value){
		$this->set("isLeader",$value === true);
	}

	public function getGroupMembers(){
		return $this->get("groupmembers");
	}
	
	public function setGroupMembers($value){
		$this->set("groupmembers",$value);
	}
	
	public function setSignupList($value){
		$this->set("signups",$value);
	}

	public function getSignupList(){
		return $this->get("signups");
	}

}	

