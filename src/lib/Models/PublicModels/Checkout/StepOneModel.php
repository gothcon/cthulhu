<?php 
require_once("lib/Models/HtmlListModel.php");

Class StepOneModel extends HtmlModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("cart",new Order());
	}		
	
	public function setCart($value){
		$this->set("cart",$value);
	}
	
	public function getCart(){
		return $this->get("cart");
	}

}
