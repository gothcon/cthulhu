<?php 
require_once("lib/Models/HtmlListModel.php");

Class StepTwoModel extends HtmlModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("cart",new Order());
		$this->init("paymentMethods",array());
	}		
	
	public function setCart($value){
		$this->set("cart",$value);
	}
	
	public function getCart(){
		return $this->get("cart");
	}
	
	public function SetAvailablePaymentMethods($value){
		$this->set("paymentMethods",$value);
	}
	
	public function GetAvailablePaymentMethods(){
		return $this->get("paymentMethods");
	}

}
