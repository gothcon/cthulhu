<?php 
require_once("lib/Models/HtmlModel.php");

Class OrderSuccessModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("order",new Order());
	}

	public function getOrder(){
		return $this->get("order");
	}
	
	public function setOrder($value){
		$this->set("order",$value);
	}
}	

