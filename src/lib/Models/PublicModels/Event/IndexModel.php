<?php 
require_once("lib/Models/HtmlListModel.php");

Class IndexModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("eventTypeList",array());
		$this->init("occationList",array());
		$this->init("currentEvent",false);
		$this->init("userGroups",array());
	}		

	public function setCurrentEvent($value){
		$this->set("currentEvent",$value);
	}		
	public function getCurrentEvent(){
		return $this->get("currentEvent");
	}

	public function setOccationTree($occations){
		$this->set("occationList",$occations);
		
	}
	public function getOccationTree(){
		return $this->get("occationList");
		
	}
	
	
	public function getUserGroups(){
		return $this->get("userGroups");
	}
	public function setUserGroups($value){
		$this->set("userGroups",$value);
	}	
	
	public function getEventTypeList(){
		return $this->get("eventTypeList");
	}
	public function setEventTypeList($value){
		$this->set("eventTypeList",$value);
	}

}
