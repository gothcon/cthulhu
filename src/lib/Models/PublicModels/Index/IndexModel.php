<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("news",array());
		$this->init("order",new Order());
		$this->init("signups",array());
	}

		public function getNews(){
			return $this->get("news");
		}
		
		public function setNews($news){
			$this->set("news",$news);
		}
		/**
		 * 
		 * @return Order
		 */
		public function getCart(){
			return $this->get("order");
		}
		
		public function setCart($cart){
			$this->set("order",$cart);
		}
		public function getSignups(){
			return $this->get("signups");
		}
		
		public function setSignups($signups){
			$this->set("signups",$signups);
		}
}		

