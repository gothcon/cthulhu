<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("eventTypeList",array());
		$this->init("eventTree",array());
		$this->init("slotList",array());
		$this->init("currentEventType",false);
		$this->init("userGroups",array());
	}

	public function getUserGroups(){
		return $this->get("userGroups");
	}
	
	public function setUserGroups($value){
		$this->set("userGroups",$value);
	}		

	public function getCurrentEventType(){
		return $this->get("currentEventType");
	}
	
	public function setCurrentEventType($value){
		$this->set("currentEventType",$value);
	}		
	
	public function getEventTypeList(){
		return $this->get("eventTypeList");
	}
	
	public function setEventTypeList($value){
		$this->set("eventTypeList",$value);
	}
	
	public function loadEventTree(){
		return $this->get("eventTree");
	}
	
	public function setEventTree($value){
		$this->set("eventTree",$value);
	}	
	
	public function setSignupSlotList($value){
		$this->set("slotList",$value);
	}
	
	public function getSignupSlotList(){
		return $this->get("slotList");
	}

}
