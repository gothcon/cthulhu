<?php 
require_once("lib/Models/HtmlModel.php");

Class ListModel extends HtmlModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("categories",array());
		$this->init("articles",array());
		$this->init("article", new Article());
		$this->init("currentCart", new Order());
		$this->init("article_path",array());
		$this->init("currentCustomer",new Person());
	}

	public function getCategories(){
		return $this->get("categories");
	}
	
	public function setCategories($value){
		$this->set("categories",$value);
	}

	public function getArticles(){
		return $this->get("articles");
	}
	
	public function setArticles($value){
		$this->set("articles",$value);
	}

	public function getArticle(){
		return $this->get("article");
	}
	
	public function setArticle($value){
		$this->set("article",$value);
	}

	public function getArticlePath(){
		return $this->get("article_path");
	}
	
	public function setArticlePath($value){
		$this->set("article_path",$value);
	}

	public function getCurrentCart(){
		$this->get("currentCart");
		return $this->get("currentCart");
	}
	
	public function setCurrentCart($value){
		$this->set("currentCart",$value);
	}
	
	public function getCurrentCustomer(){
		return $this->get("currentCustomer");
	}
	
	public function setCurrentCustomer($value){
		$this->set("currentCustomer",$value);
	}
	

}	

