<?php
class JsonModel{
	
	const STATUS_OK = 0; 
	const STATUS_INVALID_REQUEST_DATA = 1; 
	const STATUS_REDIRECT = 2;
	const STATUS_INSUFFICIENT_PERMISSION = 2;
	
	protected $status = JsonModel::STATUS_OK;
	protected $data;
	protected $message = "";
	
	

	public function setMessage($message){
		$this->message = $message;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function getStatus(){
		return $this->status;
	}
	
	public function setStatus($status){
		switch($status){
			case JsonModel::STATUS_OK:
			case JsonModel::STATUS_INVALID_REQUEST_DATA:
			case JsonModel::STATUS_REDIRECT:
			$this->status = $status;
		}
	}
	
	public function setData($data){
		$this->data = $data;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function getJsonData(){
		return new JsonReturnData($this->getData(),$this->getStatus(),$this->getMessage());
	}
}	

class JsonReturnData{
	public $data;
	public $status = JsonModel::STATUS_OK;
	public $message = "";
	public function __construct($data,$status,$message){
		$this->data = $data;
		$this->status = $status;
		$this->message = $message;
	}
}