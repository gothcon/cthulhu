<?php
class SimpleJsonModel{
	protected $data;
	
	public function setData($data){
		$this->data = $data;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function getJsonData(){
		return $this->getData();
	}
}	