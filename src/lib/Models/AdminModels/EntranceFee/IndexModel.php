<?php 
require_once("lib/Models/HtmlModel.php");
require_once("lib/Database/Entities/EntranceFee.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("entranceFee",new Article());
	}

	public function getEntranceFee(){
		return $this->get("entranceFee");
	}
	
	public function setEntranceFee($value){
		$this->set("entranceFee",$value);
	}
}		

