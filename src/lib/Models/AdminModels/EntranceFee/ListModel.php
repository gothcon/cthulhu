<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{	public function __construct(){
	parent::__construct();
		$this->init("entranceFees",array());
	}

	public function getEntranceFees(){
		return $this->get("entranceFees");
	}
	
	public function setEntranceFees($value){
		$this->set("entranceFees",$value);
	}
}		

