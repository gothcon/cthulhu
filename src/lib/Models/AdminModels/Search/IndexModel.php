<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	function __construct(){
		parent::__construct();
		$this->init("searchResult",false);
		$this->init("searchTerm","");
	}
	
	function getSearchResult(){
		return $this->get("searchResult");
	}
	
	function setSearchResult($searchResult){
		$this->set("searchResult",$searchResult);
	}

	function getSearchTerm(){
		return $this->get("searchTerm");
	}
	
	function setSearchTerm($searchTerm){
		$this->set("searchTerm",$searchTerm);
	}

	
}	

