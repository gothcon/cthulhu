<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	function __construct(){
		parent::__construct();
		$this->init("searchResult",false);
	}
	
	function getSearchResult(){
		return $this->get("searchResult");
	}
	
	function setSearchResult($searchResult){
		$this->set("searchResult",$searchResult);
	}

}	

