<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{	
	public function __construct() {
		parent::__construct();
		$this->init("timespan",new Timespan());
		$this->init("visibleTimespans",array());
		$this->init("schedules",array());
	}
	
	// entire convention timespan
	
	public function SetMasterTimespan($timespan){
		$this->set("timespan",$timespan);
	}
	
	public function GetMasterTimespan(){
		return $this->get("timespan");
	}

	public function SetVisibleTimespans($timespans){
		$this->set("visibleTimespans",$timespans);
	}
	
	public function GetVisibleTimespans(){
		return $this->get("visibleTimespans");
	}

	// content
	
	public function SetResourceSchedules($schedules){
		$this->set("schedules",$schedules);
	}

	public function GetResourceSchedules(){
		return $this->get("schedules");
	}
}	