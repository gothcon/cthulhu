<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("eventList",array());
	}

	public function getEventList(){
		return $this->get("eventList");
	}
	
	public function setEventList($value){
		$this->set("eventList",$value);
	}	
	

}
