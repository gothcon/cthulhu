<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	function __construct(){
		parent::__construct();
		$this->init("occations",array());
		$this->init("timespanSelectList",array());
		$this->init("eventtypeSelectList",array());
		$this->init("event",new Event());
		$this->init("current_occation",new EventOccation());
		$this->init("current_timespan",new Timespan());
		$this->init("current_slot",new SignupSlot());
		$this->init("slottypeSelectList",array());
		$this->init("signupSlots",array());
		$this->init("signups",array());
		$this->init("resourceBookings",array());
	}
	
	function getOccations(){
		return $this->get("occations");
	}
	
	function setOccations($occations){
		$this->set("occations",$occations);
	}
	
	function getEventTypeSelectList(){
		return $this->get("eventtypeSelectList");
	}
	
	function setEventTypeSelectList($list){
		$this->set("eventtypeSelectList",$list);
	}
	
	function getTimespanSelectList(){
		return $this->get("timespanSelectList");
	}
	
	function setTimespanSelectList($list){
		$this->set("timespanSelectList",$list);
	}
	
	function setSlotTypeSelectList($list){
		
		$this->set("slottypeSelectList",$list);
	}
	
	function getSlotTypeSelectList(){
		
		return $this->get("slottypeSelectList");
	}
	
	function getSignupSlots(){
		return $this->get("signupSlots");
	}

	function setSignupSlots($slots){
		$this->set("signupSlots",$slots);
	}
	
	function getSignups(){
		return $this->get("signups");
	}

	function setSignups($signups){
		$this->set("signups",$signups);
	}
	function getResourceBookings(){
		return $this->get("resourceBookings");
	}

	function setResourceBookings($resourceBookings){
		$this->set("resourceBookings",$resourceBookings);
	}
	
	function getEvent(){
		return $this->get("event");
	}

	function setEvent($event){
		$this->set("event",$event);
	}
	
	function setCurrentOccation($occation){
		$this->set("current_occation",$occation);
	}
	function getCurrentOccation(){
		return $this->get("current_occation");
	}
	
	function setCurrentTimespan($current_timespan){
		$this->set("current_timespan",$current_timespan);
	}
	function getCurrentTimespan(){
		return $this->get("current_timespan");
	}

	function setCurrentSignupSlot($current_slot){
		$this->set("current_signupslot",$current_slot);
	}
	function getCurrentSignupSlot(){
		return $this->get("current_signupslot");
	}
}	

