<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("events",array());
		$this->init("eventOccations",array());
		$this->init("articles",array());
		$this->init("slotTypes",array());
		$this->init("attenders",array());
	}

	public function getEventList(){
		return $this->get("events");
	}

	public function setEventList($value){
		$this->set("events",$value);
	}
	
	/**
	public function getEventOccationList(){
		return $this->get("eventOccations");
	}

	public function setEventOccationList($value){
		$this->set("eventOccations",$value);
	}
	**/
	
	public function getArticleList(){
		return $this->get("articles");
	}

	public function setArticleList($value){
		$this->set("articles",$value);
	}
	
	public function getSlotTypeList(){
		return $this->get("slotTypes");
	}

	public function setSlotTypeList($value){
		$this->set("slotTypes",$value);
	}
	
	public function getAttenders(){
		return $this->get("attenders");
	}

	public function setAttenders($value){
		$this->set("attenders",$value);
	}
	
}		

