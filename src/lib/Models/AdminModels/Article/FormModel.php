<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{

	public function __construct(){
		parent::__construct();
		$this->init("types",array());
		$this->init("article", new Article());
		$this->init("article_path",array());
		$this->init("orderRows",array());
		$this->init("statistics",new stdClass());
	}

	public function getArticleOrderRows(){
		return $this->get("orderRows");
	}
	
	public function setArticleOrderRows($value){
		$this->set("orderRows",$value);
	}
	
	public function loadArticleOrderingStatistics(){
		return $this->get("statistics");
	}
	
	public function setArticleOrderingStatistics($value){
		$this->set("statistics",$value);
	}
	
	public function getArticle(){
		return $this->get("article");
	}
	
	public function setArticle($value){
		$this->set("article",$value);
	}

	public function getTypes(){
		return $this->get("types");
	}
	
	public function setTypes($value){
		$this->set("types",$value);
	}

	public function getArticlePath(){
		return $this->get("article_path");
	}
	
	public function setArticlePath($value){
		$this->set("article_path",$value);
	}

}	

