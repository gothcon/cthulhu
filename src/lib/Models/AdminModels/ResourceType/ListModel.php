<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	public function __construct(){
		parent::__construct();
		$this->init("resourceTypeList",array());
	}

	public function getResourceTypeList(){
		return $this->get("resourceTypeList");
	}
	
	public function setResourceTypeList($value){
		$this->set("resourceTypeList",$value);
	}
	
}	

