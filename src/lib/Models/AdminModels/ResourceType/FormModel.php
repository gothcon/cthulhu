<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("resourceType",new ResourceType());
		$this->init("resourceList",array());
		$this->init("sleepingStatistics",array());
	}

	public function getResourceType(){
		return $this->get("resourceType");
	}
	
	public function setResourceType($value){
		$this->set("resourceType",$value);
	}
	
	public function getResourceList(){
		return $this->get("resourceList");
	}
	
	public function setResourceList($value){
		$this->set("resourceList",$value);
	}
	
	public function getSleepingStatistics(){
		return $this->get("sleepingStatistics");
	}
	
	public function setSleepingStatistics($value){
		$this->set("sleepingStatistics",$value);
	}

}	

