<?php 
require_once("lib/Models/HtmlModel.php");

Class ListModel extends HtmlModel{	public function __construct(){

	parent::__construct();
		$this->init("list",array());
		$this->init("paymentMethods",array());
	}

	public function getList(){
			return $this->get("list");
		}
		
	public function setList($value){
		$this->set("list",$value);
	}
	public function getPaymentMethods(){
			return $this->get("paymentMethods");
		}
		
	public function setPaymentMethods($value){
		$this->set("paymentMethods",$value);
	}
}		

