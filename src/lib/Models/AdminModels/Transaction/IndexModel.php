<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("transaction",array());
	}

	public function getTransaction(){
		return $this->get("transaction");
	}
	
	public function setTransaction($value){
		$this->set("transaction",$value);
	}
}		

