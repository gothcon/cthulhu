<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("signups",array());
	}

	public function getSignups(){
		return $this->get("signups");
	}
	
	public function setSignups($value){
		$this->set("signups",$value);
	}

}	

