<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("resource",new Resource());
		$this->init("resource_types",array());
		$this->init("bookings",array());
		$this->init("sleeping_occations",array());
       		$this->init("timespan",new Timespan());
		$this->init("visibleTimespans",array());
		$this->init("schedules",array());
	}

	public function getResource(){
		return $this->get("resource");
	}
	
	public function setResource($value){
		$this->set("resource",$value);
	}
	
	public function getTypes(){
		return $this->get("resource_types");
	}
	
	public function setTypes($value){
		$this->set("resource_types",$value);
	}
	
	public function getBookings(){
		return $this->get("bookings");
	}
	public function setBookings($value){
		$this->set("bookings",$value);
	}
	
	public function setSleepingOccations($value){
		$this->set("sleeping_occations",$value);
	}
	
	public function getSleepingOccations(){
		return $this->get("sleeping_occations");
	}
	
	public function SetMasterTimespan($timespan){
		$this->set("timespan",$timespan);
	}
	
	public function GetMasterTimespan(){
		return $this->get("timespan");
	}

	public function SetVisibleTimespans($timespans){
		$this->set("visibleTimespans",$timespans);
	}
	
	public function GetVisibleTimespans(){
		return $this->get("visibleTimespans");
	}

	// content
	
	public function SetResourceSchedules($schedules){
		$this->set("schedules",$schedules);
	}

	public function GetResourceSchedules(){
		return $this->get("schedules");
	}
        
}	