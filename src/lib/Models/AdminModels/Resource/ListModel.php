<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("resourceList",array());
		$this->init("sleepingStatistics",array());
	}

	public function getResourceList(){
		return $this->get("resourceList");
	}
	
	public function setResourceList($value){
		$this->set("resourceList",$value);
	}	
	
	public function getSleepingStatistics(){
		return $this->get("sleepingStatistics");
	}
	
	public function setSleepingStatistics($value){
		$this->set("sleepingStatistics",$value);
	}


}	

