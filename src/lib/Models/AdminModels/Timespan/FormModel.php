<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("timespan",array());
	}
	
	public function setTimespan($list){
		$this->set("timespan",$list);
	}
	
	public function getTimespan(){
		return $this->get("timespan");
	}
}	

