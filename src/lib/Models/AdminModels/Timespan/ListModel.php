<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("timespanList",array());
	}
	
	public function setTimespanList($list){
		$this->set("timespanList",$list);
	}
	
	public function getTimespanList(){
		return $this->get("timespanList");
	}


}	

