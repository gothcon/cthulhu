<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("person",new Person());
		$this->init("user",new User());
		$this->init("signups",array());
		$this->init("groupMemberships",array());
		$this->init("orders",array());
		$this->init("userlevels",array());
		$this->init("availableSleepingResources",array());
		$this->init("sleepingSlotBooking",0);
	}

	public function setPerson($value){
		$this->set("person",$value);
	}

	public function getPerson(){
		return $this->get("person");
	}
	
	public function setGroupMembershipList($value){
		$this->set("groupMemberships",$value);
	}
	
	public function getGroupMembershipList(){
		return $this->get("groupMemberships");
	}
	
	public function setSignupList($value){
		$this->set("signups",$value);
	}

	public function getSignupList(){
		return $this->get("signups");
	}

	public function setOrderList($value){
		$this->set("orders",$value);
	}

	public function getOrderList(){
		return $this->get("orders");
	}
	public function setUser($value){
		$this->set("user",$value);
	}

	public function getUser(){
		return $this->get("user");
	}
	
	public function setUserLevelSelectData($list){
		$this->set("userlevels",$list);
	}
	
	public function getUserLevelSelectData(){
		return $this->get("userlevels");
	}
	
	public function setAvailableSleepingResources($resources){
		$this->set("availableSleepingResources",$resources);
	}

	public function getAvailableSleepingResources(){
	 return $this->get("availableSleepingResources");
	}

	public function setSleepingSlotBooking($booking){
		$this->set("sleepingSlotBooking",$booking);
	}

	public function getSleepingSlotBooking(){
	 return $this->get("sleepingSlotBooking");
	}
	
}	

