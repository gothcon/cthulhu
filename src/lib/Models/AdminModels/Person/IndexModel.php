<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("person",new Person());
	}

	public function getPerson(){
		return $this->get("person");
	}
	
	public function setPerson($value){
		$this->set("person",$value);
	}

}	

