<?php 
require_once("lib/Models/HtmlModel.php");

Class OrderPaymentModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("mostRecentTransactions",array());
		$this->init("mostRecentTransaction",array());
	}
	public function getMostRecentTransactions(){
		return $this->get("mostRecentTransactions");
	}
	public function setMostRecentTransactions($value){
		$this->set("mostRecentTransactions",$value);
	}
	public function getMostRecentTransaction(){
		return $this->get("mostRecentTransaction");
	}
	public function setMostRecentTransaction($value){
		$this->set("mostRecentTransaction",$value);
	}
}		

