<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	public function __construct(){
		parent::__construct();
		$this->init("people",array());
	}

	public function getPersonList(){
		return $this->get("people");
	}
	
	public function setPersonList($value){
		$this->set("people",$value);
	}
	
}	

