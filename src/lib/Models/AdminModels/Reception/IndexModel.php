<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("person",new Person());
		$this->init("remainingAmount",0);
		$this->init("articlesToBeDelivered",array());
		$this->init("sleepingSlotBooking",0);
		$this->init("entranceFeeIsSelected",false);		
		$this->init("entranceFeeArticles",array());
		$this->init("notes",array());
	}

	public function setPerson($value){
		$this->set("person",$value);
	}

	public function getPerson(){
		return $this->get("person");
	}
	
	public function setEntranceFeeArticles($value){
		$this->set("entranceFeeArticles",$value);
	}

	public function getEntranceFeeArticles(){
		return $this->get("entranceFeeArticles");
	}
	
	public function setRemainingAmount($value){
		$this->set("remainingAmount",$value);
	}

	public function getRemainingAmount(){
		return $this->get("remainingAmount");
	}

	public function setSleepingSlotBooking($booking){
		$this->set("sleepingSlotBooking",$booking);
	}

	public function getSleepingSlotBooking(){
		return $this->get("sleepingSlotBooking");
	}
	
	public function setArticlesToBeDelivered($value){
		$this->set("articlesToBeDelivered",$value);
	}

	public function getArticlesToBeDelivered(){
		return $this->get("articlesToBeDelivered");
	}
	
	public function setEntranceFeeSelected($value){
		$this->set("entranceFeeIsSelected",$value ? true : false);
	}
	
	public function entranceFeeSelected(){
		return $this->get("entranceFeeIsSelected");
	}
	
	public function addNote($note){
		$notes = $this->get("notes");
		$notes[] = $note;
		$this->setNotes($notes);
	}
	
	public function setNotes($notes){
		$this->set("notes",$notes);
	}

	public function getNotes(){
		return $this->get("notes");
	}
	
	
}	

