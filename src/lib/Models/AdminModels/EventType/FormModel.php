<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("eventType",new EventType());
	}

	public function getEventType(){
		return $this->get("eventType");
	}
	
	public function setEventType($value){
		$this->set("eventType",$value);
	}

}	

