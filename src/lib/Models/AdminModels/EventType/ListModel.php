<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	public function __construct(){
		parent::__construct();
		$this->init("eventTypeList",array());
	}

	public function getEventTypeList(){
		return $this->get("eventTypeList");
	}
	
	public function setEventTypeList($value){
		$this->set("eventTypeList",$value);
	}
	
}	

