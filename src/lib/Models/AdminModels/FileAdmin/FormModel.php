<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("file",array());
	}

	public function getFile(){
		return $this->get("file");
	}
	
	public function setFile($file){
		$this->set("file",$file);
	}
}	

