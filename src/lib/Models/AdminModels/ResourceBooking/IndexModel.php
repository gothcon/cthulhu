<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{

	public function __construct(){
		parent::__construct();
		$this->init("resource",new Resource());
		$this->init("events", array());
		$this->init("event_occations", array());
		$this->init("timespans", array());
	}

	public function getResource(){
		return $this->get("resource");
	}
	
	public function setResource($value){
		$this->set("resource",$value);
	}

	public function getEvents(){
		return $this->get("events");
	}
	
	public function setEvents($value){
		$this->set("events",$value);
	}	

	public function getEventOccations(){
		return $this->get("event_occations");
	}
	
	public function setEventOccations($value){
		$this->set("event_occations",$value);
	}	
	
	public function getTimespans(){
		return $this->get("timespans");
	}
	
	public function setTimespans($value){
		$this->set("timespans",$value);
	}

	
	
}	

