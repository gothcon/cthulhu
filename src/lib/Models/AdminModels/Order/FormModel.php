<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{

	public function __construct(){
		parent::__construct();
		$this->init("order",array());
	}

	public function getOrders(){
		return $this->get("order");
	}
	
	public function setOrders($value){
		$this->set("order",$value);
	}

}	

