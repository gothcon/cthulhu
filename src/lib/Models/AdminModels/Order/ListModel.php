<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	
	public function __construct(){
		parent::__construct();
		$this->init("orders",array());
	}

	public function getOrders(){
		return $this->get("orders");
	}
	
	public function setOrders($value){
		$this->set("orders",$value);
	}

}	

