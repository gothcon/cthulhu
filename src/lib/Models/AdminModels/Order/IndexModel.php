<?php 
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("order",new Order());
		$this->init("orderPaymentRemaining",0);
		$this->init("orderPayments",array());
		$this->init("orderRowStatusValues",array());
		$this->init("orderStatusValues",array());
	}

	public function getOrder(){
		return $this->get("order");
	}
	
	public function setOrder($value){
		$this->set("order",$value);
	}
	
	public function getOrderPayments(){
		return $this->get("orderPayments");
	}
	
	public function setOrderPayments($value){
		$this->set("orderPayments",$value);
	}
	
	public function getRemainingSum(){
		return $this->get("orderPaymentRemaining");
	}
	
	public function setRemainingSum($value){
		$this->set("orderPaymentRemaining",$value);
	}
	
	public function setOrderRowStatusValues($value){
		$this->set("orderRowStatusValues",$value);
	}
	public function getOrderRowStatusValues(){
		return $this->get("orderRowStatusValues");
	}	
	public function setOrderStatusValues($value){
		$this->set("orderStatusValues",$value);
	}
	public function getOrderStatusValues(){
		return $this->get("orderStatusValues");
	}

}	

