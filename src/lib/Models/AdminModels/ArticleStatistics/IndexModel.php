<?php 
namespace lib\Models\AdminModels\ArticleStatistics;
use \HtmlModel;
use \stdClass;
require_once("lib/Models/HtmlModel.php");

Class IndexModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("statistics",new stdClass());
	}

	public function getStatistics(){
		return $this->get("statistics");
	}

	public function setStatistics($value){
		$this->set("statistics",$value);
	}
	
}		