<?php 
require_once("lib/Models/HtmlModel.php");

Class FormModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("slotType",new SlotType());
	}

	public function getSlotType(){
		return $this->get("slotType");
	}
	
	public function setSlotType($value){
		$this->set("slotType",$value);
	}

}	

