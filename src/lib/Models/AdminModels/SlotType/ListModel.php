<?php 
require_once("lib/Models/HtmlListModel.php");

Class ListModel extends HtmlListModel{
	public function __construct(){
		parent::__construct();
		$this->init("slotTypeList",array());
	}

	public function getSlotTypeList(){
		return $this->get("slotTypeList");
	}
	
	public function setSlotTypeList($value){
		$this->set("slotTypeList",$value);
	}
	
}	

