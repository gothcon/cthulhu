<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchemaModel
 *
 * @author Joakim
 */
require_once("lib/Models/PdfModel.php");
class PdfResourceScheduleModel extends PdfModel{
	
	public function __construct() {
		parent::__construct();
		$this->init("resources",array());
		$this->init("timespan",null);
	}
	
	public function SetResources($resources){
		$this->set("resources",$resources);
	}
	
	/** @return PdfResourceItem[] */ 
	public function GetResources(){
		return $this->get("resources");
	}

	public function SetMasterTimespan($timespan){
		$this->set("timespan",$timespan);
	}
	
	public function GetMasterTimespan(){
		return $this->get("timespan");
	}
}

?>
