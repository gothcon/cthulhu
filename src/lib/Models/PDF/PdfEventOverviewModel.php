<?php 
require_once("lib/Models/PdfModel.php");

Class PdfEventOverviewModel extends PdfModel{	
	public function __construct(){
	parent::__construct();
		$this->init("events",array());
		$this->init("bookings",array());
	}

	public function getEventTree(){
		return $this->get("events");
	}
		
	public function setEventTree($value){
		$this->set("events",$value);
	}
	
	public function getFacilityBookingList(){
		return $this->get("bookings");
	}
		
	public function setFacilityBookingList($value){
		$this->set("bookings",$value);
	}
}	