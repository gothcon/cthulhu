<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchemaModel
 *
 * @author Joakim
 */
require_once("lib/Models/PdfModel.php");
class PdfScheduleModel extends PdfModel{
	
	public function __construct() {
		parent::__construct();
		$this->init("timespan",new Timespan());
		$this->init("visibleTimespans",array());
		$this->init("eventTree",array());
	}
	
	// entire convention timespan
	
	public function SetMasterTimespan($timespan){
		$this->set("timespan",$timespan);
	}
	
	public function GetMasterTimespan(){
		return $this->get("timespan");
	}

	public function SetVisibleTimespans($timespans){
		$this->set("visibleTimespans",$timespans);
	}
	
	public function GetVisibleTimespans(){
		return $this->get("visibleTimespans");
	}

	// content
	
	public function SetEventTree($eventTree){
		$this->set("eventTree",$eventTree);
	}

	public function GetEventTree(){
		return $this->get("eventTree");
	}

}

?>
