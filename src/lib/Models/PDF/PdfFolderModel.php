<?php 
require_once("lib/Models/PdfModel.php");

Class PdfFolderModel extends PdfModel{	
	public function __construct(){
	parent::__construct();
		$this->init("events",array());
	}

	/**
	 * 
	 * @return TreeItem_EventType[]
	 */
	public function getEventTree(){
		return $this->get("events");
	}
		
	public function setEventTree($value){
		$this->set("events",$value);
	}

}	