<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchemaModel
 *
 * @author Joakim
 */
require_once("lib/Models/PdfModel.php");
class PdfOrderingModel extends PdfModel{
	
	public function __construct() {
		parent::__construct();
		$this->init("articles",array());
	}
	
	public function SetArticles($articles){
		$this->set("articles",$articles);
	}
	
	/** @return TreeItem_Article */ 
	public function GetArticles(){
		return $this->get("articles");
	}

}

?>
