<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SchemaModel
 *
 * @author Joakim
 */
require_once("lib/Models/PdfModel.php");
class PdfSleeperListModel extends PdfModel{
	
	public function __construct() {
		parent::__construct();
		$this->init("resources",array());
	}
	
	public function SetResources($resources){
		$this->set("resources",$resources);
	}
	
	/** @return PdfSleepingResource[] */ 
	public function GetResources(){
		return $this->get("resources");
	}

}

?>
