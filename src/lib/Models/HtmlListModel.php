<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ListingModel
 *
 * @author Joakim
 */
require_once("lib/Models/HtmlModel.php");


class HtmlListModel extends HtmlModel {
	public function __construct(){
		parent::__construct();
		$this->init("sortParameters",array());
		$this->init("list",array());
	}
	
	public function getSortParameters(){
		return $this->get("sortParameters");
	}
	public function setSortParameters($value){
		$this->set("sortParameters",$value);
	}
	
	public function getList(){
		return $this->get("list");
	}
	
	public function setList($value){
		$this->set("list",$value);
	}	
}

?>
