<?php
require_once("lib/Models/Model.php");
class PdfModel extends Model{
	
	public function __construct(){
		$this->init("title","GothCon PDF");
		$this->init("author","GothCon");
		$this->init("creator","GothCon");
		$this->init("width",842);
		$this->init("height",595);
	}
	
	public function SetTitle($title){
		$this->set("title",$title);
	}
	
	public function GetTitle(){
		return $this->get("title");
	}
	
	public function SetAuthor($author){
		$this->set("author",$author);
	}
	
	public function GetAuthor(){
		return $this->get("author");
	}
	
	public function SetCreator($creator){
		$this->set("creator",$creator);
	}
	
	public function GetCreator(){
		return $this->get("creator");
	}

}