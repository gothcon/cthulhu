<?php 
require_once("lib/Models/Model.php");

Class HtmlModel extends Model{
	
	public function __construct(){
		$this->init("javascripts",array());
		$this->init("stylesheets",array());
		$this->init("title","untitled document");
		$this->init("metadata",array());
		$this->init("errors",array());
		$this->init("baseUrl","/");
		$this->init("useFriendlyUrls",true);
	}
	
	public function AddStylesheet($path){
		$list = $this->get("stylesheets");
		$list[] = $path;
		$this->set("stylesheets",$list);
	}
	
	public function GetStylesheets(){
		return $this->get("stylesheets");
	}
	
	public function AddJavascript($path){
		$list = $this->get("javascripts");
		$list[] = $path;
		$this->set("javascripts",$list);
	}
	
	public function GetJavascripts(){
		return $this->get("javascripts");
	}

	public function AddMetadata($key,$value){
		$list = $this->get("metadata");
		$list[$key] = $path;
		$this->set("metadata",$list);
	}
	
	public function GetMetadata(){
		return $this->get("metadata");
	}
	
	public function SetTitle($title){
		$this->set("title",$title);
	}
	
	public function GetTitle(){
		return $this->get("title");
	}
	
	public function SetValidationErrors($errorArray){
		$this->set("errors",$errorArray);
	}
	
	public function GetValidationErrors(){
		return $this->get("errors");
	}
	
	public function SetBaseUrl($value){
		$this->set("baseUrl",$value);
	}	
	
	public function GetBaseUrl(){
		return $this->Get("baseUrl");
	}
	
	public function SetUseFriendlyUrls($value){
		$this->set("useFriendlyUrls",$value);
	}
	public function GetUseFriendlyUrls(){
		return $this->get("useFriendlyUrls",$value);
	}
	
}	

