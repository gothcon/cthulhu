<?php
	class Model{

		public $dataArray = array();

		public function init($key,$defaultValue=null){
			if($this->isInitialized($key)){
				print_r(get_callstack());
				die("Value {$key} is already initialized");
				
			}
			$this->dataArray[$key] = $defaultValue;
		}
		
		public function set($key,$value){
			if(!$this->isInitialized($key)){
				print_r(get_callstack());
				die("No value for {$key} has been set");
			}
			$this->dataArray[$key] = $value;
		}

		public function get($key){
			if(!$this->isInitialized($key)){
				print_r(get_callstack());
				die("No value for {$key} has been set");
			}
			return $this->dataArray[$key];
		}
		
		public function isInitialized($key){
			return array_key_exists($key,$this->dataArray);
		}
	
	}