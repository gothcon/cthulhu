<?php 
require_once("lib/Models/HtmlModel.php");

Class LoginModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("errorMessage","");
		$this->init("returnUrl","");
	}

	public function getErrorMessage(){
		return $this->get("errorMessage");
	}
	
	public function setErrorMessage($value){
		$this->set("errorMessage",$value);
	}
	
	public function setReturnUrl($url){
		$this->set("returnUrl",$url);
	}
	
	public function getReturnUrl(){
		return $this->get("returnUrl");
	}
}	

