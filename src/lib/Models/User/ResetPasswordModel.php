<?php 
require_once("lib/Models/HtmlModel.php");

Class ResetPasswordModel extends HtmlModel{
	public function __construct(){
		parent::__construct();
		$this->init("usernameErrorMessage"	,"");
		$this->init("emailErrorMessage"		,"");
		$this->init("passwordErrorMessage"	,"");
		$this->init("emailSuccessMessage"	,"");
		$this->init("usernameSuccessMessage","");
		$this->init("passwordSuccessMessage","");
		
	}

	public function getUsernameErrorMessage(){
		return $this->get("usernameErrorMessage");
	}
	
	public function setUsernameErrorMessage($value){
		$this->set("usernameErrorMessage",$value);
	}

	public function getEmailAddressErrorMessage(){
		return $this->get("emailErrorMessage");
	}
	
	public function setEmailAddressErrorMessage($value){
		$this->set("emailErrorMessage",$value);
	}

	public function getEmailAddressSuccessMessage(){
		return $this->get("emailSuccessMessage");
	}
	
	public function setEmailAddressSuccessMessage($value){
		$this->set("emailSuccessMessage",$value);
	}

	public function getUsernameSuccessMessage(){
		return $this->get("usernameSuccessMessage");
	}
	
	public function setUsernameSuccessMessage($value){
		$this->set("usernameSuccessMessage",$value);
	}

	public function getPasswordSuccessMessage(){
		return $this->get("passwordSuccessMessage");
	}
	
	public function setPasswordSuccessMessage($value){
		$this->set("passwordSuccessMessage",$value);
	}

	public function getPasswordErrorMessage(){
		return $this->get("passwordErrorMessage");
	}
	
	public function setPasswordErrorMessage($value){
		$this->set("passwordErrorMessage",$value);
	}
}		

