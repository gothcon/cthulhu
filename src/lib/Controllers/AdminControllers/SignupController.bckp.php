<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");

	class SignupController extends AdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/SignupSlotController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		public static function getControllerName(){
			return "anmalning";
		}
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Signup/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Signup";
		}
		
		public function userHasPermisson($action) {
			if($action == CONTROLLER_ACTION_DELETE){
				$user = UserAccessor::getCurrentUser();
				return $user->userHasAccess(UserLevel::WORKER);
			}
			else{
				return parent::userHasPermisson($action);
			}
		}
	
		function handleAction($action,$target,$identifiers){
                        $view = parent::createJsonView();
                        $model = parent::createJsonModel();
			if($action == CONTROLLER_ACTION_NEW){
				$slot =SignupSlot::loadById(fromPost("signup_slot_id"));
				$identifiers = fromPost("entityIDs");
				$type = fromPost("signup_type"); 
				$signups = SignupController::createSignups($identifiers,$type);
				$data = array("signups" => static::toJsonObject($signups));
				if(count($signups) > 0){
					$signup_slot = SignupSlot::loadById($signups[0]->signup_slot_id);
					$data["signup_slot"] =  $slot->getStandardClone();
				}
				$model->setMessage(Translator::translate("signup/saveSuccessfullMessage"));
				$model->setData($data);
			}else if($action == CONTROLLER_ACTION_DELETE){
				Signup::deleteById($identifiers["default"]);
				if($this->isJsonRequest()){
					$model->setMessage(Translator::translate("signup/deleteSuccessfullMessage"));
				}else{
					$redirectUrl = fromGet("returnPath");
					return new RedirectView($redirectUrl);
				}
			}else if($action == CONTROLLER_ACTION_SAVE){
 				$signup = Signup::loadById($identifiers["default"]);
				$is_paid = fromPost("is_paid");
				if($is_paid === null){
					$signup->is_paid = 0;
				}else if($is_paid == 1){
					$signup->is_paid = 1;
				}
				$is_approved = fromPost("is_approved");
				if($is_approved === null){
					$signup->is_approved = 0;
				}else if($is_approved == 1){
					$signup->is_approved = 1;
				}
				$signup->save();
				if($this->isJsonRequest()){
					$model->setData($this->toJsonObject($signup));
				}else{
					$redirectUrl = fromGet("returnPath");
					return new RedirectView($redirectUrl);
				}
			}
			$view->setModel($model);
			return $view;
		}
		
		static function createSignup($identifier,$type){
			$signups = CreateSignups(array($identifier));
			return $signups[0];
		}
		
		static function createSignups($identifiers,$type){
			$signups = array();
			foreach($identifiers as $entityID){
				$signup = new Signup();
				Controller::populateFromPost($signup);
				if($type == "people"){
					$signup->person_id = $entityID;
				}else{
					$signup->group_id = $entityID;
				}
				$signup->save();
				$signups[] = Signup::loadById($signup->id);
			}
			return $signups;
		}
	
		static function populateFromPost(&$entity,$prefix=""){
			parent::populateFromPost($entity,$prefix);
			$entity->is_approved = fromPost("is_approved",0);
			$entity->is_paid = fromPost("is_paid",0);
		}
	
	}