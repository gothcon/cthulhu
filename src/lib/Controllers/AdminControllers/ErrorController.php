<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class ErrorController extends AdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Views/PublicViews/Master/PublicMaster.php");
			require_once("lib/Controllers/AdminControllers/NewsController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		public function HandleAction($action, $target, $identifiers) {
			
			if($target=="error"){
				switch($identifiers["error"]){
					case "404":
						$view = $this->create404View();
						$model = $this->create404Model();
						$view->setModel($model);
						return $view;
						break;
				}
			}
			return parent::HandleAction($action, $target, $identifiers);
		}
		
		public static function getControllerName(){
			return "index";
		}
		
		protected function handleListRequest($action, $target, $identifiers) {
			$view = new RedirectView($this->internalLink("error", "404"));
			return $view; 
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			$view = new RedirectView($this->internalLink("error", "404"));
			return $view;
		}
	}



