<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsController
 *
 * @author Joakim
 */
require_once("lib/Controllers/AdminControllers/AdminController.php");
class NewsController extends AdminController {
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Database/Entities/NewsArticle.php");
		parent::__construct($internalLinkBuilder);
	}
	
	protected function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/NewsArticle";
	}

	protected function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/News";
	}
	
	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
		
	}
	
	public function userHasPermisson($action){
		return UserAccessor::getCurrentUser()->userHasAccess(UserLevel::STAFF);
	}

	
	public function HandleAction($action, $target, $identifiers) {
		switch($action){
			case CONTROLLER_ACTION_DELETE:
				NewsArticle::deleteById($identifiers["default"]);
				return new RedirectView($this->internalLink("news"));
				break;
			default:
				return parent::HandleAction($action, $target, $identifiers);
			break;
		}
	}
	
	
	protected function handleFormRequest($action, $target, $identifiers) {
		$view = parent::handleFormRequest($action, $target, $identifiers);
		$model = $view->getModel();
		if(isset($identifiers["default"]) && $identifiers["default"] > 0){
			$newsArticle = NewsArticle::loadById($identifiers["default"]);
			$model->setTitle(Translator::translate("news/editArticle").": \"".$newsArticle->title."\"");
		}else{
			$newsArticle = new NewsArticle();
			$model->setTitle(Translator::translate("news/newArticle"));
		}
		if(POSTBACK){
			static::populateFromPost($newsArticle);
			$newsArticle->save();
			return new RedirectView($this->internalLink("news","",false));
		}
		$model->setNewsArticle($newsArticle);
		$view = new AdminMaster($view);
		return $view;
	}
	 
	protected function handleListRequest($action, $target, $identifiers) {
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setNewsArticles(NewsArticle::loadList());
		$model->setTitle(Translator::translate("news/articles"));
		$view = new AdminMaster($view);
		return $view;
	}
	
	protected function handleIndexRequest($action, $target, $identifiers) {
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$view = new AdminMaster($view);
		return $view;
	}
	
	public static function populateFromPost(&$entity, $prefix = "") {
		parent::populateFromPost($entity, $prefix);
		// custom since these are not included in the persisted properties
		$entity->publish_at_date = fromPost("publish_at_date");
		$entity->publish_at_time = fromPost("publish_at_time");
		$entity->unpublish_at_date = fromPost("unpublish_at_date");
		$entity->unpublish_at_time = fromPost("unpublish_at_time");
	}
	
}

?>
