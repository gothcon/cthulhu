<?php

require_once("lib/Controllers/AdminControllers/AdminController.php");

class SignupSlotArticleController extends AdminController{
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/ArticleController.php");
		parent::__construct($internalLinkBuilder);
	}
	
	protected function getViewDirectory(){
		die("Not implemented");
	}

	protected function getModelDirectory(){
		die("Not implemented");
	}
	
	public function handleAction($action, $target, $identifiers) {
		die("Not implemented");
	}
	
	public static function saveSignupSlotArticle($signupSlotArticle, $saveArticle = false){
		if($saveArticle){
			$article = $signupSlotArticle->article;
			$article->save();
			$signupSlotArticle->article = $article;
		}
		return $signupSlotArticle->save();
	}
	/**
	 * 
	 * @param SignupSlotArticle $signupSlotArticle
	 * @param bool $deleteArticle
	 * @return bool
	 */
	public static function deleteSignupSlotArticle($signupSlotArticle,$deleteArticle=true){
		$articleId = $signupSlotArticle->article_id; 
		$result = $signupSlotArticle->delete();
		if($deleteArticle)
			$result &= Article::deleteById($articleId);
		return $result;
	}
	
	
	
	
}

?>
