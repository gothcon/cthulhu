<?php 

require_once("lib/Controllers/AdminControllers/AdminController.php");
require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
require_once("lib/Models/SimpleJsonModel.php");

use Repository\GroupRepository;
use Repository\PersonRepository;

class PersonController extends AdminController{

    protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
        $noOfSegments = count($segments);

		switch($noOfSegments){
            case 0:
                $action = CONTROLLER_ACTION_LISTING;
                break;
			case 2:
				$identifiers['default'] = $segments[0];
				if($segments[1] == CONTROLLER_ACTION_AVAILABLE_GROUPS){
					$action = $segments[1];
					break;
				}
				if($segments[1] == CONTROLLER_ACTION_ADD_MEMBERSHIP){
					$action = $segments[1];
					break;
				}
				if($segments[1] == CONTROLLER_ACTION_GET_AVAILABLE_SLEEPING_SLOTS){
					$action = $segments[1];
					break;
				}				
				if($segments[1] == CONTROLLER_ACTION_SAVE_ACCOMMODATION){
					$action = $segments[1];
					break;
				}
				if($segments[1] == CONTROLLER_ACTION_ORDERS){
					$action = $segments[1];
					break;
				}
				if($segments[1] == CONTROLLER_ACTION_USER){
					$action = $segments[1];
					break;
				}
            default :
                $action = CONTROLLER_ACTION_FORM;
                $identifiers['default'] = $segments[0];
                if($noOfSegments >= 2){
                    $targetType = $segments[$noOfSegments-1];
                }
        }
        return true;
    }

    function getViewDirectory(){
            return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Person";
    }	

    function getModelDirectory(){
            return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Person";
    }

    public static function getControllerName(){
            return "person";
    }

    function HandleAction($action, $target, $identifiers) {
        if(JSONREQUEST){
            return $this->handleJsonRequest($action,$target,$identifiers);
        }else{
            return $this->handleHtmlRequest($action, $target, $identifiers);
        }
    }

    function MergeJsonData(&$entity,$jsonObj){

        $entityReflectionClass = new ReflectionClass(get_class($entity));

        foreach($jsonObj as $property => $value){
            $methodName = "set".$property;
            if($entityReflectionClass->hasMethod($methodName)){
                $entity->$methodName($value);
            }
        }
    }

    function handleSavePerson($action,$target,$identifiers){

        $personData = json_decode(file_get_contents("php://input"));

        if(isset($personData->id) && $personData->id > 0){
            unset($personData->user);
            unset($personData->modifiedAt);
            unset($personData->modifiedBy);
            unset($personData->createdAt);
            unset($personData->createdBy);
			unset($personData->sleepingSlotBooking);
            $person = PersonRepository::person($personData->id);
        }else{
            $person = PersonRepository::create();
        }
        if($person != null){
            $this->MergeJsonData($person, $personData);
            \Repository\PersonRepository::Save($person);
        }
        return $person;
    }

    function handleSaveUser($action,$target,$identifiers){
        $person = PersonRepository::person($identifiers["default"]);
        $user = $person->getUser();
        if($user == null){
            $user = new \Entities\User();
            $user->setPerson($person);
        }
        $userData = json_decode(file_get_contents("php://input"));
        $this->MergeJsonData($user, $userData);
        \Repository\UserRepository::Save($user);
        return $user;
    }

    function handleJsonRequest($action, $target, $identifiers) {
       
        $model = $this->createJsonModel();
        $view = $this->createJsonView();

        if($_SERVER["REQUEST_METHOD"] == "GET"){
            
			if($action == CONTROLLER_ACTION_ORDERS){
				$model = new SimpleJsonModel();
				$model->setData(\Repository\OrderRepository::getPersonalOrders($identifiers['default']));
			}
			else if($action == CONTROLLER_ACTION_AVAILABLE_GROUPS){
				$model->setData(GroupRepository::getAvailableGroups($identifiers["default"]));
			}
			else if($action == CONTROLLER_ACTION_GET_AVAILABLE_SLEEPING_SLOTS ){
				$model = new SimpleJsonModel();
				$model->setData(Repository\AccommodationResourceRepository::getPersonalSleepingResorces(Application::getConfigurationValue("sovplatsarrangemang"), $identifiers['default']));
			}
			else if($action == CONTROLLER_ACTION_LISTING){
				$view = $this->createJsonView();
				$model = new SimpleJsonModel();
				$model->setData(PersonRepository::all());
			}
            else{
				if(isset($identifiers["default"])  && $identifiers["default"] > 0){
					$view = $this->createDoctrineJsonView();
					$model = new SimpleJsonModel();
					$model->setData(PersonRepository::person($identifiers["default"]));
				}
                else{
                    $model->setData(PersonRepository::create());
                }
            }
        }
        else if($_SERVER["REQUEST_METHOD"] == "POST"){
			if($action == CONTROLLER_ACTION_ADD_MEMBERSHIP){
				$responseJsonObject = $this->getPostedJsonObject();
				$membership = GroupRepository::addMembership($identifiers['default'], $responseJsonObject->group_id);
				$model->setData($membership);
				
			}
			else if($action == CONTROLLER_ACTION_SAVE_ACCOMMODATION){
				$responseJsonObject =  $this->getPostedJsonObject();
				$accommodationResourceId = $responseJsonObject->accommodationResourceId;
				$personId = $identifiers['default'];
				$booking = Repository\AccommodationResourceRepository::savePersonalAccommodation($personId, $accommodationResourceId);

				if($accommodationResourceId >= 1){
					$model = new SimpleJsonModel();
					$model->setData($booking);
					$view = $this->createDoctrineJsonView();
				}
			}
			else if($action == CONTROLLER_ACTION_USER){
				$user = $this->handleSaveUser($action,$target,$identifiers);
				$view = $this->createDoctrineJsonView();
				$model = new SimpleJsonModel();
				$model->setData(PersonRepository::person($identifiers["default"]));
			}
            else{ 
				$person = $this->handleSavePerson($action,$target,$identifiers);
				$model = new SimpleJsonModel();
				$view = $this->createDoctrineJsonView();
				$model->setData($person);
			}
        }
		else if($_SERVER["REQUEST_METHOD"] == "DELETE"){
		 
			if(!UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
				header('HTTP/1.1 403 Forbidden');
				print 'Du har inte rättigheter att ta bort personer ';
				exit;
			}
			$person = GothConEntityManager::createPersonalReference($identifiers['default']);
			PersonRepository::delete($person);
		}
		else{
            return parent::handleJsonRequest($action, $target, $identifiers);
        }
        $view->setModel($model);
        return $view;
    }

    function handleHtmlRequest($action,$target,$identifiers){
        require_once("lib/Views/DoctrineJsonView.php");
		$view = $this->createFormView();
        $model = $this->createFormModel();
        $model->SetTitle("Personer");
		if(is_numeric($identifiers["default"]) && $identifiers["default"] > 0){
			$person = PersonRepository::person($identifiers["default"]);
			$jsonData = json_encode(DoctrineEntitySerializer::Serialize($person));
			$model->setPerson($jsonData);
		}else{
			$model->setPerson(null);
		}
        $view->setModel($model);
        $masterView = new AngularAdminMaster($view);
        $masterView->setModel($model);
        return $masterView;
    }

}

?>