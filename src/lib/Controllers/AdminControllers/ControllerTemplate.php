<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class ControllerTemplate extends AdminController{
	
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		die("no view directory specified");
	}	
	
	function getModelDirectory(){
		die("no model directory specified");
	}
	
	function handleAction($action,$target,$identifiers){
		if($this->isJsonRequest()){
			if($action == CONTROLLER_ACTION_DELETE){
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$success = true;
					if($success){
						$model->setStatus(JsonModel::STATUS_OK);
						$model->setMessage("OK");
						$model->setData(null);
					}
					else{
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage("Not OK");
						$model->setData(null);
					}
			}
			
			if($action == CONTROLLER_ACTION_NEW){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$success = true;
				if($success){
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
					$model->setData(null);
				}
				else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage("Not OK");
					$model->setData(null);
				}
			}   
			$view->setModel($model);
			return $view;
		}else{
			return parent::handleAction($action,$target,$identifiers);
		}
	}

	function handleIndexRequest($action,$target,$identifiers){
		die("index request not implemented");
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("View");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
		$view = parent::handleFormRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("Edit");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("List");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
}

?>