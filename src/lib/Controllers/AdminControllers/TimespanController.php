<?php
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class TimespanController extends AngularAdminController{
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Timespan/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Timespan";
		}
		
		public static function getControllerName(){
			return "timespan";
		}
		
		static function getMasterTimespan(){
			return Timespan::loadById(Application::GetConfigurationValue("konventspass_hela_konventet", true));
		}
		
		protected function handleJsonRequest($action, $target, $identifiers) {
			$view = parent::handleJsonRequest($action, $target, $identifiers);
			$model = $view->getModel();			
			$model->setData($this->toJsonObject(Timespan::loadPublic()));
			$view->setModel($model);
			return $view;
		}
		
		public function handleDelete($target, $identifiers, $returnAsJson = false) {
			Timespan::deleteById($identifiers["default"]);
			$view = new RedirectView($this->internalLink(static::getControllerName()));
			return $view;
		}
		
		public function handleGet($target, $identifiers, $returnAsJson = false) {
			if($returnAsJson){
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				if(isset($identifiers["default"])){
					$model->setData(\Repository\TimespanRepository::get($identifiers["default"]));
				}else{
					$model->setData(\Repository\TimespanRepository::getPublic());
				}
			}else{
				$view = $this->createIndexView();
				$model = $this->createIndexModel();
			}
			$view->setModel($model);
			return $view;
		}

		public function handlePost($target, $identifiers, $returnAsJson = false) {
			if($returnAsJson){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				if(isset($identifiers["default"]) && is_numeric($identifiers["default"]) && $identifiers["default"] > 0){
					$timespan = \Repository\TimespanRepository::getAsObject($identifiers["default"]);
				}else{
					$timespan = new \Entities\Timespan();
				}
				
				$postData = $this->GetPostData();
				$timespan->setName($postData->name);
				$timespan->setStartsAt(new DateTime($postData->startsAt));
				$timespan->setEndsAt(new DateTime($postData->endsAt));
				$timespan->setIsPublic(true);
				$timespan->setShowInSchedule($postData->showInSchedule);
				\Repository\TimespanRepository::save($timespan);
				$model->setData($timespan);
				$view->setModel($model);
				return $view;
			}else{
				header("HTTP/1.0 400 Bad request");
				echo "<h1>Method not allowed</h1>";
				exit();
			}
			
		}
	
		protected function handleIndexRequest($action, $target, $identifiers) {
			return static::handleFormRequest($action, $target, $identifiers);
		}
		
		function handleFormRequest($action,$target,$identifiers){
//			$view = parent::handleFormRequest($action,$target,$identifiers);
//			$model = $view->getModel();
//			
//			
//			if($this->isPostback()){
//				
//				
//				
//			}
//			$model->setTimespan($timespan);
//			$view->setModel($model);
//			return new AngularAdminMaster($view);
		}
	}