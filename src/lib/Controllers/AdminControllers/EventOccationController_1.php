<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class EventOccationController extends AdminController{
		
		
	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
		parent::__construct($internalLinkBuilder);
	}

	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	public static function getControllerName(){
		return "arrangemangspass";
	}
	
	function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/EventOccation";
	}

	function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/EventOccation";
	}
	function handleAction($action,$target,$identifiers){
		
		switch($action){
			case CONTROLLER_ACTION_INDEX:
				if(JSONREQUEST){
					$view = $this->handleJsonRequest ($action, $target, $identifiers);
				}
				break;
			case CONTROLLER_ACTION_SAVE:
			case CONTROLLER_ACTION_NEW:
				
				$view = $this->handleSaveEventOccation($action, $target, $identifiers);
				break;
			case CONTROLLER_ACTION_DELETE:
				$view = $this->handleDeleteEventOccation($action, $target, $identifiers);
				break;
			case CONTROLLER_ACTION_SAVE:
				$view = $this->handleUpdateEventOccation($action, $target, $identifiers);
				break;
			case CONTROLLER_ACTION_LISTING:
				if(JSONREQUEST){
					$model = $this->createJsonModel();
					$model->setData(Booking::getBookingTreeForResource(fromGet("resourceId",null)));
					$view = $this->createJsonView();
					$view->setModel($model);
					break;
				}else{
					$view = $this->handleListRequest($action, $target, $identifiers);
				}
			default:
				$view = parent::handleAction($action, $target, $identifiers);
		}
		return $view;
	}
	
	protected function handleIndexRequest($action,$target,$identifiers){
		die("index request not implemented");
	}
	
	protected function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
	}
	
	protected function handleListRequest($action,$target,$identifiers){		
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->SetMasterTimespan(TimespanController::getMasterTimespan());
		$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
		$model->SetResourceSchedules(Event::loadTree());
		$model->SetTitle(Translator::translate("resources/overview"));
		$view->setModel($model);
		return new AdminMaster($view);
	}

	static function populateFromPost(&$entity, $prefix = "") {
		parent::populateFromPost($entity, "event_occation_");
		$timespan_id = $entity->timespan_id;
		// this is a custom timespan
		$timespan = new Timespan();
		parent::populateFromPost($timespan, "timespan_");
		$timespan->starts_at_date = fromRequest("timespan_starts_at_date");
		$timespan->starts_at_time = fromRequest("timespan_starts_at_time");
		$timespan->ends_at_date = fromRequest("timespan_ends_at_date");
		$timespan->ends_at_time = fromRequest("timespan_ends_at_time");
		$timespan->id = $timespan_id;
		$entity->setTimespan($timespan);
		return $entity;
	}

	protected function handleSaveEventOccation($action, $target, $identifiers) {
		if(fromPost("event_occation_id")){
			$eventOccation = EventOccation::loadById(fromPost("event_occation_id"));
		}else{
			$eventOccation = new EventOccation();
		}
		static::populateFromPost($eventOccation);
		// get data from post
		
		// save the occation if it is valid
		$actionWasSuccessful = $eventOccation->validate() && $eventOccation->save();
		if($actionWasSuccessful){
			$eventOccation = EventOccation::loadById($eventOccation->id);
			// get Slot Type data
			$types = fromPost("slot_type",array());
			
			$createdSlots = SignupSlot::loadAllFilteredByOccation($eventOccation);
			
			foreach($types as $type_id){
				if(!$type_id || $type_id == "null")
					continue;
				
				$is_hidden					= fromPost(array("is_hidden",$type_id),-1);
				$requires_approval			= fromPost(array("requires_approval",$type_id),null);
				$maximum_signup_count		= fromPost(array("maximum_signup_count",$type_id),null);
				$maximum_spare_signup_count = fromPost(array("maximum_spare_signup_count",$type_id),null);
				$price						= fromPost(array("price",$type_id));

				$signupSlot = new SignupSlot();
				foreach($createdSlots as $key => $oldSignupSlot){
					if($oldSignupSlot->slot_type_id == $type_id){
						$signupSlot = $oldSignupSlot;
						unset($createdSlots[$key]);
					}
				}
				
				$article = $signupSlot->getArticle();
				$article->price							= (is_numeric($price)) ? $price : 0;
				$signupSlot->setArticle($article);	
				$signupSlot->maximum_signup_count		= (is_numeric($maximum_signup_count)) ? $maximum_signup_count : null;
				$signupSlot->maximum_spare_signup_count = (is_numeric($maximum_spare_signup_count)) ? $maximum_spare_signup_count : null;
				$signupSlot->requires_approval			= ($requires_approval == "null") ? 0 : $requires_approval;
				$signupSlot->is_hidden					= ($is_hidden == "null") ? 0 : $is_hidden;
				$signupSlot->event_occation_id			= $eventOccation->id;
				$signupSlot->slot_type_id				= $type_id;
				$signupSlot->save();
				
			}
			// remove all the slots not used
			foreach($createdSlots as $slot){
				SignupSlot::deleteById($slot->id);
			}
			
		}

		if ($this->isJsonRequest()) {
			// this is a json request
			$model = $this->createJsonModel();
			$view = $this->createJsonView();
			if ($actionWasSuccessful) {
				$model->setData($eventOccation->getStandardClone());
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveSucceeded"));
			} 
			else {
				$model->setData($eventOccation->getStandardClone());
				$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
				$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveFailed"));
			}
			$view->setModel($model);
			return $view;
		} else {
			// this is a normal request
			if ($actionWasSuccessful) {
				// the action was successful
				// redirect to index view
				$view = new RedirectView(fromGet("returnPath"));
			} else {
				// invalid occation, show the form
				$view = $this->handleIndexRequest($action, $target, $identifiers);
				$model = $view->getModel();
				$this->model->setCurrentOccation($eventOccation);
				$this->model->setCurrentTimespan($eventOccation->getTimespan());
			}
		}
		return $view;
	}
	
	protected function handleUpdateEventOccation($action, $target, $identifiers) {
		
		if(!$identifiers['default'] || !is_numeric($identifiers['default']))
			die("invalid identifier");
			
		$eventOccation = EventOccation::loadById($identifiers['default']);

		if(fromRequest("event_occation_main_resource_booking_id",null)){
			$eventOccation->main_resource_booking_id = fromRequest("event_occation_main_resource_booking_id");
			$eventOccation->save();
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setData($this->toJsonObject($eventOccation));
				$model->setMessage(Translator::translate("event/jsonMessages/mainResourceUpdateSucceeded"));
				$view->setModel($model);
				return $view;
			}else{
				return new RedirectView(fromRequest("returnUrl"));
			}
		}else{
			if ($eventOccation->update()) {
				if ($this->isJsonRequest()) {
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$jsonEventObject = static::toJsonObject($eventOccation);
					$jsonEventObject->timestamp = static::toJsonObject($eventOccation->getTimespan());

					$model->setData($jsonEventObject);
					$model->setStatus(0);
					$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveSucceeded"));
					$view->setModel($model);
				} else {
					// redirect to index view
					$view = new RedirectView($this->internalLink(static::getControllerName(),$identifiers["default"] . "/#Pass och anmalningar"));
				}
			} else {
				// invalid occation, show the form
				$view = $this->handleIndexRequest($action, $target, $identifiers);
				$model = $view->getModel();
				$this->model->setCurrentOccation($eventOccation);
				$this->model->setCurrentTimespan($eventOccation->getTimespan());
			}
		}
		return $view;
	}

	protected function handleDeleteEventOccation($action, $target, $identifiers) {
		
		EventOccation::deleteById($identifiers["default"]);
		
		if ($this->isJsonRequest()) {
			$view = $this->createJsonView();
			$model = $this->createJsonModel();
			$model->setStatus(0);
			$model->setMessage(Translator::translate("eventOccation/jsonMessages/deletionSucceeded"));
			$view->setModel($model);
		} else {
			$view = new RedirectView($this->internalLink(static::getControllerName(), $identifiers["default"] . "/#Pass och anmalningar"));
		}
		return $view;
	}
	
	protected function handleJsonRequest($action, $target, $identifiers) {
		$view = parent::handleJsonRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$data = new stdClass();
		$model->setData(EventOccation::loadOccationAndSlots($identifiers["default"]));
		$model->setStatus(0);
		$model->setMessage("");
		$view->setModel($model);
		return $view;
	}
	
}

?>