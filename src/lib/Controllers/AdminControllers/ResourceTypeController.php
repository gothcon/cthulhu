<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class ResourceTypeController extends AdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/ResourceController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		public static function getControllerName(){
			return "resurstyp";
		}		
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/ResourceType/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/ResourceType";
		}
		
		function handleJsonRequest($action,$target,$identifiers){
			return $this->handleListRequest($action,$target,$identifiers);
		}
		
		public static function getSelectDataSource(){
			$allTypes = ResourceType::loadList();
			$ds = array();
			foreach($allTypes as $type){
				$ds[] = array("label" => $type->name, "value" => $type->id);
			}
			return $ds;
		}
		
		function handleListRequest($action,$target,$identifiers){
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
			}else{
				$view = parent::handleListRequest($action,$target,$identifiers);
				$model = $view->getModel();
			}
			$allTypes = ResourceType::loadResourceTypesWithStatus();
			if($this->isJsonRequest()){
				$allTypesInJsonFormat = static::toJsonObject($allTypes);
				$model->setData($allTypesInJsonFormat);
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage(Translator::translate("resourceType/loadMessage"));
				$view->setModel($model);
			}else{
				$model->setTitle(Translator::translate("resourceType/resourceTypes"));
				$model->setResourceTypelist($allTypes);
				$view->setModel($model);
				$view = new AdminMaster($view);
				$view->setModel($model);
			}
			return $view;
		}
	
		function PopulateType(&$type,$action,$identifiers){
			$this->populateFromPost($type);
			if($action == CONTROLLER_ACTION_SAVE && isset($identifiers["default"]) && is_numeric($identifiers["default"])){
				$type->id = $identifiers["default"];
			}
		}

		function handleFormRequest($action,$target,$identifiers){
			
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$type = new ResourceType();
			}else{
				$view = parent::handleFormRequest($action,$target,$identifiers);
				$model = $view->getModel();
				if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
					$type = ResourceType::loadById($identifiers["default"]);
					$model->setTitle(Translator::translate("resourceType/resourceType")." {$type->name}");
					$model->setResourceList(Resource::loadByType($type->id));
					// $model->setSleepingStatistics(ResourceController::getSleepingStatistics());
				}else{
					$type = new ResourceType();
					$model->setTitle(Translator::translate("resourceType/new"));
				}
			}

			
			
			if($this->isPostback()){
				$this->populateFromPost($type);

				if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
					$type->id = $identifiers["default"];
				}
				if($type->save()){
					if($this->isJsonRequest()){
						$model->setStatus(JsonModel::STATUS_OK);
						$view->setModel($model);
						$model->setData($type->getStandardClone());
						$model->setMessage(Translator::translate("resourceType/updateMessage"));
					}else{
						return new RedirectView($this->controllerPath());
					}
				}else{
					if($this->isJsonRequest()){
						$model->setStatus(JsonModel::STATUS_INVALID_DATA);
					}
				}
			}
			
			$view->setModel($model);
			
			if(!$this->isJsonRequest()){
				$model->setResourceType($type);
				$view = new AdminMaster($view);
				$view->setModel($model);
			}
			return $view;
		}
		
		protected function handleIndexRequest($action, $target, $identifiers) {
			return $this->handleFormRequest($action, $target, $identifiers);
		}
		
		function handleAction($action,$target,$identifiers){
			if($action == CONTROLLER_ACTION_NEW){
				return $this->handleFormRequest($action,$target,$identifiers);
			}
			if($action == CONTROLLER_ACTION_SAVE){
				 
				return $this->handleFormRequest($action,$target,$identifiers);
			}
			
			return parent::handleAction($action,$target,$identifiers);
		}
	
	}



