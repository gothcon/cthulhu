<?php
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class ResourceBookingController extends AngularAdminController{
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/ResourceBooking/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/ResourceBooking";
		}
		
		public static function getControllerName(){
			return "resourceBooking";
		}
		
		public function HandleServiceRequest($serviceName) {
			if($serviceName == "availableEventOccations"){
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$resourceId = fromGet("resourceId",false);
				if(!$resourceId){
					header("HTTP/1.0 400 Bad request, the query param \$resourceId was missing.");
					exit();
				}else{
					$data = \Repository\EventOccationRepository::getBookingTreeForResource($resourceId);
					$model->setData($data);
				}
				$view->setModel($model);
				return $view;
			}else{
				return parent::HandleServiceRequest($serviceName);
			}
		}

	}