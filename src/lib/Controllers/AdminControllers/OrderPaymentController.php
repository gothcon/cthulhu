<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	
	
	class OrderPaymentController extends AdminController{
	
		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/OrderController.php");
			require_once("lib/Controllers/AdminControllers/OrderPaymentController.php");
			require_once("lib/Controllers/AdminControllers/TransactionController.php");
			parent::__construct($internalLinkBuilder);
		}	
		
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/OrderPayment/";
	}	
	
	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/OrderPayment/";
	}

	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) == 1 && $segments[0] == CONTROLLER_ACTION_REGISTER){
			$action = $segments[0];
			$targetType ="default";
			$identifiers = array();
			return true;
		}else if(count($segments) == 2 && $segments[1] == CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT ){
			
			$action = $segments[1];
			$targetType ="default";
			$identifiers = array("default" => $segments[0]);
			return true;
		}
		else{
			return parent::parseUrl($segments, $action, $targetType, $identifiers);
		}
	}
	
	function handleAction($action,$target,$identifiers){

		switch($action){
			case CONTROLLER_ACTION_REGISTER:
				return $this->handleRegisterRequest($action,$target,$identifiers);
				break;
			case CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT:
				return $this->handleCancelRequest($action,$target,$identifiers);
				break;
			default:
				return parent::handleAction($action,$target,$identifiers);
		}
		
	}
	
	function createOrderPaymentView(){
		require_once($this->getViewDirectory()."OrderPayment.php");
		$view = new OrderPaymentView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}

	function createOrderPaymentModel(){
		require_once($this->getModelDirectory()."OrderPaymentModel.php");
		return new OrderPaymentModel();
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$view = $this->createOrderPaymentView();
		$model = $this->createOrderPaymentModel();
		$view->setModel($model);
		return $view;
	}
	
	public function handleCancelRequest($action,$target,$identifiers) {
		
		$orderPayment = OrderPayment::loadById($identifiers["default"]);
		$transaction = $orderPayment->transaction;
		$transaction->cancelled = true;
		$orderPayment->transaction = $transaction;
		$saveWasSuccessful = $orderPayment->save();
		
		if($this->isJsonRequest()){
			$view = $this->createJsonView();
			$model = $this->createJsonModel();
			$model->setData($orderPayment->getStandardClone());
		
			if($saveWasSuccessful){
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage("Orderinbetalningen har makulerats");
			}else{
				$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
				$model->setMessage(MessageAccessor::getErrorMessages());
				MessageAccessor::clearAllMessages();
			}
			$view->setModel($model);
			return $view;
		}
		else{
			if($saveWasSuccessful){
				MessageAccessor::addInformationMessage("Orderinbetalningen har makulerats");
			}else{
				MessageAccessor::addErrorMessage("Kunde inte makulera orderinbetalningen");
			}
			return new RedirectView($this->internalLink("orderPayment",CONTROLLER_ACTION_REGISTER));
		}
	}
	
	/**
	 * @param OrderPayment $orderPayment
	 */
	function notify($orderPayment){
		
		$order = $orderPayment->order;
		$transactionAmount = $orderPayment->transaction->amount;
		$paidSoFar	= OrderPayment::getTotalOrderPaymentSum($order->id);
		
		$recipient = $order->getPerson()->email_address;

		$subject= Application::getConfigurationValue("betalningsbekraftelse_amne");
		$sender	= Application::getConfigurationValue("betalningsbekraftelse_fran");
		$body	= array();

		$orderReference = str_replace("<ORDER_ID>",$order->id,Application::getConfigurationValue("order_reference_template"));
		
		$translatorFrom = array(
			"<ORDER_REFERENCE>",
			"<ORDER_TOTAL>",
			"<PAYMENT_AMOUNT>", 
			"<ALL_PAYMENTS_TOTAL>",
			"<MISSING_AMOUNT>",
			"<OVERFLOW_AMOUNT>"	
		);
		
		$translatorTo = array(
			$orderReference,
			number_format($order->total,2),
			number_format($transactionAmount,2),
			number_format($paidSoFar,2),
			number_format($order->total - $paidSoFar,2),
			number_format(($paidSoFar - $transactionAmount) > $order->total ? $transactionAmount : $paidSoFar - $order->total)
		);
		
		
		// paid to little

		$body[]	= str_replace(array(),array($orderReference,$order->total,$transactionAmount,$paidSoFar),Application::getConfigurationValue("betalningsbekraftelse_meddelande"));
		
		
		if($paidSoFar < $order->total){
			$body[] = Application::getConfigurationValue("betalt_for_lite");
		}
		else if($paidSoFar > $order->total){
			$body[] = Application::getConfigurationValue("betalt_for_mycket");
		}else{
			$body[] = Application::getConfigurationValue("betalt_lagom");
		}
		
		$body[] = Application::getConfigurationValue("betalningsbekraftelse_meddelandefot"); 
		$message = str_replace($translatorFrom,$translatorTo,implode($body,"\n\r"));
		return Application::sendMail($recipient, $sender, $subject, $message);
		
	}
	
	function saveOrderPayment(&$orderPayment){

		$order = $orderPayment->order;
		$oldOrderStatus = $order->status;
		
		Application::log("Saving order payment...");
		if($orderPayment->save()){
			// order payment save updates the order status unless the order has been canceled, then it returns false anyway
			$newOrderStatus = $orderPayment->order->status;

			
			$orderWasPaid = ($oldOrderStatus == Order::STATUS_PAID);
			$orderIsPaid = ($newOrderStatus == Order::STATUS_PAID);
			
			// if payment status has changed
			if($orderWasPaid != $orderIsPaid){
				
				// load all signup slot articles used to associate the signups to the article of the order row
				$slotArticlesBySlot = array();
				$signupSlotArticles = SignupSlotArticle::loadList();
				foreach($signupSlotArticles as $slotArticle){
					$slotArticlesBySlot[$slotArticle->signup_slot_id] = $slotArticle;
				}

				$signups = Signup::loadAllFilteredByPerson($order->person_id);
				foreach($signups as $key => $signup){
					Application::log("signup #".$signup->id);
					if(!$signup->is_signup_owner){
						continue;
					}
					if(isset($slotArticlesBySlot[$signup->signup_slot_id])){
						$signupSlotArticle = $slotArticlesBySlot[$signup->signup_slot_id];
						$orderRow = $order->getOrderRow($signupSlotArticle->article_id);
						if($orderRow){
							$signup = Signup::loadById($signup->id);
							$signup->is_paid = $orderIsPaid;
							$signup->save();
							$orderRow->status = ($orderIsPaid ? OrderRow::STATUS_DELIVERED : OrderRow::STATUS_NOT_DELIVERED);
							$orderRow->save();
							
							$order->setOrderRow($orderRow);
						}
					}
				}

			}
			return true;
		}
		Application::log($orderPayment);
	}
	
	function saveTransaction($orderId,$amount,&$orderPayment,$sendNotification = true){
		
		// fetch the order
		$order = Order::loadById($orderId);
		if($order && $order->id){
			
			$transaction = new Transaction();
			$transaction->amount = fromPost("amount");
			$transaction->payment_method = fromPost("payment_method","wire_transfer");
			$transaction->note = "Betalning för order # {$orderId}";

			$orderPayment->transaction = $transaction;
			$orderPayment->order = $order;
			
			if($orderPayment->save()){
				if($sendNotification){
					$this->notify($orderPayment);
				}
                                if($orderPayment->order->status != Order::STATUS_PAID){
                                    // TODO: ADD TO LANGUAGE STRINGS
                                    MessageAccessor::addWarningMessage("Something is fishy. The order might not be fully paid");
                                }
				return true;
			}
			else{
				static::addValidationErrorsToMessageHandler($orderPayment, "orderPayment/validationErrors");
			}
			$orderPayment->order = $order;
			$orderPayment->transaction = $transaction;
		}else{
                    // TODO: ADD TO LANGUAGE STRINGS
                    MessageAccessor::addErrorMessage("Kunde inte hitta order");
		}
		

		return false;
	}
	
	function handleRegisterRequest($action,$target,$identifiers){
		if(ISPOSTBACK){
			$orderPayment = new OrderPayment;
			$saveWasSuccessful = $this->saveTransaction(fromPost("reference"), fromPost("amount"), $orderPayment);
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$model->setData($orderPayment->getStandardClone());
				
				if($saveWasSuccessful){
                                        $warnings = MessageAccessor::getWarningMessages();
                                        $model->setStatus(JsonModel::STATUS_OK);
                                        if(count($warnings) == 0){
                                            // TODO: ADD TO LANGUAGE STRINGS
                                            $model->setMessage("Orderbetalningen har sparats");
                                        }else{
                                            $model->setMessage($warnings);
                                        }
				}else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage(MessageAccessor::getErrorMessages());
					
				}
                                MessageAccessor::clearAllMessages();
				$view->setModel($model);
				return $view;
			}
			else if($saveWasSuccessful){
				return new RedirectView($this->internalLink("orderPayment",CONTROLLER_ACTION_REGISTER));
			}
		}
		
		$view = $this->createOrderPaymentView();
		$model = $this->createOrderPaymentModel();
		$model->setMostRecentTransactions(OrderPayment::getRecent(10));
		$view->setModel($model);
		$model->setTitle(Translator::translate("orderPayment/registerOrderPayment"));
		$view = new AdminMaster($view);
		return $view;

	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
	}
	
	const ACTION_REGISTER = "registrera";
	
}?>