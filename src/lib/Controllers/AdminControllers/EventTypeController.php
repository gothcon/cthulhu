<?php
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class EventTypeController extends AngularAdminController{
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/EventType/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/EventType";
		}
		
		public static function getControllerName(){
			return "eventType";
		}		
		
		public function handleGet($target, $identifiers, $returnAsJson = false) {
			if($returnAsJson){
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$model->setData(\Repository\EventTypeRepository::getAll());
			}
			else{
				$view = $this->createListView();
				$model = $this->createListModel();
			}
			$view->setModel($model);
			return $view;
		}
		
		public function handlePost($target, $identifiers, $returnAsJson = false) {
			$view = $this->createDoctrineJsonView();
			$model = $this->createJsonModel();
			if(isset($identifiers[$target])){
				$eventtype = Repository\EventTypeRepository::get($identifiers[$target]);
			}else{
				$eventtype = new Entities\EventType();
			}
			$eventtypeData = $this->GetPostData();
			$eventtype->setName($eventtypeData->name);
			Repository\EventTypeRepository::save($eventtype);
			$model->setData($eventtype);
			$view->setModel($model);
			return $view;
		}
		
		public function handleDelete($target, $identifiers, $returnAsJson = false) {
			$view = $this->createDoctrineJsonView();
			$model = $this->createJsonModel();
			$eventtype = Repository\EventTypeRepository::get($identifiers[$target]);
			$model->setData(Repository\EventTypeRepository::delete($eventtype));
			$view->setModel($model);
			return $view;
		}
		
//		
//		
//		
//		function HandleAction($action,$target,$identifiers){
//			if($action == CONTROLLER_ACTION_DELETE){
//				
//				EventType::deleteById($identifiers["default"]);
//				if($this->isJsonRequest()){
//					$view = $this->createJsonView();
//					$model = $this->createJsonModel();
//					$model->setStatus(JsonModel::STATUS_OK);
//					$view->setModel($model);
//				}else{
//					$view = new RedirectView($this->controllerPath());
//				}
//
//				return $view;
//			}
//			else if(($action== CONTROLLER_ACTION_SAVE || $action == CONTROLLER_ACTION_NEW)&& $this->isJsonRequest() && $this->isPostBack()){
//				$view = $this->createJsonView();
//				$model = $this->createJsonModel();
//				$view->setModel($model);
//				$type = new EventType();
//				$this->populateFromPost($type);
//				if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
//					$type->id = $identifiers["default"];
//				}
//				if($type->validate()){
//					$type->save();
//					$model->setStatus(JsonModel::STATUS_OK);
//					
//					$object = static::toJsonObject($type);
//					$model->setData($object);
//					
//					$model->setMessage(Translator::translate("eventType/".($action == CONTROLLER_ACTION_NEW ? "createMessage" : "updateMessage")));
//				}else{
//					$model->setStatus(JsonModel::STATUS_INVALID_DATA);
//				}
//				return $view;
//			}
//			else{
//			
//				return parent::HandleAction($action,$target,$identifiers);
//			}
//		}
//		
		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$model->setTitle(Translator::translate("eventType/eventTypes"));
			$model->setEventTypeList();
			$view->setModel($model);
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			$view = parent::handleIndexRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$type = EventType::loadById($identifiers["default"]);
			$model->setEventType($type);
			$view->setModel($model);
			$view = new AdminMaster($view);
			$view->setModel($model);
			$model->setTitle(Translator::translate("eventType/eventType")." {$type->name}");
			return $view;
		}
	
		function handleFormRequest($action,$target,$identifiers){
			$view = parent::handleFormRequest($action,$target,$identifiers);
			$model = $view->getModel();
			if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
				$type = EventType::loadById($identifiers["default"]);
				$model->setTitle(Translator::translate("eventType/eventType").": {$type->name} - ".Translator::translate("general/editMode"));
			}else{
				$type = new EventType();
				$model->setTitle(Translator::translate("eventType/new"));
			}
			if($this->isPostback()){
				$this->populateFromPost($type);
				if($type->validate())
					$type->save();
					$view = new RedirectView($this->controllerPath());
					return $view;
			}
			$model->setEventType($type);
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
	}