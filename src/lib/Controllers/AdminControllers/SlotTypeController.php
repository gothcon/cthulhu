<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class SlotTypeController extends AdminController{
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/SlotType/";
		}
		
		public static function getControllerName(){
			return "slotType";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/SlotType";
		}
		
		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();

			
			
			$slotTypes = \Repository\SlotTypeRepository::all();
			
			$data = array();
			
			foreach($slotTypes as $slotType){
				$slotTypeDO = new stdClass();
				$slotTypeDO->name = $slotType['name'];
				$slotTypeDO->id = $slotType['id'];
				$slotTypeDO->signupCount = 0;
				
				$uniqueSignups = array();
				
				foreach($slotType['signupSlots'] as $slot){
					foreach($slot['signups'] as $signup){
						if($slotType['cardinality'] == 1 && $signup['person']){
							$slotTypeDO->signupCount++;
							$uniqueSignups[$signup['person']['id']] = $signup['person']['id'];
						}
						if($slotType['cardinality'] > 1 && $signup['group']){
							$slotTypeDO->signupCount++;
							$uniqueSignups[$signup['group']['id']] = $signup['group']['id'];
						}
					}
				}
				$slotTypeDO->uniqueSignupCount = count($uniqueSignups);
				array_push($data, $slotTypeDO);
			}

			
			$model->setSlotTypeList($data);
			$view->setModel($model);
			$view = new AdminMaster($view);
			$view->setModel($model);
			$model->setTitle(Translator::translate("slotType/slotTypes"));
			return $view;
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			$view = parent::handleIndexRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$type = SlotType::loadById($identifiers["default"]);
			$model->setSlotType($type);
			$view->setModel($model);
			$view = new AdminMaster($view);
			$view->setModel($model);
			$model->setTitle(Translator::translate("slotType/slotType").": {$type->name}");
			return $view;
		}
	
		function handleFormRequest($action,$target,$identifiers){
			
			$view = parent::handleFormRequest($action,$target,$identifiers);
			$model = $view->getModel();
			
			if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
				$type = SlotType::loadById($identifiers["default"]);
				$model->setTitle(sprintf(Translator::translate("slotType/formPageTitle"),$type->name));
			}else{
				$type = new SlotType();
				$model->setTitle(Translator::translate("slotType/new"));
			}
			if($this->isPostback()){
				
				$this->populateFromPost($type);
				if($type->validate()){
					$type->save();
					$view = new RedirectView($this->controllerPath());
					return $view;
				}
				
			}
			$model->setSlotType($type);
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
	}