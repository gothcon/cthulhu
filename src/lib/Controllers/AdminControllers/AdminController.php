<?php

require_once("lib/Controllers/Controller.php");
class AdminController extends Controller{
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Views/AdminViews/Master/AdminMaster.php");
		parent::__construct($internalLinkBuilder);
	}
	
	public function PreHandleAction() {
		return true;
		$httpReferer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";
		$siteRoot = Application::GetConfigurationValue("SiteRoot");
		
		if($httpReferer != "" && substr($httpReferer, 0,strlen($siteRoot)) == $siteRoot){
			return true;
		}else{
			return false;
		}
	}
	
	/** @return bool **/
	public function userHasPermisson($action){
            $currentUser = UserAccessor::getCurrentUser();
            switch($action){
                case CONTROLLER_ACTION_DELETE:
                case CONTROLLER_ACTION_DELETE_BOOKING:
                case CONTROLLER_ACTION_DELETE_EVENT_OCCATION:
                case CONTROLLER_ACTION_DELETE_GROUP:
                case CONTROLLER_ACTION_DELETE_RESOURCE_BOOKING:
                case CONTROLLER_ACTION_DELETE_SIGNUP_SLOT:
                        return $currentUser->userHasAccess(UserLevel::STAFF);
                        break;
                case CONTROLLER_ACTION_DELETE_GROUPSIGNUP:
                case CONTROLLER_ACTION_DELETE_MEMBERSHIP:
                case CONTROLLER_ACTION_DELETE_SIGNUP:
                        return $currentUser->userHasAccess(UserLevel::WORKER);
                        break;
                default :
                        return true;
                        break;
            }
	}
	
	public function create400View() {
		return new AdminMaster(parent::create400View());
	}
	public function create401View() {
		return new AdminMaster(parent::create401View());
	}
	public function create404View() {
		return new AdminMaster(parent::create404View());
	}
}

?>