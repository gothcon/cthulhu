<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	
	class TransactionController extends AdminController{
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Transaction";
	}	
	
	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Transaction";
	}

	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) == 2 && $segments[1] == CONTROLLER_ACTION_CANCEL_TRANSACTION){
			$identifiers["default"] = $segments[0];
			$action = CONTROLLER_ACTION_CANCEL_TRANSACTION;
			return true;
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	
	function handleAction($action,$target,$identifiers){
		if($this->isJsonRequest()){
			if($action == CONTROLLER_ACTION_CANCEL_TRANSACTION){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$success = true;
				if($success){
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
					$model->setData(null);
				}
				else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage("Not OK");
					$model->setData(null);
				}
			}
			
			if($action == CONTROLLER_ACTION_NEW){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$success = true;
				if($success){
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
					$model->setData(null);
				}
				else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage("Not OK");
					$model->setData(null);
				}
			}   
			$view->setModel($model);
			return $view;
		}else{
			if($action == CONTROLLER_ACTION_CANCEL_TRANSACTION){
				$transaction = Transaction::loadById($identifiers["default"]);
				if(!$transaction)
					die("No transaction found");
				$transaction->cancelled = 1;
				$transaction->save();
				return new RedirectView($this->internalLink("transaction",$transaction->id));
			}
			return parent::handleAction($action,$target,$identifiers);
		}
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$transaction = Transaction::loadById($identifiers["default"]);

		$model->setTitle("Transaktion #".( $transaction->id . ($transaction->cancelled ? " <span class='cancelledTransaction'>Makulerad!</span>": "")));
		$model->setTransaction($transaction);
		
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
		$view = parent::handleFormRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("Edit");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
	function handleListRequest($action,$target,$identifiers){
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle(Translator::translate("transaction/transactionList"));
		
		$from = fromGet("from",null) === "" ? null : fromGet("from",null);
		$to = fromGet("to",null) === "" ? null : fromGet("to",null);
		$paymentMethod = fromGet("payment_method","");
		$model->setPaymentMethods(array(
			array("value" => "" , "label" => Translator::translate("transaction/allPaymentMethods")),
			array("value" => "cc" , "label" => Translator::translate("transaction/paymentMethods/cc")),
			array("value" => "cash" , "label" => Translator::translate("transaction/paymentMethods/cash")),
			array("value" => "wire_transfer" , "label" => Translator::translate("transaction/paymentMethods/wire_transfer"))
		));
		$model->setList(Transaction::loadFilteredTransactions($paymentMethod,$from,$to));

		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
}

?>