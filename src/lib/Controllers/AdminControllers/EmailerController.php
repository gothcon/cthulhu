<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class EmailerController extends AdminController{
		
		public function __construct($internalLinkBuilder) {
			parent::__construct($internalLinkBuilder);
		}

		function getDefaultAction(){
			return CONTROLLER_ACTION_INDEX;
		}

		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Emailer";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Emailer";
		}

		public function userHasPermisson($action) {
			return UserAccessor::getCurrentUser()->userHasAccess(UserLevel::STAFF);
		}
		
		function handleAction($action,$target,$identifiers){
			if($this->isJsonRequest()){
				if($action == "delete"){
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
				}

				if($action == "new"){
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
					$model->setData(null);
				}   
				$view->setModel($model);
				return $view;
			}else{
				return parent::handleAction($action,$target,$identifiers);
			}
		}

		function handleIndexRequest($action,$target,$identifiers){
			
			$entranceFeeCategoryId = Application::getConfigurationValue("artikelkategori_intrade");
			
			$view = parent::handleIndexRequest($action, $target, $identifiers);

			
			$model = $view->getModel(); 
			$model->setEventList(static::createEventDropdownData(Event::loadList("name","asc"),false));
			$model->setSlotTypeList(static::createSlotTypeDropdownData(SlotType::loadList(),false));
			$model->setArticleList(static::createArticleDropdownData(Article::loadChildrenByParent($entranceFeeCategoryId),false));
			
			
			$emailAddresses = array();
			if(ISPOSTBACK){
				
				if(fromPost("add")){
				
					$selection = new EmailSelection();
					$mainSelection = fromPost("mainSelection");

					switch($mainSelection){
						case "all":
							$emailAddresses = Email::getAllEmailAddresses();
							$selection->basedOn = EmailSelectionType::ALL;
							break;
						case "organizers":
							$eventId = fromPost("organizer_filter_event_id",null);
							$emailAddresses = Email::getOrganizerEmailAddresses();
							$selection->basedOn = EmailSelectionType::ORGANIZERS;
							break;
						case "attenders":
							$eventId		= fromPost("signup_filter_event"	)	? fromPost("signup_filter_event_id"		,null)	: null;
							$slotTypeId		= fromPost("signup_filter_slot_type")	? fromPost("signup_filter_slot_type_id"	,null)	: null;
							$isValid		= fromPost("signup_filter_status"	)	? fromPost("signup_filter_valid"		,null)	: null;
							$isPaid			= fromPost("signup_filter_payment"	)	? fromPost("signup_filter_paid"			,null)	: null;
							$emailAddresses = Email::getAttenderEmailAddresses($eventId,$slotTypeId,$isPaid,$isValid);
							$selection->basedOn = EmailSelectionType::ATTENDERS;
							break;
						case "manual":
							$emailAddresses  = explode(",", fromPost("manual_addresses",""));
							break;
						case "signup_slot":
							$emailAddresses = Email::getAttenderEmailAddresses($eventId,$eventOccationId);
							$selection->basedOn = EmailSelectionType::SIGNUP_TYPE;
							break;
						case "entrance_fee":
							$entranceArticleId	= fromPost("entrance_fee_filter")			? fromPost("entrance_fee_id" ,null)		: null;
							$isPaid				= fromPost("entrance_fee_payment_filter")	? fromPost("entrance_fee_paid" ,null)	: null;
							if(!is_null($isPaid)){
								if($isPaid){
									$paymentStates = array(Order::STATUS_PARTIALLY_PAID,Order::STATUS_PAID);
								}else{
									$paymentStates = array(Order::STATUS_CART,Order::STATUS_SUBMITTED);
								}
							}else{
								$paymentStates = array();
							}
							$emailAddresses		= Email::getCustomerEmailAdresses($entranceFeeCategoryId , $paymentStates, $entranceArticleId);
							break;
						case "unpaid_orders":
							$paymentStates = array(Order::STATUS_CART, Order::STATUS_SUBMITTED, Order::STATUS_PARTIALLY_PAID);
							$emailAddresses		= Email::getCustomerEmailAdresses(null , $paymentStates, null);
							break;
					}

					$addedAdressesCount = 0;
					$storedAddresses = EmailSelectionAccessor::getSelectedEmailAddresses();
					
					
					
					
					foreach($emailAddresses as $address){
						$address = trim($address);
						if(!isset($storedAddresses[$address])){
							$addedAdressesCount++;
							
						}
						$storedAddresses[$address] = $address;
						
					}
					
					EmailSelectionAccessor::setSelectedEmailAddresses($storedAddresses);

					MessageAccessor::addInformationMessage(($addedAdressesCount > 0 ? ($addedAdressesCount == 1 ? "1 ny ":"{$addedAdressesCount} nya " ): "Ingen ny")." mottagare har lagts till");
				}else if(fromPost("clear")){
					EmailSelectionAccessor::clearSelections();
				}
				else {
					$subject = fromPost("subject",false);
					$message = fromPost("message",false);
					$recipients = EmailSelectionAccessor::getSelectedEmailAddresses();
					
					
					if(strlen($subject) == 0){
						MessageAccessor::addErrorMessage("Inget ämne angett");
					}
					
					if(strlen($message) == 0){
						MessageAccessor::addErrorMessage("Meddelandet är tomt");
					}
					
					if(count($recipients) == 0){
						MessageAccessor::addErrorMessage("Det finns inga mottagare");
					}
					if(strlen($subject) && strlen($message) && count($recipients)){
						if(Application::sendMail(EmailSelectionAccessor::getSelectedEmailAddresses(), Application::getConfigurationValue("email_from"), $subject, $message)){
							EmailSelectionAccessor::clearSelections();
							MessageAccessor::addInformationMessage("Nyhetsbrevet har skickats");
							return new RedirectView($this->internalLink("emailer"));
						}
						else{
							MessageAccessor::addErrorMessage("Kunde inte skicka mailet!");
						}
					}
				}
				
			}
			$view = parent::handleIndexRequest($action, $target, $identifiers); 
			$model->setAttenders(EmailSelectionAccessor::getSelectedEmailAddresses());
			$model->setTitle("Skicka epost");
			$view->setModel($model);
			return new AdminMaster($view);
		}

		function handleFormRequest($action,$target,$identifiers){
			die("form request not implemented");
		}

		function handleListRequest($action,$target,$identifiers){
			die("list request not implemented");
		}
		
		/** 
		 * @param ListItem_Event[] $eventList
		 * @param bool $includeAll
		 */
		static function createEventDropdownData($eventList,$includeAll = true){
			$return = $includeAll ? array(array("label" => "Alla arrangemang","value"=> "-1")) : array(); 
			foreach($eventList as $event){
				$return[] = array("label" => $event->event, "value" => $event->event_id);
			}
			return $return;
		}
		
		/**
		 * @param SlotType[] $slotTypes
		 * @param bool $includeAll
		 */
		static function createSlotTypeDropdownData($list,$includeAll = true){
			$return = $includeAll ? array(array("label" => "Alla typer","value"=> "-1")) : array(); 
			foreach($list as $type){
				$return[] = array("label" => $type->name, "value" => $type->id);
			}
			return $return;
		}
		
		/**
		 * @param Article[] $list
		 * @param bool $includeAll
		 */
		static function createArticleDropdownData($list,$includeAll = true){
			$return = $includeAll ? array(array("label" => "Alla inträden","value"=> "-1")) : array(); 
			foreach($list as $article){
				$return[] = array("label" => $article->name, "value" => $article->id);
			}
			return $return;
		}

	}

?>
