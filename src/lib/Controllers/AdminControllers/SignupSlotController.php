<?php
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class SignupSlotController extends AngularAdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/ArticleController.php");
			require_once("lib/Controllers/AdminControllers/SignupSlotArticleController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		public static function getControllerName(){
			return "signupSlot";
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/SignupSlot/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/SignupSlot";
		}
		
		public function handleDelete($target, $identifiers, $returnAsJson = false) {
			$model = $this->createJsonModel();
			$view = $this->createJsonView();
			$signupSlotRef = \GothConEntityManager::createSignupSlotReference($identifiers["default"]);
			$model->setData(\Repository\SignupSlotRepository::delete($signupSlotRef));
			$view->setModel($model);
			return $view;
		}
		
		public function handleGet($target, $identifiers, $returnAsJson = false) {
			$model = $this->createJsonModel();
			$view = $this->createJsonView();
			if(isset($_GET["person_id"])){
			   $model->setData(\Repository\EventRepository::getPersonalSignupSlots($_GET["person_id"])); 
			}
			if(isset($_GET["group_id"])){
			   $model->setData(\Repository\EventRepository::getGroupSignupSlots($_GET["group_id"])); 
			}
			$view->setModel($model);
			return $view;
		}
		
		public function handlePost($target, $identifiers, $returnAsJson = false) {
			$postData = $this->GetPostData();

			if($postData->id > 0){
				$signupSlot = Repository\SignupSlotRepository::getAsObject($postData->id);
			}else{
				$signupSlot = new Entities\SignupSlot();
				$eventOccationId = $postData->eventOccation->id;
				$eventOccation = GothConEntityManager::createEventOccationReference($eventOccationId);
				$signupSlot->setEventOccation($eventOccation);
			}
			
			if(isset($postData->slotType) && isset($postData->slotType->id)){
				$slotType = GothConEntityManager::createSlotTypeReference($postData->slotType->id);
				$signupSlot->setSlotType($slotType);
			}else{
				$signupSlot->setSlotType(null);
			}
			
			$signupSlot->setMaximumSignupCount(isset($postData->maximumSignupCount) ? $postData->maximumSignupCount : null);
			$signupSlot->setMaximumSpareSignupCount(isset($postData->maximumSpareSignupCount) ? $postData->maximumSpareSignupCount : null);
			$signupSlot->setRequiresApproval(isset($postData->requiresApproval) ? $postData->requiresApproval : null);
			$signupSlot->setIsHidden(isset($postData->isHidden) ? $postData->isHidden : null);

			\Repository\GothConRepository::save($signupSlot);
			
			$signupSlot = Repository\SignupSlotRepository::getAsArray($signupSlot->getId());
			
			$model = $this->createJsonModel();
			$view = $this->createDoctrineJsonView();
			
			$model->setData($signupSlot);
			$view->setModel($model);
			return $view;
			
			
		}
	}
