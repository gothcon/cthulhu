<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");

	
	class OldPersonController extends AdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/SignupController.php");
			require_once("lib/Controllers/AdminControllers/GroupMembershipController.php");
			require_once("lib/Controllers/AdminControllers/AdminUserController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
			if(count($segments) == 2){

				switch($segments[1]){
					case CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING:
						$action = $segments[1];
						$identifiers["default"] = $segments[0];
						return true;
						break;
				}
			}
			return parent::parseUrl($segments, $action, $targetType, $identifiers);
		}
		
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/OldPerson";
		}	
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/OldPerson";
		}
		
		public static function getControllerName(){
			return "person";
		}
		
		public function PreHandleAction() {
			return parent::PreHandleAction();
		}
		
		function handleJsonRequest($action,$target,$identifiers){
			$view = parent::createJsonView();
			$model = parent::createJsonModel();
			$model->setData(Person::loadList());
			$view->setModel($model);
			return $view;
		}
		
		function handleAction($action,$target,$identifiers){
			if($action == CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING){
				
				$personalSleepingSlotBooking = SleepingSlotBooking::loadByPersonId($identifiers["default"]);
				if($personalSleepingSlotBooking && fromPost("sleeping_resource_id") == -1){
						$personalSleepingSlotBooking->delete();
				}else if(fromPost("sleeping_resource_id") != -1){
					if(!$personalSleepingSlotBooking){
						$personalSleepingSlotBooking = new SleepingSlotBooking();
						$personalSleepingSlotBooking->person_id = $identifiers["default"];
					}
					$personalSleepingSlotBooking->sleeping_resource_id = fromPost("sleeping_resource_id");
					$personalSleepingSlotBooking->save();
				}
				
				return new RedirectView($this->internalLink("person","{$identifiers["default"]}/". "#".Html::CreateHash(Translator::translate("sleepingResource/sleepingResource"))));
			}
			if($target == "signup" 		&& $action == CONTROLLER_ACTION_DELETE){
				Signup::deleteById($identifiers[$target]);
				if($this->isJsonRequest()){
					$model = $this->createJsonModel();
					$view = $this->createJsonView();
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage(Translator::translate("signup/deleteMessage"));
					$view->setModel($model);
				}else{
					$view = new RedirectView($this->internalLink(static::getControllerName(), "{$identifiers["default"]}.{$person}/#Anmalningar"));
				}
				return $view;
			}
			else if($target == "default" && $action == CONTROLLER_ACTION_DELETE){
				Person::deleteById($identifiers[$target]);
				return new RedirectView($this->controllerPath());
			}
			else if($target == "default" && $action == CONTROLLER_ACTION_SAVE && $this->isJsonRequest()){
				$id = $identifiers[$target];
				if($id>0)
					$person = Person::loadById($id);
				else
					$person = new Person();
				
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$this->populateFromPost($person);
				if($person->save()){
					$model->setData(static::toJsonObject($person));
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage(Translator::translate("people/updateSuccessMessage"));
				}else{
					$validationErrors = $person->getValidationErrors();
					$message = array();
					foreach($validationErrors as $invalidPropertyName => $propertyError){
						$message[] = new MessageEntry(Translator::translate("people/validationErrors/{$propertyError}"), MessageType::ERROR,$invalidPropertyName);
					}
					$model->setData(static::toJsonObject($person));
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage($message);
				}
				$view->setModel($model);
				return $view;
			}else{ 
				return parent::handleAction($action,$target,$identifiers);
			}
		}
		
		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();
					
			$sortParameters = static::getSortParameters(array("property" => "last_name","direction" => "asc", "offset" => "0", "count" => "-1"));
			$model->setSortParameters($sortParameters);
			$model->setPersonList(Person::loadSorted($sortParameters["property"],$sortParameters["direction"]));
			
			$model->setTitle(Translator::translate("people/list"));
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}

		function handleFormRequest($action,$target,$identifiers){
			
			$view = $this->createFormView();
			$model = $this->createFormModel();
			
			$model->setUserLevelSelectData(UserController::getUserLevelSelectData()); 
			
			if(isset($identifiers["default"]) && is_numeric($identifiers["default"]) && $identifiers["default"] > 0){
				$person = Person::loadById($identifiers["default"]);
				$model->setPerson($person);
				// get all the signups
				$model->setSignupList(Signup::loadAllFilteredByPerson($person));
				// get group memberships
				$model->setGroupMembershipList(GroupMembership::loadByPerson($person));
				// get orders
				$model->setOrderList(Order::loadByPersonId($person->id));

				$model->setAvailableSleepingResources(SleepingSlotBooking::getPersonalSleepingResorces(Application::getConfigurationValue("sovplatsarrangemang"),$person->id));
				$model->setSleepingSlotBooking(SleepingSlotBooking::loadByPersonId($person->id));
				
				$user = User::loadByPersonId($person->id);
				if(!$user){
					$user = new User();
					$user->password = User::generateRandomString("8");
					$user->person_id = $person->id;
					if(User::usernameIsTaken($person->first_name)){
						$user->username = $person->first_name.$person->id; 
					}else{
						$user->username = $person->first_name;
					}
					$user->save();
				}
				$model->setUser($user);
				
			
			}
			else{
				$person = new Person();
				$model->setPerson($person);
			}
			
			if($this->isPostback()){
				$this->populateFromPost($person);
				$person->save();
				$view = new RedirectView($this->internalLink(static::getControllerName(), "{$person->id}/"));
				return $view;
			}

			if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
				$model->setTitle(Translator::translate("general/edit")." {$person->id}. {$person->first_name} {$person->last_name}");
			}else{
				$model->setTitle(Translator::translate("people/newPerson"));
			}	
			$view->setModel($model);
			$masterView = new AdminMaster($view);
			$masterView->setModel($model);
			return $masterView;
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			return $this->handleFormRequest($action,$target,$identifiers);
		}

	}

?>