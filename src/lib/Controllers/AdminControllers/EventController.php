<?php

require_once("lib/Controllers/AdminControllers/AngularAdminController.php");


class EventController extends AngularAdminController {

	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/SignupController.php");
		require_once("lib/Controllers/AdminControllers/SignupSlotController.php");
		require_once("lib/Controllers/AdminControllers/EventOccationController.php");
		require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		parent::__construct($internalLinkBuilder);
	}

	public static function getControllerName() {
		return "event";
	}

	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Event";
	}

	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Event";
	}
	
	public function handlePost($target, $identifiers, $returnAsJson = false) {
		$eventData = $this->GetPostData();
		$event = $this->saveEvent($eventData);
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		$model->setData($event);
		$view->setModel($model);
		return $view;
	}
	
	protected function saveEvent($eventData){
		if(isset($eventData->id) && $eventData->id > 0){
			$event = Repository\EventRepository::getEvent($eventData->id,true);
		}else{
			$event = new \Entities\Event();
		}
		
		$organizer = $event->getOrganizer();
		if(!$organizer)
		{
			/// no previous organizer but a new organizer 
			$organizer = new Entities\Organizer();
			$organizer->setEvent($event);
		}
		if(isset($eventData->organizer->groupId)){
			$organizer->setGroup(GothConEntityManager::createGroupReference($eventData->organizer->groupId));
		}else{
			$organizer->setGroup(null);
		}
		if(isset($eventData->organizer->personId)){
			$organizer->setPerson(GothConEntityManager::createPersonalReference($eventData->organizer->personId));
		}else{
			$organizer->setPerson(null);
		}
		$event->setOrganizer($organizer);
		
		if(isset($eventData->info)){
			$event->setInfo($eventData->info);
		}
		if(isset($eventData->internalInfo)){
			$event->setInternalInfo($eventData->internalInfo);
		}
		if(isset($eventData->name)){
			$event->setName($eventData->name);
		}
		if(isset($eventData->preamble)){
			$event->setPreamble($eventData->preamble);
		}
		if(isset($eventData->scheduleName)){
			$event->setScheduleName($eventData->scheduleName);
		}
		if(isset($eventData->text)){
			$event->setText($eventData->text);
		}
		if(isset($eventData->visibleInPublicListing)){
			$event->setVisibleInPublicListing($eventData->visibleInPublicListing);
		}else{
			$event->setVisibleInPublicListing(0);
		}
		
		if(isset($eventData->visibleInSchedule)){
			$event->setVisibleInSchedule($eventData->visibleInSchedule);	
		}else{
			$event->setVisibleInSchedule(0);
		}
		
		if(isset($eventData->eventtypeId)){
			$event->setEventtype(GothConEntityManager::createEventTypeReference($eventData->eventtypeId));
		}
		
		Repository\EventRepository::save($event);
		
		return Repository\EventRepository::getEvent($event->getId(),false);
	}

	private function createCustomTimespan($timespanData){
		$timespan = new \Entities\Timespan();
		$timespan->setIsPublic(false);
		$timespan->setName($timespanData->name);
		$startsAt = new DateTime($timespanData->startsAt);
		$endsAt = new DateTime($timespanData->endsAt);
		$timespan->setStartsAt($startsAt);
		$timespan->setEndsAt($endsAt);
		$timespan->setShowInSchedule(false);
		return $timespan;
	}
	
	protected function createEventOccation($eventOccationData,$eventId) {
		return EventOccationController::saveEventOccation($eventOccationData, $eventId);
	}
	
	public function handleGet($target = "default", $identifiers = array(), $returnAsJson = false) {
		if(count($identifiers) > 0){
			if(is_numeric($identifiers[$target])){
				$event = \Repository\EventRepository::getEvent($identifiers[$target],false);
			}else{
				$event = new \Entities\Event();
			}
			if($returnAsJson){
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$model->setData($event);
			}else{
				$view =  $this->createIndexView();
				$model = $this->createIndexModel();
				$view->setInternalLinkBuilder($this->internalLinkBuilder);
			}
			$view->setModel($model);
			return $view;
		}else{
			// $view = $this->handleListRequest(CONTROLLER_ACTION_LISTING, $target, $identifiers);
			if($returnAsJson){
				$events = \Repository\EventRepository::getAll();
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$model->setData($events);
				$view->setModel($model);
			}else{
				$view =  $this->createIndexView();
				$model = $this->createIndexModel();
				$view->setInternalLinkBuilder($this->internalLinkBuilder);
				$view->setModel($model);
			}

			return $view;
		}

	}
	
	public function handleServiceRequest($serviceName){
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		if($serviceName == "formMetadata"){
			 $eventTypes = Repository\EventTypeRepository::getAll();
		     $publicTimespans = Repository\TimespanRepository::getPublic();
			 $sigupSlotTypes = Repository\SlotTypeRepository::all();
			 $model->setData(array("eventTypes" => $eventTypes, "timespans" => $publicTimespans, "slotTypes" => $sigupSlotTypes));
		}
		if($serviceName == "createEventOccation"){
			$eventId = $this->urlSegments[0];
			$eventOccationData = static::getPostedJsonObject();
			$eventOccation = $this->createEventOccation($eventOccationData,$eventId);
			$model->setData($eventOccation);
		}
		if($serviceName == "getResourceBookingScheduleData"){
			$data = array(
				"events" => Repository\EventRepository::getResourceBookings(),
				"publicTimespans" => Repository\TimespanRepository::getPublicScheduleTimespans());

			$model->setData($data);
		}
		$view->setModel($model);
		return $view;
	}
}