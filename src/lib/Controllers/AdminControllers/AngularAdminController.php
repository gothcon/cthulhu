<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AngularAdminController
 *
 * @author Joakim
 */
require_once("lib/Controllers/AngularController.php");
class AngularAdminController extends AngularController{
	//put your code here
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
		parent::__construct($internalLinkBuilder);
	}
	
	public function preRenderView($view){
		if(IS_GET_REQUEST && !JSONREQUEST){
			$view = new AngularAdminMaster($view);
		}
		return $view;
	}
	
}
