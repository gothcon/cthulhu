<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
	class StatisticsController extends AdminController{
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_INDEX;
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Statistics";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Statistic/";
	}
	
	function handleAction($action,$target,$identifiers){
		if($this->isJsonRequest()){
			if($action == CONTROLLER_ACTION_DELETE){
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$success = true;
					if($success){
						$model->setStatus(JsonModel::STATUS_OK);
						$model->setMessage("OK");
						$model->setData(null);
					}
					else{
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage("Not OK");
						$model->setData(null);
					}
			}
			
			if($action == CONTROLLER_ACTION_NEW){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$success = true;
				if($success){
					$model->setStatus(JsonModel::STATUS_OK);
					$model->setMessage("OK");
					$model->setData(null);
				}
				else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage("Not OK");
					$model->setData(null);
				}
			}   
			$view->setModel($model);
			return $view;
		}else{
			return parent::handleAction($action,$target,$identifiers);
		}
	}

	function getIndexView(){
		require_once($this->getViewDirectory() . "/Index.php");
		$view = new IndexView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	function getIndexModel(){
		require_once(parent::getModelDirectory() . "/HtmlModel.php");
		$model = new HtmlModel();
		return $model;
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$model = $this->getIndexModel();
		
		$data = array();
		
		$noOfLogins = 0;
		$uniqueSignupees = array();
		
		

		
		$view = $this->getIndexView();
		$model->setTitle("Statistics");
		$view->setModel($model);
		return new AngularAdminMaster($view);
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
		$view = parent::handleFormRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("Edit");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("List");
		$view->setModel($model);
		$view = new AdminMaster($view);
		return $view;
	}
	
}

?>