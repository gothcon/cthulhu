<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class GroupMembershipController extends AdminController{
	
		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/GroupController.php");
			require_once("lib/Controllers/AdminControllers/SignupController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		public static function getControllerName(){
			return "gruppmedlemskap";
		}

		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}

		function getViewDirectory(){
			die("not implemented");
		}	

		function getModelDirectory(){
			die("not implemented");
		}

		/**
		 * 
		 * @param type $action
		 * @param type $target
		 * @param type $identifiers
		 */
		
		public function userHasPermisson($action) {
			if($action == CONTROLLER_ACTION_DELETE){
				$user = UserAccessor::getCurrentUser();
				return $user->userHasAccess(UserLevel::WORKER);
			}
			else{
				return parent::userHasPermisson($action);
			}
		}
		
		
		protected function handleJsonRequest($action, $target, $identifiers) {

			
			if($_SERVER["REQUEST_METHOD"] == "DELETE"){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$id = $identifiers["default"];
				\Repository\GroupRepository::removeMembership($id);
				$model->setStatus(JsonModel::STATUS_OK);
			}else{
				$view = parent::handleJsonRequest($action, $target, $identifiers);
				$model = $view->getModel();
				$model->setStatus(JsonModel::STATUS_OK);
				switch($action){
					case CONTROLLER_ACTION_DELETE:
						GroupMembership::deleteById($identifiers["default"]);
						$model->setMessage(Translator::translate("groupMembership/deleteMessage"));
						break;
					case CONTROLLER_ACTION_UPDATE:
						$membership = GroupMembership::loadById($action["default"]);
						$this->populateFromPost($membership);
						$membership->save();
						$model->setData(static::toJsonObject($membership));
						$model->setMessage(Translator::translate("groupMembership/updateMessage"));
					break;
					case CONTROLLER_ACTION_NEW:
						$model->setStatus(JsonModel::STATUS_OK);
						if(fromPost("entityIDs") && fromPost("group_id")){
							$membershipIDs = array();
							foreach(fromPost("entityIDs") as $id){
								if(!$this->isMemberInGroup(fromPost("group_id"),$id)){
									$membership = new Groupmembership();
									$membership->group_id = fromPost("group_id");
									$membership->person_id = $id;
									if($membership->save()){
										$membershipIDs[] = $membership->id;
									}
								}
							}
							$memberships = GroupMembership::loadByIds($membershipIDs);
							if(count($membershipIDs)){
								$model->setData(static::toJsonObject($memberships));
							}
							$model->setStatus(JsonModel::STATUS_OK);
							if(count($membershipIDs) == 1){
								$group = Group::loadById($memberships[0]->group_id);
								$model->setMessage(sprintf(Translator::translate("groupMembership/createMessage"),"{$memberships[0]->person->first_name} {$memberships[0]->person->last_name}",$group->name));
							}else if(count($membershipIDs) > 1){
								$model->setMessage(Translator::translate("groupMembership/createMultipleMessage"));
							}else{
								$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
								$model->setMessage(Translator::translate("groupMembership/cannotAddPersonTwiceErrorMessage"));
							}
						}else{
							$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
							$model->setMessage(Translator::translate("general/faultyData"));
							$model->setData(null);
						}
						break;
					default:
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
				}
			}
			$view->setModel($model);
			return $view;
		}
		

		private $membersInGroup = array();

		public function isMemberInGroup($groupId,$personId){
			if(!isset($this->membersInGroup[$groupId])){
				$members = GroupMembership::loadByGroup($groupId);
				$this->membersInGroup[$groupId] = array();
				foreach($members as $member){
					$this->membersInGroup[$groupId][$member->person_id] = $member;
				}
			}
			return isset($this->membersInGroup[$groupId][$personId]);
		}

		function handleAction($action,$target,$identifiers){
			if($this->isJsonRequest()){
				return $this->handleJsonRequest($action, $target, $identifiers);
			}else{
				if($action == CONTROLLER_ACTION_UPDATE){
					$membership = GroupMembership::loadById($identifiers["default"]);
					$group = Group::loadById($membership->group_id);
					$group->leader_membership_id = fromPost("is_leader") ? $membership->id : 0;
					$group->save();
					return new RedirectView(fromGet("returnUrl",""));
				}
				else if($action == CONTROLLER_ACTION_DELETE){
					GroupMembership::deleteById($identifiers["default"]);
					return new RedirectView(fromGet("returnUrl",""));
				}
				else{
					return parent::handleAction($action,$target,$identifiers);
				}
			}
		}

		function handleIndexRequest($action,$target,$identifiers){
			die("index request not implemented");
		}

		function handleFormRequest($action,$target,$identifiers){
			die("form request not implemented");
		}

		function handleListRequest($action,$target,$identifiers){
			die("list request not implemented");
		}

	}