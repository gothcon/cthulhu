<?php 
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	require_once("lib/Views/DoctrineJsonView.php");
	
	class GroupController extends AngularAdminController{
	
		public function __construct($internalLinkBuilder) {
			parent::__construct($internalLinkBuilder);
		}

		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Group";
		}	

		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Group";
		}
		
		public function HandleServiceRequest($serviceName) {
			if($serviceName == "UpdateLeader"){
				
				
			}else{
				parent::HandleServiceRequest($serviceName);
			}
		}

		function handleJsonRequest($action,$target,$identifiers){
			$view = parent::handleJsonRequest($action, $target, $identifiers);
			$model = $view->getModel();
			$model->setStatus(JsonModel::STATUS_OK);
			switch($action){
				case CONTROLLER_ACTION_UPDATE_LEADER:
					$group = Group::loadById($identifiers["default"]);
					if($group){
						$group->leader_membership_id = fromPost("leader_membership_id");
						$leaderMembership = GroupMembership::loadById($group->leader_membership_id);
						$group->save();
						$model->setMessage("Ledaren har uppdaterats");
						$model->setData((string)$leaderMembership->getPerson());
					}else{
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage(Translator::translate("group/invalidRequestErrors/invalidGroupId"));
					}
					break;
				case CONTROLLER_ACTION_LISTING:
					if(!fromGet("id",false)){
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage(Translator::translate("group/invalidRequestErrors/invalidPersonId"));
						$view->setModel($model);
					}else{
						$groups = GroupMembership::loadGroupsByLeader(fromGet("id"));
						$model->setData(static::toJsonObject($groups));
					}
					break;
				case CONTROLLER_ACTION_DELETE_GROUPSIGNUP:
					$signupId = fromGet("signup_id");
					Signup::deleteById($signupId);
					$model->setMessage(Translator::translate("signup/deleteMessage"));
					$model->setStatus(JsonModel::STATUS_OK);
					break;
				default:
					if($target == "default"){
						if(isPositiveNumber($identifiers[$target])){
							$group = Group::loadById($identifiers[$target]);
							$group->id = $identifiers[$target];
						}else{
							$group = new Group();
						}
						if(ISPOSTBACK){
							$this->populateFromPost($group);
							$group->id = isPositiveNumber($identifiers[$target]) ? $identifiers[$target] : 0;
							if($group->save()){
								$model->setMessage(Translator::translate("group/updateSuccessMessage"));
							}
							else{
								$validationErrors = $group->getValidationErrors();
								foreach($validationErrors as $invalidPropertyName => $propertyError){
									$message[] = new MessageEntry(Translator::translate("group/validationErrors/{$propertyError}"), MessageType::ERROR,$invalidPropertyName);
								}			
								$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
								$model->setMessage($message);
							}
						}
						$model->setData($group->getStandardClone());
					}
					else if($target == "signup"){
						
					}
			}
			$view->setModel($model);
			return $view;
		}

		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$sortParameters = static::getSortParameters(array("property" => "name","direction" => "asc", "offset" => "0", "count" => "-1"));
			$model->setSortParameters($sortParameters);
			$model->setGroupList(Group::loadSorted($sortParameters["property"],$sortParameters["direction"]));
			$model->setTitle(Translator::translate("group/groupList"));
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			
			$view = parent::handleIndexRequest($action,$target,$identifiers);
			$groupdata = array();
			
			
			if(isset($identifiers["default"]) && isPositiveNumber($identifiers["default"])){
				$group = Group::loadById($identifiers["default"]);
				$details = array(
					"name" => $group->name,
					"email" => $group->email_address,
					"note" => $group->note
				);
				$group = \Repository\GroupRepository::getGroup($identifiers["default"]);
				$groupdata["details"] = $group;
				
				
			}else{
				$groupdata["details"] = new Group();
			}
			$model = $view->getModel();
			$model->setGroup(\DoctrineEntitySerializer::Serialize($groupdata));

//			if(ISPOSTBACK){
//				static::populateFromPost($group);
//				if($group->save()){
//					$returnUrl = ($action == CONTROLLER_ACTION_UPDATE_LEADER) ? $this->internalLink("group", $group->id."/".CONTROLLER_ACTION_SAVE."#Medlemmar"): $this->internalLink(static::getControllerName(),$group->id);
//					$view = new RedirectView($returnUrl);
//					return $view;
//				}else{
//					static::addValidationErrorsToMessageHandler($group, "group/validationErrors");
//				}
//			}

//			$view = parent::handleIndexRequest($action,$target,$identifiers);
//			$model = $view->getModel();
//
//			if($group->id > 0){
//				$model->setTitle("{$group->id}. {$group->name}, {$group->email_address}");
//			}else{
//				$model->setTitle(Translator::translate("group/new"));
//			}
//
//			$model->setGroup($group);
//			$model->setSignupList(Signup::loadAllFilteredByGroup($group));
//			$model->setGroupMembers(GroupMembership::loadByGroup($group->id));
//			$view->setModel($model);
			$view = new AngularAdminMaster($view);
			$view->setModel($model);
			return $view;
		}

		function handleAction($action,$target,$identifiers){
			if(JSONREQUEST){
				return $this->handleJsonRequest($action, $target, $identifiers);
			}else{
				switch($action){
					case CONTROLLER_ACTION_UPDATE_LEADER:
						return $this->handleIndexRequest($action, $target, $identifiers);
						break;
					case CONTROLLER_ACTION_DELETE:
						Group::deleteById($identifiers["default"]);
						return new RedirectView($this->controllerPath());
						break;
					case CONTROLLER_ACTION_DELETE_MEMBERSHIP:
						$membershipId = fromGet("membership_id");
						GroupMembership::deleteById($membershipId);			
						return new RedirectView($this->internalLink("group",$identifiers["default"]."#".Html::CreateHash(Translator::translate("group/members")),false));
						break;
					case null:
						return $this->handleIndexRequest($action,$target,$identifiers);
						break;
					default:
						return parent::handleAction($action,$target,$identifiers);
				}
			}
		}

		function handleFormRequest($action,$target,$identifiers){
			return $this->handleIndexRequest($action,$target,$identifiers);
		}
		
	}

?>