<?php
require_once("lib/Controllers/AdminControllers/AdminController.php");



class FileAdminController extends AdminController{
	//put your code here
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) == 2){
			switch($segments[1]){
				case CONTROLLER_ACTION_DOWNLOAD:
				case CONTROLLER_ACTION_SHOW_IMAGE:
					$identifiers["default"] = $segments[0];
					$action = $segments[1];
					return true;
					break;
				default:
					return parent::parseUrl($segments, $action, $targetType, $identifiers);
				}
		}else{
			return parent::parseUrl($segments, $action, $targetType, $identifiers);
		}
	}
	
	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
	}
	
	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/FileAdmin";
	}
	
	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/FileAdmin";
	}
	
	protected function handleListRequest($action, $target, $identifiers) {
		$innerView = parent::handleListRequest($action, $target, $identifiers);
		$model = $innerView->getModel();
		$model->setTitle(Translator::translate("filehandling/list"));
		$model->setList(Image::loadSorted());
		$innerView->setModel($model);
		$masterView = new AdminMaster($innerView);
		return $masterView;
	}
	
	protected function handleFormRequest($action, $target, $identifiers) {
		if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
			$image = Image::loadByIdWithoutContent($identifiers["default"]);
			$oldFileExists = true;
			$pathToOldFile = Application::getFilesPath().$image->path.$image->name;
		}else{
			$image = new Image();
			$oldFileExists = false;
		}
		
		if(ISPOSTBACK){
			static::populateFromPost($image);
			$pathToNewFile = Application::getFilesPath().$image->path.$image->name;

			// new uploaded file
			if(strlen($image->content)){
				
				// delete the old file
				if($oldFileExists && $pathToOldFile != $pathToNewFile){
					// echo deleting old file
					unlink($pathToOldFile);
				}

				// echo "create file {$pathToNewFile}";
				
				// create new directory if necessary
				if(!file_exists(dirname($pathToNewFile)))
					mkdir(dirname($pathToNewFile), "0777", true);
				// copy the content to the cache
				$fp = fopen($pathToNewFile,'w');
				fwrite($fp,$image->content);
				fclose($fp);

			}else{
				// no new file
				if($pathToOldFile != $pathToNewFile && $oldFileExists){
					// file was renamed or moved
					if(file_exists($pathToOldFile)){
						if(file_exists($pathToNewFile)){
							unlink($pathToNewFile);
						}
						if(!file_exists(dirname($pathToNewFile)))
							mkdir(dirname($pathToNewFile));
						rename($pathToOldFile,$pathToNewFile);			
					}
					// TODO delete empty directories
				}
			}
			$image->save();
			
			return new RedirectView($this->internalLink("fileAdmin",$image->id,false));
		}
		
		$view = parent::handleFormRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setFile($image);
		if($image->id)
			$model->setTitle(Translator::translate("filehandling/edit") . " " .$image->name);
		else
			$model->setTitle(Translator::translate("filehandling/new"));
		$view->setModel($model);
		$masterView = new AdminMaster($view);
		return $masterView;
	}
	protected function handleIndexRequest($action, $target, $identifiers) {
		return $this->handleFormRequest($action, $target, $identifiers);
	}	
	
	public static function populateFromPost(&$image, $prefix = "") {
		parent::populateFromPost($image,$prefix);
		if($image->path == ""){
			$image->path = "/";
		}else{
			if($image->path{strlen($image->path)-1} !=  "/")
				$image->path .= "/";
		}
		if($image->path{0} != "/"){
			$image->path = "/".$image->path;
		}
		if(file_exists($_FILES['file']['tmp_name'])){
			$fp = fopen($_FILES['file']['tmp_name'],"rb");
			$image->content = fread($fp, $_FILES['file']['size']);
			fclose($fp);
		}
		if(trim($image->name) == "")
			$image->name = basename( $_FILES['file']['name']);
	}
	
	public function HandleAction($action, $target, $identifiers) {
		switch ($action){
			case CONTROLLER_ACTION_DOWNLOAD:
			case CONTROLLER_ACTION_SHOW:
				return $this->HandleFileRequest($action, $target, $identifiers);
				break;
			default:
				return parent::HandleAction($action, $target, $identifiers);
		}
	}
	
	public function HandleFileRequest($action, $target, $identifiers) {
		$file_id = $identifiers["default"];
		$file = Image::loadByIdWithoutContent($file_id);
		if($file){
			$fileDirectoryPath = Application::getFilesPath().$file->filePath;
			$filePath = $fileDirectoryPath."/".$file.name;
			// if not in the cache directory
			
			if(!file_exists($filePath)){
				$content = Image::loadContentById("/".$url);
				if(strlen($content)){
					mkdir(dirname($fileDirectoryPath), "0777", true);
					$fp = fopen($imageName,'w');
					fwrite($fp,$content);
					fclose($fp);
				}else{
					die("Error in file database. Could not find file (".$file->filePath."/".$file.name.")");
				}
			}
			header("Pragma: public");
			if($action == CONTROLLER_ACTION_DOWNLOAD){
				
			}else{
				header("Content-type: image/png"); 
				header('Content-Transfer-Encoding: binary'); 
				readfile($imageName);
			}
			exit;
		}else{
			$view = new FourOFourView();
			$view->init();
			$view->render();
		}
	}
}

?>
