<?php

require_once("lib/Controllers/AdminControllers/AdminController.php");




class OldEventController extends AdminController {

	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/SignupController.php");
		require_once("lib/Controllers/AdminControllers/SignupSlotController.php");
		require_once("lib/Controllers/AdminControllers/EventOccationController.php");
		require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		parent::__construct($internalLinkBuilder);
	}
	
	protected $invalidUrl = false;

	public static function getControllerName() {
		return "event";
	}

	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
	}

	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Event";
	}

	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Event";
	}
	
	protected function createScheduleModel(){
		require_once($this->getModelDirectory() . "/ScheduleModel.php");
		return new ScheduleModel();
	}

	protected function createScheduleView(){
		require_once($this->getViewDirectory() . "/Schedule.php");
		return new ScheduleView();
	}

	protected function createSignupOverviewModel(){
		require_once($this->getModelDirectory() . "/SignupOverviewModel.php");
		return new SignupOverviewModel();
	}

	protected function createSignupOverView(){
		require_once($this->getViewDirectory() . "/SignupOverView.php");
		$view = new SignupOverView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) > 0 && $segments[0] == "sleepingEvent"){
			$segments[0] = (int)Application::getConfigurationValue("sovplatsarrangemang");
		}
		
		if(count($segments) == 1 and $segments[0] == CONTROLLER_ACTION_SHOW_SIGNUP_OVERVIEW){
			$action = $segments[0];
			return true;
		}
		else if(count($segments) == 2){
			if(is_numeric($segments[0])){
				switch($segments[1]){
					case CONTROLLER_ACTION_OCCATIONS:
					case CONTROLLER_ACTION_CREATE_EVENT_OCCATION:
					case CONTROLLER_ACTION_DELETE_EVENT_OCCATION:
					case CONTROLLER_ACTION_UPDATE_EVENT_OCCATION:
					case CONTROLLER_ACTION_CREATE_SIGNUP_SLOT:
					case CONTROLLER_ACTION_DELETE_SIGNUP_SLOT:
					case CONTROLLER_ACTION_UPDATE_SIGNUP_SLOT:
						$identifiers["default"] = $segments[0];
						$action = $segments[1];
						$targetType = "default";
						return true;
				}
			}
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}	
	
	public function handleAction($action, $target, $identifiers) {
		if(JSONREQUEST){
			return $this->handleJsonRequest($action, $target, $identifiers);
		}
		else{
			switch($action){
				case CONTROLLER_ACTION_CREATE_EVENT_OCCATION:
				case CONTROLLER_ACTION_UPDATE_EVENT_OCCATION:
					$eventOccation = new EventOccation();
					if($this->saveEventOccation($eventOccation)){
						$view = new RedirectView($this->internalLink("event", $identifiers["default"]."#".Html::CreateHash(Translator::translate("event/eventOccationsAndSignups"))));
					}
					else{
						$view = $this->handleIndexRequest ($action, $target, $identifiers);
					}
					break;
				case CONTROLLER_ACTION_DELETE_SIGNUP_SLOT:
					if(UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
						$id = fromGet("signup_slot_id");
						SignupSlot::deleteById($id);
						$view = new RedirectView($this->internalLink("event", $identifiers["default"]."#".Html::CreateHash(Translator::translate("event/eventOccationsAndSignups"))));
					}else{
						MessageAccessor::addErrorMessage(Translator::translate("general/insufficientPermission"));
						$view = new RedirectView($this->internalLink("event", $event->id));
					}
					break;
				case CONTROLLER_ACTION_CREATE_SIGNUP_SLOT:
				case CONTROLLER_ACTION_UPDATE_SIGNUP_SLOT:
					$signupSlot = new SignupSlot();
					if($this->saveSignupSlot($signupSlot)){
						$view = new RedirectView($this->internalLink("event", $identifiers["default"]."#".Html::CreateHash(Translator::translate("event/eventOccationsAndSignups"))));
					}else{
						$view = $this->handleIndexRequest($action, $target, $identifiers);
					}
					break;
				case CONTROLLER_ACTION_SHOW_SIGNUP_OVERVIEW:
					$view = $this->handleSignupOverviewRequest($action, $target, $identifiers);
					break;
				case CONTROLLER_ACTION_NEW:
					$view = $this->handleFormRequest($action, $target, $identifiers);
					break;
				case CONTROLLER_ACTION_SAVE:
					$event = new Event();
					if($this->saveEvent($event,$identifiers)){
						$view = new RedirectView($this->internalLink(static::getControllerName(), $event->id . "/"));
					}else{
						$view = $this->handleIndexRequest($action, $target, $identifiers);
						$model = $view->getModel();
						$model->setEvent($event);
						$view->setModel($model);
					}
					return $view;
					break;
				case CONTROLLER_ACTION_DELETE:
					Event::deleteById($identifiers["default"]);
					$view = new RedirectView($this->internalLink("event"));
					return $view;
					break;
				case CONTROLLER_ACTION_LISTING:
					if(fromGet("format") == "schedule"){
						$view = $this->handleScheduleRequest($action, $target, $identifiers);
					}else{
						$view = $this->handleListRequest($action, $target, $identifiers);
					}
					break;
				default:
					$view = parent::handleAction($action, $target, $identifiers);
			}
			return $view;
		}
	}
		
	protected function handleJsonRequest($action, $target, $identifiers) {
		$view = parent::handleJsonRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setStatus(JsonModel::STATUS_OK);
		switch($action){
			case CONTROLLER_ACTION_LISTING:
				if(fromGet("mode") == "tree"){
					$tree = array();
					if($this->loadEventTree($tree)){
						$model->setData($tree);
					}else{
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage("Could not load event tree");
					}
				}else{
					$model->setData(static::toJsonObject(Event::loadList()));
				}
				break;
			case CONTROLLER_ACTION_CREATE_SIGNUP_SLOT:
			case CONTROLLER_ACTION_UPDATE_SIGNUP_SLOT:
				$signupSlot = new SignupSlot();
				if($this->saveSignupSlot($signupSlot)){
					if($action == CONTROLLER_ACTION_CREATE_EVENT_OCCATION)
						$model->setMessage(Translator::translate("signupSlot/creationSuccessful"));
					else
						$model->setMessage(Translator::translate("signupSlot/saveSuccessful"));
				}else{
					
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage(MessageAccessor::getMessages());
					MessageAccessor::clearAllMessages();
				}
				$model->setData($signupSlot->getStandardClone());
				break;
			case CONTROLLER_ACTION_CREATE_EVENT_OCCATION:
			case CONTROLLER_ACTION_UPDATE_EVENT_OCCATION:
				$eventOccationId = fromPost("event_occation_id",0);
				if(fromPost("event_occation_main_resource_booking_id",false)){
					if(is_numeric($eventOccationId) && $eventOccationId > 0){
						$eventOccation = EventOccation::loadById($eventOccationId);
						$eventOccation->main_booking_id = fromPost("event_occation_main_resource_booking_id",false);
						$eventOccation->save();
						$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveSucceeded"));
					}
					else{
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						$model->setMessage("Not OK");
						$model->setData(null);
					}
				}else{
					$eventOccation = new EventOccation();
					if($this->saveEventOccation($eventOccation)){
						$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveSucceeded"));
					}else{
						$model->setMessage(MessageAccessor::getMessages());
						$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
						MessageAccessor::clearAllMessages();
					}
					$model->setData($eventOccation->getStandardClone());
				}
				break;
			case CONTROLLER_ACTION_DELETE_EVENT_OCCATION:
				EventOccation::deleteById(fromGet("event_occation_id"));
				$model->setMessage(Translator::translate("eventOccation/jsonMessages/deletionSucceeded"));
				break;
			case CONTROLLER_ACTION_SAVE:
				$event = new Event();
				if($this->saveEvent($event, $identifiers)){
					$model->setMessage(Translator::translate("event/jsonMessages/saveSucceeded"));
				}
				else{
					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
					$model->setMessage(MessageAccessor::getMessages());
					MessageAccessor::clearAllMessages();
				}
				$model->setData($event->getStandardClone());
				break;
			case CONTROLLER_ACTION_OCCATIONS :
					$model = new SimpleJsonModel();
					$model->setData(Repository\EventRepository::getEventOccations($identifiers["default"]));
				break;
			default:
				break;
		}
		$view->setModel($model);
		return $view;
	}
	
	protected function handleScheduleRequest($action,$target, $identifiers){
		$view = $this->createScheduleView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = $this->createScheduleModel();
		$model->SetMasterTimespan(TimespanController::getMasterTimespan());
		$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
		$model->SetResourceSchedules(Event::loadEventResourceSchedules());
		$model->SetTitle(Translator::translate("resources/overview"));
		$view->setModel($model);
		return new AdminMaster($view);
	}

	protected function handleSignupOverviewRequest($action,$target, $identifiers){
		$view = $this->createSignupOverView();
		$model = $this->createSignupOverviewModel();
		$model->setEventTree(Event::loadOverviewTree());
		$model->SetTitle(Translator::translate("event/signupOverview"));
		$view->setModel($model);
		return new AdminMaster($view);
	}
	
	protected function handleListRequest($action, $target, $identifiers) {
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$sortParameters = static::getSortParameters(array("property" => "event_type","direction" => "asc", "offset" => "0", "count" => "-1"));
		$model->setSortParameters($sortParameters);
		$model->setEventList(Event::loadList($sortParameters["property"],$sortParameters["direction"]));
		$model->setTitle(Translator::translate("event/events"));
		return new AdminMaster($view);
	}

	protected function handleIndexRequest($action, $target, $identifiers) {
		
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		// load event
		if (isset($identifiers["default"]) && isPositiveNumber($identifiers["default"])) {
			$event = Event::loadById($identifiers["default"]);
			$model->setTitle(sprintf(Translator::translate("event/eventTitle"),$event->name));
			$model->setSignupSlots(SignupSlot::loadByEvent($event));
			$model->setSignups(Signup::loadByEvent($event));
			$model->setResourceBookings(EventOccationBooking::loadByEventId($event->id));
			$allEventOccations = EventOccation::loadByEvent($event);
			$model->setOccations($allEventOccations);
			$model->setTimespanSelectList(static::createTimespanSelectData(Timespan::loadPublic()));
			$model->setSlottypeSelectList(static::createSlotTypeSelectData(SlotType::loadList()));
		} else {
			$event = new Event();
			$model->setTitle(Translator::translate("event/newEvent"));
		}
		$model->setEvent($event);
		$model->setEventTypeSelectList(static::createEventTypeSelectData(EventType::loadList()));

		$model->init("targetType", $target);
		$model->init("targetAction", $action);
		$model->init("targetIdentifiers", $identifiers);
		$view->setModel($model);
		return new AdminMaster($view);
	}
	
	protected function handleFormRequest($action, $target, $identifiers) {
		return $this->handleIndexRequest($action, $target, $identifiers);
	}
	
	protected function loadEventTree(&$tree){
		$cardinality = fromGet("cardinality", "all");
		$personId = fromGet("person_id", 0);
		$groupId = fromGet("group_id", 0);
		if ($groupId !== 0) {
			$tree = Event::loadGroupEventTree($groupId);
		} else if ($personId !== 0) {
			$tree = Event::loadIndividualEventTree($personId, true);
		} else {
			$tree = Event::loadEventTree($cardinality, $groupId, $personId);
		}
		$tree = static::toJsonObject($tree);
		return true;
	}
	
	protected function saveSignupSlot(&$signupSlot){
		$signupSlotId = fromGet("signup_slot_id",0);
		if(is_numeric($signupSlotId)){
			if($signupSlotId > 0){
				$signupSlot = SignupSlot::loadById($signupSlotId);
			}else{
				$signupSlot = new SignupSlot;
			}
			
			static::populateFromPost($signupSlot);
			if(trim($signupSlot->maximum_signup_count) == "")
				$signupSlot->maximum_signup_count = null;
			if(trim($signupSlot->maximum_spare_signup_count) == "")
				$signupSlot->maximum_spare_signup_count = null;
			
			$article = $signupSlot->getArticle();
			$article->price = fromPost("article_price");
			$article->p = Application::getConfigurationValue("artikelkategori_arrangemang");
			$signupSlot->setArticle($article);

			if($signupSlot->save()){
				return true;
			}else{
				static::addValidationErrorsToMessageHandler($signupSlot->getValidationErrors(), "signupSlot/validationErrors");
				return false;
			}
			
		}
		return false;
	}
	
	protected function saveEventOccation(&$eventOccation){
		$eventOccationId = fromPost("event_occation_id",0);
		if(is_numeric($eventOccationId) && $eventOccationId > 0)
			$eventOccation = EventOccation::loadById($eventOccationId);
		else{
			$eventOccation = new EventOccation();
		}
		
		static::populateFromPost($eventOccation,"event_occation_");
		$timespan = new Timespan();
		static::populateFromPost($timespan, "timespan_");
		$timespan->starts_at_date	= fromPost("timespan_starts_at_date");
		$timespan->starts_at_time	= fromPost("timespan_starts_at_time");
		$timespan->ends_at_date		= fromPost("timespan_ends_at_date");
		$timespan->ends_at_time		= fromPost("timespan_ends_at_time");		
		$eventOccation->setTimespan($timespan);
		
		if($eventOccation->save()){
			return true;
		}else{
			static::addValidationErrorsToMessageHandler($eventOccation, "eventOccation/validationErrors");
			return false;
		}
	}
	
	protected function saveEvent(&$event,$identifiers){
		if($identifiers["default"] && is_numeric($identifiers["default"])){
			$event = Event::loadById($identifiers["default"]);
		}else{
			$event = new Event();
		}
		$this->populateFromPost($event);
		
		$organizer = new Organizer();
		$organizer->id = fromPost("organizer_id",0);
		
		if(fromPost("organizer_group_id",0) != 0){
			$organizer->group_id = fromPost("organizer_group_id");
		}else if(fromPost("organizer_person_id") != 0){
			$organizer->person_id = fromPost("organizer_person_id");
		}
		
		if($organizer->person_id > 0 || $organizer->group_id > 0){
			$event->setOrganizer($organizer);
		}else{
			$event->setOrganizer(null);
		}

		if($event->save()){
			return true;
		}else{
			static::addValidationErrorsToMessageHandler($event, "event/validationErrors");
			return false;
		}
	}

	static function createEventTypeSelectData($eventTypes){
		foreach ($eventTypes as $key => $value) {
			$eventTypes[$key] = array("label" => $value->name, "value" => $value->id);
		}
		return $eventTypes;
	}

	static function createTimespanSelectData($allTimespans){
		$selectList = array();
		foreach ($allTimespans as $timespan) {
			// if(!in_array($timespan->id, $occationTimespanIDs))
			$selectList[] = array("value" => $timespan->id, "label" => $timespan->__toString());
		}
		array_unshift($selectList, array("label" => Translator::translate("general/custom"), "value" => "0"));
		return $selectList;
	}
	
	static function createSlotTypeSelectData($slotTypes){
		$slotTypeSelectList = array();
		foreach ($slotTypes as $type) {
			$slotTypeSelectList[] = array("value" => $type->id, "label" => $type->name);
		}
		return $slotTypeSelectList;
	}
	
}