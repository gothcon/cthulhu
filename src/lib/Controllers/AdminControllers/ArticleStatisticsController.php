<?php
use \lib\Views\AdminViews\ArticleStatistics\IndexView;
use \lib\Models\AdminModels\ArticleStatistics\IndexModel;
require_once("lib/Controllers/AdminControllers/AdminController.php");
use \AdminMaster;
class ArticleStatisticsController extends AdminController{
	
	/** @return bool **/
	public function userHasPermisson($action){
		$currentUser = UserAccessor::getCurrentUser();
		return $currentUser->userHasAccess(UserLevel::WORKER);
	}
	
	function getDefaultAction(){
		return CONTROLLER_ACTION_INDEX;
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/ArticleStatistics";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/ArticleStatistics";
	}
	
	protected function handleIndexRequest($action, $target, $identifiers) {
		$model = $this->createIndexModel();
		$view = $this->createIndexView();
		$statistics = \Repository\StatisticsRepository::GetArticleSalesAmounts();
		$model->setStatistics($statistics);
		$model->SetTitle("Beställningsstatistik");
		$view->setModel($model);
		return new AdminMaster($view);
	}
	
	/** @return \lib\Models\AdminModels\ArticleStatistics\IndexModel */
	function createIndexModel() {
		require_once($this->getModelDirectory()."/IndexModel.php");
		return new IndexModel();
	}
	
	/** @return \lib\Views\AdminViews\ArticleStatistics\IndexView */
	function createIndexView() {
		require_once($this->getViewDirectory()."/Index.php");
		$view = new IndexView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
}

?>