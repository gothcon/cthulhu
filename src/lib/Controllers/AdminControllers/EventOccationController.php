<?php 
	require_once("lib/Views/AdminViews/Master/AdminMaster.php");
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class EventOccationController extends AngularAdminController{
		
	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
		parent::__construct($internalLinkBuilder);
	}
	
	public static function getControllerName(){
		return "arrangemangspass";
	}
	
	function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/EventOccation";
	}

	function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/EventOccation";
	}

	public function handleDelete($target, $identifiers, $returnAsJson = false) {
		$eventOccationId = $identifiers["default"];
		$eventOccation = GothConEntityManager::createEventOccationReference($eventOccationId);
		Repository\EventOccationRepository::delete($eventOccation);
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		$model->setData(true);
		$view->setModel($model);
		return $view;
	}
	
	public function handlePost($target, $identifiers, $returnAsJson = false) {
		$eventOccationData = $this->GetPostData();
		$eventOccation = $this->saveEventOccation($eventOccationData);
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		$model->setData($eventOccation);
		$view->setModel($model);
		return $view;
	}
	
	public function handleGet($target = "default", $identifiers = array(), $returnAsJson = false) {
		if(count($identifiers) > 0){
			if(is_numeric($identifiers[$target])){
				$eventOccation = \Repository\EventOccationRepository::get($identifiers[$target],false);
			}else{
				$eventOccation = new \Entities\EventOccation();
			}
			if($returnAsJson){
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$model->setData($eventOccation);
			}else{
				header("HTTP/1.0 400 Not implemented");
				exit();
			}
			$view->setModel($model);

		}else{
			// $view = $this->handleListRequest(CONTROLLER_ACTION_LISTING, $target, $identifiers);
			if($returnAsJson){
				$events = \Repository\TimespanRepository::getScheduleData();
				$timespans = \Repository\TimespanRepository::getPublicScheduleTimespans();
				$view = $this->createDoctrineJsonView();
				$model = $this->createJsonModel();
				$model->setData(array("events" => $events, "publicTimespans" => $timespans));
				$view->setModel($model);
			}else{
				$view = $this->handleListRequest();
			}
			
		}
		return $view;
	}
	
	public function handleServiceRequest($serviceName){
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		switch($serviceName){
			case "formMetadata":
				 $eventTypes = Repository\EventTypeRepository::getAll();
				 $publicTimespans = Repository\TimespanRepository::getPublic();
				 $sigupSlotTypes = Repository\SlotTypeRepository::all();
				 $model->setData(array("eventTypes" => $eventTypes, "timespans" => $publicTimespans, "slotTypes" => $sigupSlotTypes));
				 break;
			case "scheduleableEvents" :
				if(count($this->urlSegments) < 3){
					header("HTTP/1.0 400 Bad Request");
					echo "<br/><h1>Missing event occation id in url!</h1>";
					exit;
				}
				$model->setData(EventOccation::loadOccationAndSlots($this->urlSegments[2]));
				 break;
			default:
		}
		$view->setModel($model);
		return $view;
	}
	
	public static function saveEventOccation($eventOccationData,$eventId = null) {
		if(isset($eventOccationData->id) && $eventOccationData->id > 0){
			// this is an update 
			$eventOccation = \Repository\EventOccationRepository::getAsObject($eventOccationData->id);
			
			if(!$eventOccationData->timespan->isPublic){
				// Detta är ett anpassat pass
				if($eventOccationData->timespan->id > 0){
					$timespan = Repository\TimespanRepository::get($eventOccationData->timespan->id);
				}
				
				if(!$timespan){
					$timespan = new Entities\Timespan();
				}
				$timespan->setName($eventOccationData->timespan->name);
				$timespan->setStartsAt(new DateTime($eventOccationData->timespan->startsAt));
				$timespan->setEndsAt(new DateTime($eventOccationData->timespan->endsAt));
				$timespan->setIsPublic(false);
				$timespan->setShowInSchedule($eventOccationData->timespan->showInSchedule);
				\Repository\TimespanRepository::save($timespan);
			}else{
				// det nya passet är ett standardpass
				$oldTimespan = $eventOccation->getTimespan();
				if($oldTimespan && $oldTimespan->isCustom()){
					\Repository\EventRepository::delete($oldTimespan);
				}
				$timespan = \Repository\TimespanRepository::getAsObject($eventOccationData->timespan->id);
			}
		}else{
			// this is a new event occation
			$eventOccation = new \Entities\EventOccation();
			$event = GothConEntityManager::createEventReference($eventId);
			$eventOccation->setEvent($event);
			if(!$eventOccationData->timespan->isPublic){
				$timespan = $this->createCustomTimespan($eventOccationData->timespan);
				\Repository\EventRepository::save($timespan);
			}else{
				$timespan = \Repository\TimespanRepository::getAsObject($eventOccationData->timespan->id);
			}
		}
		$eventOccation->setName($eventOccationData->name);
		$eventOccation->setIsHidden($eventOccationData->isHidden);
		$eventOccation->setTimespan($timespan);
		\Repository\EventOccationRepository::save($eventOccation);
		return \Repository\EventOccationRepository::get($eventOccation->getId());
	}
	
	protected function handleUpdateEventOccation($action, $target, $identifiers) {
		
		if(!$identifiers['default'] || !is_numeric($identifiers['default']))
			die("invalid identifier");
			
		$eventOccation = EventOccation::loadById($identifiers['default']);

		if(fromRequest("event_occation_main_resource_booking_id",null)){
			$eventOccation->main_resource_booking_id = fromRequest("event_occation_main_resource_booking_id");
			$eventOccation->save();
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setData($this->toJsonObject($eventOccation));
				$model->setMessage(Translator::translate("event/jsonMessages/mainResourceUpdateSucceeded"));
				$view->setModel($model);
				return $view;
			}else{
				return new RedirectView(fromRequest("returnUrl"));
			}
		}else{
			if ($eventOccation->update()) {
				if ($this->isJsonRequest()) {
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$jsonEventObject = static::toJsonObject($eventOccation);
					$jsonEventObject->timestamp = static::toJsonObject($eventOccation->getTimespan());

					$model->setData($jsonEventObject);
					$model->setStatus(0);
					$model->setMessage(Translator::translate("eventOccation/jsonMessages/saveSucceeded"));
					$view->setModel($model);
				} else {
					// redirect to index view
					$view = new RedirectView($this->internalLink(static::getControllerName(),$identifiers["default"] . "/#Pass och anmalningar"));
				}
			} else {
				// invalid occation, show the form
				$view = $this->handleIndexRequest($action, $target, $identifiers);
				$model = $view->getModel();
				$this->model->setCurrentOccation($eventOccation);
				$this->model->setCurrentTimespan($eventOccation->getTimespan());
			}
		}
		return $view;
	}

	protected function handleDeleteEventOccation($action, $target, $identifiers) {
		
		EventOccation::deleteById($identifiers["default"]);
		
		if ($this->isJsonRequest()) {
			$view = $this->createJsonView();
			$model = $this->createJsonModel();
			$model->setStatus(0);
			$model->setMessage(Translator::translate("eventOccation/jsonMessages/deletionSucceeded"));
			$view->setModel($model);
		} else {
			$view = new RedirectView($this->internalLink(static::getControllerName(), $identifiers["default"] . "/#Pass och anmalningar"));
		}
		return $view;
	}
	
	protected function handleJsonRequest($action, $target, $identifiers) {
		$view = parent::handleJsonRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$data = new stdClass();
		$model->setData(EventOccation::loadOccationAndSlots($identifiers["default"]));
		$model->setStatus(0);
		$model->setMessage("");
		$view->setModel($model);
		return $view;
	}
	
	protected function handleListRequest(){		
		$view = $this->createListView();
		$model = $this->createListModel();
		$model->SetMasterTimespan(TimespanController::getMasterTimespan());
		$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
		$model->SetResourceSchedules(Event::loadTree());
		$model->SetTitle(Translator::translate("resources/overview"));
		$view->setModel($model);
		return $view;
	}
	
}

?>