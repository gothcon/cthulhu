<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class OrderController extends AdminController{

		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/PersonController.php");
			require_once("lib/SessionAccessors/CartAccessor.php");
			parent::__construct($internalLinkBuilder);
		}
		
		public static function getControllerName(){
			return "order";
		}	
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Order/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Order";
		}
		
		static function createOrderRowStatusSelectData($statusValues){
			$values = array();
			foreach($statusValues as $statusValue){
				$values[] = array("value" => $statusValue,"label" => Translator::translate("order/orderRowStatus/".$statusValue));
			}
			return $values;
		}
		
		static function createOrderStatusSelectData($statusValues){
			$values = array();
			foreach($statusValues as $statusValue){
				$values[] = array("value" => $statusValue,"label" => Translator::translate("order/orderStatus/".$statusValue));
			}
			return $values;
		}
		
		function handleCreateOrderFromCart($action,$target,$identifiers){
			$order = CartAccessor::getCurrentCart();
			$currentCustomer = CartAccessor::getCurrentCustomer();
			$order->person_id = $currentCustomer->id;
			if($order->isEmpty()){
				die(Translator::translate("order/emptyOrder"));
			}
			$order->status = "submitted";
			$order->save();
			CartAccessor::destroy();
			return new RedirectView($this->internalLink(PersonController::getControllerName(), $currentCustomer->id ."/#".Html::CreateHash(Translator::translate("people/submittedOrders"))));
		}
		
		
		protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
			
			if(count($segments) == 2 && $segments[1] == CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT){
				$identifiers["default"] = $segments[0];
				$action = CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT;
				return true;
			}
			
			return parent::parseUrl($segments, $action, $targetType, $identifiers);
		}
		
		function handleAction($action,$target,$identifiers){
			if($action == CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT){
				
				$orderPayment = OrderPayment::loadById($identifiers["default"]);
				if(!$orderPayment){
					return $this->handle404 ();
				}
				$orderId = $orderPayment->order_id;
				$orderPayment->transaction->cancelled = 1;
				$orderPayment->save();
				
				return new RedirectView($this->internalLink("order",$orderId."#".Html::CreateHash(Translator::translate("orderPayment/payments"))));
			}
			else if($action  == CONTROLLER_ACTION_CREATE_ORDER_FROM_CART){
				$view = $this->handleCreateOrderFromCart($action, $target, $identifiers);
			}
			else if($action == CONTROLLER_ACTION_NEW && !$this->isPostback()){
				$id = fromGet('p',0);
				CartAccessor::setCurrentCustomer(Person::loadById($id));
				$order = Order::loadOpen($id);
				
				if($order){
					CartAccessor::setCurrentCart($order);
				}
				else{
					$order = new Order;
					$order->person_id = $id;
					CartAccessor::setCurrentCart ($order);
				}
				$view = new RedirectView($this->internalLink("article"));
				
			}else{
				$view = parent::handleAction($action,$target,$identifiers);
			}
			return $view;
		}	
		
		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$model->setTitle(Translator::translate("order/list"));
			$model->setOrders(Order::loadList());
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
		
		function handleFormRequest($action,$target,$identifiers){
			$view = parent::handleFormRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$model->setTitle(Translator::translate("order/order"));
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}

		function handleIndexRequest($action,$target,$identifiers){
			$view = parent::handleIndexRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$order = Order::loadById($identifiers['default']);
			if($this->isPostback()){
				static::populateFromPost($order); 
				$order->save();
				$order = Order::loadById($order->id);
			}
			$model->setTitle(sprintf(Translator::translate("order/orderTitle"),$order->id));
			
			$model->setOrder($order);
		
			$model->setOrderPayments(OrderPayment::getPaymentsForOrder($order->id));
			$model->setRemainingSum($order->total - OrderPayment::getTotalOrderPaymentSum($order->id));
			
			
			
			$model->setOrderStatusValues(static::createOrderStatusSelectData(Order::getStatusValues()));
			$model->setOrderRowStatusValues(static::createOrderRowStatusSelectData(OrderRow::getStatusValues()));
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;
		}
		
		static public function populateFromPost(&$order,$prefix=""){
			parent::populateFromPost($order,$prefix);
			
			if($order->person_id == 0){
				$order->person_id = null;
			}

			$articleIDs = array_keys(fromPost('row_status',array()));
			foreach($articleIDs as $articleId){
				$row = $order->getOrderRow($articleId);
				$row->note = fromPost(array('row_note',$articleId),"");
				$row->count = fromPost(array('row_count',$articleId),"-1");
				$row->price_per_each = fromPost(array('row_pricePerEach',$articleId),"-0.5");
				$row->status = fromPost(array('row_status',$articleId),"foo");
				$order->setOrderRow($row);
			}
			
			return true;
		}
		

	}