<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
		require_once("lib/Database/MySQL/Repositories/SearchRepository.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReceptionController
 *
 * @author Joakim
 */
class ReceptionController extends AdminController{
			
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}

	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Reception";
	}

	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Reception";
	}
	
	protected function handleIndexRequest($action, $target, $identifiers) {

		$entranceFeeSelected = false;
		$toBeDelivered = array();
		$remainingAmount = 0;
		$person = Person::loadById($identifiers["default"]);
		$personalOrders = Order::loadByPersonId($person->id);

		if(ISPOSTBACK){
			if(fromPost("update")){
				$cart = Order::loadOpen($person->id);
				if(!$cart){
					$cart = new Order;
					$cart->person_id = $person->id;
				}
				$entranceFeeArticleId = fromPost("entrance_fee_article_id",null);
				if($entranceFeeArticleId != null){
					$entranceFeeArticle = Article::loadById($entranceFeeArticleId);
					$orderRow = new OrderRow;
					$orderRow->article = $entranceFeeArticle;
					$orderRow->count = 1;
					$orderRow->price_per_each = $entranceFeeArticle->price;
					$cart->addOrderRow($orderRow);
					$cart->save();
				}
				return new RedirectView($this->internalLink("reception",$person->id));
			}else{
				
				if(fromPost("IdentityIsOk") && fromPost("EntranceFeeIsOk") && fromPost("PaymentIsOk") && fromPost("PreOrdersAreOk") && fromPost("SleepingIsOk") && fromPost("OtherIsOk")){
					
					$deliveredItems = fromPost("orderRow", array());
					$amount = fromPost("amount");
					$paymentMethod = fromPost("payment_method");
					
					foreach($personalOrders as $order){
						$orderRows = OrderRow::loadByOrder($order->id);
						$order->setOrderRows($orderRows);
						foreach($orderRows as $row){
							if(isset($deliveredItems[$row->id]) && $row->status == OrderRow::STATUS_NOT_DELIVERED){
								$row->status = OrderRow::STATUS_DELIVERED;
								$row->save();
							}
						}
					}
					
					
					
					if($amount){
						// the receptionist has received money
						// load the signup slot articles that associates signups with the the order row article
						$signupSlotArticles = SignupSlotArticle::loadList();
						$signups = Signup::loadAllFilteredByPerson($person->id);
						$slotArticlesBySlot = array();
						foreach($signupSlotArticles as $slotArticle){
							$slotArticlesBySlot[$slotArticle->signup_slot_id] = $slotArticle;
						}
						
						foreach($personalOrders as $order){
							$orderRows = $order->getOrderRows();

							if($order->status == Order::STATUS_CANCELLED || $order->status == Order::STATUS_PAID){
								continue;
							}
							
							$remainingAmount = $order->getRemainingSum();
							
							if($remainingAmount > 0 && $amount >= $remainingAmount){
								$amount -= $remainingAmount;
								
								$transaction = new Transaction();
								$transaction->amount = $remainingAmount;
								$transaction->payment_method = $paymentMethod;
								$transaction->note = "Betalning för order # " . $order->id;
								
								$orderPayment = new OrderPayment();
								$orderPayment->order = $order;
								$orderPayment->transaction = $transaction;
								$orderPayment->save();
								
								// traverse the signups and look for signups that requires payment
								foreach($signups as $key => $signup){
									if(!$signup->is_signup_owner || $signup->is_paid){
										// this has been paid or is a group signup where the person only is a member
										continue;
									}
									if(isset($slotArticlesBySlot[$signup->signup_slot_id])){
										// this is a signup that has a fee
										$signupSlotArticle = $slotArticlesBySlot[$signup->signup_slot_id];
										// see if the paid order contains any payment of that signup
										$orderRow = $order->getOrderRow($signupSlotArticle->article_id);
										if($orderRow){
											$signup = Signup::loadById($signup->id);
											$signup->is_paid = true;
											$signup->save();
											$orderRow->status = OrderRow::STATUS_DELIVERED;
											$orderRow->save();
											$order->setOrderRow($orderRow);
										}
									}
								}
							}
						}
					}
					
					MessageAccessor::addInformationMessage("{$person->first_name} {$person->last_name} ({$person->identification}) har registrerats som anländ");

					return new RedirectView($this->internalLink("reception"));
				}else{
					MessageAccessor::addErrorMessage("Alla kontrollpunkterna kollades inte av!");
				}
			}
		}
		
		$entranceFeeArticles = $this->getEntranceFeeArticles();

		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		
		foreach($personalOrders as $order){
			$orderRows = OrderRow::loadByOrder($order->id);
			$order->setOrderRows($orderRows);

			if($order->status == Order::STATUS_CART){
				$order->removeUnavailableArticles();
			}
			
			if($order->status == Order::STATUS_CANCELLED){
				continue;
			}
			$remainingOrderAmount = $order->getRemainingSum();
			$remainingAmount += ($remainingOrderAmount <= 0) ? 0 : $remainingOrderAmount;
			if($remainingOrderAmount < 0)
				$model->addNote("<a href='".$this->internalLink("order",$order->id)."'>Betalat för mycket på order # ".$order->id."</a>");
			
			foreach($orderRows as $row){
				if(isset($entranceFeeArticles[$row->article_id]) && $row->status == OrderRow::STATUS_NOT_DELIVERED){
					$entranceFeeSelected = true;
				}
				//if($row->status == OrderRow::STATUS_NOT_DELIVERED){
					$toBeDelivered[] = $row; 
				//}
			}
		}
		
		
		
		

		$model->setPerson($person);
		
		$model->setRemainingAmount($remainingAmount);
		$model->setEntranceFeeSelected($entranceFeeSelected);
		$model->setEntranceFeeArticles($this->getEntranceFeeArticles());
		
		$model->setArticlesToBeDelivered($toBeDelivered);
		$model->setSleepingSlotBooking(SleepingSlotBooking::loadByPersonId($person->id));
		
		$model->setTitle("Ankomstregistrering");
		
		return new AdminMaster($view);
	}
	
	/** @return Article[] */                       
	protected function getEntranceFeeArticles(){
		$articleFeeCategoryId = Application::getConfigurationValue("artikelkategori_intrade");
		$parent = Article::loadById($articleFeeCategoryId);
		$articles = $parent->loadAllChildren("entrance_fee");
		
		$indexedByIdArticles = array();
		foreach($articles as $article){
			$indexedByIdArticles[$article->id] = array("label" => $article->name." ( ".  number_format($article->price,2)."kr )", "value" => $article->id);
		}
		return $indexedByIdArticles;
	}

	protected function handleListRequest($action, $target, $identifiers) {
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		if(isset($_GET) && fromGet("first_name") || fromGet("last_name") || fromGet("identification")){
			$searchRespository = new SearchRepository();
			$people = $searchRespository->searchPerson(fromGet("first_name"), fromGet("last_name"), fromGet("identification"));
			$model->setPersonList($people);
		}
		$model->setTitle("Ankomstregistrering");
		$view->setModel($model);
		return new AdminMaster($view);
	}
	
}

?>
