<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class SleepingSlotBookingsController extends AdminController{
	

	
	public function userHasPermisson($action) {
		if($action == CONTROLLER_ACTION_DELETE){
			$user = UserAccessor::getCurrentUser();
			return $user->userHasAccess(UserLevel::WORKER);
		}
		else{
			return parent::userHasPermisson($action);
		}
	}
	
	/**
	 * 
	 * @param int $id
	 * @return \JsonView
	 */
	function handleDeleteRequest($action,$target,$identifiers){
	   $model = $this->createJsonModel();
	   $view = $this->createJsonView();
	   \Repository\AccommodationResourceRepository::delete($identifiers["default"]);
	   $model->setStatus(JsonModel::STATUS_OK);
	   $view->setModel($model);
	   return $view;
   }
   
	function handleAction($action,$target,$identifiers){
		if($this->isJsonRequest()){
			if(IS_DELETE_REQUEST){
				return $this->handleDeleteRequest($action,$target,$identifiers);
			}else{
				parent::handleJsonRequest($action, $target, $identifiers);
			}
		}else{
			parent::HandleAction($action, $target, $identifiers);
		}
	}

	
	
	function handleIndexRequest($action,$target,$identifiers){
		die("index request not implemented");
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
	}
	
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		die("no view directory specified");
	}	
	
	function getModelDirectory(){
		die("no model directory specified");
	}
	
}

?>