<?php 

	use \Repository\ResourceRepository;

	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	class BookingController extends AngularAdminController{
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
		require_once("lib/Models/SimpleJsonModel.php");	
		require_once("lib/Views/DoctrineJsonView.php");
		require_once("lib/Views/JsonView.php");

		parent::__construct($internalLinkBuilder);
	}	
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		die("no view directory specified");
	}	
	
	function getModelDirectory(){
		die("no model directory specified");
	}
	
	public function HandleServiceRequest($serviceName) {
		if($serviceName=="getBookingsByResource"){
			if(fromGet('resource_id',false)){
				$model = parent::createJsonModel();
				$resourceBookings = $this->getResourceBookings(fromGet('resource_id',false));
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage("OK");
				$model->setData($resourceBookings);
				$view = parent::createDoctrineJsonView();
				$view->setModel($model);
				return $view;
			}
		}
		if($serviceName=="getScheduleData"){
			$model = new  JsonModel();
			$view = new DoctrineJsonView();
			$model->setData(array(
				'publicTimespans' => \Repository\TimespanRepository::getPublicScheduleTimespans(),
				'resources' => \Repository\ResourceRepository::getScheduleData()
			));
			$view->setModel($model);
			return $view;
		}
		return parent::HandleServiceRequest($serviceName);
	}
	
	public function handleGet($target, $identifiers, $returnAsJson = false) {
		parent::handleGet($target, $identifiers, $returnAsJson);
	}	
	
	public function getResourceBookings($resourceId){
		return ResourceRepository::getEventOccationBookings($resourceId);
	}

	function handleIndexRequest($action,$target,$identifiers){
		die("index request not implemented");
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
	}
	
}

?>