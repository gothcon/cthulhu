<?php 
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	require_once("lib/Models/SimpleJsonModel.php");

    use \Doctrine\ORM\Query;
        
	class SignupController extends AdminController{

		public function __construct($internalLinkBuilder) {
				require_once("lib/Controllers/AdminControllers/SignupSlotController.php");
				parent::__construct($internalLinkBuilder);
		}

		public static function getControllerName(){
				return "anmalning";
		}

		function getDefaultAction(){
				return CONTROLLER_ACTION_LISTING;
		}

		function getViewDirectory(){
				return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Signup/";
		}

		function getModelDirectory(){
				return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Signup";
		}

		public function userHasPermisson($action) {
				if($action == CONTROLLER_ACTION_DELETE){
						$user = UserAccessor::getCurrentUser();
						return $user->userHasAccess(UserLevel::WORKER);
				}
				else{
						return parent::userHasPermisson($action);
				}
		}

		function handleAction($action,$target,$identifiers){
			return $this->handleJsonRequest($action,$target,$identifiers);
		}

		function handleJsonRequest($action,$target,$identifiers){
			$model = $this->createJsonModel();
			$view = $this->createJsonView();
			$em = \GothConEntityManager::getInstance();

			if($_SERVER["REQUEST_METHOD"] == "GET"){
				if(isset($_GET["person_id"]) && $_GET["person_id"] > 0){
					$signups = \Repository\SignupRepository::getByPerson($_GET["person_id"]);
					$model = new SimpleJsonModel();
					$model->setData($signups);
				}
				else if(isset($_GET["group_id"]) && $_GET["group_id"] > 0){
					$signups = \Repository\SignupRepository::getByGroup($_GET["group_id"]);
					$model = new SimpleJsonModel();
					$model->setData($signups);
				}
				else{
					return parent::handleJsonRequest($action, $target, $identifiers);
				}
			}
			else if($_SERVER["REQUEST_METHOD"] == "POST" || $_SERVER["REQUEST_METHOD"] == "PUT"){

				if(isset($_POST['signup_type'])){
					switch($_POST['signup_type']){
						// THIS STIL USES THE OLD REPOSITORIES
						case "group":
							$groupId = $_POST['entityIDs'][0];
							$signupSlotId = $_POST['signup_slot_id'];
							$signup = Repository\SignupRepository::createAndSaveGroupSignup($groupId, $signupSlotId);
							$signups[] = Signup::loadById($signup->getId());

							$data = array("signups" => static::toJsonObject($signups));
							if(count($signups) > 0){
								$signup_slot = SignupSlot::loadById($signups[0]->signup_slot_id);
								$data["signup_slot"] =  $signup_slot->getStandardClone();
							}
							$model->setMessage(Translator::translate("signup/saveSuccessfullMessage"));
							$model->setData($data);
							break;
						case "people" :
							// THIS STIL USES THE OLD REPOSITORIES
							$personId = $_POST['entityIDs'][0];
							$signupSlotId = $_POST['signup_slot_id'];
							$signup = Repository\SignupRepository::createAndSavePersonalSignup($personId, $signupSlotId);
							$signups[] = Signup::loadById($signup->getId());

							$data = array("signups" => static::toJsonObject($signups));
							if(count($signups) > 0){
								$signup_slot = SignupSlot::loadById($signups[0]->signup_slot_id);
								$data["signup_slot"] =  $signup_slot->getStandardClone();
							}
							$model->setMessage(Translator::translate("signup/saveSuccessfullMessage"));
							$model->setData($data);
					}
				}else{
					if(isset($_GET["person_id"])){
						$personId = $_GET["person_id"];
						$jsonData = Controller::getPostedJsonObject();
						$signupSlotId = $jsonData->signupSlotId;
						$signup = Repository\SignupRepository::createAndSavePersonalSignup($personId, $signupSlotId);
						$signup = Repository\SignupRepository::getByPerson($personId, $signup->getId());
						$model->setData($signup[0]);
					}else if(isset($_GET["group_id"])){
						$groupId = $_GET["group_id"];
						$jsonData = Controller::getPostedJsonObject();
						$signupSlotId = $jsonData->signupSlotId;
						$signup = Repository\SignupRepository::createAndSaveGroupSignup($groupId, $signupSlotId);
						$signup = Repository\SignupRepository::getByGroup($groupId, $signup->getId());
						$model->setData($signup[0]);
					}
				}
			}
			else if($_SERVER["REQUEST_METHOD"] == "DELETE"){
				\Repository\SignupRepository::deleteById($identifiers['default']);
			}
			else{
				return parent::handleJsonRequest($action, $target, $identifiers);
			}
			$view->setModel($model);
			return $view;
		}

		static function createSignup($identifier,$type){
				$signups = CreateSignups(array($identifier));
				return $signups[0];
		}

		static function createSignups($identifiers,$type){
				$signups = array();
				foreach($identifiers as $entityID){
						$signup = new Signup();
						Controller::populateFromPost($signup);
						if($type == "people"){
								$signup->person_id = $entityID;
						}else{
								$signup->group_id = $entityID;
						}
						$signup->save();
						$signups[] = Signup::loadById($signup->id);
				}
				return $signups;
		}

		static function populateFromPost(&$entity,$prefix=""){
				parent::populateFromPost($entity,$prefix);
				$entity->is_approved = fromPost("is_approved",0);
				$entity->is_paid = fromPost("is_paid",0);
		}
	
	}