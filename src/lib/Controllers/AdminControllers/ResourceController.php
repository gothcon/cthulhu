<?php
	
	use \Repository\EventRepository;
	use \Repository\ResourceRepository;
	use \DoctrineEntitySerializer;
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
	require_once("lib/Models/SimpleJsonModel.php");	
	require_once("lib/Views/DoctrineJsonView.php");
	require_once("lib/Views/JsonView.php");

	
	
	class ResourceController extends AdminController{
		
		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/TimespanController.php");
			require_once("lib/Controllers/AdminControllers/ResourceTypeController.php");
			require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		function getDefaultAction(){
                    return CONTROLLER_ACTION_INDEX;
		}
		
		function getViewDirectory(){
                    return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Resource/";
		}
		
		function getModelDirectory(){
                    return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Resource/";
		}
	
		public static function getControllerName(){
			return "resource";
		}		

		protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
			if(count($segments) >= 2 && is_numeric($segments[0])){
				$identifiers["default"] = $segments[0];
				$action = $segments[1];
				$targetType = "default";
				return true;
			}else if( count($segments) == 1 && $segments[0] == CONTROLLER_ACTION_AVAILABLE_RESOURCE_TYPES){
				$action = $segments[0];
				$targetType = "default";
				return true;
			}
			else if( count($segments) == 1 && $segments[0] == "schedule"){
				$action = $segments[0];
				$targetType = "default";
				return true;
			}
			return parent::parseUrl($segments, $action, $targetType, $identifiers);
		}
		
		function handleDeleteResourceBookingAction($action,$target,$identifiers){
			if(JSONREQUEST){
				$booking_id = fromGet("booking_id",false);
				if($booking_id && $booking_id > 0 && is_numeric($booking_id)){
					Booking::deleteById($booking_id);
					$view = $this->createJsonView();
					$model = $this->createJsonModel();
					$model->setMessage(Translator::translate("resourceBooking/deleteMessage"));
					$model->setStatus(JsonModel::STATUS_OK);
					$view->setModel($model);
					return $view;
				}
			}else{
				$booking_id = fromGet("booking_id",false);
				if($booking_id && $booking_id > 0 && is_numeric($booking_id)){
					Booking::deleteById($booking_id);
					$view = new RedirectView($this->internalLink("resource", $identifiers["default"]."#".Html::CreateHash(Translator::translate("resources/bookings")),false));
					return $view;
				}
			}
		}
		
		function HandleAction($action, $target, $identifiers) {
			if(JSONREQUEST){
				return $this->handleJsonRequest($action,$target,$identifiers);
			}else{
				if(isset($_GET["format"]) && $_GET["format"] == "schedule"){
					return $this->handleScheduleRequest($action,$target,$identifiers);
				}
				return $this->handleHtmlRequest($action, $target, $identifiers);
			}
		}
		
		function handleScheduleRequest($action,$target,$identifiers){
			$view = $this->getScheduleView();
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			$model = $this->getScheduleModel();
			$model->SetMasterTimespan(TimespanController::getMasterTimespan());
			$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
			$model->SetResourceSchedules(Booking::loadResourceSchedules());
			$model->SetTitle(Translator::translate("resources/overview"));
			$view->setModel($model);
			return new AngularAdminMaster($view);
		}
		
		function handleSaveRequest($action,$target,$identifiers){
			$resource = null;
			if(isset($identifiers["default"]) && is_numeric($identifiers["default"])){
				$resource = ResourceRepository::get($identifiers["default"]);
			}
			if($resource == null){
				$resource = new \Entities\Resource();
			}
			
			$resourceData = $this->getPostedJsonObject();

			if(isset($resourceData->description)){
				$resource->setDescription($resourceData->description);
			}
			
			$resource->setResourceType(ResourceRepository::getResourceType($resourceData->resourceType));
			$resource->getResourceType();
			$resource->setName($resourceData->name);
			$resource->setAvailable(isset($resourceData->isAvailable) ? $resourceData->isAvailable : false);
			
			$sleepingResource = $resource->getSleepingResource();
			
			if($sleepingResource == null){
				$sleepingResource = new \Entities\SleepingResource();
				$sleepingResource->setResource($resource);
				$resource->setSleepingResource($sleepingResource);
			}
			
			if(isset($resourceData->noOfSleepingSlots) && is_numeric($resourceData->noOfSleepingSlots)){
				$sleepingResource->setNoOfSlots($resourceData->noOfSleepingSlots);
			}else{
				$sleepingResource->setNoOfSlots(0);
			}
			
			$sleepingResource->setIsAvailable($resourceData->sleepingIsAvailable);
			
			\Repository\ResourceRepository::save($resource);
			
			return $this->handleGetJsonRequest($resource->getId());
		}
		
		function getResource($id){
			$resource = ResourceRepository::get($id);
			$sleepingResource = $resource->getSleepingResource();
			if(!$sleepingResource){
				$sleepingResource = new \Entities\SleepingResource();
				$sleepingResource->setResource($resource);
				$sleepingResource->setIsAvailable(false);
				$sleepingResource->setNoOfSlots(0);
				ResourceRepository::save($sleepingResource);
				$resource->setSleepingResource($sleepingResource);
			}
			return $resource;
		}
		
		function getSleepingEventOccations($resourceId){
			return ResourceRepository::getSleepingOccations(
			Application::getConfigurationValue("sovplatsarrangemang"),$resourceId);
		}

		function handleUpdateSleepingOccations($action,$target,$identifiers){
			$resourceId = $identifiers["default"];
			$newSelectionData = $this->getPostedJsonObject();
			$sleepingEventId = Application::getConfigurationValue("sovplatsarrangemang");
			ResourceRepository::setSleepingOccations($sleepingEventId, $resourceId, $newSelectionData);
			$model = new JsonModel();
			$view = new JsonView();
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * @param int $resourceId
		 * @return \JsonView
		 */
		function handleGetJsonRequest($resourceId){
			$model = new SimpleJsonModel();
			$view = new JsonView();
			$resource = $this->getResource($resourceId);
			$sleepingEventOccations = $this->getSleepingEventOccations($resource->getId());
			$model->setData(
				array('resource' => \DoctrineEntitySerializer::Serialize($resource,4), 'sleepingEventOccations' => $sleepingEventOccations));
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * @return \JsonView
		 */
		function handleGetJsonListRequest(){
			$model = new SimpleJsonModel();
			$view = new JsonView();
			$model->setData(\Repository\ResourceRepository::all());
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * 
		 * @return JsonView
		 */
		function handleGetJsonResourceTypeListRequest(){
			$model = new SimpleJsonModel();
			$view = new JsonView();
			$model->setData(\Repository\ResourceRepository::types());
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * 
		 * @return JsonView
		 */
		function handleGetResourceScheduleRequest($action, $target, $identifiers){
			$model = new SimpleJsonModel();
			$view = new DoctrineJsonView();
			
			$data = array(
				"bookings" => ResourceRepository::getScheduleDataForResource($identifiers['default']),
				"publicTimespans" => Repository\TimespanRepository::getPublicScheduleTimespans());

			$model->setData($data);
			$view->setModel($model);
			return $view;
		}
		
		
		/**
		 * 
		 * @param int $id
		 * @return \JsonView
		 */
		function handleDeleteResourceRequest($id){
			if(!UserAccessor::currentUserHasAccess(UserLevel::STAFF)){
					header('HTTP/1.1 403 Forbidden');
					print 'Du har inte rättighet att ta bort resurser';
					exit;
			}
			$model = new SimpleJsonModel();
			$view = new JsonView();
			$model->setData(ResourceRepository::delete($id));
			$view->setModel($model);
			return $view;
		}
		

		function handleGetBookingsRequest($action,$target,$identifiers){
			$view = new JsonView();
			$model = new SimpleJsonModel();
			$model->setData(ResourceRepository::getBookings($identifiers["default"]));
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * 
		 * @param string $action
		 * @param string $target
		 * @param array $identifiers
		 * @return View
		 */
		function handleJsonRequest($action, $target, $identifiers) {
			if(IS_GET_REQUEST){
				if($action == CONTROLLER_ACTION_AVAILABLE_RESOURCE_TYPES)
				{
					return $this->handleGetJsonResourceTypeListRequest();
				}
				else if($action == CONTROLLER_TARGET_BOOKINGS)
				{
					return $this->handleGetResourceScheduleRequest($action, $target, $identifiers);
				}
				else
				{
					if(isset($identifiers["default"])  && $identifiers["default"] > 0){
						return $this->handleGetJsonRequest($identifiers['default']);
					}
					else{
						return $this->handleGetJsonListRequest();
					}
				}
			}
			else if(IS_POST_REQUEST){
				if($action == CONTROLLER_TARGET_ACCOMMODATION){
					return $this->handleAddAccommodations($action,$target,$identifiers);
				}
				else if($action == CONTROLLER_ACTION_SAVE_SLEEPING_OCCATIONS){
					return $this->handleUpdateSleepingOccations($action, $target, $identifiers);
				}
				else{ 
					return $this->handleSaveRequest($action, $target, $identifiers);
				}
			}
			else if(IS_DELETE_REQUEST){
				if($action == CONTROLLER_TARGET_ACCOMMODATION){
					return $this->handleDeleteAccommodationRequest($action,$target,$identifiers);
				}else{
					return $this->handleDeleteResourceRequest((int)$identifiers["default"]);
				}
			}

			return parent::handleJsonRequest($action, $target, $identifiers);
		}
		
		function handleHtmlRequest($action, $target, $identifiers){
			$view = $this->createIndexView();
			$model = $this->createIndexModel();
			$model->SetTitle("Platser och resurser");
			$view->setModel($model);
			$masterView = new AngularAdminMaster($view);
			$masterView->setModel($model);
			return $masterView;
		}
		
		function handleAddAccommodations($action, $target, $identifiers){
			$model = new JsonModel();
			$view = new JsonView();
			$model->setData(\Repository\AccommodationResourceRepository::addAccommodations($identifiers['default'],$this->getPostedJsonObject()));
			$view->setModel($model);
			return $view;
		}
		
		public function getScheduleModel(){
			require_once($this->getModelDirectory() . "ScheduleModel.php");
			return new ScheduleModel();
		}
		
		public function getScheduleView(){
			require_once($this->getViewDirectory() . "Schedule.php");
			return new ScheduleView();
		}
		
		
	}