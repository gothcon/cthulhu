<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	class ArticleController extends AdminController{
		
		public function __construct($internalLinkBuilder) {
			require_once("lib/Views/AdminViews/Master/AdminMaster.php");
			require_once("lib/SessionAccessors/CartAccessor.php");
			parent::__construct($internalLinkBuilder);
		}
		
		static function getControllerName(){
			return "article";
		}
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Article/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Article/";
		}
		
		
		public function createCategoryView(){
			require_once($this->getViewDirectory() . "Category.php");
			$view = new CategoryView(); 
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		protected function getCurrentCartOrNew(){
			
			$currentCustomerId = CartAccessor::getCurrentCustomer()->id;
			$cart = Order::loadOpen($currentCustomerId);
			if(!$cart){
				
				if($currentCustomerId == 0){
					return false;
				}
				$cart = new Order;
				$cart->person_id = $currentCustomerId;
				$cart->save();
			}
			return $cart;
		}
		
		 function handleAction($action,$target,$identifiers){
			
			if($action == CONTROLLER_ACTION_DELETE && $target != "cart"){
				$article = Article::loadById($identifiers["default"]);
				$parentId = $article->p;
				Article::deleteById($identifiers["default"]);
				$view = new RedirectView($this->internalLink(static::getControllerName(), $article->p != 0 ? "?p=".$article->p : "" ));
			}
			else{ 
				$cart = $this->getCurrentCartOrNew();
			
				if(!$cart){
					CartAccessor::clearCart();
					return parent::HandleAction($action, $target, $identifiers);
				}
				CartAccessor::setCurrentCart($cart);
				switch($action){
					case CONTROLLER_ACTION_ADD_TO_CART:
						$orderRow = new OrderRow();
						$article = Article::loadById($identifiers['default']);
						$orderRow->article_id = $article->id;
						$orderRow->name = $article->name;
						$orderRow->price_per_each = $article->price;
						$orderRow->count = fromPost("antal");
						MessageAccessor::addInformationMessage("<em>".$orderRow->count. " x " . $orderRow->name."</em> ". Translator::translate("articles/hasBeenAddedToCart"));
						$cart->addOrderRow($orderRow);
						$cart->save();
						CartAccessor::setCurrentCart($cart);
						$p = fromGet("p",false);
						if(JSONREQUEST){
							$view = $this->handleListRequest($action, $target, $identifiers);
						}else{
							$view = new RedirectView($this->internalLink(static::getControllerName(),  $p ? "?p=".$p : "" ));
						}
					break;
					case CONTROLLER_ACTION_REMOVE_FROM_CART:
						$cart = $this->getCurrentCartOrNew();
						$cart->deleteOrderRow($identifiers["default"]);
						$cart->save();
						CartAccessor::setCurrentCart($cart);
						$p = fromGet("p",false);
						$view = new RedirectView($this->internalLink(static::getControllerName(),  $p ? "?p=".$p : "" ));
					break;
					case CONTROLLER_ACTION_DELETE:
						$cart = $this->getCurrentCartOrNew();
						$cart->deleteOrderRow($identifiers['cart']);
						$cart->save();
						CartAccessor::setCurrentCart($cart);
						break;
					default :
						$view = parent::handleAction($action,$target,$identifiers);
				}
			}
			return $view;
		}	
		
		function handleListRequest($action,$target,$identifiers){
			
			$p = fromGet("p",null); 
			if(is_numeric($p)){
				$currentParent = Article::loadById($p);
			}else{
				$currentParent = Article::loadRootNode();
			}
			
			$path = $currentParent->loadPath();
			$articlesAndCategories = $currentParent->loadChildren();

			if($currentParent->type != "category"){
				unset($path[count($path)-1]);
			}
			
			$articles = array();
			$categories = array();
			foreach($articlesAndCategories as $articleOrCategory){
				switch($articleOrCategory->type){
					case "category":
						$categories[] = $articleOrCategory;
					break;
					case "article":
					case "slot_fee":
					case "entrance_fee":
						$articles[] = $articleOrCategory;
					break;
				}
			}

			$view = parent::handleListRequest($action,$target,$identifiers);
			$model = $view->getModel();
			$model->setTitle(Translator::translate("articles/articles"));
			$model->setCategories($categories);
			$model->setArticles($articles);
			$model->setArticlePath($path);
			$model->setArticle($currentParent);
			$model->setCurrentCustomer(CartAccessor::getCurrentCustomer());
			$model->setCurrentCart(CartAccessor::getCurrentCart());
			$view = new AdminMaster($view);
			$view->setModel($model);
			return $view;

		}
		
		function handleIndexRequest($action,$target,$identifiers){
			return $this->handleFormRequest($action, $target, $identifiers);
		}
	
		static function populateFromPost(&$article,$prefix=""){
			parent::populateFromPost($article,$prefix);
			parent::populateFromPost($article,$prefix.$article->type."_");
			if($article->image_id > 0){
				$image = Image::loadByIdWithoutContent($article->image_id);
			}else{
				$image = new Image();
			}
			$article->available_from_date = fromPost("available_from_date",$article->available_from_date);
			$article->available_from_time = fromPost("available_from_time",$article->available_from_time);
			$article->available_to_date = fromPost("available_to_date",$article->available_to_date);
			$article->available_to_time = fromPost("available_to_time",$article->available_to_time);
		}
	
		function handleFormRequest($action,$target,$identifiers){
			$view = parent::handleFormRequest($action,$target,$identifiers);
			$model = $view->getModel();
			if($action == CONTROLLER_ACTION_NEW){
				$article = new Article();
				$article->p =  fromGet('p');
				$model->setTitle(Translator::translate("articles/newArticle"));
			}else{
				$article = Article::loadById($identifiers['default']);
				
				$model->setTitle("Detaljer för " . $article->name . "(".$article->id.")");
			}
			if($this->isPostback()){
				static::populateFromPost($article);
				if(!is_numeric($article->image_id)){
					unset($article->image_id);
					unset($article->image);
				}
				$article->save();
				if($article->type == "category"){
					$view = new RedirectView($this->internalLink(static::getControllerName(),"?p=".$article->id));
				}else{
					$view = new RedirectView($this->internalLink(static::getControllerName(),"?p=".$article->p));
				}

				return $view;
			}
			
			if($action != CONTROLLER_ACTION_NEW){
				$model->setArticle($article);
				
				if($article->type == ARTICLE_TYPE_CATEGORY){
					$view = $this->createCategoryView();
					$stats = Order::getCategoryOrderingStatistics($article);
					$model->setArticleOrderingStatistics($stats);
					$view->setModel($model);
					
				}else{
					$stats = Order::getArticleOrderingStatistics(true,$article->id);
					$model->setArticleOrderingStatistics($stats);
				}
			}
			$model->setTypes(array(
				array("label" => Translator::translate("articles/articleTypes/article"),"value" => "article"),
				array("label" => Translator::translate("articles/articleTypes/category"),"value" => "category"),
				array("label" => Translator::translate("articles/articleTypes/slot_fee"), "value" => "slot_fee"),
				array("label" => Translator::translate("articles/articleTypes/entrance_fee"), "value" => "entrance_fee")
			
			));
			$view = new AdminMaster($view);
			$view->setModel($model);
			
			return $view;
		}
	
	}