<?php
use \lib\Views\AdminViews\UserStatistics\IndexView;
use \lib\Models\AdminModels\UserStatistics\IndexModel;
require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
require_once("lib/Controllers/AdminControllers/AdminController.php");
use \AngularAdminMaster;
class UserStatisticsController extends AdminController{
	
	/** @return bool **/
	public function userHasPermisson($action){
		$currentUser = UserAccessor::getCurrentUser();
		switch($action){
			case CONTROLLER_ACTION_DELETE:
			case CONTROLLER_ACTION_DELETE_BOOKING:
			case CONTROLLER_ACTION_DELETE_EVENT_OCCATION:
			case CONTROLLER_ACTION_DELETE_GROUP:
			case CONTROLLER_ACTION_DELETE_RESOURCE_BOOKING:
			case CONTROLLER_ACTION_DELETE_SIGNUP_SLOT:
					return $currentUser->userHasAccess(UserLevel::STAFF);
			case CONTROLLER_ACTION_DELETE_GROUPSIGNUP:
			case CONTROLLER_ACTION_DELETE_MEMBERSHIP:
			case CONTROLLER_ACTION_DELETE_SIGNUP:
					return $currentUser->userHasAccess(UserLevel::WORKER);
			default :
					return true;
		}
	}
	
	function getDefaultAction(){
		return CONTROLLER_ACTION_INDEX;
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/UserStatistics";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/UserStatistics";
	}
	
	protected function handleIndexRequest($action, $target, $identifiers) {
		$model = $this->createIndexModel();
		$view = $this->createIndexView();
		$loginCount = \Repository\UserRepository::GetUserLogonCount();
		
		$statistics = new \stdClass();
		$statistics->loginCount = $loginCount;
		$statistics->arrivalCount = \Repository\StatisticsRepository::GetArrivalCountByGender(); 
		$statistics->approximatedTotalArrivalCount = \Repository\StatisticsRepository::GetApproximatedTotalArrivalCount();
		$statistics->unarrivedCount = \Repository\StatisticsRepository::GetUnarrivedCount();
		$model->setStatistics($statistics);
		$model->SetTitle("Användarstatistik");
		$view->setModel($model);
		return new AngularAdminMaster($view);
	}
	
	/** @return \lib\Models\AdminModels\UserStatistics\IndexModel */
	function createIndexModel() {
		require_once($this->getModelDirectory()."/IndexModel.php");
		return new IndexModel();
	}
	
	/** @return \lib\Views\AdminViews\UserStatistics\IndexView */
	function createIndexView() {
		require_once($this->getViewDirectory()."/Index.php");
		$view = new IndexView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
}

?>