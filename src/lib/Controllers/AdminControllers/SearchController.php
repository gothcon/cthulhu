<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	require_once("lib/Database/MySQL/Repositories/SearchRepository.php");
	
	class SearchController extends AdminController{
		
		function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Search/";
		}
		
		function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Search";
		}
		
		public static function getControllerName(){
			return "sok";
		}
		
		function handleAction($action,$target,$identifiers){
			if($this->isJsonRequest()){
				$view = parent::handleJsonRequest($action,$target,$identifiers);
			}else{
				$view = parent::handleIndexRequest($action,$target,$identifiers);
			}
			
			$searchRepository = new SearchRepository();
			
			$model = $view->getModel();
			$q = fromRequest("q",false);
			if($q !== false){
				$r = fromRequest("r",null);
				$searchResult =	$searchRepository->doSearch($q, $r);
				if($this->isJsonRequest()){
					$model->setData($searchResult);
					$model->setStatus(JsonModel::STATUS_OK);
				}else{
					$result = $searchResult;
					$model->setSearchTerm($q);
					$model->setSearchResult($result);
				}
			}
			
			$view->setModel($model);
			
			if(!$this->isJsonRequest()){
				$model->setTitle("Söksidan");
				$view = new AdminMaster($view);
				$view->setModel($model);
			}
			return $view;
			
		}
		
	}
