<?php 
	require_once("lib/Controllers/AdminControllers/AngularAdminController.php");
	require_once("lib/Database/ListItems/ListItem_EventOccationBooking.php");
	class EventOccationBookingController extends AngularAdminController{
	
	public function __construct($internalLinkBuilder) {
		require_once("lib/Controllers/AdminControllers/TimespanController.php");
		parent::__construct($internalLinkBuilder);
	}	
		
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	function getViewDirectory(){
		die("no view directory specified");
	}	
	
	function getModelDirectory(){
		die("no model directory specified");
	}
	
	function loadBookings($sleepingEventId){
		$data = null;
		// load available resources
		if(fromGet("eventOccationId")){
				$eventOccation = EventOccation::loadById(fromGet("eventOccationId"));
				if($eventOccation->event_id == $sleepingEventId){
						$data = Booking::getAvailableResourcesForSleepingEventOccation(fromGet("eventId"));
				}
				$data = Booking::getAvailableResourcesForEventOccation(fromGet("eventOccationId"));
		}else if(fromGet("eventId")){
			if(fromGet("eventId") == $sleepingEventId){
					$data = Booking::getAvailableResourcesForSleepingEvent(fromGet("eventId"));
			}else{
					$data = Booking::getAvailableResourcesForEvent(fromGet("eventId"));
			}
		}
		return $data;
	}
        
	function handleGetResourceBookings($resourceId){
		$data = new stdClass();
		$data->resourceSchedule = Booking::loadResourceSchedules($resourceId);
		$data->masterTimespan = $this->toJsonObject(TimespanController::getMasterTimespan());
		return $data;
	}
        
	function createEventBookings($eventId,&$model){
		// Book this resource for all event occations
		$eventOccations = EventOccation::loadByEvent($eventId);
		// get the resources 
		$resourceList = fromPost("resource");
		$resourceSchedule = Event::loadEventResourceSchedules($eventId);

		$alreadyBookedOccations = array();
		foreach($resourceSchedule[0]->events[0]->event_occations as $occation){
			foreach($occation->booked_resources as $bookedResource){
				$alreadyBookedOccations[$occation->id] = $bookedResource->resource_id;
			}
		}

		foreach($resourceList as $resourceId){
			if($resourceId == "null" || is_null($resourceId)){
				continue;
			}
			foreach($eventOccations as $eventOccation){
				if(isset($alreadyBookedOccations[$eventOccation->id])){
					continue;
				}
				$booking = new EventOccationBooking();
				$booking->setResourceId($resourceId);
				$booking->setEventOccationId($eventOccation->id);
				$booking->save();
			}
		}
		$data = new stdClass();

		$eventResourceList = array();
		foreach($resourceSchedule[0]->events[0]->event_occations as $occation){
			foreach($occation->booked_resources as $booking){
				if(!isset($resourceList[$booking->resource_name])){
					$resourceList[$booking->resource_name] = array();
				}
				$eventOccationBooking = new ListItem_EventOccationBooking();
				$eventOccationBooking->resource_name = $booking->resource_name;
				$eventOccationBooking->booking_id = $booking->booking_id;
				$eventOccationBooking->starts_at = $occation->starts_at;
				$eventOccationBooking->ends_at = $occation->ends_at;
				$eventOccationBooking->event_occation_name = $eventOccationBooking->event_occation_name;
				$eventResourceList[$booking->resource_name][] = $eventOccationBooking;
			}
		}

		$data->resourceSchedule = $eventResourceList;
		$data->masterTimespan = $this->toJsonObject(TimespanController::getMasterTimespan());
		$model->setData($data);

	}
        
	function createEventOccationBookings($eventOccationId,&$model){
		$eventOccation = EventOccation::loadById($eventOccationId);
		$resourceList = fromPost("resource");
		foreach($resourceList as $id){
			if($id == null || $id == "null")
				continue;
			$booking = new EventOccationBooking();
			$booking->setResourceId($id);
			$booking->setEventOccationId($eventOccationId);
			$booking->save();

			if(fromPost("mainResourceId") && fromPost("mainResourceId") == $id){
				$eventOccation->main_booking_id = $booking->id;
				$eventOccation->save();
			}
		}

		$data = new stdClass();
		$resourceSchedule = Event::loadEventResourceSchedules($eventOccation->event_id);



		$eventResourceList = array();
		foreach($resourceSchedule[0]->events[0]->event_occations as $occation){
			foreach($occation->booked_resources as $booking){
				if(!isset($resourceList[$booking->resource_name]))
						$resourceList[$booking->resource_name] = array();
				$eventOccationBooking = new ListItem_EventOccationBooking();
				$eventOccationBooking->booking_id = $booking->booking_id;
				$eventOccationBooking->starts_at = $occation->starts_at;
				$eventOccationBooking->ends_at = $occation->ends_at;
				$eventOccationBooking->event_occation_name = $eventOccationBooking->event_occation_name;
				$eventResourceList[$booking->resource_name][] = $eventOccationBooking;
			}
		}


		$data->resourceSchedule = $eventResourceList;
		$data->masterTimespan = $this->toJsonObject(TimespanController::getMasterTimespan());
		$model->setData($data);
	}
        
	function createResourceBookings($resourceId,&$model){
		$eventOccationList = fromPost("eventOccation");
		foreach($eventOccationList as $eventOccationId){
			if($eventOccationId == "null" || is_null($eventOccationId)){
				continue;
			}
			$booking = new EventOccationBooking();
			$booking->setResourceId($resourceId);
			$booking->setEventOccationId($eventOccationId);
			$booking->save();
		}
		$data = new stdClass();
		$data->resourceSchedule = Booking::loadResourceSchedules($resourceId);
		$data->masterTimespan = $this->toJsonObject(TimespanController::getMasterTimespan());
		$model->setData($data);
	}
        
	function createResourceBooking($event_occation_id,$resource_id, &$model){
		$booking = new EventOccationBooking();
		$booking->event_occation_id = $event_occation_id;
		$booking->setResourceId($resource_id);
		if($booking->save()){
			$reloadedBooking = Booking::loadById($booking->getBooking()->id);
			$model->setData($reloadedBooking->getStandardClone());
			$model->setMessage(Translator::translate("resourceBooking/saveMessage"));
			$model->setStatus(JsonModel::STATUS_OK);
		}
		else{
			$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
			$model->setMessage("Not OK");
			$model->setData(null);
		}
	}
	
	function createBooking($eventOccationId,$resourceId){
		$eventOccation = GothConEntityManager::createEventOccationReference($eventOccationId);
		$resource = GothConEntityManager::createResourceReference($resourceId);
		$booking = new \Entities\Booking();
		$booking->setResource($resource);
		\Repository\GothConRepository::save($booking);
		$eventOccationBooking = new \Entities\EventOccationBooking();
		$eventOccationBooking->setBooking($booking);
		$eventOccationBooking->setEventOccation($eventOccation);
		\Repository\GothConRepository::save($eventOccationBooking);
		return \Repository\EventOccationBookingRepository::get($eventOccationBooking->getId());
	}
  	
	public function handleGet($target,$identifiers,$returnAsJson = false){
		$view = parent::handleGet($target,$identifiers,$returnAsJson);
		$model = $view->getModel();
		if($returnAsJson){
			$model->setData($this->handleGetResourceBookings(fromGet("resource_id")));
			$model->setStatus(JsonModel::STATUS_OK);
		}
		$view->setModel($model);
		return $view;
	}
	
	public function handlePost($target,$identifiers,$returnAsJson = false){
		$view = $this->createJsonView();
		$model = $this->createJsonModel();
		if(fromGet("multiple")){
			$eventId = fromGet("event_id");
			$eventOccationId = fromGet("event_occation_id");
			$resourceId = fromGet("resource_id");
			if($eventId){
				$this->createEventBookings($eventId, $model);
			}
			else if($eventOccationId){
				$this->createEventOccationBookings($eventOccationId, $model);
			}
			else if($resourceId){
				$this->createResourceBookings($resourceId, $model);
			}
		}
		else{
			$jsonData = $this->GetPostData();
			if($jsonData){
				$responseData = array();
				
				if(is_array($jsonData)){
					foreach($jsonData as $key => $booking){
						$responseData[] = $this->createBooking($booking->eventOccationId, $booking->resourceId);
					}
					$model->setData($responseData);
				}else{
					$model->setData("object");
				}
			}
			else{
				$event_occation_id = fromPost("event_occation_id",false);
				$resource_id = fromPost("resource_id",false);
				$this->createResourceBooking($event_occation_id, $resource_id, $model);
			}
		}
		$view->setModel($model);
		return $view;
	}

	public function handleDelete($target,$identifiers,$returnAsJson = false){
		$view = parent::createJsonView();
		$model = parent::createJsonModel();
		$booking_id = $identifiers['default'];
		$success = true;	
		if($booking_id && $booking_id > 0 && is_numeric($booking_id)){
			$success = Booking::deleteById($booking_id);
		}

		if($success){
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage(Translator::translate("resourceBooking/deleteMessage"));
				$model->setData(null);
		}
		else{
				$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
				$model->setMessage("Not OK");
				$model->setData(null);
		}
		$view->setModel($model);
		return $view;
	}

	function handleIndexRequest($action,$target,$identifiers){
		die("index request not implemented");
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("form request not implemented");
	}
	
	function handleListRequest($action,$target,$identifiers){
		die("list request not implemented");
	}
	
}

?>