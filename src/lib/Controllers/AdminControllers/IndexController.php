<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
	class IndexController extends AdminController{
		
		public function __construct($internalLinkBuilder) {
			require_once("lib/Controllers/AdminControllers/NewsController.php");
			parent::__construct($internalLinkBuilder);
		}
		
		function getDefaultAction(){
			return CONTROLLER_ACTION_INDEX;
		}
		
		protected function getViewDirectory() {
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Index";
		}

		protected function getModelDirectory() {
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Index";
		}
		
		public function HandleAction($action, $target, $identifiers) {
			
			if($target=="error"){
				switch($identifiers["error"]){
					case "404":
						return $this->handle404();
						break;
					case "401":
						return $this->handle401();
						break;
					case "400":
						return $this->handle400();
						break;
				}
			}
			return parent::HandleAction($action, $target, $identifiers);
		}
		
		public static function getControllerName(){
			return "index";
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			
			$view = parent::handleIndexRequest($action,$target,$identifiers);
			$model = $view->getModel();
			
			$model->setTitle(Translator::translate("index/startPage"));
			
			$model->setNewsArticles(NewsArticle::loadList());
		
			$view = new AngularAdminMaster($view);
			$view->setModel($model);
			return $view;
			
		}
	}



