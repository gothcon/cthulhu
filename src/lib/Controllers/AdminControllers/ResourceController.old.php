<?php
	require_once("lib/Controllers/AdminControllers/AdminController.php");
	
	class ResourceController extends AdminController{
		
		public function __construct($internalLinkBuilder) {
                    require_once("lib/Controllers/AdminControllers/TimespanController.php");
                    require_once("lib/Controllers/AdminControllers/ResourceTypeController.php");
                    require_once("lib/Controllers/AdminControllers/ResourceBookingController.php");
                    parent::__construct($internalLinkBuilder);
		}
		
		function getDefaultAction(){
                    return CONTROLLER_ACTION_LISTING;
		}
		
		function getViewDirectory(){
                    return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/AdminViews/Resource/";
		}
		
		function getModelDirectory(){
                    return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Resource/";
		}
	
		public static function getControllerName(){
			return "resource";
		}		
                
		public function getScheduleModel(){
			require_once($this->getModelDirectory() . "ScheduleModel.php");
			return new ScheduleModel();
		}
		
		public function getScheduleView(){
			require_once($this->getViewDirectory() . "Schedule.php");
			return new ScheduleView();
		}

                protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
                    if(count($segments) == 2 && is_numeric($segments[0])){
                        switch($segments[1]){
                            case CONTROLLER_ACTION_CREATE_RESOURCE_BOOKING:
                            case CONTROLLER_ACTION_DELETE_RESOURCE_BOOKING:
                            case CONTROLLER_ACTION_UPDATE_RESOURCE_BOOKING:
                                $identifiers["default"] = $segments[0];
                                $action = $segments[1];
                                $targetType = "default";
                                return true;
                        }

                    }
                    return parent::parseUrl($segments, $action, $targetType, $identifiers);
                }
		
                function handleDeleteResourceBookingAction($action,$target,$identifiers){
                    if(JSONREQUEST){
					$booking_id = fromGet("booking_id",false);
					if($booking_id && $booking_id > 0 && is_numeric($booking_id)){
						Booking::deleteById($booking_id);
						$view = $this->createJsonView();
						$model = $this->createJsonModel();
						$model->setMessage(Translator::translate("resourceBooking/deleteMessage"));
						$model->setStatus(JsonModel::STATUS_OK);
						$view->setModel($model);
						return $view;
					}
				}else{
					$booking_id = fromGet("booking_id",false);
					if($booking_id && $booking_id > 0 && is_numeric($booking_id)){
						Booking::deleteById($booking_id);
						$view = new RedirectView($this->internalLink("resource", $identifiers["default"]."#".Html::CreateHash(Translator::translate("resources/bookings")),false));
						return $view;
					}
				}
                
                }

                function handleCreateUpdateAction($action,$target,$identifiers){
                    $resource = new Resource();
                    $this->populateFromPost($resource);
                    $resource->id = $identifiers["default"];
                    static::saveResource($resource,fromPost("sleeping_is_possible"));
                    $view = $this->createJsonView();
                    $model = $this->createJsonModel();
                    $model->setData(static::toJsonObject($resource));
                    $model->setMessage(Translator::translate("resources/saveMessage"));
                    $model->setStatus(JsonModel::STATUS_OK);
                    $view->setModel($model);
                    return $view;
                }
                
		function HandleAction($action,$target,$identifiers){
                    switch($action){
                        case CONTROLLER_ACTION_DELETE_RESOURCE_BOOKING :
                            return $this->handleDeleteResourceBookingAction($action,$target,$identifiers);
                            break;
                        case CONTROLLER_ACTION_SAVE:
                        case CONTROLLER_ACTION_NEW:
                            if($this->isPostback() && $this->isJsonRequest() && $target == "default"){
                                return $this->handleCreateUpdateAction($action,$target,$identifiers);
                            }
                            break;
                        case CONTROLLER_ACTION_DELETE:
                            Resource::deleteById($identifiers['default']);
                            return new RedirectView($this->controllerPath());
                            break;
                        default:
                            if(fromGet("format") == "schedule"){
                                return $this->handleScheduleRequest($action, $target, $identifiers);
                            }
                    }
                    return parent::HandleAction($action,$target,$identifiers);
		}
		
		function handleScheduleRequest($action,$target,$identifiers){
                    $view = $this->getScheduleView();
                    $view->setInternalLinkBuilder($this->internalLinkBuilder);
                    $model = $this->getScheduleModel();
                    $model->SetMasterTimespan(TimespanController::getMasterTimespan());
                    $model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
                    $model->SetResourceSchedules(Booking::loadResourceSchedules());
                    $model->SetTitle(Translator::translate("resources/overview"));
                    $view->setModel($model);
                    return new AdminMaster($view);
		}

		function handleFormRequest($action,$target,$identifiers){
			return $this->handleIndexRequest($action,$target,$identifiers);
		}
		
		function handleJsonRequest($action, $target, $identifiers) {
			$view = parent::handleJsonRequest($action, $target, $identifiers);
			$model = $view->getModel();
			if(fromGet("eventId")){
				$model->setData($this->toJsonObject(Resource::loadEventBookingList(fromGet("eventId"))));
			}else if(fromGet("eventOccationId")){
				$model->setData($this->toJsonObject(Resource::loadEventOccationBookingList(fromGet("eventOccationId"))));
			}
			$view->setModel($model);
			return $view;
		}
		
		function handleIndexRequest($action,$target,$identifiers){
			$_view = parent::handleIndexRequest($action,$target,$identifiers);
			$model = $_view->getModel();
			$view = new AdminMaster($_view);
			$sleepingResource = false;
			if(isset($identifiers["default"]) && $identifiers["default"] > 0)
			{
					$model->SetMasterTimespan(TimespanController::getMasterTimespan());
					$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
					$model->SetResourceSchedules(Booking::loadResourceSchedules());


//					// try to load a sleeping resource
//					$sleepingResource = SleepingResource::loadByResourceId($identifiers["default"]);
//
//					if(is_null($sleepingResource)){
//							// no sleeping resource exist, load the resource instead
//							$resource = Resource::loadById($identifiers["default"]);
//					}else{
//							// else use the sleeping resource resource
//							$resource = $sleepingResource->getResource();
//							$sleepingOccations = SleepingResource::loadSleepingOccations(Application::getConfigurationValue("sovplatsarrangemang"),$resource->id);
//							$model->setSleepingOccations($sleepingOccations);
//					}
					
					
					$resource = Repository\ResourceRepository::get($identifiers["default"]);
					if($resource == null){
						$resource = new \Entities\Resource();
					}
			}
			else{
				// else, create a new Resource
				$resource = new \Entities\Resource();
			}
			
			if($this->isPostback()){
				if(fromPost("sleeping_resource_id")){
					// This is a sleeping resource (which means the resouce already exists because the sleeping resource is based on it
					// The resource was loaded previously
					$sleepingResource = SleepingResource::loadById(fromPost("sleeping_resource_id"));
					$sleepingResource->is_available = fromPost("is_available",1);
					$sleepingResource->no_of_slots = fromPost("maximum_signup_count");
					$sleepingResource->save();
					$this->UpdateSleepingOccations($sleepingResource->getResource());
					$view = new RedirectView($this->internalLink(static::getControllerName(),$resource->id."/#".Html::CreateHash(Translator::translate("resources/sleeping"))));
				}
				else{
					// This is not a sleeping resource

					$this->populateFromPost($resource);
					if(fromPost("sleeping_is_possible")){
						// sleeping is possible
						if(!$sleepingResource){
								// a sleeping resource already exists
								$sleepingResource = new SleepingResource();
						}
						$sleepingResource->is_available = fromPost("is_available",1);
						$sleepingResource->no_of_slots = fromPost("maximum_signup_count");
						$sleepingResource->setResource($resource);
						$sleepingResource->save();
					}else{
							// sleeping was possible
							if($sleepingResource){
								$sleepingResource->delete();
								$sleepingResource = null;
							}
							// save resource
							$resource->save();
					}

					$view = new RedirectView($this->internalLink(static::getControllerName(),$resource->id."/#detaljer"));
				}
			}
			else{
				if($resource->getId() > 0){
					$model->setTitle(sprintf(Translator::translate("resources/editPageTitle"),"{$resource->getId()}. {$resource->getName()}"));
					$model->setBookings(Booking::loadBookingsByResourceId($resource->getId()));
				}else{
					$model->setTitle(Translator::translate("resources/new"));
				}
			}

            $model->setResource($resource);	
			
			$model->setTypes(ResourceTypeController::getSelectDataSource());
			
			$view->setModel($model);
			return $view;
		}
		
		function UpdateSleepingOccations($resource){
			// booked sleeping occations for this resource:
			$sleepingOccations = SleepingResource::loadSleepingOccations(Application::getConfigurationValue("sovplatsarrangemang"),$resource->id);
			$updatedSleepingOccations = fromPost("sleeping_occations",array());

			foreach($sleepingOccations as $occation){
					if($occation->booking_id){
							// this occation has been booked previously
							if(in_array($occation->event_occation_id, $updatedSleepingOccations)){
									// this occation is in the updated list and we unset this so it wont be booked again
									unset($updatedSleepingOccations[$occation->event_occation_id]);
							}else{
									// this occation has been booked but is not int the updated list
									// delete it
									Booking::deleteById($occation->booking_id);
							}
					}
			}
			foreach($updatedSleepingOccations as $occationId){
					$booking = new EventOccationBooking();
					$booking->setResourceId($resource->id);
					$booking->setEventOccationId($occationId);
					$booking->save();
			}


		}
                
		function handleListRequest($action,$target,$identifiers){
			$view = parent::handleListRequest($action,$target,$identifiers);

			$model = $view->getModel();
			$model->setTitle(Translator::translate("resources/resources"));

			$sortParameters = static::getSortParameters(array("property" => "type","direction" => "asc", "offset" => "0", "count" => "-1"));
			$model->setSortParameters($sortParameters);
			$model->setResourceList(Resource::loadSorted($sortParameters["property"],$sortParameters["direction"]));
			$model->setSleepingStatistics(SleepingResource::loadSleepingStatistics());

			$master = new AdminMaster($view);
			$master->setModel($model);
			return $master;
		}
		
	}