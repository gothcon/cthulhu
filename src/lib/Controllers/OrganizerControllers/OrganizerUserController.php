<?php 
require_once("lib/Controllers/UserController.php");
require_once("lib/Views/OrganizerViews/Master/OrganizerMaster.php");

class OrganizerUserController extends UserController{
	
	public function __construct($internalLinkBuilder) {
		parent::__construct($internalLinkBuilder);
	}
        
	public function handleAction($action, $target, $identifiers) {
		if(JSONREQUEST){
        	return $this->handleJsonRequest ($action, $target, $identifiers);
		}
		else{
			$view = parent::handleAction($action, $target, $identifiers);
			if(is_a($view,"HtmlView")){
				return new OrganizerMaster($view);
			}
			return $view;
		}
	}
	
//	protected function handleJsonRequest($action, $target, $identifiers) {
//		$view = parent::handleJsonRequest($action, $target, $identifiers);
//		if($action != CONTROLLER_ACTION_GET_CURRENT_USER){
//			$model = $view->getModel();
//			$model->setStatus(JsonModel::STATUS_OK);
//		
//			$user = User::loadById($identifiers['default']);
//		
//			if(!$user){
//				$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
//				$model->setMessage(Translator::translate("user/databaseErrors/userNotFound"));
//			}
//			else{
//				$user->is_active	= fromPost("is_active");
//				$user->level		= fromPost("level");
//				$user->username		= fromPost("username");
//
//
//				$password			= fromPost("password");
//				$password_repeat	= fromPost("repeat_password");
//
//				// update the password
//				if(strlen($password)){
//					$user->setNewPassword($password, $password_repeat);
//				}  
//
//
//				if(!$user->save()){
//					$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
//					$validationErrors = $user->getValidationErrors();
//					foreach($validationErrors as $invalidPropertyName => $propertyError){
//						$message[] = new MessageEntry(Translator::translate("user/validationErrors/{$propertyError}"), MessageType::ERROR,$invalidPropertyName);
//					}
//					$model->setMessage($message);
//				}
//				else{
//					$model->setMessage(Translator::translate("user/saveSuccessfullMessage"));
//				}
//			}
//			$view->setModel($model);
//		}
//		return $view;
//	}
	
}



?>