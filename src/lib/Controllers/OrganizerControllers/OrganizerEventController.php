<?php

require_once("lib/Controllers/PublicControllers/PublicController.php");
require_once("lib/Views/OrganizerViews/Master/OrganizerMaster.php");

                                              

class OrganizerEventController extends PublicController{
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/OrganizerViews/Event/";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/OrganizerModels/Event";
	}
	
	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
	}
	
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		
		if(count($segments) == 2){
			if(is_numeric($segments[0])){
				switch($segments[1]){
					case CONTROLLER_ACTION_UPDATE_EVENT_OCCATION:
					case CONTROLLER_ACTION_DELETE_SIGNUP:
					case CONTROLLER_ACTION_UPDATE_SIGNUP:
						$identifiers["default"] = $segments[0];
						$action = $segments[1];
						$targetType = "default";
						return true;
				}
			}
		}
		
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	
	protected function handleListRequest($action, $target, $identifiers) {
		$personId = UserAccessor::getCurrentUser()->person_id;
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setTitle("Dina arrangemang");
		$model->setEventList(Organizer::loadPersonalSortedEventOrganizerList($personId,"name"));
		$masterView = new OrganizerMaster($view);
		return $masterView;
	}

	protected function handleDeleteSignup($action, $target, $identifiers) {
		$signup = Signup::loadById(fromGet("signup_id",0));
		
		if($signup && Organizer::personIsSlotOrganizer(UserAccessor::getCurrentUser()->person_id, $signup->signup_slot_id)){
			if($signup->delete()){
				MessageAccessor::addInformationMessage("Anmälningen har tagits bort");
			}
		}
		return new RedirectView($this->internalLink("event","{$identifiers["default"]}#Pass_och_anmalningar",false));
	}
	
	public function HandleAction($action, $target, $identifiers) {
		if(JSONREQUEST){
			return $this->handleJsonRequest($action, $target, $identifiers);
		}else{
			switch($action){
				case CONTROLLER_ACTION_UPDATE_SIGNUP:
					return $this->handleIndexRequest($action, $target, $identifiers);
					break;
				case CONTROLLER_ACTION_DELETE_SIGNUP:
					return $this->handleDeleteSignup($action, $target, $identifiers);
					break;
			default:	
				return parent::HandleAction($action, $target, $identifiers);
			}
		}
	}

	
	function handleIndexRequest($action, $target, $identifiers) {
		
		if($action == CONTROLLER_ACTION_UPDATE_SIGNUP){
			$signupId = fromGet("signup_id");
			$isApproved = fromPost("is_approved",0);
			$signup = Signup::loadById($signupId);
			$signup->is_approved = $isApproved;
			if(Organizer::personIsSlotOrganizer(UserAccessor::getCurrentUser()->person_id, $signup->signup_slot_id)){
				$signup->save();
			}
			return new RedirectView($this->internalLink("event","{$identifiers["default"]}#Pass_och_anmalningar",false));
		}
		
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();

		
		
		// event
		if (isset($identifiers["default"])) {
			$event = Event::loadById($identifiers["default"]);
			if($event && Organizer::personIsEventOrganizer(UserAccessor::getCurrentUser()->person_id, $event->id)){
				$model->setTitle(sprintf(Translator::translate("event/eventTitle"),$event->name));
				$model->setSignupSlots(SignupSlot::loadByEvent($event));
				$model->setSignups(Signup::loadByEvent($event));
				$model->setResourceBookings(EventOccationBooking::loadByEventId($event->id));
				$model->setOccations(EventOccation::loadByEvent($event));
				$model->setTimespanSelectList(static::createTimespanSelectData(Timespan::loadPublic()));
				$model->setSlottypeSelectList(static::createSlotTypeSelectData(SlotType::loadList()));
			}else{
				return new RedirectView($this->internalLink("event","",false));
			}
		} else {
			$event = new Event();
			$model->setTitle(Translator::translate("event/newEvent"));
		}
		$model->setEvent($event);
		$model->setEventTypeSelectList(static::createEventTypeSelectData(EventType::loadList()));

		$model->init("targetType", $target);
		$model->init("targetAction", $action);
		$model->init("targetIdentifiers", $identifiers);
		$view->setModel($model);
		$masterView = new OrganizerMaster($view);
		
		return $masterView;
	}
	
	
	static function createEventTypeSelectData($eventTypes){
		foreach ($eventTypes as $key => $value) {
			$eventTypes[$key] = array("label" => $value->name, "value" => $value->id);
		}
		return $eventTypes;
	}

	static function createTimespanSelectData($allTimespans){
		$selectList = array();
		foreach ($allTimespans as $timespan) {
			// if(!in_array($timespan->id, $occationTimespanIDs))
			$selectList[] = array("value" => $timespan->id, "label" => $timespan->__toString());
		}
		array_unshift($selectList, array("label" => Translator::translate("general/custom"), "value" => "0"));
		return $selectList;
	}
	
	static function createSlotTypeSelectData($slotTypes){
		$slotTypeSelectList = array();
		foreach ($slotTypes as $type) {
			$slotTypeSelectList[] = array("value" => $type->id, "label" => $type->name);
		}
		return $slotTypeSelectList;
	}
	
	protected function handleJsonRequest($action, $target, $identifiers) {
		
		if($action == CONTROLLER_ACTION_UPDATE_SIGNUP){
			$signupId = fromGet("signup_id");
			$isApproved = fromPost("is_approved") != 1 ? 0 : 1;
			$signup = Signup::loadById($signupId);
			
			if(Organizer::personIsSlotOrganizer(UserAccessor::getCurrentUser()->person_id, $signup->signup_slot_id)){
				$signup->is_approved = $isApproved;
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$success = $signup->save();
			}
			if($success){
				$model->setStatus(JsonModel::STATUS_OK);
				$model->setMessage("Anm&auml;lningsstatusen har uppdaterats");
				$model->setData(null);
			}
			else{
				$model->setStatus(JsonModel::STATUS_INVALID_REQUEST_DATA);
				$model->setMessage("Not OK");
				$model->setData(null);
			}
			
			$view->setModel($model);
			return $view;
			
		}
		return parent::handleJsonRequest($action, $target, $identifiers);
	}
	
}

?>
