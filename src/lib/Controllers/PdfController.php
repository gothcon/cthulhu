<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pdfGenerator
 *
 * @author Joakim
 */
require_once("lib/Controllers/Controller.php");
require_once("lib/Controllers/AdminControllers/TimespanController.php");


class PdfController extends Controller {
	
	public function HandleAction($action, $target, $identifiers) {
		$file = fromGet("file");
		switch($file){
			case "schema":
			case "schedule":
				return $this->handleScheduleRequest($action,$target,$identifiers);
				break;
			case "folder":
				return $this->handleFolderRequest($action, $target, $identifiers);
				break;
			case "overview":
				return $this->handleEventOverviewRequest($action, $target, $identifiers);
				break;
			case "resourceSchedules":
				return $this->handleResourceSchedulesRequest($action, $target, $identifiers);
				break;
			case "sleeperList":
				return $this->handleSleeperListRequest($action, $target, $identifiers);
				break;
			case "articleOrders":
				return $this->handleArticleOrdersRequest($action, $target, $identifiers);
				break;
			default:
				die("file not found");
		}
	}
	
	public function handleFolderRequest($action,$target,$identifiers){
		$eventTree = Event::loadFolderTree();
		$model = $this->createFolderModel();
		$model->setEventTree($eventTree);
		$model->setTitle("Folder");
		$view = $this->createFolderView();
		$view->setModel($model);
		return $view;
	}
	
	public function handleEventOverviewRequest($action,$target,$identifiers){
		$event_id = fromGet("event_id");
		$eventTree = Event::loadOverviewTree($event_id);
		$model = $this->createEventOverviewModel();
		$model->setEventTree($eventTree);
		$model->setTitle("Arrangemangssammanfattning");
		$view = $this->createEventOverviewView();
		$view->setModel($model);
		return $view;
	}
	
	public function handleScheduleRequest($action,$target,$identifiers){
		require_once("lib/Views/PDF/PdfScheduleView.php");
		require_once("lib/Models/PDF/PdfScheduleModel.php");
		$view = new PdfScheduleView();
		$model = new PdfScheduleModel();
		$model->SetMasterTimespan(TimespanController::getMasterTimespan());
		$model->SetVisibleTimespans(Timespan::loadVisibleInSchedule());
		$model->setEventTree(Event::loadScheduleTree());
		$model->setTitle("Schema");
		$view->setModel($model);
		return $view;
	}

	public function handleResourceSchedulesRequest($action,$target,$identifiers){
		require_once("lib/Views/PDF/PdfResourceScheduleView.php");
		require_once("lib/Models/PDF/PdfResourceScheduleModel.php");
		$view = new PdfResourceScheduleView();
		$model = new PdfResourceScheduleModel();
		$model->setTitle("Resursscheman");
		$model->SetMasterTimespan(TimespanController::getMasterTimespan());
		$model->SetResources(Booking::getPdfResourceList());
		$view->setModel($model);
		return $view;
	}

	public function handleSleeperListRequest($action,$target,$identifiers){
		require_once("lib/Views/PDF/PdfSleeperListView.php");
		require_once("lib/Models/PDF/PdfSleeperListModel.php");
		$view = new PdfSleeperListView();
		$model = new PdfSleeperListModel();
		$model->setTitle("Sovbokningslistor");
		$model->SetResources(SleepingSlotBooking::loadListForPdf());
		$view->setModel($model);
		return $view;
	}
	
	public function handleArticleOrdersRequest($action,$target,$identifiers){
		$articles = Order::getArticleOrderingStatistics(true);
		$model = $this->createOrderingModel();
		$model->setArticles($articles);
		$model->setTitle("Artikelbeställningar");
		$view = $this->createOrderingView();
		$view->setModel($model);
		return $view;
	}
	
	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Models/PDF/";
	}
	
	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/PDF/";
	}
	
	protected function createEventOverviewModel() {
		require_once($this->getModelDirectory()."PdfEventOverviewModel.php");
		return new PdfEventOverviewModel();
	}
	
	protected function createEventOverviewView(){
		require_once($this->getViewDirectory()."PdfEventOverView.php");
		return new PdfEventOverView;
	}

	protected function createOrderingModel() {
		require_once($this->getModelDirectory()."PdfOrderingModel.php");
		return new PdfOrderingModel();
	}
	
	protected function createOrderingView(){
		require_once($this->getViewDirectory()."PdfOrderingView.php");
		return new PdfOrderingView();
	}
	
	protected function createFolderModel() {
		require_once($this->getModelDirectory()."PdfFolderModel.php");
		return new PdfFolderModel();
	}
	
	protected function createFolderView(){
		require_once($this->getViewDirectory()."PdfFolderView.php");
		return new PdfFolderView;
	}
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments)){
			if($segments[0] == CONTROLLER_ACTION_SHOW_EVENT_OVERVIEW || $segments[0] == CONTROLLER_ACTION_SHOW_FOLDER){
				$action = $segments[0];
				return true;
			}
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
}