<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicGroupController
 *
 * @author Joakim
 */
require_once("lib/Controllers/PublicControllers/PublicController.php");
require_once("lib/Views/PublicViews/Master/PublicMaster.php");
require_once("lib/Controllers/AdminControllers/GroupController.php");


class PublicGroupController extends PublicController{
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) == 1 && $segments[0] == CONTROLLER_ACTION_JOIN_GROUP){
			$action = CONTROLLER_ACTION_JOIN_GROUP;
			return true;
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Group";
	}	
	
	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Group";
	}
	
	function getDefaultAction(){
		return CONTROLLER_ACTION_LISTING;
	}
	
	protected function createJoinModel(){
		require_once($this->getModelDirectory() . "/JoinModel.php");
		return new JoinModel();
	}

	protected function createJoinView(){
		require_once($this->getViewDirectory() . "/Join.php");
		$view = new JoinView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	public function HandleAction($action, $target, $identifiers) {
		switch($action){
			case CONTROLLER_ACTION_NEW:
				return $this->handleIndexRequest($action, $target, $identifiers);
				break;
			case CONTROLLER_ACTION_DELETE:
				$group_id = $identifiers["default"];
				$membership = GroupMembership::loadByGroupAndPerson($group_id, UserAccessor::getCurrentUser()->person_id);
				$group = Group::loadById($group_id);
				GroupMembership::deleteById($membership->id);
				$remainingGroupMembers = GroupMembership::loadByGroup($group_id);

				if(count($remainingGroupMembers) > 0){
					$group->leader_membership_id = $remainingGroupMembers[0]->id;
					$group->save();
				}else{
					Group::deleteById($group_id);
				}
				return new RedirectView(fromGet("returnUrl"));
				break;
			case CONTROLLER_ACTION_JOIN_GROUP:
				return $this->handleJoinRequest($action, $target, $identifiers);
				break;
			default:
				return parent::HandleAction($action, $target, $identifiers);
		}
	}
	
	protected function handleJoinRequest($action,$target,$identifiers){
		if(ISPOSTBACK){
			$membership = new GroupMembership();
			$membership->person_id = UserAccessor::getCurrentUser()->person_id;
			$membership->group_id = fromPost("group_id");

			$password = trim(fromPost("password"));
			$group = Group::loadById($membership->group_id);
			$encryptedPassword = User::encryptPassword($password, $group->salt);
			if($encryptedPassword != $group->password){
				MessageAccessor::addErrorMessage(Translator::translate("group/validationErrors/wrongPassword"));
			}
			else if($membership->save()){
				return new RedirectView($this->internalLink("group",$group_id));
			}else{
				static::addValidationErrorsToMessageHandler($membership, "group/validationErrors");
			}
		}
		$personId = UserAccessor::getCurrentUser()->person_id;
		$view = $this->createJoinView();
		$model = $this->createJoinModel();
		$model->setTitle("Gå med i lag");
		$model->setGroupList(static::createGroupSelectData(Group::loadAvailable($personId)));
		$view->setModel($model);
		return new PublicMaster($view);
	}
	
	protected function handleListRequest($action, $target, $identifiers) {
		$view = parent::handleListRequest($action,$target,$identifiers);
		$model = $view->getModel();
		$model->setGroupList(Group::loadList(UserAccessor::getCurrentUser()->person_id));
		$model->setTitle(Translator::translate("group/groupList"));
		$view = new PublicMaster($view);
		$view->setModel($model);
		return $view;	
	}
	
	function handleFormRequest($action, $target, $identifiers) {
		$this::handleIndexRequest($action, $target, $identifiers);
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		
		$addUserAsLeader = false;

		if(isset($identifiers["default"]) && isPositiveNumber($identifiers["default"])){
			$group = Group::loadById($identifiers["default"]);
		}else{
			$group = new Group();
			$addUserAsLeader = true;
		}

		$groupId = $identifiers["default"];
		$personId = UserAccessor::getCurrentUser()->person_id;

		$isLeader = false;
		$isMember = false;

		if(GroupMembership::personIsLeader($personId, $groupId) || $action == CONTROLLER_ACTION_NEW){
			$isLeader = true;
			$isMember = true;
		}else if(GroupMembership::personIsMember($personId, $groupId)){
			$isMember = true;
		}

		if(ISPOSTBACK){
			if($isLeader || $group->id == 0){
				static::populateFromPost($group);
				$validPassword = true;
				if(trim(fromPost("password")) !== ""){
					if(fromPost("password") == fromPost("repeat_password")){
						$group->salt = User::generateRandomString(12);
						$group->password = User::encryptPassword(fromPost("password"), $group->salt);
					}else{
						MessageAccessor::addErrorMessage(Translator::translate("group/validationErrors/passwordsDontMatch"),"password");
						$validPassword = false;
					}
				}
				if($validPassword){
					if($group->save()){
						if($addUserAsLeader){
							$groupMembership  = new Groupmembership();
							$groupMembership->person_id = UserAccessor::getCurrentUser()->person_id;
							$groupMembership->group_id = $group->id;
							$groupMembership->save();
							$group->leader_membership_id = $groupMembership->id;
							$group->save();
						}
						return new RedirectView($this->internalLink("group",$group->id));
					}else{
						static::addValidationErrorsToMessageHandler($group, "group/validationErrors");
					}
				}
			}
		}

		$view = parent::handleIndexRequest($action,$target,$identifiers);
		$model = $view->getModel();

		$model->setIsLeader($isLeader);
		$model->setIsMember($isMember);

		if($group->id > 0){
			$model->setTitle("{$group->name}");
		}else{
			$model->setTitle(Translator::translate("group/new"));
		}

		$model->setGroup($group);
		$model->setSignupList(Signup::loadAllFilteredByGroup($group));
		$model->setGroupMembers(GroupMembership::loadByGroup($group->id));
		$view->setModel($model);
		return new PublicMaster($view);
	}
	
	protected static function createGroupSelectData($list){
		$selectData = array();
		foreach($list as $group){
			$selectData[] = array("label" => $group->name, "value" => $group->id);
		}
		return $selectData;
	}	
		
}

?>
