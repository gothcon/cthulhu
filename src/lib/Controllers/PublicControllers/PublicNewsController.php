<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NewsController
 *
 * @author Joakim
 */
require_once("lib/Controllers/PublicControllers/PublicController.php");
class PublicNewsController extends PublicController {
	
	protected function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/News";
	}

	protected function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/News";
	}
	
	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
		
	}

	public static function getCurrentlyPulished(){
		$articles = NewsArticle::loadList();	
		$data = array();
		foreach($articles as $article){
			if(!$article->is_internal && $article->isPublished())
				$data[] = $article;
		}
		return $data;
	}
	 
	protected function handleListRequest($action, $target, $identifiers) {
		$view = parent::handleListRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->setNewsArticles(NewsArticle::loadPublicList());
		$model->setTitle("Nyheter");
		$view = new PublicMaster($view);
		return $view;
	}
	
	protected function handleIndexRequest($action, $target, $identifiers) {
		$view = parent::handleIndexRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$newsArticle = NewsArticle::loadById($identifiers["default"]);
		if($newsArticle && !$newsArticle->is_internal){
			$model->setNewsArticle($newsArticle);
			return new PublicMaster($view,true);
		}else{
			return new RedirectView($this->internalLink("news"));
		}
	}
	
}

?>
