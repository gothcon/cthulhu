<?php
require_once("lib/Controllers/Controller.php");


class FileController extends Controller{
	//put your code here
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		switch(count($segments)){
			case 0:
				return parent::parseUrl($segments, $action, $targetType, $identifiers);
				break;
			default:
				if(is_numeric($segments[0])){
					return parent::parseUrl($segments, $action, $targetType, $identifiers);
				}
				else{
					$action = CONTROLLER_ACTION_SHOW_IMAGE;
					$identifiers["default"] = implode("/",$segments);
					return true;
				}
				break;
		}
		parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	
	protected function getFormatDimensions($formatString){
		switch($formatString){
			case "articleThumb":
				return array(32,32);
			break;
			case "articleCategory":
				return array(134,134);
			break;
			default:
				if(preg_match("/(\d*)x(\d*)/", $formatString,$match)){
					return array($match[1],$match[2]);
				}
		}
		return false;
	}
	
	protected function createFilename($url,$w,$h){
		$info = pathinfo($url);
		return "{$info['dirname']}/{$info['filename']}_{$w}x{$h}.{$info['extension']}";
	}
	
	public function getPathToNoImagePlaceholder($dimensions = false){

		$originalImagePath = Application::getDocumentRoot()."/Resources/Images/MissingImage.png";
		
		if(!file_exists($originalImagePath)){
			return false;
		}
	
		if($dimensions){
			$w = $dimensions["w"];
			$h = $dimensions["h"];
			// get the path to the formatted image
			$formattedImagePath = $this->createFilename($originalImagePath, $w, $h);
			
			if(!file_exists($formattedImagePath)){
				// create dir
				if(!file_exists(dirname($formattedImagePath)))
					mkdir(dirname($formattedImagePath), "0777", true);

				// read original file info memory
				$fp = fopen($originalImagePath,'r');
				$originalImageContent = fread($fp,  filesize($originalImagePath));
				fclose($fp);

				// save resized file
				$fp = fopen($formattedImagePath,'w');
				fwrite($fp,static::resize($originalImageContent,$h,$w));
				fclose($fp);
			}
			return $formattedImagePath;
		}
		
		return $originalImagePath;

	}
	
	public function getPathToFile($imageUri){
		
		$imagePath = Application::getFilesPath()."/".$imageUri;
		
		if(!file_exists($imagePath)){
			
			$imageFormatPattern = "/(.+)_(\d+)x(\d+)(.*)/";
			
			$matches = array();

			// load the file from the uri
			
			if(preg_match($imageFormatPattern ,$imageUri ,$matches)){
				
				// this was a request for a resized image
				$w = $matches[2];
				$h = $matches[3];
				
				$filename = basename($imagePath);
				$strippedFilename = preg_replace($imageFormatPattern , "$1$4",$filename);
				
				$originalUri = str_replace($filename,$strippedFilename,$imageUri);
				$originalPath = Application::getFilesPath()."/".$originalUri;
				
				$image = Image::loadByUrl("/".$originalUri);
				if(!$image){
					return $this->getPathToNoImagePlaceholder(array("w" => $w, "h" => $h));
				}
				
				if(!file_exists($originalPath)){
					// write a copy of the image to the cache directory
					if(!file_exists(dirname($originalPath))){
						mkdir(dirname($imagePath), "0777", true);
					}
					$fp = fopen($originalPath,'w');
					fwrite($fp,$image->content);
					fclose($fp);
				}
				
				// write a resized copy of the file to the cache directory
				if(!file_exists(dirname($imagePath))){
					mkdir(dirname($imagePath), "0777", true);
				}
				$fp = fopen($imagePath,'w');
				fwrite($fp,static::resize($image->content,$w,$h));
				fclose($fp);
				
				
			}
			else{
				$image = Image::loadByUrl("/".$imageUri);
				if(!$image){
					return $this->getPathToNoImagePlaceholder();
				}
				// write a copy of the image to the cache directory
				if(!file_exists(dirname($imagePath))){
					mkdir(dirname($imagePath), "0777", true);
				}
				$fp = fopen($imagePath,'w');
				fwrite($fp,$image->content);
				fclose($fp);
			}
		}
		
		return $imagePath;
	}
	
	public function HandleRequest($url) {
		// $pathToRequestedFile = $this->getPathToRequestedFile($url, fromGet("format",false));
		$pathToRequestedFile = $this->getPathToFile($url);

		if($pathToRequestedFile){
			header('Expires: '.  date(DATE_RFC822,time() + 300) );
			header("Content-type: image/png"); 
			header('Content-Transfer-Encoding: binary'); 
			readfile($pathToRequestedFile);
		}else{
			$view = $this->create404View();
			$model = $this->create404Model();
			$view->setModel($model);
			$view->render();
		}
	}
	
	static function resize($source, $maxY = 64, $maxX = 64){
		
		// Load the sourcefile
		$sourceImageHandle = imagecreatefromstring($source);
		$width = imagesx($sourceImageHandle);
		$height = imagesy($sourceImageHandle);

		$scaling_factor = 1;
		$y_scaling_factor = ($maxY/$height);

		if($maxX != null){
			$x_scaling_factor = ($maxX / $width);
			$scaling_factor = $y_scaling_factor >  $x_scaling_factor ? $x_scaling_factor : $y_scaling_factor;
		}
		else{
			$scaling_factor = $y_scaling_factor;
		}

		$newheight = $height * $scaling_factor;
		$newwidth  = $width * $scaling_factor;

		//create an empty image handle
		$resizedImageHandle = imagecreatetruecolor($newwidth, $newheight);
		// $whiteBgColor = imagecolorallocate($resizedImageHandle, 255,255,255);
		// imagefill($resizedImageHandle,0,0,$whiteBgColor);
		imagecolortransparent($resizedImageHandle, imagecolorallocatealpha($resizedImageHandle, 0, 0, 0, 127));
		imagealphablending($resizedImageHandle, false);
		imagesavealpha($resizedImageHandle, true);
		// Resize
		imagecopyresampled($resizedImageHandle, $sourceImageHandle, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		// And save to file
		
		ob_start();
		imagepng($resizedImageHandle,NULL,9);
		$foo = ob_get_clean(); 
		return $foo;
	}
	
}

?>
