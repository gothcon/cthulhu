<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of publicOrderController
 *
 * @author Joakim
 */
require_once("lib/Controllers/PublicControllers/PublicController.php");
class PublicOrderController extends PublicController{
	//put your code here
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Order/";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Order";
	}
	
	public function handleCreateOrderFromCart($action, $target, $identifiers) {
		$order = PublicCartAccessor::getCurrentCart();
		$order->person_id = UserAccessor::getCurrentUser()->person_id;
		if($order->isEmpty()){
			die("empty order");
		}
		$order->status = Order::STATUS_SUBMITTED;
		$order->save();
		PublicCartAccessor::destroy();
		return new RedirectView($this->internalLink("person", UserAccessor::getCurrentUser()->person_id ."/#Bestallningar",false));
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$view = parent::handleIndexRequest($action,$target,$identifiers);
		$model = $view->getModel();
		$order = Order::loadById($identifiers['default']);
		if($this->isPostback()){
			static::populateFromPost($order); 
			$order->save();
			$order = Order::loadById($order->id);
		}
		$model->setTitle(sprintf(Translator::translate("order/orderTitle"),$order->id));

		
		$model->setOrderPayments(OrderPayment::getPaymentsForOrder($order->id));
		$model->setRemainingSum($order->total - OrderPayment::getTotalOrderPaymentSum($order->id));
		$model->setOrder($order);
		
		$view->setModel($model);
		return  new PublicMaster($view);
	}

}