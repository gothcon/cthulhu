<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CheckoutController
 *
 * @author Joakim
 */
require_once("lib/Controllers/PublicControllers/PublicController.php");
class CheckoutController extends PublicController{
	
	protected function getModelDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Checkout";
	}
	
	protected function getViewDirectory() {
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Checkout";
	}
	
	public function HandleStepOne($action,$target,$identifiers){
		$cart = PublicCartAccessor::getCurrentCart();
		
		if(POSTBACK){
			// update the notes
			$notes = fromRequest("note",false);
			if($notes){
				foreach($notes as $key => $note){
					$orderRow = $cart->getOrderRow($key);
					$orderRow->note = $note;
					$cart->setOrderRow($orderRow);
				}
			}
			$counts = fromRequest("count",false);
			if($counts){
				foreach($counts as $key => $count){
					if($count == 0){
						$cart->deleteOrderRow($key);
						continue;
					}
					$orderRow = $cart->getOrderRow($key);
					$orderRow->count = $count;
					$cart->setOrderRow($orderRow);
				}
			}
			$cart->note = fromPost("orderNote");
			MessageAccessor::addInformationMessage(Translator::translate("checkout/orderHasBeenUpdated"));
			return new RedirectView($this->internalLink("checkout","?step=1",false));
		}
		require_once($this->getModelDirectory()."/StepOneModel.php");
		require_once($this->getViewDirectory()."/StepOneView.php");
		$view = new StepOneView();
		$model = new StepOneModel();
		$model->SetTitle(Translator::translate("checkout/step1Title"));
		$model->SetCart($cart);
		$view->setModel($model);
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return new PublicMaster($view);

	}
	
	public function HandleStepTwo($action,$target,$identifiers){
		if(POSTBACK){
			$cart = PublicCartAccessor::getCurrentCart();
			$cart->preferred_payment_method = fromPost("payment_method",null);
			PublicCartAccessor::setCurrentCart($cart);
			return new RedirectView($this->internalLink("checkout","?step=final",false));
		}
		// välj betalningssätt, huruvida betalningskoder skall användas direkt 
		// eller inte
		require_once($this->getModelDirectory()."/StepTwoModel.php");
		require_once($this->getViewDirectory()."/StepTwoView.php");
		$view = new StepTwoView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = new StepTwoModel();
		$model->SetTitle(Translator::translate("checkout/step2Title"));
		$model->SetAvailablePaymentMethods(OrderPayment::GetPaymentMethods());
		$model->SetCart(PublicCartAccessor::getCurrentCart());
		$view->setModel($model);
		$view = new PublicMaster($view);
		return $view;
	}
	
	public static function GetPaymentMethods(){
		$array = OrderPayment::GetPaymentMethods();
		foreach($array as $key => $value){
			$array[$key][$label] = Translator::translate($array[$key][$label]);
		}
		return $array();
	}
	
        public function HandleDibsCancel($action,$target,$identifiers){
        	// välj betalningssätt, huruvida betalningskoder skall användas direkt 
		// eller inte
		require_once($this->getModelDirectory()."/StepTwoModel.php");
		require_once($this->getViewDirectory()."/StepTwoView.php");
		$view = new StepTwoView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = new StepTwoModel();
		$model->SetTitle(Translator::translate("checkout/step2Title"));
		$model->SetAvailablePaymentMethods(OrderPayment::GetPaymentMethods());
		$model->SetCart(PublicCartAccessor::getCurrentCart());
		$view->setModel($model);
		$view = new PublicMaster($view);
		return $view;
        }
        
	public function HandleSuccess($action,$target,$identifiers){
		require_once($this->getModelDirectory()."/OrderSuccessModel.php");
		require_once($this->getViewDirectory()."/OrderSuccessView.php");
		$orderId = fromGet("order_id");
		$order = Order::loadById($orderId);
		$view = new OrderSuccessView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = new OrderSuccessModel();
		$model->SetOrder($order);
		$model->SetTitle(Translator::translate("checkout/orderSubmitted"));
		$view->setModel($model);
		$view = new PublicMaster($view);
		return $view;
	}
	
	public function HandleFinalize($action,$target,$identifiers){
		if(POSTBACK){
			$cart = PublicCartAccessor::getCurrentCart();
			if(count($cart->getOrderRows()) == 0){
				return new RedirectView($this->internalLink("webshop"));
			}
			$cart->status = "submitted";
			$cart->has_been_submitted = 1;
			$cart->person_id = UserAccessor::getCurrentUser()->person_id;
			Order::createFromCart($cart);
			PublicCartAccessor::clearCart();
			return new RedirectView($this->internalLink("checkout","?step=success&order_id={$cart->id}",false));
		}
		// skapa order, koll mot eventuell betalningstjänst
		// visa resultat
		require_once($this->getModelDirectory()."/FinalStepModel.php");
		require_once($this->getViewDirectory()."/FinalStepView.php");
		$view = new FinalStepView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = new FinalStepModel();
		$model->SetCart(PublicCartAccessor::getCurrentCart());
		$model->SetTitle(Translator::translate("checkout/finalStepTitle"));
		$view->setModel($model);
		$view = new PublicMaster($view);
		return $view;
	}
	
	public function HandleAction($action, $target, $identifiers) {
		if(UserAccessor::getCurrentUser()->person_id == 0){
			$returnUrl = urlencode($this->internalLink("checkout","?step=1",false));
			return new RedirectView($this->internalLink("user",CONTROLLER_ACTION_LOGIN."?return_url={$returnUrl}"));
		}
		
		$cart = PublicCartAccessor::getCurrentCart();
		
		if($cart->removeUnavailableArticles() > 0)
			MessageAccessor::addWarningMessage(Translator::translate("checkout/unavailableArticlesInCart"));
		
		$step = fromGet("step");
		
                if($step == "success" )
			$step = "s";
		if($step == "final" || strtolower($this->requestedControllerName) == "dibssuccess")
			$step = "f";

                if(strtolower($this->requestedControllerName) == "dibscancel")
                    $step = "dibscancel";
                
                
		if(count($cart->getOrderRows()) == 0 && $step != "s"){
			return new RedirectView($this->internalLink("webshop"));
		}
		
		if($action == CONTROLLER_ACTION_REMOVE_FROM_CART){
			$cart->deleteOrderRow($identifiers["default"]);
			PublicCartAccessor::setCurrentCart($cart);
			return new RedirectView($this->internalLink("checkout","?step=1"));
		}
		
		
		switch ($step) {
                    case "dibscancel":
                            $view = $this->HandleDibsCancel($action,$target,$identifiers);
                        break;
                    case "2":
                            $view = $this->HandleStepTwo($action, $target, $identifiers);
                            break;
                    case "s":
                    case "success":
                            $view = $this->HandleSuccess($action, $target, $identifiers);
                            break;
                    case "f":
                    case "final":	
                            $view = $this->HandleFinalize($action, $target, $identifiers);
                            break;
                    case "1":
                    default:
                            $view = $this->HandleStepOne($action, $target, $identifiers);
                            break;
		}
		return $view;
	}
}

?>
