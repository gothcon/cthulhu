<?php

require_once("lib/Controllers/PublicControllers/PublicEventController.php");

class PublicEventTypeController extends PublicController{
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Eventtype/";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Eventtype";
	}

	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		
		$currentUser = UserAccessor::getCurrentUser();
		$model = $this->createListModel();
		$view = $this->createIndexView();
		$view->setModel($model);
		
		$model->setEventTypeList(EventType::loadNotEmptyList());
		$model->setCurrentEventType(EventType::loadById($identifiers['default']));
		$model->setEventTree(Event::loadPublicEventTree($identifiers['default'],0,$currentUser->person_id));
		
		
		
		if($currentUser->person_id){
			$model->setUserGroups(Group::loadByLeaderId($currentUser->person_id));
		}
		
		$model->setTitle("Arrangemang: ".$model->getCurrentEventType()->name);
		return new PublicMaster($view);
		
	}
	
	function handleFormRequest($action,$target,$identifiers){
		die("not implemented");
	}
	
	function handleListRequest($action,$target,$identifiers){
		$model = $this->createListModel();
		$model->setTitle("Arrangemang");
		$view = $this->createListView();
		$view->setModel($model);
		$model->setEventTypeList(EventType::loadNotEmptyList());
		$view = new PublicMaster($view);
		$view->setModel($model);
		
		return $view;
	}
	
}

?>
