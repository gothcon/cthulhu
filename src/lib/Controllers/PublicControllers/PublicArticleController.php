<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicArticleController
 *
 * @author Joakim
 */
require_once("lib/Controllers/PublicControllers/PublicController.php");
class PublicArticleController extends PublicController{
	
	static function getControllerName(){
		return "article";
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Article";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Article";
	}

	protected function getDefaultAction() {
		return CONTROLLER_ACTION_LISTING;
	}
	
	function handleAction($action, $target, $identifiers) {
			$cart = PublicCartAccessor::getCurrentCart();
			switch($action){
				case CONTROLLER_ACTION_ADD_TO_CART:	
					$orderRow = new OrderRow();
					$article = Article::loadById($identifiers['default']);
					$orderRow->article_id = $article->id;
					$orderRow->name = $article->name;
					$orderRow->price_per_each = $article->price;
					$orderRow->count = fromPost("antal");
					$cart->addOrderRow($orderRow);
					$cart->save();
					$p = fromGet("p",false);
					if($this->isJsonRequest()){
						$view = $this->handleListRequest($action, $target, $identifiers);
					}else{
						$view = new RedirectView($this->internalLink(static::getControllerName(),  $p ? "?p=".$p : "" ));
					}
					MessageAccessor::addInformationMessage("<em>".$orderRow->count. " x " . $orderRow->name."</em> ". Translator::translate("articles/hasBeenAddedToCart"));
					return $view;
				break;
				case CONTROLLER_ACTION_REMOVE_FROM_CART:
					$cart = PublicCartAccessor::getCurrentCart();
					$cart->deleteOrderRow($identifiers["default"]);
					$cart->save();
					$p = fromGet("p",false);
					if($this->isJsonRequest()){
						$view = $this->handleListRequest($action, $target, $identifiers);
					}else{
						$view = new RedirectView($this->internalLink(static::getControllerName(),  $p ? "?p=".$p : "" ));
					}
					return $view;
				break;
				default:
					$view = parent::handleAction($action,$target,$identifiers);
			}
			PublicCartAccessor::setCurrentCart($cart);
			return $view;
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		
		$view = parent::handleIndexRequest($action,$target,$identifiers);
		$model = $view->getModel();
		$view->setModel($model);
		$model->setTitle("Webbshoppen");
		return new PublicMaster($view);
	}

	function handleListRequest($action,$target,$identifiers){

		$currentUser = UserAccessor::getCurrentUser();

		$p = fromGet("p",null); 
		if(is_numeric($p)){
			$currentParent = Article::loadById($p);
		}else{
			$currentParent = Article::loadRootNode();
		}

		$path = $currentParent->loadPath();
		$articlesAndCategories = $currentParent->loadPublicChildren();

		if($currentParent->type != "category"){
			unset($path[count($path)-1]);
		}

		$articles = array();
		$categories = array();
		foreach($articlesAndCategories as $articleOrCategory){
			switch($articleOrCategory->type){
				case "category":
					$categories[] = $articleOrCategory;
				break;
				case "article":
				case "slot_fee":
				case "entrance_fee":
					$articles[] = $articleOrCategory;
				break;
			}
		}

		$view = parent::handleListRequest($action,$target,$identifiers);
		
		$model = $view->getModel();
		$model->setTitle("Webbshoppen");
		$model->setCategories($categories);
		$model->setArticles($articles);
		$model->setArticlePath($path);
		$model->setArticle($currentParent);
		
		if($currentUser->person_id > 0){
			$model->setCurrentCustomer(Person::loadById($currentUser->person_id));
		}else{
			$model->setCurrentCustomer(new Person());
		}
		$model->setCurrentCart(PublicCartAccessor::getCurrentCart());
		$view->setModel($model);

		$view = new PublicMaster($view);
		$view->setModel($model);
		
		return $view;
	}

	
	
}

?>
