<?php

require_once("lib/Controllers/PublicControllers/PublicController.php");
require_once("lib/Controllers/PublicControllers/PublicUserController.php");





class PublicPersonController extends PublicController{
		
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		if(count($segments) == 0){
			$action = CONTROLLER_ACTION_INDEX;
			return true;
		}
		else if(count($segments) == 1){
			switch($segments[0]){
				case CONTROLLER_ACTION_LEAVE_GROUP:
				case CONTROLLER_ACTION_SAVE_USER:
				case CONTROLLER_ACTION_DELETE_GROUP:
				case CONTROLLER_ACTION_CREATE_GROUP:
				case CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING:
					$action = $segments[0];
					return true;
					break;
			}
		}
		else if(count($segments) == 2){
			switch($segments[1]){
				case CONTROLLER_ACTION_SAVE_USER:
					$action = $segments[1];
					$identifiers["default"] = $segments[0];
					return true;
					break;
			}
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Person";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/AdminModels/Person";
	}
	
	public static function getFromArray($array,$identifier,$defaultValue = false){
		if(!isset($array[$identifier]))
			return $defaultValue;
		return $array[$identifier];
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$person_id = UserAccessor::getCurrentUser()->person_id;
		$innerView = $this->createIndexView();
		$model = $this->createFormModel();
		$model->setPerson(Person::loadById($person_id));
		$model->setGroupMembershipList(GroupMembership::loadByPerson($person_id));
		$model->setSignupList(Signup::loadAllFilteredByPerson($person_id));
		$model->setUser(UserAccessor::getCurrentUser());
		$model->setOrderList(Order::loadByPersonId($person_id));
		$model->setUserLevelSelectData(PublicUserController::getUserLevelSelectData()); 
		$model->setAvailableSleepingResources(SleepingSlotBooking::getPersonalSleepingResorces(Application::getConfigurationValue("sovplatsarrangemang"),$person_id));
		$model->setSleepingSlotBooking(SleepingSlotBooking::loadByPersonId($person_id));
		$model->setTitle("Din sida");
		$innerView->setModel($model);
		return new PublicMaster($innerView,true);
	}
	
	public function createRegisterModel(){
		return $this->createFormModel();
	}
	
	public function createRegisterView(){
		require_once($this->getViewDirectory()."/Register.php");
		$view = new RegisterView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	function handleFormRequest($action,$target,$identifiers){
		
		$validationErrors = array();
		if($action == CONTROLLER_ACTION_SAVE){
			$person = Person::loadById(UserAccessor::getCurrentUser()->person_id);
		}else{
			$person = new Person();
		}

		if(POSTBACK){
			static::populateFromPost($person);
			$isValid = $person->save();
			$validationErrors = $person->getValidationErrors();
			if($isValid && $action == CONTROLLER_ACTION_NEW){
				MessageAccessor::clearAllMessages();
				$user = new User();
				$password = fromPost("password");
				$password_repeat = fromPost("repeat_password","");
				$user->setNewPassword($password,$password_repeat);
				$user->username = fromPost("username");
				$user->person_id = $person->id;
				$user->is_active = true;
				$user->level = UserLevel::USER;
				if(!$user->save()){
					$isValid = false;
					Person::deleteById($person->id);
					static::addValidationErrorsToMessageHandler($user, "user/errorMessage");
				}
				else{
					
				}
			}else{
				static::addValidationErrorsToMessageHandler($person, "people/validationErrors");
			}
		}
		
		if(POSTBACK && $isValid){
			
			// no validation errors
			if($action == CONTROLLER_ACTION_NEW){
				MessageAccessor::addInformationMessage(Translator::translate("people/creationSuccessful"));
				UserAccessor::setCurrentUser($user);
				$defaultOrder = new Order;
				$defaultOrder->person_id = $user->person_id;
				PublicCartAccessor::setCurrentCart($order ? $order : $defaultOrder);
				
				if(WizardAccessor::getWizard())
					$returnUrl = $this->internalLink("wizard","?step=1");
				else
					$returnUrl = $this->internalLink("index","");
			}else{
				MessageAccessor::addInformationMessage(Translator::translate("people/publicUpdateSuccessMessage"));
				$returnUrl = fromRequest("returnUrl", $this->internalLink("person",""));
			}
			$view = new RedirectView($returnUrl);
		}
		else{
			// has validation errors or is first view
			if($action == CONTROLLER_ACTION_NEW){
				$view = $this->createRegisterView();
				$model = $this->createRegisterModel();
				$model->setTitle("Registrera användare");
			}else{
				$view = $this->handleIndexRequest($action, $target, $identifiers);
				$view = $view->innerView;
				$model = $view->getModel();
			}
			$view->setModel($model);
			$model->setPerson($person);
			$view = new PublicMaster($view,true);
		}
		
	
		
		return $view;
	}
	
	function handleSaveUser($action, $target, $identifiers){
		if(POSTBACK){
			$userDetailsModified = false;
            $emailAddressModified = false;
			$updateSucceeded = false;
			$currentUser = UserAccessor::getCurrentUser();
			$personOk = true;
            $userOk = true;
                             
			// make sure the user supplied the correct password when changing passwords
			$user = User::getUserByUsernameAndPassword($currentUser->username, fromPost("current_password"));
			
			if(!$user){
				MessageAccessor::addErrorMessage(Translator::translate("user/validationErrors/invalidPassword"));
				return $this->handleIndexRequest($action, $target, $identifiers);
			}
			else{
                $newEmailAddress = fromPost("email_address");
				$newUsername = fromPost("username");
				$password = fromPost("password");
				$password_repeat = fromPost("repeat_password");
				
				// update the password
				if(strlen($password)){
					$user->setNewPassword($password, $password_repeat);
					$userDetailsModified = true;
				}  

				if(UserAccessor::getCurrentUser()->username != $newUsername){
					$user->username = $newUsername;
					$userDetailsModified = true;
				}

				$person = Person::loadById($currentUser->person_id);

				if($newEmailAddress == ""){
					MessageAccessor::addErrorMessage(Translator::translate("people/validationErrors"));   
				}else if($person->email_address != $newEmailAddress){
					$emailAddressModified = true;
					$res = Person::loadByEmail($newEmailAddress);
					if(count($res) == 0){
						$person->email_address = $newEmailAddress;
						$personOk = $person->save();
					}else{
						MessageAccessor::addErrorMessage(Translator::translate("people/validationErrors/emailAddressInUse"),"email_address");
						$personOk = false;
					}
				}

				

				if($userDetailsModified){
					$userOk = $user->save();
				}

				if($emailAddressModified || $userDetailsModified){
					$updateSucceeded = $userOk && $personOk;
					if(!$userOk){
						$this->addValidationErrorsToMessageHandler($user, "user/validationErrors");
					}
					if(!$personOk){
						$this->addValidationErrorsToMessageHandler($person, "people/validationErrors");
					}
				}
				else{
					MessageAccessor::addInformationMessage(Translator::translate("user/noChanges"));
					return new RedirectView($this->internalLink("person", $user->person_id."/#".Html::CreateHash(Translator::translate("people/userDetails"))));
				}
			}
			if(!$updateSucceeded){
				return $this->handleIndexRequest($action, $target, $identifiers);
			}else{
				UserAccessor::setCurrentUser($user);
				MessageAccessor::addInformationMessage(Translator::translate("user/saveSuccessfullMessage"));
				return new RedirectView($this->internalLink("person", $user->person_id."/#".Html::CreateHash(Translator::translate("people/userDetails"))));
			}
		}
	}
	
	public function HandleAction($action, $target, $identifiers) {
		
		switch($action){
			case CONTROLLER_ACTION_LEAVE_GROUP:
			case CONTROLLER_ACTION_DELETE_GROUP:
				$group_id = fromGet("group_id");
				$membership = GroupMembership::loadByGroupAndPerson($group_id, UserAccessor::getCurrentUser()->person_id);
				$group = Group::loadById($group_id);
				
				GroupMembership::deleteById($membership->id);
				
				$remainingGroupMembers = GroupMembership::loadByGroup($group_id);
				if(count($remainingGroupMembers) > 0){
					$group->leader_membership_id = $remainingGroupMembers[0]->id;
				}else{
					Group::deleteById($group_id);
				}
				MessageAccessor::addInformationMessage(Translator::translate("groupMembership/deleteMessage"));
				return new RedirectView($this->internalLink("person", "#".Html::CreateHash(Translator::translate("people/signupsAndGroupMemberships"))));
				break;
			case CONTROLLER_ACTION_CREATE_GROUP:
				$group = new Group();    
				static::populateFromPost($group);
				if($group->save()){
					$groupMembership  = new Groupmembership();
					$groupMembership->person_id = UserAccessor::getCurrentUser()->person_id;
					$groupMembership->group_id = $group->id;
					$groupMembership->save();
					$group->leader_membership_id = $groupMembership->id;
					$group->save();
					MessageAccessor::addInformationMessage(Translator::translate("group/creationSuccessPublicMessage"));
				}else{
					$validationErrors = $group->getValidationErrors();
					foreach($validationErrors as $error)
						MessageAccessor::addErrorMessage(Translator::translate ("group/validationErrors/".$error));
				}

				return new RedirectView($this->internalLink("person", "#".Html::CreateHash(Translator::translate("people/signupsAndGroupMemberships"))));
				break;
			case CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING:
				
				$personalSleepingSlotBooking = SleepingSlotBooking::loadByPersonId(UserAccessor::getCurrentUser()->person_id);
				if($personalSleepingSlotBooking && fromPost("sleeping_resource_id") == -1){
						$personalSleepingSlotBooking->delete();
				}else if(fromPost("sleeping_resource_id") != -1){
					if(!$personalSleepingSlotBooking){
						$personalSleepingSlotBooking = new SleepingSlotBooking(); 
						$personalSleepingSlotBooking->person_id = UserAccessor::getCurrentUser()->person_id;
					}
					$personalSleepingSlotBooking->sleeping_resource_id = fromPost("sleeping_resource_id");
					$personalSleepingSlotBooking->save();
				}
				MessageAccessor::addInformationMessage(Translator::translate("people/sleepingSlotHasBeenUpdated"));
				return new RedirectView($this->internalLink("person", "#".Html::CreateHash(Translator::translate("sleepingResource/sleepingResource"))));
				break;
			case CONTROLLER_ACTION_SAVE_USER:
				if(ISPOSTBACK)
					return $this->handleSaveUser($action, $target, $identifiers);
			break;
				default:
			return parent::HandleAction($action, $target, $identifiers);
		}
		return parent::HandleAction($action, $target, $identifiers);
	}
	

	
}

?>