<?php

require_once("lib/Controllers/PublicControllers/PublicController.php");

class PublicIndexController extends PublicController{
	
	protected function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Index";
	}

	protected function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Index";
	}
	
	protected function createHelpModel(){
		require_once($this->getModelDirectory() . "/HelpModel.php");
		return new HelpModel();
	}

	protected function createHelpView(){
		require_once($this->getViewDirectory() . "/Help.php");
		$view = new HelpView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	public function handleIndexRequest($action,$target,$identifiers){
		$view = $this->createIndexView();
		$model = $this->createIndexModel();
		$model->setTitle("Startsidan");
		$model->setNews(NewsArticle::loadPublicList(5));
		$model->setCart(PublicCartAccessor::getCurrentCart());
		$view->setModel($model);
		return new PublicMaster($view,true);
	}
	
	protected function handleListRequest($action, $target, $identifiers) {
		parent::handleListRequest($action, $target, $identifiers);
	}
	
	public function HandleAction($action, $target, $identifiers) {
		switch($this->requestedControllerName == "help"){
			case "help":
				$view = $this->createHelpView();
				$model = $this->createHelpModel();
				$model->setTitle("Hjälp!");
				$view->setModel($model);
				$view = new PublicMaster($view);
				break;
			default:
				if($target=="error"){
					switch($identifiers["error"]){
						case "404":
							$view = $this->create404View();
							$model = $this->create404Model();
							$view->setModel($model);
							break;
					}
				}else{
					$view = parent::HandleAction($action, $target, $identifiers);
				}
				break;
		}
		return $view;
	}
}

?>
