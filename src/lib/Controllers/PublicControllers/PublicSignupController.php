<?php
require_once("lib/Controllers/PublicControllers/PublicController.php");
class PublicSignupController extends PublicController{
		
		function getViewDirectory(){
			die("not implemented");
		}
		
		function getModelDirectory(){
			die("not implemented");
		}
		
		function handleAction($action,$target,$identifiers){
			
			$currentUser = UserAccessor::getCurrentUser();
			
			if($action == CONTROLLER_ACTION_NEW){
				$slot = SignupSlot::loadById(fromPost("signup_slot_id",false));
				$article = $slot->getArticle();
				$signup = new Signup();
				$signup->signup_slot_id = $slot->id;
				$signupSuccessful = false;
				if($slot->getSlotType()->cardinality > 1){
					$group_id = fromPost("group_id",false);
					if(!$group_id){
						$group  = new Group();
						$group->name = fromPost("group_name");
						if(!$group->save()){
							/** TODO: add error handling **/
						}
						$groupMembership = new Groupmembership();
						$groupMembership->person_id = $currentUser->person_id;
						$groupMembership->group_id = $group->id;
						$groupMembership->save();
						$group->leader_membership_id = $groupMembership->id;
						$group->save();
						$group_id = $group->id;
					}
					$signup->group_id = $group_id;
					if(GroupMembership::personIsLeader($currentUser->person_id, $group_id)){
						$signupSuccessful = $signup->save();
					}
					
				}else{
					$signup->person_id = $currentUser->person_id;
					$signupSuccessful = $signup->save();
				}
				if($signupSuccessful){
					MessageAccessor::addInformationMessage(Translator::translate("signup/addSuccessfullMessage"));
					if($article->price > 0){
						$cart = PublicCartAccessor::getCurrentCart();
						$row = new OrderRow();
						$row->count = 1;
						$row->article_id = $article->id;
						$row->price_per_each = $article->price;
						$row->name = $article->name;
						$cart->addOrderRow($row);
						$cart->save();
						PublicCartAccessor::setCurrentCart($cart);
						MessageAccessor::addInformationMessage("<em> 1 x " . $row->name."</em> ". Translator::translate("articles/hasBeenAddedToCart"));
					}
				}
			}
			
			if($action == CONTROLLER_ACTION_DELETE){
				$signup = Signup::loadById($identifiers['default']);
				if($signup->person_id && $signup->person_id == UserAccessor::getCurrentUser()->person_id){
					Signup::deleteById($identifiers['default']);
				}
				else if($signup->group_id && GroupMembership::personIsLeader(UserAccessor::getCurrentUser()->person_id, $signup->group_id)){
					$signup = Signup::loadById($identifiers['default']);
					Signup::deleteById($identifiers['default']);
				}
				MessageAccessor::addInformationMessage(Translator::translate("signup/deleteMessage"));
				
				$slot = SignupSlot::loadById($signup->signup_slot_id);
				$article = $slot->getArticle();
				if($article->id > 0){
					$cart = PublicCartAccessor::getCurrentCart();
					$row = $cart->getOrderRow($article->id);
					if($row && $row->count == 1){
						$cart->deleteOrderRow($article->id);
						MessageAccessor::addInformationMessage("<em>1 x " . $row->name."</em> ". Translator::translate("articles/hasBeenRemovedFromCart"));
					}else if($row->count > 1){
						$row->count--;
						$cart->setOrderRow($row);
						MessageAccessor::addInformationMessage("<em>1 x " . $row->name."</em> ". Translator::translate("articles/hasBeenRemovedFromCart"));
					}
					PublicCartAccessor::setCurrentCart($cart);
				}
				
				
				$returnUrl = fromRequest("returnUrl", $this->internalLink("person","#Anmalningar_och_lagmedlemskap",false));
				return new RedirectView($returnUrl);
			}
			
			return new RedirectView(fromGet("returnUrl"));
		}
}

?>
