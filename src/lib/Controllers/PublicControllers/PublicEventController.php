<?php
require_once("lib/Controllers/AdminControllers/EventTypeController.php");
require_once("lib/Controllers/PublicControllers/PublicController.php");



class PublicEventController extends PublicController{
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Views/PublicViews/Event/";
	}

	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../../Models/PublicModels/Event";
	}

	function createOrganizerEventListView(){
		require_once($this->getViewDirectory()."OrganizerEventList.php");
		$view = new OrganizerEventListView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}

	function createOrganizerEventListModel(){
		require_once($this->getModelDirectory()."OrganizerEventModel.php");
		return new OrganizerEventModel();
	}
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	function handleIndexRequest($action,$target,$identifiers){
		$model = $this->createIndexModel();
		
		$currentUser = UserAccessor::getCurrentUser();
		if($currentUser->person_id){
			$model->setUserGroups(Group::loadByLeaderId($currentUser->person_id));
		}

		$tree = Event::loadPublicEventTree(0,$identifiers['default'],  $currentUser->person_id);
		$model->setCurrentEvent($tree[0]->events[0]);
		$model->setEventTypeList(EventType::loadList());
		$model->setTitle("Arrangemang: ".$model->getCurrentEvent()->name);
		$view = $this->createIndexView();
		$view->setModel($model);
		$masterView = new PublicMaster($view);
		$masterView->setModel($model);
		return $masterView;
	}
	
	function handleListRequest($action,$target,$identifiers){
		if($action == CONTROLLER_ACTION_LISTING){
			return new RedirectView($this->internalLink("eventType"));
		}else{
			return new OrganizerEventListView();
		}
	}
}

?>
