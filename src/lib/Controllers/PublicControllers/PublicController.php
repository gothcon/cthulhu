<?php
require_once("lib/Controllers/Controller.php");
require_once("lib/Views/PublicViews/Master/PublicMaster.php");
class PublicController extends Controller{
	public function create400View() {
		return new PublicMaster(parent::create400View());
	}
	public function create401View() {
		return new PublicMaster(parent::create401View());
	}
	public function create404View() {
		return new PublicMaster(parent::create404View());
	}
}
