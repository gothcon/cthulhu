<?php

require_once("lib/Controllers/PublicControllers/PublicController.php");
class WizardController extends PublicController{
	
	public $step1Body = "Snabbguide till gothcon 2013";
	public $step1Title = "Nästa steg";
	public $step1PreviousStep ="wizard";
	public $step1NextStep ="";
	public $step1PreviousStepLabel ="";
	public $step1NextStepLabel ="";
	public $step1Redirect;
	
	public function HandleAction($action, $target, $identifiers) {
		
		$closeWizard = fromGet("close");
		$step = fromGet("step",1);
		
		$wizardData = array();
		
		
		for($i = 0; $i < 6; $i++){
			$wizardData[$i] = new Wizard();
			$wizardData[$i]->title				= Translator::translate("wizard/step".$i."/title");
			$wizardData[$i]->body				= Translator::translate("wizard/step".$i."/body");
			
			// next step url
			switch($i){
				case 0:
					$wizardData[$i]->nextStepUrl = "#";
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					$wizardData[$i]->nextStepUrl = $this->internalLink("wizard", "?step=".($i+1));
					break;
				case 5:
					$wizardData[$i]->nextStepUrl = "#";
			}
			$wizardData[$i]->nextStepLabel		= Translator::translate("wizard/step".$i."/nextStepLabel");
			
			
			// previous step url
			switch($i){
				case 0:
				case 1:
					$wizardData[$i]->previousStepUrl = "#";
					break;
				case 2:
				case 3:
				case 4:
				case 5:
					$wizardData[$i]->previousStepUrl = $this->internalLink("wizard", "?step=".($i-1));
			}
			$wizardData[$i]->previousStepLabel	= Translator::translate("wizard/step".$i."/previousStepLabel");
			
			$wizardData[$i]->closeWizardLabel	= Translator::translate("wizard/closeWizardLabel");
			$wizardData[$i]->closeWizardUrl		= $this->internalLink("wizard","?close=true");
			$wizardData[$i]->currentStep		= $i;
		}
		
		
		$wizardData[0]->targetUrl			= $this->internalLink("person",CONTROLLER_ACTION_NEW);
		$wizardData[1]->targetUrl			= $this->internalLink("eventType");
		$wizardData[2]->targetUrl			= $this->internalLink("article", "?p=31");
		$wizardData[3]->targetUrl			= $this->internalLink("person", "#Sovplats");
		$wizardData[4]->targetUrl			= $this->internalLink("webshop");
		$wizardData[5]->targetUrl			= $this->internalLink("checkout");

		
		if($closeWizard){
			if(JSONREQUEST){
				parent::handleJsonRequest($action, $target, $identifiers);
			}else{
				WizardAccessor::setWizard(0);
				return new RedirectView($this->internalLink("index"));
			}
		}
		
		
		if($step < 6){
			if(UserAccessor::getCurrentUser()->id && $step == 0){
				$step++;
			}
			$wizard = $wizardData[$step];
			WizardAccessor::setWizard($wizard);
			return new RedirectView($wizard->targetUrl);
		}
		else{
			WizardAccessor::setWizard(0);
			return parent::handle404();
		}
		
	}
	
	public function __construct($internalLinkBuilder) {
		parent::__construct($internalLinkBuilder);
		
		$currentUser = UserAccessor::getCurrentUser();
		if($currentUser->id){
			
		}
	}
	
	protected function handleJsonRequest($action, $target, $identifiers) {
		$view = parent::handleJsonRequest($action, $target, $identifiers);
		$model = $view->getModel();
		$model->status = JsonModel::STATUS_OK;
		return $view;
	}
	
	
}

?>
