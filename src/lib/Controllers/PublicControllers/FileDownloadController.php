<?php
require_once("lib/Controllers/Controller.php");


class ImageController extends Controller{
	//put your code here
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
		switch(count($segments)){
			case 0:
				return parent::parseUrl($segments, $action, $targetType, $identifiers);
				break;
			default:
				if(is_numeric($segments[0])){
					return parent::parseUrl($segments, $action, $targetType, $identifiers);
				}
				else{
					$action = CONTROLLER_ACTION_SHOW_IMAGE;
					$identifiers["default"] = implode("/",$segments);
					return true;
				}
				break;
		}
		parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	public function HandleAction($action, $target, $identifiers) {
		parent::HandleAction($action, $target, $identifiers);
	}
	
	public function HandleRequest($url) {
		if($url != ""){
			$documentRoot = Application::getConfigurationValue("DocumentRoot");
			$imageDirectory = $documentRoot."/Images/";
			$imageName = $imageDirectory.$url;

			if(!file_exists($imageDirectory.$url)){
				$image = Image::loadByUrl("/".$url);
				if($image){
					mkdir(dirname($imageName), "0777", true);
					$fp = fopen($imageName,'w');
					fwrite($fp,$image->content);
				}else{
					$imageName = $imageDirectory."MissingImage.png";
				}
			}
			header("Pragma: public");
			header("Content-type: image/png"); 
			header('Content-Transfer-Encoding: binary'); 
			readfile($imageName);
			exit;
		}else{
			$view = new FourOFourView();
			$view->init();
			$view->render();
		}
	}
}

?>
