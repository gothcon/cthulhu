<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AngularController
 *
 * @author Joakim
 */
require_once("lib/Controllers/Controller.php");
class AngularController extends Controller{

	public function __construct($internalLinkBuilder) {
		if(!defined("ANGULAR_ACTIONS")){
			define("ANGULAR_ACTIONS",true);
			define("ANGULAR_ACTION_NEW","new");
			define("ANGULAR_ACTION_LIST","list");
			define("ANGULAR_ACTION_GET","view");
			define("ANGULAR_ACTION_UPDATE","update");
			define("ANGULAR_ACTION_CREATE","create");
		}
		require_once("lib/Views/AdminViews/Master/AngularAdminMaster.php");
		parent::__construct($internalLinkBuilder);
	}
	
	public function PreHandleAction() {
		return true;
		$httpReferer = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "";
		
		$siteRoot = Application::GetConfigurationValue("SiteRoot");
		
		if($httpReferer != "" && substr($httpReferer, 0,strlen($siteRoot)) == $siteRoot){
			return true;
		}else{
			return false;
		}
	}
	
	public function userHasPermission($action = null){
		return true;
	}
	
	public function GetPostData($jsonDecode = true){
		$content = file_get_contents("php://input");
		if($jsonDecode){
			$content = json_decode($content);
		}
		return $content;
	}
	
	public function HandleServiceRequest($serviceName) {
		header("Status: 400 Bad Request");
		$view = $this->createDoctrineJsonView();
		$model = $this->createJsonModel();
		$model->setData("Endpoint not found");
		$view->setModel($model);
		return $view;
	}
	
	public function HandleRequest($url, $requestedAs = null, $doRender = true) {
		$this->url = $url;
		$this->urlSegments = $this->splitUrl($this->url);
		$this->requestedControllerName = $requestedAs;
		$requestIsValid = $this->preHandleAction();
		if($requestIsValid){
			if(!$this->userHasPermisson()){
				$view = $this->handlePermissionDenied();
			}else{
				$noOfSegments = count($this->urlSegments);
				if( $noOfSegments >= 2 && $this->urlSegments[$noOfSegments-2] == "svc" && JSONREQUEST){
					$view = $this->HandleServiceRequest($this->urlSegments[$noOfSegments-1]);
				}
				else{
					$view = $this->HandleAction();
				}
			}
		}else{
			$view = $this->handle400();
		}
		
		if($doRender){
			$view = $this->preRenderView($view);
			$view->init();
			$view->render();
		}
		else{
			$view->init();
			return $view;
		}
	}
	
	public function handleGet($target,$identifiers,$returnAsJson = false){
		if($returnAsJson){
			$view = $this->createJsonView();
			$model = $this->createJsonView();
			$view->setModel($model);
			return $view;
		}
		$view = $this->createIndexView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		$model = $this->createIndexModel();
		$view->setModel($model);
		return $view;
	}
	
	public function handlePost($target,$identifiers,$returnAsJson = false){
		
	}

	public function handleDelete($target,$identifiers,$returnAsJson = false){
		
	}
	
	public function HandleAction($action = null, $target = null, $identifiers = null) {
		if(JSONREQUEST){
			if(IS_GET_REQUEST){
				if(count($this->urlSegments) > 0){
					if(is_numeric($this->urlSegments[0])){
						return $this->handleGet("default",array("default" => $this->urlSegments[0]),true);
					}
					else{
						return $this->handleGet("default",array("default" => "new"),true);
					}
				}else{
					return $this->handleGet("default",array(),true);
				}
			}
			else if(IS_POST_REQUEST){
				if(count($this->urlSegments) > 0 && is_numeric($this->urlSegments[0])){
					return $this->handlePost("default",array("default"=>$this->urlSegments[0]),true);
				}else{
					return $this->handlePost("default",array(),true);
				}
			}
			else if(IS_DELETE_REQUEST){
				if(count($this->urlSegments) > 0 && is_numeric($this->urlSegments[0])){
					return $this->handleDelete("default",array("default"=>$this->urlSegments[0]));
				}
			}
			else{
				// if everything fails, return a list of data
				return $this->handleGet("default",array(),true);
			}
		}
		else{
			if(count($this->urlSegments) > 0){
				if(is_numeric($this->urlSegments[0])){
					return $this->handleGet("default",array("default" => $this->urlSegments[0]),false);
				}
				else{
					return $this->handleGet("default",array("default" => "new"),false);
				}
			}else{
				return $this->handleGet("default",array(),false);
			}
		}
	}
	
}
