<?php 

	require_once("lib/Controllers/IController.php");

	class Controller implements IController{
	
		protected $action,$identifier,$urlSegments,$isPostback,$controllerBaseUrl,$url;
		/**
		 *
		 * @var InternalLinkBuilder
		 */
		protected $internalLinkBuilder;
		protected $requestedControllerName = null;
		
		public static function getPostedJsonObject(){
			return json_decode(file_get_contents("php://input"));
		}
		
		/** @return string */
		public static function getControllerName(){
			print_r(get_callstack());
			die("controller name not implemented");
		}
		
		/** @return string */
		public function routerPath(){
			return $this->internalLinkBuilder->controllerBaseUrl;
		}

		/** @return string */
		public function controllerPath(){
			return $this->internalLinkBuilder->internalLink(static::getControllerName());
		}
		
		/** @return string */
		public function internalLink($ControllerName,$parameters=""){
			return $this->internalLinkBuilder->internalLink($ControllerName,$parameters);
		}
		
		public function __construct($internalLinkBuilder){
			require_once("lib/Views/RedirectView.php");
			require_once("lib/Models/Model.php");
			require_once("lib/Models/HtmlModel.php");
			require_once("lib/ClassLib/Translator.php");
			// $this->controllerBaseUrl = $controllerBaseUrl;
			$this->internalLinkBuilder = $internalLinkBuilder;
			$this->invalidUrl = false;
			if(!defined("POSTBACK")){
                define("POSTBACK",($_SERVER['REQUEST_METHOD'] == 'POST'));
				define("IS_POST_REQUEST",	$_SERVER['REQUEST_METHOD'] == 'POST');
				define("IS_DELETE_REQUEST",	$_SERVER['REQUEST_METHOD'] == 'DELETE');
				define("IS_GET_REQUEST",	$_SERVER['REQUEST_METHOD'] == 'GET');
				
			}
			if(!defined("JSONREQUEST")){
                            if(strpos($_SERVER["HTTP_ACCEPT"],"application/json") === false && strtolower (fromGet("format","")) != "json"){
				define("JSONREQUEST",  false);
                            }else{
				define("JSONREQUEST",  true);                                
                            }
			}
		}
		
		/**
		public function transfer($controllerName){
			$controller = new $controllerName($this->internalLinkBuilder);
			return $controller->handleRequest($this->url,$requestedAs, false);
		}
		**/
		
		/** @return bool */
		public function isPostback(){
			return ($_SERVER['REQUEST_METHOD'] == 'POST');
		}
		
		/** @return bool */
		public function isJsonRequest(){
			if(JSONREQUEST)
				return true;
			$format = fromGet("format","");
			return strToLower($format) == 'json';
         }
		
		/** @return ReadOnlyEntity */
		static public function populateFromPost(&$entity,$prefix=""){
			$persistedProperties = $entity->getPersistedProperties();
			foreach($persistedProperties as $key => $value){
				if(isset($_POST[$prefix.$key])){
					$entity->$key = fromPost($prefix.$key,null);
				}
			}
			return $entity;
		}
		
		/** @return ReadOnlyEntity */
		static public function populateFromArray(&$entity,$array,$prefix = ""){
			$persistedProperties = $entity->getPersistedProperties();
			foreach($persistedProperties as $key => $value){
				if(isset($array[$prefix.$key])){
					$entity->$key = fromArray($array,$prefix.$key,null);
				}
			}
			return $entity;
		}
		
		/** @return Array */
		static public function getSortParameters($defaultValues,$prefix="sort_"){
			$property = fromGet("{$prefix}property",$defaultValues["property"]);
			$direction = fromGet("{$prefix}direction",$defaultValues["direction"]);
			$offset = fromGet("{$prefix}offset",$defaultValues["offset"]);
			$count = fromGet("{$prefix}count",$defaultValues["count"]);
			return array("property" => $property,"direction" => $direction, "offset" => $offset, "count" => $count);
		}
		
		/** @return Array */
		protected function splitUrl($url){
			if(strlen($url) != 0){
				if($url{strlen($url)-1} == "/")
					$url = substr($url,0,strlen($url)-1);
				if(strlen($url) > 0 && $url{0} == "/")
					$url = substr($url,1,strlen($url)-1);
			}
			return strlen($url) > 0 ? explode("/",$url) : array();
		}
		
		/** @return bool */
		protected function parseUrl($segments,&$action,&$targetType,&$identifiers){

			$actionsWithoutIdentifier = array(
				CONTROLLER_ACTION_LISTING,
				CONTROLLER_ACTION_NEW,
				CONTROLLER_ACTION_CREATE_ORDER_FROM_CART,
				CONTROLLER_ACTION_LOGIN,
				CONTROLLER_ACTION_LOGOUT,
				CONTROLLER_ACTION_SHOW_CART
			);
			
			$actionsThatNeedIdentifier = array(
				CONTROLLER_ACTION_INDEX,
				CONTROLLER_ACTION_EDIT,
				CONTROLLER_ACTION_DELETE,
				CONTROLLER_ACTION_FORM,
				CONTROLLER_ACTION_SAVE,
				CONTROLLER_ACTION_UPDATE,
				CONTROLLER_ACTION_ADD_TO_CART,
				CONTROLLER_ACTION_REMOVE_FROM_CART,
			);
		
			$identifiers = array();
			$action = null;
			$targetType = null;
			$segments = array_reverse($segments);
			
			if(count($segments) == 0){
				// empty url
				$action = "";
				$targetType = "default";
				$offset = 0;
			}
			else if(in_array($segments[0],$actionsThatNeedIdentifier)){
				// begins with an action that needs an identifier index, edit, delete
				
				$action = $segments[0];
				if(!isset($segments[1])){
					return false;
				}
				
				$targetType = isset($segments[2]) ? $segments[2] : "default";
				
				$identifier = strpos($segments[1],".") !== false ? trim(substr($segments[1],0,strpos($segments[1],"."))) : $segments[1];
					
				if(!is_numeric($identifier)){
					return false;
				}	
				$identifiers[$targetType] = $segments[1];
				$offset = 3;
			}
			else if(in_array($segments[0],$actionsWithoutIdentifier)){
				// list, new
				$action = $segments[0];
				$targetType = isset($segments[1]) ? $segments[1] : "default";
				$offset = 2;
			}
			// identifier
			else{
				$identifier = strpos($segments[0],".") !== false ? trim(substr($segments[0],0,strpos($segments[0],"."))) : $segments[0];
				if(is_numeric($identifier)){
					$action = "index";
					$targetType = isset($segments[1]) ? $segments[1] : "default";
					$identifiers[$targetType] = $identifier;
					$offset = 2;
				}
				else{
					// invalid url
					return false;
				}
			}
			
			for(; $offset < count($segments); $offset +=2){
				$identifier = strpos($segments[$offset],".") !== false ? trim(substr($segments[$offset],0,strpos($segments[$offset],"."))) : $segments[$offset];
				if(!is_numeric($identifier)){
					$this->invalidUrl = true;
					return;
				}
				$identifierTarget = isset($segments[$offset+1]) ? $segments[$offset+1] : "default";
				$identifiers[$identifierTarget] = $identifier;
			}
			
			switch($action){
				case CONTROLLER_ACTION_ADD_TO_CART :
				case CONTROLLER_ACTION_CREATE_ORDER_FROM_CART:
				case CONTROLLER_ACTION_REMOVE_FROM_CART:
				case CONTROLLER_ACTION_EDIT:
				case CONTROLLER_ACTION_SAVE:
				case CONTROLLER_ACTION_UPDATE:
				case CONTROLLER_ACTION_FORM:
				case CONTROLLER_ACTION_DELETE:
				case CONTROLLER_ACTION_LISTING:
				case CONTROLLER_ACTION_INDEX:
				case CONTROLLER_ACTION_NEW:
				case CONTROLLER_ACTION_LOGIN:
				case CONTROLLER_ACTION_LOGOUT:
					break;
				default:
					$action = $this->getDefaultAction();
			}
			Application::log(array("segments" => $segments,"action" => $action, "target type" => $targetType,"identifiers" =>$identifiers));
			return true;
		}
	
		/** @return string */
		protected function getViewDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views";
		}
		
		/** @return string */
		protected function getModelDirectory(){
			return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Models";
		}
		
		/** @return RedirectView */
		protected function createRedirectView($location){
			require_once("lib/Views/RedirectView.php");
			return new RedirectView($location);
		}

		/** @return HtmlView */
		protected function createFormView(){
			require_once($this->getViewDirectory()."/Form.php");
			$formViewClassName = "FormView";
			$view = new $formViewClassName;
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		/** @return HtmlListView */
		protected function createListView(){		
			require_once($this->getViewDirectory()."/Listing.php");

			$listingViewClassName = "ListingView";
			$view = new $listingViewClassName;
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;

		}
		
		/** @return JsonView */
		protected function createDoctrineJsonView(){
			require_once("lib/Views/DoctrineJsonView.php");
			return new DoctrineJsonView();
		}
                
                /** @return DoctrineJsonView */
		protected function createJsonView(){
			require_once("lib/Views/JsonView.php");
			return new JsonView();
		}
		
		/** @return HtmlView */
		protected function createIndexView(){
			require_once($this->getViewDirectory()."/Index.php");
			$indexViewClassName = "IndexView";
			$view = new $indexViewClassName;
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		/** @return string */
		protected function getDefaultAction(){
			return CONTROLLER_ACTION_INDEX;
		}
		
		/** @return string */
		protected function getDefaultIdentifiedAction(){
			return CONTROLLER_ACTION_INDEX;
		}

		/** @return HtmlListView */
		protected function handleListRequest($action,$target,$identifiers){
			$model = $this->createListModel();
			$view = $this->createListView();
			$view->setModel($model);
			return $view;
		}
		
		/** @return JsonView */
		protected function handleJsonRequest($action,$target,$identifiers){
			$model = $this->createJsonModel();
			$view = $this->createJsonView();
			$view->setModel($model);
			return $view;
		}
		
		/** @return HtmlView */
		protected function handleFormRequest($action,$target,$identifiers){
			$model = $this->createFormModel();
			$view = $this->createFormView();
			$view->setModel($model);
			return $view;
		}
		
		/** @return HtmlView */
		protected function handleIndexRequest($action,$target,$identifiers){
			$model = $this->createIndexModel();
			$view = $this->createIndexView();
			$view->setModel($model);
			return $view;
		}
		
		/** @return HtmlView */
		protected function handleDeleteRequest($action,$target,$identifiers){
			$model = $this->createIndexModel();
			$view = $this->createIndexView();
			$view->setModel($model);
			return $view;
		}
		
		/** @return HtmlListView */
		protected function createListModel(){
			require_once($this->getModelDirectory()."/ListModel.php");
			return new ListModel();
		}
		
		/** @return HtmlModel */
		protected function createFormModel(){
			require_once($this->getModelDirectory()."/FormModel.php");
			return new FormModel();
		}
		
		/** @return HtmlModel */
		protected function createIndexModel(){
			require_once($this->getModelDirectory()."/IndexModel.php");
			return new IndexModel();
		}
		
		/** @return JsonModel */
		protected function createJsonModel(){
			require_once("lib/Models/JsonModel.php");
			return new JsonModel();
		}
		
		/**
		 * 
		 * @param string $action
		 * @param string $target
		 * @param string $identifiers
		 * @return View
		 */
		public function HandleAction($action,$target,$identifiers){
			switch($action){
				case CONTROLLER_ACTION_LISTING:
					if($this->isJsonRequest()){
						$view = $this->handleJsonRequest($action,$target,$identifiers);
					}
					else{
						$view = $this->handleListRequest($action,$target,$identifiers);
					}
				break;
				case CONTROLLER_ACTION_SAVE:
				case CONTROLLER_ACTION_FORM:
				case CONTROLLER_ACTION_NEW:
				case CONTROLLER_ACTION_EDIT:
					$view = $this->handleFormRequest($action,$target,$identifiers);
				break;
				case CONTROLLER_ACTION_INDEX:
					if($this->isJsonRequest()){
						$view = $this->handleJsonRequest($action,$target,$identifiers);
					}
					else{
						$view = $this->handleIndexRequest($action,$target,$identifiers);
					}
				break;
				case CONTROLLER_ACTION_DELETE:
					$view = $this->handleDeleteRequest($action,$target,$identifiers);
				break;
				default:
					$view = new RedirectView($this->internalLink("index","invalidUrl=true&url={$this->url}"));
				break;
			}
			return $view;
		}
		
		
		/** @return FourOFourView */
		function create404View(){
			require_once("lib/Views/FourOFourView.php");
			$view = new FourOFourView();
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		/** @return HtmlModel */
		function create404Model(){
			require_once("lib/Models/HtmlModel.php");
			return new HtmlModel();
		}		
		
		/** @return FourHundredView */
		function create400View(){
			require_once("lib/Views/FourHundredView.php");
			$view = new FourHundredView();
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		/** @return HtmlModel */
		function create400Model(){
			require_once("lib/Models/HtmlModel.php");
			return new HtmlModel();
		}
		
		/** @return HtmlModel */
		function create401Model(){
			require_once("lib/Models/HtmlModel.php");
			return new HtmlModel();
		}
		
		/** @return FourOOneView */
		function create401View(){
			require_once("lib/Views/FourOOneView.php");
			$view = new FourOOneView();
			$view->setInternalLinkBuilder($this->internalLinkBuilder);
			return $view;
		}
		
		/** @return FourHundredView */
		protected function handle400(){
			$view = $this->create400View();
			$model = $this->create400Model();
			$view->setModel($model);
			return $view;
		}
		
		/** @return FourOOneView */
		protected function handle401(){
			$view = $this->create401View();
			$model = $this->create401Model();
			$model->setTitle("401 - Rättighet saknas!");
			$view->setModel($model);
			return $view;
		}
		
		/** @return FourOFourView */
		protected function handle404(){
			$view = $this->create404View();
			$model = $this->create404Model();
			$view->setModel($model);
			return $view;
		}
		
		/**
		 * Override this method to validate the request. e.g. Check for CSRF attacks
		 * Return true if the request is OK otherwise return false 
		 * @return boolean
		 */
		public function preHandleAction(){
			return true;
		}
		
		/** @return View */
		public function HandleRequest($url,$requestedAs = null, $doRender = true){
                        
            $this->url = $url;
			$this->urlSegments = $this->splitUrl($this->url);
			$this->requestedControllerName = $requestedAs;
			$requestIsValid = $this->preHandleAction();
			if($requestIsValid){
                            	
				if($this->parseUrl($this->urlSegments,$action,$target,$identifiers)){
					if(!$this->userHasPermisson($action)){
						$view = $this->handlePermissionDenied($action,$target,$identifiers);
					}else{
						$view = $this->HandleAction($action,$target,$identifiers);
					}
				}else{
					$view = $this->handle400();
				}

                                
			}else{
				$view = $this->handle400();
			}
			$view->init();
                        
                               
			if($doRender){
				$view = $this->preRenderView($view);
				$view->render();
			}
			else{
				return $view;
			}

		}
		
		public function preRenderView($view){
			return $view;
		}
		
		/** @return bool **/
		public function userHasPermisson($action = "*"){
			return true;
		}

		/** @return View **/
		public function handlePermissionDenied($action = null,$target = null,$identifiers = null){
			if($this->isJsonRequest()){
				$view = $this->createJsonView();
				$model = $this->createJsonModel();
				$model->setStatus(JsonModel::STATUS_INSUFFICIENT_PERMISSION);
				$model->setMessage(Translator::translate("general/insufficientPermission"));
				$view->setModel($model);
				return $view;
			}else{
				$view = $this->handle401();
				return $view;
			}
		}

		/** @return mixed **/
		public static function toJsonObject($mixed){
			
			$paramIsObject = is_object($mixed);
			$paramIsEntity = is_a($mixed,"ReadOnlyEntity");
			
			$jsonObject = new stdClass;
			
			if(is_array($mixed)){
				foreach($mixed as $key => $entity){
					$mixed[$key] = static::toJsonObject($entity);
				}
				return $mixed;
			}
			else if($paramIsObject){
				
				if($paramIsEntity){
					$jsonObject = $mixed->getStandardClone();
				}else{
					foreach($mixed as $key => $value){
						$jsonObject->$key =  static::toJsonObject($value);
					}
				}
				return $jsonObject;
			}
			else{
				return $mixed;
			}
		}
		
		public static function addValidationErrorsToMessageHandler($entityOrArray,$xpathToTranslation){
			
			$lastChar = $xpathToTranslation{(strlen($xpathToTranslation)-1)};
			if($lastChar != "/")
				$xpathToTranslation .="/";
			
			if(is_array($entityOrArray))
				$validationErrors = $entityOrArray;
			if(is_a($entityOrArray,"ReadWriteEntity"))
				$validationErrors = $entityOrArray->getValidationErrors();
			foreach($validationErrors as $invalidProperty => $errorString){
				MessageAccessor::addErrorMessage(Translator::translate("{$xpathToTranslation}{$errorString}"),$invalidProperty);
			}
			
		}
	}
	if(!defined("CONTROLLER_ACTIONS")){
		define("CONTROLLER_ACTION_REGISTER",Translator::translate("actions/register"));
		define("CONTROLLER_ACTION_NEW",Translator::translate("actions/new"));
		define("CONTROLLER_ACTION_SAVE",Translator::translate("actions/save"));
		define("CONTROLLER_ACTION_UPDATE",Translator::translate("actions/update"));
		define("CONTROLLER_ACTION_DELETE",Translator::translate("actions/delete"));
		define("CONTROLLER_ACTION_LISTING",Translator::translate("actions/list"));
		define("CONTROLLER_ACTION_FORM",Translator::translate("actions/form"));
		define("CONTROLLER_ACTION_EDIT",Translator::translate("actions/edit"));
		define("CONTROLLER_ACTION_INDEX",Translator::translate("actions/index"));
		define("CONTROLLER_ACTION_VIEW",Translator::translate("actions/view"));
		define("CONTROLLER_ACTION_LOGIN",Translator::translate("actions/login"));
		define("CONTROLLER_ACTION_LOGOUT",Translator::translate("actions/logout"));
		define("CONTROLLER_ACTION_ADD_TO_CART",Translator::translate("actions/addToCart"));
		define("CONTROLLER_ACTION_SHOW_CART",Translator::translate("actions/showCart"));
		define("CONTROLLER_ACTION_REMOVE_FROM_CART",Translator::translate("actions/removeFromCart"));
		define("CONTROLLER_ACTION_CREATE_ORDER_FROM_CART",Translator::translate("actions/createOrderFromCart"));
		
		// define("CONTROLLER_ACTION_CANCEL_ORDER",Translator::translate("actions/cancelOrder"));
		
		define("CONTROLLER_ACTION_SHOW_SIGNUP_OVERVIEW", Translator::translate("actions/showSignupOverview"));

		define("CONTROLLER_ACTION_CREATE_EVENT_OCCATION", Translator::translate("actions/createEventOccation"));
		define("CONTROLLER_ACTION_UPDATE_EVENT_OCCATION", Translator::translate("actions/updateEventOccation"));
		define("CONTROLLER_ACTION_DELETE_EVENT_OCCATION", Translator::translate("actions/deleteEventOccation"));

		define("CONTROLLER_ACTION_CREATE_SIGNUP_SLOT", Translator::translate("actions/createSignupSlot"));
		define("CONTROLLER_ACTION_UPDATE_SIGNUP_SLOT", Translator::translate("actions/updateSignupSlot"));
		define("CONTROLLER_ACTION_DELETE_SIGNUP_SLOT", Translator::translate("actions/deleteSignupSlot"));
		
		define("CONTROLLER_ACTION_CREATE_SIGNUP", Translator::translate("actions/createSignup"));
		define("CONTROLLER_ACTION_UPDATE_SIGNUP", Translator::translate("actions/updateSignup"));
		define("CONTROLLER_ACTION_DELETE_SIGNUP", Translator::translate("actions/deleteSignup"));     
		
		define("CONTROLLER_ACTION_SHOW","show");
		define("CONTROLLER_ACTION_DOWNLOAD","download");

		define("CONTROLLER_ACTION_DELETE_MEMBERSHIP",Translator::translate("actions/deleteMembership"));
		define("CONTROLLER_ACTION_DELETE_GROUPSIGNUP",Translator::translate("actions/deleteGroupSignup"));
		define("CONTROLLER_ACTION_UPDATE_LEADER",Translator::translate("actions/updateGroupLeader"));
		
		define("CONTROLLER_ACTION_UPDATE_SLEEPING_SLOT_BOOKING",Translator::translate("actions/updateSleepingSlotBooking"));
		
		define("CONTROLLER_ACTION_CREATE_RESOURCE_BOOKING", Translator::translate("actions/createResourceBooking"));
		define("CONTROLLER_ACTION_UPDATE_RESOURCE_BOOKING", Translator::translate("actions/updateResourceBooking"));
		define("CONTROLLER_ACTION_DELETE_RESOURCE_BOOKING", Translator::translate("actions/deleteResourceBooking"));

		define("CONTROLLER_ACTION_CREATE_BOOKING", Translator::translate("actions/createResourceBooking"));
		define("CONTROLLER_ACTION_UPDATE_BOOKING", Translator::translate("actions/updateResourceBooking"));
		define("CONTROLLER_ACTION_DELETE_BOOKING", Translator::translate("actions/deleteResourceBooking"));
		
		define("CONTROLLER_ACTION_CANCEL_TRANSACTION",Translator::translate("actions/cancelTransaction"));
		define("CONTROLLER_ACTION_CANCEL_ORDER_PAYMENT",Translator::translate("actions/cancelOrderPayment"));
		
		define("CONTROLLER_ACTION_LIST_ORGANIZER_EVENTS",Translator::translate("actions/listOrganizerEvents"));
		
		define("CONTROLLER_ACTION_JOIN_GROUP",Translator::translate("actions/joinGroup"));
		define("CONTROLLER_ACTION_ACCOMODATION", "accommodation");
		define("CONTROLLER_ACTION_ORDERS", "orders");
		define("CONTROLLER_ACTION_USER", "user");
		define("CONTROLLER_ACTION_AVAILABLE_GROUPS"	, "availableGroups");
		define("CONTROLLER_ACTION_ADD_MEMBERSHIP" , "addMembership");
		define("CONTROLLER_ACTION_GET_AVAILABLE_SLEEPING_SLOTS" , "availableAccommodations");
		define("CONTROLLER_ACTION_ACCOMMODATION" , "saveAccommodation");
		define("CONTROLLER_ACTION_SAVE_ACCOMMODATION" , "saveAccommodation");
		define("CONTROLLER_ACTION_LEAVE_GROUP" ,Translator::translate("actions/leaveGroup"));
		define("CONTROLLER_ACTION_CREATE_GROUP"	,Translator::translate("actions/createGroup"));
		define("CONTROLLER_ACTION_DELETE_GROUP"	,Translator::translate("actions/deleteGroup"));
		
		define("CONTROLLER_ACTION_SAVE_USER"	,Translator::translate("actions/saveUser"));
        define("CONTROLLER_ACTION_GET_CURRENT_USER","GetCurrentUser");
		define("CONTROLLER_ACTION_FORGOT_PASSWORD",Translator::translate("actions/forgotPassword"));

		define("CONTROLLER_TARGET_ACCOMMODATION" , "accommodation");
		define("CONTROLLER_TARGET_BOOKINGS" , "bookings");
		define("CONTROLLER_ACTION_AVAILABLE_RESOURCE_TYPES" , "availableResourceTypes");
		define("CONTROLLER_ACTION_SAVE_SLEEPING_OCCATIONS", "sleepingOccations");
		define("CONTROLLER_ACTION_OCCATIONS", "occations");
		define("CONTROLLER_ACTIONS",true);
	}