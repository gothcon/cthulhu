<?php
interface IController{

	/** @return View */
	function HandleRequest($url);
}