<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PublicUserController
 *
 * @author Joakim
 */
require_once("lib/Controllers/Controller.php");



class UserController extends Controller {
	
	function getViewDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Views/User";
	}
	
	function getModelDirectory(){
		return dirname(__FILE__) . DIRECTORY_SEPARATOR."../Models/User";
	}
	
	protected function getDefaultAction() {
		return 	CONTROLLER_ACTION_LOGIN;
	}	
	
	protected function parseUrl($segments, &$action, &$targetType, &$identifiers) {
                if(count($segments) == 1 && $segments[0] == "current"){
                    $action = CONTROLLER_ACTION_GET_CURRENT_USER;
                    return true;
                }
            
		if(count($segments) == 1 && $segments[0] == CONTROLLER_ACTION_FORGOT_PASSWORD){
			$action = $segments[0];
			return true;
		}
		return parent::parseUrl($segments, $action, $targetType, $identifiers);
	}
	
	function createLoginView(){
		require_once("{$this->getViewDirectory()}/Login.php");
		$view = new LoginView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	function createLoginModel(){
		require_once("{$this->getModelDirectory()}/LoginModel.php");
		$model = new LoginModel();
		return $model;
	}

	function createResetPasswordView(){
		require_once("{$this->getViewDirectory()}/ResetPassword.php");
		$view = new ResetPasswordView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	function createResetPasswordModel(){
		require_once("{$this->getModelDirectory()}/ResetPasswordModel.php");
		$model = new ResetPasswordModel();
		return $model;
	}
	
	function createSetNewPasswordView(){
		require_once("{$this->getViewDirectory()}/SetNewPassword.php");
		$view = new SetNewPasswordView();
		$view->setInternalLinkBuilder($this->internalLinkBuilder);
		return $view;
	}
	
	static function getUserLevelSelectData(){
		return array(
			array("label" => Translator::translate("user/userLevels/visitor"),"value" => UserLevel::VISITOR),
			array("label" => Translator::translate("user/userLevels/user"),"value" => UserLevel::USER),
			array("label" => Translator::translate("user/userLevels/organizer"),"value" => UserLevel::ORGANIZER),
			array("label" => Translator::translate("user/userLevels/worker"),"value" => UserLevel::WORKER),
			array("label" => Translator::translate("user/userLevels/staff"),"value" => UserLevel::STAFF),
			array("label" => Translator::translate("user/userLevels/administrator"),"value" => UserLevel::ADMIN)
		);
	}
	
        protected function handleJsonRequest($action, $target, $identifiers) {
            if($action == CONTROLLER_ACTION_GET_CURRENT_USER){
                $view = $this->createJsonView();
                $model = $this->createJsonModel();
                
                $currentUser = new stdClass();
                $currentUser->username = UserAccessor::getCurrentUser()->username;
                $currentUser->level = UserAccessor::getCurrentUser()->level;
                $currentUser->id = UserAccessor::getCurrentUser()->id;
                
                $model->setData($currentUser);
                $view->setModel($model);
            }else{
                $view = parent::handleJsonRequest($action, $target, $identifiers);
            }
            return $view;
        }
	
	function handleAction($action,$target,$identifiers){
 
            if($action == CONTROLLER_ACTION_LOGIN){
                    $view = $this->createLoginView();
                    $model = $this->createLoginModel();
                    if($this->isPostback()){
                            $username = fromPost('username');
                            $password = fromPost('password');
                            if(static::login($username, $password)){

                                    $returnUrl = fromGet("return_url");

                                    if(strpos($returnUrl,"?"))
                                        list($controllerPath,$foo) = explode("?",$returnUrl);
                                    else {
                                        $controllerPath = $returnUrl;
                                    }
                                    $loginFormUrl = $this->internalLink("user", CONTROLLER_ACTION_LOGIN);

                                    if(substr_count($loginFormUrl, $controllerPath) > 0)
                                        $url = Application::GetConfigurationValue("SiteRoot",true).Application::getConfigurationValue("ApplicationPath",true);
                                    else
                                        $url = fromGet("return_url");                             
                                    return new RedirectView($url);
                            }else{
                                $model->setErrorMessage(Translator::translate("user/wrongCredentials"));
                            }
                    }
                    $returnUrl = fromGet("return_url",false);
                    if(!$returnUrl){
                            if($action  == CONTROLLER_ACTION_LOGIN){
                                    $returnUrl = $this->internalLink("index");
                            }else{
                                    $returnUrl = $_SERVER['SCRIPT_URL'];
                            }
                    }
                    $returnUrl = urlencode($returnUrl);
                    $model->setReturnUrl($returnUrl);
                    $model->setTitle("Logga in");
                    $view->setModel($model);
                    // $view = new PublicMaster($view);
            }
            else if($action == CONTROLLER_ACTION_LOGOUT){
                    static::logout();
                    $url = Application::GetConfigurationValue("SiteRoot",true).Application::getConfigurationValue("ApplicationPath",true);
                    $view = new RedirectView($url);
            }else if($action == CONTROLLER_ACTION_FORGOT_PASSWORD){
                $view = $this->handleResetPasswordRequest($action, $target, $identifiers);
            }
            else{
                $view = parent::HandleAction($action, $target, $identifiers);
            }
            return $view;
	}
	
	static public function login($username,$password){
            $user = User::getUserByUsernameAndPassword($username,$password);
			if(!$user && $username != ""){
				$users = User::getUsersByEmailAddress($username);
				if(count($users) == 1){
					$user = User::getUserByUsernameAndPassword($users[0]->username,$password);
				}
			}
            if($user && $user->is_active){
                    $user->most_recent_login = strftime("%Y-%m-%d %H:%M:%S",time());
                    $user->save();

                    UserAccessor::setCurrentUser($user);
                    $order = Order::loadOpen($user->person_id);
                    PublicCartAccessor::setCurrentCustomer(Person::loadById($user->person_id));
                    $defaultOrder = new Order;
                    $defaultOrder->person_id = $user->person_id;
                    PublicCartAccessor::setCurrentCart($order ? $order : $defaultOrder);
                    return true;
            }else{
                    return false;
            }
	}
	
	static public function logout(){
            UserAccessor::clearCurrentUser();
            PublicCartAccessor::clearCart();
            CartAccessor::clearCart();
            WizardAccessor::clearWizard();
	}
	
	protected function handleFormRequest($action, $target, $identifiers) {
            die("User form request handler not implemented");
	}
	
	protected function handleResetPasswordRequest($action,$target,$identifiers){
		$model = $this->createResetPasswordModel();
		
		if(fromGet("reset_password_token")){
			$user = User::getUserByResetPasswordToken(fromGet("reset_password_token"));
			if(!$user){
				$model->setPasswordErrorMessage("felaktig eller använd länk");
			}else{
				if(ISPOSTBACK){
					if(strlen(fromPost("password")) < 5 ){
						$model->setPasswordErrorMessage("lösenordet är för kort");
					}else if(fromPost("password") != fromPost("repeat_password")){
						$model->setPasswordErrorMessage("lösenorden matchar inte");
					}else{
						$user->setNewPassword(fromPost("password"));
						$user->reset_password_token = "";
						$user->save();
						$model->setPasswordSuccessMessage("Lösenordet har uppdaterats");
					}
				}
			}
			$view = $this->createSetNewPasswordView();
		}
		else{
			
			if(ISPOSTBACK){
				$username = fromPost("username");
				$emailAddress = fromPost("email_address");
				if($username){
					$user = User::getUserByUsername($username);
					if($user){
						$person = Person::loadById($user->person_id);
						if($person){
							$user->reset_password_token = md5(microtime());
							$user->save();
						// send mail 
							$message = $this->internalLink("user",CONTROLLER_ACTION_FORGOT_PASSWORD."?reset_password_token=".$user->reset_password_token,false);
							Application::sendMail($person->email_address,"Begäran om att nollställa lösenord på GothCons webbanmälning",$message);
						}

					}else{
						$model->setUsernameErrorMessage("Användaren hittades inte");
					}
				}else if($emailAddress){
					$users = User::getUsersByEmailAddress($emailAddress);
					if(count($users)){
						$message = "Foljande anvandare är registrerade med denna epostadress:";
						foreach($users as $user){
							$message .= "\n" . $user->username;
						}
						$message .="\n";
						Application::sendMail($emailAddress,"Dina användare på GothCons webbanmälning",$message);
					}else{
						$model->setEmailAddressErrorMessage("Inga användare har registrerat sig med den epostadressen!");
					}
				}else{
					if(fromPost("email_address",-1) != -1){
						$model->setEmailAddressErrorMessage("Du måste ange en epostadress!");
					}else if (fromPost("username",-1) != -1) {
						$model->setUsernameErrorMessage("Du måste ange ett användarnamn!");
					}
				}
			}
			$view = $this->createResetPasswordView();
			
		}
		
		$view->setModel($model);

		return $view;
	}
	
        public function userHasPermisson($action) {
            return parent::userHasPermisson($action);
        }
        
}

?>
