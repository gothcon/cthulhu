<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application
 *
 * @author Joakim
 */
require_once("lib/Doctrine/bootstrap.php");
require_once("lib/ClassLib/IRequestHandler.php");
require_once("lib/ClassLib/ConfigurationManager.php");
require_once("lib/ClassLib/UrlHandler.php");
require_once("lib/ClassLib/Translator.php");
require_once("lib/ClassLib/LoggingManager.php");
require_once("lib/ClassLib/Timer.php");
require_once("lib/SessionAccessors/LanguageStringAccessor.php");
require_once("lib/Database/MySQL/MySQLRepository.php");
require_once("lib/Database/MySQL/MySQLEntityClassConfigurator.php");
require_once("lib/ClassLib/Global.php");
class Application implements IRequestHandler{
	/**
	 * @var ConfigurationManager 
	 */
	private $configManager;
	/**
	 *
	 * @var UrlHandler
	 */
	private $urlHandler;
	/**
	 *
	 * @var bool
	 */
	private $useFriendlyUrls;
	/**
	 *
	 * @var LoggingManager
	 */
	private $loggingManager;
	
	/**
	 *
	 * @var SimpleXMLElement
	 */
	private $mappingsFile;

	/**
	 * 
	 * @param string $absolutePathToConfigFile
	 */
	function __construct($absolutePathToConfigDirectory){
		if(static::$_instance !== null){
			die("only one instance can be created per request");
		}
		
		$absolutePathToConfigFile = $absolutePathToConfigDirectory."/Site.ini";
		
		// Include session accessors
		$dirName_SessionAccessors=dirname(__FILE__)."/../SessionAccessors";
		static::includeDirectoryOnce($dirName_SessionAccessors);
		
		// Configuration manager
		$this->configManager = new ConfigurationManager($absolutePathToConfigFile);
		
		// Paths
		$siteUrl = $this->configManager->getConfigurationValue("SiteRoot", true);
		$applicationPath = $this->configManager->getConfigurationValue("ApplicationPath", true);
		
		// Urlhandling
		$this->useFriendlyUrls = $this->configManager->getConfigurationValue("Use friendly urls",true);
		$this->urlHandler = new UrlHandler($siteUrl, $applicationPath,$this->useFriendlyUrls);
		
		// Router and controller mappings 
		$mappingsPath = $this->configManager->getConfigurationValue("RequestMappingFilePath",true);
		$this->mappingsFile = simplexml_load_file($mappingsPath);
		
		// Translation
		Translator::setLang(LanguageStringAccessor::getCurrentLanguage() ? LanguageStringAccessor::getCurrentLanguage() : "sv_SE");
		Translator::setPathToLanguageFiles($this->configManager->getConfigurationValue("LanguageRoot"));
		
		// Logging
		$this->loggingManager = new LoggingManager($this->configManager->getConfigurationValue("logFile",true));
		
		// Email
		ini_set("SMTP", $this->configManager->getConfigurationValue("email_smtp"));
		ini_set("smtp_port", $this->configManager->getConfigurationValue("email_smtp_port"));
		
		// Database
		MySQLEntityClassConfigurator::initRepositories();
		MySQLRepository::setConnection($this->getPDO());
                
                
		static::$_instance = $this;
	}

	/**
	 * 
	 * @param string $pathToDirectory
	 */
	public static function includeDirectoryOnce($pathToDirectory){
            $dirHandle = dir($pathToDirectory);
            while (($entry = $dirHandle->read()) !== false) {
                if($entry{0} != ".")
                   require_once($pathToDirectory."/{$entry}");
            }
            $dirHandle->close();
	}
	
	/**
	 *
	 * @var Application
	 */
	private static $_instance = null;
	
	/**
	 * 
	 * @param string|stdClass|array $mixed
	 */
	public static function log($mixed){
		if(static::$_instance == null)
			die("no instance exists");
		static::$_instance->loggingManager->log($mixed);
	}

	/**
	 * 
	 * @return C
	 */
	public static function getConfigurationValues(){
		if(static::_instance == null)
			die("no instance exists");
		return static::$_instance->configManager->getConfigurationValues();
	}

	/**
	 * 
	 * @param string $entryKey
	 * @param bool $dieIfNotSet
	 * @return string
	 */
	public static function getConfigurationValue($entryKey,$dieIfNotSet = false){
		if(static::$_instance == null)
			die("no instance exists");
		return static::$_instance->configManager->getConfigurationValue($entryKey,$dieIfNotSet);
	}
	
	/**
	 * @return string
	 */
	public static function getRootUrl(){
		return static::getConfigurationValue("SiteRoot");
	}

	/**
	 * @return string
	 */
	public static function getApplicationPath(){
		return static::getConfigurationValue("ApplicationPath");
	}
	
	/**
	 * @return string
	 */
	public static function getApplicationUrl(){
		return static::getConfigurationValue("SiteRoot") . static::getConfigurationValue("ApplicationPath");
	}
	
	/**
	 * @return string
	 */
	public static function getFilesPath(){
		return static::getConfigurationValue("DocumentRoot") . "/Files";
	}

	/**
	 * @return string
	 */
	public static function getDocumentRoot(){
		return static::getConfigurationValue("DocumentRoot");
	}
	
	/**
	 * @return string
	 */
	public static function getDocumentPath(){
		return static::getConfigurationValue("DocumentRoot") . "/Files";
	}
	
	/**
	 * @return string
	 */
	public static function getFilesRootUrl(){
		return static::getApplicationUrl() . "/Files";
	}
	
	/**
	 * 
	 * @param string $url
	 */
	public function handleRequest($url = null) {
           
            // Update language
            if(fromGet("lang")){
                if(Translator::setLang(fromGet("lang"))){
                        LanguageStringAccessor::setCurrentLanguage(fromGet("lang"));
                }
            }

            $applicationRelativeRequest = $this->urlHandler->getApplicationRelativeUrl($url);

            $segments = Application::splitUrl($applicationRelativeRequest);
            
			try{
				if(count($segments)){
					$localizedName = $segments[0];
					$name = $this->getRouterName($localizedName,LanguageStringAccessor::getCurrentLanguage());
					$router = $this->getRouter($name);

					if($router == null){
							$router = $this->getDefaultRouter();
							$router->handleRequest($applicationRelativeRequest);
					}else{
							$router->handleRequest(count($segments) > 1 ? $segments[1] : "");
					}

				}else{
					$router = $this->getDefaultRouter();
					$router->handleRequest("");
				}
			}  catch (Exception $e){
				header("HTTP/1.0 400 Bad request");
				print_r($e->getMessage());
			}
            
		
	}

	/**
	 * @var array
	 */
	private $routerNamesTranslator = null;
	/**
	 * 
	 * @param string $localizedName
	 * @param string $lang
	 * @return string
	 */
	private function getRouterName($localizedName,$lang){
		if($this->routerNamesTranslator == null){
			$this->routerNamesTranslator = array();
			$xpathExpression = "./localizedRouterNames/".$lang;
			$localizedRouterNamesLists = $this->mappingsFile->xpath($xpathExpression);
			$aliases = (array)$localizedRouterNamesLists[0];
			foreach($aliases as $key => $value){
				$this->routerNamesTranslator[$value] = $key;
			}
		}
		if(key_exists($localizedName, $this->routerNamesTranslator)){
			return $this->routerNamesTranslator[$localizedName];
		}
		return $localizedName;
	}
	
	/**
	 * 
	 * @param string $url
	 * @return array
	 */
	private static function splitUrl($url){
            
            if(strlen($url) == 0){
                return array();
            }
            $url = preg_replace("/^\/|\/$/","", $url);
            
            if(strlen($url) == 0){
                return array();
            }
            
            return explode("/",$url,2);

        }
	
	/**
	 * 
	 * @return Router
	 */
	private function getDefaultRouter(){
		$nodes = $this->mappingsFile->xpath("/requestMappings/router[@default='true']");
		if(!count($nodes)){
			$nodes = $this->mappingsFile->xpath("/requestMappings/router");
		}
		if(count($nodes)){
			$attributes = $nodes[0]->attributes();
			$routerClassName = (string)$attributes["class"];
			include("lib/Routers/".$routerClassName.".php");
			$basePath = $this->urlHandler->createUrl("");
			$router = new $routerClassName($nodes[0],$this->useFriendlyUrls,$basePath);
			return $router;
		}
		die("No router found - aborting");
	}
	
	/**
	 * 
	 * @param type $name
	 * @return null|Router
	 */
	private function getRouter($name){

		$routerConfigurationData = $this->getRouterConfigurationData($name);
		if(is_null($routerConfigurationData)){
			return null;
		}
		
		$attributes = $routerConfigurationData->attributes();
		$routerClassName = (string)$attributes["class"];

		include("lib/Routers/".$routerClassName.".php");
		$routerBasePath = $this->urlHandler->createUrl("/".$this->getRouterAlias($name, LanguageStringAccessor::getCurrentLanguage()));
		
		$router = new $routerClassName($routerConfigurationData,$this->useFriendlyUrls,$routerBasePath);
		return $router;
	}
	
	
	/**
	 * @param string $routerName
	 * @return SimpleXMLElement
	 */
	private function getRouterConfigurationData($routerName){
		$xpathArg = "/requestMappings/router[@name='{$routerName}']";
		$nodes = $this->mappingsFile->xpath("/requestMappings/router[@name='{$routerName}']");
		if(count($nodes) == 0){
			return null;
		}
		return $nodes[0];
	}
	
	/**
	 * 
	 * @param string $routerName
	 * @param string $language
	 * @return string
	 */
	private function getRouterAlias($routerName,$language){
		$node = $this->mappingsFile->xpath("/requestMappings/localizedRouterNames/{$language}/{$routerName}");
		if(count($node)==0){
			return $routerName;
		}
		return (string)$node[0];
	}
	
	/**
	 * @return \PDO
	 */
	public function GetPDO(){
		$dsn = $this->configManager->GetConfigurationValue("dbDataSourceName");
		$username = $this->configManager->GetConfigurationValue("dbUsername");
		$password = $this->configManager->GetConfigurationValue("dbPassword");
		$pdo = new PDO($dsn,$username,$password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''));
		return $pdo; 
	}
	
	
	public static function sendMail($to,$from,$subject,$message){
		
		// This is a bit backwards to avoid sending loads of mails. Instead it places all recipents as BCC.
		// This means we use the from address as main recipient since the mail must be sent to someone
		$BCCRecipients = is_array($to) ? $to : (array($to));
		
		$to = $from;

		$subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
		
		$headers = array();
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=UTF-8';
		$headers[] = 'From: '.$from;
		$headers[] = 'Reply-To: '.$from;
		$headers[] = 'Bcc: '.  implode(",", $BCCRecipients);
		$headers[] = "X-Mailer: PHP/".phpversion();
		
		return mail($to, $subject, $message , implode("\n\r", $headers));
	}
	
}

?>
