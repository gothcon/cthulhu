<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Translator
 *
 * @author Joakim
 */
class Translator {
	
	static $wordList = null;
	static $defaultLang = "sv_SE";
	static $pathToLanguageFiles = "localhost/lang/";
	
// static $defaultLang = "en_EN";
	
	static function setPathToLanguageFiles($path){
		static::$pathToLanguageFiles = $path;
	}
	
	static function setLang($lang){
		switch($lang){
			case "sv_SE":
			case "en_EN":
				Translator::$defaultLang = $lang;
				return true;
			break;
			default:
				return false;
			break;
		}
	}
	
	static function getLang(){
		return Translator::$defaultLang;
	}
	
	static function out($identifier,$lang = null){
		$lang = ($lang == null ? static::$defaultLang : $lang);
		echo static::translate($identifier,$lang);
	}
	
	static function translate($identifier,$lang = null){
		
		$lang = ($lang == null ? static::$defaultLang : $lang);
		if($identifier{0} != "/")
			$identifier = "/{$identifier}";
		$originalIdentifier = $identifier;

		$identifier = "/terms{$identifier}";
			
		if(static::$wordList == null){
			 static::$wordList = simplexml_load_file(static::$pathToLanguageFiles."{$lang}.xml");
		}

		$term = static::$wordList->xpath($identifier);
		
		if($term === false || count($term) == 0 ){
			//$array = array_reverse(explode("/",$identifier));
			//return $array[0];
			return $originalIdentifier;
		}
		else{
			return "{$term[0]}";
		}
	}
}

?>
