<?php

class MessageHandler{
	private $messages = array();
	function AddInfo($text){
		$this->messages[] = new Message($text,MessageType::INFO);
	}
	function AddWarning($text){
		$this->messages[] = new Message($text,MessageType::WARNING);
	}
	function AddError($text){
		$this->messages[] = new Message($text,MessageType::ERROR);
	}
	function getMessages(){
		return $this->messages;
	}
	public function clearMessages(){
		$this->messages = array();
	}
	public function hasMessages(){
		return count($this->messages) > 0;
	}

}

class Message{
	public $text;
	public $type;

	function __construct($text,$type){
		$this->text = $text;
		$this->type = $type;
	}

	public function getText(){
		return $this->text;
	}
	public function getType(){
		return $this->id;
	}
}

class MessageType{
	const INFO = 0;
	const WARNING = 1;
	const ERROR = 2;
}