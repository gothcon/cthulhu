<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailSender
 *
 * @author Joakim
 */
class EmailSender {
	
	private $asHtml,$recipients,$message,$sender, $subject;
	
	function __construct($sender = "GothCon <noreply@gothcon.se>",$subject="",$message="",$asHtml=false){
		$this->sender = $sender;
		$this->subject = $subject;
		$this->message = $message;
		$this->asHtml = $asHtml;
		$this->recipients = array();
	}
	
	function clear(){
		$this->asHtml = false;
		$this->recipients = array();
		$this->message = "";
		$this->sender = "GothCon <noreply@gothcon.se>";
		$this->subject = "";
	}
	
	function setSender($emailAddress, $name=""){
		$this->sender = strlen($name) ? "{$name} <{$emailAddress}>" : $emailAddress;
	}
	
	function setSubject($subject){
		$this->subject = $subject;
		
	}
	
	function addRecipient($emailAddress){
		$this->recipients[strtolower($emailAddress)] = strtolower($emailAddress);
	}
	
	function addRecipients($emailAddresses){
		foreach($emailAddresses as $address)
			$this->addRecipient($address);
	}
	
	function setMessage($message,$asHtml = false){
		$this->message = $message;
		$this->htmlMode = $asHtml;
	}
	
	function log(){
		
	}
	
	function sendMessage(){
		
		ini_set("smtp_port",25);
		ini_set("SMTP","localhost");
		ini_set("sendmail_from",$this->senderEmail);
		
		if(!count($this->recipients))
			return false;
		
		$to = $this->sender;
		
		$headers = array();
		$headers[] = 'To: '.$this->sender;
		$headers[] = 'From: '.$this->sender;
		$headers[] = 'Bcc: ' . implode(",", array_unique($this->recipients));
		if($this->asHtml)
			$headers[] = 'Content-type: text/html; charset=utf-8';
		else
			$headers[] = 'Content-type: text/plain; charset=utf-8';
		
		mail($to,$this->subject , $this->message, implode("\r\n", $headers));
		$this->log();
		$this->clear();
	}
}

?>
