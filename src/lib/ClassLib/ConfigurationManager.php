<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConfigurationManager
 * @author Joakim
 */

class ConfigurationManager {
	
	/**
	 *
	 * @var array
	 */
	private $configArray = null;
	
	/**
	 * 
	 * @param string $iniFilePath
	 */
	public function __construct($iniFilePath){
		$this->configArray =  parse_ini_file($iniFilePath);
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getConfigurationValues(){
		return $this->configArray;
	}

	/**
	 * 
	 * @param string $entryKey
	 * @param bool $dieIfNotSet
	 * @return string
	 */
	public function getConfigurationValue($entryKey,$dieIfNotSet = false){
		if(!isset($this->configArray[$entryKey])){
			if($dieIfNotSet)
				die("Configuration value \"{$entryKey}\" is not set");
			return "";
		}
		return $this->configArray[$entryKey];
	}
}

?>
