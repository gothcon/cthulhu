<?php
	if(!defined("ISPOSTBACK")){
		define("ISPOSTBACK",count($_POST) > 0 ? true: false);
	}
	class Html{

		static protected $noJavascript = 1;
		
		static function SetNoJavascript($status){
			Html::$noJavascript = $status;
		}
		
		static function PrintText($unsafeText){
			echo static::renderText($unsafeText);
		}
		
		static function renderText($unsafeText){
			return $unsafeText;
		}

		static function PrintHtml($unsafeHtml){
			echo $unsafeHtml;
		}

		static function RenderUrl($unsafeHtml){
			return urlencode($unsafeHtml);
		}

		static function PrintUrl($unsafeHtml){
			echo static::RenderUrl($unsafeHtml);
		}

		static function _commonAttributes($attributeArray){
			$temp = array();
			foreach($attributeArray as $key => $value){
				if(strstr($value,"javascript") !== false){
				  $value = htmlentities($value,ENT_QUOTES,"utf-8");
				}
				$temp[] = "{$key}='".$value."'";
			}
			return implode(" ",$temp);
		}

		static function _renderCheckbox($name,$value=false,$checked=false,$attributes = array()){
			$value = $value ? $value : $name;
			if($checked)
				$attributes["checked"] = "checked";
			$attributes["value"] = $value;
			$attributes["name"] = $name;
			$attributes["type"] = "checkbox";
			return Html::_simpleTag("input",$attributes);
		}

		static function _renderRadio($name,$value=false,$checked = false,$attributes = array()){
			$value = strlen($value) ? $value : $name;
			if($checked)
				$attributes["checked"] = "checked";
			$attributes["value"] = $value;
			$attributes["name"] = $name;
			$attributes["type"] = "radio";
			return Html::_simpleTag("input",$attributes);
		}

		static function _renderPassword($name){
			$attributes = array("name" => $name, "class" => "password" , "type" => "password");
			return Html::_simpleTag("input", $attributes);
		}

		static function _renderHidden($name, $value, $attributes = array()){
			$attributes["name"] = $name;
			$attributes["value"] = $value;
			$attributes["type"] = "hidden";
			return Html::_simpleTag("input",$attributes);
		}

		static function _renderButton($label,$attributes){
			$attributes["type"] = "button";
			$attributes["value"] = $label;
			if(isset($attributes["class"]) && strlen($attributes["class"])){
				$attributes["class"] .=" button";
			}
			else{
				$attributes["class"]="button";
			}
			return Html::_simpleTag("input",$attributes);
		}

		static function _renderTextinput($name, $value, $multiline=false, $attributes = array()){
			$attributes["name"] = $name;
			if($multiline){
				isset($attributes["class"]) ? $attributes["class"] .= " textarea" : $attributes["class"] = "textarea";
				return Html::_complexTag("textarea",$attributes,htmlentities($value,ENT_QUOTES,"utf-8"));
			}
			else{
				isset($attributes["class"]) ? $attributes["class"] .= " text" : $attributes["class"] = "text";
				$attributes["value"] = htmlentities($value,ENT_QUOTES,"utf-8");
				$attributes["type"] = "text";
				return Html::_simpleTag("input",$attributes);
			}
		}

		static protected function _complexTag($name,$attributes=array(),$value=""){
			return "<{$name} ".Html::_commonAttributes($attributes) .">{$value}</{$name}>";
		}

		static protected function _simpleTag($name,$attributes =array() ){
			$os = "<{$name} " .Html::_commonAttributes($attributes) ." />";
			return $os;
		}

		static function Checkbox($name,$value=false,$checked=false,$attributes=array(),$remember=true){
			$offset = strpos($name,"[");

			if($offset !== false){
				$_name = substr($name,0,$offset);
				$key = substr($name,$offset+1,strlen($name-1));
				$currentValue = isset($_POST[$_name][$key]);
			}else{
				$currentValue = isset($_POST[$name]);
			}

			$checked = ($remember && ISPOSTBACK) ? $currentValue : $checked;
			$attributes["class"] = "checkbox";

			echo Html::_renderCheckbox($name,$value,$checked,$attributes);
		}

		static function Button($label,$attributes= array()){
			echo Html::_renderButton($label,$attributes);
		}

		static function Radio($name,$value=false,$checked=false,$attributes=array(),$remember=true){
			$attributes["class"] = isset($attributes["class"]) ? $attributes["class"]. " radio" : "radio";
			$checked = ($remember && ISPOSTBACK && isset($_POST[$name])) ? ($_POST[$name] == $value? true : false ) : $checked;
			echo Html::_renderRadio($name,$value,$checked,$attributes);
		}

		static function Password($name,$attributes = array()){
			echo Html::_renderPassword($name,$attributes);
		}

		static function Text($name,$defaultValue ="",$multiline=false, $attributes = array(),$remember = true){
			if($remember && ISPOSTBACK){
				$offset = strpos($name,"[");
				if($offset !== false){
					$_name = substr($name,0,$offset);
					$key = substr($name,$offset+1,strlen($name)-($offset+2));
					$value = isset($_POST[$_name][$key]) ? $_POST[$_name][$key] : $defaultValue;
				}else{
					$value = isset($_POST[$name]) ? $_POST[$name] : $defaultValue;
				}
			}else{
				$value = $defaultValue;
			}
			echo Html::_renderTextinput($name,$value,$multiline,$attributes);
		}

		static function Hidden($name,$defaultValue ="", $attributes = array(),$remember = true){
			$value = ($remember && ISPOSTBACK && isset($_POST[$name])) ? $_POST[$name] : $defaultValue;
			echo Html::_renderHidden($name,$value,$attributes);
		}

		static $submitButtonId = 1;
		
		static function SubmitButton($label,$buttonAttributes = array()){
			$id = "submitButton".static::$submitButtonId++;
			if(!static::$noJavascript){
				// javascript enabled
				$attributes = array();
				$attributes["type"]= "submit";
				$attributes["value"]= $label;
				$attributes["style"] ="position:absolute; right:8000px";
				$attributes["id"] = $id;
				echo Html::_simpleTag("input", $attributes);
			}else{
				$buttonAttributes["type"]= "submit";
				$buttonAttributes["value"]= $label;
				if(isset($buttonAttributes["class"])){
					$buttonAttributes["class"] .= " submitButton ";
				}
				else{
					$buttonAttributes["class"] = "submitButton";
				}
				echo Html::_simpleTag("input", $buttonAttributes);
			}
			if(!static::$noJavascript){
				$buttonAttributes["title"] = $label;
				$buttonString = $label ."<span class=\"end\"/>";
				$buttonAttributes["href"] = "#";
				$buttonAttributes["onclick"] = "$(\"#". $id ."\").click(); return false;";
				if(isset($buttonAttributes["class"])){
					$buttonAttributes["class"] .= " submitButton buttonLink";
				}
				else{
					$buttonAttributes["class"] = "submitButton buttonLink";
				}
				echo Html::_complexTag("a",$buttonAttributes,$buttonString);
			}
		}		
		
		static function DeleteButton($url,$label="ta bort",$warningMessage="Är du säker?",$buttonAttributes = array()){
			$buttonAttributes["title"] = $label;
			$buttonString = $label ."<span class=\"end\"/>";
			$buttonAttributes["href"] = $url;
			$buttonAttributes["onclick"] = "if(!confirm(\"".str_replace("\"","\\\"",$warningMessage)."\")){ return false; }";
			if(isset($buttonAttributes["class"])){
				$buttonAttributes["class"] .= " deleteButton buttonLink";
			}
			else{
				$buttonAttributes["class"] = "deleteButton buttonLink";
			}
			echo Html::_complexTag("a",$buttonAttributes,$buttonString);
		}
		
		static function LinkButton($label,$url,$buttonAttributes = array()){
			$buttonString = $label ."<span class=\"end\"/>";
			$buttonAttributes["href"] = $url;
			
			if(isset($buttonAttributes["class"])){
				$buttonAttributes["class"] .= " buttonLink";
			}
			else{
				$buttonAttributes["class"] = "buttonLink";
			}
			if(!isset($buttonAttributes["title"])){
				$buttonAttributes["title"] = $label;
			}
			echo Html::_complexTag("a",$buttonAttributes,$buttonString);
		}

		static function ReqFieldIndicator($doShow,$label="*"){
			$label = $doShow ? $label : "";
			echo Html::_complexTag("div", array("class" => "requiredFieldIndicator"),$label);
		}
		
		static function Select($name, $valuelist=array( array("label"=>"välj värde","value" => -1)), $selectedValue=-1 ,$attributes=array(),$remember=true){
			$attributes["name"] = $name;
			$optionsString = "";
			if($remember && ISPOSTBACK){
				$selectedValue = isset($_POST[$name]) ? $_POST[$name] : $selectedValue;
			}
			foreach($valuelist as $value){
				$optionAttributes = isset($value["attributes"]) ? $value["attributes"] : array();
				$optionAttributes["value"] = $value["value"];
				if($value["value"] == $selectedValue && $selectedValue != ""){
					$optionAttributes["selected"] = "selected";
				}
				if(isset($value["class"]) && strlen($value["class"])){
					$optionAttributes["class"] = $value["class"];
				}
				$optionsString .= Html::_complexTag("option",$optionAttributes,htmlspecialchars($value["label"],ENT_QUOTES,"utf-8"));
			}

			echo Html::_complexTag("select",$attributes,$optionsString);
		}

		static function Label($text,$labelFor,$attributes = array()){
			$attributes["for"] = $labelFor;
			echo Html::_complexTag("label",$attributes,$text);
		}
		
		static function FileUpload($name,$attributes=array()){
			echo Html::Hidden("MAX_FILE_SIZE","1000000");
			$attributes["type"] = "file";
			$attributes["name"] = $name;
			$attributes["class"] = isset($attributes["class"]) ? $attributes["class"]. " file" : "file";
			echo Html::_simpleTag("input", $attributes);
		}

		static function CreateHash($subject){
			return str_replace(array("å","ä","ö","Å","Ä","Ö"," "), array("a","a","o","A","A","O","_"), $subject);
		}
		
	}

class Pdf{
	public static function stripHtml($text){
		$text = preg_replace("/(<(\/?)(h1|h2|h3|h4|h5|h6|p|div|a|i|b|strong|em|ul|ol|li|dl|dd|dt)([^>]*)>)/","[$2$3]",$text);
		$text = preg_replace("/(<(\/?)([a-zA-Z0-9]*)([^>]*)>)/","",$text);
		return $text;
	}
}
	
	
	class ValidatorType{
		const REQUIRED_FIELD_VALIDATOR = 1;
		const STRING_LENGTH_VALIDATOR = 2;
		const NUMERIC_VALIDATOR = 3;
		const REGEX_VALIDATOR = 4;
		const EQUAL_TO_VALIDATOR = 5;
		const CHECKBOX_CHECKED_VALIDATOR = 6;
                const PNR_VALIDATOR = 7;
	}
