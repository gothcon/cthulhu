<?php
	function fromPost($key,$default = null){
		return isset($_POST) ? fromArray($_POST,$key,$default) : $default;
	}
	
	function fromArray($array,$mixed,$default){
		if(is_array($mixed)){
			$no_of_keys = count($mixed);
			for($i = 0; $i < $no_of_keys; $i++){
				$key_offset = ($no_of_keys - $i+1);
				if(!isset($array[$mixed[$i]])){
					return $default;
				}else{
					$array = $array[$mixed[$i]];
				}
			}
			return $array;

		}
		else{
			return isset($array[$mixed]) ? $array[$mixed] : $default;
		}

	}
            	
	function makeHashSafe($string){
		return str_replace(array("å","ä","ö","Å","Ä","Ö"," "),array("a","a","o","A","A","O","_"),  $string);
	}
	
	function fromGet($key,$default = null){
		return isset($_GET) ? fromArray($_GET,$key,$default) : $default;
	}
	
	function fromRequest($key, $default = null){
		return isset($_REQUEST) ? fromArray($_REQUEST,$key,$default) : $default;
	}
	
	function phtml($string){
		// the tags used by tiny mce
		$allowable_tags = "<table><tr><td><th><thead><tbody><tfoot>";
		$allowable_tags .= "<p><div><span><blockquote><address>";
		$allowable_tags .= "<ul><ol><li>";
		$allowable_tags .= "<strong><em><sub><pre>";
		$allowable_tags .= "<a><img>";
		$allowable_tags .= "<h1><h2><h3><h4><h5><h6><h7>";
		$string = strip_tags($string, $allowable_tags);
		echo $string;
	}
	
	function safe($string){
		return htmlentities($string,ENT_QUOTES,'UTF-8',false);
	}
	
	function psafe($string){
		echo safe($string);
	}
	
	function num($numericString,$ifNotNumeric=-1){
		return is_numeric($numericString) ? $numericString : $ifNotNumeric;
	}

	function pnum($numericString){
		echo num($numericString);
	}
	
	function isPositiveNumber($value){
		return isset($value) && is_numeric($value) && $value > 0;
	}
	
	function get_callstack() {
		$dt = debug_backtrace();
		$cs = '';
		foreach ($dt as $t) {
		  $cs .= $t['file'] . ' line ' . $t['line'] . ' function ' . $t['function'] . "()\n";
		}
		return $cs;
	}
	// Mon, 22 Jul 2002 11:12:01 GMT
	function generateExpiresString($time){
		$weekdays = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
		$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		
		$monthAsString = $weekdays[strftime("%w",$time)];
		$dayOfMonth = strftime("%d",$time);
		$monthAsString = $months[strftime("%#m",$time)];
		$year = "";
		
		
		// $string = $weekdays[strftime("",$time)]. ", " .strftime("",$time)." ", ;	
		
	}