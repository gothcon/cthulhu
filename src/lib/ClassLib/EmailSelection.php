<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailSelection
 *
 * @author Joakim
 */

class EmailSelectionType{
	const ALL = 1;
	const ORGANIZERS = 2;
	const ATTENDERS = 3;
	const SIGNUP_TYPE = 4;
}

class EmailSelection {
	var $basedOn = EmailSelectionType::ALL;
	var $event_id = 0;
	var $event_occation = 0;
	var $signup_slot = 0;
	var $signup_slot_type = 0;
	var $only_paid = false;
	var $only_approved = false;
	var $emailList = array();
	var $removedAddresses = array();

	function getEmailAddresses(){
		$emailList = $this->emailList;
		foreach($removedAddresses as $addressToRemove){
			unset($emailList[$addressToRemove]);
		}
		return $emailList;
	}
	
	function __toString(){
		switch($this->basedOn){
			case EmailSelectionType::ALL:
				return "All";
				break;
			case EmailSelectionType::ATTENDERS:
				return "Attenders";
				break;
			case EmailSelectionType::ORGANIZERS:
				return "Organizers";
				break;
			case EmailSelectionType::SIGNUP_TYPE:
				return "SignupType";
				break;
		}
		return "Error";
	}
}

?>
