<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoggingManager
 *
 * @author Joakim
 */
class LoggingManager {
	
	/**
	 *
	 * @var string
	 */
	private $logFile;
	/**
	 *
	 * @var FilePointerResource
	 */
	private $filePointer;

	/**
	 * 
	 * @param string $file
	 */
	function __construct($file = ""){
		$this->logFile = $file != "" ? $file : dirname(__FILE__).DIRECTORY_SEPARATOR."log.txt";
		@$this->filePointer = fopen($this->logFile,"a+") or die("The Logging manager does not have permission to write to the file {$file}");
	}

	function __destruct(){
		if($this->filePointer) {
			fclose($this->filePointer);
		}
	}
	
	/**
	 * 
	 * @param mixed $obj
	 * @param FilePointerResource $filePointer
	 * @param int $indent
	 */
	function log($obj,$filePointer = null,$indent = 0){
		if($filePointer==null){
			$filePointer = $this->filePointer; // = fopen($this->logFile,"a+");
		}
		
		// skriv ut värde eller "array"
		
		if(is_array($obj)){
			// skriv ut startparentes
			fwrite($filePointer,str_repeat("\t", $indent)."(\n");
			foreach($obj as $key=>$value){
				fwrite($filePointer,  str_repeat("\t", $indent+1)."[{$key}]=>");
				$this->log($value,$filePointer,$indent+1);
			}
			fwrite($filePointer,  str_repeat("\t", $indent).")\n");
		}else{
                    fwrite($filePointer,$obj."\n");
                }

	}
		
	
}

?>
