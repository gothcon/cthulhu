<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InternalLinkBuilder
 *
 * @author Joakim
 */
require_once("lib/ClassLib/Translator.php");

class InternalLinkBuilder {
	private $basePath;
		private $controllerNameTranslator;
		private $useFriendlyUrls;
		/**
		 * 
		 * @param string $basePath
		 * @param array $controllerNameTranslator
		 * @param bool $useFriendlyUrls
		 */
		public function   __construct($basePath,$controllerNameTranslator = array(),$useFriendlyUrls=true){
			$this->basePath = $basePath;
			$this->controllerNameTranslator = $controllerNameTranslator;
			$this->useFriendlyUrls = $useFriendlyUrls;
		}
		
		/**
		 * 
		 * @param string $ControllerName
		 * @param string $parameters
		 * @return string
		 */
		public function internalLink($ControllerName,$parameters=""){
			
			if(isset($this->controllerNameTranslator[$ControllerName])){
				$ControllerName = $this->controllerNameTranslator[$ControllerName];
			}
			
			if($this->useFriendlyUrls){
				return "{$this->basePath}/{$ControllerName}/{$parameters}";
			}
			else{
				$parameters = str_replace("?","&amp;", $parameters);
				return "{$this->basePath}/index.php?request_url=/{$ControllerName}/{$parameters}";
			}
		}
}

?>
