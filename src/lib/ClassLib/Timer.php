<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Timer
 *
 * @author Joakim
 */
class Timer {
	private $startTime;
	private $stopTime;
	private $name;
	static private $timers = array();
	static function createTimer($name){
		$timer = new Timer($name);
		Timer::$timers[$name] = $timer;
		return $timer;
	}

	public function __construct($name) {
		$this->startTime = microtime(true);
		$this->name = $name;
	}
	
	public function stop(){
		$this->stopTime = microtime(true);
	}
	
	public function __toString() {
		return "Name: {$this->name}, Start:{$this->startTime}, End:{$this->stopTime}, Total: ".($this->stopTime - $this->startTime);
	}
	
	public static function printTimers(){
		foreach(static::$timers as $name => $timer){
			echo $timer ."\n";
		}
	}
	
}


?>
