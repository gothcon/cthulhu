<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UrlHandler
 *
 * @author Joakim
 */
class UrlHandler{

	private $siteUrl;
	private $applicationPath;
	private $useFriendlyUrl;
	
	function __construct($siteUrl, $applicationPath,  $useFriendlyUrl=false){
		$this->siteUrl = $siteUrl;
		$this->applicationPath = $applicationPath;
		$this->useFriendlyUrl = $useFriendlyUrl;
	}
	
	function getApplicationRelativeUrl($url=null){
		if($url == null){
			if($this->useFriendlyUrl){
				// get the url from php server
				if(strpos($_SERVER["REQUEST_URI"],"?")){
					list($requestUri,$rest) = explode("?", $_SERVER["REQUEST_URI"], 2);	
				}else{
					$requestUri = $_SERVER["REQUEST_URI"];
				}
			}
			else{
				$requestUri = isset($_GET["request_url"]) ? $_GET["request_url"] : $this->applicationPath;
			}
		}else{
			$requestUri = $url;
		}
		$requestUri = str_replace($this->applicationPath,"",$requestUri);
		return $requestUri;
	}
	
	function createUrl($applicationRelativeUrl){
		if($this->useFriendlyUrl){
			$url = $this->siteUrl.$this->applicationPath.$applicationRelativeUrl;
		}  else {
			$applicationRelativeUrl = str_replace("?", "&", $applicationRelativeUrl);
			$url = $this->siteUrl.$this->applicationPath."index.php?request_url=".$applicationRelativeUrl;
		}
		return $url;
	}
}

?>
