<?php
	require_once("lib/SessionAccessors/SessionAccessor.php");
	
	class MessageAccessor extends SessionAccessor{
		protected static $prefix = "messages";
		
		public static function clearAllMessages(){
			static::clear("list");
		}
		
		public static function addInformationMessage($string,$property=""){
			$messageEntry = new MessageEntry($string,MessageType::INFO,$property);
			static::addMessageEntry($messageEntry);
		}
		
		public static function addErrorMessage($string,$property=""){
			$messageEntry = new MessageEntry($string,MessageType::ERROR,$property);
			static::addMessageEntry($messageEntry);
		}
		
		public static function addWarningMessage($string,$property=""){
			$messageEntry = new MessageEntry($string,MessageType::WARNING,$property);
			static::addMessageEntry($messageEntry);
		}
		
		protected static function addMessageEntry($messageEntry){
			$list = static::getMessages();
			$list[] = $messageEntry;
			static::set("list",$list);
		}
		
		/** @return MessageEntry[] */
		public static function getMessages($type = null){
			$list = static::get("list",array());
			
			if(is_null($type))
				return $list;
			
			$returnList = array();
			foreach ($list as $messageEntry){
				if($messageEntry->type == $type)
					$returnList[] = $messageEntry;
			}
			return $returnList;
			
		}
		
		public static function getErrorMessages(){
			return static::getMessages(MessageType::ERROR);
		}
		
		public static function getInformationMessages(){
			return static::getMessages(MessageType::INFO);
		}

		public static function getWarningMessages(){
			return static::getMessages(MessageType::WARNING);
		}
		
	}
	
	class MessageEntry{
		var $type;
		var $message;
		public function __construct($message,$type,$property = "") {
			$this->message = $message;
			$this->type = $type;
			$this->property = $property;
		}
		public function __toString() {
			return $this->message;
		}
	}
	
	class MessageType{
		const INFO = "info";
		const WARNING = "warning";
		const ERROR = "error";
	}