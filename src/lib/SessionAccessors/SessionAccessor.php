<?php

class  SessionAccessor{

	protected static $prefix = "generic_session";

	protected static function init(){
		if(!defined("SESSION_STARTED")){
			define("SESSION_STARTED",true);
			session_start();
			if(!(isset($_SESSION['_initiated']) && $_SESSION['_initiated'])){
				session_regenerate_id();
				$_SESSION['_initiated'] = true;
			}
			if(isset($_SESSION['_user_agent'])){
				if($_SESSION['_user_agent'] != md5($_SERVER['HTTP_USER_AGENT'])){ 
					$keys = array_keys($_SESSION);
					foreach( $keys as $key){
						unset($_SESSION[$key]);
					}
				}
			}else{
				$_SESSION['_user_agent'] = md5($_SERVER['HTTP_USER_AGENT']);
			}
		}
		if(!isset($_SESSION[static::$prefix]) || !is_array($_SESSION[static::$prefix]))
			$_SESSION[static::$prefix]=array();
	}

	protected static function get($name,$defaultIfNotSet = null){
		static::init();
		return isset($_SESSION[static::$prefix][$name]) 
				? $_SESSION[static::$prefix][$name] 
				: $defaultIfNotSet;
	}

	protected static function set($name,$value){
		static::init();
		$_SESSION[static::$prefix][$name] = $value;
	}

	public static function clear($name){
		static::init();
		unset($_SESSION[static::$prefix][$name]);
	}

	public static function destroy(){
		static::init();
		unset($_SESSION[static::$prefix]);
	}

}