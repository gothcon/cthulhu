<?php
	require_once("lib/ClassLib/EmailSelection.php");
	require_once("lib/SessionAccessors/SessionAccessor.php");
	
	class EmailSelectionAccessor extends SessionAccessor{

		protected static $prefix = "email_selection";
		
		public static function getSelectedEmailAddresses(){
			return static::get("adresses", array());
		}
		
		public static function setSelectedEmailAddresses($adresses){
			return static::set("adresses",$adresses);
		}
		
		public static function clearSelections(){
			return static::clear("adresses");
		}

	}