<?php
	require_once("lib/SessionAccessors/SessionAccessor.php");
	require_once("lib/Database/Entities/Order.php");
	require_once("lib/Database/Entities/Person.php");
	
	class CartAccessor extends SessionAccessor{

		protected static $prefix = "cart";
		
		/**
		 * 
		 * @return Order
		 */
		public static function getCurrentCart(){
			return static::get("cart", new Order());
		}
		public static function setCurrentCart($cart){
			return static::set("cart",$cart);
		}	
		/**
		 * 
		 * @return Person
		 */
		public static function getCurrentCustomer(){
			return static::get("customer",new Person());
		}
		public static function setCurrentCustomer($customer){
			return static::set("customer",$customer);
		}	
		public static function clearCart(){
			return static::clear("cart");
		}
		public static function clearCustomer(){
			return static::clear("customer");
		}
	}
	
	