<?php
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."SessionAccessor.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Database/Entities/Order.php");
	require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../Database/Entities/Person.php");
	
	class LanguageStringAccessor extends SessionAccessor{
		protected static $prefix = "languageString";
		
		public static function getCurrentLanguage(){
			return static::get("language", "sv_SE");
		}
		public static function setCurrentLanguage($languageString){
			return static::set("language",$languageString);
		}	
		
	}
	
	