<?php
	require_once("lib/SessionAccessors/SessionAccessor.php");
	require_once("lib/Database/Entities/User.php");
	require_once("lib/ClassLib/Wizard.php");
	
	class WizardAccessor extends SessionAccessor{
		protected static $prefix = "wizard";
		
		/**
		 * 
		 * @return Wizard
		 */
		public static function getWizard(){
			return static::get("wizard", null);
		}
		
		public static function setWizard($wizard){
			return static::set("wizard",$wizard);
		}	
		
		public static function clearWizard(){
			return static::clear("wizard");
		}
	}
	
	