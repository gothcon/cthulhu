<?php
	require_once("lib/SessionAccessors/SessionAccessor.php");
	require_once("lib/Database/Entities/User.php");
	
	class UserAccessor extends SessionAccessor{
		protected static $prefix = "user";
		
		/**
		 * 
		 * @return User
		 */
		public static function getCurrentUser(){
			return static::get("user", new User());
		}
		
		public static function setCurrentUser($user){
			return static::set("user",$user);
		}	
		
		public static function clearCurrentUser(){
			return static::clear("user");
		}
		
		public static function currentUserHasAccess($minLevel){
			return static::getCurrentUser()->userHasAccess($minLevel);
		}
		
		
	}
	
	