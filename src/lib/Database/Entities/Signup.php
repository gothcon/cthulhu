<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property int $signup_slot_id 
 * @property int $person_id
 * @property int $group_id 
 * @property int $is_approved
 * @property int $is_paid
 */
class Signup extends ReadWriteEntity{
	
	/**
	 * @var SignupSlot
	 */
	protected $signupslot;
	
	/**
	 * @var Person
	 */
	protected $person;
	
	/**
	 *
	 * @var Group
	 */
	protected $group;
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["signup_slot_id"] = null;
		$this->persistedProperties["person_id"] = null;
		$this->persistedProperties["group_id"] = null;
		$this->persistedProperties["is_approved"] = 0;
		$this->persistedProperties["is_paid"] = 0;
		$this->signupslot = null;
		$this->person = null;
		$this->group = null;
	}
	
	/**
	 * 
	 * @return Group
	 */
	public function getGroup(){
		return $this->group;
	}
	
	public function setGroup($value){
		$this->group = $value;
	}

	/**
	 * 
	 * @return Person
	 */
	public function getPerson(){
		return $this->person;
	}
	
	public function setPerson($value){
		$this->person = $value;
	}
	
	/**
	 * 
	 * @return SignupSlot
	 */
	public function getSignupSlot(){
		return $this->signupslot;
	}
	
	public function setSignupSlot($value){
		$this->signupslot = $value;
	}
	
	/**
	 * 
	 * @return stdClass
	 */
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->person = $this->person 		== null ? null : $this->person->getStandardClone();
		$stdObj->group = $this->group 			== null ? null : $this->group->getStandardClone();
		$stdObj->signupslot = $this->signupslot == null ? null : $this->signupslot->getStandardClone();
		return $stdObj;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getInformation(){
		$signupSlot = $this->signupslot;
		$string = "";
		if($signupSlot->requires_approval){
			if($this->is_approved)
				$string .="Godkänd. ";
			else
				$string .="Ej godkänd. ";
		}
		if($signupSlot->article_id){
			if($this->is_paid)
				$string .="Betalad. ";
			else
				$string .="Ej betalad. ";
		}
		return $string;
	}
	
	/**
	 * 
	 * @return SignupRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadAllFilteredByPerson($person){
		return static::getRepository()->loadAllFilteredByPerson($person);
	}
	public static function loadAllFilteredByGroup($group){
		return static::getRepository()->loadAllFilteredByGroup($group);
	}
	public static function loadByEvent($event){
		return static::getRepository()->loadByEvent($event);
	}
	public static function loadAllFilteredBySlot($slot){
		return static::getRepository()->loadAllFilteredBySlot($slot);
	}
	
	public static function saveSignups(&$signups) {
		$rep = static::getRepository()->saveSignups($signups);
		foreach ($signups as $signup) {
			$rep->save($signup);
		}
		return true;
	}
	
	public function __toString() {
		if($this->person_id){
			$name = "{$this->person->first_name} {$this->person->last_name}";
		}else{
			$name = $this->group->name;
		}
		return "Signup #{$this->id}: {$name}, {$this->signupslot->getSlotType()->name}, {$this->signupslot->getEventOccation()->getEvent()->name} - {$this->signupslot->getEventOccation()->getTimespan()}";
		
	}
	
	public static function loadAllGroupedBySignupSlotId(){
		return static::getRepository()->loadAllGroupedBySignupSlotId();
	}
	
	/**
	 * 
	 * @param int $id
	 * @return Signup
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
}

?>
