<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "SignupSlotArticle.php");


/**
 * @property int $event_occation_id 
 * @property int $slot_type_id
 * @property int $maximum_signup_count 
 * @property int $maximum_spare_signup_count
 * @property int $requires_approval 
 * @property int $is_hidden
 */
class SignupSlot extends ReadWriteEntity{
	
	/**
	 *
	 * @var EventOccation
	 */
	protected $event_occation;
	
	/**
	 *
	 * @var SlotType
	 */
	protected $slot_type;
	
	/**
	 *
	 * @var SignupSlotArticle
	 */
	protected $signup_slot_article;

	public function __construct(){
		parent::__construct();
		$this->persistedProperties["event_occation_id"] = null;
		$this->persistedProperties["slot_type_id"] = null;
		$this->persistedProperties["maximum_signup_count"] = null;
		$this->persistedProperties["maximum_spare_signup_count"] = null;
		$this->persistedProperties["requires_approval"] = 0;
		$this->persistedProperties["is_hidden"] = 1;
		$this->event_occation = null;
		$this->slot_type = null;
		$this->signup_slot_article = new SignupSlotArticle();
		$this->signup_slot_article->setSignupSlot($this);
	}
	
	public function __set($name, $value) {
		if(($name == "maximum_signup_count" || $name == "maximum_spare_signup_count")&& trim($value) == "")
			$value = null;
		parent::__set($name, $value);
	}
	
	public function getSlotType(){
		return $this->slot_type;
	}
	
	public function getDetailedDescription(){
		return $this->name;
	}
	
	public function getEventOccation(){
		return $this->event_occation;
	}
	
	public function setEventOccation($value){
		$this->event_occation = $value;
	}
	
	public function getEntrance(){
		return $this->entrance;
	}
	
	public function setEntrance($value){
		$this->entrance_id = $value->id;
		$this->entrance = $value;
	}
	
	/**
	 * 
	 * @return Article
	 */
	public function getArticle(){
		return !is_null($this->signup_slot_article) ? $this->signup_slot_article->article: new Article();
	}
	
	public function setArticle($value){
		$this->signup_slot_article->article = $value;
	}
	
	/**
	 * 
	 * @param SignupSlotArticle $value
	 */
	public function setSignupSlotArticle($value){
		$this->signup_slot_article = $value;
		$value->setSignupSlot($this);
	}

	public function getSignupSlotArticle(){
		return $this->signup_slot_article;
	}
	
	public function setSlotType($value){
		$this->slot_type = $value;
	}
	
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->event_occation = $this->event_occation == null ? null : $this->event_occation->getStandardClone();
		$stdObj->slot_type = $this->slot_type == null ? null : $this->slot_type->getStandardClone();
		$stdObj->signup_slot_article = $this->signup_slot_article == null ? null :$this->signup_slot_article->getStandardClone();
		return $stdObj;
	}
	
	public function getInformation(){
		$string = "";
		if($this->maximum_signup_count > 0){
			$string .="Begränsat antal platser: {$this->maximum_signup_count}. ";
		}
		if($this->maximum_spare_signup_count > 0){
			$string .="Antal reservplatser: {$this->maximum_spare_signup_count}. ";
		}
		if($this->requires_approval == 1){
			$string .="Kräver godkännande. ";
		}
		return $string;
	}
	
	/**
	 * @return SignupSlotRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadByEvent($event){
		return static::getRepository()->loadByEvent($event);
	}
	
	public static function loadByEventType($eventType){
		return static::getRepository()->loadByEventType($eventType);
	}
	
	/** 
	 * @param EventOccation $occation
	 * @return SignupSlot[]
	 */
	public static function loadAllFilteredByOccation($occation){
		return static::getRepository()->loadAllFilteredByOccation($occation);
	}
	
	public function __toString() {
		return "SignupSlot #{$this->id}: {$this->slot_type->name} {$this->event_occation->getEvent()->name} - {$this->event_occation->getTimespan()} ";
	}
	
	public function save(){

		// if this is a new slot...
		if($this->id == 0){
			// save the persisted properties
			parent::save();
			
			$newArticle = $this->getArticle();
			
			if($newArticle->price > 0){
				// load a fully populated signup slot
				$newSignupSlot = SignupSlot::loadById($this->id);
				$eventOccation = $newSignupSlot->getEventOccation();
				$slotType = $newSignupSlot->getSlotType();

				// set the reference to the signup slot
				$newSignupSlotArticle = $newSignupSlot->getSignupSlotArticle();
				$newSignupSlotArticle->signup_slot_id = $this->id;

				// set the article name of the 
				$newArticle->name = sprintf('Arrangemangsavgift för %1$s,%2$s (%3$s)',$eventOccation->getEvent()->name, $eventOccation->getName(), $slotType->name);
				$newArticle->p = Application::getConfigurationValue("artikelkategori_arrangemang", true);
				$newSignupSlotArticle->setArticle($newArticle);
				$newSignupSlotArticle->save();
			}
			return true;
		}

		// we update a new slot
		// get the old to use as a reference
		$oldVersion = SignupSlot::loadById ($this->id);
		$oldSignupSlotArticle = $oldVersion->getSignupSlotArticle();
		$oldArticle = $oldSignupSlotArticle->article;

		// save and reload since the article uses data from properties not loaded
		$saveSuccessful = parent::save();

		$reloadedSignupSlot = SignupSlot::loadById($this->id);
		
		if($this->getArticle()->price != 0){
			$eventOccation = $reloadedSignupSlot->getEventOccation();
			$slotType = $reloadedSignupSlot->getSlotType();
			$newSignupSlotArticle = $this->getSignupSlotArticle();
			$newSignupSlotArticle->setSignupSlot($reloadedSignupSlot);
			$newArticle = $newSignupSlotArticle->article;
			$newArticle->name = sprintf('Arrangemangsavgift för %1$s,%2$s (%3$s)',$eventOccation->getEvent()->name, $eventOccation->getName(), $slotType->name);
			$newArticle->id = $oldArticle->id;
			$newSignupSlotArticle->setArticle($newArticle);
			$newSignupSlotArticle->id = $oldSignupSlotArticle->id;
			$newSignupSlotArticle->save();
		}
		else{
			if($oldSignupSlotArticle->id){
				$oldSignupSlotArticle->delete();
			}
		}
		return $saveSuccessful;
	}
	
	/**
	 * @param int $id
	 * @return SignupSlot
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public static function deleteById($id){
		$slot = static::loadById($id);
		if(!$slot)
			die("could not find signup slot with id ({$id})");

		// delete possible signup article
		$signupSlotArticle = $slot->getSignupSlotArticle();
		if($signupSlotArticle->id)
			$signupSlotArticle->delete();

		// delete signups
		$signups = Signup::loadAllFilteredBySlot($slot);
				
		foreach($signups as $signup)
			$signup->delete();
		
		parent::deleteById($id);
	}
	
	public static function loadSignupSlots($includeSignups = true, $eventId = 0, $eventTypeId = 0 ,$personId = 0,$groupId = 0,$cardinality = "both"){
				
		if($includeSignups){
			$tree = static::getRepository ()->loadSignupSlotTree (0,0,$eventId, $eventTypeId,$cardinality);
		}
		else{
			$tree = static::getRepository ()->loadSignupStatisticsTree (0, 0, $eventId,$eventTypeId,$cardinality);
		}
		if($personId > 0){
			$personalSignups = Signup::loadAllFilteredByPerson($personId);
			$slotIds = array();
			foreach($personalSignups as $signup){
				$slotIds[$signup->signup_slot_id] = $signup->signup_slot_id;
			}
			foreach($tree as &$signupSlotArray){
				foreach($signupSlotArray as &$signupSlot)
					if(array_key_exists($signupSlot->id, $slotIds))
						$signupSlot->slot_status = "alreadySignedUp";
		
			}
		}
		
		if($groupId > 0){
			$signups = Signup::loadAllFilteredByGroup($groupId);
			$slotIds = array();
			foreach($signups as $signup){
				$slotIds[$signup->signup_slot_id] = $signup->signup_slot_id;
			}
			foreach($tree as &$signupSlotArray){
				foreach($signupSlotArray as &$signupSlot)
					if(array_key_exists($signupSlot->id, $slotIds))
						$signupSlot->slot_status = "alreadySignedUp";
		
			}
		}
		return $tree;
	}
	
	public function delete(){
		
		$signups = Signup::loadAllFilteredBySlot($this->id);
		foreach($signups as $signup)
			$signup->delete();
		return parent::delete();
	}
	
}

?>
