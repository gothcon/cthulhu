<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IBooking.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property int $resource_id 
 * @property string $description 
 */
class Booking extends ReadWriteEntity implements IBooking{
	//put your code here
	
	/**
	 *
	 * @var Resource
	 */
	protected $_resource;
	
	public function __construct() {
		parent::__construct();
		$this->persistedProperties["description"] = null;
		$this->persistedProperties["resource_id"] = null;
		$this->_resource = new Resource();
	}
	
	public function __set($name, $value) {
		if($name == "resource_id")
			$this->_resource->id = $value;
		parent::__set($name, $value);
	}
	
	public function getDescription() {
		return $this->description;
	}

	public function getEnd() {
		return "9999-12-31 23:59:59";
	}

	public function getStart() {
		return "1900-01-01 00:00:00";
	}

	public function getResource() {
		return $this->_resource;
	}

	public function setResource($resource){
		$this->_resource = $resource;
		$this->resource_id = $resource->id;
	}
	
	public static function getAvailabilityForTimespan($timespan){
		return static::getRepository()->getAvailabilityForTimespan($timespan);
	}
	
	public static function getAvailableResourcesForEvent($eventId){
		return static::getRepository()->getAvailableResourcesForEvent($eventId);
	}
	
	public static function getAvailableResourcesForEventOccation($eventOccationId){
		return static::getRepository()->getAvailableResourcesForEventOccation($eventOccationId);
	}
	
	public static function getAvailableResourcesForSleepingEvent($eventId){
		return static::getRepository()->getAvailableResourcesForSleepingEvent($eventId);
	}
	
	public static function getAvailableResourcesForSleepingEventOccation($eventOccationId){
		return static::getRepository()->getAvailableResourcesForSleepingEventOccation($eventOccationId);
	}
	
	/**
	 * 
	 * @return BookingRepository 
	 */
	public static function getRepository() {
		return  parent::getRepository();
	}
	
	public static function loadBookingsByResourceId($resourceId){
		return static::getRepository()->getBookingsByResourceId($resourceId);
	}

	public function setResourceId($id) {
		$this->resource_id = $id;
	}
	
	public function getStandardClone() {
		$clone = parent::getStandardClone();
		$clone->resource = $this->_resource->getStandardClone();
		return $clone;
	}
	
	public static function loadResourceSchedules($resourceId = false,$includeUnavailable = true){
		return static::getRepository()->loadResourceSchedules($resourceId,$includeUnavailable);
	}
	
	/** @return PdfResourceItem[] */
	public static function getPdfResourceList(){
		return static::getRepository()->getPdfResourceList();
	}
	
		
	public static function getBookingTreeForResource($resourceId){
		return static::getRepository()->getBookingTreeForResource($resourceId);
	}
	
}





?>
