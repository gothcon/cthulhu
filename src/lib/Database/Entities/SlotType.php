<?php
define('SLOTTYPE_CARDINALITY_GROUP',5);
define('SLOTTYPE_CARDINALITY_PERSON', 1);
/**
 * @property string $name
 * @property int $cardinality
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
class SlotType extends ReadWriteEntity{
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["cardinality"] = null;
	}
	
	public function __toString() {
		return "SlotType #{$this->id}: {$this->name}";
	}
	
	public function delete() {
		/** Todo fix delete function **/
		return parent::delete();
	}
	
	
}
