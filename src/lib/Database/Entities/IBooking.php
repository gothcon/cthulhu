<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IBooking
 *
 * @author Joakim
 */
interface IBooking{
	/**
	 * @return string
	 */
	function getDescription();
	
	/**
	 * @return string
	 */
	function getStart();

	/**
	 * @return string
	 */
	function getEnd();

	/**
	 * @return Resource
	 */
	function getResource();
	
	/**
	 * 
	 * @param Resource $resource
	 */
	function setResource($resource);
	
	function setResourceId($id);
}


?>
