<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
	
/**
 * @property string $first_name
 * @property string $last_name
 * @property string $email_address
 * @property int $email_is_public
 * @property string $phone_number
 * @property string $identification
 * @property string $street_address
 * @property string $zip
 * @property string $city
 * @property string $country
 * @property string $foreign_address
 * @property string $note
 * @property string $internal_note
 * @property int $is_outdated
 * @property int $xp
 */

	class Person extends ReadWriteEntity{
		public function __construct(){
			parent::__construct();
			$this->persistedProperties["first_name"] = null;
			$this->persistedProperties["last_name"] = null;
			$this->persistedProperties["email_address"] = null;
			$this->persistedProperties["email_is_public"] = 0;
			$this->persistedProperties["phone_number"] = null;
			$this->persistedProperties["identification"] = null;
			$this->persistedProperties["street_address"] = null;
			$this->persistedProperties["zip"] = null;
			$this->persistedProperties["city"] = null;
			$this->persistedProperties["country"] = null;
			$this->persistedProperties["foreign_address"] = "";
			$this->persistedProperties["note"] = null;
			$this->persistedProperties["internal_note"] = null;
			$this->persistedProperties["is_outdated"] = 0;
			$this->persistedProperties["xp"] = 0;
		}
		public function __toString(){
			return "Person #{$this->id}: {$this->first_name} {$this->last_name}"; 
		}
		
		protected function validateProperties() {
			$valid = true;
			if(trim($this->persistedProperties["first_name"]) == ""){
				$this->setValidationError("first_name", "firstNameNotSet");
				$valid = false;
			}
			if(trim($this->persistedProperties["last_name"]) == ""){
				$this->setValidationError("last_name", "lastNameNotSet");
				$valid = false;
			}
			if(trim($this->persistedProperties["email_address"]) == ""){
				$this->setValidationError("email_address", "emailAddressNotSet");
				$valid = false;
			}			
            if(!preg_match("/^.+@.+(\..+)+/", trim($this->persistedProperties["email_address"]))  ){
				$this->setValidationError("email_address", "emailAddressInvalid");
				$valid = false;
			}
			return $valid;
		}

		public static function loadSorted($property="last_name",$dir = "asc"){
			return Person::getRepository()->loadSorted($property,$dir);
		}
		
		public static function loadByEmail($emailAddress){
			return Person::getRepository()->loadByEmail($emailAddress);
		}

		
		public function delete(){
			
			$organizers = Organizer::loadByPersonId($this->id);
			foreach($organizers as $organizer){
				$organizer->delete();
			}
			
			$signups = Signup::loadAllFilteredByPerson($this->id);
			foreach($signups as $signup){
				$signup->delete();
			}
			
			$groupMemberships = GroupMembership::loadByPerson($this->id);
			foreach($groupMemberships as $groupMembership){
				$groupMembership->delete();
			}
			
			$user = User::loadByPersonId($this->id);
			if($user)
				$user->delete();
			
			$sovplats = SleepingSlotBooking::loadByPersonId($this->id);
			if($sovplats)
				$sovplats->delete();
			
			parent::delete();
		}
		
		/**
		 * 
		 * @param int $id
		 * @return Person
		 */
		public static function loadById($id) {
			return parent::loadById($id);
		}
	}
?>
