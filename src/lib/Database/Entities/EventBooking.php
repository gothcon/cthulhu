<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IBookingProxy.php");
/**
 * @property int $event_id 
 * @property int $booking_id 
 */
class EventBooking extends ReadWriteEntity implements IBookingProxy{
	/**
	 *
	 * @var Event
	 */
	private $_event;
	/**
	 * @var Booking 
	 */
	private $_booking;
	
	public function __construct() {
		parent::__construct();
		$this->persistedProperties["event_id"] = null;
		$this->persistedProperties["booking_id"] = null;
		$this->_booking = new Booking();
		$this->_event = null;
	}
	
	/**
	 * 
	 * @param Booking $booking
	 */
	public function setBooking($booking){
		$this->_booking = $booking;
		$this->_booking_id = $booking->id;
	}
	
	public function getBooking() {
		return $this->_booking;
	}
	
	public function getDescription() {
		return $this->_event != null ? $this->_event->name : $this->_booking->getDescription();
	}

	public function getResource() {
		return $this->_booking->getResource();
	}
	
	public function setResource($resource){
		$this->_booking->setResource($resource);
	}

	public function getStart() {
		return $this->_booking->getStart();
	}

	public function getEnd() {
		return $this->_booking->getEnd();
	}
	
	public function getEvent(){
		return $this->_event;
	}
	
	/**
	 * @param Event $event
	 */
	public function setEvent($event){
		$this->_event = $event;
		$this->event_id = $event->id;
		$this->_booking->description = $event->name;
	}

	public function getType() {
		return "eventBooking";
	}

	public function setResourceId($id) {
		$this->_booking->resource_id = $id;
	}
	
	public function save($overrideValidation = false){
		static::getRepository()->begin();
		
		$event = Event::loadById($this->event_id);
		if($event)
			$this->_event = $event;
		else{
			static::getRepository()->rollback();
			return false;
		}
		
		$this->_booking->save();
		$this->booking_id = $this->_booking->id;
		if(parent::save($overrideValidation)){
			static::getRepository()->commit();
			return true;
		}
		static::getRepository()->rollback();
		return false;
	}
	
	public function getStandardClone() {
		$clone = parent::getStandardClone();
		$clone->event = $this->_event->getStandardClone();
		$clone->booking = $this->_booking->getStandardClone();
		return $clone;
	}
	
	/** @return EventBookingRepository  */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	/**
	 * 
	 * @param int $id
	 * @return EventBooking[]
	 */	
	public static function loadByEventId($id){
		return static::getRepository()->loadByEventId($id);
	}

}


?>
