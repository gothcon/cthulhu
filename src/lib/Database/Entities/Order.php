<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "OrderRow.php");

/**
 * @property string $status 
 * @property string $preferred_payment_method 
 * @property float $total 
 * @property int $person_id 
 * @property string $submitted_at 
 * @property string $note  
 * @property int $has_been_submitted
 */
class Order extends ReadWriteEntity{
	
	/**
	 *
	 * @var OrderRow[]
	 */
	private $orderRows;
	private $person;
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["status"] = "cart";
		$this->persistedProperties["total"] = "0";
		$this->persistedProperties["person_id"] = "0";
		$this->persistedProperties["submitted_at"] = strftime("%Y-%m-%d %H:%M:%S",time());
		$this->persistedProperties["note"] = "";
		$this->persistedProperties["preferred_payment_method"] = null;
		$this->persistedProperties["dibs_secret"] = null;
		$this->persistedProperties["has_been_submitted"] = 0;
		$this->orderRows = array();
		$this->person = new Person();
	}
	
	
	public function setPerson($person){
		$this->person = $person;
		$this->person_id = $person->id;
	}
	
	/**
	 * @return Person
	 */
	public function getPerson(){
		if($this->person_id > 0 && !$this->person){
			$this->person = Person::loadById($this->person_id);
		}
		return $this->person;
	}
	
	/**
	 * 
	 * @param OrderRow $row
	 * @param bool $recalculate
	 */
	public function addOrderRow($row,$recalculate = true){
		$row->order_id = $this->id;
		$orderRow = $this->getOrderRow($row->article_id);
		
		if($orderRow && is_a($orderRow,"OrderRow") && is_numeric($row->article_id)){
			$this->addToExistingRow($row);
		}else{
			$this->setOrderRow($row);
		}
		if($recalculate){
			$this->recalculateOrder();
		}
		$this->is_modified = true;
	}
	
	private function addToExistingRow($row){
		$existingRow = $this->getOrderRow($row->article_id);
		$existingRow->count += $row->count;
		$this->setOrderRow($existingRow);
	}
	
	/** @param bool $recalculateRows */
	public function recalculateOrder($recalculateRows = false){
		$this->total = 0;
		foreach($this->orderRows as $index => $row){
			if($recalculateRows){
				$this->rows[$index]->recalculate();
			}
			$this->total += $this->orderRows[$index]->total;
		}
		if($this->total == 0){
			switch($this->status){
				case "submitted":
					$this->status = "paid";
			}
		}
	}
	
	public function getNumberOfArticles(){
		$count = 0;

		foreach($this->orderRows as $row){
			$count += $row->count;
		}
		return $count;
	}

	/** @return OrderRow[] **/
	public function getOrderRows(){
		return $this->orderRows;
	}
	
	public function setOrderRows($orderRows){
		$this->orderRows = array();
		$this->addOrderRows($orderRows);
	}
	
	public function addOrderRows($orderRows){
		foreach($orderRows as $row){
			$this->addOrderRow($row);
		}
		$this->recalculateOrder();
	}
	
	/** @return OrderRow **/
	public function getOrderRow($articleId){
		foreach($this->orderRows  as $orderRow){
			if($orderRow->article_id == $articleId)
				return $orderRow;
		}
		return false;
	}
	
	/**
	 * This method overwrites the old order row with the row supplied
	 * @param type $orderRow
	 */
	public function setOrderRow($orderRow){
		$rowFound = false;
		foreach($this->orderRows  as $key => $existingOrderRow){
			if($orderRow->article_id == $existingOrderRow->article_id){
				$this->orderRows[$key] = $orderRow;
				$rowFound = true;
				break;
			}
		}
		if(!$rowFound){
			$this->orderRows[] = $orderRow;
		}
		$this->recalculateOrder();
	}
	
	public function deleteOrderRow($articleId){
		foreach($this->orderRows as $key => $row){
			if($row->article_id == $articleId){
				unset($this->orderRows[$key]);
				break;
			}
		}
		$this->recalculateOrder();
		
	}
	
	public function clearOrderRows(){
		$this->orderRows = array();
		$this->recalculateOrder();
	}
	
	public function isEmpty(){
		return count($this->orderRows) == 0; 
	}
	
	private $remainingSum = null;
	public function getRemainingSum(){
		if(is_null($this->remainingSum))
			$this->remainingSum = $this->total - OrderPayment::getTotalOrderPaymentSum($this->id);
		return $this->remainingSum;
	}

	public function __get($propertyName){
		switch($propertyName){
			case "delivery_status":
				$delivered= true;
				$notDelivered = true;
				if(count($this->orderRows) == 0){
					return "not_delivered";
				}
				foreach($this->orderRows as $row){

					switch($row->status){
						case "delivered" :
							$notDelivered = false;
							$delivered = $delivered && true;
							break;
						case "not_delivered":
							$delivered = false;
							$notDelivered = $notDelivered && true;
							break;
					}
				}
				
				return ($delivered || $notDelivered && $delivered != $notDelivered) ? ($delivered ? "delivered" : "not_delivered") : "partly_delivered";
			break;
			default:
				return parent::__get($propertyName);
		}
		
	}
	
	public function save(){
		return Order::getRepository()->save($this);
	}
	
	public static function createFromCart(&$cart){
		return Order::getRepository()->save($cart);
	}
	
	public static function loadByPersonId($id,$loadOrderRows = false){
		$list = static::getRepository()->loadByPersonId($id);
		if($loadOrderRows){
			foreach($list as &$order){
				$order->setOrderRows(OrderRow::loadByOrder($order->id));
			}
		}
		return $list;
	}

	public static function loadOpen($personId){
		return static::getRepository()->loadOpen($personId);
	}
	
	/** returns no of removed rows **/
	public function removeUnavailableArticles(){
		// remove all unavailable items
		$cartRows = $this->getOrderRows();
		$itemsRemovedCount = 0;
		$updatedCartRows = array();
		foreach ($cartRows as $key => $row) {
			$cartRowArticle = Article::loadById($row->article_id);
			if($cartRowArticle && $cartRowArticle->isAvailable()){
				$updatedCartRows[$key] = $row;
			}else{
				$itemsRemovedCount++;
			}
		}
		if($itemsRemovedCount){
			$this->setOrderRows($updatedCartRows);
			$this->save();
			return $itemsRemovedCount;
		}
	}
	
	/**
	 * 
	 * @param int $id
	 * @return Order
	 */
	public static function loadById($id){
		$order = parent::loadById($id);
		if($order)
			$order->setOrderRows(OrderRow::loadByOrder($id));
		return $order;
	}
	
	
	const STATUS_CART = "cart";
	const STATUS_SUBMITTED = "submitted";
	const STATUS_PARTIALLY_PAID = "partially_paid";
	const STATUS_PAID = "paid";
	const STATUS_CANCELLED = "cancelled";
	
	public static function getStatusValues(){
		return array(static::STATUS_CART,static::STATUS_SUBMITTED, static::STATUS_PARTIALLY_PAID,static::STATUS_PAID,static::STATUS_CANCELLED);
	}
	
	public function __toString() {
		return "Order #{$this->id}: {$this->total} by {$this->getPerson()}";
	}

	public static function deleteById($id) {
		$orderRows = OrderRow::loadByOrder($id);
		foreach ($orderRows as $row) {
			$row->delete();
		}	
		return parent::deleteById($id);
	}
	
	public function loadOrderRows(){
		$this->orderRows = OrderRow::loadByOrder($this->id);
	}
	

	/**
	 * @return OrderRepository Description
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	
	public static function getCategoryOrderingStatistics($category){
		return static::getRepository()->GetCategoryOrderingStatistics($category);
	}
	
	public static function getArticleOrderingStatistics($includeOrderRows = false,$articleId = 0){
		$list = static::getRepository()->GetArticleOrderingStatistics($articleId);
		if($includeOrderRows){
			$listIndex = array();
			foreach($list as $key => $article){
				$article->setOrderRows(array());
				$listIndex[$article->article_id] = &$list[$key];
			}
			
			$rows = OrderRow::loadOrderRowStatistics($articleId);
			foreach($rows as $row){
				$listIndex[$row->article_id]->addOrderRow($row);
			}
			
		}
		if(count($list) == 0 && $articleId > 0){
			$list[] = new TreeItem_ArticleOrderingStatistics();
			$list[0]->setOrderRows(array());
		}
		return $list;
	}
	
}

?>
