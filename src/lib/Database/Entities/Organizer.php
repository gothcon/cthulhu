<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property int $person_id 
 * @property int $group_id 
 * @property int $event_id 
 * @property Person $person 
 * @property Group $group
 * @property Event $event
 */

class OrganizerType{
	public static $NOT_SET = 0;
	public static $GROUP = 1;
	public static $PERSON = 2;
}

class Organizer extends ReadWriteEntity{
	
	
	
	/**
	 *
	 * @var Person
	 */
	protected $_person;
	
	/**
	 *
	 * @var Group
	 */
	protected $_group;
	
	/**
	 * @var Event
	 */
	protected $_event;
	
	public function setEventReference(&$event){
		$this->_event = $event;
		$this->persistedProperties["event_id"] = $event->id;
	}
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["person_id"] = null;
		$this->persistedProperties["group_id"] = null;
		$this->persistedProperties["event_id"] = null;
		$this->_person = null;
		$this->_group = null;
		$this->_event = null;
	}
	
	public function __set($name, $value) {
		switch($name){
			case "event_id":
				$this->persistedProperties["event_id"] = $value;
				$this->_event = null;
				break;
			case "person_id":
				$this->_group = null;
				$this->persistedProperties["group_id"] = null;
				$this->persistedProperties["person_id"] = $value;
				$this->_person = null;
				break;
			case "group_id":
				$this->_group = null;
				$this->persistedProperties["group_id"] = $value;
				$this->persistedProperties["person_id"] = null;
				$this->_person = null;
				break;
			case "person":
				$this->_group = null;
				$this->persistedProperties["group_id"] = null;
				$this->persistedProperties["person_id"] = $value->id;
				$this->_person = $value;
				break;
			case "group":
				$this->_group = $value;
				$this->persistedProperties["group_id"] = $value->id;
				$this->persistedProperties["person_id"] = null;
				$this->_person = null;
				break;
			default:
				parent::__set($name, $value);
		}
	}
	
	public function __get($name) {
		switch($name){
			case "person" : 
				return $this->_person;
				break;
			case "group" :
				return $this->_group;
				break;
			case "event" :
				return $this->_event;
			default:
				return parent::__get($name);
		}
	}
	
	public function getOrganizerType(){
		if($this->person != null)
			return OrganizerType::$PERSON;
		if($this->group != null)
			return OrganizerType::$GROUP;
		return OrganizerType::$NOT_SET;
	}
	
	public function getGroupOrPerson(){
		if($this->person != null)
			return $this->_person;
		if($this->group != null)
			return $this->_group;
		return false;
	}
	
	public function getName(){
		
		if($this->person_id > 0)
			return "{$this->person->first_name} {$this->person->last_name}";
		else if($this->group_id > 0)
			return $this->group->name;

		return "unknown";
	}
	
	public function __toString() {
		return "Organizer #{$this->id}: " . $this->getName() . ", event_id: {$this->event_id}";
	}
	
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->person = $this->person == null ? null : $this->person->getStandardClone();
		$stdObj->group = $this->group == null ? null : $this->group->getStandardClone();
		$stdObj->event = $this->event == null ? null : $this->event->getStandardClone();
		return $stdObj;
	}

	public static function loadByPersonId($id){
		return static::getRepository()->loadByPersonId($id);
	}

	public static function loadByGroupId($id){
		return static::getRepository()->loadByGroupId($id);
	}
	
	public static function loadByEventId($id){
		return static::getRepository()->loadByEventId($id);
	}
	
	public static function loadSortedEventOrganizerList($property="event_type",$dir = "asc"){
		return Organizer::getRepository()->loadSortedEventOrganizerList(0,$property,$dir);
	}

	public static function loadPersonalSortedEventOrganizerList($personId,$property="event_type",$dir = "asc"){
		return Organizer::getRepository()->loadSortedEventOrganizerList($personId,$property,$dir);
	}
	
	/**
	 * 
	 * @return OrganizerRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function personIsEventOrganizer($personId,$eventId){
		return static::getRepository()->personIsEventOrganizer($personId, $eventId);
	}
	
	public static function personIsSlotOrganizer($personId,$slotId){
		return static::getRepository()->personIsSlotOrganizer($personId, $slotId);
	}
	
	
	
}

?>
