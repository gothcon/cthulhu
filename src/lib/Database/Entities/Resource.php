<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

// TODO: Add properties to PHPDocs
/**
 * @property string $name 
 * @property int $resource_type_id 
 * @property int $sleeping_event_occation_id
 * @property bool $available
 * @property ResourceType $resourceType
 */
class Resource extends ReadWriteEntity{
	
	protected $_resourceType;
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["resource_type_id"]= null;
		$this->persistedProperties["available"]= true;
		$this->_resourceType = null;
	}
	
	public function __set($name,$value){
		switch($name){
			case "resourceType":
				$this->persistedProperties["resource_type_id"] = $value->id;
				$this->_resourceType = $value;
				break;
			default:
				parent::__set($name,$value);
				break;
		}
	}
	
	public function __get($name){
		switch($name){
			case "resourceType":
				return $this->_resourceType;
				break;
			default:
				return parent::__get($name);
				break;
		}
	}
	
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->resource_type = $this->_resourceType == null ? null : $this->_resourceType->getStandardClone();
		return $stdObj;
	}

	/**
	 * @return ResourceRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadByType($typeId){
		return static::getRepository()->loadByType($typeId);
	}
	
	public static function loadSleepingStatistics(){
		return static::getRepository()->loadSleepingStatistics();
	}
	
	public static function loadEventBookingList($eventId = null){
		return static::getRepository()->loadEventBookingList($eventId);
	}
	
	public static function loadEventOccationBookingList($eventOccationId = null){
		return static::getRepository()->loadEventOccationBookingList($eventOccationId);
	}
	
	public function delete(){
		/** Todo: fix delete **/
		parent::delete();
	}
	
}

?>
