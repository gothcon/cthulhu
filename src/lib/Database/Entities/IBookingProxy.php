<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IBookingProxy
 *
 * @author Joakim
 */
interface IBookingProxy extends IBooking{
	/**
	 * @return Booking;
	 */
	function getBooking();
	/**
	 * 
	 * @param Booking $booking
	 */
	function setBooking($booking);
	
	function getType();
	
}


?>
