<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property string $title
 * @property string $preamble
 * @property string $text
 * @property string $author
 * @property string $publish_at
 * @property string $unpublish_At
 * @property int $is_internal
 */
class NewsArticle extends ReadWriteEntity{
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["title"] = null;
		$this->persistedProperties["preamble"] = null;
		$this->persistedProperties["text"] = null;
		$this->persistedProperties["author"] = null;
		$this->persistedProperties["publish_at"] = $this->persistedProperties["created_at"];
		$this->persistedProperties["unpublish_at"] = null;
		$this->persistedProperties["is_internal"] = null;
		
	}
	
	private $publishAtDate = null;
	private $publishAtTime = null;
	private $unpublishAtTime = null;
	private $unpublishAtDate = null;
	
	public function __set($name,$value){
		switch($name){
			case "publish_at":
				if(strtotime($value) == 0){
					$value = null;
				}
				if(!is_null($value)){
					$this->publishAtDate = substr($value,0,10);
					$this->publishAtTime = substr($value,11,8); 
				}
				else{
					$this->publishAtDate = null;
					$this->publishAtTime = null; 
				}
				parent::__set("publish_at",$value);
				break;
			
			case "unpublish_at":
				if(strtotime($value) == 0){
					$value = null;
				}
				if($value != null){
					$this->unpublishAtDate = substr($value,0,10);
					$this->unpublishAtTime = substr($value,11,8);
				}
				else{
					$this->unpublishAtDate = null;
					$this->unpublishAtTime = null; 
				}
				parent::__set("unpublish_at",$value);
				break;
			
			case "unpublish_at_time":
				if(is_null($value)  || trim($value) === ""){
					$this->unpublishAtTime = null;
					$this->persistedProperties["unpublish_at"] = null;
					
				}else{
					// either a valid timestring or null
					$value = Timespan::createTime($value);
					$this->unpublishAtTime =  strtotime("2000-01-01 {$value}") > 0 ? $value : null;
					
					if(!is_null($this->unpublishAtDate) && !is_null($this->unpublishAtTime)){
						$this->persistedProperties["unpublish_at"] = "{$this->unpublishAtDate} {$this->unpublishAtTime}";
					}else{
						$this->persistedProperties["unpublish_at"] = null;
					}
				}
					
				break;
			
			case "unpublish_at_date":
				if(is_null($value)  || trim($value) === ""){
					$this->unpublishAtDate = null;
					$this->persistedProperties["unpublish_at"] = null;
				}else{
					$this->unpublishAtDate = strtotime("{$value} 00:00:00") > 0 ? $value : null;
					if(!is_null($this->unpublishAtDate) && !is_null($this->unpublishAtTime)){
						$this->persistedProperties["unpublish_at"] = "{$this->unpublishAtDate} {$this->unpublishAtTime}";
					}else{
						$this->persistedProperties["unpublish_at"] = null;
						$value = null;
					}
				}
				break;
			
			case "publish_at_time":
				if(is_null($value)  || trim($value) === ""){
					$this->publishAtTime = null;
					$this->persistedProperties["publish_at"] = null;
				}else{
					// either a valid timestring or null
					$value = Timespan::createTime($value);
					$this->publishAtTime =  strtotime("2000-01-01 {$value}") > 0 ? $value : null;
					if(!is_null($this->publishAtDate) && !is_null($this->publishAtTime)){
						$this->persistedProperties["publish_at"] = "{$this->publishAtDate} {$this->publishAtTime}";
					}else{
						$this->persistedProperties["publish_at"] = null;
					}
				}
				break;
				
			case "publish_at_date":
				if(is_null($value)  || trim($value) === ""){
					$this->publishAtDate = null;
					$this->persistedProperties["publish_at"] = null;
					
				}else{
					$this->publishAtDate = strtotime("{$value} 00:00:00") > 0 ? $value : null;
					if(!is_null($this->publishAtDate) && !is_null($this->publishAtTime)){
						$this->persistedProperties["publish_at"] = "{$this->publishAtDate} {$this->publishAtTime}";
					}else{
						$this->persistedProperties["publish_at"] = null;
						$value = null;
					}
				}
				break;

			default:
				parent::__set($name,$value);
		}
	}
	public function __get($name){
		if($name == "unpublish_at_time"){
			return $this->unpublishAtTime;
		}
		else if($name == "unpublish_at_date"){
			return $this->unpublishAtDate;
		}
		else if($name == "publish_at_time"){
			return $this->publishAtTime;
		}
		else if($name == "publish_at_date"){
			return $this->publishAtDate;
		}
		else{
			return parent::__get($name);
		}
	}

	public function save(){

		if(strlen(trim($this->unpublish_At)) === 0){
			$this->persistedProperties["unpublish_at"] = null;
		}
		
		if(strlen(trim($this->publish_At)) === 0){
			$this->persistedProperties["publish_at"] = $this->created_at;
		}

		parent::save();
	}
	
	public function isPublished(){
		$now = time();
		$publishAt = strtotime($this->publish_at);
		$unpublishAt = strtotime($this->unpublish_at);
		
		return $now >= $publishAt && ( is_null($this->unpublish_at) ? true : $now < $unpublishAt);
	}
	
	public function __toString() {
		return "NewsArticle #{$this->id}: {$this->title} by {$this->author}";
	}
	
	public static function loadPublicList($count = 10){
		return static::getRepository()->getPublicNews($count);
	}
	
	/**
	 * 
	 * @return NewsArticleRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
}

?>
