<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property int $timespan_id
 * @property int $event_id
 * @property string $name
 * @property int $is_hidden
 * @property int $main_booking_id
 */
class EventOccation extends ReadWriteEntity{
	
	protected $timespan, $event;
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["timespan_id"] = null;
		$this->persistedProperties["event_id"]= null;
		$this->persistedProperties["name"]= "";
		$this->persistedProperties["is_hidden"]= 0;
		$this->timespan = null;
		$this->persistedProperties["main_booking_id"] = null;
		$this->event = null;
	}
	
	/**
	 * 
	 * @param int $id
	 * @return EventOccation
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public function setTimespan($timespan){
		$this->timespan = $timespan;
		$this->timespan_id = $timespan->id;
	}
	
	/**
	 * 
	 * @return Timespan
	 */
	public function getTimespan(){
		return $this->timespan;
	}
	
	public function setEvent($event){
		$this->event = $event;
	}	
	
	/**
	 * 
	 * @return Event
	 */
	public function getEvent(){
		return $this->event;
	}
	
	public function getName(){
		return $this->timespan->getShortDescription() . (strlen($this->name) ? " : {$this->name}" : "");
	}
	
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->timespan = $this->timespan == null ? null : $this->timespan->getStandardClone();
		$stdObj->event = $this->event == null ? null : $this->event->getStandardClone();
		return $stdObj;
	}
	
	public function __toString(){
		return "EventOccation #{$this->id}: {$this->event->name} {$this->name} / {$this->timespan}";
	}
	
	public static function loadEventOccationTree($eventId = 0){
		return static::getRepository()->loadEventOccationTree($eventId);
	}
	
	public static function loadByEvent($mixed){
		return static::getRepository()->loadByEvent($mixed);
	}

	public function save() {
		if($this->id == 0){
			// this is a new occation...
			if($this->timespan_id == 0){
				$timespan = $this->getTimespan();
				$timespan->save();
				$this->setTimespan($timespan);
			}
		}else{
			// load the previous version
			$previousVersion = EventOccation::loadById($this->id);

			$previousTimespan = $previousVersion->getTimespan();
		
			if($this->timespan_id != 0){
				// this is a public timespan
				if(!$previousTimespan->is_public){
					// previous was custom
					die("should not happen now");
					$previousTimespan->delete();
				}
			}else{
				// this is a custom timespan
				if(!$previousTimespan->is_public){
					// the previous was also custom. overwrite it
					$timespan->id = $previousTimespan->id;
				}
				die("should not happen either");
				$timespan->save();
				$this->setTimespan($timespan);
			}
		}
		return static::getRepository()->save($this);
	}
	
	public function delete(){
		$timespan = $this->getTimespan();

		if(!$timespan->is_public){
			Timespan::deleteById($timespan->id);
		}
		
		// signup slots
		$slots = SignupSlot::loadAllFilteredByOccation($this);
		foreach ($slots as $slot) {
			$slot->delete();
		}
		
		// event occation bookings
		$bookings = EventOccationBooking::loadByEventOccationId($this->id);
		foreach ($bookings as $booking) {
			$booking->delete();
		}
		return parent::delete();
	}
	
	/**
	 * 
	 * @return EventOccationRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadOccationAndSlots($eventOccationId){
		return static::getRepository()->loadOccationAndSlots($eventOccationId);
	}
	
}

?>
