<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property int $level
 * @property string $person_id
 * @property int $is_active
 * @property string $most_recent_login
 * @property string reset_password_token
 */

class User extends ReadWriteEntity{

	protected $newPasswordIsEmpty = false;
	protected $passwordRepeat = null;
	protected $newPassword = null;


	public function __construct(){
		parent::__construct();
		$this->persistedProperties["username"] = "";
		$this->persistedProperties["password"] = "";
		$this->persistedProperties["old_password"] = "";
		$this->persistedProperties["reset_password_token"] = "";
		$this->persistedProperties["salt"] = "";
		$this->persistedProperties["level"] = 0;
		$this->persistedProperties["person_id"] = 0;
		$this->persistedProperties["is_active"] = 0;
		$this->persistedProperties["most_recent_login"] = "0000-00-00 00:00:00";
	}
	
	/**
	 * @return UserRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	/**
	 *
	 * @param type $username
	 * @return type User
	 */
	public static function getUserByUsername($username){
		return static::getRepository()->getUserByUsername($username);
	}
	
	/**
	 * 
	 * @param string $token
	 * @return User
	 */
	public static function getUserByResetPasswordToken($token){
		return static::getRepository()->getUserByResetEmailToken($token);
	}
	
	/**
	 * 
	 * @param string $emailAddress
	 * @return array
	 */
	public static function getUsersByEmailAddress($emailAddress){
		return static::getRepository()->getUsersByEmailAddress($emailAddress);
	}

	/**
	 *
	 * @param type $username
	 * @return bool 
	 */
	public static function usernameIsTaken($username,$id=0){
		return static::getRepository()->usernameIsTaken($username,$id);
	}
	
	/**
	 * @param type $id
	 * @return User 
	 */
	public static function loadByPersonId($id){
		return static::getRepository()->loadByPersonId($id);
	}

	public function __toString() {
		return "{$this->id}. {$this->username}";
	}
	
	public static function getUserByUsernameAndOldPassword($username,$password){
		return static::getRepository()->getUserByUsernameAndOldPassword($username, $password);
	}
	
	/**
	 * 
	 * @param string $username
	 * @param string $password
	 * @return User
	 */
	public static function getUserByUsernameAndPassword($username,$password){
                if($password == "")
                    return null;
            
		$user = User::getUserByUsername($username);
		if($user && static::encryptPassword($password,$user->salt) == $user->password)
			return $user;
		else{
                    if($password == "")
			$user = static::getUserByUsernameAndOldPassword ($username, $password);
			if($user){
				$user->setNewPassword($password);
				$user->old_password = "";
				$user->save();
			}
		}
		return $user;
	}
	
	public function setNewPassword($password,$passwordRepeat = null){
		if($password == ""){
			$this->newPasswordIsEmpty = true;
		}
		$this->newPassword = $password;
		$this->passwordRepeat = $passwordRepeat;
		$this->persistedProperties["salt"] = static::createSalt();
		$this->persistedProperties["password"] = static::encryptPassword($password,$this->salt); 
		$this->is_modified = true;
	}
	
	static public function generateRandomString($length = 12 ,$allchar = "abcdefghijkmnoprstuzABCDFKLMRSTUVWX1235690#!?"){
		mt_srand ((double) microtime() * 1000000);
		$allCharCount=strlen($allchar);
		$string = "";
		for($i = 0; $i < $length;$i++)
		{
			$string .= $allchar[mt_rand(0,$allCharCount-1)];
		}
		return $string;
	}
	
	static public function createSalt(){
		return static::generateRandomString(12);
	}
	
	static public function encryptPassword($password,$salt){
		$hash = $salt . $password;
		for($i=0; $i < 1000; $i++){
			$hash = md5($hash);
		}
		return $hash;
	}
	
	public function userHasAccess($level){
		return $this->level >= $level;
	}
	
	
	protected function validateProperties() {
		$isValid = true;
		if($this->username == ""){
			$this->setValidationError("username","usernameMustBeSet");
			$isValid = false;
		}
		if(User::usernameIsTaken($this->username,$this->id)){
			$this->setValidationError("username","usernameExists");
			$isValid = false;
		}
		if($this->newPasswordIsEmpty){
			$this->setValidationError("password","passwordIsEmpty");
			$isValid = false;
		}else if($this->newPassword != $this->passwordRepeat && !is_null($this->passwordRepeat)){
			$this->setValidationError("password", "passwordsDiffer");
			$isValid = false;
		}
		return $isValid;
	}
	
	
}

class UserLevel{
	
	public static function toString($level){
		
		switch($level){
			case UserLevel::USER:
				return "user";
				break;
			case UserLevel::ORGANIZER:
				return "organizer";
				break;
			case UserLevel::WORKER:
				return "worker";
				break;
			case UserLevel::STAFF:
				return "staff";
				break;
			case UserLevel::ADMIN:
				return "administrator";
				break;			
                        case UserLevel::ABOVEADMIN:
				return "administrator";
				break;
			default:
				return "visitor";
				break;
		}
		
	}
	
	const VISITOR	= 0;
	const USER      = 1;
	const ORGANIZER	= 20;
	const WORKER	= 30;
	const STAFF	= 40;
	const ADMIN	= 100;
	const ABOVEADMIN= 1000;
}