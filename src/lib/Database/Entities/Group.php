<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property string $name
 * @property string $email_address
 * @property string $note
 * @property string $password
 * @property string $salt
 * @property string $internal_note
 * @property string $leader_membership_id
 */
class Group extends ReadWriteEntity{
	
	protected $_leaderMembership;
	
	public function __construct(){
		parent::__construct();	
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["email_address"] = null;
		$this->persistedProperties["password"] = null;
		$this->persistedProperties["salt"] = User::createSalt();
		$this->persistedProperties["note"] = null;
		$this->persistedProperties["internal_note"] = null;
		$this->persistedProperties["leader_membership_id"] = 0;
		$this->_leaderMembership = new GroupMembership();
	}
	
	public function __toString(){
		return "Group #{$this->id}: {$this->name}, {$this->email_address}";
	}
	
	public function setLeaderMembership($membership){
		$this->persistedProperties["leader_membership_id"] = $membership->id;
		$this->_leaderMembership = $membership;
		
	}
	
        /**
         * 
         * @return GroupMembership
         */
	public function getLeaderMembership(){
		return $this->_leaderMembership;
	}

	public static function loadByLeaderId($personId){
		return static::getRepository()->loadByLeaderId($personId);
	}
	
	/**
	 * 
	 * @return GroupRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadList($personIsMemberId = 0) {
		return static::getRepository()->loadList($personIsMemberId);
	}

	public static function loadAvailable($memberPersonId){
		$memberGroupList = Group::loadList($memberPersonId);
		foreach($memberGroupList as $group){
			if(!$group->is_member){
				$returnList[] = $group;
			}
		}
		return $returnList;
	}
        
        	/**
	 * 
	 * @param int $id
	 * @return Group
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public function delete() {
		$id = $this->id;
		$groupMemberships = GroupMembership::loadByGroup($id);
		foreach($groupMemberships as $membership)
			$membership->delete();
		
		$signups = Signup::loadAllFilteredByGroup($id);
		foreach($signups as $signup)
			$signup->delete();

		$organizers = Organizer::loadByGroupId($id);
		foreach ($organizers as $organizer) {
			$organizer->delete();
		}
		
		return parent::delete();
	}
	
	protected function validateProperties() {
		$isValid = true;
		if(strlen(trim($this->name)) === 0){
			$this->setValidationError ("name", "nameNotSet");
			$isValid = false;
		}
		else if (static::nameIsTaken($this->name,$this->id)){
			$this->setValidationError ("name", "nameIsTaken");
			$isValid = false;
		}
		return $isValid;
	}
	
	public static function nameIsTaken($name,$id){
		return static::getRepository()->checkName($name,$id);
	}
	
}
?>
