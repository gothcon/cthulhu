<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IBookingProxy.php");
class TimedBooking extends ReadWriteEntity implements IBookingProxy{
	
	/**
	 * @var Timespan
	 */
	private $_timespan;
	
	/**
	 * @var Booking
	 */
	private $_booking;
	
	public function __construct() {
		parent::__construct();
		$this->persistedProperties["booking_id"] = 0;
		$this->persistedProperties["starts_at"] = null;
		$this->persistedProperties["ends_at"] = null;
		$this->_booking = new Booking();
		$this->_timespan = new Timespan();
	}
	
	public function setPersistedProperty($propertyName, $propertyValue) {
		if($propertyName == "starts_at"){
			$this->_timespan->starts_at = $propertyValue;
		}else if($propertyName == "ends_at"){
			$this->_timespan->ends_at = $propertyValue;
		}
		parent::setPersistedProperty($propertyName, $propertyValue);
	}
	
	public function __set($name, $value) {
		if($name == "starts_at"){
			$this->_timespan->starts_at = $value;
		}else if($name == "ends_at"){
			$this->_timespan->ends_at = $value;
		}else{
			parent::__set($name, $value);
		}
	}
	
	public function getDescription() {
		return $this->_booking->description;
	}

	/**
	 * 
	 * @param Timespan $timespan
	 */
	public function setTimespan($timespan){
		$this->_timespan = $timespan;
	}
	
	public function getTimespan(){
		return $this->_timespan;
	}
	
	public function getEnd(){
		return $this->_timespan->ends_at;
	}

	public function getResource() {
		return $this->_booking->getResource();
	}

	public function getStart() {
		return $this->_timespan->starts_at;
	}

	public function getBooking() {
		return $this->_booking;
	}

	public function setBooking($booking) {
		$this->_booking = $booking;
	}

	public function setResource($resource) {
		$this->_booking->setResource($resource);
	}

	public function getType() {
		return "timedBooking";
	}
	
	public function setResourceId($id) {
		$this->_booking->resource_id = $id;
	}

	public function getStandardClone() {
		$clone = parent::getStandardClone();
		$clone->booking = $this->_booking->getStandardClone();
		return $clone;
	}
	
	public function delete() {
		/** Todo fix delete function **/
		return parent::delete();
	}
	
}


?>
