<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property string $name
 * @property string $starts_at
 * @property string $ends_at
 * @property int $is_public
 * @property int $show_in_schedule
 */
class Timespan extends ReadWriteEntity{
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["starts_at"] = null;
		$this->persistedProperties["ends_at"] = null;
		$this->persistedProperties["is_public"] = 0;
		$this->persistedProperties["show_in_schedule"] = 0;
	}
	
	public function __toString(){
		$year_start = substr($this->starts_at,0,4);
		$month_start = substr($this->starts_at,5,2);
		$day_start = substr($this->starts_at,8,2);
		$hour_start = substr($this->starts_at,11,2);
		$minute_start = substr($this->starts_at,14,2);
		$second_start = substr($this->starts_at,17,2);

		$year_end = substr($this->ends_at,0,4);
		$month_end = substr($this->ends_at,5,2);
		$day_end = substr($this->ends_at,8,2);
		$hour_end = substr($this->ends_at,11,2);
		$minute_end = substr($this->ends_at,14,2);
		$second_end = substr($this->ends_at,17,2);
		
		$fromFormat = '%a %d/%m %H:%M';
		if($month_end != $month_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $day_end != $day_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $hour_end != $hour_start)
			$toFormat = '%H:%M';
		else
			$toFormat = '%H:%M';
	
		$from = utf8_encode(strftime($fromFormat,strtotime($this->starts_at)));
		$to = utf8_encode(strftime($toFormat,strtotime($this->ends_at)));
	
		$string = $from ." - ". $to;
	
		if(strlen($this->name)){
			$string = "{$this->name} ({$string})";
		}
	
		// return $string;
		return $string;
	}
	
	public function getSmartIntervalString(){
		$month_start = substr($this->starts_at,5,2);
		$day_start = substr($this->starts_at,8,2);
		$hour_start = substr($this->starts_at,11,2);

		$month_end = substr($this->ends_at,5,2);
		$day_end = substr($this->ends_at,8,2);
		$hour_end = substr($this->ends_at,11,2);
		
		$fromFormat = '%a %d/%m %H:%M';
		if($month_end != $month_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $day_end != $day_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $hour_end != $hour_start)
			$toFormat = '%H:%M';
		else
			$toFormat = '%H:%M';
	
		$from = utf8_encode(strftime($fromFormat,strtotime($this->starts_at)));
		$to = utf8_encode(strftime($toFormat,strtotime($this->ends_at)));
	
		$string = $from ." - ". $to;
		if(strlen($this->name)){
			$string = "{$this->name} ({$string})";
		}
		return $string;
	}
	
	public function getSmartIntervalAsArray(){
		$month_start = substr($this->starts_at,5,2);
		$day_start = substr($this->starts_at,8,2);
		$hour_start = substr($this->starts_at,11,2);

		$month_end = substr($this->ends_at,5,2);
		$day_end = substr($this->ends_at,8,2);
		$hour_end = substr($this->ends_at,11,2);
		
		$fromFormat = '%a %d/%m %H:%M';
		if($month_end != $month_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $day_end != $day_start)
			$toFormat = '%a %d/%m %H:%M';
		else if( $hour_end != $hour_start)
			$toFormat = '%H:%M';
		else
			$toFormat = '%H:%M';
	
		$from = strftime($fromFormat,strtotime($this->starts_at));
		$to = strftime($toFormat,strtotime($this->ends_at));
	
		$string = $from ." - ". $to;
		return array("from" => $from, "to" => $to);
	}
	
	public function getShortDescription(){
		if(strlen($this->name)){
			$string = $this->name;
		}else{
			$year_start = substr($this->starts_at,0,4);
			$month_start = substr($this->starts_at,5,2);
			$day_start = substr($this->starts_at,8,2);
			$hour_start = substr($this->starts_at,11,2);
			$minute_start = substr($this->starts_at,14,2);
			$second_start = substr($this->starts_at,17,2);

			$year_end = substr($this->ends_at,0,4);
			$month_end = substr($this->ends_at,5,2);
			$day_end = substr($this->ends_at,8,2);
			$hour_end = substr($this->ends_at,11,2);
			$minute_end = substr($this->ends_at,14,2);
			$second_end = substr($this->ends_at,17,2);

			$fromFormat = '%a %d/%m %H:%M';
			if($month_end != $month_start)
				$toFormat = '%a %d/%m %H:%M';
			else if( $day_end != $day_start)
				$toFormat = '%a %d/%m %H:%M';
			else if( $hour_end != $hour_start)
				$toFormat = '%H:%M';
			else
				$toFormat = '%H:%M';

			$from = strftime($fromFormat,strtotime($this->starts_at));
			$to = strftime($toFormat,strtotime($this->ends_at));

			$string = $from ." - ". $to;
		}
	
		return $string;
	}
	
	private $startsAtDate = "";
	private $startsAtTime ="";
	private $endsAtTime = "";
	private $endsAtDate = "";
	
	public function __set($name,$value){
		if($name == "starts_at"){
			$this->startsAtDate = substr($value,0,10);
			$this->startsAtTime = substr($value,11,8); 
			parent::__set("starts_at","{$this->startsAtDate} {$this->startsAtTime}");
		}
		else if($name == "ends_at"){
			$this->endsAtDate = substr($value,0,10);
			$this->endsAtTime = substr($value,11,8);
			parent::__set("ends_at","{$this->endsAtDate} {$this->endsAtTime}");
		}
		else if($name == "ends_at_time"){
			$this->endsAtTime = Timespan::createTime($value);
			parent::__set("ends_at","{$this->endsAtDate} {$this->endsAtTime}");
		}
		else if($name == "ends_at_date"){
			$this->endsAtDate = $value;
			$this->ends_at = "{$this->endsAtDate} {$this->endsAtTime}";
		}
		else if($name == "starts_at_time"){
			$this->startsAtTime = Timespan::createTime($value);
			parent::__set("starts_at","{$this->startsAtDate} {$this->startsAtTime}");
		}
		else if($name == "starts_at_date"){
			$this->startsAtDate = $value;
			parent::__set("starts_at","{$this->startsAtDate} {$this->startsAtTime}");		
		}
		else{
			parent::__set($name,$value);
		}
	}
	public function __get($name){
		if($name == "ends_at_time"){
			return $this->endsAtTime;
		}
		else if($name == "ends_at_date"){
			return $this->endsAtDate;
		}
		else if($name == "starts_at_time"){
			return $this->startsAtTime;
		}
		else if($name == "starts_at_date"){
			return $this->startsAtDate;
		}
		else{
			return parent::__get($name);
		}
	}
	public static function createTime($value){
		if(is_null($value))
			return null;
		if(strlen($value) == 0){
			return "00:00:00";
		}else{
			$value = strlen($value) == 1 ? "0".$value : $value;
			$value = str_pad(str_replace(array(":"," "),"",$value),6,"0");
			$value = str_split($value,2);
			return implode(":",$value);
		}
	}
	
	public function getStandardClone(){
		$clone = parent::getStandardClone();
		$clone->asString = $this->__toString();
		return $clone;
	}
	
	public function overlaps($otherTimespan){
		$this_starts_at = strtotime($this->starts_at);
		$this_ends_at = strtotime($this->ends_at);
		
		$otherTimespan_starts_at = strtotime($otherTimespan->starts_at);
		$otherTimespan_ends_at = strtotime($otherTimespan->ends_at);
		
		$overlaps =	($otherTimespan_starts_at > $this_starts_at && $otherTimespan_starts_at < $this_ends_at)  || 
					($otherTimespan_starts_at < $this_starts_at && $otherTimespan_ends_at > $this_ends_at) || 
					($otherTimespan_starts_at <= $this_starts_at && $otherTimespan_ends_at >= $this_ends_at);

		return $overlaps;
	 }
	
	public static function loadAll(){
		return static::getRepository()->load();
	}
	
	/**
	 * @return TimespanRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadPublic(){
		return static::getRepository()->loadPublic();
	}
	
	public static function loadVisibleInSchedule(){
		return static::getRepository()->loadVisibleInSchedule(); 
	} 

	public static function deleteIfPublic($id){
		$timespan = static::loadById($id);
		if($timespan->is_public)
			return $timespan->delete ();
	}
	
	/**
	 * @param int $id
	 * @return Timespan
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
		
	public function delete() {
		/** Todo fix delete function **/
		return parent::delete();
	}
	
}

?>
