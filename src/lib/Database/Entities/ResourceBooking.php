<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
class ResourceBooking extends ReadOnlyEntity{
	
	protected $_resource;
	
	public function __construct(){
		parent::__construct();
		$now = time();
		$this->persistedProperties["starts_at"] = strftime("%Y-%m-%d %H:%M:%S",$now);
		$this->persistedProperties["ends_at"] = strftime("%Y-%m-%d %H:%M:%S",$now);
		$this->persistedProperties["description"]= null;
		$this->persistedProperties["entity_type"]= null;
		$this->persistedProperties["resource_id"] = null;
		$this->persistedProperties["entity_id"] = null;
		$this->_resource = null;
	}
	
	public function __set($name,$value){
		switch($name){
			case "timespan":
				$this->starts_at = $timespan->starts_at;
				$this->ends_at = $timespan->ends_at;
				break;
			case "resource":
				$this->persistedProperties["resource_id"] = $value->id;
				$this->_resource = $value;
				break;
			default:
				parent::__set($name,$value);
				break;
		}
	}
	
	public function __get($name){
		switch($name){
			case "timespan":
				$timespan = new Timespan();
				$timespan->starts_at = $this->starts_at;
				$timespan->ends_at = $this->ends_at;
				return $timespan;
				break;
			case "resource":
				return $this->_resource;
				break;
			default:
				return parent::__get($name);
				break;
		}
	}

	/**
	 * @return ResourceBookingRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadByResource($resource_id){
		return static::getRepository()->getAllFilteredByResource($resource_id);
	}
	
	public static function loadByEvent($event_id){
		return static::getRepository()->loadByEvent($event_id);
	}
	
	public static function getBookingStatus($timespanId){
		return static::getRepository()->getBookingStatus($timespanId);
	}
	
	public static function getBookingStatusByEventOccations($eventOccations){
		$bookingStatus = array();
		foreach($eventOccations as $occation){
			$bookingStatus[$occation->id] = static::getBookingStatus($occation->timespan_id);
		}
		return $bookingStatus;
	}
	
	public static function getResourceBookings($resourceId){
		return static::getRepository()->loadResourceSchedules($resourceId);
	}
	
	public static function loadResourceSchedules($resourceId=null){
		return static::getRepository()->loadResourceSchedules($resourceId);
	}
	
	public function delete(){
		/** Todo: fix delete **/
		parent::delete();
	}
	
	
}

?>