<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadOnlyEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."..".DIRECTORY_SEPARATOR . "IReadWriteEntity.php");
class ReadWriteEntity extends ReadOnlyEntity implements IReadWriteEntity{
	
	/**
	 * 
	 * @return IReadWriteRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	/**
	 * @param IReadWriteRepository $iReadWriteRepository 
	 */
	public static function setRepository($iReadWriteRepository) {
		parent::setRepository($iReadWriteRepository);
	}
	
	/**
	 * @var boolean
	 */
	protected $is_modified = true;	

	/**
	 * @var boolean
	 */
	private $is_valid = "";

	/**
	 * @var array
	 */
	private $validationErrors = array();
	
	/**
	 *
	 * @return boolean 
	 */
	public function isModified(){
		return $this->isModified;
	}
	
	public function __set($name,$value){
		$this->is_modified = true;
		parent::__set($name,$value);
	}

	/**
	 *
	 * @return boolean
	 */
	protected function validateProperties(){
		return true;
	}
	
	/**
	 *
	 * @return boolean 
	 */
	public function validate(){
		
		if($this->is_modified){
			$this->validationErrors = array();
			$this->is_valid = $this->validateProperties();
			$this->is_modified = false;
		}
		return $this->is_valid;
	}

	public function setValidationError($property,$value){
		$this->validationErrors[$property] = $value;
	}
	
	public function getValidationError($property){
		return isset($this->validationErrors[$property]) ? $this->validationErrors[$property] : "" ;
	}
	
	public function getValidationErrors(){
		if($this->is_modified || !isset($this->is_valid))
			$this->validate();
		return $this->validationErrors;
	}
	
	public function propertyIsInvalid($propertyName){
		if($this->is_modified || !isset($this->is_valid))
			$this->validate();
		return isset($this->validationErrors[$propertyName]);
	}
	
	public function delete(){
		if($this->id > 0){
			static::getRepository()->delete($this->id);
			$this->id = 0;
			return true;
		}
		return false;
	}
	
	public static function deleteById($id){
		$entity = static::loadById($id);
		return $entity->delete();
	}
	
	public function save($overrideValidation = false) {
		
		if($overrideValidation || $this->validate()){
			return static::getRepository()->save($this);
		}
		else{
			return false;
		}
	}
	
	/**
	 * 
	 * @param int $id
	 * @return ReadWriteEntity
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public function getStandardClone(){
		$anonymousObject = parent::getStandardClone();
		$anonymousObject->validationErrors = $this->getValidationErrors();
		return $anonymousObject;
	}
	
}

?>