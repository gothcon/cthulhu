<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "Organizer.php");

/**
 * @property string $name
 * @property string $preamble
 * @property string $text
 * @property string $internal_info
 * @property string $schedule_name
 * @property int $visible_in_schedule
 * @property int $visible_in_public_listing
 * @property int $eventtype_id
 */
class Event extends ReadWriteEntity{
	/**
	 *
	 * @var EventType
	 */
	protected $eventtype;
	
	/**
	 * @var Organizer 
	 */
	protected $organizer;
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = "";
		$this->persistedProperties["preamble"] = "";
		$this->persistedProperties["text"] = "";
		$this->persistedProperties["info"] = "";
		$this->persistedProperties["internal_info"] = "";
		$this->persistedProperties["schedule_name"] = "";
		$this->persistedProperties["visible_in_schedule"] = "1";
		$this->persistedProperties["visible_in_public_listing"] = "1";
		$this->persistedProperties["eventtype_id"] = null;
		$this->eventtype = null;
		$this->organizer = new Organizer();
	}
	
	public function setEventType($eventtype){
		$this->eventtype = $eventtype;
		$this->eventtype_id = $eventtype->id;
	}
	
	public function getEventType(){
		return $this->eventtype;
	}
	
	public function __toString() {
		return "Event #{$this->id}: {$this->name}";
	}

	public function save(){
		parent::save();
		//if($this->id == 0 && $this->organizer == null){
		//	$this->organizer = new Organizer();
		//}
		if($this->organizer){
			$this->organizer->setEventReference($this);
			$this->organizer->save();
                        if($this->organizer->person_id){
                                $user = User::loadByPersonId($this->organizer->person_id);
                        }
                        else if($this->organizer->group_id)
                        {
                                $person = Group::loadById($this->organizer->group_id)->getLeaderMembership()->getPerson();
                                $user = User::loadByPersonId($person->id);
                        }
                        if($user && $user->level < UserLevel::ORGANIZER){
                                $user->level = UserLevel::ORGANIZER;
                                $user->save();
                        }
                }
		return true;
	}
	
	public function delete(){
		$id = $this->id;
		$allOk = true;
		
		$repo =  static::getRepository();
		$repo->begin();
		
		// organizer
		$organizer = $this->getOrganizer();
		if($organizer && $organizer->id > 0){
			$allOk &= $organizer->delete();
		}
		// event occations
		$eventOccations = EventOccation::loadByEvent($id);
		foreach ($eventOccations as $eventOccation) {
			$allOk &= $eventOccation->delete();
		}
		
		// event bookings
		$eventBookings = EventBooking::loadByEventId($id);
		foreach ($eventBookings as $booking) {
			$allOk &= $booking->delete();
		}
		
		$allOk &= parent::delete();
		if($allOk){
			$repo->commit();
		}else{
			$repo->rollback();
		}
		return $allOk;
	}
	
	/**
	 * @return Organizer
	 */
	public function getOrganizer(){
		return $this->organizer;
	}
	
	/**
	 *
	 * @param Organizer $organizer 
	 */
	public function setOrganizer($organizer){
		$this->organizer = $organizer;
		if(!is_null($organizer))
			$organizer->setEventReference($this);
	}
	
	/**
	 *
	 * @return stdClass
	 */
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->organizer = $this->organizer->getStandardClone();
		return $stdObj;
	}
	
	/**
	 *
	 * @param string $property
	 * @param string $dir
	 * @return Event
	 */
	public static function loadSorted($property="event_type",$dir = "asc"){
		return static::getRepository()->loadSorted($property,$dir);
	}
	
	/**
	 * @param mixed $type
	 * @return Event[]
	 */
	public static function loadByType($type){
		return static::getRepository()->loadByType($type);
	}
	
	public static function loadByOrganizer($groupOrPerson){
		return static::getRepository()->loadByOrganizer($groupOrPerson);
	}
	
	public static function loadEventResourceSchedules($eventId = null){
		$treeLoader = new EventTreeLoader(static::getRepository());
		if(!is_null($eventId))
			$treeLoader->setEventFilter($eventId);
		$treeLoader->setResourceInclusion(EventTreeLoader::RESOURCE_INCLUSION_ALL);
		return $treeLoader->load();
	}
	
	/** @return EventRepository */
	public static function getRepository() {
		return parent::getRepository();
	}
	

	
	public static function loadTree(){
		$treeLoader = new EventTreeLoader(static::getRepository());
		return $treeLoader->load();
	}
	
	public static function loadPublicEventTree($eventTypeId = 0,$eventId = 0,$personId = 0){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_PUBLIC);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS);
		$treeLoader->personalize($personId);
		if($eventId)
			$treeLoader->setEventFilter($eventId);
		if($eventTypeId)
			$treeLoader->setEventTypeFilter ($eventTypeId);
		return $treeLoader->load();
	}
	
	public static function loadScheduleTree(){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_PUBLIC);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_NONE);
		return $treeLoader->load();
	}
	
	public static function loadFolderTree($eventTypeId = false){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_PUBLIC);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS);
		if($eventTypeId)
			$treeLoader->setEventTypeFilter($eventTypeId);
		return $treeLoader->load();
	}
	
	public static function loadOverviewTree($event_id = 0){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_BOTH);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_SIGNUPS);
		$treeLoader->setResourceInclusion(EventTreeLoader::RESOURCE_INCLUSION_ALL);
		if($event_id)
			$treeLoader->setEventFilter ($event_id);
		return $treeLoader->load();
	}
	
	public static function loadIndividualEventTree($personId = 0, $onlyIndividualSlots = false){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_BOTH);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS);
		$treeLoader->setExcludeEventsWithNoSlots();
		if($personId)
			$treeLoader->personalize($personId);
		if($onlyIndividualSlots)
			$treeLoader->setCardinalityFilter (EventTreeLoader::CARDINALITY_FILTER_INDIVIDUAL_EVENTS);
		
		return $treeLoader->load();
	}
	
	public static function loadGroupEventTree($groupId = 0){
		$treeLoader = new EventTreeLoader(static::getRepository());
		$treeLoader->setVisibilityFilter(EventTreeLoader::VISIBILITY_FILTER_BOTH);
		$treeLoader->setSignupAndSlotInclusion(EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS);
		$treeLoader->setCardinalityFilter(EventTreeLoader::CARDINALITY_FILTER_GROUP_EVENTS);
		$treeLoader->setExcludeEventsWithNoSlots();
		if($groupId)
			$treeLoader->groupify($groupId);
		
		return $treeLoader->load();
	}
	
	
	public static function loadList($property="event_type",$dir = "asc") {
		return static::getRepository()->loadList($property,$dir);
	}
}


class EventTreeLoader{
	
	protected $_filterOnEvent = false;
	protected $_eventId = 0;
	protected $_filterOnEventType = false;
	protected $_eventTypeId = 0;
	
	protected $_cardinalityFilter = EventTreeLoader::CARDINALITY_FILTER_BOTH;
	protected $_visibilityFilter = EventTreeLoader::VISIBILITY_FILTER_BOTH;
	
	protected $_includeEmptyTypes = false;
	protected $_includeEventsWithNoSlots = true;
	protected $_signupAndSlotInclusion = EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_NONE;
	protected $_resourceInclusion = EventTreeLoader::RESOURCE_INCLUSION_MAIN_RESOURCE;
	
	protected $_groupified = false;
	protected $_groupId = 0;
	protected $_personalized = false;
	protected $_personId = 0;
	
	/** @var EventRepository **/
	protected $_repository;
	
	/** @var TreeItem_EventType[] **/
	protected $_tree = array();
	
	/** @param type EventRepository **/
	public function __construct($repository){
		$this->_repository = $repository;
	}
	
	public function setCardinalityFilter($value){
		switch ($value) {
			case EventTreeLoader::CARDINALITY_FILTER_BOTH:
			case EventTreeLoader::CARDINALITY_FILTER_INDIVIDUAL_EVENTS:
			case EventTreeLoader::CARDINALITY_FILTER_GROUP_EVENTS:
				$this->_cardinalityFilter = $value;
				break;
			default:
				die("invalid cardinality filter: ". $value);
				$this->_cardinalityFilter = EventTreeLoader::CARDINALITY_FILTER_BOTH;
			break;
		}
	}
	
	public function setVisibilityFilter($value){
		switch ($value) {
			case EventTreeLoader::VISIBILITY_FILTER_BOTH:
			case EventTreeLoader::VISIBILITY_FILTER_PUBLIC:
				$this->_visibilityFilter = $value;
				break;
			default:
				die("invalid visibility filter: ". $value);
				$this->_visibilityFilter = EventTreeLoader::VISIBILITY_FILTER_BOTH;
			break;
		}
	}
	
	public function setSignupAndSlotInclusion($value){
		switch ($value) {
			case EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_SIGNUPS:
			case EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS:
			case EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_NONE:
				$this->_signupAndSlotInclusion = $value;
				break;
			default:
				die("invalid slot inclusion value: ". $value);
				$this->_signupAndSlotInclusion = EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_NONE;
			break;
		}
	}

	public function setResourceInclusion($value){
		switch ($value) {
			case EventTreeLoader::RESOURCE_INCLUSION_ALL:
			case EventTreeLoader::RESOURCE_INCLUSION_MAIN_RESOURCE:
			case EventTreeLoader::RESOURCE_INCLUSION_NONE:
				$this->_resourceInclusion = $value;
				break;
			default:
				die("invalid resource inclusion value: ". $value);
				$this->_resourceInclusion = EventTreeLoader::RESOURCE_INCLUSION_NONE;
			break;
		}
	}
	
	/** @return TreeItem_EventType[] */
	public function load(){
		$this->_tree = $this->loadFromDatabase();
		$this->appendExternalData();
		return $this->_tree;
	}
	
	protected function loadFromDatabase(){
		return $this->_repository->loadTree(($this->_visibilityFilter == static::VISIBILITY_FILTER_PUBLIC) , $this->_eventTypeId, $this->_eventId);
	}
	
	protected function appendExternalData(){
				
		// load the signup slots
		switch($this->_signupAndSlotInclusion){
			case EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_SIGNUPS:
				$signupSlots = SignupSlot::loadSignupSlots (true, $this->_eventId, $this->_eventTypeId,$this->_personId,$this->_groupId,$this->_cardinalityFilter);
				break;
			case EventTreeLoader::SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS:
				$signupSlots = SignupSlot::loadSignupSlots(false, $this->_eventId, $this->_eventTypeId,$this->_personId,$this->_groupId,$this->_cardinalityFilter);
				break;
			default :
				$signupSlots = array();
		}	
		
		// load the main resources
		$bookedResources = array();
		$mainResources = array();
		switch($this->_resourceInclusion){
			case EventTreeLoader::RESOURCE_INCLUSION_ALL:
				$bookedResources = EventOccationBooking::loadResourceBookingList();
			case EventTreeLoader::RESOURCE_INCLUSION_MAIN_RESOURCE:
				$mainResources = EventOccationBooking::loadMainResourceList();
				break;
			default :
				$signupSlots = array();
		}
		
		// append the extra data to the event tree
		foreach($this->_tree as $key => &$eventType){
			foreach($eventType->events as $eventKey => &$event){
				foreach ($event->event_occations as &$eventOccation){
					if(isset($signupSlots[$eventOccation->id])){
						$eventOccation->signup_slots = $signupSlots[$eventOccation->id];
						// print_r($signupSlots[$eventOccation->id]);
					}
					if(isset($mainResources[$eventOccation->id])){
						$eventOccation->main_resource = $mainResources[$eventOccation->id]->resource_name. " (".$mainResources[$eventOccation->id]->resource_type_name.")";
						$eventOccation->main_resource_id = $mainResources[$eventOccation->id]->resource_id;
					}
					if(isset($bookedResources[$eventOccation->id])){
						$eventOccation->booked_resources = $bookedResources[$eventOccation->id];
					}
				}
				if((!$this->_includeEventsWithNoSlots) && (!$event->hasSignupSlots())){
					unset($eventType->events[$eventKey]);
				}
			}
			// remove event types without events
			if(!$this->_includeEmptyTypes){
				if(count($eventType->events) == 0){
					unset($this->_tree[$key]);
				}
			}
		}
	}
	
	public function setEventFilter($eventId){
		$this->filterOnEvent = true;
		$this->_eventId = $eventId;
	}
	
	public function resetEventFilter(){
		$this->_filterOnEvent = false;
		$this->_eventId = 0;
	}
	
	public function setEventTypeFilter($eventTypeId){
		$this->_filterOnEventType = true;
		$this->_eventTypeId = $eventTypeId;
	}
	
	public function resetEventTypeFilter(){
		$this->_filterOnEventType = false;
		$this->_eventTypeId = 0;
	}
	
	public function setIncludeEmptyEventTypes(){
		$this->_includeEmptyTypes = true;
	}
	
	public function setExcludeEmptyEventTypes(){
		$this->_includeEmptyTypes = false;
	}
	
	public function setExcludeEventsWithNoSlots(){
		$this->_includeEventsWithNoSlots = false;
	}
	
	public function setIncludeEventsWithNoSlots(){
		$this->_includeEventsWithNoSlots = true;
	}
	
	/**
	 * Loads a signup tree for a person, including both group and individual events and flagging events the 
	 * person already signed up for
	 * @param int $groupId
	 */
	public function individualize(){
		$this->setCardinalityFilter(EventTreeLoader::CARDINALITY_FILTER_INDIVIDUAL_EVENTS);
	}
	
	/**
	 * Loads a signup tree for a person, including both group and individual events and flagging events the 
	 * person already signed up for
	 * @param int $groupId
	 */
	public function personalize($personId){
		$this->_personId = $personId;
		$this->_personalized = true;
	}
	
	
	/**
	 * Loads a signup tree for a group, including only group events and flagging events the group already signed up for
	 * @param int $groupId
	 */
	public function groupify($groupId){
		$this->_groupId = $groupId;
		$this->_groupified = true;
		$this->setCardinalityFilter(EventTreeLoader::CARDINALITY_FILTER_GROUP_EVENTS);
	}
	
	const CARDINALITY_FILTER_BOTH = "both";
	const CARDINALITY_FILTER_GROUP_EVENTS = "group";
	const CARDINALITY_FILTER_INDIVIDUAL_EVENTS = "person";
	
	const VISIBILITY_FILTER_BOTH = "all";
	const VISIBILITY_FILTER_PUBLIC = "public";
	
	const SIGNUP_AND_SLOT_INCLUSION_SIGNUPS = "signupsAndSlotData";
	const SIGNUP_AND_SLOT_INCLUSION_ONLY_SLOT_STATUS = "slotStatusOnly";
	const SIGNUP_AND_SLOT_INCLUSION_NONE = "noSlotStatus";
	
	const RESOURCE_INCLUSION_ALL = "includeResources";
	const RESOURCE_INCLUSION_MAIN_RESOURCE = "includeMainOnly";
	const RESOURCE_INCLUSION_NONE = "includeMainOnly";
	
}