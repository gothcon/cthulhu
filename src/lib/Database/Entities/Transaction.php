<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property float $amount
 * @property string $note
 * @property string $payment_method
 * @property int $cancelled
 */
class Transaction extends ReadWriteEntity{
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["amount"] = null;
		$this->persistedProperties["note"] = null;
		$this->persistedProperties["payment_method"] = null;
		$this->persistedProperties["cancelled"] = 0;
	}
	
	/**
	 *
	 * @param string $paymentMethod
	 * @param string $from
	 * @param string $to
	 * @return array
	 */
	public static function loadFilteredTransactions($paymentMethod = "",$from = null,$to = null) {
		return static::getRepository()->loadAllTransactions($paymentMethod, $from, $to);
	}
	
	public function __toString() {
		return "Transaction #{$this->id}: {$this->amount}, {$this->payment_method} ". ($this->cancelled ? " (cancelled) " : "");
	}
	
	public function __set($name, $value) {
		if($name == "cancelled")
			parent::__set($name, $value && true);
		else
			parent::__set($name,$value);
	}
	
	protected function validateProperties() {
		$valid = true;
		
		if(is_null($this->amount) || !is_numeric($this->amount) || $this->amount == 0 ){
			$this->setValidationError("amount", "invalidAmount");
			$valid = false;
		}
		if(is_null($this->note) || $this->note == ""){
			$this->setValidationError("note","noteMustBeSet");
			$valid = false;
		}
		if(is_null($this->payment_method) || $this->payment_method == ""){
			$this->setValidationError("payment_method", "paymentMethodMustBeSet");
			$valid = false;
		}
		
		return $valid;
	}
	
	/** @return Transaction **/
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	
}