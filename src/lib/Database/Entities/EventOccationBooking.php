<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IBookingProxy.php");
/**
 * @property int $event_occation_id 
 * @property int $booking_id 
 */
class EventOccationBooking extends ReadWriteEntity implements IBookingProxy{
	
	/**
	 * @var EventOccation
	 */
	protected $_event_occation;
	
	/**
	 * @var Booking
	 */
	protected $_booking;

	public function __construct() {
		parent::__construct();
		$this->persistedProperties["event_occation_id"] = null;
		$this->persistedProperties["booking_id"] = null;
		$this->_booking = new Booking();
		$this->_event_occation = new EventOccation();
	}

	public function getBooking() {
		return $this->_booking;
	}

	public function getDescription() {
		return $this->_booking->description;
		return $this->_event_occation->getName();
	}

	public function getEnd() {
		return $this->_event_occation->getTimespan()->ends_at;
	}

	public function getResource() {
		return $this->_booking->getResource();
	}

	public function getStart() {
		return $this->_event_occation->getTimespan()->starts_at;
	}

	public function setBooking($booking) {
		$this->_booking = $booking;
	}

	public function setResource($resource) {
		$this->_booking->setResource($resource);
	}
	
	public function setEventOccation($eventOccation){
		$this->_event_occation = $eventOccation;
		$this->event_occation_id = $eventOccation->id;
	}

	public function setEventOccationId($eventOccationId){
		$this->_event_occation->id = $eventOccationId;
		$this->event_occation_id = $eventOccationId;
	}
	
	
	public function getEventOccation(){
		return $this->_event_occation;
	}

	public function getType() {
		return "eventOccationBooking";
	}

	public function setResourceId($id) {
		$this->_booking->resource_id = $id;
	}

	public function save($overrideValidation = false){
		$this->_event_occation = EventOccation::loadById($this->event_occation_id);
		$this->_booking->description = $this->_event_occation->getEvent()->name.",".$this->_event_occation->getName();
		$this->_booking->save();
		$this->booking_id = $this->_booking->id;
		return parent::save($overrideValidation);
	}
	
	public function getStandardClone() {
		$clone = parent::getStandardClone();
		$clone->eventOccation = $this->_event_occation->getStandardClone();
		$clone->booking = $this->_booking->getStandardClone();
		return $clone;
	}
	
	/**
	 * 
	 * @return EventOccationBookingRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	public static function loadByEventId($eventId){
		return static::getRepository()->loadByEventId($eventId);
	}
	
	public static function loadAllGroupedByEventOccation($event_id=0){
		return static::getRepository()->loadAllGroupedByEventOccation($event_id);
	}
	
	/**
	 * @param int $id
	 * @return EventOccationBooking[]
	 */
	public static function loadByEventOccationId($event_occation_id){
		return static::getRepository()->loadByEventOccationId($event_occation_id);
	}
	
	public function delete() {
		$booking = $this->getBooking();
		$booking->delete();
		return parent::delete();
	}
	
	/**
	 * 
	 * @param int $id
	 * @return EventOccationBooking
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public static function loadMainResourceList($eventTypeId = 0){
		return static::getRepository()->loadEventOccationBookingList($eventTypeId,true);
	}

	public static function loadResourceBookingList($eventTypeId = 0){
		return static::getRepository()->loadEventOccationBookingList($eventTypeId,false);
	}
}


?>
