<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property string $path
 * @property string $name
 * @property string $thumbnail_small
 * @property string $content
 */
class Image extends ReadWriteEntity {
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["path"] = null;
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["thumbnail_small"] = null;
		$this->persistedProperties["content"] = null;
	}
	
	public function __toString() {
		return "Image #{$this->id}: {$this->url}";
	}

	/**
	 * 
	 * @return ImageRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadByUrl($url){
		return static::getRepository()->loadByUrl($url);
	}
	
	public static function loadByIdWithoutContent($id){
		return static::getRepository()->loadByIdWithoutContent($id);
	}
	
	public static function loadContentById($id){
		return static::getRepository()->loadContentById($id);
	}
	
	public function getName($width=0,$height=0){
		if(strlen($this->name)==0){
			return false;
		}
		if($width == 0 || $height == 0)
			return $this->name;
		
		$array = explode(".", $this->name);
		$array[0] = $array[0]."_{$width}x{$height}";
		return implode(".",$array);
	}
	
	public function save($overrideValidation = false) {
		if(strlen($this->content)){
			$this->thumbnail_small = "data:image/png;base64,". base64_encode($this->resize($this->content));
		}
		parent::save($overrideValidation);
	}
	
	function resize($source, $maxY = 64, $maxX = 64){

		// Load the sourcefile
		$sourceImageHandle = imagecreatefromstring($source);
		$width = imagesx($sourceImageHandle);
		$height = imagesy($sourceImageHandle);

		$scaling_factor = 1;
		$y_scaling_factor = ($maxY/$height);

		if($maxX != null){
			$x_scaling_factor = ($maxX / $width);
			$scaling_factor = $y_scaling_factor >  $x_scaling_factor ? $x_scaling_factor : $y_scaling_factor;
		}
		else{
			$scaling_factor = $y_scaling_factor;
		}

		$newheight = $height * $scaling_factor;
		$newwidth  = $width * $scaling_factor;

		//create an empty image handle
		$resizedImageHandle = imagecreatetruecolor($newwidth, $newheight);
		// $whiteBgColor = imagecolorallocate($resizedImageHandle, 255,255,255);
		// imagefill($resizedImageHandle,0,0,$whiteBgColor);
		imagecolortransparent($resizedImageHandle, imagecolorallocatealpha($resizedImageHandle, 0, 0, 0, 127));
		imagealphablending($resizedImageHandle, false);
		imagesavealpha($resizedImageHandle, true);
		// Resize
		imagecopyresampled($resizedImageHandle, $sourceImageHandle, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		// And save to file
		
		ob_start();
		imagepng($resizedImageHandle,NULL,0);
		$foo = ob_get_clean(); 
		return $foo;
	}
	
	
}
?>