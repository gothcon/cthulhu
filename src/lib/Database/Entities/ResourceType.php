<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
class ResourceType extends ReadWriteEntity{
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
	}
	
	/**
	 * 
	 * @return ResourceTypeRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadResourceTypesWithStatus(){
		return static::getRepository()->loadResourceTypesWithStatus();
	}
	
	public function delete(){
		/** Todo: fix delete **/
		parent::delete();
	}
}
