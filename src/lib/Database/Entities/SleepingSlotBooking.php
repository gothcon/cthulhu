<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property int $person_id 
 * @property int $sleeping_resource_id 
 */
class SleepingSlotBooking extends ReadWriteEntity{
	protected $_person = null;
	protected $_sleepingResource = null;
	public function __construct() {
		parent::__construct();
		$this->persistedProperties['sleeping_resource_id'] = 0;
		$this->persistedProperties['person_id'] = 0;
	}
	
	
	
	
	/**
	 * 
	 * @return SleepingSlotBookingRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	
	/**
	 * @param int $id
	 * @return SleepingSlotBooking
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	public static function loadListBySleepingResourceId($sleepingResourceId){
		return static::getRepository()->loadAllBySleepingResourceId($sleepingResourceId);
	}
	
	public static function loadByPersonId($personId){
		$res  =  static::getRepository()->loadBookingByPersonId($personId);
		return $res;
	}
	
	
	/**
	 * 
	 * @param Person $person
	 */
	public function setPerson($person){
		$this->_person = $person;
		$this->person_id = $person->id;
	}
	
	/**
	 * 
	 * @return Person
	 */
	public function getPerson(){
		return $this->_person;
	}
	
	public function setSleepingResource($sleeping_resource){
		$this->_sleepingResource = $sleeping_resource;
		$this->sleeping_resource_id = $sleeping_resource->id;
	}
	
	/**
	 * 
	 * @return SleepingResource
	 */
	public function getSleepingResource(){
		return $this->_sleepingResource;
	}
	
	public function getStandardClone() {
		$clone = parent::getStandardClone();
		$clone->person = $this->_person->getStandardClone();
		return $clone;
	}
	
	public static function getSleepingResources($sleepingEventId,$includeUnavailable = false,$includeFull=false){
		return static::getRepository()->loadSleepingResources($sleepingEventId,0, $includeUnavailable, $includeFull);
	}
			
	public static function getPersonalSleepingResorces($sleepingEventId,$personId,$includeUnavailable=false,$includeFull=false){
		return static::getRepository()->loadSleepingResources($sleepingEventId,$personId, $includeUnavailable, $includeFull);
	}
	
	public static function getUnbookedPersons(){
		return static::getRepository()->loadUnbookedPeople();
	}
	
	public static function loadListForPdf($resourceId = 0){
		return static::getRepository()->loadListForPdf($resourceId);
	}
	
}

?>
