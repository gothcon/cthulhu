<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property string $name
 * @property int $l
 * @property int $r
 * @property int $p
 * @property int $image_id
 * @property decimal $price
 * @property string $available_from
 * @property string $available_to
 * @property string $type
 * @property string $description
 * @property int $image_id
 */
class Article extends ReadWriteEntity{
	
	/**
	 * 
	 * @return ArticleRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["l"] = null;
		$this->persistedProperties["r"] = null;
		$this->persistedProperties["p"] = null;
		$this->persistedProperties["price"] = 0.00;
		$this->persistedProperties["available_from"] = null;
		$this->persistedProperties["available_to"] = null;
		$this->persistedProperties["type"] = "category";
		$this->persistedProperties["description"] = "";
		$this->persistedProperties["image_id"] = null;
	}
	
	private $availableFromDate = null;
	private $availableFromTime =null;
	private $availableToTime = null;
	private $availableToDate = null;
	
	/**  @var Image */
	private $_image;
	
	private function isNullOrEmpty($value){
		return is_null($value) || strlen(trim($value)) == 0;
	}
	
	private function isDate($date){
		if($this->isNullOrEmpty($date))
			return false;
		if(strtotime("{$date} 00:00:00") <= 0)
			return false;
		return true;
	}
	
	private function isTime($time){
		if($this->isNullOrEmpty($time))
			return false;
		$time = static::createTime($time);
		if($time !== false && strtotime("2000-01-01 {$time}") <= 0)
			return false;
		return true;
	}
	
	private function updateAvailableFrom(){
		if(!is_null($this->availableFromDate) &&  !is_null($this->availableFromTime)){
			 parent::__set("available_from","{$this->availableFromDate} {$this->availableFromTime}");
		}
		else{
			parent::__set("available_from",null);
		}
	}
	
	private function updateAvailableTo(){
		if(!is_null($this->availableToDate) &&  !is_null($this->availableToTime)){
			 parent::__set("available_to","{$this->availableToDate} {$this->availableToTime}");
		}
		else{
			parent::__set("available_to",null);
		}
	}
	
	
	public function __set($name,$value){
		if($name == "available_from"){
			if(!is_null($value) && strtotime($value) > 0){
				$this->availableFromDate = substr($value,0,10);
				$this->availableFromTime = substr($value,11,8); 
				parent::__set("available_from",$value);
			}else{
				$this->availableFromDate = null;
				$this->availableFromTime = null; 
			}
		}
		else if($name == "available_to"){
			if(!is_null($value) && strtotime($value) > 0){
				$this->availableToDate = substr($value,0,10);
				$this->availableToTime = substr($value,11,8); 
				parent::__set("available_to",$value);
			}else{
				$this->availableToDate = null;
				$this->availableToTime = null;
				parent::__set("available_to",null);
			}
		}
		else if($name == "available_to_time"){
			$value = !$value ? "23:59:59" : static::createTime($value);
			$this->availableToTime = $this->isTime($value) ? $value : null;
			$this->updateAvailableTo();
		}
		else if($name == "available_to_date"){
			$this->availableToDate = $this->isDate($value) ? $value : null;
			$this->updateAvailableTo();
		}
		else if($name == "available_from_time"){
			$value = !$value ? "00:00:00" : static::createTime($value);
			$this->availableFromTime = $this->isTime($value) ? $value : null;
			$this->updateAvailableFrom();
		}
		else if($name == "available_from_date"){
			$this->availableFromDate = $this->isDate($value) ? $value : null;
			$this->updateAvailableFrom();
		}
		else{
			parent::__set($name,$value);
		}
	}
	
	public function __get($name){
		
		if($name == "available_to_time"){
			return $this->availableToTime;
		}
		else if($name == "available_to_date"){
			return $this->availableToDate;
		}
		else if($name == "available_from_time"){
			return $this->availableFromTime;
		}
		else if($name == "available_from_date"){
			return $this->availableFromDate;
		}
		else{
			return parent::__get($name);
		}
	}
	
	
	public function isAvailable(){
		$now = time();
		
		return ((is_null($this->available_from) ||  $now >= strtotime($this->available_from)) && (is_null($this->available_to) || $now <= strtotime($this->available_to)));
	}
	
	
	private static function createTime($value){
		$value = str_replace(array(":"," "),"",$value);
		if(!is_numeric($value))
			return false;
		$value = strlen($value) == 1 ? "0".$value : $value;
		$value = str_pad($value,6,"0");
		$value = str_split($value,2);
		return implode(":",$value);
	}
	
	/**
	 * @return Article
	 */
	public static function loadRootNode(){
		return static::getRepository()->getRootNode();	
	}
	
	/**
	 * @return array
	 */
	public function loadPath(){
		return static::getRepository()->loadPath($this);
	}
	
	
	/**
	 * @return array
	 */
	public function loadSiblings(){
		return static::getRepository()->loadSiblings($this);
	}

	/**
	 * @return array
	 */
	public function loadChildren(){
		return static::getRepository()->getChildren($this);
	}

	/**
	 * @return array
	 */
	public function loadAllChildren($type = null){
		return Article::getRepository()->getAllChildren($this,$type);
	}
	
	public function loadPublicChildren(){
		return static::getRepository()->loadPublicChildren($this);
	}
	
		/**
	 * @return array
	 */
	public static function loadChildrenByParent($parentId){
		return static::getRepository()->getChildren($parentId);
	}
	
	/**
	 * @return array
	 */
	public function loadPublicSiblings(){
		return static::loadPublicSiblings($this);
	}
	
	/**
	 * @param int $id
	 * @return Article 
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}

	/**
	 * @return Article[] 
	 */
	public static function loadList() {
		return parent::loadList();
	}

	public function __toString() {
		return "Article #{$this->id}: {$this->name}";
	}
	
	/**
	 * 
	 * @param Image $image
	 */
	public function setImage($image){
		$this->_image = $image;
		if($image->id > 0)
			$this->persistedProperties["image_id"] = $image->id;
	}
	
	/**
	 * 
	 * @return Image
	 */
	public function getImage(){
		return $this->_image;
	}
	
	/**
	 * 
	 * @return bool
	 */
	public function save($overrideValidation = false) {
		static::getRepository()->begin();
		
		if($this->type != "signup_slot_fee")
			$this->signup_slot_id = null;
		if($this->type != "entrance_fee")
			$this->entrance_id = null;
		
		if($this->_image != null && strlen($this->_image->content)){
			$this->_image->save();
			$this->persistedProperties["image_id"] = $this->_image->id;
		}

		if(parent::save($overrideValidation)){
			static::getRepository()->commit();
			return true;
		}else{
			static::getRepository()->rollback();
			return false;
		}
	}
}
	define("ARTICLE_TYPE_CATEGORY","category");
	define("ARTICLE_TYPE_ARTICLE","article");
	define("ARTICLE_TYPE_COMPOSITE_CATEGORY","category");
	define("ARTICLE_TYPE_ENTRANCE_FEE","entrance_fee");
	define("ARTICLE_TYPE_SLOT_FEE","slot_fee");
	define("ARTICLE_TYPE_PACKAGE","package");
?>
