<?php
/**
 * @property string $subject 
 * @property string $message 
 * @property string $sender 
 * @property string $to 
 * @property string $cc 
 * @property string $bcc 
 * @property bool $as_html 
 * @property int $no_of_recipients
 */

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
class Email extends ReadWriteEntity{
	
	/**
	 * 
	 * @return EmailRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public function __construct() {
		parent::__construct();
		$this->persistedProperties["subject"] = null;
		$this->persistedProperties["message"]= null;
		$this->persistedProperties["sender"]= null;
		$this->persistedProperties["to"]= true;
		$this->persistedProperties["cc"]= true;
		$this->persistedProperties["bcc"]= true;
		$this->persistedProperties["as_html"]= true;
		$this->persistedProperties["no_of_recipients"] = 0;
	}

	public static function getAllEmailAddresses(){
		return static::getRepository()->getAllEmailAddresses();
	}
	
	public static function getOrganizerEmailAddresses($eventId=0){
		return static::getRepository()->getOrganizerEmailAddresses($eventId);
	}
	
	public static function getAttenderEmailAddresses($eventId = null, $slotTypeId = null,$paymentStatus = null,$validationStatus = null){
		return static::getRepository()->getAttenderEmailAddresses($eventId, $slotTypeId,$paymentStatus,$validationStatus);
	}
	
	public static function getAttenderGroupTree(){
		return static::getRepository()->getAttenderGroupTree();
	}
	
	public static function getCustomerEmailAdresses($categoryId = null ,$paymentState = array(),$articleId = null){
		if(is_string($paymentState))
			$paymentState = array($paymentState);
		
		return static::getRepository()->getCustomerEmailAdresses($categoryId, $paymentState, $articleId);
	}
	
}

?>
