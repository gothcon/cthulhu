<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "Order.php";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "Transaction.php";

/**
 * @property int $transaction_id
 * @property Transaction $transaction
 * @property int $order_id 
 * @property Order $order 
 * @property-read string $paymentMethod 
 */
class OrderPayment extends ReadWriteEntity{
	
	protected $_transaction, $_order;
	
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["transaction_id"] = null;
		$this->persistedProperties["order_id"] = null;
		$this->_transaction = null;
		$this->_order = null;
	}
	
	public function __get($propertyName){
		switch($propertyName){
			case "payment_method" :
				return $this->_transaction != null ? $this->_transaction->payment_method : null;
			case "transaction":
				return $this->_transaction;
				break;
			case "order":
				return $this->_order;
				break;
			default:
				return parent::__get($propertyName);
		}
	}
	
	public function __set($propertyName,$value){
		switch($propertyName){
			case "transaction":
				$this->_transaction = is_null($value) ? null : $value;
				$this->persistedProperties["transaction_id"] = is_null($value) ? null : $value->id;
				break;
			case "order":
				$this->_order = is_null($value) ? null : $value;
				$this->persistedProperties["order_id"] = is_null($value) ? null : $value->id;
				break;
			default:
				parent::__set($propertyName,$value);
		}
	}
	
	public static function getRecent($count = 10){
		return static::getRepository()->getRecent($count);
	}
	
	public static function getPaymentsForOrder($orderId){
		return static::getRepository()->getPaymentsForOrder($orderId);
	}
	
	static public function getTotalOrderPaymentSum($orderId){
		$orderPayments = static::getRepository()->getPaymentsForOrder($orderId);
		$sum = 0;
		foreach($orderPayments as $orderPayment){
			if($orderPayment->transaction->cancelled){
				continue;
			}
			$sum += $orderPayment->transaction->amount;
		}
		return $sum;
	}
	
	/**
	 * 
	 * @return OrderPaymentRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function GetPaymentMethods(){
		return array(
			array("label" => "checkout/wire_transfer", "value"=>"wire_transfer"),
			array("label" => "checkout/card", "value"=>"card")
		);
	}
	
	public function validateProperties() {
		$isValid = true;
		if(!$this->order){
			$this->setValidationError("order", static::VALIDATION_ERROR_NO_ORDER);
			$isValid = false;
		}
		else if($this->order->status == Order::STATUS_CANCELLED && $this->id == 0){
			// we cannot add another order payment to a canceled order
			$this->setValidationError("order", static::VALIDATION_ERROR_ORDER_IS_CANCELED);
			$isValid = false;
		}
		
		if(!$this->transaction){
			$this->setValidationError("order", static::VALIDATION_ERROR_NO_TRANSACTION);
			$isValid = false;
		}else if(!is_numeric($this->transaction->amount) || $this->transaction->amount == 0){
			$this->setValidationError("transaction",static::VALIDATIION_ERROR_AMOUNT_NOT_NUMERIC_OR_NULL);
		}
		
		return $isValid;
	}
	
	
	public function save(){
		// save the transaction.
		$oldOrder = Order::loadById($this->order_id);
		static::getRepository()->begin();
		// save the transaction
		$this->transaction->save();
		$this->transaction_id = $this->transaction->id;
		// save this
		if(parent::save()){
			// update the order payment status
			$order = $this->order;

			if($order->status != Order::STATUS_CANCELLED){			

				$totalOrderPayment = OrderPayment::getTotalOrderPaymentSum($this->order_id);
				
				if($totalOrderPayment == 0 && $order->total > 0){
					$order->status = $order->has_been_submitted ? Order::STATUS_SUBMITTED : Order::STATUS_CART;
				}
				else if($totalOrderPayment < $order->total){
					$order->status = Order::STATUS_PARTIALLY_PAID;
				}
				else{
					$order->status = Order::STATUS_PAID;
				}
			}

			$orderWasPaid = ($oldOrder->status == Order::STATUS_PAID);
			$orderIsPaid = ($order->status == Order::STATUS_PAID);

			// if payment status has changed
			if($orderWasPaid != $orderIsPaid){
				Application::log("Order payment status has changed from {$oldOrder->status} to {$order->status}");
				// load all signup slot articles used to associate the signups to the article of the order row
				$slotArticlesBySlot = array();
				$signupSlotArticles = SignupSlotArticle::loadList();
				foreach($signupSlotArticles as $slotArticle){
					$slotArticlesBySlot[$slotArticle->signup_slot_id] = $slotArticle;
				}

				$signups = Signup::loadAllFilteredByPerson($order->person_id);
				foreach($signups as $signup){
					if(!$signup->is_signup_owner){
						continue;
					}
					Application::log("Signup #".$signup->id);
					if(isset($slotArticlesBySlot[$signup->signup_slot_id])){
						$signupSlotArticle = $slotArticlesBySlot[$signup->signup_slot_id];
						$orderRow = $order->getOrderRow($signupSlotArticle->article_id);
						
						if($orderRow){
							Application::log("found order row!");
							$signup = Signup::loadById($signup->id);
							$signup->is_paid = $orderIsPaid;
							$signup->save();
							$orderRow->status = ($orderIsPaid ? OrderRow::STATUS_DELIVERED : OrderRow::STATUS_NOT_DELIVERED);

							$order->setOrderRow($orderRow);
						}
					}
				}
			}
			$order->save();
			$this->order = $order;
			static::getRepository()->commit();
			return true;
		}
		else{
			static::getRepository()->rollback();
			return false;
		}
	}
	
	public function delete() {
		$this->transaction->delete();
		parent::delete();
	}
	
	public function getStandardClone() {
		$obj = parent::getStandardClone();
		$obj->transaction = is_null($this->_transaction) ? null : $this->_transaction->getStandardClone();
		$obj->order = is_null($this->_order) ? null :$this->_order->getStandardClone();
		return $obj;
	}
	
	/**
	 * 
	 * @param int $id
	 * @return OrderPayment
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	const VALIDATION_ERROR_ORDER_IS_CANCELED = "orderIsCanceled";
	const VALIDATION_ERROR_NO_ORDER = "noOrder";
	const VALIDATION_ERROR_NO_TRANSACTION = "noTransaction";
	const VALIDATION_ERROR_AMOUNT_NOT_NUMERIC_OR_NULL = "transactionAmountError";
	
}
?>