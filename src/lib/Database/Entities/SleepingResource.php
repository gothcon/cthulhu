<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property int $resource_id 
 * @property int $no_of_slots 
 * @property bool $is_available
 */
class SleepingResource extends ReadWriteEntity{
	
	protected $_resource = null;
	protected $_sleepingSlotBookings = array();
	public function __construct() {
		parent::__construct();
		$this->persistedProperties['resource_id'] = 0;
		$this->persistedProperties['no_of_slots'] = 0;
		$this->persistedProperties['is_available'] = 1;
	}
	
	public function setSleepingSlotBookings($bookings){
		$this->_sleepingSlotBookings = $bookings;
	}
	
	public function getSleepingSlotBookings(){
		return $this->_sleepingSlotBookings;
	}
	
	/**
	 * @param int $id
	 * @return SleepingResource
	 */
	public static function loadById($id) {
		return parent::loadById($id);
	}
	
	/**
	 * 
	 * @param int $resourceId
	 * @return SleepingResource
	 */
	public static function loadByResourceId($resourceId){
		$sleeping_resource = static::getRepository()->loadByResourceId($resourceId); 
		if($sleeping_resource)
			$sleeping_resource->setSleepingSlotBookings(SleepingSlotBooking::loadListBySleepingResourceId($sleeping_resource->id));
		return $sleeping_resource;
	}
	
	public function setResource($resource){
		$this->_resource = $resource;
		$this->resource_id = $resource->id;
	}
	
	/**
	 * 
	 * @return Resource
	 */
	public function getResource(){
		return $this->_resource;
	}
	
	/**
	 * 
	 * @return SleepingResourceRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public function save($overrideValidation = false) {
		if($this->_resource != null){
			$this->_resource->save($overrideValidation);
			$this->resource_id = $this->_resource->id;
		}
		parent::save($overrideValidation);
	}
	
	public static function loadSleepingStatistics() {
		$list =static::getRepository()->loadStatusList();
		return $list;
	}
	
	public static function loadSleepingOccations($sleepingEventId,$resourceId){
		return static::getRepository()->loadSleepingOccations($sleepingEventId,$resourceId);
	}
	
	public function delete() {
		$sleepingSlotBookings = SleepingSlotBooking::loadListBySleepingResourceId($this->id);
		foreach ($sleepingSlotBookings as $sleepingSlotBooking) {
			$sleepingSlotBooking->delete();
		}
	}
	
}

?>
