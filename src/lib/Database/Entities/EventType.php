<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");

/**
 * @property string $name
 */
class EventType extends ReadWriteEntity {
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["name"] = null;
	}
	
	public function __toString() {
		return "EventType #{$this->id}: {$this->name}";
	}
	
	/**
	 * 
	 * @return EventTypeRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public static function loadNotEmptyList(){
		return static::getRepository()->loadAllNotEmpty();
	}
	
	public function delete() {
		$events = Event::loadByType($this->id);
		foreach ($events as $event) {
			$event->eventtype_id = null;
			$event->save();
		}
		parent::delete();
	}
}
?>