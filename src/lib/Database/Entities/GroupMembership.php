<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property int $person_id
 * @property int $group_id
 */
class GroupMembership extends ReadWriteEntity{
	/**
	 * @var Person 
	 */
	protected $person;
	/**
	 * @var Group 
	 */
	protected $group;
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["person_id"] = null;
		$this->persistedProperties["group_id"] = null;
		$this->person = null;
		$this->group = null;
	}
	
	public function setPerson($person){
		$this->person = $person;
		$this->person->id = $person->id;
	}
	/**
	 *
	 * @return Person
	 */
	public function getPerson(){
		return $this->person;
	}
	
	/**
	 *
	 * @return Group
	 */
	public function getGroup(){
		return $this->group;
	}
	
	public function setGroup($group){
		$this->group = $group;
		$this->group_id = $group->id;
	}
	
	public function __toString() {
		return "GroupMembership #{$this->id} Group: {$this->group->name}, Member: {$this->person->first_name} {$this->person->last_name}";
	}
	
	/**
	 * @return stdClass
	 */
	public function getStandardClone(){
		$stdObj = parent::getStandardClone();
		$stdObj->person = $this->person == null ? null : $this->person->getStandardClone();
		$stdObj->group = $this->group == null ? null : $this->group->getStandardClone();
		return $stdObj;
	}
	
	public static function loadByGroup($groupId){
		return GroupMembership::getRepository()->loadByGroup($groupId);
	}
	
	/**
	 * @param int $groupId
	 * @param int $personId
	 * @return GroupMembership
	 */
	public static function loadByGroupAndPerson($groupId,$personId){
		return GroupMembership::getRepository()->loadByGroupAndPerson($groupId,$personId);
	}
	
	public static function loadGroupsByLeader($person_id){
		return GroupMembership::getRepository()->loadGroupsByLeader($person_id);
	}
	
	public static function loadByPerson($person){
		return GroupMembership::getRepository()->loadByPerson($person);
	}
			
	public static function loadByIds($idArray){
		return GroupMembership::getRepository()->loadByIds($idArray);
	}
	
	public static function personIsLeader($person_id,$group_id){
		return GroupMembership::getRepository()->personIsLeader($person_id,$group_id);
	}
	
	public static function personIsMember($person_id,$group_id){
		return GroupMembership::getRepository()->personIsMember($person_id,$group_id);
	}
	
	public static function loadByLeader($personOrId){
		return GroupMembership::getRepository()->loadByLeader($personOrId);
	}
	
	public function delete(){
		$id = $this->id;
		$membership = GroupMembership::loadById($id);
		if($membership->getGroup()->leader_membership_id == $id){
			$group = Group::loadById($membership->group_id);
			$group->leader_membership_id = 0;
			$remainingMembers = GroupMembership::loadByGroup($membership->group_id);
			if(count($remainingMembers)){
				$group->leader_membership_id = $remainingMembers[0]->id;
			}
			$group->save();
		}
		return parent::delete();
	}
	
	/**
	 * 
	 * @return GroupMembershipRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
}


?>
