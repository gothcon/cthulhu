<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR ."..".DIRECTORY_SEPARATOR . "IReadOnlyEntity.php");
/**
 * @property int $id
 * @property string $created_at
 * @property string $modified_at
 * @property string $created_by
 * @property string $modified_by
 * @property int $is_deleted
 */
class ReadOnlyEntity implements IReadOnlyEntity{
	
	/**
	 * @var array 
	 */
	protected $persistedProperties;
	
	public function getPersistedProperties() {
		return $this->persistedProperties;
	}
	
	public function getPersistedPropertyNames(){
		return array_keys($this->persistedProperties);
	}
	
	public function setPersistedProperty($propertyName,$propertyValue){
		$this->__set($propertyName,$propertyValue);
	}
	
	public function setPersistedProperties($properties){
		$this->persistedProperties = $properties;
	}

	
	/**
	 *
	 * @var IReadOnlyRepository 
	 */
	protected static $repository = array();
	
	/**
	 * @return IReadOnlyRepository
	 */
	static public function getRepository(){
		
		$name = get_called_class();
		
		if(!isset(static::$repository[$name])){
			die("repository not set for class {$name}");
		}
		return static::$repository[$name];
	}
	
	/**
	 * @param IReadOnlyRepository $iReadOnlyRepository 
	 */
	public static function setRepository($iReadOnlyRepository) {
		$name = get_called_class();
		static::$repository[$name] = $iReadOnlyRepository;
	}

	public function __construct(){
		$this->persistedProperties = array(
			"id" 			=> 0,
			"created_at" 	=> strftime("%Y-%m-%d %H:%M:%S",time()),
			"modified_at" 	=> null,
			"created_by" 	=> null,
			"modified_by" 	=> null,
			"is_deleted" 	=> 0
		);
	}
	
	public function __get($name){
		$name = strtolower($name);
		if(array_key_exists($name,$this->persistedProperties)){
			return $this->persistedProperties[$name];
		}
		if(property_exists($this,$name)){
			return $this->$name;
		}
	}
	
	public function __set($name,$value){
		$this->is_modified = true;
		if(array_key_exists($name,$this->persistedProperties)){
			if($this->persistedProperties[$name] != $value){
				$this->persistedProperties[$name] = $value;
			}
		}
		if(property_exists($this,$name)){
			$this->$name = $value;
			$name_id_property = $name."_id";
			if(array_key_exists($name_id_property,$this->persistedProperties)){
				// print_r(get_callstack());
				// die("should not be used");
				// $this->persistedProperties[$name_id_property] = $value->id;
			}
		}
	}
	
	public function __isset($name){
		return isset($this->persisitedProperties[$name]);
	}
	
	public function getStandardClone(){
		$anonymousObject = new stdClass();
		foreach($this->persistedProperties as $key => $value){
			$anonymousObject->$key = $value;
		}
		return $anonymousObject;
	}

	public function load(){
		$repository = static::getRepository();
		$loadedEntity = $repository->load($this->id);
		$this->persistedProperties = $loadedEntity->getPersistedProperties();
	}
	
	/**
	 * @param type $id
	 * @return IReadOnlyEntity
	 */
	public static function loadById($id) {
		return static::getRepository()->load($id);
	}
	
	public static function loadList() {
		return static::getRepository()->load();
	}
	
	public static function loadSorted($parameter="id",$dir = "asc"){
		return static::getRepository()->loadSorted($parameter,$dir);
	}
	
}
?>