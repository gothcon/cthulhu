<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "ReadWriteEntity.php");
/**
 * @property int $order_id 
 * @property int $article_id 
 * @property string $name 
 * @property int $count 
 * @property float $price_per_each
 * @property float $total
 * @property string $status
 * @property string $price_model 
 * @property string $note
 * @property Article $article
 */
class OrderRow extends ReadWriteEntity{
	public function __construct(){
		parent::__construct();
		$this->persistedProperties["order_id"] = 0;
		$this->persistedProperties["article_id"] = null;
		$this->persistedProperties["name"] = null;
		$this->persistedProperties["count"] = 1;
		$this->persistedProperties["price_per_each"] = 0;
		$this->persistedProperties["total"] = 0;
		$this->persistedProperties["status"] = "not_delivered";
		$this->persistedProperties["price_model"] = "normal";
		$this->persistedProperties["note"] = "";
		$this->_article = new Article();
	}
	
	public function recalculate(){
		$this->persistedProperties["total"] = $this->persistedProperties["price_per_each"] * $this->persistedProperties["count"];
	}

	public function __get($name){
		switch($name){
			case "article":
				return $this->_article;
				break;
			default:
				return parent::__get($name);
		}
	}
	
	public function __set($name,$value){
		
		switch($name){
			case "article":
				$this->_article = $value;
				if($value==null){
					$this->persistedProperties['article_id'] = null;
					$this->persistedProperties['name'] = "";
				}else{
					$this->persistedProperties['article_id'] = $value->id;
					$this->persistedProperties['name'] = $value->name;
				}
				break;
			case "total":
				parent::__set($name,$value);
				$this->persistedProperties["price_model"] = "custom";
				break;
			case "count":
			case "price_per_each":
				parent::__set($name,$value);
				if($this->persistedProperties["price_model"] == "normal")
					$this->recalculate();
				break;
			default:
				parent::__set($name,$value);
		}
	}
	
	const STATUS_DELIVERED = "delivered";
	const STATUS_NOT_DELIVERED = "not_delivered";
	const STATUS_CANCELLED = "cancelled";
	
	public static function getStatusValues(){
		return array(static::STATUS_DELIVERED, static::STATUS_NOT_DELIVERED);
	}
	
	public static function loadArticleOrderingStatistics($articleId){
		return OrderRow::getRepository()->loadArticleOrderingStatistics($articleId);
	}
	
	public static function loadArticlesOrderingStatistics(){
		return OrderRow::getRepository()->loadArticlesOrderingStatistics();
	}
	
	public static function loadByArticle($articleId){
		return OrderRow::getRepository()->loadByArticle($articleId);
	}
	
	public static function loadByOrder($orderId){
		return OrderRow::getRepository()->loadByOrder($orderId);
	}
	
	/**
	 * @return OrderRowRepository
	 */
	public static function getRepository() {
		return parent::getRepository();
	}
	
	public function __toString() {
		return "OrderRow #{$this->id}: {$this->name} á {$this->price_per_each} x {$this->count} = {$this->total}";
	}
	
	public static function loadOrderRowStatistics($articleId = 0){
		return static::getRepository()->loadOrderRowsStatistics($articleId);
	}
	
}

?>
