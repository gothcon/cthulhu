<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreeItem_Event
 *
 * @author Joakim
 */
class TreeItem_Event {
	public $name, $id, $organizer, $organizer_id, $organizer_type, $preamble, $text, $info, $schedule_name;
	/** @var TreeItem_EventOccation[] */
	public $event_occations = array();
	
	public function hasSignupSlots(){
		foreach($this->event_occations as $occation){
			if($occation->hasSignupSlots())
				return true;
		}
		return false;
	}
}