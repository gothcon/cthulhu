<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreeItem_Event
 *
 * @author Joakim
 */
class TreeItem_ArticleOrderingStatistics {
	public $article_name, $article_id, $no_ordered = 0, $no_paid  = 0, $no_delivered  = 0;
	/** @var TreeItem_OrderRow[] */
	private $order_rows = array();
	
	private $rows_have_been_set = false;
	
	public function setOrderRows($orderRows){
		$this->order_rows = $orderRows;
		$this->rows_have_been_set = true;
	}
	
	public function addOrderRow($orderRow){
		$this->order_rows[] = $orderRow;
		$this->rows_have_been_set = true;
	}
	
	/** @var TreeItem_OrderRow[] */
	public function getOrderRows(){
		if(!$this->rows_have_been_set)
			die("order row details have not been loaded");
		return $this->order_rows;
	}
	
	
}