<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreeItem_SignupSlot
 *
 * @author Joakim
 */
class TreeItem_SignupSlot {
	public $id, $slot_type_name, $slot_type_id, $slot_type_cardinality, $requires_approval, $cost, $max_signup_count, $max_spare_signup_count, $current_signup_count, $slot_status;
	
	/** @var TreeItem_Signup[] */
	public $signups = array();
}