<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreeItem_Signup
 *
 * @author Joakim
 */
class TreeItem_SleepingResource {
	public $resource_name, $resource_type, $resource_id, $sleeping_resource_id, $no_of_slots, $no_of_free_slots, $no_of_occupied_slots;
	
	/** @var TreeItem_SleepingSlotBooking[] */
	private $sleeping_slot_bookings = array();
	private $bookings_have_been_set = false;
	
	public function setSlotBookings($bookings){
		$this->sleeping_slot_bookings = $bookings;
		$this->bookings_have_been_set = true;
	}
	
	public function addSlotBooking($booking){
		$this->sleeping_slot_bookings[] = $booking;
		$this->bookings_have_been_set = true;
	}
	
	/** @var TreeItem_SleepingSlotBooking[] */
	public function getOrderRows(){
		if(!$this->bookings_have_been_set)
			die("slot bookings have not been loaded");
		return $this->sleeping_slot_bookings;
	}
	
	
}