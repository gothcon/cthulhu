<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreeItem_EventOccation
 *
 * @author Joakim
 */
class TreeItem_EventOccation {
	public $name, $full_name, $id, $starts_at, $ends_at, $timespan_name, $timespan_as_string,$main_resource,$main_resource_id;
	
	/** @var TreeItem_SignupSlot[] */
	public $signup_slots = array();

	/** @var TreeItem_EventOccationBooking[] */
	public $booked_resources = array();
	
	public function hasSignupSlots(){
		return count($this->signup_slots) > 0;
	}
}