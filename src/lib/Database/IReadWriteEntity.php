<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IReadOnlyEntity.php");
interface IReadWriteEntity extends IReadOnlyEntity{
	public static function deleteById($id);
	public function delete();
	public function save();
}

?>
