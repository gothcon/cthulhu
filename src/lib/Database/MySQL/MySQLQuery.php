<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MySQLQuery
 *
 * @author Joakim
 */
class MySQLQuery{
	var $mainTable;
	var $mainTableAlias;
	var $columnNames = array();
	var $joinData = array();		

	function __construct($mainTable,$mainTableAlias,$mainTableEntityOrColumnNames){
		$this->mainTable = $mainTable;
		$this->mainTableAlias = $mainTableAlias;
		$this->addSelectColumnNames($mainTableEntityOrColumnNames,$mainTableAlias);
	}

	function addSubqueryJoin($subQuery,$queryResultAlias,$foreignKeyColumn,$tableAlias,$keyColumn){
		$this->joinData[] = new JoinData($subQuery,$queryResultAlias,$foreignKeyColumn,$tableAlias,$keyColumn);
		foreach($subQuery->columnNames as $name){
			/*
			list($foo,$name) = explode(" as `", $name);
			$this->columnNames[] = "`{$queryResultAlias}`.`{$name} as `{$queryResultAlias}_{$name}";
			*/
			list($foo,$name) = explode(" as ", $name);
			$this->columnNames[] =$name;
		}
	}

	function addJoin($foreignTable,$foreignTableAlias,$foreignKeyColumn,$tableAlias,$keyColumn,$foreignEntityOrColumNames=null){
		$this->joinData[] = new JoinData($foreignTable,$foreignTableAlias,$foreignKeyColumn,$tableAlias,$keyColumn);
		if($foreignEntityOrColumNames != null)
			$this->addSelectColumnNames($foreignEntityOrColumNames,$foreignTableAlias);
	}

	private function addSelectColumnNames($entityOrColumNames,$tableAlias){
		if(is_string($entityOrColumNames)){
			$entityOrColumNames = new $entityOrColumNames;
		}
		if(is_a($entityOrColumNames,"IEntity")){
			$entityOrColumNames = array_keys($entityOrColumNames->getPersistedProperties());
		}
		foreach($entityOrColumNames as $columnName){
			$aliasedColumnName = "`{$tableAlias}`.`{$columnName}` as `{$tableAlias}_{$columnName}`";
			$this->columnNames[] = $aliasedColumnName;
		}
	} 	

	public function __toString(){
		$query = "select ";
		$first = true;
		foreach($this->columnNames as $columnName){
			if(!$first)
				$query .= ",\n";
			else
				$first = false;
			$query .= $columnName;
		}
		$query .= "\nfrom `{$this->mainTable}` `{$this->mainTableAlias}`";

		foreach($this->joinData as $joinData){
			$query .= "\nleft join ";
			if(is_a($joinData->foreignTable,"Query")){
				$query .= "({$joinData->foreignTable})";
			}else{
				$query .= "{$joinData->foreignTable}";
			}
			$query .= " `{$joinData->foreignTableAlias}` on `{$joinData->foreignTableAlias}`.`{$joinData->foreignKeyColumn}` = `{$joinData->tableAlias}`.`{$joinData->keyColumn}`";
		}
		return $query;
	}

	public function getQueryString(){
		return $this->__toString();
	}

}

class JoinData{

	var $foreignTable,
		$foreignTableAlias,
		$foreignKeyColumn,
		$tableAlias,
		$keyColumn;

	function __construct($foreignTable,$foreignTableAlias,$foreignKeyColumn,$tableAlias,$keyColumn){
		$this->foreignTable = $foreignTable;
		$this->foreignTableAlias = $foreignTableAlias;
		$this->foreignKeyColumn = $foreignKeyColumn;
		$this->tableAlias = $tableAlias;
		$this->keyColumn = $keyColumn;

	}

}
	


?>
