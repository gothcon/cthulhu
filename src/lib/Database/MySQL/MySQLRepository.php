<?php
include_once(dirname(__FILE__). DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR  ."IRepository.php");
/**
 * @property MySQLConnectionFactory $connectionFactory
 * @property PDO $dbConn;
 */
abstract class MySQLRepository implements IRepository{
	
	
	public function getDatabaseTableAlias(){
		return this::$databaseTableAlias;
	}	
	
	public function getDatabaseTableName(){
		return this::$databaseTableName;
	}
	
	private static $dbConn = null;

	public static final function setConnection($connection){
		static::$dbConn = $connection;
	}
	
	public function __construct() {
		;
	}
	
	public function __destruct() {
	}
	
	
	/**
	 *
	 * @return PDO
	 */
	public function getConnection(){
		return MySQLRepository::$dbConn;
	}
	
	/**
	 *
	 * @param string $querystring
	 * @param mixed $parameters
	 * @return PDOStatement 
	 */
	protected final function execute($querystring,$parameters=array()){
		
		$conn = $this->getConnection();
		
		if(!is_array($parameters) && !is_object($parameters)){
			$parameters = array($parameters);
		}
		$statement = $conn->prepare($querystring);
		foreach($parameters as $identifier => $value){
		if(is_int($value)){
			$param = PDO::PARAM_INT;
		}
		elseif(is_bool($value)){
			$param = PDO::PARAM_BOOL;
		}
		elseif(is_null($value)){
			$param = PDO::PARAM_NULL;
		}
		elseif(is_string($value)){
			$param = PDO::PARAM_STR;
		}
		else{
			$param = FALSE;
		}    
		if($param)
			$statement->bindValue(":{$identifier}",$value,$param);
		else
			$statement->bindValue(":{$identifier}",$value);
		}
		$statement->execute();
			// print_r(debug_print_backtrace());
			// print_r($statement);
		$errorInfo = $statement->errorInfo(); 
		
		if($errorInfo[0] != "00000"){
			print_r($statement->errorInfo());
			$statement->debugDumpParams();
			die();
		}
		return $statement;
	}	
	
	/**
	 *
	 * @param string $value
	 * @return string
	 */
	protected function quote($value,$asNumber = false){
		if(is_null($value))
			return "NULL";
		if($asNumber && is_numeric($value))
			return $value;
		else
			return $this->getConnection()->quote($value);
	}
		
	protected function logChanges($entity,$operation,$currentUser){
		$entityClassName = get_class($entity);
		switch($operation){
			case "delete":
				$label = "Deleted {$entityClassName} {$entity->id}";
				if(isset($entity->persistedProperties['name'])){
					$label .= ". {$entity->persistedProperties['name']}"; 
				}
			break;
			case "create":
				$label = "Created {$entityClassName} {$entity->id}";
				if(isset($entity->persistedProperties['name'])){
					$label .= ". {$entity->persistedProperties['name']}"; 
				}
				break;
			case "update":
				$label = "Updated {$entityClassName} {$entity->id}";
				if(isset($entity->persistedProperties['name'])){
					$label .= ". {$entity->persistedProperties['name']}"; 
				}
				break;
		}

		$querystring = "
			insert into changelog 
				(`entity_class_name`,`entity_id`,`entity_name`,`operation`,`serialized_entity_data`,`current_user`) 
			values
				(:entity_class_name,:entity_id,:entity_name,:operation,:serialized_entity_data,:current_user)";
		$parameters = array();
		$parameters["entity_class_name"] = $entityClassName;
		$parameters["entity_id"] = $entity->id;
		$parameters["entity_name"] = $label;
		$parameters["operation"] = $operation;
		$parameters["serialized_entity_data"] = serialize($entity->persistedProperties);

		$parameters["current_user"] = $currentUser;
		$this->execute($querystring, $parameters);

	}

	static private $tableIsLocked = false;
	static protected $transactionOwner = "";
	
	
	public function begin(){



		
		$transactionOwner = get_class($this);

		if(static::$transactionOwner == "" || static::$transactionOwner == $transactionOwner){
			$this->getConnection()->beginTransaction();
			static::$transactionOwner = $transactionOwner;
		}
	}
	
	public function commit(){
		$transactionOwner = get_class($this);
		if(static::$transactionOwner == $transactionOwner){
			$this->getConnection()->commit();
			static::$transactionOwner = "";
		}
	}
	
	public function rollback(){
		$transactionOwner = get_class($this);
		if(static::$transactionOwner == $transactionOwner){
			$this->getConnection()->rollBack();
			static::$transactionOwner = "";
		}
	}
	
	protected function lockTable($tables = null,$type = LockTypes::WRITE){
	
		if($tables == null){
			$tables = array($this);
		}
	
		switch(strtolower($type)){
			case LockTypes::READ:
			case LockTypes::WRITE:
				break;
			default:
				die("illegal lock type, only 'read' and 'write' are valid types");
				break;
		}
		$query = "LOCK TABLES";
		
		if(!is_array($tables)){
				$tables = array($tables);
		}
		
		$first = true;
		foreach($tables as $table){
			if($first){
				$first = false;
			}
			else{
				$query .=",";
			}
		
			if(is_subclass_of($table,"MySQLRepository")){
				$query .= " `".$table->getDatabaseTableName()."` as `".$table->getDatabaseTableAlias()."` {$type}";
				$query .= ", `".$table->getDatabaseTableName()."`";
			}
			else if(is_string($table)){
				$query .= " `{$table}`";
			}
			else if(is_object($table)){
				$query .= " `{$table->name}` as `{$table->alias}`";
			}
			$query .= " {$type}";
			
		}
		
		$this->execute($query);

		// MySQLRepository::$tableIsLocked = true;
		return true;			
	}

	protected function unlockTables(){
		//if(MySQLRepository::$tableIsLocked == true){
			$this->execute("UNLOCK TABLES");
			// MySQLRepository::$tableIsLocked = false;
		//}
	}
}


class LockTypes{
	const READ = "read";
	const WRITE = "write";
}

?>
