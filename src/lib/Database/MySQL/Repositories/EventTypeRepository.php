<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class EventTypeRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "event_types";
	protected $entityClassName = "EventType";
	protected $databaseTableAlias = "et";
	
	
	public function loadAllNotEmpty(){
		$query = "SELECT et.* FROM events e 
			LEFT JOIN event_types et ON e.eventtype_id = et.id
			WHERE e.visible_in_public_listing = 1
			GROUP BY et.id
			ORDER BY name
			";
		return $this->loadMany("{$query}",array(),"PopulateNotEmpty");
	}
	
	public function PopulateNotEmpty($eventType,$row,$prefix = ""){
		
		return $this->PopulateEntity($eventType, $row, $prefix);
	}	
}