<?php


require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class GroupMembershipRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "group_memberships";
	protected $entityClassName = "GroupMembership";
	protected $databaseTableAlias = "gm";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("people", "p", "id", $this->databaseTableAlias, "person_id", new Person());
		$query->addJoin("groups", "g", "id", $this->databaseTableAlias, "group_id", new Group());
		return $query->getQueryString();
	}
	
	public function loadByGroup($groupId){
		return $this->loadMany("
		{$this->getSelectAllQuery()}
		where gm.group_id =:id", array("id" => $groupId),"PopulateEntity");
	}
	
	public function loadByGroupAndPerson($groupId,$personId){
		return $this->loadOne("
		{$this->getSelectAllQuery()}
		where gm.group_id =:groupId and gm.person_id = :personId", array("groupId" => $groupId, "personId" => $personId),"PopulateEntity");
	}

	public function loadGroupsByLeader($person_id){
		$memberships = $this->loadByPerson($person_id,true);
		$groups = array();
		foreach($memberships as $membership){
			$groups[] = $membership->getGroup();
		}
		return $groups;
	}
	
	public function loadByPerson($person){
	
		if(is_numeric($person) && $person > 0){
			$id = $person;
		}else if(is_object($person)){
			$id = $person->id;
		}else{
			die("Could not load group memberships: Invalid person/id");
		}
	
		$where = "gm.person_id =:id";
		
		return $this->loadMany("
		{$this->getSelectAllQuery()}
		where {$where}", array("id" => $id),"PopulateEntity");
	}
	
	public function loadByIds($idArray){
		switch(count($idArray)){
			case 0:
				$where = "";
				$params = array();
			break;
			case 1:
				$where = " where gm.id = :id";
				$params = array("id" => $idArray[0]);
			
			default:
				$where = " where gm.id in(";
				for($i = 0; $i < count($idArray); $i++){
					if($i != 0)
						$where .= ",";
					$where .= $idArray[$i];
				}
				$where .= ")";
				$params = array();
			break;
		}
		$query = $this->getSelectAllQuery(). $where;
		return $this->loadMany($query, $params, "PopulateEntity");
	}
	
	public function personIsLeader($person_id,$group_id){
		$query = "
			select 
				count(gm.id) as count 
				from groups g
				left join group_memberships gm on g.id = gm.group_id
			where 
				gm.person_id = :person_id 
				AND gm.group_id = :group_id 
				AND g.leader_membership_id = gm.id";

		$parameters = array("person_id" => $person_id, "group_id" => $group_id);
		$resultSet = $this->execute($query, $parameters);
		$row = $resultSet->fetch(PDO::FETCH_ASSOC);
		return $row["count"] == 1;
	}
	
	public function personIsMember($person_id,$group_id){
		$query = "
			SELECT
				count(gm.id) as count 
			FROM group_memberships gm 
			WHERE 
				gm.person_id = ".$this->quote($person_id) ."
				AND gm.group_id =  ".$this->quote($group_id);

		$resultSet = $this->execute($query);
		$row = $resultSet->fetch(PDO::FETCH_ASSOC);
		return $row["count"] == 1;
	}
	
	public function loadByLeader($personOrId){
	
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("people", "p", "id", $this->databaseTableAlias, "person_id", new Person());
		$query->addJoin("groups", "g", "id", $this->databaseTableAlias, "group_id", new Group());
		
		
		if(is_numeric($personOrId) && $personOrId > 0){
			$id = $personOrId;
		}else if(is_object($personOrId)){
			$id = $personOrId->id;
		}else{
			die("Could not load group memberships: Invalid person/id");
		}
		$queryString = "{$query->getQueryString()} where gm.person_id =:id && gm.id =  g.leader_membership_id";
		return $this->loadMany($query, array("id" => $id),"PopulateEntity");
	}
	
	public function PopulateEntity($groupMember,$row,$prefix="gm_"){
		$groupMember = parent::PopulateEntity($groupMember,$row,$prefix);
		$person = new Person();
		$person = parent::PopulateEntity($person,$row,"p_");
		
		$group = new Group();
		$group = parent::PopulateEntity($group, $row, "g_");
		
		$groupMember->setGroup($group);
		$groupMember->setPerson($person);
		return $groupMember;
	}
	
}