<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class ResourceBookingRepository extends MySQLReadOnlyRepository{
	protected $databaseTableName = "resource_bookings";
	protected $entityClassName = "ResourceBooking";
	protected $databaseTableAlias = "fb";
	
	public function getAllFilteredByResource($resource_id){

		$query ="
			select 
				e.name as label, 
				rb.id as id, 
				eo.id as event_occation_id, 
				t.starts_at as t_starts_at, 
				t.ends_at as t_ends_at, 
				'arrangemangsbokning' as type from resource_bookings_event_occations rb
			left join event_occations eo on rb.event_occation_id = eo.id
			left join events e on eo.event_id = e.id
			left join timespans t on eo.timespan_id = t.id
			where rb.resource_id = " . $this->quote($resource_id);
		
		try{
			$statement = $this->execute($query);
		}
		catch (PDOException $e) {
			echo "PDO Error: ".$e->getMessage();
			print_r($statement->errorInfo());
		}
		
		$searchResult = array();
		while($obj = $statement->fetch(PDO::FETCH_ASSOC)){
			$timespan = new Timespan();
			$timespan = parent::populateEntity($timespan,$obj,"t_");
			$obj["timespan_as_string"] = $timespan->__toString();
			$searchResult[] = $obj;
		}
		
		return $searchResult;
			
	}
	public function loadByEvent($event_id){
		$query = new MySQLQuery($this->databaseTableName,$this->entityClassName,$this->databaseTableAlias);
		$query->addJoin("event_occations",	"eo",	"id" ,$this->databaseTableAlias,	"event_occation_id",	new EventOccation());
		$query->addJoin("resources",		"r",	"id", $this->databaseTableAlias,	"resource_id",			new Resource());
		$query->addJoin("resource_types",	"rt",	"id", "r",							"resource_type_id",		new ResourceType());
		$queryString = $query->getQueryString();
		return $this->loadMany($queryString . " where eo.event_id = :event_id",array("event_id" => $event_id),"PopulateEntity");
	}
	public function PopulateEntity($emptyBookingEntity,$row,$prefix="rb_"){
		$bookingEntity = parent::PopulateEntity($emptyBookingEntity,$row,$prefix);
		$resourceEntity = new Resource();
		$resourceType = new ReourceType();
		parent::populateEntity($resourceType,$row,"ft_");
		$resourceEntity->setResourceType($resourceType);
		parent::populateEntity($resourceEntity,$row,"f_");
		$bookingEntity->setResource($resourceEntity);
		
		return $bookingEntity;
	}
	
	public function getBookingStatus($timespanId){
		$query = "
			SELECT 
				-- ISNULL(rb.resource_id) AS completely_empty, 
				r.name,
				r.id, COUNT(_overlapping) > 0 AS occupied FROM resources r
			LEFT JOIN (

				SELECT rb.resource_id AS resource_id,
				CASE
				WHEN t1.starts_at > t2.starts_at AND t1.starts_at < t2.ends_at THEN 1
				WHEN t1.ends_at > t2.starts_at AND t1.ends_at < t2.ends_at THEN 1
				WHEN t1.starts_at <= t2.starts_at && t1.ends_at >= t2.ends_at THEN 1
				WHEN t2.starts_at <= t1.starts_at && t2.ends_at >= t1.ends_at THEN 1
				ELSE NULL
				END AS _overlapping
				FROM resource_bookings rb
				LEFT JOIN 
					event_occations eo ON eo.id = rb.event_occation_id
				LEFT JOIN 
					events e ON e.id = eo.event_id
				LEFT JOIN 
					timespans t1 ON t1.id = eo.timespan_id
				JOIN 
					timespans t2
				WHERE t2.id = :timespanId

			) AS rb ON rb.resource_id = r.id
			GROUP BY r.id			
			order by r.available desc, r.name
		";
		$parameters = array("timespanId" => $timespanId);
		$resultSet = $this->execute($query, $parameters);
		$result = array();
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$result[$row["name"]] = $row["occupied"] ? 0 : 1;
		}
		return $result;
	}
	
	/**
	 * Loads the bookings for the supplied resource or all resources if no 
	 * resource is supplied
	 * @param type $resourceId
	 * @return \stdClass
	 */
	public function loadResourceSchedules($resourceId = false){
		$queryString ="
			SELECT 
				r.id AS r_id, r.name AS r_name, r.available AS r_available,
				rt.name AS rt_name,
				rb.id AS rb_id,
				t.starts_at AS t_starts_at, t.ends_at AS t_ends_at, t.id AS t_id,
				eo.name AS eo_name, eo.id AS eo_id,
				e.name AS e_name, e.id AS e_id
			FROM `resources` r
			LEFT JOIN `resource_bookings_event_occations` rb ON r.id = rb.resource_id
			LEFT JOIN `resource_types` rt ON rt.id = r.resource_type_id
			LEFT JOIN `event_occations` eo ON eo.id = rb.event_occation_id
			LEFT JOIN `timespans` t ON t.id = eo.timespan_id
			LEFT JOIN `events` e ON e.id = eo.event_id
		";
		
		$param = array();
		
		if(is_numeric($resourceId)){
			$queryString .= "WHERE r.id = :resourceId ";
			$param["resourceId"] = $resourceId;
		}
		
		$queryString .= "
			ORDER BY r.available desc,rt.name asc,r.name asc ,e.name asc			
		"; 
		$resultSet = $this->execute($queryString,$param);
		$resourceScheduleList = array();
		$currentResource = new stdClass();
		$currentEvent = new stdClass();
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			// new resource
			if(isset($currentResource->id) && $currentResource->id != $row["r_id"]){
				if(isset($currentEvent->id)){
					$currentResource->events[] = $currentEvent;
					$currentEvent = new stdClass();
				}
				$resourceScheduleList[] = $currentResource;
				$currentResource = new stdClass();
			}
			
			// new resource 
			if(!isset($currentResource->id)){
				$currentResource->id = $row["r_id"];
				$currentResource->name = $row["r_name"];
				$currentResource->type = $row["rt_name"];
				$currentResource->available = $row["r_available"];
				$currentResource->events = array();
			}
			// new event
			if(isset($currentEvent->id) && $currentEvent->id != $row["e_id"]){
				$currentResource->events[] = $currentEvent;
				$currentEvent = new stdClass();
			}
			// new event
			if(!isset($currentEvent->id) && ($row["e_id"])){
				if($row["e_id"]){
					$currentEvent->name = $row["e_name"];
					$currentEvent->id = $row["e_id"];
					$currentEvent->bookings = array();
				}else{
					$currentEvent->name = "Snabbokningar";
					$currentEvent->id = -1;
					$currentEvent->bookings = array();
				}
			}
			if($row["eo_id"]){
				$booking = new stdClass();
				$booking->starts_at = $row["t_starts_at"];
				$booking->ends_at = $row["t_ends_at"];
				$booking->name = $row["eo_name"];
				$booking->booking_id = $row["rb_id"];
				$currentEvent->bookings[] = $booking;
			}
		}
		if(isset($currentEvent->id))
			$currentResource->events[] = $currentEvent;
		$resourceScheduleList[] = $currentResource;
		
		return $resourceScheduleList;
	}
	
	
}