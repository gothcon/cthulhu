<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class GroupRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "groups";
	protected $entityClassName = "Group";
	protected $databaseTableAlias = "g";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery("groups", "g", "Group");
		$query->addJoin("group_memberships", "gm", "id", "g", "leader_membership_id", new GroupMembership());
		$query->addJoin("people", "p", "id", "gm", "person_id", new Person());
		return $query->getQueryString();
	}
	
	public function loadSorted($property="name",$dir = "asc"){
		return parent::loadSorted($property,$dir);
	}
	
	
	public function PopulateEntity($group,$row,$prefix="g_"){
		$group = parent::PopulateEntity($group,$row,$prefix);
		$groupMembership = new GroupMembership();
		$groupMembership = parent::PopulateEntity($groupMembership,$row,"gm_");
		$person = new Person();
		$person = parent::PopulateEntity($person,$row,"p_");
		$groupMembership->setPerson($person);
		$group->setLeaderMembership($groupMembership);
		return $group;
	}
	
	public function loadByLeaderId($leaderId){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,array("name", "id"));
		$query->addJoin("group_memberships", "gm", "id", $this->databaseTableAlias, "leader_membership_id",array("id"));
		$query .= " where gm.person_id=:leaderId";
		$params = array("leaderId" => $leaderId);
		$statement = $this->execute($query,$params);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function checkName($name,$id){
		$query = "select count(id) > 0 as name_taken from groups where name like ".$this->quote($name)." and id != ".$this->quote($id);
		$statement = $this->execute($query);
		return $statement->fetchColumn() == "1";
	}
	
	public function loadList($personIsMemberId){
		$query = "
			SELECT g.name, g.email_address, g.id, CONCAT(p.id,'.',' ',p.first_name,' ',p.last_name) AS leader, !ISNULL(_isMember.id) AS is_member FROM groups g 
			LEFT JOIN group_memberships gm ON gm.id = g.leader_membership_id
			LEFT JOIN people p ON p.id = gm.person_id
			LEFT JOIN (SELECT gm.id,gm.group_id FROM group_memberships gm WHERE gm.person_id = :personIsMemberId) _isMember ON gm.group_id = _isMember.group_id
			ORDER BY g.name ASC
		";
		$statement = $this->execute($query,array("personIsMemberId" => $personIsMemberId));
		return $statement->fetchAll(PDO::FETCH_OBJ);
	}
}