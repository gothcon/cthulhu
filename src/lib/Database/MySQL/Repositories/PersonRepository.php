<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class PersonRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "people";
	protected $entityClassName = "Person";
	protected $databaseTableAlias = "p";
	
	public function loadSorted($property="last_name",$dir = "asc"){
		switch($property){
			case "id":
				$order = "order by p.id {$dir}";
				break;
			case "first_name":
				$order = "order by p.first_name {$dir},p.last_name {$dir}";
				break;
			case "last_name":
				$order = "order by p.last_name {$dir},p.first_name {$dir}";
				break;
			case "email":
				$order = "order by p.email_address {$dir}";
				break;
		}
		return $this->loadMany($this->getSelectAllQuery()." ".$order);
	}
	
	public function loadByEmail($emailAddress){
		return $this->loadMany($this->getSelectAllQuery()." where p.email_address = :address", array("address" => $emailAddress));
	}
	
}