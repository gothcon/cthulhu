<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class OrganizerRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "organizers";
	protected $entityClassName = "Organizer";
	protected $databaseTableAlias = "o";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("people",		"p", "id", "o", "person_id", new Person());
		$query->addJoin("groups",		"g", "id", "o", "group_id", new Group());
		return $query->getQueryString();
	}
	
	public function loadByPersonId($id){
		return $this->loadMany($this->getSelectAllQuery()." where person_id = :personId", array("personId" => $id));
	}
	
	public function loadByGroupId($id){
		return $this->loadMany($this->getSelectAllQuery()." where group_id = :groupId", array("groupId" => $id));
	}
	
	public function loadByEventId($id){
		return $this->loadMany($this->getSelectAllQuery()." where event_id = :eventId", array("eventId" => $id));
	}
	
	public function personIsEventOrganizer($personId,$eventId){
		$query = "
			SELECT
				COUNT(`organizers`.`event_id`) as is_organizer
				FROM `organizers` 
				LEFT JOIN `groups` ON `groups`.`id` = `organizers`.`group_id`
				LEFT JOIN `group_memberships` ON `group_memberships`.`id` = `groups`.`leader_membership_id`
				WHERE (`group_memberships`.`person_id` = ".$this->quote($personId)." OR `organizers`.`person_id` = ".$this->quote($personId).")
				AND `organizers`.`event_id` = ".$this->quote($eventId)."
			";
		$statement = $this->execute($query);
		return $statement->fetchColumn();
	}
	
	public function personIsSlotOrganizer($personId,$slotId){
		$query = "
			SELECT COUNT(`organizers`.`event_id`)
			FROM `signup_slots`
			LEFT JOIN `event_occations` ON `event_occations`.`id` = `signup_slots`.`event_occation_id`
			LEFT JOIN `organizers` ON `organizers`.`event_id` = `event_occations`.`event_id`
			LEFT JOIN `groups` ON `groups`.`id` = `organizers`.`group_id`
			LEFT JOIN `group_memberships` ON `group_memberships`.`id` = `groups`.`leader_membership_id`
			WHERE (`group_memberships`.`person_id` = ".$this->quote($personId)." OR `organizers`.`person_id` = ".$this->quote($personId).")
			AND `signup_slots`.`id` = ".$this->quote($slotId)."
		";
		$statement = $this->execute($query);
		return $statement->fetchColumn();
	}
	
	public function loadSortedEventOrganizerList($personId = 0, $property="event_type", $dir = "asc"){
		
		$query = "
			SELECT 
				o.id AS entity_id,
				o.name AS organizer_name, 
				e.id AS event_id, 
				e.name AS event_name,
				e.visible_in_public_listing,
				e.visible_in_schedule,
				et.id AS event_type_id, 
				et.name AS event_type_name, 
				`type` 
			
			FROM events e
			LEFT JOIN event_types et 
				ON et.id = e.eventtype_id
			"
					.( $personId ? "INNER JOIN" : "LEFT JOIN" ).	
"
			(
					SELECT 
						g.id AS id,
						g.NAME AS first_name, 
						g.NAME AS last_name, 
						g.name AS NAME,
						o.event_id AS event_id,
						'group' AS `type`
					FROM organizers o 
					INNER JOIN groups g ON o.group_id = g.id
					"
					.( $personId ? " LEFT JOIN group_memberships gm on gm.id = g.leader_membership_id WHERE gm.person_id = ".$this->quote($personId) : "" ).	
				"
					UNION 
					SELECT 
						p.id AS id,
						p.first_name,
						p.last_name,
						CONCAT(p.first_name,' ',p.last_name) AS NAME,
						o.event_id AS event_id,
						'person' AS `type` 
					FROM organizers o 
					INNER JOIN people p ON o.person_id = p.id
					"
					.( $personId ? "WHERE o.person_id = ".$this->quote($personId) : "" ).	
"
			) o
			ON  
			o.event_id = e.id
		";
		$order = "";
		switch($property){
			case "id":
				$order = "order by e.id {$dir}";
				break;
			case "organizer":
				$order = "order by last_name {$dir}, first_name {$dir}";
				break;
			case "name":
				$order = "order by e.name {$dir}";
				break;
			case "event_type":
				$order = "order by et.name {$dir}, e.name {$dir}";
				break;
		}
		$query .= $order;
		
		$resultSet = $this->execute($query);
		
		$eventList = array();
		
		while($obj = $resultSet->fetch(PDO::FETCH_OBJ)){
			$eventList[] = $obj;
		}
		return $eventList;
	}

	public function PopulateEntity($organizer,$row,$prefix="o_"){
		$organizer = parent::PopulateEntity($organizer,$row,$prefix);
		$person = new Person();
		$person = parent::PopulateEntity($person,$row,"p_");
		$group = new Group();
		$group = parent::PopulateEntity($group,$row,"g_");
		$organizer->group = $group;
		$organizer->person = $person;
		return $organizer;
	}
	
}