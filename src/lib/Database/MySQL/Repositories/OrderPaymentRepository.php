<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
class OrderPaymentRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "order_payments";
	protected $entityClassName = "OrderPayment";
	protected $databaseTableAlias = "op";
	
	public function getSelectAllQuery() {
		$query = new MySQLQuery($this->databaseTableName, $this->databaseTableAlias, $this->entityClassName);
		$query->addJoin("transactions", "tr","id","op","transaction_id", "Transaction");
		$query->addJoin("orders", "o","id","op","order_id", "Order");
		return $query->getQueryString();
	}
	
	public function getRecent($count = 10){
		$query = $this->getSelectAllQuery() . " order by op_id desc limit " . $this->quote($count,true);
		return $this->loadMany($query);
	}
	
	/**
	 * 
	 * @param int $orderId
	 * @return OrderPayment[]
	 */
	public function getPaymentsForOrder($orderId){
		$query = $this->getSelectAllQuery() . " where order_id = :order_id order by transaction_id desc";
		return $this->loadMany($query , array("order_id" => $orderId));
	}
	
	protected function populateEntity($orderPayment,$row,$prefix = "op_"){
		$orderPayment = parent::populateEntity($orderPayment,$row,$prefix);
		$order = parent::populateEntity(new Order(),$row,"o_");;
		$order->loadOrderRows();
		$orderPayment->order = $order;
		$orderPayment->transaction = parent::populateEntity(new Transaction(),$row,"tr_");
		return $orderPayment;
	}
	
}