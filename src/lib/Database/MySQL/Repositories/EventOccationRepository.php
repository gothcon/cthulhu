<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class EventOccationRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "event_occations";
	protected $entityClassName = "EventOccation";
	protected $databaseTableAlias = "eo";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("events",		"e", "id", $this->databaseTableAlias, "event_id", new Event());
		$query->addJoin("event_types",	"et", "id", "e", "eventtype_id", new EventType());
		$query->addJoin("timespans",	"t", "id", $this->databaseTableAlias, "timespan_id", new Timespan());
		return $query->getQueryString();
	}
	
	protected function PopulateEntity($eventOccation,$row,$prefix="eo_"){
		$eventOccation = parent::PopulateEntity($eventOccation,$row,$prefix);
		
		$event = new Event();
		$event = parent::PopulateEntity($event,$row,"e_");

		$eventtype = new EventType();
		$eventtype = parent::PopulateEntity($eventtype,$row,"et_");
		
		$timespan = new Timespan();
		$timespan = parent::PopulateEntity($timespan,$row,"t_");
		
		$event->eventtype = $eventtype;
		$eventOccation->event = $event;
		$eventOccation->timespan = $timespan;

		return $eventOccation;
	}
	
	public function loadEventOccationTree($eventId = 0){
		$query = $this->getSelectAllQuery();
		$param = array();
		if($eventId && is_numeric($eventId)){
			$param= array("et.event_id" => $eventId);
			$query .= "where et.event_id = :eventId";
		}
		$query .= " order by e.name asc";

		$eventOccations = $this->loadMany($query,$param,"PopulateEntity");
		$currentEvent = new stdClass();
		$tree = array();
		foreach($eventOccations as $eventOccation){
			$event = $eventOccation->getEvent();
			$timespan = $eventOccation->getTimespan();
			if(!isset($currentEvent->id) || $currentEvent->id != $event->id){
				if(isset($currentEvent->id)){
					$tree[] = $currentEvent;
				}
				$currentEvent = new stdClass();
				$currentEvent->id = $event->id;
				$currentEvent->name = $event->name;
				$currentEvent->eventOccations = array(); 
			}
			$currentEventOccation = new stdClass();
			$currentEventOccation->name = $eventOccation->name;
			$currentEventOccation->starts_at = $timespan->starts_at;
			$currentEventOccation->ends_at = $timespan->ends_at;
			$currentEventOccation->id = $eventOccation->id;
			$currentEvent->eventOccations[] = $currentEventOccation;
		}
		$tree[] = $currentEvent;
		return $tree;
	}
	
	public function loadByEvent($event){
		$id_parameters = -1;
		if(is_numeric($event)){
			$id_parameters = array("id" => $event);
		}else if(is_array($event)){
			$id_parameters = $event;
		}else if(is_object($event)){
			$id_parameters = array($event->id);
		}
		if(count($id_parameters) == 0)
			die("invalid ids");
			
		$where = "event_id in(";
		$first = true;
		foreach($id_parameters as $key => $id){
			if($first)
				$first = false;
			else
				$where .= ",";
			$where .= ":{$key}";
		}
		$where .= ")";
		$query = "{$this->getSelectAllQuery()} where {$where} order by t.starts_at asc, t.ends_at asc";
		return $this->loadMany($query,$id_parameters,"PopulateEntity");
	}
	
	public function loadOccationAndSlots($eventOccationId){
		$query = "
			SELECT 
				`event_occations`.`id`,
				`event_occations`.`timespan_id`,
				`event_occations`.`event_id`,
				`event_occations`.`name`,
				`event_occations`.`is_hidden` AS 'occation_is_hidden',
				`signup_slots`.`id` AS signup_slot_id,
				`signup_slots`.`slot_type_id`,
				`signup_slots`.`maximum_signup_count`,
				`signup_slots`.`maximum_spare_signup_count`,
				`signup_slots`.`requires_approval`,
				`signup_slots`.`is_hidden` AS 'slot_is_hidden',
				`timespans`.`starts_at`,
				`timespans`.`ends_at`,
				`articles`.`price`
			FROM `event_occations`
			LEFT JOIN `signup_slots` ON `signup_slots`.`event_occation_id` = `event_occations`.`id`
			LEFT JOIN `slot_types` ON `slot_types`.`id` = `signup_slots`.`slot_type_id`
			LEFT JOIN `timespans` ON `timespans`.`id` = `event_occations`.`timespan_id`
			LEFT JOIN `signup_slot_articles` ON `signup_slot_articles`.`signup_slot_id` = `signup_slots`.`id`
			LEFT JOIN `articles` ON `articles`.`id` = `signup_slot_articles`.`article_id`
			WHERE `event_occations`.`id` = :id";
		$params = array("id" => $eventOccationId);
		
		$result = $this->execute($query,$params);
		$currentEventOccation = null;
		$tree = array();
		
		while($obj = $result->fetchObject()){
			if(is_null($currentEventOccation)){
				$timespan = new Timespan();
				$timespan->starts_at = $obj->starts_at;
				$timespan->ends_at = $obj->ends_at;

				$currentEventOccation = new stdClass();
				$currentEventOccation->id = $obj->id;
				$currentEventOccation->timespan_id = $obj->timespan_id;
				$currentEventOccation->timespan = $timespan->getSmartIntervalString();
				$currentEventOccation->starts_at = $obj->starts_at;
				$currentEventOccation->ends_at = $obj->ends_at;
				$currentEventOccation->event_id = $obj->event_id;
				$currentEventOccation->name = $obj->name;
				$currentEventOccation->is_hidden = $obj->occation_is_hidden;
				$currentEventOccation->signup_slots = array();
			}
			if($obj->signup_slot_id){
				$signupSlot = new stdClass();
				$signupSlot->id = $obj->signup_slot_id;
				$signupSlot->slot_type_id = $obj->slot_type_id;
				$signupSlot->maximum_signup_count = $obj->maximum_signup_count;
				$signupSlot->maximum_spare_signup_count = $obj->maximum_spare_signup_count;
				$signupSlot->requires_approval = $obj->requires_approval;
				$signupSlot->is_hidden = $obj->slot_is_hidden;
				$signupSlot->price = $obj->price;
				$currentEventOccation->signup_slots[] = $signupSlot;
			}
		}
		return $currentEventOccation;
	}
	
}