<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_ArticleOrderingStatistics.php");

class OrderRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "orders";
	protected $entityClassName = "Order";
	protected $databaseTableAlias = "o";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("people", "p", "id", $this->databaseTableAlias, "person_id", new Person());
		return $query->getQueryString();
	}
	
	/**
	 * 
	 * @param Order $order
	 */
	public function save(&$order){
		$this->begin();
		$allOk = true;
		if($order->id > 0){
			$unmodifiedOrderRows = OrderRow::loadByOrder($order->id);
			
			$orderRows = $order->getOrderRows();
			
			$orderRowsToDelete = array();
			
			// add all order rows existing before modification
			foreach($unmodifiedOrderRows as $unmodifiedOrderRow){
				$orderRowsToDelete[$unmodifiedOrderRow->id] = $unmodifiedOrderRow->id;
			}
			
			// and remove all that still exists from the delete list
			foreach($orderRows as $orderRow){
				if(isset($orderRowsToDelete[$orderRow->id] )){
					unset($orderRowsToDelete[$orderRow->id]);
				}
			}
			
			// remove all order rows in the existing order
			foreach($orderRowsToDelete as $orderRowId){
				$allOk &= OrderRow::deleteById($orderRowId);
			}
		}
		// this check is not really necessary, but lessens the burden of the database since any operation carried out 
		// when not allOk will be rolled back
		if($allOk){
			$allOk &= parent::save($order);
			$orderRows = $order->getOrderRows();
			foreach($orderRows as $index => $orderRow){
				$orderRow->order_id = $order->id;
				$allOk &= $this->saveRow($orderRow);
				$orderRows[$index] = $orderRow;
			}
		}
		if($allOk){
			$this->commit();
			return true;
		}
		else{
			$this->rollback ();
			return false;
		}
	}
	
	protected function saveRow(&$row){
		$orderRowRepository = new OrderRowRepository($this->getConnection());
		if($row->article_id == 0){
			$row->article = null;
		}
		return $orderRowRepository->save($row);
	}

	/**
	 * 
	 * @param int $id
	 * @return Order
	 */
	public function loadByPersonId($id){
		$orders = $this->loadMany("{$this->getSelectAllQuery()} where o.person_id=:person_id",array("person_id" =>$id),"PopulateEntity");
		return $orders;
	}
	
	public function loadOpen($personId){
		$query = "{$this->getSelectAllQuery()} where o.person_id= ".$this->quote($personId). " AND o.status = ".$this->quote(Order::STATUS_CART);
		$order = $this->loadOne($query,array(),"PopulateEntity");
		
		if($order)
			$order->setOrderRows(OrderRow::loadByOrder ($order->id));
		return $order;
	}
	
	/**
	 * 
	 * @param Order $order
	 * @param array $row
	 * @param string $prefix
	 * @return Order
	 */
	public function PopulateEntity($order,$row,$prefix="o_"){
		$order = parent::PopulateEntity($order,$row,$prefix);
		$person = parent::PopulateEntity(new Person(), $row, "p_");
		$order->setPerson($person);
		return $order;
	}
	
	public function GetCategoryOrderingStatistics($mixed){
		if(!is_object($mixed)){
			$mixed = $this->load($mixed->id);
		}
		
		$query = "
			SELECT 
				articles.name as article_name,
				articles.id as article_id, 
				SUM( IF(order_rows.count,order_rows.count,0)) AS no_ordered, 
				SUM( IF(orders.status = 'paid',order_rows.count,0) ) AS no_paid, 
				SUM( IF(order_rows.status = 'delivered',1,0) ) as no_delivered
			FROM articles
			LEFT JOIN order_rows ON order_rows.article_id = articles.id 
			LEFT JOIN orders ON orders.id = order_rows.order_id AND orders.status != 'cancelled' AND orders.status != 'cart' ".( $mixed->id > 0 ?"
			WHERE 
				articles.l > ".$this->quote($mixed->l)." 
				AND articles.r < ".$this->quote($mixed->r)." 
				AND articles.type != 'category'
			": " WHERE articles.type != 'category'
			")."GROUP BY articles.id
			ORDER BY articles.name
		";
		$resultSet = $this->execute($query);
		$articles = $resultSet->fetchAll(PDO::FETCH_CLASS, "TreeItem_ArticleOrderingStatistics");
		return $articles;
		
	}
	
	public function GetArticleOrderingStatistics($articleId = 0){
		
		$query = "
			SELECT 
				articles.name as article_name,
				articles.id as article_id, 
				SUM( IF(order_rows.count,order_rows.count,0)) AS no_ordered, 
				SUM( IF(orders.status = 'paid',order_rows.count,0) ) AS no_paid, 
				SUM( IF(order_rows.status = 'delivered',1,0) ) as no_delivered
			FROM articles
			LEFT JOIN order_rows ON order_rows.article_id = articles.id 
			LEFT JOIN orders ON orders.id = order_rows.order_id AND orders.status != 'cancelled' AND orders.status != 'cart' ".( $articleId > 0 ?"
			WHERE articles.id = ".$this->quote($articleId)." AND articles.type != 'category'
			": " WHERE articles.type != 'category'
			")."GROUP BY articles.id
			ORDER BY articles.name
		";
		$resultSet = $this->execute($query);
		$articles = $resultSet->fetchAll(PDO::FETCH_CLASS, "TreeItem_ArticleOrderingStatistics");
		return $articles;
		
	}
	
	
}