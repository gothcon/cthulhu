<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class SlotTypeRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "slot_types";
	protected $entityClassName = "SlotType";
	protected $databaseTableAlias = "st";
}