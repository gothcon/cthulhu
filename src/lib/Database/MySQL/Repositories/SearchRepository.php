<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SearchRepository
 *
 * @author Joakim
 */
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."MySQLRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR . "Entities/SearchResultItem.php");
class SearchRepository extends MySQLRepository{
	public function doSearch($q,$r=null){
		$priorities = array(
			"name_eq" => 64,
			"type_eq" => 63,
			"name_begins_with" => 32,
			"type_begins_with" => 31,
			"word_begins_with" => 16,
			"name_ends_with" => 8,
			"name_contains" => 4,
			"type_contains" => 2
		);
			
		$q_beginsWith = $this->quote("{$q}%");
		$q_wordBeginsWith = $this->quote("% {$q}%");
		$q_contains = $this->quote("%{$q}%");	
		$q_equals = $this->quote($q);


		if($r != null){
			if($r == "all"){
				$repositories = array(
					"people" => true,
					"groups" => true,
					"events" => true,
					"resources" => true);
			}else{
				$repositories = array(
					"people" => false,
					"groups" => false,
					"events" => false,
					"resources" => false);

				if(is_array($r)){
					$temp = $r;
				}else{
					$temp = explode(",",$r);
				}
				foreach($temp as $repository){
					if(array_key_exists($repository,$repositories)){
						$repositories[$repository] = true;
					}
				}
			}
		}else{
			$repositories = array(
				"people" => true,
				"groups" => true,
				"events" => true,
				"resources" => true);
		}
		$query =""; 
		if($repositories["people"]){
			if(strlen($query))
				$query .= "UNOIN";
			$query .="
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_eq"]} AS priority FROM people WHERE CONCAT(first_name,' ',last_name) = {$q_equals})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_begins_with"]} AS priority FROM people WHERE CONCAT(first_name,' ',last_name) LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_eq"]} AS priority FROM people WHERE identification = {$q_equals})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_begins_with"]} AS priority FROM people WHERE identification LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_eq"]} AS priority FROM people WHERE last_name = {$q_equals})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_begins_with"]} AS priority FROM people WHERE first_name LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT CONCAT(first_name,' ',last_name, IF(ISNULL(identification),'',CONCAT(' (',identification,')'))) AS label, id, 'person' AS type, {$priorities["name_begins_with"]} AS priority FROM people WHERE last_name LIKE {$q_beginsWith})
			";
		}
		if($repositories["events"]){
			if(strlen($query))
				$query .= "UNION";
			$query .="
			(SELECT NAME AS label, id, 'event' AS type, {$priorities["name_eq"]} AS priority FROM events WHERE NAME = {$q_equals})
			UNION DISTINCT
			(SELECT NAME AS label, id, 'event' AS type, {$priorities["name_begins_with"]} AS priority FROM events WHERE NAME LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT NAME AS label, id, 'event' AS type, {$priorities["word_begins_with"]} AS priority FROM events WHERE NAME LIKE {$q_wordBeginsWith})
			UNION DISTINCT
			(SELECT e.name AS label, e.id, 'event' AS type, {$priorities["type_eq"]} AS priority FROM events e LEFT JOIN event_types t ON t.id = e.eventtype_id WHERE t.name = {$q_equals})
			UNION DISTINCT
			(SELECT e.name AS label, e.id, 'event' AS type, {$priorities["type_begins_with"]} AS priority FROM events e LEFT JOIN event_types t ON t.id = e.eventtype_id WHERE t.name LIKE {$q_beginsWith})
			";
		}
		if($repositories["groups"]){
			if(strlen($query))
				$query .= "UNION DISTINCT";
			$query .="
			(SELECT NAME AS label, id, 'group' AS type, {$priorities["name_eq"]} AS priority FROM groups WHERE NAME = {$q_equals})
			UNION DISTINCT
			(SELECT NAME AS label, id, 'group' AS type, {$priorities["name_begins_with"]} AS priority FROM groups WHERE NAME LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT NAME AS label, id, 'group' AS type, {$priorities["word_begins_with"]} AS priority FROM groups WHERE NAME LIKE {$q_wordBeginsWith})
			";
		}

		if($repositories["resources"]){
			if(strlen($query))
				$query .= "UNION";
			$query .="
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["name_eq"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE r.name = {$q_equals})
			UNION DISTINCT
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["name_begins_with"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE r.name LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["word_begins_with"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE r.name LIKE {$q_wordBeginsWith})
			UNION DISTINCT
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["type_eq"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE rt.name = {$q_equals})
			UNION DISTINCT
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["type_begins_with"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE rt.name LIKE {$q_beginsWith})
			UNION DISTINCT
			(SELECT r.name AS label, r.id, rt.name AS type, {$priorities["type_contains"]} AS priority FROM resources r LEFT JOIN resource_types rt ON rt.id = r.resource_type_id WHERE rt.name LIKE {$q_contains})
			";
		}

		$queryString ="SELECT label,id,type,max(priority) as priority FROM(
			$query
		)AS t GROUP BY label ORDER BY priority DESC, label ASC
		";

		$result = $this->execute($queryString);

		$searchResult = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return $searchResult;
	}

	public function searchPerson($firstName,$lastName,$identification){
		/** TODO fix search **/
		$whereClauses = array();
		
		if(strlen($firstName))
			$whereClauses[] = "first_name like ".$this->quote("{$firstName}%");
		if(strlen($lastName))
			$whereClauses[] = "last_name like ".$this->quote("{$lastName}%");
		if(strlen($identification))
			$whereClauses[] = "identification like ".$this->quote("{$identification}%");
		
		if(count($whereClauses) == 0){
			$query = "select first_name,last_name,id,identification from people";
			
		}else{
			$query = "select first_name,last_name,id,identification from people WHERE " . implode(" AND ",$whereClauses);
		}
		$query .= " ORDER BY last_name asc, first_name asc, identification";

		$statement = $this->execute($query);
		return $statement->fetchAll(PDO::FETCH_CLASS);
		
		
	}
	
	
}

?>
