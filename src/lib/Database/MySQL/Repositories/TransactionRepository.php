<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class TransactionRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "transactions";
	protected $entityClassName = "Transaction";
	protected $databaseTableAlias = "tr";
	
	
	public function loadAllTransactions($paymentMethod = "",$from = null,$to = null) {
		$query = $this->getSelectAllQuery();
		$operation = "where";
		if($paymentMethod != ""){
			$query .= "{$operation} payment_method = " . $this->quote($paymentMethod);
			$operation = " and";
		}
		if(! is_null($from)){
			if(is_numeric($from)){
				$query .= "{$operation} id >= " . $this->quote($from);
				$operation = " and";
			}else if(strtotime($from) != 0){
				$query .= "{$operation} created_at >= " . $this->quote($from);
				$operation = " and";
			}
		}

		if(! is_null($to)){
			if(is_numeric($to)){
				$query .= "{$operation} id <= " . $this->quote($to);
				$operation = " and";
			}else if(strtotime($to) != 0){
				$query .= "{$operation} created_at <= " . $this->quote($to);
				$operation = " and";
			}
		}
		
		return $this->loadMany($query. "order by id desc");
	}
	
	
}