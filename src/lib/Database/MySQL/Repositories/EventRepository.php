<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_EventType.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_Event.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_EventOccation.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_SignupSlot.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_Signup.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../ListItems/ListItem_Event.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class EventRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "events";
	protected $entityClassName = "Event";
	protected $databaseTableAlias = "e";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("event_types",	"et", "id", $this->databaseTableAlias, "eventtype_id", new EventType());
		$query->addJoin("organizers",	"o", "event_id", $this->databaseTableAlias, "id", new Organizer());
		$query->addJoin("people",		"p", "id", "o", "person_id", new Person());
		$query->addJoin("groups",		"g", "id", "o", "group_id", new Group());
		return $query->getQueryString();
	}
	
	protected function PopulateEntity($event,$row,$prefix="e_"){
	
		$event = parent::PopulateEntity($event,$row,$prefix);
		$event->eventtype = parent::PopulateEntity( new Eventtype(),$row,"et_");

		$organizer = parent::PopulateEntity(new Organizer(),$row,"o_");
		
		if($row["p_id"])
			$organizer->person = parent::PopulateEntity(new Person(),$row,"p_");
		else
			$organizer->group = parent::PopulateEntity(new Group(),$row,"g_");
		
		$organizer->setEventReference($event);
		$event->organizer = $organizer;
		
		return $event;
	}
	
	public function getCustomListQuery(){
		$query = "
			SELECT e.name, e.id AS event_id, et.name AS event_type, et.id AS event_type_id, organizers.organizer_type,  organizers.organizer_entity_id, organizers.first_name, organizers.last_name, organizers.organizer_id FROM `events` e
				LEFT JOIN `event_types` et ON et.id = e.`eventtype_id`
				LEFT JOIN(
					SELECT o.event_id, o.id AS organizer_id, p.first_name, p.last_name, p.id AS organizer_entity_id, 'person' AS organizer_type
					FROM `organizers` o
					LEFT JOIN people p ON p.id = o.person_id
					WHERE !ISNULL(o.person_id)
					UNION
					SELECT o.event_id, o.id, g.name, g.name, g.id, 'group' 
					FROM `organizers` o
					LEFT JOIN groups g ON g.id = o.group_id
					WHERE !ISNULL(o.group_id)
				) organizers ON organizers.event_id = e.id
		";
		return $query;
	}
	
	public function populateListEntity($entity,$row,$prefix = ""){
		$obj = new stdClass();
		$obj->event = $row["name"];
		$obj->event_id = $row["event_id"];
		$obj->event_type_id = $row["event_type_id"];
		$obj->event_type = $row["event_type"];
		$obj->organizer_type = $row["organizer_type"];
		$obj->organizer_entity_id = $row["organizer_entity_id"];
		$obj->organizer_name = $row["first_name"];
		if($row["organizer_type"] == "person"){
			$obj->organizer_name .= " " . $row["last_name"];
		}
		return $obj;
	}
	
	public function loadSorted($property="event_type",$dir = "asc"){
		
		switch($property){
			case "id":
				$order = "order by e.id {$dir}";
				break;
			case "name":
				$order = "order by e.name {$dir}";
				break;
			case "event_type":
				$order = "order by et.name {$dir}, e.name {$dir}";
				break;
		}
		
		return $this->loadMany($this->getCustomListQuery()." ".$order,array(),"populateListEntity");
	}	
	
	/**
	 * @param mixed $type
	 * @return Event[]
	 */
	public function loadByType($type){
		
		if(is_numeric($type) && $type > 0){
			$id_s = array($type);
		}else if(is_array($type)){
			$id_s = $type;
		}else if(is_object($type)){
			$id_s = array($type->id);
		}
		if(count($id_s) == 0)
			die("invalid ids");
		
		
		$where = "where e.eventtype_id in(";
		$first = true;
		$placeHolderKeys = array_keys($id_s);
		
		foreach($placeHolderKeys as $key){
			if($first)
				$first = false;
			else
				$where .= ",";
			$where .= ":".$key;
		}
		
		$where .= ")";
		
		$query = "
			{$this->getSelectAllQuery()}
			{$where}";
			// die($query);
		return $this->loadMany($query,$id_s,"PopulateEntity");
	}
	
	public function loadByOrganizer($groupOrPerson){
		$type = get_class_name($groupOrPerson);
			
		if($type == "Person"){
			$where = "p_id = :id";
		}else if($type == "Group"){
			$where = "g_id = :id";
		}

		return $this->loadMany( $this->getSelectAllQuery()." where ".$where , array( "id" => $groupOrPerson->id) , "PopulateEntity");
	}
	
	/**
	 * 
	 * @param bool $onlyPublic
	 * @param int $eventType
	 * @param int $event
	 * @param bool $includeEmptyEventTypes
	 * @return TreeItem_EventType[]
	 */
	public function loadTree($onlyPublic = true, $eventType = 0, $event = 0, $includeEmptyEventTypes = false){
		 
		$params = array();
		
		$query = "
			SELECT 
				`event_types`.`name` AS `event_type`,
				`event_types`.`id` AS `event_type_id`,
				`events`.`name` AS `event`,
				`events`.`id` AS `event_id`,
				`events`.`preamble` AS `event_preamble`,
				`events`.`text` AS `event_text`,
				`events`.`info` AS `event_info`,
				`events`.`schedule_name` AS `event_schedule_name`,
				organizers.organizer,
				organizers.organizer_id,
				organizers.organizer_type,
				`event_occations`.`name` AS `event_occation`,
				`event_occations`.`id` AS `event_occation_id`,
				`timespans`.`name` AS `timespan`,
				`timespans`.`starts_at`,
				`timespans`.`ends_at`
			FROM
				`event_types`
			LEFT JOIN 
				`events` on `events`.`eventtype_id` = `event_types`.`id`" .($onlyPublic ? " AND events.visible_in_public_listing = 1" : "" ). " 
			LEFT JOIN (
				SELECT 
					CONCAT(`people`.`first_name`, ' ',`people`.`last_name`) AS organizer,
					`people`.`id` AS organizer_id,
					'person' AS organizer_type,
					`organizers`.`event_id`
				FROM `organizers`
				INNER JOIN `people` ON `people`.`id` = `organizers`.`person_id`
				UNION 
				SELECT 
					`groups`.`name` AS organizer,
					`groups`.`id` AS organizer_id,
					'group' AS organizer_type,
					`organizers`.`event_id` 
				FROM `organizers`
				INNER JOIN `groups` ON `groups`.`id` = `organizers`.`group_id`
			) organizers ON `events`.`id` = `organizers`.`event_id`
			LEFT JOIN `event_occations` ON `event_occations`.`event_id` = `events`.`id`" .($onlyPublic ? " AND !event_occations.is_hidden = 1" : "" ). "
			LEFT JOIN `timespans` ON `timespans`.`id` = `timespan_id`";
		
		if($eventType > 0){
			$params["event_type_id"] = $eventType;
			$query .="
				WHERE `event_types`.`id` = :event_type_id
			";
		}
		
		if($event > 0){
			$params["event_id"] = $event;
			$query .="
				WHERE `events`.`id` = :event_id
			";
		}
		
		$orderBy = "
			ORDER BY 
				`event_types`.`name` asc,
				`events`.`name` asc,
				`timespans`.`starts_at` asc,
				`timespans`.`ends_at` desc
		";
		
		$resultSet = $this->execute($query.$orderBy,$params);

		$eventTypes				= array(); 	
		$currentEventType		= null;
		$currentEvent			= null;
		$currentEventOccation	= null;

		$currentEventTypeCount		= 0;
		$currentEventCount			= 0;
		$currentEventOccationCount	= 0;



		
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			
			$event_type_id = $row["event_type_id"];
			$event_id = $row["event_id"];
			$event_occation_id = $row["event_occation_id"];

			
			if($event_type_id > 0){
				if($currentEventType == null || $currentEventType->id != $event_type_id){
					// if we dont want to include empty event types
					// filter them out here to keep the sql query simpler at the cost of a bit more data fetched (one extra row)
					if($currentEventType != null && !$includeEmptyEventTypes && $currentEventCount == 0){
						$currentEventTypeCount--;
						unset($eventTypes[$currentEventTypeCount]);
					}
					$eventType = new TreeItem_EventType;
					$eventType->events = array();
					$eventType->id = $row["event_type_id"];
					$eventType->name = $row["event_type"];
					$eventTypes[$currentEventTypeCount] = $eventType;
					$currentEventType = &$eventTypes[$currentEventTypeCount++];
					$currentEventCount = 0;
				}
			}

			if($event_id > 0){
				if($currentEvent == null || $currentEvent->id != $event_id){
					$event = new TreeItem_Event;
					$event->event_occations = array();
					$event->id = $row["event_id"];
					$event->name = $row["event"];
					$event->preamble = $row["event_preamble"];
					$event->text = $row["event_text"];
					$event->info = $row["event_info"];
					$event->schedule_name = $row["event_schedule_name"];
					$event->organizer = $row["organizer"];
					$event->organizer_id = $row["organizer_id"];
					$event->organizer_type = $row["organizer_type"];
					$currentEventType->events[$currentEventCount] = $event;
					$currentEvent = &$currentEventType->events[$currentEventCount++];
					$currentEventOccationCount = 0;
				}
			}
			
			if($event_occation_id > 0){
				if($currentEventOccation == null || $currentEventOccation->id != $event_occation_id){
					$eventOccation = new TreeItem_EventOccation;
					$eventOccation->id = $row["event_occation_id"];
					$eventOccation->name = $row["event_occation"];
					$eventOccation->timespan_name = $row["timespan"];
					$eventOccation->starts_at = $row["starts_at"];
					$eventOccation->ends_at = $row["ends_at"];
					$timespan = new Timespan();
					$timespan->starts_at = $eventOccation->starts_at;
					$timespan->ends_at = $eventOccation->ends_at;
					$timespan->name = $eventOccation->timespan_name;
					$eventOccation->timespan_as_string = $timespan->getSmartIntervalString();
					$eventOccation->full_name = $eventOccation->timespan_as_string . (strlen($eventOccation->name) ? " - ".$eventOccation->name : "");
					$currentEvent->event_occations[$currentEventOccationCount] = $eventOccation;
					$currentEventOccation = &$currentEvent->event_occations[$currentEventOccationCount++];
				}
			}
		}	

		return $eventTypes;
		
		
	}

	/** @return ListItem_Event[] */
	protected function _loadList($orderClause = ""){
		$query = "
			SELECT 
				`event_types`.`name` AS `event_type`,
				`event_types`.`id` AS `event_type_id`,
				`events`.`name` AS `event`,
				`events`.`id` AS `event_id`,
				`events`.`schedule_name` AS `event_schedule_name`,
				organizers.organizer,
				organizers.organizer_id,
				organizers.organizer_type,
				`events`.`visible_in_schedule`,
				`events`.`visible_in_public_listing`,
				organizers.email_address as organizer_email_address
			FROM
				`event_types`
			LEFT JOIN 
				`events` ON `events`.`eventtype_id` = `event_types`.`id`
			LEFT JOIN (
				SELECT 
					CONCAT(`people`.`last_name`, ', ',`people`.`first_name`) AS organizer,
					`people`.`id` AS organizer_id,
					'person' AS organizer_type,
					`organizers`.`event_id`,
					`people`.`email_address`,
					CONCAT(`people`.`last_name`, ' ',`people`.`first_name`) as organizer_sort_key
				FROM `organizers`
				INNER JOIN `people` ON `people`.`id` = `organizers`.`person_id`
				UNION 
				SELECT 
					`groups`.`name` AS organizer,
					`groups`.`id` AS organizer_id,
					'group' AS organizer_type,
					`organizers`.`event_id`, 
					`groups`.`email_address`,
					`groups`.`name` AS organizer_sort_key
				FROM `organizers`
				INNER JOIN `groups` ON `groups`.`id` = `organizers`.`group_id`
			) organizers ON `events`.`id` = `organizers`.`event_id` 
			{$orderClause}
		";
		$resultSet = $this->execute($query);
		return $resultSet->fetchAll(PDO::FETCH_CLASS, "ListItem_Event");
	}
	
	public function loadList($property="event_type",$dir = "asc"){
		switch($property){
			case "id":
				$order = "order by event_id {$dir}";
				break;
			case "organizer":
				$order = "order by organizer_sort_key {$dir}";
				break;
			case "name":
				$order = "order by event {$dir}";
				break;
			case "type":
				$order = "order by event_type {$dir}, event {$dir}";
				break;
			default:
				$order = "";
		}
		
		return $this->_loadList($order);
	}
	
	public function loadEventResourceSchedules($eventId = null){
		$query = new MySQLQuery("event_occations", "eo", array("name","id"));		
		$query->addJoin("events",								"e",	"id",					"eo",	"event_id",			array("name","id"));
		$query->addJoin("timespans",							"t",	"id",					"eo",	"timespan_id",		array("starts_at","ends_at","id","name"));
		$query->addJoin("event_occation_bookings",				"eob",	"event_occation_id",	"eo",	"id",				array("id"));
		$query->addJoin("bookings",								"b",	"id",					"eob",	"booking_id",		array("id"));
		$query->addJoin("resources",							"r",	"id",					"b",	"resource_id",		array("name","id"));
		$query->addJoin("resource_types",						"rt",	"id",					"r",	"resource_type_id", array("name"));
		$queryString = $query->getQueryString();
		// die($query);
		$params = array();
		$operator = " where";
		
		if($eventId){
			$queryString .= "{$operator} e.id = :eventId";
			$params["eventId"] = $eventId;
			$operator = "and";
		}
		
		$events = array(); 	
		
		$resultSet = $this->execute($queryString." ORDER BY e.name ASC",$params);
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			
			$event_id = $row["e_id"];
			$event_occation_id = $row["eo_id"];
			$resource_id = $row["r_id"];
			$resource_booking_id = $row["b_id"];
			
			if(!isset($events[$event_id]) && $event_id > 0){
				$event = parent::PopulateEntity(new Event(),$row,"e_");
				$events[$event_id] = new StdClass();
				$events[$event_id]->name = $event->name; 
				$events[$event_id]->id = $event->id; 
				$events[$event_id]->eventOccations = array();
				$events[$event_id]->resources = array();
			}
			
			if(!isset($events[$event_id]->resources[$resource_id]) && $resource_id > 0){

				$resource = parent::PopulateEntity(new Resource(),$row,"r_");
				$resourceType = parent::PopulateEntity(new ResourceType(),$row,"rt_");
				
				$events[$event_id]->resources[$resource_id] = new stdClass();
				$events[$event_id]->resources[$resource_id]->name = $resource->name;
				$events[$event_id]->resources[$resource_id]->id = $resource->id;
				$events[$event_id]->resources[$resource_id]->resourceType = $resourceType->name;
				$events[$event_id]->resources[$resource_id]->bookings = array();
				
			}	
			
			if(!isset($events[$event_id]->resources[$resource_id]->bookings[$resource_booking_id]) && $resource_booking_id){

				$eventOccation = parent::PopulateEntity(new EventOccation(),$row,"eo_");
				$timespan = parent::PopulateEntity(new Timespan(), $row, "t_");
				$events[$event_id]->resources[$resource_id]->bookings[$resource_booking_id] = new stdClass();
				$events[$event_id]->resources[$resource_id]->bookings[$resource_booking_id]->starts_at = $timespan->starts_at;
				$events[$event_id]->resources[$resource_id]->bookings[$resource_booking_id]->ends_at = $timespan->ends_at;
				$events[$event_id]->resources[$resource_id]->bookings[$resource_booking_id]->booking_id = $resource_booking_id;
			}
			
			if(!isset($events[$event_id]->eventOccations[$event_occation_id]) && $event_occation_id > 0){

				$eventOccation = parent::PopulateEntity(new EventOccation(),$row,"eo_");
				$timespan = parent::PopulateEntity(new Timespan(), $row, "t_");
				$eventOccation->setTimespan($timespan); 
				$events[$event_id]->eventOccations[$event_occation_id] = new StdClass();
				$events[$event_id]->eventOccations[$event_occation_id]->name = $eventOccation->name;
				$events[$event_id]->eventOccations[$event_occation_id]->starts_at = $timespan->starts_at;
				$events[$event_id]->eventOccations[$event_occation_id]->ends_at = $timespan->ends_at;
				$events[$event_id]->eventOccations[$event_occation_id]->id =  $eventOccation->id;
			}
			
		}	

		return $events;
	}
	
	
	 
	
}

class EventTree{
	public $events,$signupSlots,$eventTypes,$eventOccations;
	function __construct($eventTypes,$events,$occations,$slots){
		$this->eventTypes = $eventTypes;
		$this->events = $events;
		$this->eventOccations = $occations;
		$this->signupSlots = $slots;
	}
}