<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
class ArticleRepository extends MySQLReadWriteRepository{
	
	protected $databaseTableName = "articles";
	protected $entityClassName = "Article";
	protected $databaseTableAlias = "a";
	
	
	public function getSelectAllQuery() {
		$temp = new Image();
		$properties = $temp->getPersistedProperties();
		unset($properties["content"]);
		$properties = array_keys($properties);
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("images", "i", "id", "a", "image_id",$properties);
		return $query->getQueryString();
	}
	
	
	protected function populateEntity($entity, $row, $prefix = false) {
		$article = parent::populateEntity($entity, $row, $prefix);
		$image = parent::populateEntity(new Image, $row,"i_");
		$article->setImage($image);
		return $article;
	}
	
	/**
	 * @return $article[] 
	 */
	public function getParentOrderedList(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin($this->databaseTableName, "p", "id", $this->databaseTableAlias, "id", $this->entityClassName);
		$queryString = "{$query->getQueryString()} ORDER BY p.name, {$this->databaseTableAlias}.name";
		return $this->loadMany($queryString,array(),"PopulateEntity");
	}
	
	public function delete($mixed){
		$id = (is_object($mixed)) ? $mixed->id : $mixed; 
		$selectSubnodesQuery = "SELECT count(*) as 'nodesToDeleteCount' from articles where l >= :l and r <= :r";
		$deleteQuery = "delete from articles where l >= :l and r <= :r";
		$updateLeftValueQuery = "
			UPDATE articles
			SET l = l - :count
			WHERE l > :r
			ORDER BY l ASC ";
		$updateRightValueQuery = "
			UPDATE articles
			SET r = r - :count
			WHERE r > :r
			ORDER BY l ASC ";

		
		// -------------------------------------------------------------------- transaction begins
		$this->begin();

		// get the node to delete
		$article = $this->load($id);

		// store the left and right nodeid:s
		$selected_left_value = $article->l;
		$selected_right_value = $article->r;

		// get a list of all the nodes to be deleted (selected and ascendants)
		$resultSet = $this->execute($selectSubnodesQuery,array("l" => $selected_left_value,"r" => $selected_right_value));

		$result = $resultSet->fetch(PDO::FETCH_ASSOC);
		$count = $result["nodesToDeleteCount"] * 2;

		// delete nodes
		$this->execute($deleteQuery, array("l" => $selected_left_value,"r" => $selected_right_value));

		// update all the nodes with a higher r-value set l-value
		$this->execute($updateLeftValueQuery,array("count" => $count, "r" => $selected_right_value));
		$this->execute($updateRightValueQuery,array("count" => $count, "r" => $selected_right_value));

			// unlock table
		$this->commit();
		// -------------------------------------------------------------------- transaction is commited
		return true;

	}
	
	protected function insert(&$article){
		//$this->begin();
		// get parent query
		$parentQuery = "SELECT * FROM {$this->databaseTableName} as {$this->databaseTableAlias} WHERE article_id = ?";
		$selectQuery = "SELECT * FROM {$this->databaseTableName} as {$this->databaseTableAlias} order by r desc LIMIT 1";

		$updateLeftValueQuery = "
			UPDATE {$this->databaseTableName} as {$this->databaseTableAlias}
			SET l = l + 2
			WHERE l > :r
			ORDER BY l DESC ";

		$updateRightValueQuery = "
			UPDATE {$this->databaseTableName} as {$this->databaseTableAlias}
			SET r = r + 2
			WHERE r >= :r
			ORDER BY l DESC ";

		

		// lock database
		$imageRepository = new ImageRepository();


		$this->lockTable(array($this,$imageRepository));
		
					
		
		if($article->p == 0){
			$firstAsList = $this->loadMany("{$this->getSelectAllQuery()} order by r desc LIMIT 1", array(),"PopulateEntity");
			$rootNode = $firstAsList[0];
			$article->l = $rootNode->r + 1;
			$article->r = $rootNode->r + 2;
		}
		else{
			$parentNode = $this->load($article->p);
			$selected_left_value = $parentNode->l;
			$selected_right_value = $parentNode->r;

			
			
			$article->l = $selected_right_value;
			$article->r = $selected_right_value + 1;

			// update all the nodes with a higher r-value set l-value
			$this->execute($updateLeftValueQuery,array("r" => $selected_right_value));
			$this->execute($updateRightValueQuery,array("r" => $selected_right_value));
			
			
		}

		parent::insert($article);
		$this->unlockTables();
		//$this->commit();
		return true;
	}
		
	protected function update(&$article){
		// lock database
				// lock database
		$imageRepository = new ImageRepository();
		

		$this->lockTable(array($this,$imageRepository));
		$originalArticle = $this->load($article->id);
		$article->l = $originalArticle->l;
		$article->r = $originalArticle->r;
		$article->p = $originalArticle->p;
		parent::update($article);
		$this->unlockTables();

	}
	
	/**
	 *
	 * @return Article 
	 */
	public function getRootNode(){
		$rootNode = $this->load(1);	
		return $rootNode;
	}
	
	/**
	 * @param mixed $mixed Either an id or an Article object
	 * @return $article[] 
	 */
	public function loadPath($mixed){
		if(is_numeric($mixed)){
			$article = $this->load($article);
		}else{
			$article = $mixed;
		}
		
		if(!$article || $article->id == 0){
			return array($article);
		}
		$query = $this->getSelectAllQuery()."
			WHERE a.l <= :l AND a.r >= :r
			ORDER BY a.l ASC
		";
		$params = array("l"=> $article->l,"r" => $article->r);
		return $this->loadMany($query,$params,"PopulateEntity");
	}
	
	/**
	 *
	 * @param type $article
	 * @return $article[] 
	 */
	public function loadSiblings($article){
		$query = $this->getSelectAllQuery()."
			WHERE a.p = :p
			ORDER BY a_l ASC
		";
		$params = array("p" => $article->p);
		
		return $this->loadMany($query,$params,"PopulateEntity");
	}

	/**
	 *
	 * @param type $article
	 * @return $article[] 
	 */
	public function getChildren($mixed){
		if(is_object($mixed)){
			$mixed = $mixed->id;
		}
		
		$query = $this->getSelectAllQuery()."
			WHERE a.p = ".$this->quote($mixed)."
			ORDER BY a.name ASC
		";
		
		return $this->loadMany($query);
		
	}
	
		/**
	 *
	 * @param type $article
	 * @return $article[] 
	 */
	public function getAllChildren($mixed,$type){
		
		if($type == null){
			$query = $this->getSelectAllQuery()."
				WHERE a.l > ".$this->quote($mixed->l)." 
				AND a.r < ".$this->quote($mixed->r)." 
				ORDER BY a.name ASC
			";
		}else{
			$query = $this->getSelectAllQuery()."
				WHERE a.l > ".$this->quote($mixed->l)." 
				AND a.r < ".$this->quote($mixed->r)."
				AND a.type = '".$type."'
				ORDER BY a.name ASC
			";
		
		}
		return $this->loadMany($query);
		
	}
	
		/**
	 *
	 * @param type mixed
	 * @return $article[] 
	 */
	public function loadPublicChildren($mixed){
		if(is_a($mixed, "ReadOnlyEntity")){
			$mixed = $mixed->id;
		}
		$now = time();
		$query = $this->getSelectAllQuery()."
			WHERE a.p = ".$this->quote($mixed)."
			AND			
				( ISNULL(a.available_from) OR UNIX_TIMESTAMP(a.available_from) <= ".$this->quote($now)." )
			AND 
				( ISNULL(a.available_to) OR UNIX_TIMESTAMP(a.available_to) >= ".$this->quote($now)." )
			ORDER BY a.name ASC
		";
		return $this->loadMany($query);
		
	}
	
	/**
	 *
	 * @param type $article
	 * @return $article[] 
	 */
	public function loadPublicSiblings($mixed){
		if(is_a($mixed, "ReadOnlyEntity")){
			$mixed = $mixed->p;
		}
		$now = time();
		$query = $this->getSelectAllQuery()."
			WHERE a.p = ".$this->quote($mixed)."
			AND			
				( ISNULL(a.available_from) OR UNIX_TIMESTAMP(a.available_from) <= ".$this->quote($now)." )
			AND 
				( ISNULL(a.available_to) OR UNIX_TIMESTAMP(a.available_to) >= ".$this->quote($now)." )
			ORDER BY a.name ASC
		";
		return $this->loadMany($query);
	}

}