<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
class BookingRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "bookings";
	protected $entityClassName = "Booking";
	protected $databaseTableAlias = "b";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,new $this->entityClassName());
		$query->addJoin("timed_bookings",			"tb",	"booking_id",	$this->databaseTableAlias,	"id",				new TimedBooking());
		$query->addJoin("event_bookings",			"eb",	"booking_id",	$this->databaseTableAlias,	"id",				new EventBooking());
		$query->addJoin("events",					"e",	"id",			"eb",						"event_id",			new Event());
		$query->addJoin("event_types",				"et",	"id",			"e",						"eventtype_id",		new EventType());
		$query->addJoin("event_occation_bookings",	"eob",	"booking_id",	$this->databaseTableAlias,	"id",				new EventOccationBooking());
		$query->addJoin("event_occations",			"eo",	"id",			"eob",						"event_occation_id",new EventOccation());
		$query->addJoin("events",					"e2",	"id",			"eo",						"event_id",			new Event());
		$query->addJoin("event_types",				"et2",	"id",			"e2",						"eventtype_id",		new EventType());
		$query->addJoin("timespans",				"t",	"id",			"eo",						"timespan_id",		new Timespan());
		$query->addJoin("resources",				"r",	"id",			$this->databaseTableAlias,	"resource_id",		new Resource());
		$query->addJoin("resource_types",			"rt",	"id",			"r",						"resource_type_id",	new ResourceType());
		return $query->getQueryString();
	}
	
	/**
	 * 
	 * @param ReadWriteEntity $entity
	 * @param array $row
	 * @param string $prefix
	 * @return IBookingProxy
	 */
	protected function populateEntity($entity, $row, $prefix = "b_") {
		$booking = parent::populateEntity($entity, $row, $prefix);
		$resource = parent::populateEntity(new Resource, $row, "r_");
		$resourceType = parent::populateEntity(new ResourceType, $row, "rt_");
		$resource->resourceType = $resourceType;
		$booking->setResource($resource);
		if($row["tb_id"]){
			$timedBooking  = parent::populateEntity(new TimedBooking(), $row,"tb_");
			$timedBooking->setBooking($booking);
			return $timedBooking;
		}
		else if($row["eb_id"]){
			// Event booking
			$eventBooking = parent::populateEntity(new EventBooking(), $row, "eb_");
			$event = parent::populateEntity(new Event(), $row, "e_");
			$eventType = parent::populateEntity(new EventType(), $row, "et_");
			$event->setEventType($eventType);
			$eventBooking->setEvent($event);
			$eventBooking->setBooking($booking);
			return $eventBooking;
		}
		else if($row['eob_id']){
			
			$event = parent::populateEntity(new Event(), $row, "e2_");
			$event->setEventType(parent::populateEntity(new EventType(), $row, "et2_"));

			$timespan = parent::populateEntity(new Timespan(),$row,"t_");
			
			$eventOccation = parent::populateEntity(new EventOccation(), $row,"eo_");
			$eventOccation->setEvent($event);
			$eventOccation->setTimespan($timespan);
			
			$event_occation_booking = parent::populateEntity(new EventOccationBooking(),$row, "eob_");
			$event_occation_booking->setEventOccation($eventOccation);
			$event_occation_booking->setBooking($booking);
			return $event_occation_booking;
		}
		return $booking;
	}
	
	private function getAvailableResources($eventId,$eventOccation_id, $includeUnavailable = true, $sleepingEvent = false){
		$query = "
SELECT * FROM(			
			SELECT r.id AS resource_id, r.name AS resource, rt.name AS resource_type, COUNT(is_overlapping) = 0 AS is_available, COUNT(is_same_occation) > 0 AS is_already_booked 
			FROM  resources r 
			LEFT JOIN resource_types rt ON rt.id = r.resource_type_id
			LEFT JOIN(
				SELECT *,
					CASE
						WHEN occation = booking_occation THEN NULL
						WHEN occation_starts_at > booking_starts_at AND occation_starts_at < booking_ends_at THEN 1
						WHEN occation_ends_at > booking_starts_at AND occation_ends_at < booking_ends_at THEN 1
						WHEN occation_starts_at <= booking_starts_at && occation_ends_at >= booking_ends_at THEN 1
						WHEN booking_starts_at <= occation_starts_at && booking_ends_at >= occation_ends_at THEN 1
						ELSE NULL
					END 
					AS is_overlapping,
					CASE 
						WHEN occation = booking_occation THEN 1
						ELSE NULL
					END 
					AS is_same_occation
				FROM
				(
					SELECT 
						t.starts_at AS occation_starts_at, 
						t.ends_at AS occation_ends_at,
						eo.event_id AS occation_event, 
						eo.id AS occation
					FROM 	event_occations eo
					LEFT JOIN timespans t ON t.id = eo.timespan_id
					WHERE ".
								(($eventOccation_id) ? "eo.id={$this->quote($eventOccation_id)}" : "eo.event_id={$this->quote($eventId)}") 
							."
				) 
				event_occations
				JOIN (	

					SELECT 
						b.resource_id,
						starts_at AS booking_starts_at,
						ends_at AS booking_ends_at,
						booking_id,
						NULL AS booking_occation,
						NULL AS booking_event
					FROM timed_bookings tb
					LEFT JOIN bookings b ON b.id = tb.booking_id
					UNION

					SELECT
						b.resource_id,
						'1900-01-01 00:00:00', 
						'9999-12-31 23:59:59',
						booking_id,
						NULL,
						eb.event_id
					FROM event_bookings eb
					LEFT JOIN bookings b ON b.id = eb.booking_id
					UNION

					SELECT 
						b.resource_id,
						t.starts_at,
						t.ends_at, 
						eob.booking_id,
						eob.event_occation_id,
						eo.event_id
					FROM event_occation_bookings eob
					LEFT JOIN bookings b ON b.id = eob.booking_id
					LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
					LEFT JOIN timespans t ON t.id = eo.timespan_id

				) resource_bookings
			) 
			resource_booking_status ON resource_booking_status.resource_id = r.id
			
			GROUP BY r.id
			ORDER BY rt.name, r.name
) available_resources
		";
		
		if($sleepingEvent){
			$query .= "
				LEFT JOIN sleeping_resources sr ON sr.resource_id = available_resources.resource_id
				WHERE !ISNULL(sr.id) 
			";
		}
		$resultSet = $this->execute($query);
		$result = array();
		while($object = $resultSet->fetch(PDO::FETCH_OBJ)){
			if($object->is_available || $includeUnavailable)
				$result[] = $object;
		}
		return $result;
	}		
	
	public function getAvailabilityForTimespan($timespanId){
		$query = "
			SELECT resource AS NAME,resource_id AS id, COUNT(_overlapping) = 0 AS available 
			FROM
			(
				SELECT 
					sb.starts_at,
					sb.ends_at,
					booking_id,
					sb.description AS description, 
					r.name AS resource, 
					r.id AS resource_id,
						CASE
						WHEN sb.starts_at > t.starts_at AND sb.starts_at < t.ends_at THEN 1
						WHEN sb.ends_at > t.starts_at AND sb.ends_at < t.ends_at THEN 1
						WHEN sb.starts_at <= t.starts_at && sb.ends_at >= t.ends_at THEN 1
						WHEN t.starts_at <= sb.starts_at && t.ends_at >= sb.ends_at THEN 1
					ELSE NULL
					END AS _overlapping 
				FROM 
				resources r
				LEFT JOIN bookings b ON r.id = b.resource_id
				LEFT JOIN 
				(
					SELECT 
						starts_at, 
						ends_at, 
						booking_id, 
						'' AS description,
						'timed' AS TYPE
					FROM timed_bookings
					UNION DISTINCT 
					(
						SELECT 
						'1900-00-00 00:00:00' AS starts_at, 
						'9999-12-31 23:59:59' AS ends_at,
						booking_id,
						e.name AS description,
						'event' AS TYPE 
						FROM event_bookings eb
						LEFT JOIN EVENTS e ON e.id = eb.event_id
					)
					UNION DISTINCT 
					(
						SELECT 
							t.starts_at,t.
							ends_at, 
							eob.booking_id,
							CONCAT(e.name,',',eo.name,',',t.name) AS description,
							'event_occation' AS TYPE
							FROM event_occation_bookings eob
							LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
							LEFT JOIN timespans t ON t.id = eo.timespan_id
							LEFT JOIN EVENTS e ON e.id = eo.event_id
					)
				 ) sb
				 ON b.id = sb.booking_id
				 JOIN timespans t ON t.id = :timespanId
			) sub GROUP BY resource_id		
			ORDER BY available DESC, NAME
		";
		$parameters = array("timespanId" => $timespanId);
		$resultSet = $this->execute($query, $parameters);
		$result = array();
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$result[$row["name"]] = $row["occupied"] ? 0 : 1;
		}
		return $result;
	}

	public function getAvailableResourcesForSleepingEvent($eventId, $includeUnavailable = true){
		return $this->getAvailableResources($eventId, 0, $includeUnavailable,true);
	}

	public function getAvailableResourcesForSleepingEventOccation($eventOccationId, $includeUnavailable = true){
		return $this->getAvailableResources(0, $eventOccationId, $includeUnavailable,true);
	}
	
	public function getAvailableResourcesForEvent($eventId, $includeUnavailable = true){
		return $this->getAvailableResources($eventId, 0, $includeUnavailable);
	}

	public function getAvailableResourcesForEventOccation($eventOccationId, $includeUnavailable = true){
		return $this->getAvailableResources(0, $eventOccationId, $includeUnavailable);
	}
	
	public function getBookingsByResourceId($resourceId){
		$query = $this->getSelectAllQuery()." where b.resource_id=".$this->quote($resourceId);
		return $this->loadMany($query);
	}
	
	/**
	 * Loads the bookings for the supplied resource or all resources if no 
	 * resource is supplied
	 * @param type $resourceId
	 * @return \stdClass
	 */
	public function loadResourceSchedules($resourceId = false,$includeUnavailable=true){
		$queryString ="
			SELECT 
				r.id AS r_id, r.name AS r_name, r.available AS r_available, 
				rt.name AS rt_name, 
				rb.booking_id AS rb_id, rb.description, 
				t.starts_at AS t_starts_at, t.ends_at AS t_ends_at, t.id AS t_id,
				eo.name AS eo_name, eo.id AS eo_id,
				e.name AS e_name, e.id AS e_id
			FROM `resources` r
			LEFT JOIN (
				-- Timed bookings
				SELECT 
					b.resource_id,
                                        b.description,
					starts_at AS booking_starts_at,
					ends_at AS booking_ends_at,
					booking_id,
					NULL AS booking_occation,
					NULL AS booking_event
				FROM timed_bookings tb
				LEFT JOIN bookings b ON b.id = tb.booking_id
				UNION
				-- Event bookings
				SELECT
					b.resource_id,
                                        b.description,
					'1900-01-01 00:00:00', 
					'9999-12-31 23:59:59',
					booking_id,
					NULL,
					eb.event_id
				FROM event_bookings eb
				LEFT JOIN bookings b ON b.id = eb.booking_id
				UNION
				-- Event occation Bookings
				SELECT 
					b.resource_id,
                                        b.description,
					t.starts_at,
					t.ends_at, 
					eob.booking_id,
					eob.event_occation_id,
					eo.event_id
				FROM event_occation_bookings eob
				LEFT JOIN bookings b ON b.id = eob.booking_id
				LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
				LEFT JOIN timespans t ON t.id = eo.timespan_id
			)rb ON r.id = rb.resource_id
			LEFT JOIN `resource_types` rt ON rt.id = r.resource_type_id
			LEFT JOIN `event_occations` eo ON eo.id = rb.booking_occation
			LEFT JOIN `timespans` t ON t.id = eo.timespan_id
			LEFT JOIN `events` e ON e.id = eo.event_id
		";
		
		$param = array();
		$operator = "WHERE";
		
		if(is_numeric($resourceId)){
			$queryString .= " {$operator} r.id = :resourceId ";
			$param["resourceId"] = $resourceId;
			$operator = "AND";
		}
		
		if(!$includeUnavailable){
			$queryString .= " {$operator} r.available = 1";
		}
		
		$queryString .= "
			ORDER BY r.available desc,rt.name asc,r.name asc ,e.name asc			
		"; 
		$resultSet = $this->execute($queryString,$param);
		$resourceScheduleList = array();
		$currentResource = new stdClass();
		$currentEvent = new stdClass();
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			// new resource
			if(isset($currentResource->id) && $currentResource->id != $row["r_id"]){
				if(isset($currentEvent->id)){
					$currentResource->events[] = $currentEvent;
					$currentEvent = new stdClass();
				}
				$resourceScheduleList[] = $currentResource;
				$currentResource = new stdClass();
			}
			
			// new resource 
			if(!isset($currentResource->id)){
				$currentResource->id = $row["r_id"];
				$currentResource->name = $row["r_name"];
				$currentResource->type = $row["rt_name"];
				$currentResource->available = $row["r_available"];
				$currentResource->events = array();
			}
			// new event
			if(isset($currentEvent->id) && $currentEvent->id != $row["e_id"]){
				$currentResource->events[] = $currentEvent;
				$currentEvent = new stdClass();
			}
			// new event
			if(!isset($currentEvent->id) && ($row["e_id"])){
				if($row["e_id"]){
					$currentEvent->name = $row["e_name"];
					$currentEvent->id = $row["e_id"];
					$currentEvent->bookings = array();
				}else{
					$currentEvent->name = "Snabbokningar";
					$currentEvent->id = -1;
					$currentEvent->bookings = array();
				}
			}
			if($row["eo_id"]){
				$booking = new stdClass();
                                $booking->description = $row["description"];
				$booking->starts_at = $row["t_starts_at"];
				$booking->ends_at = $row["t_ends_at"];
				$booking->name = $row["eo_name"];
				$booking->booking_id = $row["rb_id"];
				$currentEvent->bookings[] = $booking;
			}
		}
		if(isset($currentEvent->id))
			$currentResource->events[] = $currentEvent;
		$resourceScheduleList[] = $currentResource;
		
		return $resourceScheduleList;
	}
	
	public function getBookingTreeForResource($resourceId){
		
		$query = "
			SELECT `event_id`, `event`,  `event_occation_id`,`event_occation`,`occation_starts_at`, `occation_ends_at`, timespan_name as timespan, COUNT(is_overlapping) = 0 AS ok_to_book, COUNT(is_same_occation) = 1 AS already_booked FROM
			(
				SELECT *,
					CASE
						WHEN event_occation_id = booking_occation_id THEN NULL
						WHEN occation_starts_at > booking_starts_at AND occation_starts_at < booking_ends_at THEN 1
						WHEN occation_ends_at > booking_starts_at AND occation_ends_at < booking_ends_at THEN 1
						WHEN occation_starts_at <= booking_starts_at && occation_ends_at >= booking_ends_at THEN 1
						WHEN booking_starts_at <= occation_starts_at && booking_ends_at >= occation_ends_at THEN 1
						ELSE NULL
					END 
					AS is_overlapping,
					CASE 
						WHEN event_occation_id = booking_occation_id THEN 1
						ELSE NULL
					END 
					AS is_same_occation	

				FROM 
				(
					SELECT 
						e.name AS event, e.id AS event_id,
						eo.id AS event_occation_id, 
						eo.name AS event_occation, 
						t.starts_at AS occation_starts_at,
						t.ends_at AS occation_ends_at,
						t.name AS timespan_name,
						t.id AS timespan_id
					FROM event_occations eo 
					LEFT JOIN `events` e ON e.id = eo.event_id
					LEFT JOIN `timespans` t ON t.id = eo.timespan_id
				 ) occations
				 JOIN 
				 (
					-- Timed bookings
					SELECT 
						b.resource_id,
						starts_at AS booking_starts_at,
						ends_at AS booking_ends_at,
						booking_id,
						NULL AS booking_occation_id,
						NULL AS booking_event_id
					FROM timed_bookings tb
					LEFT JOIN bookings b ON b.id = tb.booking_id
					UNION
					-- Event bookings
					SELECT
						b.resource_id,
						'1900-01-01 00:00:00', 
						'9999-12-31 23:59:59',
						booking_id,
						NULL,
						eb.event_id
					FROM event_bookings eb
					LEFT JOIN bookings b ON b.id = eb.booking_id
					UNION
					-- Event occation Bookings
					SELECT 
						b.resource_id,
						t.starts_at,
						t.ends_at, 
						eob.booking_id,
						eob.event_occation_id,
						eo.event_id
					FROM event_occation_bookings eob
					LEFT JOIN bookings b ON b.id = eob.booking_id
					LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
					LEFT JOIN timespans t ON t.id = eo.timespan_id
					UNION
					SELECT 
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL
				)
				bookings 
				WHERE bookings.resource_id= :resourceId
				OR ISNULL(bookings.resource_id)
			) event_occations
			GROUP BY event_occation_id
			ORDER BY `event`, occation_starts_at ASC
		";
				
		$param = array("resourceId" => $resourceId);
		
		$eventList = array();

		$resultSet = $this->execute($query, $param);
		
		$currentEvent = null;
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			if(!is_null($currentEvent) && $currentEvent->id != $row["event_id"]){

				$eventList[] = $currentEvent;
				$currentEvent = null;
			}
			
			
			if(is_null($currentEvent)){
				$currentEvent = new stdClass();
				$currentEvent->eventOccations =  array();
				$currentEvent->name = $row["event"];
				$currentEvent->id = $row["event_id"];
			}
			
			$eventOccation = new stdClass();
			$timespan = new Timespan();
			$timespan->starts_at = $row['occation_starts_at'];
			$timespan->ends_at = $row['occation_ends_at'];
			$eventOccation->timespan = (string)$timespan . (strlen($row["timespan"]) ? " : {$row['timespan']}" :"");
                        $eventOccation->name = $row["event_occation"];
			$eventOccation->id = $row["event_occation_id"];
			$eventOccation->ok_to_book = $row['ok_to_book'];
			$eventOccation->already_booked = $row['already_booked'];
			$currentEvent->eventOccations[] = $eventOccation;
		}
		
		$eventList[] = $currentEvent;
		
		return $eventList;
		
		
	}

	/**
	 * @param int $resourceId
	 * @return PdfResourceItem[]
	 */
	public function getPdfResourceList($resourceId=0){
		$query = "
		SELECT CONCAT(`resources`.`name`, ' (' ,`resource_types`.`name`,')') AS  `resource`, bookings.description,  resources.id as resource_id, bookings.booking_starts_at, bookings.booking_ends_at, bookings.timespan 
		FROM `resources`
		LEFT JOIN `resource_types` ON `resource_types`.`id` = `resources`.`resource_type_id`
		LEFT JOIN 
		(
			SELECT 
				b.resource_id,
				b.description,
				starts_at AS booking_starts_at,
				ends_at AS booking_ends_at,
				NULL AS timespan,
				booking_id,
				NULL AS booking_occation_id,
				NULL AS booking_event_id
			FROM timed_bookings tb
			LEFT JOIN bookings b ON b.id = tb.booking_id
			UNION
			-- Event bookings
			SELECT
				b.resource_id,
				b.description,
				'1900-01-01 00:00:00', 
				'9999-12-31 23:59:59',
				NULL,
				booking_id,
				NULL,
				eb.event_id
			FROM event_bookings eb
			LEFT JOIN bookings b ON b.id = eb.booking_id
			UNION
			-- Event occation Bookings
			SELECT 
				b.resource_id,
				CONCAT(e.name, ' ', eo.name) AS description,
				t.starts_at,
				t.ends_at, 
				t.name,
				eob.booking_id,
				eob.event_occation_id,
				eo.event_id
			FROM event_occation_bookings eob
			LEFT JOIN bookings b ON b.id = eob.booking_id
			LEFT JOIN event_occations eo ON eob.event_occation_id = eo.id
			LEFT JOIN `events` e ON e.id = eo.event_id
			LEFT JOIN timespans t ON t.id = eo.timespan_id
		) `bookings` ON `bookings`.`resource_id` = `resources`.`id`
		WHERE resources.available
		ORDER BY `resources`.`name`, booking_starts_at ASC, booking_ends_at ASC	
		";
		
		$resourceList = array();

		$resultSet = $this->execute($query);
		$currentResource = null;
		$resourceCount = 0;
		while($bookingObj = $resultSet->fetch(PDO::FETCH_OBJ)){

			if(is_null($currentResource) || $currentResource->id != $bookingObj->resource_id){
				
				$resourceList[$resourceCount] = new PdfResourceItem();
				$currentResource = &$resourceList[$resourceCount];
				$currentResource->name = $bookingObj->resource;
				$currentResource->id = $bookingObj->resource_id;
				$resourceCount++;
			}
			$booking = new PDFResourceBookingItem();
			$booking->description = $bookingObj->description;
			$booking->starts_at = $bookingObj->booking_starts_at;
			$booking->ends_at = $bookingObj->booking_ends_at;
			$booking->timespan_description = $bookingObj->timespan;
			
			$currentResource->bookings[] = $booking;
		}

		return $resourceList;
	}

}

class PdfResourceItem{
	public $name;
	public $id;
	/** @var PDFResourceBookingItem[] */
	public $bookings = array();
}

class PDFResourceBookingItem{
	public $description;
	public $starts_at;
	public $ends_at;
	public $timespan_description;
}

?>