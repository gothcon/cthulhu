<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class SleepingResourceRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "sleeping_resources";
	protected $entityClassName = "SleepingResource";
	protected $databaseTableAlias = "sr";
	
	protected function getSelectAllQuery() {
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("resources",		"r",	"id", $this->databaseTableAlias,	"resource_id",			new Resource());
		$query->addJoin("resource_types",	"rt",	"id", "r",							"resource_type_id",		new ResourceType());
		return $query->getQueryString();
	}
	
	/**
	 * @param int $resourceId
	 * @return SleepingResource
	 */
	public function loadByResourceId($resourceId){
		$query = $this->getSelectAllQuery();
		$query .= " where resource_id = ".  $this->quote($resourceId);
		return $this->loadOne($query);
	}
	
	protected function populateEntity($entity, $row, $prefix = "sr_") {
		$sleepingResource = parent::populateEntity($entity, $row, $prefix);
		$resource = parent::populateEntity(new Resource(), $row, "r_");
		$resourceType = parent::populateEntity(new ResourceType(),$row,"rt_");
		$resource->resourceType=$resourceType;
		$sleepingResource->setResource($resource);
		return $sleepingResource;
	}
	
	public function loadStatusList(){
		$query = "
			SELECT 
				sr.id, 
				sr.no_of_slots, 
				sr.is_available, 
				sr.no_of_slots - COUNT(ssb.id) AS free_slots, 
				sr.resource_id
			FROM `sleeping_resources` sr
			LEFT JOIN `sleeping_slot_bookings` ssb ON ssb.sleeping_resource_id = sr.id
			GROUP BY sr.id";
		
		$resultSet = $this->execute($query);
		
		$statusList = array();
		while($object = $resultSet->fetch(PDO::FETCH_OBJ)){
			$statusList[$object->resource_id] = $object;
		}
		return $statusList;
	}
	
	/**
	 * Not used at the moment
	 * @param int $sleeping_resource_id
	 * @return TreeItem_SleepingResource[]
	 */
	public function loadSleepingStats(){
		$query = "
			SELECT 
				COUNT(`sleeping_slot_bookings`.`id`) AS no_of_slot_bookings, 
				resources.name AS resource_name, 
				resource_types.name AS resource_type, 
				resources.id AS resource_id, 
				sleeping_resources.id AS sleeping_resource_id,
				resources.available AS is_available, 
				sleeping_resources.is_available AS is_public, 
				no_of_slots AS no_of_slots, 
				COUNT(`sleeping_slot_bookings`.`id`) AS no_of_occupied_slots, 
				no_of_slots - COUNT(`sleeping_slot_bookings`.`id`) AS no_of_free_slots 
			FROM `sleeping_resources` 
			LEFT JOIN `resources` ON `resources`.`id` = `sleeping_resources`.`resource_id`
			LEFT JOIN `resource_types` ON `resource_types`.`id` = `resources`.`resource_type_id`
			LEFT JOIN `sleeping_slot_bookings` ON `sleeping_resources`.`id` = `sleeping_slot_bookings`.`sleeping_resource_id`
			GROUP BY `sleeping_resources`.`id`
			ORDER BY resource_name
		";
		
		$resultSet = $this->execute($query);
		
		$list = $resultSet->fetchAll(PDO::FETCH_CLASS, "TreeItem_SleepingResource");
		
		return $list;
	}
	
	
	public function loadSleepingOccations($sleepingEventId,$resourceId = false){
		$resourceId = $resourceId ? $resourceId : 0;
		$query = "
			SELECT  event_occations.*, already_made_bookings.booking_id, already_made_bookings.event_occation_booking_id
			FROM 
			(
				SELECT t.starts_at, t.ends_at, t.name AS timespan, eo.name AS event_occation, eo.id AS event_occation_id, eo.event_id  FROM event_occations eo
				LEFT JOIN timespans t ON t.id = eo.timespan_id
				WHERE eo.event_id = :sleepingEventId
			) event_occations
			LEFT JOIN
			(
				SELECT b.id AS booking_id, eob.id AS event_occation_booking_id, eob.event_occation_id, b.resource_id
				FROM event_occation_bookings eob
				LEFT JOIN bookings b ON b.id = eob.booking_id
				WHERE b.resource_id = :resourceId
			) already_made_bookings 
			ON already_made_bookings.event_occation_id = event_occations.event_occation_id
			ORDER BY starts_at , ends_at
		";
		
		$params = array("sleepingEventId" => $sleepingEventId,"resourceId" => $resourceId);

		// die(print_r(array($query,$params)));
		$resultSet = $this->execute($query,$params);
		
		$occationList = array();
		while($object = $resultSet->fetch(PDO::FETCH_OBJ)){
			$occationList[] = $object;
		}
		return $occationList;
	}
	
}