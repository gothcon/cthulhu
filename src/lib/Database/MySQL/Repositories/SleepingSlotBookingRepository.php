<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class SleepingSlotBookingRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "sleeping_slot_bookings";
	protected $entityClassName = "SleepingSlotBooking";
	protected $databaseTableAlias = "ssb";

	
	public function getSelectAllQuery() {
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("people", "p", "id", $this->databaseTableAlias, "person_id", "Person");
		$query->addJoin("sleeping_resources", "sr", "id", $this->databaseTableAlias, "sleeping_resource_id", "SleepingResource");
		$query->addJoin("resources", "r", "id", "sr", "resource_id", "Resource");
		return $query->getQueryString();
	}
	
	public function loadAllBySleepingResourceId($resourceId){
		$query = $this->getSelectAllQuery();
		$query .= " where sleeping_resource_id = " . $this->quote($resourceId);
		return $this->loadMany($query);
	}
	
	/**
	 * 
	 * @param SleepingSlotBooking $sleepingSlotBooking
	 * @param array $row
	 * @param string $prefix
	 */
	protected function populateEntity($sleepingSlotBooking, $row, $prefix = "ssb_") {
		
		$sleepingSlotBooking = parent::populateEntity($sleepingSlotBooking, $row, $prefix);
		$person = parent::populateEntity(new Person(), $row,"p_");
		
		
		$resource = parent::populateEntity(new Resource, $row, "r_");
		$sleepingResource = parent::populateEntity(new SleepingResource, $row, "sr_");
		$sleepingResource->setResource($resource);
		
		$sleepingSlotBooking->setPerson($person);
		$sleepingSlotBooking->setSleepingResource($sleepingResource);
		
		
		return $sleepingSlotBooking;
	}
	
	public function loadBookingByPersonId($personId){
		/*
		$query = "
			SELECT `ssb`.`id` AS booking_id,r.`name` AS `resource`, `rt`.`name` AS `resource_type` FROM `sleeping_slot_bookings` `ssb`
			LEFT JOIN `sleeping_resources` `sr` ON `sr`.`id` = `ssb`.`sleeping_resource_id`
			LEFT JOIN `resources` `r` ON `r`.`id` = `sr`.`resource_id`
			LEFT JOIN `resource_types` `rt` ON `rt`.`id` = `r`.`resource_type_id`
			WHERE ssb.person_id = :personId
		";
		$statement = $this->execute($query,array("personId" => $personId));
		$resource = $statement->fetch(PDO::FETCH_OBJ);
		return $resource;
		 */
		$query = $this->getSelectAllQuery(). " where ssb.person_id = :personId";
		
		$booking = $this->loadOne($query, array("personId" => $personId));
		return $booking;
	}
	
	public function loadSleepingResources($sleepingEventId,$personId=0,$includeUnavailable=false,$includeFull=false){
		$query = "
			SELECT 
				sleeping_resources.sleeping_resource_id,
				sleeping_resources.resource_id,
				CONCAT(r.name,'(',rt.name,')') AS resource, 
				no_of_slots, 
				current_booking_count, 
				no_of_slots - current_booking_count AS available_slot_count,
				b.id AS booking_id,
				t.name AS timespan,
				t.starts_at,
				t.ends_at, 
				is_available,
				personal_booking_id
			FROM (
				SELECT 
					sr.id AS sleeping_resource_id ,
					sr.is_available, 
					sr.resource_id, 
					sr.no_of_slots, 
					COUNT(ssb.id) AS current_booking_count
				FROM sleeping_resources sr
				LEFT JOIN sleeping_slot_bookings ssb ON ssb.sleeping_resource_id = sr.id
				GROUP BY sr.id
			) sleeping_resources
			LEFT JOIN (
				SELECT id as personal_booking_id , sleeping_resource_id  from sleeping_slot_bookings where person_id = :personId
			) personal_bookings on personal_bookings.sleeping_resource_id = sleeping_resources.sleeping_resource_id
			LEFT JOIN resources r ON r.id  = sleeping_resources.resource_id
			LEFT JOIN resource_types rt ON rt.id = r.resource_type_id
			LEFT JOIN bookings b ON b.resource_id = r.id
			LEFT JOIN event_occation_bookings eob ON eob.booking_id = b.id
			LEFT JOIN event_occations eo ON eo.id = eob.event_occation_id 
			LEFT JOIN timespans t ON t.id = eo.timespan_id
			WHERE eo.event_id = :sleepingEventId
		";
		$operation = "AND";
		if(!$includeFull){
			$query .= $operation." (no_of_slots > current_booking_count OR personal_booking_id > 0)\n";
			$operation = "AND";
		}
		if(!$includeUnavailable){
			$query .= $operation." is_available";
		}
		
		$query .=" ORDER BY r.id, t.starts_at";
		$statement = $this->execute($query,array("personId" =>  $personId, "sleepingEventId" => $sleepingEventId));
		$resourceList = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		$count = -1;
		$resultList = array();
		$currentResource = false;
		foreach($resourceList as $resource){
			if(!$currentResource || $resource["sleeping_resource_id"] != $currentResource->sleeping_resource_id  && $count > -1){
				$resultList[++$count] = new stdClass;
				$currentResource = &$resultList[$count];
				$currentResource->resource = $resource["resource"];
				$currentResource->sleeping_resource_id = $resource["sleeping_resource_id"];
				$currentResource->is_availale = $resource["is_available"];
				$currentResource->no_of_free_slots = $resource["available_slot_count"];
				$currentResource->is_personal_booking = $resource["personal_booking_id"] > 0;
				$currentResource->personal_booking_id = $resource["personal_booking_id"];
				$currentResource->eventOccations = array();
			}
			$eventOccation = new stdClass();
			$eventOccation->name = $resource["timespan"];
			$eventOccation->starts_at = $resource["starts_at"];
			$eventOccation->ends_at = $resource["ends_at"];
			$currentResource->eventOccations[] = $eventOccation;
		}
		return $resultList;
	}

	public function loadUnbookedPeople(){
		$query = "
				select p.first_name, p.last_name, p.identification, p.id
				from people p 
				left join sleeping_slot_bookings ssb on ssb.person_id = p.id
				where isnull(ssb.id)
				order by last_name, first_name
		";
		$statement = $this->execute($query);
		return $statement->fetchAll(PDO::FETCH_OBJ);
	}
	
	
	public function loadListForPdf($resourceId = 0){
		$query = "
		SELECT 
			`resources`.`id` AS `resource_id`,
			CONCAT(`resources`.`name` ,' (',`resource_types`.`name`,') ') AS resource,
			`sleeping_resources`.`no_of_slots`,
			CONCAT(`people`.`id`,'. ',`people`.`first_name`,' ',`people`.`last_name`, IF(ISNULL(`people`.`identification`),'',CONCAT(' (',SUBSTR(SUBSTRING_INDEX(`people`.`identification`,' ',1),1,6),')'))) AS person,
			`people`.`id` AS person_id
		FROM `resources` 
		LEFT JOIN `resource_types` ON `resource_types`.`id` = `resources`.`resource_type_id`
		LEFT JOIN `sleeping_resources` ON `sleeping_resources`.`resource_id` = `resources`.`id`
		LEFT JOIN `sleeping_slot_bookings` ON `sleeping_slot_bookings`.`sleeping_resource_id` = `sleeping_resources`.`id`
		LEFT JOIN `people` ON `people`.`id` = `sleeping_slot_bookings`.`person_id`
		ORDER BY resource
		";
		
		$resourceList = array();

		$resultSet = $this->execute($query);
		$currentResource = null;
		$resourceCount = 0;
		while($resourceObj = $resultSet->fetch(PDO::FETCH_OBJ)){
			if(is_null($currentResource) || $currentResource->resource_id != $resourceObj->resource_id){
				$resourceList[$resourceCount] = new PdfSleepingResource();
				$currentResource = &$resourceList[$resourceCount];
				$currentResource->name = $resourceObj->resource;
				$currentResource->resource_id = $resourceObj->resource_id;
				$currentResource->no_of_slots = $resourceObj->no_of_slots;
				$resourceCount++;
			}
			$currentResource->sleepers[] = $resourceObj->person;
		}
		return $resourceList;
	}
	
	public function loadBookings($sleeping_resource_id = 0){
		
	}
	
	
}

class PdfSleepingResource{
	public $name;
	public $resource_id;
	public $no_of_slots;
	public $sleepers = array();
}
