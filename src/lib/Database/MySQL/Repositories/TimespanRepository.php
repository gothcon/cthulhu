<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
class TimespanRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "timespans";
	protected $entityClassName = "Timespan";
	protected $databaseTableAlias = "t";

	public function load($id = false) {
		if($id === false){
			return $this->loadMany($this->getSelectAllQuery()." order by starts_at, ends_at desc");
		}else{
			return parent::load($id);
		}
	}
	
	public function loadPublic(){
		$query = new MySQLQuery("timespans","t",new Timespan());
		$queryString = "{$query->getQueryString()} where is_public  = 1  order by starts_at, ends_at desc";
		return $this->loadMany($queryString,array(),"PopulateEntity");
	}
	
	public function loadVisibleInSchedule(){
		$query = new MySQLQuery("timespans","t",new Timespan());
		$queryString = "{$query->getQueryString()} where show_in_schedule  = 1  order by starts_at, ends_at desc";
		return $this->loadMany($queryString,array(),"PopulateEntity");
	}
	
}