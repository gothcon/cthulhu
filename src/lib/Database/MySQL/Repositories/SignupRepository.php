<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
class SignupRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "signups";
	protected $entityClassName = "Signup";
	protected $databaseTableAlias = "s";
	
	function getSelectAllQuery(){
	
		$query = new MySQLQuery("event_occations","eo",	"EventOccation");
		$query->addJoin("events",		"e",	"id",	"eo",	"event_id",		new Event());
		$query->addJoin("timespans",	"t",	"id",	"eo",	"timespan_id",	new Timespan());
		$query->addJoin("signup_slots", "ss", "event_occation_id", "eo", "id", new SignupSlot());
		$query->addJoin("signups", "s", "signup_slot_id", "ss", "id", new Signup());
		$query->addJoin("slot_types", "st","id","ss","slot_type_id", new SlotType());
		$query->addJoin("people", "p", "id", "s", "person_id", new Person());
		$query->addJoin("groups", "g", "id", "s", "group_id", new Group());
		$query->addJoin("signup_slot_articles", "ssa", "signup_slot_id", "ss", "id", new SignupSlotArticle());
		
		return $query->getQueryString();
		
	}
	
	public function PopulateEntity($signup,$row,$prefix="s_"){
		
		$signup = parent::PopulateEntity($signup,$row,$prefix);
		if(!$signup->id)
			return false;

		$event = parent::PopulateEntity(new Event(),$row,"e_");
		
		$person = parent::PopulateEntity( new Person(),$row,"p_");
		

		$group = parent::PopulateEntity(new Group(),$row,"g_");


		$signupslot = parent::PopulateEntity(new SignupSlot(),$row,"ss_");

		$slottype = parent::PopulateEntity(new SlotType(),$row,"st_");
		$signupslot->setSlotType($slottype);
		
		$timespan = parent::PopulateEntity(new Timespan(),$row,"t_");


		$eventOccation = parent::PopulateEntity(new EventOccation(),$row,"eo_");
		$eventOccation->setEvent($event);
		$eventOccation->setTimespan($timespan);

		$signupslot->event_occation = $eventOccation;
		
		$signupSlotArticle = parent::PopulateEntity(new SignupSlotArticle(),$row,"ssa_");
		
		$signupslot->setSignupSlotArticle($signupSlotArticle);
		
		
		$signup->setPerson($person);
		$signup->setGroup($group);
		$signup->setSignupSlot($signupslot);
		
		return $signup;
	}
	
	public function loadAllFilteredByPerson($person){
		if(is_numeric($person) && $person > 0){
			$id = $person;
		}else if(is_object($person)){
			$id = $person->id;
		}
		$queryString = "
		SELECT 
			e.name AS event_name, 
			e.id as event_id,
			eo.name AS event_type_name, 
			eo.id AS event_occation_id,
			t.name AS timespan_name,
			t.id as timespan_id,
			t.starts_at, 
			t.ends_at,
			ss.maximum_signup_count, 
			ss.id as signup_slot_id, 
			ss.maximum_spare_signup_count,
			ssa.article_id,
			ss.requires_approval,
			st.name as slot_type_name,
			g.name AS group_name,
			s.signup_id as id, 
			s.group_id, 
			s.person_id,
			s.is_paid,
			s.is_approved,
			s.is_signup_owner,
			s.signup_order_number FROM (
				SELECT 
					personalSignup.id AS signup_id,
					personalSignup.signup_slot_id,
					personalSignup.is_paid,
					personalSignup.is_approved,
					personalSignup.person_id,
					NULL AS group_id,
					1 AS is_signup_owner,
					COUNT(allSignups.id) AS signup_order_number
				FROM signups personalSignup
				JOIN signups allSignups
				WHERE personalSignup.signup_slot_id = allSignups.signup_slot_id
				AND personalSignup.id >= allSignups.id
				AND personalSignup.person_id = ".$this->quote($id)."
				GROUP BY signup_slot_id
                                UNION
				SELECT 		
					groupSignups.signup_id,
					groupSignups.signup_slot_id,
					groupSignups.is_paid,
					groupSignups.is_approved,
					NULL AS person_id,
					groupSignups.group_id,
					groupSignups.is_signup_owner, 
					COUNT(allSignups.id) AS signup_order_number 
				FROM(
					SELECT 
						s.id AS signup_id, 
						s.signup_slot_id, 
						s.is_paid, 
						s.is_approved, 
						s.group_id AS group_id,
						gm.id = g.leader_membership_id AS is_signup_owner 
					FROM group_memberships gm 
					INNER JOIN groups g ON g.id = gm.group_id
					INNER JOIN signups s ON s.group_id = g.id
					WHERE gm.person_id = ".$this->quote($id)."
				) groupSignups
				JOIN signups allSignups
				WHERE groupSignups.signup_slot_id = allSignups.signup_slot_id
				AND groupSignups.signup_id >= allSignups.id
				GROUP BY groupSignups.group_id, signup_slot_id
		) s
		LEFT JOIN signup_slot_articles ssa ON ssa.signup_slot_id = s.signup_slot_id
		LEFT JOIN signup_slots ss ON s.signup_slot_id = ss.id
		LEFT JOIN slot_types st on st.id = ss.slot_type_id
		LEFT JOIN event_occations eo ON eo.id = ss.event_occation_id
		LEFT JOIN timespans t ON t.id = eo.timespan_id
		LEFT JOIN events e ON e.id = eo.event_id
		LEFT JOIN groups g ON g.id = s.group_id 
		";

		$resultSet = $this->execute($queryString);
		
		$returnArray = array();
		
		while($item = $resultSet->fetch(PDO::FETCH_OBJ)){
			$returnArray[] = $item;
		}
		
		return $returnArray;
		
	}
	
	public function loadAllFilteredByGroup($group){
		if(is_numeric($group) && $group > 0){
			$id = $group;
		}else if(is_object($group)){
			$id = $group->id;
		}
		
		$query = $this->getSelectAllQuery().
		"
			where g.id = :id
			order by t.starts_at, t.ends_at
		";

		return $this->loadMany($query,array("id" => $id),"PopulateEntity");	
	}
	
	public function loadByEvent($event){
		$id_parameters = -1;
		if(is_numeric($event) && $event > 0){
			$id_parameters = array("id" => $event);
		}else if(is_array($event)){
			$id_parameters = $event;
		}else if(is_object($event)){
			$id_parameters = array($event->id);
		}
		if(count($id_parameters) == 0)
			die("invalid ids");
			
		$where = "event_id in(";
		$first = true;
		foreach($id_parameters as $key => $id){
			if($first)
				$first = false;
			else
				$where .= ",";
			$where .= ":{$key}";
		}
		$where .= ")";
		$query = "{$this->getSelectAllQuery()} where {$where}";
		
		return $this->loadMany($query,$id_parameters,"PopulateEntity");
	}

	public function loadAllFilteredBySlot($slot){
		$id_parameters = -1;
		if(is_numeric($slot) && $slot > 0){
			$id_parameters = array("id" => $slot);
		}else if(is_array($slot)){
			$id_parameters = $slot;
		}else if(is_object($slot)){
			$id_parameters = array("id" => $slot->id);
		}
		
		if(count($id_parameters) == 0)
			die("invalid ids");
		$where = "where ss.id = :id";
		$query = "{$this->getSelectAllQuery()} {$where}";

		return $this->loadMany($query,$id_parameters,"PopulateEntity");
	}
	
  	
	/**
	 * This function selects all signups together with relevant slot data. This contains all data necessary to 
	 * determine who is going to be on top...
	 * @return array
	 */
	public function loadAllGroupedBySignupSlotId(){
		$query = "
			SELECT 
				united_signups.signee_id, 
				united_signups.signee, 
				united_signups.signee_type, 
				`signups`.`id` AS signup_id,
				CASE WHEN `signup_slots`.`requires_approval` THEN `signups`.`is_approved` ELSE -1 END AS `signup_is_approved`,
				CASE WHEN `signup_slot_articles`.`id` THEN `signups`.`is_paid` ELSE -1 END AS `signup_is_paid`,
				`signups`.`signup_slot_id`,
				`signup_slots`.`maximum_signup_count`,
				`signup_slots`.`maximum_spare_signup_count`,
				`signup_slots`.`event_occation_id`
			FROM (
				SELECT 
					CONCAT(`people`.`first_name`, ' ',`people`.`last_name`) AS signee,
					`people`.`id` AS signee_id,
					'person' AS signee_type,
					`signups`.`id` 
				FROM `signups`
				INNER JOIN `people` ON `people`.`id` = `signups`.`person_id`
				UNION
				SELECT 
					`groups`.`name` AS signee,
					`groups`.`id` AS signee_id,
					'group' AS signee_type,
					`signups`.`id` 
				FROM `signups`
				INNER JOIN `groups` ON `groups`.`id` = `signups`.`group_id`

			) united_signups
			LEFT JOIN `signups` ON `signups`.`id` = united_signups.id
			LEFT JOIN `signup_slots` ON `signup_slots`.`id`=`signups`.`signup_slot_id`
			LEFT JOIN `signup_slot_articles` ON `signup_slot_articles`.`signup_slot_id` = `signup_slots`.`id`
			ORDER BY signup_slot_id, signup_id ASC
		";
		
		$statement = $this->execute($query);
		
		$result = array();
		
		while($signup = $statement->fetchObject()){
			if(!isset($result[$signup->signup_slot_id])){
				$result[$signup->signup_slot_id] = array();
			}
			$result[$signup->signup_slot_id][] = $signup;
		}
		
		return $result;
	}
}