<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_OrderRow.php");

class OrderRowRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "order_rows";
	protected $entityClassName = "OrderRow";
	protected $databaseTableAlias = "orws";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		$query->addJoin("articles", "a","id", $this->databaseTableAlias, "article_id", new Article());
		return $query->getQueryString();
	}
	
	public function loadArticleOrderingStatistics($articleId){
		return $this->_loadArticlesOrderingStatistics($articleId);
	}
	
	public function loadArticlesOrderingStatistics($articleId = 0){
		return $this->_loadArticlesOrderingStatistics($articleId);
	}
	
	private function _loadArticlesOrderingStatistics($articleId=0){
		$params = array();
		$query = "
			SELECT 
				order_rows.article_id,
				order_rows.name,
				SUM(CASE 
					WHEN orders.status = 'cancelled' THEN 0
					WHEN orders.status + 0 > 1 THEN order_rows.count
					ELSE 0 
				END) AS ordered,		
				SUM(CASE 
					WHEN orders.status = 'cancelled' THEN 0
					WHEN orders.status + 0 > 2 THEN order_rows.count
					ELSE 0 
				END) AS paid,
				SUM( IF(order_rows.status +0 = 1 ,order_rows.count,0) ) AS delivered
			FROM articles
			left join order_rows on order_rows.article_id = articles.id
			LEFT JOIN orders ON order_rows.order_id = orders.id
					
		";
		if($articleId){
			$query = "{$query} WHERE articles.id = :article_id";
			$params["article_id"] = $articleId;
		}
		$query = "{$query} GROUP BY order_rows.article_id";
		
		$resultSet = $this->execute($query,$params);
		$article_status = array();
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$statusObj = new stdClass();
			$statusObj->name		= $row["name"];
			$statusObj->ordered		= $row["ordered"];
			$statusObj->paid		= $row["paid"];
			$statusObj->delivered	= $row["delivered"];
			$statusObj->article_id	= $row["article_id"];
			$article_status[] = $statusObj;
		}
		
		if($articleId)
			return isset($article_status[0]) ? $article_status[0] : false;
		else {
			return $article_status;
		}
	}
	
	public function loadByArticle($articleId){
		$query = "
			SELECT 
				p.first_name, p.last_name, p.id,
				orws.count, orws.note, orws.status,
				o.status as order_status,orws.order_id, o.note as order_note
			FROM order_rows orws
			LEFT JOIN orders o ON o.id = orws.order_id
			LEFT JOIN people p ON p.id = o.person_id where orws.article_id = :articleId";
		
		$resultSet = $this->execute($query,array("articleId" => $articleId));
		
		$rows = array();
		 
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$rowObj = new stdClass();
			$rowObj->customer_first_name	= $row["first_name"];
			$rowObj->customer_last_name		= $row["last_name"];
			$rowObj->customer_id			= $row["id"];
			$rowObj->article_count			= $row["count"];
			$rowObj->article_note			= $row["note"];
			$rowObj->article_delivery_status= $row["status"];
			$rowObj->article_order_status	= $row["order_status"];
			$rowObj->article_order_id		= $row["order_id"];
			$rowObj->article_order_note		= $row["order_note"];
			$rows[] = $rowObj;
		}
		
		return $rows;
		
	}
	
	public function loadByOrder($orderId){
		return $this->loadMany("{$this->getSelectAllQuery()} where order_id = :orderId", array("orderId" => $orderId),"PopulateEntity");
	}
	
	public function loadOrderRowsStatistics($articleId = 0){
		
		$query = "
			SELECT 
				CASE
					WHEN (orders.note != '' AND order_rows.note != '') THEN CONCAT(order_rows.note , '. Order: ' , orders.note)
					WHEN orders.note != '' THEN CONCAT('Order: ',orders.note)
					WHEN order_rows.note != '' THEN order_rows.note
					ELSE ''
				END AS note,
				order_rows.article_id, 
				order_rows.count AS article_count,
				orders.status = 'paid' AS is_paid,
				CONCAT(people.first_name,' ', people.last_name) AS customer,
				people.email_address AS customer_email,
				people.phone_number AS customer_phone,
				orders.id as order_id,
				people.id as customer_id,
				(order_rows.status + 0)  = 1 as is_delivered
				
			FROM order_rows
			INNER JOIN orders ON orders.id = order_rows.order_id AND orders.status != 'cancelled' AND orders.status != 'cart'
			INNER JOIN articles ON articles.id = order_rows.article_id
			LEFT JOIN people ON people.id = orders.person_id ".( $articleId > 0 ?"
			WHERE articles.id = ".$this->quote($articleId)."
			": "
			")." ORDER BY article_id			
		";
		
		$resultSet = $this->execute($query);
		$orderRows = $resultSet->fetchAll(PDO::FETCH_CLASS, "TreeItem_OrderRow");
		return $orderRows;
	}
	
	public function PopulateEntity($orderRow,$row,$prefix="orws_"){
	
		$orderRow = parent::PopulateEntity($orderRow,$row,$prefix);
		$article = new Article();
		$article = parent::PopulateEntity($article, $row,"a_");
		$orderRow->article = $article;
		return $orderRow;
	}
}