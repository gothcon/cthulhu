<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class SignupSlotRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "signup_slots";
	protected $entityClassName = "SignupSlot";
	protected $databaseTableAlias = "ss";
	
	function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName, $this->databaseTableAlias, $this->entityClassName);
		$query->addJoin("signup_slot_articles", "ssa", "signup_slot_id", $this->databaseTableAlias, "id", "SignupSlotArticle");
		$query->addJoin("articles", "a", "id", "ssa", "article_id", "Article");
		$query->addJoin("slot_types",		"st",	"id",	$this->databaseTableAlias,	"slot_type_id",		"SlotType");
		$query->addJoin("event_occations",	"eo",	"id",	$this->databaseTableAlias,	"event_occation_id","EventOccation");
		$query->addJoin("events", "e", "id", "eo", "event_id", "Event");
		$query->addJoin("event_types", "et", "id", "e", "eventtype_id", "EventType");
		$query->addJoin("timespans", "t", "id", "eo", "timespan_id", "Timespan");
		return $query->getQueryString();
	}
	
	public function PopulateEntity($signupSlot,$row,$prefix="ss_"){

		$signupSlot = parent::PopulateEntity($signupSlot,$row,$prefix);
		
		
		$signupSlotArticle	= ($row["ssa_id"])	? parent::PopulateEntity(new SignupSlotArticle(),	$row, "ssa_")	: new SignupSlotArticle() ;
		$article			= ($row["a_id"])	? parent::PopulateEntity(new Article(),				$row, "a_")		: new Article();
		$slotType			= ($row["st_id"])	? parent::PopulateEntity(new SlotType(),			$row, "st_")	: new SlotType();
		$eventOccation		= ($row["eo_id"])	? parent::PopulateEntity(new EventOccation(),		$row, "eo_")	: new EventOccation();
		$event				= ($row["e_id"])	? parent::PopulateEntity(new Event(),				$row, "e_")		: new Event();
		$eventtype			= ($row["et_id"])	? parent::PopulateEntity(new EventType(),			$row, "et_")	: new EventType();
		$timespan			= ($row["t_id"])	? parent::PopulateEntity(new Timespan(),			$row, "t_")		: new Timespan();
		
		$event->eventtype = $eventtype;
		$eventOccation->event = $event;
		$eventOccation->timespan = $timespan;
		$signupSlot->slot_type = $slotType;
		$signupSlot->event_occation = $eventOccation;
		
		$signupSlotArticle->setSignupSlot($signupSlot);
		$signupSlotArticle->setArticle($article);
		$signupSlot->setSignupSlotArticle($signupSlotArticle);
		
		return $signupSlot;
	}

	public function loadAll(){
		return $this->load();
	}
	
	public function loadByEvent($eventOrId){
		if(is_numeric($eventOrId)){
			$id_parameters = array("id" => $eventOrId);
		}else if(is_array($eventOrId)){
			$id_parameters = $eventOrId;
		}else if(is_object($eventOrId)){
			$id_parameters = array($eventOrId->id);
		}
		if(count($id_parameters) == 0)
			die("invalid ids");
			
		$where = "event_id in(";
		$first = true;
		foreach($id_parameters as $key => $id){
			if($first)
				$first = false;
			else
				$where .= ",";
			$where .= ":{$key}";
		}
		$where .= ")";
		$query = "{$this->getSelectAllQuery()} where {$where}";
		
		return $this->loadMany($query,$id_parameters,"PopulateEntity");
	}
	
	public function loadByEventType($eventType){
		if(is_numeric($eventType)){
			$id_parameters = array("id" => $eventType);
		}else if(is_array($eventType)){
			$id_parameters = $eventType;
		}else if(is_object($eventType)){
			$id_parameters = array($eventType->id);
		}
		if(count($id_parameters) == 0)
			die("invalid ids");
			
		$where = "e.eventtype_id in(";
		$first = true;
		foreach($id_parameters as $key => $id){
			if($first)
				$first = false;
			else
				$where .= ",";
			$where .= ":{$key}";
		}
		$where .= ")";
		$query = "{$this->getSelectAllQuery()} where {$where}";
		
		return $this->loadMany($query,$id_parameters,"PopulateEntity");
	}
	
	/**
	 * @param type $occation
	 * @return SignupSlot[]
	 */
	public function loadAllFilteredByOccation($occation){
		
		if(is_numeric($occation) && $occation > 0){
			$parameters = array("id" => $occation);
		}else if(is_object($occation)){
			$parameters = array("id" => $occation->id);
			
		}
		$where = "ss.event_occation_id =:id";
		$query = "{$this->getSelectAllQuery()} where {$where}";
		return $this->loadMany($query,$parameters,"PopulateEntity");
	}
	
	/**
	 * Returns an array of signups slots grouped by event occation id<br/>
	 * (array[eventOccationId] = TreeItem_SignupSlot[])
	 * @param int $slotId
	 * @param int $eventOccationId
	 * @param int $eventId
	 * @param int $eventTypeId
	 * @return array
	 */
	public function loadSignupStatisticsTree($slotId = 0, $eventOccationId = 0, $eventId = 0, $eventTypeId = 0,$cardinality = "group"){
		
		$query = "
			SELECT 
				`signup_slots`.`id`,
				`signup_slots`.`event_occation_id`,
				`signup_slots`.`maximum_signup_count` as max_signup_count,
				`signup_slots`.`maximum_spare_signup_count` as max_spare_signup_count,
				`signup_slots`.`requires_approval`,
				`articles`.`price` AS cost,
				`slot_types`.`name` as slot_type,
				`slot_types`.`id` as slot_type_id,
				`slot_types`.`cardinality` as slot_type_cardinality,
				COUNT(signups.id) AS current_signup_count
			FROM `signup_slots`";
		
		switch ($cardinality) {
			case "person":
				$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` AND slot_types.cardinality = 1";
				break;
				break;
			 case "group":
				$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` AND slot_types.cardinality > 1";
				break;
			default:
			$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` ";
				break;
		}
		
		$query .="
			LEFT JOIN `signups` ON `signups`.`signup_slot_id` = `signup_slots`.`id` 
			INNER JOIN event_occations ON event_occations.id = signup_slots.event_occation_id ". 
				($eventOccationId > 0 ? "AND `event_occations`.`id` = " . $this->quote($eventOccationId): "")."
			INNER JOIN `events` ON `events`.`id` = `event_occations`.`event_id`". 
				($eventId > 0 ? " AND `events`.`id` = " . $this->quote($eventId): "").
				($eventTypeId > 0 ? " AND `events`.`eventtype_id` = " . $this->quote($eventTypeId): "")."
			LEFT JOIN `signup_slot_articles` ON `signup_slot_articles`.`signup_slot_id` = `signup_slots`.`id`
			LEFT JOIN `articles` ON `articles`.`id` = `signup_slot_articles`.`article_id`
			". ($slotId > 0 ? "WHERE `signup_slots`.`id` = " .$this->quote($slotId) : "" )."
			GROUP BY `signup_slots`.`id`
		";
		
		$resultSet = $this->execute($query);
		
		$signupSlotTree = array();
				
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$eventOccationId = $row["event_occation_id"]; 
			if(!isset($signupSlotTree[$eventOccationId])){
				$signupSlotTree[$eventOccationId] = array();
			}
			
			$signupSlot = new TreeItem_SignupSlot;
			$signupSlot->id = $row["id"];
			$signupSlot->max_signup_count = $row["max_signup_count"];
			$signupSlot->max_spare_signup_count = $row["max_spare_signup_count"];
			$signupSlot->requires_approval = $row["requires_approval"];
			$signupSlot->current_signup_count = $row["current_signup_count"];
			
			if(is_null($signupSlot->max_signup_count) || $signupSlot->max_signup_count === ""){
				// no signup limit
				$signupSlot->slot_status = "slotAvailable";
			}else if( $signupSlot->max_signup_count > $signupSlot->current_signup_count ){
				// signup limit not reached
				$signupSlot->slot_status = "slotAvailable";
			}
			else{
				// all slots are filled
				if(is_null($signupSlot->max_spare_signup_count) || $signupSlot->max_spare_signup_count === ""){
					// no spare signup limit
					$signupSlot->slot_status = "spareSlotAvailable";
				}else if(($signupSlot->max_signup_count + $signupSlot->max_spare_signup_count) > $signupSlot->current_signup_count ){
					// spare signup limit not reached
					$signupSlot->slot_status = "spareSlotAvailable";
				}else{
					// no spare slots
					$signupSlot->slot_status = "notAvailable";
				}
			}
			
			$signupSlot->cost = $row["cost"];
			$signupSlot->slot_type_id = $row["slot_type_id"];
			$signupSlot->slot_type_name = $row["slot_type"];
			$signupSlot->slot_type_cardinality = $row["slot_type_cardinality"];
			
			$signupSlotTree[$eventOccationId][$signupSlot->id] = $signupSlot;
			
		}
		
		return $signupSlotTree;
	}
	
	/**
	 * 
	 * @param int $slotId
	 * @param int $eventOccationId
	 * @param int $eventId
	 * @param int $eventTypeId
	 * @return array[]
	 */
	public function loadSignupSlotTree($slotId = 0, $eventOccationId = 0, $eventId = 0, $eventTypeId = 0,$cardinality = "both"){
		/** TODO fix cardinality filtering **/
		$query = "
			SELECT 
				`signups`.`signup_slot_id`,
				`signups`.`id` AS signup_id,
				`signups`.`is_paid`,
				`signups`.`is_approved`,
				CONCAT(`people`.`first_name`,' ',`people`.`last_name`) AS person,
				`signups`.`person_id` AS person_id,
				`groups`.`name` AS `group`,
				`signups`.`group_id` AS group_id,
				`event_occations`.`id` as event_occation_id
			FROM `signups`
			LEFT JOIN `signup_slots` ON `signup_slots`.`id` = `signups`.`signup_slot_id`";
		switch ($cardinality) {
			case "person":
				$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` AND slot_types.cardinality = 1";
				break;
				break;
			 case "group":
				$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` AND slot_types.cardinality > 1";
				break;
			default:
			$query .="
				INNER JOIN `slot_types` ON `signup_slots`.`slot_type_id` = `slot_types`.`id` ";
				break;
		}
		$query .="
			INNER JOIN `event_occations` ON `event_occations`.`id` = `signup_slots`.`event_occation_id`". 
				($eventOccationId > 0 ? "AND `event_occations`.`id` = " . $this->quote($eventOccationId): "")." 
			INNER JOIN `events` ON `events`.`id` = `event_occations`.`event_id`". 
				($eventId > 0 ? " AND `events`.`id` = " . $this->quote($eventId): "").
				($eventTypeId > 0 ? " AND `events`.`eventtype_id` = " . $this->quote($eventTypeId): "")."
			LEFT JOIN  `people` ON `people`.id = `signups`.`person_id`
			LEFT JOIN  `groups` ON `groups`.id = `signups`.`group_id`
			". ($slotId > 0 ? "WHERE `signup_slots`.`id` = " .$this->quote($slotId) : "" )."
			ORDER BY signup_slot_id, signup_id
		";
		
		$resultSet = $this->execute($query);
		
		$tree = $this->loadSignupStatisticsTree($slotId, $eventOccationId, $eventId, $eventTypeId, $cardinality);
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$occationId = $row['event_occation_id'];
			$slotId = $row['signup_slot_id'];
			if(!isset($tree[$occationId][$slotId]))
				continue;
			$signup = new TreeItem_Signup();
			$signup->id = $row["signup_id"];
			$signup->is_approved = $row["is_approved"];
			$signup->is_paid = $row["is_paid"];
			$signup->signee_name = ($row["person_id"] > 0) ? $row["person"] : $row["group"];
			$signup->signee_id = ($row["person_id"] > 0) ? $row["person_id"] : $row["group_id"];
			$signup->signee_type = ($row["person_id"] > 0) ? "person" : "group";
			$tree[$occationId][$slotId]->signups[] = $signup;
		}
		return $tree;
	}
	
}
