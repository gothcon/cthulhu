<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImageRepository
 *
 * @author Joakim
 */
class ImageRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "images";
	protected $entityClassName = "Image";
	protected $databaseTableAlias = "i";
	
	public function loadByUrl($url){
		// IMPROVMENT, create additional column in database with full path and use as index
		return $this->loadOne($this->getSelectAllQuery()." where concat(path,i.name) = :url",array("url" => $url));
	}
	
	public function loadByIdWithoutContent($id){
		$temp = new Image();
		$properties = $temp->getPersistedProperties();
		unset($properties["content"]);
		$propertyNames = array_keys($properties);
		$query = new MySQLQuery($this->databaseTableName, $this->databaseTableAlias, $propertyNames);
		return $this->loadOne($query->getQueryString()." where id = :id",array("id" => $id));
	}
	
	public function load($id = null){
		if(is_null($id)){
			$temp = new Image();
			$properties = $temp->getPersistedProperties();
			unset($properties["content"]);
			$properties = array_keys($properties);
			$query = new MySQLQuery($this->databaseTableName, $this->databaseTableAlias, $properties);
			return $this->loadMany($query->getQueryString());
		}else{
			return parent::load($id);
		}
	}
	
	public function loadContentById($id){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,array("content"));
		$resultSet = $this->execute($query. "where id=:id",array("id" => $id));
		$content = $resultSet->fetchColumn();
		return $content;
	}
	
	/**
	 * 
	 * @param Image $entity
	 */
	public function save(&$entity) {
		// if the image data is not set, do not save it, since we only need it in certain cases.
		$persistedProperties = $entity->getPersistedProperties();
		if(strlen($persistedProperties["content"]) == 0){
			unset($persistedProperties["content"]);
			$entity->setPersistedProperties($persistedProperties);
		}
		$result = parent::save($entity);
		$persistedProperties = $entity->getPersistedProperties();
		$persistedProperties["content"] = "";
		$entity->setPersistedProperties($persistedProperties);
		return $result;
	}
	
	
}

?>
