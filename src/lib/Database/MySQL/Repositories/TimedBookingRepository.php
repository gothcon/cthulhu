<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
class TimedBookingRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "timed_bookings";
	protected $entityClassName = "TimedBooking";
	protected $databaseTableAlias = "tb";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,new $this->entityClassName());
		$query->addJoin("bookings",			"b",	"id",	$this->databaseTableAlias, "booking_id", new Booking());
		return $query->getQueryString();
	}
	
	protected function populateEntity($entity, $row, $prefix = "tb_") {
		$timedBooking = parent::populateEntity($entity, $row, $prefix);
		$booking = parent::populateEntity(new Booking, $row,"b_");
		$timedBooking->setBooking($booking);
		return $timedBooking;
	}
	
}


?>
