<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
class EventBookingRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "event_bookings";
	protected $entityClassName = "EventBooking";
	protected $databaseTableAlias = "tb";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,new $this->entityClassName());
		$query->addJoin("bookings",		"b",	"id",	$this->databaseTableAlias, "booking_id", new Booking());
		$query->addJoin("events",		"e",	"id",	$this->databaseTableAlias, "event_id", new Event());
		$query->addJoin("event_types",	"et",	"id",	"e", "eventtype_id", new EventType());
		return $query->getQueryString();
	}
	
	protected function populateEntity($entity, $row, $prefix = "tb_") {
		$eventBooking = parent::populateEntity($entity, $row, $prefix);
		$booking = parent::populateEntity(new Booking, $row,"b_");
		$event = parent::populateEntity(new Event, $row, "e_");
		$eventType = parent::populateEntity(new EventType, $row, "et_");
		$event->setEventType($eventType);
		$eventBooking->setEvent($event);
		$eventBooking->setBooking($booking);
		return $eventBooking;
	}
	
	/**
	 * @param int $id
	 * @return EventBooking[]
	 */
	public function loadByEventId($id){
		return $this->loadMany($this->getSelectAllQuery()." where event_id = :eventId", array("eventId" => $id));
	}
	
	/**
	 * @param int $id
	 * @return EventBooking
	 */
	public function loadByBookingId($id){
		return $this->loadOne($this->getSelectAllQuery()." where booking_id = :bookingId", array("bookingId" => $id));
	}
	
}


?>
