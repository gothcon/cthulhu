<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../../TreeItems/TreeItem_EventOccationBooking.php");
class EventOccationBookingRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "event_occation_bookings";
	protected $entityClassName = "EventOccationBooking";
	protected $databaseTableAlias = "eob";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,new $this->entityClassName());
		$query->addJoin("bookings",			"b",	"id",	$this->databaseTableAlias,	"booking_id",		new Booking());
		$query->addJoin("event_occations",	"eo",	"id",	$this->databaseTableAlias,	"event_occation_id",new EventOccation());
		$query->addJoin("events",			"e",	"id",	"eo",						"event_id",			new Event());
		$query->addJoin("timespans",		"t",	"id",	"eo",						"timespan_id",		new Timespan());
		$query->addJoin("event_types",		"et",	"id",	"e",						"eventtype_id",		new EventType());
		$query->addJoin("resources",		"r",	"id",	"b",						"resource_id",		new Resource());
		$query->addJoin("resource_types",	"rt",	"id",	"r",						"resource_type_id",	new ResourceType());
		return $query->getQueryString();
	}
	
	protected function populateEntity($entity, $row, $prefix = "b_") {
		$booking = parent::populateEntity($entity, $row, $prefix);
		$resource = parent::populateEntity(new Resource, $row, "r_");
		$resourceType = parent::populateEntity(new ResourceType, $row, "rt_");
		$resource->resourceType = $resourceType;
		$booking->setResource($resource);
		$event = parent::populateEntity(new Event(), $row, "e_");
		$event->setEventType(parent::populateEntity(new EventType(), $row, "et_"));

		$timespan = parent::populateEntity(new Timespan(),$row,"t_");

		$eventOccation = parent::populateEntity(new EventOccation(), $row,"eo_");
		$eventOccation->setEvent($event);
		$eventOccation->setTimespan($timespan);

		$event_occation_booking = parent::populateEntity(new EventOccationBooking(),$row, "eob_");
		$event_occation_booking->setEventOccation($eventOccation);
		$event_occation_booking->setBooking($booking);
		return $event_occation_booking;
	}
	
	public function loadByEventId($event_id){
		$query = $this->getSelectAllQuery() . " where eo.event_id = ".$this->quote($event_id);
		
		return $this->loadMany($query);
	}
	
	public function loadAllGroupedByEventOccation($event_id = 0){
		$query = "
			SELECT 
				CONCAT(`resources`.`name` ,'(',`resource_types`.`name`,')') AS `resource`,
				IF(`event_occations`.`main_booking_id` = `event_occation_bookings`.`id`,'samlingssal','') AS main_resource,
				`event_occation_bookings`.`event_occation_id`

			FROM
				`event_occation_bookings`
			LEFT JOIN `bookings` ON `bookings`.`id` = `event_occation_bookings`.`booking_id`
			LEFT JOIN `resources` ON `resources`.`id` = `bookings`.`resource_id`
			LEFT JOIN `resource_types` ON `resource_types`.`id` = `resources`.`resource_type_id`
			LEFT JOIN `event_occations` ON `event_occations`.`id` = `event_occation_bookings`.`event_occation_id`";
		
		$parameters = array();
		if($event_id){
			$query .= " WHERE `event_occations`.`event_id` = :eventId";
			$parameters["eventId"] = $event_id;
		}
		$query .= "
			ORDER BY `event_occation_bookings`.`event_occation_id`";
		
		$statement = $this->execute($query,$parameters);
		
		$result = array();
		
		while($booking = $statement->fetchObject()){
			if(!isset($result[$booking->event_occation_id])){
				$result[$booking->event_occation_id] = array();
			}
			$result[$booking->event_occation_id][] = $booking;
		}
		
		return $result;
	}
	
	/**
	 * @param int $id
	 * @return EventOccationBooking[][]
	 */
	public function loadByEventOccationId($id){
		return $this->loadMany($this->getSelectAllQuery() ." where event_occation_id = :id", array("id" => $id));
	}
	
		/**
	 * 
	 * @param int $eventTypeId
	 * @return mixed
	 */
	public function loadEventOccationBookingList($eventTypeId=0,$onlyMainResources = true){
		$query = "
			SELECT 
				`event_occations`.`id` as event_occation_id,
				`resources`.`name`,
				`resources`.`id`,
				`bookings`.`id` as booking_id,
				`resource_types`.`name` AS resource_type,
				`resource_types`.`id` AS resource_type_id
			FROM `event_occation_bookings`";
		if($onlyMainResources){
			$query .="
				LEFT JOIN `event_occations` ON `event_occations`.`main_booking_id` = `event_occation_bookings`.`id`";
		}else{
			$query .="
				LEFT JOIN `event_occations` ON `event_occations`.`id` = `event_occation_bookings`.`event_occation_id`";
		}
		$query .="
			INNER JOIN `events` ON `events`.`id` = `event_occations`.`event_id` ".
			($eventTypeId > 0 ? "AND `events`.`eventtype.id` = ".$this->quote($eventTypeId) : "")."
			LEFT JOIN `bookings` ON `bookings`.`id` = `event_occation_bookings`.`booking_id`
			LEFT JOIN `resources` ON `resources`.`id` = `bookings`.`resource_id`
			LEFT JOIN `resource_types` ON `resource_types`.`id` = `resources`.`resource_type_id`
		";

		$resultSet = $this->execute($query);
		$resources = array();
		
		
		
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			if(!isset($resources[$row["event_occation_id"]]) && !$onlyMainResources)
				$resources[$row["event_occation_id"]] = array();
			$resourceBooking = new TreeItem_EventOccationBooking();
			$resourceBooking->resource_name = $row["name"];
			$resourceBooking->resource_id = $row["id"];
			$resourceBooking->booking_id = $row["booking_id"];
			$resourceBooking->resource_type_id = $row["resource_type_id"];
			$resourceBooking->resource_type_name = $row["resource_type"];
			if($onlyMainResources)
				$resources[$row["event_occation_id"]] = $resourceBooking;
			else
				$resources[$row["event_occation_id"]][] = $resourceBooking;
		}
		return $resources;
	}
	
}



?>
