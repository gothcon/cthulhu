<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
class ResourceTypeRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "resource_types";
	protected $entityClassName = "ResourceType";
	protected $databaseTableAlias = "ft";
	
	public function loadResourceTypesWithStatus(){
		$query = "
			SELECT
				rt.name as name,rt.id as id,count(r.id) as resource_count, count(if(r.available,1,NULL)) as available_resource_count
			FROM
				resource_types rt
			LEFT JOIN resources r
				ON r.resource_type_id = rt.id
			GROUP BY rt.name
			ORDER BY rt.name asc
		";
		$result = array();
		$resultSet = $this->execute($query);
		while($row = $resultSet->fetch(PDO::FETCH_OBJ)){
			$result[] = $row; 
		}
		return $result;
	}
	
}