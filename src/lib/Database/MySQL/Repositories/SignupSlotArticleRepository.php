<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
class SignupSlotArticleRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "signup_slot_articles";
	protected $entityClassName = "SignupSlotArticle";
	protected $databaseTableAlias = "ssa";
}