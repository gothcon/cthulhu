<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");
class ResourceRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "resources";
	protected $entityClassName = "Resource";
	protected $databaseTableAlias = "r";
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery("resources","r",new Resource());
		$query->addJoin("resource_types", "rt", "id", "r", "resource_type_id", new ResourceType());
		return $query->getQueryString();
	}

	/**
	 * list with availability status according to supplied event (checks availability compared to the event occations)
	 * @param int $eventId
	 * @return array
	 */
	public function loadEventBookingList($eventId){
		$query ="
			SELECT r.name,rt.name as type,r.id,r.available,collides, alreadyBooked FROM resources r LEFT JOIN
			(
			SELECT thisEventBookings.resource_id, 
						COUNT(
						CASE WHEN 
							thisEventBookings.resource_id = otherBookings.resource_id AND thisEventBookings.event_occation_id = otherBookings.event_occation_id
						THEN 1
						ELSE NULL
						END) > 0 AS alreadyBooked,
							COUNT(CASE 
								WHEN thisEventBookings.resource_id = otherBookings.resource_id
								THEN
									CASE
										WHEN thisEventBookings.starts_at > otherBookings.starts_at AND thisEventBookings.starts_at < otherBookings.ends_at THEN 1
										WHEN thisEventBookings.ends_at > otherBookings.starts_at AND thisEventBookings.ends_at < otherBookings.ends_at THEN 1
										WHEN thisEventBookings.starts_at <= otherBookings.starts_at && thisEventBookings.ends_at >= otherBookings.ends_at THEN 1
										WHEN otherBookings.starts_at <= thisEventBookings.starts_at && otherBookings.ends_at >= thisEventBookings.ends_at THEN 1
									ELSE NULL
									END
								ELSE NULL
							END ) > 0 AS `collides`

						 FROM (
							SELECT _e.name AS event, _r.name AS resource,_t.name AS timespan, _t.starts_at, _t.ends_at, _rb.id AS resource_booking_id, _rb.resource_id, _rb.event_occation_id, _eo.event_id 
							FROM resource_bookings_event_occations _rb
							LEFT JOIN resources _r ON _rb.resource_id = _r.id
							LEFT JOIN event_occations _eo ON _eo.id = _rb.event_occation_id
							LEFT JOIN timespans _t ON _t.id = _eo.timespan_id
							LEFT JOIN events _e ON _e.id = _eo.event_id
							ORDER BY event
						) AS otherBookings
						JOIN (
							SELECT _r.name AS resource, _r.resource_type_id, _r.id AS resource_id, _eo.event_id, _t.starts_at, _t.ends_at, _eo.id AS event_occation_id
							FROM event_occations _eo 
							LEFT JOIN timespans _t ON _t.id = _eo.timespan_id
							LEFT JOIN events _e ON _e.id = _eo.event_id
							JOIN resources _r 
							WHERE _eo.event_id = ".$this->quote($eventId)."
						)  thisEventBookings
						LEFT JOIN resource_types rt ON rt.id = thisEventBookings.resource_type_id
						GROUP BY thisEventBookings.resource_id	
			) resourceStatus ON resourceStatus.resource_id = r.id
			LEFT JOIN resource_types rt ON rt.id = r.resource_type_id	
			order by r.available desc, r.name desc
		";
		$result = array();
		$resultSet = $this->execute($query);
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$result[] = $row; 
		}
		return $result;
		
	}
	
	/** 
	 * list with availability status compared to the supplied event occation
	 * @param type $eventOccationId
	 * @return type
	 */
	public function loadEventOccationBookingList($eventOccationId){
				$query ="
			SELECT r.name,rt.name as type,r.id,r.available,collides, alreadyBooked FROM resources r LEFT JOIN
			(
			SELECT thisEventBookings.resource_id, 
						COUNT(
						CASE WHEN 
							thisEventBookings.resource_id = otherBookings.resource_id AND thisEventBookings.event_occation_id = otherBookings.event_occation_id
						THEN 1
						ELSE NULL
						END) > 0 AS alreadyBooked,
							COUNT(CASE 
								WHEN thisEventBookings.resource_id = otherBookings.resource_id
								THEN
									CASE
										WHEN thisEventBookings.starts_at > otherBookings.starts_at AND thisEventBookings.starts_at < otherBookings.ends_at THEN 1
										WHEN thisEventBookings.ends_at > otherBookings.starts_at AND thisEventBookings.ends_at < otherBookings.ends_at THEN 1
										WHEN thisEventBookings.starts_at <= otherBookings.starts_at && thisEventBookings.ends_at >= otherBookings.ends_at THEN 1
										WHEN otherBookings.starts_at <= thisEventBookings.starts_at && otherBookings.ends_at >= thisEventBookings.ends_at THEN 1
									ELSE NULL
									END
								ELSE NULL
							END ) > 0 AS `collides`

						 FROM (
							SELECT _e.name AS event, _r.name AS resource,_t.name AS timespan, _t.starts_at, _t.ends_at, _rb.id AS resource_booking_id, _rb.resource_id, _rb.event_occation_id, _eo.event_id 
							FROM resource_bookings_event_occations _rb
							LEFT JOIN resources _r ON _rb.resource_id = _r.id
							LEFT JOIN event_occations _eo ON _eo.id = _rb.event_occation_id
							LEFT JOIN timespans _t ON _t.id = _eo.timespan_id
							LEFT JOIN events _e ON _e.id = _eo.event_id
							ORDER BY event
						) AS otherBookings
						JOIN (
							SELECT _r.name AS resource, _r.resource_type_id, _r.id AS resource_id, _eo.event_id, _t.starts_at, _t.ends_at, _eo.id AS event_occation_id
							FROM event_occations _eo 
							LEFT JOIN timespans _t ON _t.id = _eo.timespan_id
							JOIN resources _r 
							WHERE _eo.id = ".$this->quote($eventOccationId)."
						)  thisEventBookings
						LEFT JOIN resource_types rt ON rt.id = thisEventBookings.resource_type_id
						GROUP BY thisEventBookings.resource_id	
			) resourceStatus ON resourceStatus.resource_id = r.id
			LEFT JOIN resource_types rt ON rt.id = r.resource_type_id	
			order by r.available desc, r.name desc
		";
		$result = array();
		$resultSet = $this->execute($query);
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$result[] = $row; 
		}
		return $result;
	}
	
	public function load($id = false) {
		if(!$id){
			$query = $this->getSelectAllQuery()." order by available desc, rt.name asc, r.name asc";
			// die($query);
			return $this->loadMany($this->getSelectAllQuery()." order by available desc, rt.name asc, r.name asc", array(), "PopulateEntity");
		}
		else
			return parent::load($id);
	}

	public function loadByType($typeId) {
		
		return $this->loadMany($this->getSelectAllQuery()." where resource_type_id = ".$this->quote($typeId)." order by available desc, r.name asc", array(), "PopulateEntity");
	}
	
	public function loadSorted($property="type",$dir = "asc"){
		$dir = $dir == "asc" ? $dir : "desc";
		switch($property){
			case "id":
				$order = "order by {$this->databaseTableAlias}.id {$dir}";
				break;
			case "name":
				$order = "order by {$this->databaseTableAlias}.name {$dir}";
				break;
			case "type":
				$order = "order by rt.name {$dir}, {$this->databaseTableAlias}.name {$dir}";
				break;			
			
		}
		$query = $this->getSelectAllQuery()." ".$order; 

		return $this->loadMany($query);
	}

	public function PopulateEntity($resource,$row,$prefix="r_"){
		$resource = parent::PopulateEntity($resource,$row,$prefix);
		$resourceType = new ResourceType();
		$resourceType = parent::PopulateEntity($resourceType,$row,"rt_");
		$resource->resourceType = $resourceType;
		
		return $resource;
	}
	
}