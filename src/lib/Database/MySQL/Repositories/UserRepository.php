<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class UserRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "users";
	protected $entityClassName = "User";
	protected $databaseTableAlias = "u";

	public function getUserByUsername($username){
		return $this->loadOne($this->getSelectAllQuery() ." where u.username like :username", array("username" => $username));
	}

	public function getUserByUsernameAndOldPassword($username,$password){
		return $this->loadOne($this->getSelectAllQuery() ." where u.username like :username and old_password like password(:password)", array("username" => $username, "password" => $password));
	}

	public function getUserByResetEmailToken($token){
		return $this->loadOne($this->getSelectAllQuery() ." where u.reset_password_token = :token", array("token" => $token));
	}

	
	public function getUsersByEmailAddress($emailAddress){
		$query = new MySQLQuery("users", "u", "User");
		$query->addJoin("people", "p", "id", "u", "person_id",  "Person");
		$where = " where p.email_address = :emailAddress";
		return $this->loadMany($query->getQueryString() . $where, array("emailAddress" => $emailAddress));
	}

	public function usernameIsTaken($username,$id){
		$row = $this->loadOne("select count(*) as count from users where username like :username and id != :id", array("username" => $username,"id" => $id),"GetPlainRow");
		return $row['count'] > 0;
	}
	
	public function GetPlainRow($user,$row){
		return $row;
	}
	
	public function loadByPersonId($id){
		return $this->loadOne( "
			{$this->getSelectAllQuery()}
			where u.person_id=:id",array("id" =>$id),"PopulateEntity");
	}

}