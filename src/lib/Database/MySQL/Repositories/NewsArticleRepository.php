<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "../MySQLQuery.php");

class NewsArticleRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "news_articles";
	protected $entityClassName = "NewsArticle";
	protected $databaseTableAlias = "na";
	
	public function getPublicNews($count = 10){
		
		$now = $this->quote(strftime("%Y-%m-%d %H:%M:%S",time()));
	
		$query = $this->getSelectAllQuery().
				"
				WHERE 
					{$this->databaseTableAlias}.is_internal != 1 and
					(publish_at <= {$now} || IsNull(publish_at)) and (unpublish_at > {$now} || IsNull(unpublish_at))	
				ORDER BY publish_at desc
				LIMIT {$count}
				";

		
		return $this->loadMany($query);
	}
}