<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLReadWriteRepository.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "..". DIRECTORY_SEPARATOR ."/MySQLQuery.php");
class EmailRepository extends MySQLReadWriteRepository{
	protected $databaseTableName = "sent_emails";
	protected $entityClassName = "Email";
	protected $databaseTableAlias = "email";
	
	public function getAllEmailAddresses(){
		$querystring = "select email_address, concat(first_name,' ',last_name) as name from people where email_address != ''";
		return $this->getEmailAddresses($querystring);
	}
	
	public function getCustomerEmailAdresses($categoryId = null ,$paymentStates = array(),$articleId = null){
		$querystring ="
			SELECT DISTINCT 
			CONCAT(`people`.`first_name`,' ',`people`.`last_name`) as name,
			`people`.`email_address`
			FROM `articles`
			LEFT JOIN `order_rows` ON `articles`.`id` = `order_rows`.`article_id` 
			LEFT JOIN `orders` ON `order_rows`.`order_id` = `orders`.`id` 
			LEFT JOIN `people` ON `people`.`id` = `orders`.`person_id`
			WHERE !ISNULL(people.id)";
		
			 
		if(!is_null($categoryId)){
			$querystring .= " AND `articles`.`p` = ".$this->quote($categoryId);
		}
		if(!is_null($articleId)){
			$querystring .= " AND `articles`.`id` = ".$this->quote($articleId);
		}
		if(count($paymentStates)){
			foreach($paymentStates as &$state){
				$state = $this->quote($state);
			}
			$querystring .= " AND `orders`.`status` IN(" . implode(",", $paymentStates).")";
		}
		
		return $this->getEmailAddresses($querystring);
	}
	
	
	public function getOrganizerEmailAddresses($eventId=0){
		$querystring = "
			SELECT DISTINCT
				IF(ISNULL(o.group_id),CONCAT(p.first_name,' ',p.last_name),g.name) AS `name`,
				IF(ISNULL(o.group_id),p.email_address,g.email_address) AS `email_address`
			FROM `organizers` o
			LEFT JOIN `people` p ON p.id = o.person_id
			LEFT JOIN `groups` g ON g.id = o.group_id

			WHERE !(ISNULL(o.group_id) && ISNULL(o.person_id))
		";
		
		if($eventId)
			$querystring .= " AND o.event_id = ".$this->quote($eventId);
		
		return $this->getEmailAddresses($querystring);
	}
	
	public function getAttenderEmailAddresses($eventId = null,$slotTypeId = null,$paymentStatus = null,$approvalStatus = null){
		$eventOccationId = null;
		$querystring = "
			SELECT DISTINCT
				IF(ISNULL(s.group_id),CONCAT(p.first_name,' ',p.last_name),g.name) AS `name`,
				IF(ISNULL(s.group_id),p.email_address,g.email_address) AS `email_address`
			FROM `signups` s
			LEFT JOIN `people` p ON p.id = s.person_id
			LEFT JOIN `groups` g ON g.id = s.group_id
			LEFT JOIN `signup_slots` ss  ON ss.id = s.signup_slot_id
			LEFT JOIN `event_occations` eo ON eo.id = ss.event_occation_id 
			LEFT JOIN `signup_slot_articles` ssa ON ssa.signup_slot_id = ss.id 
			WHERE !(ISNULL(s.group_id) && ISNULL(s.person_id))
		";
		if(!is_null($eventId)){
			$querystring .= " AND eo.event_id = " . $this->quote($eventId);
		}else if(!is_null($eventOccationId)){
			$querystring .= " AND eo.id = " . $this->quote($eventOccationId);
		}
		
		if(!is_null($slotTypeId)){
			$querystring .= " AND ss.slot_type_id = " . $this->quote($slotTypeId);
		}
		
		if(!is_null($approvalStatus)){
			$querystring .= " AND s.is_approved = ".($approvalStatus ? 1 : "0 and ss.requires_approval = 1");
		}

		if(!is_null($paymentStatus)){
			$querystring .= " AND s.is_paid = ".($paymentStatus ? 1 : "0 and !isnull(ssa.id)");
		}
		// die($querystring);
		return $this->getEmailAddresses($querystring);
		
	}
	
	protected function getEmailAddresses($querystring){
		$resultSet = $this->execute($querystring);		
		$addresses = array();
		
		while($obj = $resultSet->fetch(PDO::FETCH_ASSOC)){
			$name = trim($obj["name"]);
			$addresses[] = ((strlen($name)) ? "$name <{$obj["email_address"]}>" : $obj["email_address"]);
		}
		return $addresses;
	}
	
}