<?php
include_once(dirname(__FILE__). DIRECTORY_SEPARATOR ."MySQLReadOnlyRepository.php");
include_once(dirname(__FILE__). DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR  ."IReadWriteRepository.php");
// include_once(dirname(__FILE__). DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR  .".." . DIRECTORY_SEPARATOR  ."SessionAccessors/UserAccessor.php");

class MySQLReadWriteRepository extends MySQLReadOnlyRepository implements IReadWriteRepository{
		
		public function getDatabaseTableName(){
			return $this->databaseTableName;
		}
		
		public function getDatabaseTableAlias(){
			return $this->databaseTableAlias;
		}
		
		/**
		 * @param Entity $entity 
		 */
		public function save(&$entity){
			if($entity->id > 0){
				$entity->modified_by = UserAccessor::getCurrentUser()->username;
				$resultSet = $this->update($entity);
				$operation = "update";
			}else{
				$entity->created_by = UserAccessor::getCurrentUser()->username;
				$resultSet = $this->insert($entity);
				$operation = "create";
			}
			$this->logChanges($entity, $operation,UserAccessor::getCurrentUser()->username);
			// TODO Return value should reflect outcome of save
			return true;
		}
	
		/**
		 * @param mixed $entity 
		 */
		protected function update(&$entity){
			$query = "update " . $this->databaseTableName ." set ";
			$parameters = array();
			$first = true;
			$persistedProperties = $entity->getPersistedProperties(); 
			foreach($persistedProperties as $key => $value){
				$parameters[$key] = $value;	
				if($key != "id"){
					if($first)
						$first = false;
					else{
						$query .=", ";
					}
					$query .= "{$key}=:{$key}";   
				}
			}
			$query .= " where id=:id";
			$this->execute($query,$parameters);
			return true;
		}
		
		/**
		 * @param ReadOnlyEntity $mixed 
		 */
		protected function insert(&$mixed){
			
			$query = "insert into " . $this->databaseTableName ."  (";
			$valuesString = "values(";
			$parameters = array();
			$persistedProperties = $mixed->getPersistedProperties(); 
			foreach($persistedProperties as $key => $value){
				if($key != "id"){
					$query .= "{$key},";
					$valuesString .= "[:{$key}],";
					$parameters[$key] = $value;	
				}
			}
			
			// strip the trailing comma of the query and the values string, add the finishing parenthesis and concatenate them
			$query = substr($query,0,strlen($query)-1) .") ". substr($valuesString,0,strlen($valuesString)-1) . ")";
			
			foreach($parameters as $key => $value){
				$query = str_replace("[:{$key}]",$this->quote($value),$query);
			}
			
			$this->execute($query,$parameters);
			$conn = $this->getConnection();
			$mixed->id=$conn->lastInsertId();
			return true;
			
		}
		
		/**
		 * @param mixed $entity 
		 */
		public function delete($mixed){
			$id = (is_object($mixed)) ? $mixed->id : $mixed; 
			/** TODO: only load if loggin is on **/
			$entity = $this->load($id);
			if($entity){
				$query = "delete from {$this->databaseTableName} where id = :id";
				$parameters = array("id" => $id);
				$this->execute($query,$parameters);
				$this->logChanges($entity, "delete", UserAccessor::getCurrentUser()->username);
				return true;
			}  else {
				return false;
			}
			// TODO update this to reflect success or failure
		}
		
	
}

?>
