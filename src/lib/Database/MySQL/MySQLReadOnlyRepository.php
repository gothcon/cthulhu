<?php
include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "MySQLRepository.php");
include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "IReadOnlyRepository.php");
class MySQLReadOnlyRepository extends MySQLRepository implements IReadOnlyRepository {
	
	protected $databaseTableName = null;
	protected $entityClassName = null;
	protected $databaseTableAlias = null;
	
	public function __construct() {
		parent::__construct();
		if($this->entityClassName === null || $this->databaseTableName === null || $this->databaseTableAlias == null){
			die("Class " . get_class($this) . " was not configured correctly. Entity name, database table name or database table alias was not correctly set");
		}
		require_once(dirname(__FILE__). DIRECTORY_SEPARATOR ."..".DIRECTORY_SEPARATOR."Entities". DIRECTORY_SEPARATOR .$this->entityClassName.".php");
	}
	
	public function load($id = false){
		if($id === false){
			return $this->loadMany($this->getSelectAllQuery());
		}else{
			return $this->loadOne( "
				{$this->getSelectAllQuery()}
				where {$this->databaseTableAlias}.id=:id",array("id" =>$id),"PopulateEntity");
		}
	}
	
	protected function getSelectAllQuery(){
		$query = new MySQLQuery($this->databaseTableName,$this->databaseTableAlias,$this->entityClassName);
		return $query->getQueryString();
	}

	protected function loadMany($query,$parameters = array(),$populatorCallback = "PopulateEntity"){
		// echo $query;
		$resultSet = $this->execute($query,$parameters);
		$allEntities = array();
		$className = $this->entityClassName;
		while($row = $resultSet->fetch(PDO::FETCH_ASSOC)){
			
			$entity = new $className; //
			
			$entity = call_user_func(array(&$this,$populatorCallback),$entity, $row);
			
			if($entity)
				$allEntities[] = $entity;
		}
		return $allEntities;
	}
	/**
	 * @param string $query
	 * @param mixed  $parameters
	 * @param string $populatorCallback
	 * @return Entity 
	 */
	protected function loadOne($query,$parameters=array(),$populatorCallback="populateEntity"){
		$resultSet = $this->execute($query,$parameters);
		
		$row=$resultSet->fetch(PDO::FETCH_ASSOC);
		
		if($row !== false){
			$entity = new $this->entityClassName();
			$entity = call_user_func(array(&$this,$populatorCallback),$entity, $row);
			return $entity;
		}
		return null;
	}
	
	public function loadSorted($property="id",$dir = "asc"){
		$dir = $dir == "asc" ? $dir : "desc";
		$entity = new $this->entityClassName();
		$persistedProperties = $entity->getPersistedProperties(); 
		if(array_key_exists($property, $persistedProperties)){
			$order = "order by {$this->databaseTableAlias}.{$property} {$dir}";
		}else{
			$order = "";
			die("trying to sort by unknown database column {$property}");
		}
		
		return $this->loadMany($this->getSelectAllQuery()." ".$order);
	}
	
	/**
	 *
	 * @param ReadOnlyEntity $entity
	 * @param array $row
	 * @param string $prefix
	 * @return ReadOnlyEntity 
	 */
	protected function populateEntity($entity,$row,$prefix = false){
		
		if(!is_a($entity,"IReadOnlyEntity")){
			die("Object ". get_class($entity) ."must extend the Entity class");
		}
		if($prefix === false){
			$prefix = "{$this->databaseTableAlias}_";
		}
		else if(strlen($prefix) && $prefix{strlen($prefix)-1} !== "_"){
			$prefix = $prefix ."_";
		}
		$entityPropertyNames = $entity->getPersistedPropertyNames();
		
		foreach($entityPropertyNames as $key){
			
			if(key_exists($prefix.$key, $row)){
				$entity->setPersistedProperty($key,$row[$prefix.$key]);
				// echo "setting {$key} with {$prefix}{$key} ({$row[$prefix.$key]})\n";
			}
		}
		
		return $entity;
	}
	
}

?>
