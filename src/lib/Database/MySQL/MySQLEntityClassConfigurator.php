<?php



class MySQLEntityClassConfigurator{
	
	private static $entityClassNames = array(
		"Article",
		"Event",
		"Email",
		"EventOccation",
		"EventType",
		"Group",
		"GroupMembership",
		"NewsArticle",
		"Order",
		"OrderPayment",
		"OrderRow",
		"Organizer",
		"Person",
		"Resource",
		"ResourceBooking",
		"ResourceType",
		"Signup",
		"SignupSlot",
		"SignupSlotArticle",
		"SleepingResource",
		"SleepingSlotBooking",
		"SlotType",
		"Timespan",
		"Transaction",
		"User",
		"Booking",
		"TimedBooking",
		"EventBooking",
		"EventOccationBooking",
		"Image"
	);
	
	/**
	 * @param MySQLConnectionFactory $connectionFactory 
	 */
	public static function initRepositories(){
		
		foreach(static::$entityClassNames as $entityClassName){
			
			$repositoryClassName = "{$entityClassName}Repository"; 
			require_once( dirname(__FILE__) . DIRECTORY_SEPARATOR . "../Entities/{$entityClassName}.php");
			require_once( dirname(__FILE__) . DIRECTORY_SEPARATOR . "Repositories/{$repositoryClassName}.php");
			$repository = new $repositoryClassName();
			$entityClassName::setRepository( $repository);
			
		}
		
	}
}

?>
