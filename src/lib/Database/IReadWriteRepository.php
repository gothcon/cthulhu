<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IReadOnlyRepository.php");

interface IReadWriteRepository extends IReadOnlyRepository{
	public function save(&$entity);
	public function delete($mixed);
}