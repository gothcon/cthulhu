<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IRepository.php");
interface IReadOnlyRepository extends IRepository{
	/**
	 * @return mixed
	 */
	public function load($id=null);
}

?>
