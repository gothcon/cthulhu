<?php
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "IEntity.php");
interface IReadOnlyEntity extends IEntity{
	/**
	 * @param IReadOnlyRepository $iReadOnlyRepository
	 */
	public static function setRepository($iReadOnlyRepository);
	
	public function load();
	public static function loadList();
	public static function loadById($id);
	public function getPersistedProperties();
	public function setPersistedProperties($properties);
	public function getPersistedPropertyNames();
	public function setPersistedProperty($propertyName,$propertyValue);
}

?>
