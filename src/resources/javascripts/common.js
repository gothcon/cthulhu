/*Cufon.replace("H1, H2, H3, H4, H5, .tabContent, .menu .header, #leftMenu, #topMenu"); */

$(function(){
	
	$(window).trigger('hashchange');

	$("textarea, input[type!=button], select").each(function(key,value){
		$element = $(value);
		$element.bind("focus",function(e){
			$(e.target).addClass("activeInput");
		});
		$element.bind("blur",function(e){
			$(e.target).removeClass("activeInput");
		});
	});

	$("select[name=timespan_id]").each(function(index,object){
		var $this = $(object);
		var $form = $(object).siblings(".anpassat");
		if($this.val() != 0){
			$form.hide();
		}
		$this.bind("change",function(){
			if($this.val() == 0){
				$form.slideDown();
			}
			else{
				$form.slideUp();
			}
		});
	})

	$(".hideIfJavascript").hide();
	
//	$('.tiny').tinymce({
//		
//		// Location of TinyMCE script
//		script_url : '/resources/javascripts/library/tiny_mce4/tiny_mce.js',
//		// General options
//		plugins : "pagebreak,table,image,link,preview,contextmenu,advlist",
//
//		// Theme options
//		toolbar : "undo,redo,|,link,unlink,anchor,|,bold,italic,blockquote,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,|,table",
//		menubar: false
//
//	});
});

