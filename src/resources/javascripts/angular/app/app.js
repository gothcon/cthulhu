(function(){
/*jshint strict:false */
"use strict";





angular.module("Sleeping",['GothCon','Filters','ngDialog','HttpDataProviders'])
	.directive("accommodationSelection", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,People){
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        People.availableAccommodations(newVal,function(data){
                            
							$scope.availableAccommodations = data.data;
							for(var key in $scope.availableAccommodations){
								if($scope.availableAccommodations[key].isSelected){
									$scope.selectedSleepingResourceId = $scope.availableAccommodations[key].sleepingResourceId;
								}
							}
                        });
					}
					else{
						$scope.eventtypes = [];
					}
				});
				
				$scope.selectAccommodation = function(id){
					$scope.selectedSleepingResourceId = id;
				};
				
				$scope.selectedSleepingResourceId = "-1";
				$scope.saveAccommodation = function(){
					
					People.saveAccommodation($scope.currentPerson.id,$scope.selectedSleepingResourceId,function(){
						
					});
				};
            },
			scope : {
				currentPerson : "=",
			},
			replace : true,
            templateUrl : "directives/accommodation/accommodation.tpl.html",
            restrict : 'AE'
        };
    });

angular.module("Signups",['GothCon','Filters','ngDialog','HttpDataProviders','Event'])
	.controller("SignupSlotController", function($scope){
		$scope.saveSignup = function(signupSlot){
			$scope.signupButtonIsDisabled = true;
			return $scope.__proto__.saveSignup(signupSlot);
		};
    })
	.directive("signups", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,SignupSlot,Signups){
				var onShowSignupDialogue = angular.noop;
				
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        Signups.getSignupsByPerson(newVal,function(data){
                            $scope.signups = data.data;
                        });
						$scope.saveSignup = savePersonalSignup;
						onShowSignupDialogue = getPersonalSignupSlots;
					}else{
						$scope.eventtypes = [];
					}
				});

				$scope.$watch(function(){ return (!$scope.currentGroup) ? undefined : $scope.currentGroup.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        Signups.getSignupsByGroup(newVal,function(data){
                            $scope.signups = data.data;
                        });
						$scope.saveSignup = saveGroupSignup;
						onShowSignupDialogue = getGroupSignupSlots;
					}else{
						$scope.eventtypes = [];
					}
				});
				
				$scope.deleteSignup = function (signup){
					if(confirm("Är du säker?")){
						Signups.deleteSignup(signup.id,function(){
							$scope.signups.splice($scope.signups.indexOf(signup),1);
						});
					}
				};
				
				
				function createSaveResponseHandler(signupSlot){
					return function onSaveResponse(response){
						$scope.signups.push(response.data);
						for(var etKey in $scope.eventtypes){
							// event type
							var eventType = $scope.eventtypes[etKey];
							for(var eKey in eventType.events){
								// event
								var event = eventType.events[eKey];
								for(var eoKey in event.eventOccations){
									// event occation
									var eventOccation = event.eventOccations[eoKey];
									var index = eventOccation.signupSlots.indexOf(signupSlot);
									if(index >= 0){
										eventOccation.signupSlots.splice(index, 1);
									}
									if(eventOccation.signupSlots.length === 0){
										event.eventOccations.splice(eoKey, 1);
										break;
									}
								}
								if(event.eventOccations.length === 0){
									eventType.events.splice(eKey, 1);
									break;
								}
							}
							if(eventType.events.length === 0){
								$scope.eventtypes.splice(etKey, 1);
								break;
							}
						}
					};
				}
				
				var savePersonalSignup = function(signupSlot){
					Signups.savePersonalSignup($scope.currentPerson.id, signupSlot.id,createSaveResponseHandler(signupSlot));
				};
				
				var saveGroupSignup = function(signupSlot){
					Signups.saveGroupSignup($scope.currentGroup.id, signupSlot.id, createSaveResponseHandler(signupSlot));
				};
				
				var getPersonalSignupSlots = function(){
					SignupSlot.getPersonalSignupSlots($scope.currentPerson.id,function(response){
						$scope.eventtypes = response.data;
					});
				};

				var getGroupSignupSlots = function(){
					SignupSlot.getGroupSignupSlots($scope.currentGroup.id,function(response){
						$scope.eventtypes = response.data;
					});
				};
				
				$scope.showSignupDialogue=function(){
					onShowSignupDialogue();
					ngDialog.open({
					   template : urls.getDirectivesUrl() + 'signups/signupDialogue.tpl.html',
					   className: 'ngdialog-theme-default',
					   scope : $scope
					});
				}; 
				
            },
			scope : {
				currentPerson : "=?",
				currentGroup : "=?"
			},
			replace : true,
            templateUrl : urls.getDirectivesUrl() + "signups/signups.tpl.html",
            restrict : 'AE'
        };
    });
	

}());