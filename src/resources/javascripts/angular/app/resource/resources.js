(function(){
/*jshint strict:false */


angular.module("Resource",['ngResource','GothCon','HttpDataProviders','Filters','ngDialog'])
	.provider("ResourceBookings",function(){
		var baseUrl = "";
		return {
			setBaseUrl : function(newBaseUrl){
				baseUrl = newBaseUrl;
			},
			$get : function GroupDataProviderFactory($q,$http){

				var promise;

				function getById(id){
					var deferredJob = $q.defer();
					promise = deferredJob.promise;
					
					// load from http
					$http({
						url : baseUrl + "/administrator/booking/svc/getBookingsByResource?resource_id=" + id,
						method: "get",
						responseType : "json"
					}).then(function(response){
						deferredJob.resolve(response.data.data);
					}, function(error){
						deferredJob.reject("error");
						console.log(error);
						throw "Failed to load bookings. See console for more information";
					});
					
					return promise;
				}
				function getScheduleData(){
					var deferredJob = $q.defer();
					promise = deferredJob.promise;
					
					// load from http
					$http({
						url : baseUrl + "/administrator/booking/svc/getScheduleData",
						method: "get",
						responseType : "json"
					}).then(function(response){
						deferredJob.resolve(response.data);
					}, function(error){
						deferredJob.reject("error");
						alert(error);
						throw "Failed to load bookings. See console for more information";
					});
					
					return promise;
				}
				
				return {
					getById : getById,
					getScheduleData : getScheduleData
				};

			}
		};
	})
	.controller("ResourceAppController", function($scope,$location,gcTabsBus,Resource,urls){

	})
	.directive("resource",function(){
		var extractIdRegex = /^\/resource\/(\d+|new|schedule)(\/|$)/;    

		return {
			controller : function($scope,$location,gcTabsBus,Resource,$element,$compile,urls){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);

					$scope.view = "";

					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});

					$scope.$on('$locationChangeSuccess',onLocationChange);

					$scope.currentResource = {};

					$scope.deleteAccommodation = function (accommodation){
						if(confirm("Är du säker?")){
							Resource.deleteAccommodation(accommodation.id,function(){
								$scope.resource.sleepingResource.sleepingSlotBookings.splice($scope.resource.sleepingResource.sleepingSlotBookings.indexOf(accommodation),1);
							});
						}
					};
				}

				function onLocationChange(){

					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						showList();
						return;
					}
					else if(id === "schedule")
					{
						// this is the "new" form
						showSchedule();
						return;
					}
					else if(id === "new")
					{
						// this is the "new" form
						showNewForm();
						return;
					}
					else{
						// this is the standard "view"
						showResource(id);
						return;
					}

				}

				function showList(){
					$scope.title = "Resurslista";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.currentResource = undefined;
					$scope.resources = Resource.query(function(groups){
						$scope.groupList = groups;
						$scope.view = "<resource-list resources='resources'></resource-list>";
					});
				}
				
				function showSchedule(){
					$scope.title = "Bokningsöversikt";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.currentResource = undefined;
					$scope.groupList = [];
					$scope.view = "<resource-bookings-overview></resource-bookings-overview>";
				}

				function showNewForm(){
					$scope.title = "Ny resurs";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.view = "<resource-details/>";
				}

				function showResource(id){
					id = parseInt(id);
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'resource/' + id + '/accommodation', 'label' : 'Sovning'},
						{ id : 'resource/' + id + '/bookings', 'label' : 'Bokningslista'},
						{ id : 'resource/' + id + '/schedule', 'label' : 'Bokningsschema'},
						{ id : 'resource/' + id + '/delete', 'label' : 'Ta bort'}]);

					if(!$scope.resource || "" + $scope.currentResource.id !==  "" + id){
						
						$scope.title = "Laddar detaljer för resurs #" + id;
						Resource.get(id, function(response){
							
							switch($location.path()){
							case "/resource/" + id + "/accommodation":
								$scope.view = "<resource-accommodation resource='currentResource' sleeping-event-occations='sleepingEventOccations'/>";
								break;
							case "/resource/" + id + "/bookings":
								$scope.view = "<resource-bookings-list resource='currentResource'/>";
								break;
							case "/resource/" + id + "/schedule":
								$scope.view = "<resource-bookings-schedule resource='currentResource'/>";
								break;
							case "/resource/" + id + "/delete":
								$scope.view = "<resource-delete resource='currentResource'/>";
								break;
							default:
								$scope.view = "<resource-details resource='currentResource'/>";
							}		
							
							$scope.currentResource = response.data.resource;
							$scope.title = "Detaljer för " + $scope.currentResource.name;
							$scope.sleepingEventOccations = response.data.sleepingEventOccations;
						});
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "resource/templates/resource.tpl.html",
			restrict : 'E'				
		};
	})
	.directive("resourceList",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "resource/templates/list.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceAccommodation",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.onSaveAccommodations = function(accommodations){
					Resource.addAccommodations($scope.resource.id, accommodations,function(response){
						$scope.resource.sleepingResource.sleepingSlotBookings = response.data.data;
					});
				};
					
				$scope.updateOccationSelection = function($event,occationId){
					$scope.sleepingEventOccations[occationId].selected = $event.srcElement.checked;
				};

				$scope.updateOccations = function(){
					Resource.setSleepingOccations($scope.resource.id, $scope.sleepingEventOccations);
				};
			},
			scope : {
				resource : "=",
				sleepingEventOccations : "="
			},
			replace : true,
			templateUrl : "resource/templates/accommodation.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceBookingsList",function(){
		return {
			controller : function ($scope,Resource,ResourceBookings){
				$scope.saveEventOccationBookings = function(occations){
					Resource.addOccationBookings($scope.resource.id,occations, function(response){
						ResourceBookings.getById($scope.resource.id).then(function(data){
							$scope.bookings = data;
						});
					});
				};
				$scope.bookings = [];
				ResourceBookings.getById($scope.resource.id).then(function(data){
					$scope.bookings = data;
				});
				$scope.deleteBooking = function(booking){
					if(confirm("Är du säker?")){
						Resource.deleteOccationBooking(booking.id,function(){
							for(var bKey in  $scope.bookings){
								if($scope.bookings[bKey].id === booking.id){
									$scope.bookings.splice(bKey,1);
								}
							}
						});
					}
				};
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingslist.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceBookingsSchedule",function(urls){
		return {
			controller : function($scope,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				$scope.resourceId = $scope.resource.id;
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addEvents(eventTypes,conventionStartsAt){
					var eventTypeBoxes = [];
					for(var key in eventTypes){
						
						var eventType = eventTypes[key];
						var eventTypeBox = {
							name : eventType.name,
							id : eventType.id,
							events : []
						};
						for(var eventKey in eventType.events){
							var event = eventType.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.eventOccations){
								var eventOccation = event.eventOccations[occationKey];
								if(eventOccation.timespan){
									var occationBox = {
										name : eventOccation.name || eventOccation.timespan.name,
										id : eventOccation.id,
										bookingId : eventOccation.eventOccationBookings[0].booking.id,
										width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
										offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
									};
									eventBox.eventOccations.push(occationBox);
								}
							}
							eventTypeBox.events.push(eventBox);
						}
						eventTypeBoxes.push(eventTypeBox);
					}
					$scope.eventTypes = eventTypeBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				Resource.getScheduleData($scope.resource.id, function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.publicTimespans;
					var eventTypes = response.data.bookings;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addEvents(eventTypes,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingschedule.tpl.html",
            restrict : 'E'
		};
	})
	.directive("resourceBookingsOverview",function(urls){
		return {
			controller : function($scope,ResourceBookings,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addBookings(resources,conventionStartsAt){
					var resourceBoxes = [];
					for(var key in resources){
						
						var resource = resources[key];
						var resourceBox = {
							name : resource.name,
							id : resource.id,
							events : []
						};
						for(var eventKey in resource.events){
							var event = resource.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.bookings){
								var eventOccation = event.bookings[occationKey];

								var occationBox = {
									name : eventOccation.name,
									bookingId : eventOccation.bookingId,
									width: getOccationWidth(eventOccation.startsAt,eventOccation.endsAt),
									offset: getOccationOffset(eventOccation.startsAt,conventionStartsAt)
								};
								eventBox.eventOccations.push(occationBox);

							}
							resourceBox.events.push(eventBox);
						}
						resourceBoxes.push(resourceBox);
					}
					$scope.resources = resourceBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				ResourceBookings.getScheduleData().then(function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.publicTimespans;
					var resources = response.data.resources;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addBookings(resources,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingoverview.tpl.html",
            restrict : 'E'
		};
	})
	.directive("resourceDelete",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.delete = function (){
						if(confirm("Är du säker?")){
							Resource.delete($scope.resource.id,function(){
								$scope.resource = undefined;
								$location.path("resource");
							});
						}
					};
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "resource/templates/delete.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceDetails",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.resourceTypes = [];
				Resource.types(function(response){
					$scope.resourceTypes = response.data;
				});
				$scope.saveResource = function(){
					Resource.save({
						id : $scope.resource.id || "new",
						name : $scope.resource.name,
						description : $scope.resource.description,
						resourceType : $scope.resource.resourceType.id,
						isAvailable : $scope.resource.available,
						sleepingIsAvailable : $scope.resource.sleepingResource.isAvailable,
						noOfSleepingSlots : $scope.resource.sleepingResource.noOfSlots
					},function(response){
						$scope.resource = response.data.resource;
						$scope.sleepingEventOccations = response.data.sleepingEventOccations;
						$location.path("/resource/" + $scope.resource.id);
					});
				};
				if(!$scope.resource){
					$scope.resource = {
						id : "new",
						name : "",
						description : "",
						resourceType : {id:0},
						available : true,
						sleepingResource : {
							isAvailable : false,
							noOfSlots : 0
						}
					};
				}
			},
			scope : {
				resource : "=?"
			},
			replace : true,
			templateUrl : "resource/templates/form.tpl.html",
			restrict : 'E'			
		};
	})	
	.directive("eventOccationSelector",function(urls){
		return {
            controller : function($scope,ngDialog,$element,Resource){
				
				$scope.formdata = {
					eventOccations : []
				};

				$scope.getOccationTrueValue = function(occation){
					if(occation.already_booked){
						return "";
					}
					else{
						return occation.id;
					}
				};

				$scope.getOccationFalseValue = function(){
					return "";
				};

				$scope.dialogueTemplate = "/" + urls.getAngularAppUrl() + 'resource/templates/eventOccationSelector/selectorDialogue.tpl.html';

				$scope.dialogHeader = $scope.dialogHeader || "Välj arrangemangstillfällen";
				
				$scope.onSaveSelection = $scope.onSaveSelection || function(){ return true; };
				
				$element.bind("click",function(){
					
					$scope.saveSelection = function(){
						var occations = [];
						for(var key in $scope.formdata.eventOccations){
							if($scope.formdata.eventOccations[key] && $scope.formdata.eventOccations[key] !== ""){
								occations.push($scope.formdata.eventOccations[key]);
							}
						}
						$scope.onSaveSelection({ occations : occations});
					};
					
					$scope.selectEventOccations = function(event,$event){
						var eoKey;
						var eventOccation;
						if($event.srcElement.checked){
							for(eoKey in event.eventOccations){
								eventOccation = event.eventOccations[eoKey];
								if(!eventOccation.already_booked){
									var eventOccationId = parseInt(eventOccation.id);
									$scope.formdata.eventOccations[eventOccationId] = eventOccationId;
								}
							}
						}else{
							for(eoKey in event.eventOccations){
								eventOccation = event.eventOccations[eoKey];
								if(!eventOccation.already_booked){
									$scope.formdata.eventOccations[eventOccation.id] = false;
								}
							}
						}
					};
					
					$scope.allOccationsAreBooked = function(event){
						for(var eoKey in event.eventOccations){
							var eventOccation = event.eventOccations[eoKey];
							if(!eventOccation.already_booked){
								return false;
							}
						}
						return true;
					};
					
					Resource.getScheduleableEvents($scope.resourceId,function(response){
						
						$scope.events = response.data.data;
						
						for(var key in response.data.data){
							var event = response.data.data[key];
							for(var eoKey in event.eventOccations){
								var eventOccation = event.eventOccations[eoKey];
								var eventOccationId = parseInt(eventOccation.id);
								eventOccation.already_booked = eventOccation.already_booked === "1";
								eventOccation.ok_to_book = eventOccation.ok_to_book === "1";
								$scope.formdata.eventOccations[eventOccationId] = false;
							}
						}
					});
					
					ngDialog.open({
						template : $scope.dialogueTemplate,
						className: 'ngdialog-theme-default',
						scope : $scope
					});
				});
            },
			scope : {
				onSaveSelection : "&",
				resourceId : "="
			},
			replace : false,
            restrict : 'A'
        };
	})
	.filter('bookingLabel', function($filter){
        return function(booking){
			var label = booking.eventOccationBooking.eventOccation.event.name;
			if(booking.eventOccationBooking.eventOccation.name.length){
				label += " - " + booking.eventOccationBooking.eventOccation.name;
			}
			return label;
        };
    })
	.filter('simpleTimespanLabel', function($filter){
        return function(booking){
			var timespanFilter = $filter("timespanLabel");
            return timespanFilter({startsAt: {date : booking.startsAt}, endsAt : {date : booking.endsAt} });
        };
    });

}());