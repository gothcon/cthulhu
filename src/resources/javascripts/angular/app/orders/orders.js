(function(){
/*jshint strict:false */
"use strict";

angular.module("Orders",['GothCon','Filters','ngDialog','HttpDataProviders'])
	.directive("personalOrders", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,People){
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
                        $scope.orders = People.orders(newVal);
					}
					else{
						$scope.orders = [];
					}
				});
            },
			scope : {
				currentPerson : "=",
			},
			replace : true,
            templateUrl : "orders/templates/orders.tpl.html",
            restrict : 'AE'
        };
    });

}());