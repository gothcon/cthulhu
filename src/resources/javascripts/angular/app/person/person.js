(function(){
/*jshint strict:false */
"use strict";
	
angular.module("Person",['ngResource','GothCon','Filters','Signups','Sleeping','Orders'])
	.directive("people",function(){
		var extractIdRegex = /^\/person\/(\d+|new)(\/|$)/;
		return {
			templateUrl : 'person/templates/people.tpl.html',
			controller : function($scope,$location,gcTabsBus,People,gcUser,$compile,$element){

				function initialize(){
					$scope.currentUser = gcUser.current();
					$scope.disableSaveButton = false;
					$scope.overrideFormValidation = false;
					$scope.deletePerson = deletePerson;
					$scope.$on('$locationChangeSuccess',onLocationChange);
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
				}
			
				function onLocationChange(){

					$scope.deletePerson = angular.noop;
					$scope.savePerson = angular.noop;
					$scope.saveUser = angular.noop;

					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						// this is the "new" form
						onShowNewForm();
						return;
					}
					else{
						// this is the standard "view"
						var initialData = document.getElementById("initial-person-data");
						if(initialData && initialData.innerHTML.length > 0){
							$scope.person = JSON.parse(initialData.innerHTML);
							if($scope.person.user !== null){
								$scope.user = { username : $scope.person.user.username, level : $scope.person.user.level, isActive : $scope.person.user.isActive, password :"", password_repeat : ""};
							}

						}
						onShowPerson(id);
					}
				}

				function onUserChange(id){
					


				}

				function onShowList(){
					$scope.title = "Personlista";
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/new', 'label' : 'Ny person'}]);
					$scope.view = '<people-list people="people"></people-list>';
					$scope.people = People.all();
					return;
				}

				function onShowNewForm(){
					$scope.title = "Ny person";
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/new', 'label' : 'Ny person'}]);
					$scope.view =  '<people-form person="person"></people-form>';
					$scope.person = {};
					$scope.savePerson = function(){
						$scope.disableSaveButton = true;
						$scope.person = People.save($scope.person,function(response){
							var person = response.data;
							if(person.id > 0){
								$location.path("person/" + person.id);
							}else{
								$scope.disableSaveButton = false;
							}
						});
					};
				}

				function onShowPerson(id){
					
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'person/' + id + '/user', 'label' : 'Användaruppgifter'},
						{ id : 'person/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'},
						{ id : 'person/' + id + '/accommodation', 'label' : 'Sovning'},
						{ id : 'person/' + id + '/orders', 'label' : 'Förbeställningar'},
						{ id : 'person/' + id + '/arrival', 'label' : 'Ankomstregistrering'},
						{ id : 'person/' + id + '/delete', 'label' : 'Ta bort'}]);

					$scope.deletePerson = deletePerson;
					$scope.savePerson = savePerson;
					$scope.saveUser = saveUser;

					if($scope.person && "" + $scope.person.id === "" + id){
						$scope.title ="Detaljer för " + $scope.person.firstName + " " + $scope.person.lastName + " (" + $scope.person.id + ")";
					}
					else{	
						$scope.signups = undefined;
						$scope.password_repeat = "";
						$scope.disableSaveButton = true;
						$scope.user = { username : "", level : 0, isActive : false, password : "", password_repeat : ""};
						$scope.person = People.get(id,function(response){
							$scope.title ="Detaljer för " + $scope.person.firstName + " " + $scope.person.lastName + " (" + $scope.person.id + ")";
							$scope.disableSaveButton = false;
							if(response.user !== null){
								$scope.user = { username : $scope.person.user.username, level : $scope.person.user.level, isActive : $scope.person.user.isActive, password :"", password_repeat : ""};
							}
						});	
					}

					switch($location.path()){
						case "/person/" + id + "/signups":
							$scope.view = '<people-signups person="person"/>';
							break;
						case "/person/" + id + "/accommodation":
							$scope.view = '<people-accommodation person="person"/>';
							break;
						case "/person/" + id + "/orders":
							$scope.view = '<people-orders person="person"/>';
							break;
						case "/person/" + id + "/user":
							$scope.view = '<user person="person"/>';
							break;
						case "/person/" + id + "/delete":
							$scope.view = '<people-delete person="person"/>';
							break;                
						case "/person/" + id + "/arrival":
							$scope.view = '<people-arrival person="person"/>';
							break;
						default:
							$scope.view = '<people-form person="person"/>';
					}     
				}

				function savePerson(){
					$scope.disableSaveButton = true;
					People.save($scope.person,function(response){
						if(response.status === 200){
							$scope.person = response.data;
						}
						$scope.disableSaveButton = false;
					});
				}

				function saveUser(){

					$scope.disableSaveButton = true;
					People.saveUser($scope.person,$scope.user,function($response){

						$scope.disableSaveButton = false;
						// console.log($response); 
					});
				}

				function deletePerson(){
					if(confirm("Är du säker?")){
						People.remove($scope.person.id,function(){
							$scope.person = undefined;
							$location.path("person");
						});
					}
				}

				initialize();

			},
			restrict : 'E',
			replace : true
		};
	})
	.directive("peopleAccommodation",function(){
		return {
			templateUrl : 'person/templates/accommodation.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleArrival",function(){
		return {
			templateUrl : 'person/templates/arrival.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleDelete",function(){
		return {
			templateUrl : 'person/templates/delete.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleForm",function(){
		return {
			templateUrl : 'person/templates/form.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleList",function(){
		return {
			templateUrl : 'person/templates/list.tpl.html',
			controller : function($scope,$filter){
				
				
			},
			restrict : 'AE',
			scope : {
				people : "="
			}
		};
	})
	.directive("peopleOrders",function(){
		return {
			templateUrl : 'person/templates/orders.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleSignups",function(){
		return {
			templateUrl : 'person/templates/signups.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("user",function(){
		return {
			templateUrl : 'person/templates/user.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("personalGroupMemberships", function(urls){
        return {
            controller : function($scope,gcLoaderBus,ngDialog,Groups){

				$scope.deleteMembership = function(membership){
					if(confirm("Är du säker?")){
						
						Groups.deleteMembership(membership.id,function(response){
							
							$scope.memberships.splice($scope.memberships.indexOf(membership),1);
						});
					}
				};

				$scope.addMember = function(group){
					
					Groups.addMembership(group.id,$scope.currentUserId,function(response){
						
						$scope.memberships.push(response.data);
						$scope.availableGroups.splice($scope.availableGroups.indexOf(group),1);
					});
				};
				
				$scope.showMembershipDialogue = function(){
					
					Groups.getAvailableGroups($scope.currentUserId,function(response){
						$scope.availableGroups = response.data;
						
					});
					ngDialog.open({
					   template : urls.getDirectivesUrl() + 'groupMemberships/groupMembershipDialogue.tpl.html',
					   className: 'ngdialog-theme-default',
					   scope : $scope
					});
				};
			},
			scope : {
				memberships : "=",
				currentUserId: "="
			},
			replace : true,
            templateUrl : urls.getDirectivesUrl() + "groupMemberships/groupMemberships.tpl.html",
            restrict : 'AE'
        };
    })
	.provider("gcUser", function UserRepositoryProvider(){
        
        var _currentUser;
        return{
            $get : function UserRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
                var _resource = $resource( linkBuilder.createAdminLink("user/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'current' 
							: {method : 'GET',params : { id : "current" } , interceptor : gcLoaderBusInterceptor }});
                return {
                    current : function(callback){
						callback = callback || angular.noop;
						if(_currentUser === undefined){
                            _resource.current(callback);
                        }else{
                            callback(_currentUser);
                        }
						return _currentUser;
                    }
                };
            }
        };
    })
	.provider("People", function PeopleRepositoryProvider(){
        var _resource;
        var currentPerson;
		
        return{
            $get : function PeopleRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var standardPersonLink = linkBuilder.createAdminLink("person/");
				var searchLink = linkBuilder.createAdminLink("search/");
                _resource = $resource( standardPersonLink + ":id/:operation?json",{},{	
					'search': {method:'GET'		, interceptor : gcLoaderBusInterceptor, url : searchLink },
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'orders': {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : { operation : "orders" }  },
					'saveUser'
							: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : { operation : "user" } },
					'saveAccommodation' 
							: {method : 'POST',params : { operation : "saveAccommodation" } , interceptor : gcLoaderBusInterceptor },
					'availableAccommodations' 
							: { method: "GET", isArray : true, params : { operation : "availableAccommodations" }, interceptor : gcLoaderBusInterceptor }
                });
                return {
					availableAccommodations : function(personId,callback){
						callback = callback || angular.noop;
						return _resource.availableAccommodations({ id: personId},{}, callback);
					},
					
					remove : function(personId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id: personId},callback);
					},
					saveAccommodation : function(personId, accommodationResourceId,callback){
						callback = callback || angular.noop;
						return _resource.saveAccommodation({ id: personId},{ accommodationResourceId: accommodationResourceId},callback,callback);
					},
					
					saveUser : function(person,user,callback){
						callback = callback || angular.noop;
						return _resource.saveUser ({ id: person.id},  user, callback,callback);
                    },
					
					save : function(person,callback){
						callback = callback || angular.noop;
						var currentPerson = _resource.save({},person,callback,callback);
						return currentPerson;
                    },
                    
					all : function(callback){
                        callback = callback || angular.noop;
						return _resource.query(callback,callback);
                    },
					orders : function(personId,callback){
                        callback = callback || angular.noop;
						return _resource.orders({ id: personId},callback,callback);
                    },
                    
					get : function(id,callback){
						callback = callback || angular.noop;
	                    if(currentPerson === undefined || "" + currentPerson.id !== "" + id){
							currentPerson = _resource.get({id : id},callback); 
                        }else{
                            callback(currentPerson);
                        }
                        return currentPerson;
                    },
					search : function(queryString, searchScope, callback){
						callback = callback || angular.noop;
						if(!queryString || queryString.length < 3){
							return false;
						}else{
							return _resource.search({ q : queryString, r : searchScope },callback);
						}
					}
                };
            }
        };
    });
}());