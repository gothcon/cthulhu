(function(){
/*jshint strict:false */
"use strict";
angular.module("HttpDataProviders",['ngResource','GothCon'])
	.provider("Signups",function SignupsRepositoryProvider(){
			return {
			   $get : function(linkBuilder,$resource,gcLoaderBus,gcLoaderBusInterceptor){
				   var standardSignupLink = linkBuilder.createAdminLink("signup/");
				   var _resource = $resource( standardSignupLink + ":signupId/:operation/",{},{	
						'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
						'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
						'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'getSignupsByPerson' 
								: {method : 'GET', isArray:true, interceptor : gcLoaderBusInterceptor },
						'getSignupsByGroup' 
								: {method : 'GET', isArray:true, interceptor : gcLoaderBusInterceptor }});
				   return {
						getSignupsByPerson : function(id,callback){
							return _resource.getSignupsByPerson({ person_id: id}, callback || angular.noop);
						},
						getSignupsByGroup : function(id,callback){
							return _resource.getSignupsByGroup({ group_id: id}, callback || angular.noop);
						},
						savePersonalSignup : function(personId,signupSlotId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + "?person_id=:personId").save(
								{personId : personId},
								{signupSlotId : signupSlotId },
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						},
						saveGroupSignup : function(personId,signupSlotId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + "?group_id=:groupId").save(
								{groupId : personId},
								{signupSlotId : signupSlotId },
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						},
						deleteSignup : function(signupId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + ":signupId").delete(
								{signupId : signupId},
								{},
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						}
				   };
			   }
		   } ;
		})
	.provider("Groups", function GroupsRepositoryProvider(){
        var _resource;
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus, linkBuilder){
                
				var standardGroupMembershipLink = linkBuilder.createAdminLink("groupMembership/");
				var standardPersonLink = linkBuilder.createAdminLink("person/");
				
				_resource = $resource(standardPersonLink + ":id?json",{},{
                    all : { method : "GET", isArray : true}
                });

				return {
					deleteMembership : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardGroupMembershipLink + ":id",{}).delete({ id: id}, function internalCallback(response){
							gcLoaderBus.remove();
                            if(callback){
                                callback(response);
                            }
                        });
					},
					
                    getAvailableGroups : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardPersonLink + ":personId/availableGroups",{}).get({ personId: id}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					addMembership : function(groupid,personid,callback){
						gcLoaderBus.add();
						$resource(standardPersonLink + ":personId/addMembership/",{}).save(
							{personId : personid},
							{group_id : groupid},
							function internalCallback(response){
								gcLoaderBus.remove();
								if(callback){
									callback(response);
								}
						});
					}
                };
            }
        };
    })
	.provider("Resource", function ResourceRepositoryProvider(){
        return{
            $get : function ResourceRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var accommodationLink = linkBuilder.createAdminLink("sleepingSlotBooking/:id");
				var eventOccationLink = linkBuilder.createAdminLink("eventOccation/:id/:svc/:operation?format=json");
				var resourceBookingLink = linkBuilder.createAdminLink("resourceBooking/:id/:svc/:operation?format=json");
				var eventOccationBookingLink = linkBuilder.createAdminLink("eventOccationBooking/:id/?json");
                var _resource = $resource( linkBuilder.createAdminLink("resource/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} },
					'addAccommodations'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : {operation : 'accommodation'} },
					'deleteAccommodation'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor, url : accommodationLink},
					'setSleepingOccations'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : {operation : 'sleepingOccations'} },
					'getScheduleData'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, params : {operation : 'bookings'} },
					'getScheduleableEvents'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, params : {operation : 'availableEventOccations', svc : 'svc'} , url : resourceBookingLink },
					'addOccationBookings'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor, url: eventOccationBookingLink, params : { multiple : "true", id : "ny"}, headers : {'Content-Type': 'application/x-www-form-urlencoded'} },
					'deleteOccationBooking'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor, url: eventOccationBookingLink },
					'bookings'				: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params  : {operation : "bookings" } }
				});
                return {
					'delete' : function(id,callback){
						callback = callback || angular.noop;
                        return _resource.delete({id : id},callback,callback);
                    },
					'query' : function(callback){
						callback = callback || angular.noop;
                        return _resource.query(callback,callback);
                    },
					'types' : function(callback){
						callback = callback || angular.noop;
                        return _resource.types(callback,callback);
                    },
					'get' : function(id,callback){
						callback = callback || angular.noop;
                        return _resource.get({id : id},callback,callback);
                    },
					'save' : function(data,callback){
						callback = callback || angular.noop;
                        return _resource.save({ 'id' : data.id },data,callback,callback);
					},
					'setSleepingOccations' : function(resourceId,occationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.setSleepingOccations({ 'id' : resourceId },occationSelectionData,callback,callback);
					},
					'addAccommodations' : function(resourceId,accommodationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.addAccommodations({ 'id' : resourceId },accommodationSelectionData,callback,callback);
					},
					'deleteAccommodation' : function(sleepingSlotBookingId,accommodationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.deleteAccommodation({ 'id' : sleepingSlotBookingId },accommodationSelectionData,callback,callback);
					},
					'getScheduleData' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.getScheduleData({ 'id' : resourceId },callback,callback);
					},
					'getScheduleableEvents' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.getScheduleableEvents({ 'resourceId' : resourceId },callback,callback);
					},
					'addOccationBookings' : function(resourceId,data,callback){
						callback = callback || angular.noop;
                        return _resource.addOccationBookings({ 'resource_id' : resourceId },$.param({"eventOccation" : data}),callback,callback);
					},
					'deleteOccationBooking' : function(occationBookingId,callback){
						callback = callback || angular.noop;
                        return _resource.deleteOccationBooking({ 'id' : occationBookingId },callback,callback);
					},
					'bookings' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.bookings({ 'id' : resourceId },callback,callback);
					}
					
                };
            }
        };
    });
}());