angular.module('templates-angular_templates', ['directives/accommodation/accommodation.tpl.html', 'directives/groupMemberships/groupMembershipDialogue.tpl.html', 'directives/groupMemberships/groupMemberships.tpl.html', 'directives/peopleSelector/checkbox.tpl.html', 'directives/peopleSelector/radio.tpl.html', 'directives/peopleSelector/selectorDialogue.tpl.html', 'directives/signups/signupDialogue.tpl.html', 'directives/signups/signups.tpl.html', 'directives/tabs/tabs.tpl.html', 'event/templates/details.tpl.html', 'event/templates/event.tpl.html', 'event/templates/eventBookingsSchedule.tpl.html', 'event/templates/eventOccation.tpl.html', 'event/templates/eventOccationDialogue.tpl.html', 'event/templates/eventOccationForm.tpl.html', 'event/templates/eventOccationSchedule.tpl.html', 'event/templates/list.tpl.html', 'event/templates/signupSlot.tpl.html', 'event/templates/signupSlotForm.tpl.html', 'event/templates/signups.tpl.html', 'eventtypes/templates/list.tpl.html', 'group/templates/details.tpl.html', 'group/templates/group.tpl.html', 'group/templates/list.tpl.html', 'group/templates/members_and_signups.tpl.html', 'orders/templates/orders.tpl.html', 'person/templates/accommodation.tpl.html', 'person/templates/arrival.tpl.html', 'person/templates/delete.tpl.html', 'person/templates/form.tpl.html', 'person/templates/list.tpl.html', 'person/templates/orders.tpl.html', 'person/templates/people.tpl.html', 'person/templates/signups.tpl.html', 'person/templates/user.tpl.html', 'resource/templates/accommodation.tpl.html', 'resource/templates/bookingoverview.tpl.html', 'resource/templates/bookingschedule.tpl.html', 'resource/templates/bookingslist.tpl.html', 'resource/templates/delete.tpl.html', 'resource/templates/eventOccationSelector/bookingschedule.tpl.html', 'resource/templates/eventOccationSelector/checkbox.tpl.html', 'resource/templates/eventOccationSelector/radio.tpl.html', 'resource/templates/eventOccationSelector/selectorDialogue.tpl.html', 'resource/templates/form.tpl.html', 'resource/templates/list.tpl.html', 'resource/templates/resource.tpl.html', 'timespan/templates/form.tpl.html', 'timespan/templates/list.tpl.html', 'timespan/templates/timespan.tpl.html']);

angular.module("directives/accommodation/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/accommodation/accommodation.tpl.html",
    "<div ng-form>\n" +
    "	<h4>Sovning</h4>\n" +
    "	<ul class=\"sleepingResources\">\n" +
    "		<li>\n" +
    "			<h6><input type=\"radio\" name=\"accommodationId\" value=\"-1\" ng-model=\"selectedSleepingResourceId\"> Sover inte på konventet</h6>\n" +
    "		</li>\n" +
    "		<li ng-repeat=\"accommodationResource in availableAccommodations\">\n" +
    "			<h6><input type=\"radio\" name=\"accommodationId\" value=\"{{accommodationResource.sleepingResourceId}}\" ng-model=\"selectedSleepingResourceId\" ng-click=\"selectAccommodation(accommodationResource.sleepingResourceId)\"> {{accommodationResource.name}}</h6>\n" +
    "			<ul>\n" +
    "				<li ng-repeat=\"timespan in accommodationResource.eventOccations\">{{timespan|timespanLabel:true}}</li>\n" +
    "			</ul>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<button ng-click=\"saveAccommodation()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("directives/groupMemberships/groupMembershipDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/groupMemberships/groupMembershipDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h4>Gruppmedlemskap</h4>\n" +
    "	<table id=\"groupMembershipTable\">\n" +
    "		<tr class=\"odd\">\n" +
    "			<th>Id</th>\n" +
    "			<th>Namn</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"group in availableGroups\" ng-class-even=\"even\" ng-class-odd=\"odd\" >\n" +
    "			<td>{{group.id}}</td>\n" +
    "			<td>{{group.name}}</td>\n" +
    "			<td><button ng-click=\"addMember(group)\">Lägg till medlemskap</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "</div>");
}]);

angular.module("directives/groupMemberships/groupMemberships.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/groupMemberships/groupMemberships.tpl.html",
    "<div>\n" +
    "	<h4>Gruppmedlemskap</h4>\n" +
    "	<table id=\"groupMembershipTable\">\n" +
    "		<tr class=\"odd\">\n" +
    "			<th>Id</th>\n" +
    "			<th>Namn</th>\n" +
    "			<th>Noteringar</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"membership in memberships\">\n" +
    "			<td>{{membership.id}}</td>\n" +
    "			<td>{{membership.group.name}}</td>\n" +
    "			<td>{{membership|membershipStatus}}</td>\n" +
    "			<td><button ng-click=\"deleteMembership(membership)\">Avsluta medlemskap</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button ng-click=\"showMembershipDialogue()\">Nytt medlemskap</button>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("directives/peopleSelector/checkbox.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/checkbox.tpl.html",
    "<input type=\"checkbox\" ng-click=\"handleClick($event,entity)\"/>{{entity|searchLabel}}");
}]);

angular.module("directives/peopleSelector/radio.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/radio.tpl.html",
    "<input type=\"radio\" name=\"person\" ng-click=\"handleClick($event,entity)\"/>{{entity|searchLabel}}");
}]);

angular.module("directives/peopleSelector/selectorDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/selectorDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h1>{{dialogHeader}}</h1>\n" +
    "	<div class=\"search-container\">\n" +
    "		<gc-search search-result=\"entities\" selection-scope=\"selectionScope\" search-status=\"searchStatus\"/></gc-search>\n" +
    "	</div>\n" +
    "	<div class=\"people-list-container\">\n" +
    "	<p>{{searchStatus}}</p>\n" +
    "	<ul class=\"people-list\" ng-hide=\"noResults\">\n" +
    " 		<li ng-repeat=\"entity in entities track by entity.type + entity.id\" id=\"multiSelection\">\n" +
    "			<label ng-include=\"listItemContent\"></label>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	</div>\n" +
    "	<button class=\"save-selection\" type=\"button\" ng-click=\"saveSelection() || closeThisDialog()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("directives/signups/signupDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/signups/signupDialogue.tpl.html",
    "<div class=\"dialog-contents\" style=\"\">\n" +
    "    <ul class=\"eventtypes\">\n" +
    "        <li ng-repeat=\"eventType in eventtypes\">\n" +
    "            <h1>{{eventType.name}}</h1>\n" +
    "            <ul class=\"events\">\n" +
    "                <li ng-repeat=\"event in eventType.events\">\n" +
    "                    <h2>{{event.name}}</h2>\n" +
    "                    <ul class=\"eventoccations\">\n" +
    "                        <li ng-repeat=\"eventOccation in event.eventOccations\">\n" +
    "                            <h3>{{eventOccation|eventOccationLabel}}</h3>\n" +
    "                            <ul class=\"signupslots\">\n" +
    "                                <li ng-repeat=\"signupSlot in eventOccation.signupSlots\" ng-controller=\"SignupSlotController\">\n" +
    "                                    {{signupSlot.slotType.name}}\n" +
    "                                    <button  ng-click=\"saveSignup(signupSlot)\" ng-disabled=\"signupButtonIsDisabled\" class=\"signupButton\" >Anmäl</button>\n" +
    "                                    <span class=\"clear\"></span>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>");
}]);

angular.module("directives/signups/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/signups/signups.tpl.html",
    "<div>\n" +
    "	<h4>Signups</h4>\n" +
    "	<table class=\"signupList\">\n" +
    "		<tr>\n" +
    "			<th>När</th>\n" +
    "			<th>Vad</th>\n" +
    "			<th></th>\n" +
    "			<th>Noteringar</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"signup in signups\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td>{{signup.signupSlot.eventOccation.timespan|timespanLabel}}</td>\n" +
    "			<td><a href=\"{{signup.signupSlot.eventOccation.event.id|eventLink}}/signups\" target=\"_self\">{{signup.signupSlot.eventOccation|occationLabel}}</a></td>\n" +
    "			<td></td>\n" +
    "			<td class=\"noteCol\">{{signup|signupStatus}}</td>	\n" +
    "			<td><button ng-click=\"deleteSignup(signup)\">Ta bort</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button ng-click=\"showSignupDialogue()\">Ny anmälning</button>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("directives/tabs/tabs.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/tabs/tabs.tpl.html",
    "<div class=\"tabs\">\n" +
    "    <a ng-repeat=\"tab in tabs\" ng-class=\"{'tab' : true, 'active' : isTabActive(tab) }\" ng-href=\"{{tab.id}}\">{{tab.label}}<span class=\"tabEnd\"></span></a>\n" +
    "</div>\n" +
    "\n" +
    " ");
}]);

angular.module("event/templates/details.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/details.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"form\" name=\"groupform\">\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<label>Namn <input type=\"text\" name=\"name\" ng-model=\"currentEvent.name\" required=\"required\"/><span ng-show=\"currentEvent.name.$error.required\">*</span></label>\n" +
    "			<label>Typ <select ng-model=\"currentEvent.eventtype\" ng-options='t.name for t in eventTypes track by t.id'></select></label>\n" +
    "			<label>Arrangör \n" +
    "				<span class='buttonBox' >\n" +
    "					<a ng-show=\"hasOrganizer()\" href=\"{{currentEvent.organizer|eventOrganizerLink}}\" target=\"_self\">{{currentEvent.organizer|eventOrganizerLabel}}</a>\n" +
    "					<button type=\"button\" gc-selector dialog-header=\"'Sök upp arrangör'\" selection-scope=\"'people,groups'\" on-save-selection=\"selectOrganizer(selection)\">{{selectOrganizerLabel}}</button>\n" +
    "				</span>\n" +
    "			</label>\n" +
    "			<label>Namn i schemat<input type=\"text\" name=\"name\" ng-model=\"currentEvent.scheduleName\" required=\"required\"/></label>\n" +
    "			<label>Synlig i schemat?\n" +
    "				<span class=\"buttonBox\">\n" +
    "					<input type=\"radio\" name=\"visibleInSchedule\" ng-model=\"currentEvent.visibleInSchedule\" ng-value=\"true\"/> Ja\n" +
    "					<input type=\"radio\" name=\"visibleInSchedule\" ng-model=\"currentEvent.visibleInSchedule\" ng-value=\"false\"/> Nej\n" +
    "				</span>\n" +
    "			</label>\n" +
    "			<label>Publik?\n" +
    "				<span class=\"buttonBox\">\n" +
    "					<input type=\"radio\" name=\"visibleInPublicListing\" ng-model=\"currentEvent.visibleInPublicListing\" ng-value=\"true\"/> Ja\n" +
    "					<input type=\"radio\" name=\"visibleInPublicListing\" ng-model=\"currentEvent.visibleInPublicListing\" ng-value=\"false\"/> Nej\n" +
    "				</span>\n" +
    "			</label>\n" +
    "            <label>Intern information <textarea class=\"large\" ng-model=\"currentEvent.internalInfo\"></textarea></label>\n" +
    "	        <button type=\"submit\" ng-click=\"save()\" ng-disabled=\"groupform.$invalid\" ng-class=\"{'disabled' : groupform.$invalid }\"><i class=\"fa fa-save\"></i> {{currentEvent.id ? 'spara':'skapa'}}</button>\n" +
    "	        <span class=\"clear\"></span>\n" +
    "        </fieldset>\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "            <label>Arrangemangsinformation - Ingress <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.preamble\"></textarea></label>	\n" +
    "            <label>Arrangemangsinformation - Text <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.text\"></textarea></label>	\n" +
    "            <label>Arrangemangsinformation - Övrigt <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.info\"></textarea></label>\n" +
    "		</fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "    </form>\n" +
    "</div>");
}]);

angular.module("event/templates/event.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/event.tpl.html",
    "<div class=\"event\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("event/templates/eventBookingsSchedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventBookingsSchedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='event in events'>\n" +
    "			<h4>{{event.name}}</h4>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"resource in event.resources\">\n" +
    "					<div class=\"eventName\"><a href=\"{{resource.id|resourceLink}}\">{{resource.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"booking in resource.bookings\" \n" +
    "							ng-click=\"deleteBooking($event,resource,booking)\"\n" +
    "							style=\"width:{{booking.width}}px; left:{{booking.offset}}px\">{{booking.label}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccation.tpl.html",
    "<div class=\"event-occation\">\n" +
    "	<h4>{{eventOccation|eventOccationLabel}}<a href=\"#\" class=\"edit-link\" ng-click=\"showEditForm = !showEditForm\"><i class=\"fa fa-edit\"></i></a></h4>\n" +
    "	<span class=\"event-occation-info\">{{eventOccation|eventOccationInfo}}</span>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<event-occation-form ng-show=\"showEditForm\" event-occation=\"eventOccation\" timespans=\"timespans\"></event-occation-form>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<div class=\"signup-slots\">\n" +
    "		<signup-slot ng-repeat=\"signupSlot in eventOccation.signupSlots\" class=\"signup-slot\" signup-slot=\"signupSlot\" slot-types=\"slotTypes\"></signup-slot><div class=\"resource-bookings\">\n" +
    "			<h5>Plats och resursbokningar</h5>\n" +
    "			<table class=\"signup-slots\">\n" +
    "				<tr><th>Var</th><th>Typ</th><th></th></tr>\n" +
    "				<tr class=\"resourceBookingsContainer\" ng-repeat=\"eventOccationBooking in eventOccation.eventOccationBookings track by eventOccationBooking.id\" >\n" +
    "					<td><a href=\"{{eventOccationBooking.booking.resource|resourceLink}}/schedule\" target=\"_self\">{{eventOccationBooking.booking.resource.name}}</a></td>\n" +
    "					<td>{{eventOccationBooking.booking.resource.resourceType.name}}</td>\n" +
    "					<td class=\"delete-button-col\"><button type='button' ng-click=\"deleteResourceBooking(eventOccationBooking)\"><i class=\"fa fa-times\"></i></button></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "			<button type='button' multi-select=\"true\" gc-selector dialog-header=\"'Sök efter plats eller resurs'\" selection-scope=\"'resources'\" on-save-selection=\"saveResourceBooking(selection)\"><i class=\"fa fa-plus\"></i> Ny bokning</button>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<button ng-hide=\"showNewForm\" type='button' ng-click=\"showNewForm = !showNewForm\"><i class=\"fa fa-plus\"></i> Ny anmälningstyp</button>\n" +
    "	<signup-slot-form ng-show=\"showNewForm\" slot-types=\"slotTypes\" signup-slot=\"signupSlotFormData\"></signup-slot-form>\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationDialogue.tpl.html",
    "<style>\n" +
    "	.eventOccationDialogue label{\n" +
    "		display:inline-block;\n" +
    "		float:none;\n" +
    "	}\n" +
    "	.eventOccationDialogue *{\n" +
    "		float:none;\n" +
    "	}\n" +
    "	\n" +
    "	\n" +
    "</style>\n" +
    "<div class=\"eventOccationDialogue\">\n" +
    "	<h3>Nytt pass</h3>\n" +
    "	<h4>{{event.name}} - {{timespan.name}}</h4>\n" +
    "	<label>\n" +
    "		Passnamn<br/><input type=\"text\"/>\n" +
    "	</label>\n" +
    "	<label>\n" +
    "		Dölj i listningen<br/><input type=\"checkbox\"/>\n" +
    "	</label>\n" +
    "	\n" +
    "	\n" +
    "	\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationForm.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationForm.tpl.html",
    "<div ng-form=\"eventOccationForm\" class=\"event-occation-form\">\n" +
    "	<label>Name <input type=\"text\" ng-model=\"eventOccation.name\" /></label>\n" +
    "	<label>Timespan <select required ng-model=\"eventOccation.timespan\" ng-options=\"timespan|timespanLabel:true for timespan in localTimespans track by timespan.id\"></select></label>\n" +
    "	<fieldset ng-hide=\"eventOccation.timespan.isPublic\">\n" +
    "		<label>Name <input type=\"text\" ng-mode=\"name\" /></label>\n" +
    "		<label>Börjar <input type=\"datetime-local\" ng-model=\"startsAt\"></label>\n" +
    "		<label>Slutar <input type=\"datetime-local\" ng-model=\"endsAt\"></label>\n" +
    "		<fieldset>\n" +
    "		Visa i schemat?\n" +
    "			<div class=\"input-box\">\n" +
    "				<label><input ng-model=\"showInSchedule\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "				<label><input ng-model=\"showInSchedule\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "			</div>\n" +
    "		</fieldset>\n" +
    "	</fieldset>\n" +
    "	<fieldset>\n" +
    "		Dold i listning?\n" +
    "		<div class=\"input-box\">\n" +
    "			<label><input ng-model=\"eventOccation.isHidden\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "			<label><input ng-model=\"eventOccation.isHidden\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<div style=\"display:table\">\n" +
    "		<div style=\"display: table-row\">\n" +
    "			<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"eventOccationForm.$pristine || eventOccationForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{SaveLabel}}</button></div>\n" +
    "			<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "			<div style=\"display: table-cell; width:50%\"><button type=\"button\" class=\"cancel-button right\" ng-click='onCancelClick()'><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<span class=\"clear\"></span>	\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationSchedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationSchedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteEventOccation($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("event/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"event-list\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"events by id\">ID</th>\n" +
    "            <th sort-on-click=\"events by name\">Namn</th>\n" +
    "            <th sort-on-click=\"events by eventtype.name,name\">Arrangemangstyp</th>\n" +
    "            <th>Synligt för besökare</th>\n" +
    "            <th>Synligt i schemat</th>\n" +
    "            <th>Arrangör</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"event in events\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td><a href=\"event/{{event.id}}\">{{event.id}}</a></td>\n" +
    "			<td><a href=\"event/{{event.id}}\">{{event.name}}</td>\n" +
    "			<td><a href=\"{{event.eventtype.id|eventTypeLink}}\" target=\"_self\">{{event.eventtype.name}}</td>\n" +
    "			<td>{{event.visibleInPublicListing|yesNoStatus}}</td>\n" +
    "			<td>{{event.visibleInSchedule|yesNoStatus}}</td>\n" +
    "			<td><a href=\"{{event.organizer|eventOrganizerLink}}\" target=\"_self\">{{event.organizer|eventOrganizerLabel}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("event/templates/signupSlot.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signupSlot.tpl.html",
    "<div class=\"signup-slot\">\n" +
    "	<h5>{{signupSlot.slotType.name}}<a href=\"#\" class=\"edit-link\" ng-click=\"showEditForm = !showEditForm\"><i class=\"fa fa-edit\"></i></a></h5>\n" +
    "	<span class=\"signup-slot-info\">{{signupSlot|signupSlotInfo}}</span>\n" +
    "	<signup-slot-form ng-show=\"showEditForm\" signup-slot=\"signupSlot\" slot-types=\"slotTypes\"></signup-slot-form>\n" +
    "	<table>\n" +
    "		<tr>\n" +
    "			<th class=\"nameCol\">Vem</th><th class=\"approvalCol\">Godkänd</th><th class=\"approvalCol\">Har betalt</th><th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"signup in signupSlot.signups\">\n" +
    "			<td><span ng-show=\"signup.group\"><a href=\"{{signup.group.id|groupLink}}\" target=\"_self\">{{signup.group|groupLabel}}</a></span>\n" +
    "				<span ng-show=\"signup.person\"><a href=\"{{signup.person.id|personLink}}\" target=\"_self\">{{signup.person|personLabel:noId}}</a></span>\n" +
    "			</td>\n" +
    "			<td>{{signup.isApproved|yesNoStatus:signupSlot.requiresApproval}}</td>\n" +
    "			<td>{{signup.isPaid|yesNoStatus:signupSlot.signupSlotArticle}}</td>\n" +
    "			<td class=\"delete-button-col\"><button type='button' ng-click=\"deleteSignup(signup)\"><i class=\"fa fa-user-times\"></i></button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button type='button' gc-selector dialog-header=\"selectorHeader\" selection-scope=\"selectionScope\" on-save-selection=\"onSelectEntity(selection)\"><i class=\"fa fa-user-plus\"></i></button>	\n" +
    "</div>");
}]);

angular.module("event/templates/signupSlotForm.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signupSlotForm.tpl.html",
    "<div ng-form=\"signupSlotForm\" class=\"signup-slot-form\">\n" +
    "	<label>Anmälningstyp <select required ng-model=\"signupSlot.slotType\" ng-options=\"slotType.name for slotType in slotTypes track by slotType.id\"></select></label>\n" +
    "	<label>Begränsat antal platser <input type=\"number\" ng-model=\"signupSlot.maximumSignupCount\"/></label>\n" +
    "	<label>Begränsat antal reservplatser <input type=\"number\" ng-model=\"signupSlot.maximumSpareSignupCount\"/></label>\n" +
    "	<fieldset>Kräver godkännande? \n" +
    "		<div class=\"input-box\">\n" +
    "			<label>Ja <input type=\"radio\" ng-model=\"signupSlot.requiresApproval\" ng-value=\"true\"/></label>\n" +
    "			<label>Nej <input type=\"radio\" ng-model=\"signupSlot.requiresApproval\" ng-value=\"false\"/></label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	<fieldset>Dold?\n" +
    "		<div class=\"input-box\">\n" +
    "			<label>Ja <input type=\"radio\" ng-model=\"signupSlot.isHidden\" ng-value=\"true\"/></label>\n" +
    "			<label>Nej <input type=\"radio\" ng-model=\"signupSlot.isHidden\" ng-value=\"false\"/></label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	<div style=\"display: table-row\">\n" +
    "		<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"signupSlotForm.$pristine || signupSlotForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{saveLabel}}</button></div>\n" +
    "		<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "		<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onCancelClick()\" class=\"cancel-button right\"><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "	</div>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("event/templates/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signups.tpl.html",
    "<div class=\"sectionContent event-occation-view\">\n" +
    "	<h3 class=\"event-occation-header\">Arrangmangspass</h3>\n" +
    "	<button type='button' ng-hide=\"showNewForm\" ng-click=\"showNewForm = true\"><i class=\"fa fa-plus\"></i> Nytt pass</button>\n" +
    "	<div ng-show=\"showNewForm\">\n" +
    "		<h4>Nytt arrangemangspass</h4>\n" +
    "		<event-occation-form timespans=\"timespans\"/>\n" +
    "	</div>\n" +
    "	<div class=\"event-occations\">\n" +
    "		<event-occation ng-repeat=\"eventOccation in currentEvent.eventOccations\" event-occation=\"eventOccation\" timespans=\"timespans\" slot-types=\"slotTypes\"></event-occation>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("eventtypes/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("eventtypes/templates/list.tpl.html",
    "<div class=\"event-type\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"sectionContent content\">\n" +
    "		<table class=\"event-type-list\" sortable-collection>\n" +
    "			<tr class=\"even\">\n" +
    "				<th class=\"nameCol\" sort-on-click=\"eventtypes by name\">Name</th>\n" +
    "			</tr>\n" +
    "			<tr ng-repeat=\"eventtype in eventtypes\" ng-controller=\"eventTypeItemController\">\n" +
    "				<td>\n" +
    "					<a ng-hide='inEditMode' href=\"#\" ng-click=\"$event.preventDefault();enterEditMode();\">{{eventtype.name}} <i class=\"fa fa-edit\"></i></a>\n" +
    "					<span ng-show=\"inEditMode\"><input type=\"text\" ng-model=\"eventtype.name\" />\n" +
    "						<button title=\"spara\" type=\"button\" href=\"#\" ng-click=\"save();\"><i class=\"fa fa-save\"></i></button> \n" +
    "						<button title=\"avbryt\" type=\"button\" href=\"#\" ng-click=\"cancel();\"><i class=\"fa fa-ban\"></i></button>\n" +
    "						<button title=\"ta bort\" type=\"button\" href=\"#\" ng-click=\"remove();\"><i class=\"fa fa-times\"></i></button>\n" +
    "					</span>\n" +
    "				</td>\n" +
    "			</tr>\n" +
    "			<tr ng-controller=\"eventTypeItemController\" ng-init=\"{eventtype : { name: '', id : 0 }}\">\n" +
    "				<td>\n" +
    "					<h3>Skapa ny arrangemangstyp</h3>\n" +
    "					<span><input type=\"text\" ng-model=\"eventtype.name\" />\n" +
    "						<button title=\"skapa\" type=\"button\" href=\"#\" ng-click=\"create();\"><i class=\"fa fa-save\"></i></button> \n" +
    "					</span>\n" +
    "				</td>\n" +
    "			</tr>\n" +
    "		</table>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("group/templates/details.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/details.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"form\" name=\"groupform\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Namn <input type=\"text\" name=\"name\" ng-model=\"currentGroup.name\" required=\"required\"/><span ng-show=\"currentGroup.name.$error.required\">*</span></label>\n" +
    "            <label>Epostadress <input type=\"email\" name=\"email_address\" ng-model=\"currentGroup.emailAddress\" /></label>\n" +
    "            <label>Gruppledare <select ng-model=\"currentGroup.leaderMembershipId\" ng-options=\"value.id as value.person.firstName +' ' + value.person.lastName + ' ('+ value.person.id + ')' for value in currentGroup.groupMembers\"></select></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "             <label>Övriga noteringar <textarea class=\"large\" ng-model=\"currentGroup.note\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Intern information <textarea class=\"large\" ng-model=\"currentGroup.internal_note\"></textarea></label>	\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"save()\" ng-disabled=\"groupform.$invalid\" ng-class=\"{'disabled' : groupform.$invalid }\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "	<pre style=\"display:none\">{{currentGroup|json}}</pre>\n" +
    "</div>");
}]);

angular.module("group/templates/group.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/group.tpl.html",
    "<div class=\"group\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("group/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"personList\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"groups by id\">ID</th>\n" +
    "            <th sort-on-click=\"groups by name\">Gruppnamn</th>\n" +
    "            <th sort-on-click=\"groups by emailAddress\">Epostadress</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"group in groups\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.id}}</a></td>\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.name}}</a></td>\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.emailAddress}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("group/templates/members_and_signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/members_and_signups.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "	<h3>Medlemmar</h3>\n" +
    "	<table id=\"personList\">\n" +
    "		<tr>\n" +
    "			<th class=\"idColumn\">ID</th>\n" +
    "			<th>Förnamn</th>\n" +
    "			<th>Efternamn</th>\n" +
    "			<th>Epostadress</th>\n" +
    "			<th>Gruppledare</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"membership in currentGroup.groupMembers\">\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.id}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.firstName}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.lastName}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.emailAddress}}</a></td>\n" +
    "			<td><span ng-show=\"membership.id == currentGroup.leaderMembershipId\">Gruppledare</span></td>\n" +
    "			<td><button type=\"button\" ng-click=\"deleteMembership(membership)\">Ta bort medlem</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button type=\"button\" gc-selector on-save-selection=\"addMembers(selection)\">Lägg till medlemmar</button>\n" +
    "\n" +
    "	<h3>Anmälningar</h3>\n" +
    "	<signups current-group=\"currentGroup\"/>\n" +
    "</div>");
}]);

angular.module("orders/templates/orders.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("orders/templates/orders.tpl.html",
    "<div>\n" +
    "	<table class=\"resourceList\">\n" +
    "		<tr class=\"even\">\n" +
    "			<th class=\"idCol\">Id</th>\n" +
    "			<th>Datum</th>\n" +
    "			<th class=\"typeCol\">Ordertotal</th>\n" +
    "			<th>Orderstatus</th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"order in orders\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.id}}</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.submittedAt.date|amDateFormat:'YYYY-MM-DD HH:mm'}}</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.total}}:-</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.status|orderStatus}}</a></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<a href=\"{{currentPerson.id|newOrderLink}}\" target=\"_self\">Ny förbeställning</a>\n" +
    "</div>");
}]);

angular.module("person/templates/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/accommodation.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<accommodation-selection class=\"sectionBox\" current-person=\"person\"></accommodation-selection>\n" +
    " </div>");
}]);

angular.module("person/templates/arrival.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/arrival.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "			<a ng-href=\"{{person.id|registrationLink}}\" target=\"_self\"><button type=\"button\">Till ankomstregistrering</button></a>\n" +
    "        </div>");
}]);

angular.module("person/templates/delete.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/delete.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "			<button ng-click=\"deletePerson()\" type=\"button\">Ta bort</button>\n" +
    "        </div>");
}]);

angular.module("person/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"person-form\" name=\"form\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Förnamn <input type=\"text\" name=\"firstName\" ng-model=\"person.firstName\" required=\"required\"/><span ng-show=\"form.firstName.$error.required\">*</span></label>\n" +
    "            <label>Efternamn <input type=\"text\" name=\"lastName\" ng-model=\"person.lastName\" required=\"required\"/><span ng-show=\"form.lastName.$error.required\">*</span></label>\n" +
    "            <label>Epostadress <input type=\"email\" name=\"emailAddress\" ng-model=\"person.emailAddress\" required=\"required\"/><span ng-show=\"form.emailAddress.$error.required\">*</span></label>\n" +
    "            <label>Privat epostadress? \n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"emailIsPublic\" ng-model=\"person.emailIsPublic\" ng-value=\"false\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"emailIsPublic\" ng-model=\"person.emailIsPublic\" ng-value=\"true\"/> Nej\n" +
    "                </span>\n" +
    "            </label>\n" +
    "            <label>Personnummer <input type=\"text\" ng-model=\"person.identification\" ng-pattern=\"/^[0-9]{6}( |-)?[0-9]{4}$/\"/></label>\n" +
    "            <label>Telefonnummer <input type=\"text\" ng-model=\"person.phoneNumber\"/></label>\n" +
    "            <label>Gatuadress <input type=\"text\" ng-model=\"person.streetAddress\"/></label>\n" +
    "            <label>Postnummer <input type=\"text\" ng-model=\"person.zip\"/></label>\n" +
    "            <label>Stad <input type=\"text\" ng-model=\"person.city\"/></label>\n" +
    "            <label>Utländsk adress <textarea rows=\"3\" ng-model=\"person.foreignAddress\"></textarea></label>\n" +
    "            <label>Ignorera formulärfel  <span class=\"buttonBox\"><input type=\"checkbox\" name=\"override\" value=\"override\" ng-model=\"override\"/></span></label>\n" +
    "       </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "             <label>Övriga noteringar <textarea class=\"large\" ng-model=\"person.note\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label for=\"internal_note\">Intern information <textarea class=\"large\" ng-model=\"person.internalNote\"></textarea></label>	\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"savePerson()\" ng-disabled=\"!(form.override.$modelValue || form.$valid) || disableSaveButton\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "    <script>\n" +
    "        $(\"#person-form input[type=radio]\").each(function(key,obj){\n" +
    "            var $obj = $(obj);\n" +
    "            var $parent =$obj.parent(\".buttonBox\");\n" +
    "            $obj.bind(\"focus\",function(){\n" +
    "                $parent.addClass(\"focus\");\n" +
    "            }).bind(\"blur\",function(){\n" +
    "                $parent.removeClass(\"focus\");\n" +
    "            });\n" +
    "        });\n" +
    "    </script>\n" +
    "</div>");
}]);

angular.module("person/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"personList\" sortable-collection >\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"people by id\">ID</th>\n" +
    "            <th sort-on-click=\"people by firstName,lastName\">Förnamn</th>\n" +
    "            <th sort-on-click=\"people by lastName,firstName\">Efternamn</th>\n" +
    "            <th sort-on-click=\"people by emailAddress\">Epostadress</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"person in people\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.id}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.firstName}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.lastName}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.emailAddress}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("person/templates/orders.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/orders.tpl.html",
    "<div  class=\"sectionContent\" >\n" +
    "	<personal-orders class=\"sectionBox\" current-person=\"person\"></personal-orders>\n" +
    "</div>");
}]);

angular.module("person/templates/people.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/people.tpl.html",
    "<div class=\"people\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("person/templates/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/signups.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<signups class=\"sectionBox\" current-person=\"person\"></signups>\n" +
    "	<personal-group-memberships class=\"sectionBox\" memberships=\"person.groupMemberships\" current-user-id=\"person.id\"></group-memberships>\n" +
    "</div>");
}]);

angular.module("person/templates/user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/user.tpl.html",
    "<div class=\"sectionContent\" >\n" +
    "    <form name=\"userForm\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Användarnamn <input type=\"text\" ng-model=\"user.username\"/></label>\n" +
    "            <label>Lösenord <input type=\"password\" ng-model=\"user.password\" name=\"password\" /></label>\n" +
    "            <label>Upprepa lösenord <input type=\"password\" ng-model=\"user.password_repeat\" name=\"password_repeat\" gc-must-match=\"password\"/></label>\n" +
    "            <span class=\"clear\"></span>\n" +
    "            <label>Aktiv? <span class=\"buttonBox\"><input type=\"checkbox\" ng-model=\"user.isActive\"/></span></label>\n" +
    "            <label>Användarnivå \n" +
    "                <select ng-model=\"user.level\">\n" +
    "                    <option value=\"0\">Besökare</option>\n" +
    "                    <option value=\"1\">Användare</option>\n" +
    "                    <option value=\"20\">Arrangör</option>\n" +
    "                    <option value=\"30\">Arbetare</option>\n" +
    "                    <option value=\"40\">Ledning</option>\n" +
    "                    <option value=\"100\">Administratör</option>\n" +
    "                </select>\n" +
    "            </label>\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <button ng-click=\"saveUser($event)\" ng-disabled=\"!userForm.$valid || disableSaveButton\">Skicka</button>\n" +
    "    </form>\n" +
    "</div>");
}]);

angular.module("resource/templates/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/accommodation.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<h3>Sovplatser och sovplatsbokningar</h3>\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "	<fieldset class=\"formBox\">\n" +
    "		<h4>Inbokade sovtillfällen</h4>\n" +
    "		<table>\n" +
    "			<thead>\n" +
    "				<tr>\n" +
    "					<th>Inbokad</th>\n" +
    "					<th>När</th>\n" +
    "				</tr>\n" +
    "			</thead>\n" +
    "			<tbody>\n" +
    "				<tr ng-repeat=\"occation in sleepingEventOccations\">\n" +
    "					<td><input type=\"checkbox\" value=\"{{occation.id}}\" ng-checked=\"occation.selected\" ng-click=\"updateOccationSelection($event,occation.id)\"/></td>\n" +
    "					<td>{{occation.timespan.name}} - ({{occation.timespan|timespanLabel}})</td>\n" +
    "				</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "		<button type=\"button\" ng-click=\"updateOccations()\">Uppdatera</button>\n" +
    "	</fieldset>\n" +
    "	<fieldset class=\"formBox\">\n" +
    "		<div  class=\"sleeperListContainer\" style=\"width:400px; float:left; margin-left:10px;\">\n" +
    "			<h4>Inbokade sovande</h4>\n" +
    "			<table>\n" +
    "				<colgroup>\n" +
    "					<col class=\"idCol\" />\n" +
    "					<col class=\"firstNameCol\"/>\n" +
    "					<col class=\"lastNameCol\"/>\n" +
    "					<col class=\"buttonsCol\"/>\n" +
    "				</colgroup>\n" +
    "				<tr class=\"even\">\n" +
    "					<th>Id</th>\n" +
    "					<th>Förnamn</th>\n" +
    "					<th>Efternamn</th>\n" +
    "					<th></th>\n" +
    "				</tr>\n" +
    "				<tr ng-repeat=\"accommodation in resource.sleepingResource.sleepingSlotBookings\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.id}}</a></td>\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.firstName}}</a></td>\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.lastName}}</a></td>\n" +
    "					<td><button type=\"button\" ng-click=\"deleteAccommodation(accommodation)\">Ta bort</button></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "			<button type=\"button\" people-selector multi-select on-people-select=\"onSaveAccommodations(people)\">Lägg till fler sovande</button>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</div>\n" +
    "				\n" +
    "			");
}]);

angular.module("resource/templates/bookingoverview.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingoverview.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='resource in resources'>\n" +
    "			<h3><a href=\"resource/{{resource.id}}\">{{resource.name}}</a></h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in resource.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\" target=\"_self\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createBooking($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteBooking($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("resource/templates/bookingschedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingschedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\" target=\"_self\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createBooking($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteBooking($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("resource/templates/bookingslist.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingslist.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<h4 class=\"sectionHeader\">Bokningslista</h4>\n" +
    "	<div>\n" +
    "		<table >\n" +
    "			<thead>\n" +
    "			<tr class=\"even\" sortable-collection>\n" +
    "				<th class=\"timeCol\">När</th>\n" +
    "				<th class=\"nameCol\">Vad</th>\n" +
    "				<th class=\"buttonsCol\"></th>\n" +
    "			</tr>\n" +
    "			</thead>\n" +
    "			<tbody>\n" +
    "				<tr ng-class-even=\"event\" ng-class-odd=\"odd\" ng-repeat=\"booking in bookings\">\n" +
    "					<td><a href=\"{{booking.eventOccationBooking.eventOccation.event.id|eventLink}}/signups\" target=\"_self\">{{booking.eventOccationBooking.eventOccation.timespan|simpleTimespanLabel}}</a></td>\n" +
    "					<td><a href=\"{{booking.eventOccationBooking.eventOccation.event.id|eventLink}}\" target=\"_self\">{{booking|bookingLabel}}</a></td>\n" +
    "					<td><button type=\"button\" ng-click=\"deleteBooking(booking)\">Ta bort</button></td>\n" +
    "				</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "		<button type=\"button\" resource-id=\"resource.id\" event-occation-selector on-save-selection='saveEventOccationBookings(occations)'>Lägg till bokning</button>\n" +
    "	</div>                   \n" +
    "</div>");
}]);

angular.module("resource/templates/delete.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/delete.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<button ng-click=\"delete()\" type=\"button\">Ta bort</button>\n" +
    "</div>");
}]);

angular.module("resource/templates/eventOccationSelector/bookingschedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/bookingschedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteEventOccation($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("resource/templates/eventOccationSelector/checkbox.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/checkbox.tpl.html",
    "<input type=\"checkbox\" ng-click=\"handleClick($event,person)\"/>{{person|searchLabel}}");
}]);

angular.module("resource/templates/eventOccationSelector/radio.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/radio.tpl.html",
    "<input type=\"radio\" name=\"person\" ng-click=\"handleClick($event,person)\"/>{{person|searchLabel}}");
}]);

angular.module("resource/templates/eventOccationSelector/selectorDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/selectorDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h1>{{dialogHeader}}</h1>\n" +
    "	<div class=\"event-list-container\">\n" +
    "		<p ng-show=\"noResults\">Inga arrangemang kunde hittas</p>\n" +
    "		<ul class=\"event-list\" ng-hide=\"noResults\" >\n" +
    "			<li ng-repeat=\"event in events\" class=\"event\">\n" +
    "				<input type=\"checkbox\" value='person' ng-click='selectEventOccations(event,$event)' ng-disabled=\"allOccationsAreBooked(event)\"/>{{event.name}}\n" +
    "				<ul class=\"occation-list\">\n" +
    "					<li class=\"occation\" ng-repeat=\"occation in event.eventOccations\" ng-class=\"{'colliding' : !occation.ok_to_book}\" ng-form>\n" +
    "						<input ng-disabled=\"occation.already_booked\" type=\"checkbox\"\n" +
    "							   ng-model='formdata.eventOccations[occation.id]'\n" +
    "							   ng-true-value=\"{{occation.id}}\"\n" +
    "							   ng-false-value=\"false\"/>{{occation|eventOccationLabel}}\n" +
    "					</li>\n" +
    "				</ul>\n" +
    "			</li>\n" +
    "		</ul>\n" +
    "	</div>\n" +
    "	<button class=\"save-selection\" type=\"button\" ng-click=\"saveSelection() || closeThisDialog()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("resource/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"angular-form\" name=\"form\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Namn <input type=\"text\" name=\"name\" ng-model=\"resource.name\" required=\"required\"/><span ng-show=\"form.name.$error.required\">*</span></label>\n" +
    "            <label>Type <select ng-model=\"resource.resourceType\" name=\"type\" required=\"required\" ng-options=\"type.name for type in resourceTypes track by type.id\">\n" +
    "                </select><span ng-show=\"form.type.$error.required\">*</span></label>\n" +
    "            <label>Tillgänglig?\n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"available\" ng-model=\"resource.available\" ng-value=\"true\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"available\" ng-model=\"resource.available\" ng-value=\"false\"/> Nej\n" +
    "                </span></label>\n" +
    "			<label>Användbar för sovning?\n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"sleepingResourceIsAvailable\" ng-model=\"resource.sleepingResource.isAvailable\" ng-value=\"true\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"sleepingResourceIsAvailable\" ng-model=\"resource.sleepingResource.isAvailable\" ng-value=\"false\"/> Nej\n" +
    "                </span></label>\n" +
    "			<label ng-show=\"resource.sleepingResource.isAvailable\">Antal sovplatser <input type=\"text\" ng-model=\"resource.sleepingResource.noOfSlots\"/></label>\n" +
    "		</fieldset>\n" +
    "		<fieldset class=\"formBox\">\n" +
    "             <label>Beskrivning <textarea class=\"large\" ng-model=\"resource.description\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "	    <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"saveResource()\" ng-disabled=\"!(form.override.$modelValue || form.$valid) || disableSaveButton\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "    <script>\n" +
    "        $(\"#person-form input[type=radio]\").each(function(key,obj){\n" +
    "            var $obj = $(obj);\n" +
    "            var $parent =$obj.parent(\".buttonBox\");\n" +
    "            $obj.bind(\"focus\",function(){\n" +
    "                $parent.addClass(\"focus\");\n" +
    "            }).bind(\"blur\",function(){\n" +
    "                $parent.removeClass(\"focus\");\n" +
    "            });\n" +
    "        });\n" +
    "    </script>\n" +
    "</div>");
}]);

angular.module("resource/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<table class=\"resourceList\">\n" +
    "		<tr class=\"even\">\n" +
    "			<th class=\"idCol\"><a href=\"\">Id</a></th>\n" +
    "			<th class=\"nameCol\"><a href=\"\">Namn</a></th>\n" +
    "			<th class=\"typeCol\"><a href=\"\">Typ</a></th>\n" +
    "			<th><a href=\"\">Tillgänglig</a></th>\n" +
    "			<th><a href=\"\">Sovsal</a></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"resource in resources\">\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].id}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].name}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].resourceType.name}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].available|yesNoStatus}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}/accommodation\">{{resource|sleepingResourceStatus}}</a></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "</div>");
}]);

angular.module("resource/templates/resource.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/resource.tpl.html",
    "<div class=\"resource\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("timespan/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div ng-form=\"timespanForm\" class=\"timespan-form\">\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<h4 ng-show=\"currentTimespan.id\">Passdetaljer</h4>\n" +
    "			<label>Namn <input type=\"text\" ng-model=\"currentTimespan.name\" /></label>\n" +
    "			<label>Starts at<input ng-model=\"startsAt\" type=\"datetime-local\" /></label>\n" +
    "			<label>Ends at<input ng-model=\"endsAt\" type=\"datetime-local\"/></label>\n" +
    "			<fieldset>\n" +
    "				Visa i schemat?\n" +
    "				<div class=\"input-box\">\n" +
    "					<label><input ng-model=\"currentTimespan.showInSchedule\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "					<label><input ng-model=\"currentTimespan.showInSchedule\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "				</div>\n" +
    "			</fieldset>\n" +
    "			<span class=\"clear\"></span>\n" +
    "			<div style=\"display:table\">\n" +
    "				<div style=\"display: table-row\">\n" +
    "					<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"timespanForm.$pristine || timespanForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{saveLabel}}</button></div>\n" +
    "					<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "					<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-show=\"onCancelClick\" class=\"cancel-button right\" ng-click='onCancelClick()'><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			\n" +
    "		</fieldset>\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<h4 ng-show=\"currentTimespan.id\">Inbokade pass</h4>\n" +
    "			 <table id=\"timespan-event-occations\" sortable-collection>\n" +
    "				<tr>\n" +
    "					<th>Arrangemang</th>\n" +
    "					<th>Passnamn</th>\n" +
    "					<th>Arrangemangstyp</th>\n" +
    "					<th></th>\n" +
    "				</tr>\n" +
    "				<tr ng-repeat=\"eventOccation in currentTimespan.eventOccations\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "					<td><a href=\"{{eventOccation.event.id|eventLink}}\">{{eventOccation.event.name}}</a></td>\n" +
    "					<td><a href=\"{{eventOccation.event.id|eventLink}}/signups\">{{eventOccation.name}}</a></td>\n" +
    "					<td><a href=\"{{eventOccation.event.eventtype.id|eventTypeLink}}\">{{eventOccation.event.eventtype.name}}</a></td>\n" +
    "					<td></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "		</fieldset>\n" +
    "	</div>	\n" +
    "</div>");
}]);

angular.module("timespan/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"timespanList\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th sort-on-click=\"timespans by name\">Namn</th>\n" +
    "            <th sort-on-click=\"timespans by startsAt\">Startar</th>\n" +
    "            <th sort-on-click=\"timespans by endsAt\">Slutar</th>\n" +
    "			<th></th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"timespan in timespans\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.name}}</a></td>\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.startsAt|date}}</a></td>\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.endsAt|date}}</a></td>\n" +
    "            <td></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("timespan/templates/timespan.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/timespan.tpl.html",
    "<div class=\"group\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);
