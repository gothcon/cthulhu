/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function(){
/*jshint strict:false */


angular.module("EventType",['ngResource','GothCon','HttpDataProviders','Filters','ngDialog'])
		.provider("eventTypeRepository",function(){
			return {
				$get : function EventTypeRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
					var _resource = $resource( linkBuilder.createAdminLink("eventtype/:id/:svc/:operation?json"),{},{	
						'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
						'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor }
					});
					return {
						getList : function(callback){
							callback = callback || angular.noop;
							return _resource.query(callback,callback);
						},
						get : function(id,callback){
							callback = callback || angular.noop;
							return _resource.get({id : id},callback,callback);
						},
						save : function(eventtype,callback){
							callback = callback || angular.noop;
							return _resource.save({id : eventtype.id},eventtype,callback,callback);
						},
						create : function(eventtype,callback){
							callback = callback || angular.noop;
							return _resource.save({},eventtype,callback,callback);
						},
						delete : function(eventtypeId,callback){
							callback = callback || angular.noop;
							return _resource.delete({id : eventtypeId},callback,callback);
						}
					};
				}
			};
		})
		.controller("eventTypeItemController",function($scope,eventTypeRepository){
			console.log($scope);
			$scope.inEditMode = false;
			var backupValue = angular.copy($scope.eventtype);
			$scope.enterEditMode = function(){
				$scope.inEditMode = true;
			};
			$scope.leaveEditMode = function(){
				$scope.inEditMode = false;
			};			
			$scope.save = function(){
				eventTypeRepository.save($scope.eventtype,function(){
					backupValue = angular.copy($scope.eventtype);
					$scope.inEditMode = false;
				});
			};			
			$scope.cancel = function(){
				$scope.inEditMode = false;
				angular.copy(backupValue,$scope.eventtype);
			};
			$scope.remove = function(){
				if(confirm("Är du säker på att du vill ta bort arrangemangstypen?")){
					eventTypeRepository.delete($scope.eventtype.id,function(){
						var index = $scope.eventtypes.indexOf($scope.eventtype);
						$scope.eventtypes.splice(index,1);
						$scope.inEditMode = false;
					});
				}
			};
			$scope.create = function(){
				eventTypeRepository.create($scope.eventtype,function(response){
					$scope.eventtypes.push(response.data.data);
					angular.copy(backupValue,$scope.eventtype);
				});
			};
		})
		.directive("eventTypeList",function(){
		return {
			controller : function ($scope,eventTypeRepository){
				$scope.save = function(){};
				$scope.delete = function(){};
				$scope.title = "Arrangemangstyper";
				eventTypeRepository.getList(function(response){
					$scope.eventtypes = response.data.data;
				});
				$scope.eventtypes = [];
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "eventtypes/templates/list.tpl.html",
			restrict : 'E'			
		};
	});
}());