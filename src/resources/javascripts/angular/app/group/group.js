(function(){
	function Group(promise){
		var that;
		if(promise){
			that = Object.create(promise);
		}else{
			that = this;
		}
		that.name = "";
		that.id = 0;
		that.emailAddress = "";
		that.members = [];
		that.signups = [];
		that.leader = null;

		return that;
	}
	angular.module("Group",['ngResource','ngSanitize','GothCon','HttpDataProviders','Filters','Signups','Sleeping','Orders','templates-angular_templates'])
		.provider("GroupDataProvider",function GroupDataProviderProvider(){
			var currentGroup = {};
			var baseUrl = "";
			var currentList = [];

			return {
				setBaseUrl : function(newBaseUrl){
					baseUrl = newBaseUrl;
				},
				$get : function GroupDataProviderFactory($q,$http){

					var groupPromise;
					var listPromise;

					function getById(id){
						var deferredJob = $q.defer();
						groupPromise = deferredJob.promise;
						if(!id){
							var group = getCurrent();
							deferredJob.resolve(group);
						}else{
							// load from http
							$http({
								url : baseUrl + "/admin/group/" + id,
								method: "get",
								responseType : "json"
							}).then(function(response){
								deferredJob.resolve(response.data.data);
							}, function(error){
								deferredJob.reject("error");
								console.log(error);
								throw "Failed to load group. See console for more information";
							});
						}
						return groupPromise;
					}
					function getCurrent(){

						if(!currentGroup){
							throw "No group loaded";
						}
						else{
							return currentGroup;
						}
					}
					function getAll(){
						var deferredJob = $q.defer();
						listPromise = deferredJob.promise;
						// load from http
						$http({
							url : baseUrl + "/admin/group",
							method: "get",
							responseType : "json"
						}).then(function(response){
							deferredJob.resolve(response.data.data);
						}, function(error){
							deferredJob.reject("error");
							console.log(error);
							throw "Failed to load groups. See console for more information";
						});
						currentList = deferredJob.promise;
						return currentList;
					}
					function save(groupData){
						var deferredJob = $q.defer();
						groupPromise = deferredJob.promise;
						var serviceUrl = baseUrl + "/admin/group";
						if(groupData.id){
							serviceUrl += "/" + groupData.id;
						}
						
						$http({
							url : serviceUrl,
							method: "post",
							responseType : "json",
							data: JSON.stringify(groupData)
						}).then(function(response){
							if(response.data.status === 0){
								deferredJob.resolve(response.data.data);
							}else{
								deferredJob.reject("Failed to save group. See console for more information");
								console.log(response.data);
							}
						}, function(error){
							deferredJob.reject("Failed to save group. See console for more information");
							console.log(error);
						});
						
						return groupPromise;
					}
					function createNew(){
						return new Group();
					}

					return {
						getCurrent : getCurrent,
						getById : getById,
						getAll : getAll,
						createNew : createNew,
						save : save
					};

				}
			};
		})
		.directive("group",function(){
			var extractIdRegex = /^\/group\/(\d+|new)(\/|$)/;
			var initialData;
			return {
				controller : function($scope,$location,gcTabsBus,GroupDataProvider,$element,$compile){
					
					function initialize(){
						var $content = angular.element($element.find("div")[0]);
						$scope.view = "";
						$scope.$watch("view",function(newValue,oldValue){
							if(newValue !== oldValue && newValue !== ""){
								$content.contents().remove();
								var $view = $compile(angular.element(newValue))($scope);
								$content.append($view);
							}
						});

						$scope.currentGroup = initialData;
						
						$scope.$on('$locationChangeSuccess',onLocationChange);
					}
					
					function onLocationChange(){
						var match = extractIdRegex.exec($location.path());

						var id = (match !== null && match.length > 1) ? match[1] : "";

						if(id === ""){
							// this is the list view
							onShowList();
							return;
						}
						else if(id === "new")
						{
							onShowNewForm();
							return;
						}
						else{
							onShowGroup(id);
							return;
						}
					}
					
					function onShowList(){
						gcTabsBus.setTabs([]);
						$scope.title = "Grupplista";
						gcTabsBus.setTabs([{ id : 'group/', 'label' : 'Grupplistning'},{ id : 'group/new', 'label' : 'Ny grupp'}]);
						$scope.currentGroup = undefined;
						$scope.groupList = GroupDataProvider.getAll().then(
							function(groups){
								$scope.groupList = groups;
								$scope.view = "<group-list groups='groupList'></group-list>";
							}
						);
						return;
					}

					function onShowNewForm(){
						$scope.currentGroup = GroupDataProvider.createNew();
						$scope.title = "Ny grupp";
						gcTabsBus.setTabs([{ id : 'group/', 'label' : 'Grupplistning'},{ id : 'group/new', 'label' : 'Ny grupp'}]);
						$scope.view = "<group-details current-group='currentGroup'></group-details>";
					}

					function onShowGroup(id){
						gcTabsBus.setTabs([
							{ id : 'group/', 'label' : 'Grupplistning'},
							{ id : 'group/' + id + '/', 'label' : 'Detaljer'},
							{ id : 'group/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'}]);
						switch($location.path()){
							case '/group/' + id +"/signups":
								$scope.view = "<group-signups-and-members current-group='currentGroup'></group-members-and-signups>";
								break;
							case '/group/' + id :
								$scope.view = "<group-details current-group='currentGroup'></group-details>";
								break;
							default:
								$scope.view = "<group-details current-group='currentGroup'></group-details>";
						}
						
						if(!$scope.currentGroup || "" + $scope.currentGroup.id !== "" + id){
							$scope.currentGroup = GroupDataProvider.getById(id).then(function(group){
								$scope.currentGroup = group;
								$scope.title = "Detaljer för " + $scope.currentGroup.name;
							});
						}
					}

					initialize();
					
				},
				scope : {
					
				},
				replace : true,
				templateUrl : "group/templates/group.tpl.html",
				restrict : 'AE'				
			};
		})
		.directive("groupDetails",function($templateCache){
			return {
				controller : function($scope,GroupDataProvider,$location){
					$scope.save = function(){
						GroupDataProvider.save($scope.currentGroup).then(function(createdGroup){
							$scope.currentGroup = createdGroup;
							$location.path('group/' + createdGroup.id + '/');
						},function(){
							
						});
					};
				},
				scope : {
					currentGroup : "="
				},
				replace : true,
				templateUrl : "group/templates/details.tpl.html",
				restrict : 'AE'
			};
		})
		.directive("groupList",function($templateCache){
			return {
				controller : function($scope){
					
				},
				scope : {
					groups : "="
				},
				replace : true,
				templateUrl : "group/templates/list.tpl.html",
				restrict : 'AE'
			};
		})
		.directive("groupSignupsAndMembers",function($templateCache){
			return {
				controller : function($scope,Groups){
					$scope.addMembers = function(member){
						if(member.length > 0){
							Groups.addMembership($scope.currentGroup.id,member[0].id,function(response){
								$scope.currentGroup.groupMembers.push(response.data);
							});
						}
					};
					$scope.deleteMembership = function(membership){
						if(confirm("Är du säker?")){
							Groups.deleteMembership(membership.id,function(response){
								$scope.currentGroup.groupMembers.splice($scope.currentGroup.groupMembers.indexOf(membership),1);
							});
						}
					};
				},
				scope : {
					currentGroup : "="
				},
				replace : true,
				templateUrl : "group/templates/members_and_signups.tpl.html",
				restrict : 'AE'
			};
		});
}());