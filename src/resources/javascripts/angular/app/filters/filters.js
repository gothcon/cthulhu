(function(){
/*jshint strict:false */
"use strict";

angular.module("Filters",['GothCon'])
	.filter('gcDate',function($filter){
		return function(input){
		    var date = moment(input.date ? input.date : input);
			var dateFilter = $filter("amDateFormat");
			return dateFilter(date,"dd D/M HH:mm");
		};
	})
	.filter('timespanLabel', function($filter) {
        return function(input,includeName) {
			if(input.startsAt && input.endsAt){
				var startsAt = moment(input.startsAt.date ? input.startsAt.date : input.startsAt );
				var endsAt = moment(input.endsAt.date ? input.endsAt.date : input.endsAt);
				var formatStart;
				var formatEnd;
				if(endsAt.year() !== startsAt.year()){
					// olika år
					formatStart = "dd D/M YYYY HH:mm";
					formatEnd = "dd D/M YYYY HH:mm";
				}
				else if(endsAt.month() !== startsAt.month()){
					// olika månader
					formatStart = "dd D/M HH:mm";
					formatEnd = "dd D/M HH:mm";
				}
				else if(endsAt.date() !== startsAt.date()){
					// olika dagar
					formatStart = "dd D/M HH:mm";
					formatEnd = "dd D/M  HH:mm";
				}
				else{
					// olika timmar
					formatStart = "dd D/M HH:mm";
					formatEnd = "HH:mm";
				}
				var dateFilter = $filter("amDateFormat");
				if(includeName && input.name){
					return input.name + " (" + dateFilter(startsAt,formatStart) + " - " + dateFilter(endsAt,formatEnd) + ")";
				}else{
					return dateFilter(startsAt,formatStart) + " - " + dateFilter(endsAt,formatEnd);
				}
			}else{
				return input.name;
			}
        };
    })
    .filter('occationLabel', function(){
        return function(eventOccation){
            return eventOccation.event.name + (eventOccation.name ? " - " + eventOccation.name : "");
        };
    })
	.filter('yesNoStatus', function(){
        return function(trueOrFalse,isApplicable){
			if(isApplicable !== undefined && !isApplicable){
				return "n/a";
			}
            return trueOrFalse ? "ja" : "nej";
        };
    })
	.filter('sleepingResourceStatus', function(){
        return function(resource){
			var sleepingResource = resource[0].sleepingResource;
			if(!sleepingResource  || !sleepingResource.isAvailable){
				return "nej";
			}
			else{
				var availableSlots = sleepingResource.noOfSlots - resource.sleepingSlotBookings;
				return "Ja, "  + availableSlots + " av "  +sleepingResource.noOfSlots + " platser är lediga"; 
			}
        };
    })
    .filter('orderStatus', function(){
        return function(statusInEnglish){
			switch(statusInEnglish){
				case 'cart':
					return 'öppen';
				case 'submitted':
					return 'beställd';
				case 'partially_paid':
					return 'delvis betald';
				case 'paid':
					return 'betald';
				case 'cancelled':
					return 'makulerad';
				case 'partially_delivered':
					return 'delvis levererad/utlämnad';
				case 'delivered':
					return 'levererad/utlämnad';
				default:
					return statusInEnglish;
			}
        };
    })
	.filter('signupStatus', function(){
        return function(signup){
			var statusParts = [];
			if(signup.signupOrderNumber){
				statusParts.push("Anmäld som nummer "+ signup.signupOrderNumber);
			}

			if(signup.signupSlot.requiresApproval){
				statusParts.push("behöver godkännas");
			}
			if(signup.signupSlot.signupSlotArticle){
				statusParts.push("behöver betalas");
			}
			
            var status =  statusParts.join(', ');
			return status + ".";
        };
    })
    .filter('eventOccationLabel', function($filter){
        return function(eventOccation, format){
            var label = "";
			if(format === undefined){
				var timespanFilter = $filter("timespanLabel");
				var timespan = eventOccation.timespan;
				if(!timespan){
					return "no timespan selected";
				}
				label = timespan.name ? timespan.name + " (" + timespanFilter(timespan) + ")" : timespanFilter(timespan);

				if(eventOccation.name){
					label += " - " + eventOccation.name;
				}
			}else if(format === "event"){
				label = eventOccation.timespan.name;
				if(eventOccation.name && eventOccation.name.length > 0){
					label += " : " + eventOccation.name;
				}
			}
            return label;
        };
    })
	.filter('personLabel', function(){
        return function(person){
			if(!person){
				return "";
			}
			return(person.firstName + " " + person.lastName);
        };
    })
	.filter('resourceLabel', function(){
        return function(resource){
			if(!resource){
				return "";
			}
			return(resource.id + ". " + resource.name + " (" + resource.resourceType.name + ")");
        };
    })
	.filter('resourceLabel', function(){
        return function(resource){
			if(!resource){
				return "";
			}
			return(resource.name + " (" + resource.resourceType.name + ")");
        };
    })
	.filter('resourceLink', function(linkBuilder){
        return function(resource){
			if(!resource){
				return "#";
			}
			return linkBuilder.createAdminLink("resource/" + resource.id);
        };
    })
	.filter('groupLabel', function(){
        return function(group){
			if(!group){
				return "";
			}
			return(group.id + ". " + group.name);
        };
    })
	.filter('eventOrganizerLabel', function($filter){
        return function(eventOrganizer){
			if(!eventOrganizer){
				return "ingen vald";
			}
			if(eventOrganizer.person){
				var labelFilter = $filter("personLabel");
				return labelFilter(eventOrganizer.person);
			}
			if(eventOrganizer.group){
				return eventOrganizer.group.id + ". " + eventOrganizer.group.name;
			}
			return "ingen vald";
        };
    })	
	.filter('eventOrganizerLink', function($filter){
        return function(eventOrganizer){
			var linkFilter;
			if(!eventOrganizer){
				return "#";
			}
			if(eventOrganizer.person){
				linkFilter = $filter("personLink");
				return linkFilter(eventOrganizer.person.id);
			}
			if(eventOrganizer.group){
				linkFilter = $filter("groupLink");
				return linkFilter(eventOrganizer.group.id);
			}
			return "#";
        };
    })
	.filter('signupSlotInfo', function(){
        return function(signupSlot){
			if(!signupSlot){
				return "";
			}
			var details = [];
			
			if(signupSlot.maximumSignupCount && signupSlot.maximumSignupCount >= 0){
				details.push(signupSlot.maximumSignupCount + " platser");
			}
			if(signupSlot.maximumSpareSignupCount && signupSlot.maximumSpareSignupCount >= 0){
				details.push(signupSlot.maximumSpareSignupCount + " reservplatser");
			}
			if(signupSlot.isHidden){
				details.push("Dold anmälningsmöjlighet");
			}
			if(signupSlot.requiresApproval){
				details.push("Kräver godkännande");
			}
			if(details.length){
				details.push("");
			}
			return details.join(". ");
        };
    })
	.filter('eventOccationInfo', function(){
        return function(eventOccation){
			if(!eventOccation){
				return "";
			}
			var details = [];
			
			if(eventOccation.isHidden){
				details.push("Dolt pass");
			}
			if(details.length){
				details.push("");
			}
			return details.join(". ");
        };
    })
	.filter('searchLabel', function(){
        return function(item){
			return(item.id + ". " + item.label);
        };
    })		
	.filter('personLink', function(linkBuilder){
        return function(personId){
			return linkBuilder.createAdminLink("person/" + personId);
        };
    })
	.filter('eventLink', function(linkBuilder){
        return function(eventId){
			return linkBuilder.createAdminLink("event/" + eventId);
        };
    })	
	.filter('eventTypeLink', function(linkBuilder){
        return function(eventTypeId){
			return linkBuilder.createAdminLink("eventtype/" + eventTypeId);
        };
    })
	.filter('registrationLink', function(linkBuilder){
        return function(personId){
			return linkBuilder.createAdminLink("ankomst/" + personId);
        };
    })
	.filter('groupLink',function(linkBuilder){
		return function(groupId){
			return linkBuilder.createAdminLink("group/" + groupId);
		};
	})
	.filter('timespanLink',function(linkBuilder){
		return function(timespanId){
			return linkBuilder.createAdminLink("timespan/" + timespanId);
		};
	})
	.filter('membershipLink',function(linkBuilder){
		return function(groupMembershipId){
			return linkBuilder.createAdminLink("groupmember/" + groupMembershipId);
		};
	})
	.filter('newOrderLink',function(linkBuilder){
		return function(personId){
			return linkBuilder.createAdminLink("order/ny/?p=" + personId);
		};
	})
	.filter('bookingType',function(){
		return function(bookingType){
			return bookingType;
		};
	})
	.filter('membershipStatus', function(){
        return function(membership){
			if(membership.isLeader){
				return "Gruppledare";
			}
			else{
				return "";
			}
        };
    });
}());