(function(){
/*jshint strict:false */
"use strict";

angular.module("Event",['GothCon','Filters','ngDialog','Signups','Resource','HttpDataProviders','ui.tinymce'])
	.provider("Event",function EventRepositoryProvider(){
	   return {
			$get : function EventRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var _resource = $resource( linkBuilder.createAdminLink("event/:id/:svc/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'occations'	: {method:'GET'	, interceptor : gcLoaderBusInterceptor, isArray:true, params : {svc : 'svc' , operation : 'occations'} },
					'createEventOccation'				: {method:'POST', interceptor : gcLoaderBusInterceptor, isArray:false, params : {svc : 'svc' ,operation : 'createEventOccation'} },
					'getResourceBookingScheduleData'	: {method:'GET', interceptor : gcLoaderBusInterceptor, isArray:false, params : {svc : 'svc' ,operation : 'getResourceBookingScheduleData'} },
					'formMetaData'						: {method:'GET'	, interceptor : gcLoaderBusInterceptor, params : {svc: "svc", operation : 'formMetadata'} }
				});
				return {
					query : function(callback){
						callback = callback || angular.noop;
						return _resource.query(callback,callback);
					},
					occations : function(eventId,callback){
						callback = callback || angular.noop;
						return _resource.occations({'id' : eventId} ,callback,callback);
					},
					createEventOccation : function(eventId, eventOccation, callback){
						callback = callback || angular.noop;
						return _resource.createEventOccation({'id' : eventId}, eventOccation ,callback,callback);
					},
					formMetaData : function(callback){
						callback = callback || angular.noop;
						return _resource.formMetaData({} ,callback,callback);
					},
					get : function(id,callback){
						callback = callback || angular.noop;
						return _resource.get({id : id},callback,callback);
					},
					save : function(event,callback){
						callback = callback || angular.noop;
						return _resource.save({id : event.id},event,callback,callback);
					},
					getResourceBookingScheduleData : function(callback){
						return _resource.getResourceBookingScheduleData(callback,callback);
					}
				};
			}
	   } ;
	})
	.provider("EventOccation",function EventRepositoryProvider(){
	   return {
			$get : function EventRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var _resource = $resource( linkBuilder.createAdminLink("eventOccation/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'scheduleData' : {method:'GET', interceptor : gcLoaderBusInterceptor }
				});
				return {
					query : function(callback){
						callback = callback || angular.noop;
						return _resource.query(callback,callback);
					},
					get : function(id,callback){
						callback = callback || angular.noop;
						return _resource.get({id : id},callback,callback);
					},
					'delete' : function(id,callback){
						callback = callback || angular.noop;
						return _resource.delete({id : id},callback,callback);
					},
					save : function(eventOccation,callback){
						callback = callback || angular.noop;
						return _resource.save({id : eventOccation.id},eventOccation,callback,callback);
					},
					scheduleData : function(callback){
						callback = callback || angular.noop;
						return _resource.scheduleData({},callback,callback);
					}
				};
			}
	   } ;
	})
	.provider("SignupSlot", function SignupSlotsRepositoryProvider(){
        var _resource;
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus,linkBuilder,gcLoaderBusInterceptor){
				var standardSignupSlotLink = linkBuilder.createAdminLink("signupSlot");
				var standardPersonLink = linkBuilder.createAdminLink("person");
				
                _resource = $resource(standardSignupSlotLink + "/:id",{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} },
                    'all' : { method : "GET", isArray : true}
                });
				
                return {
                    'getPersonalSignupSlots' : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardSignupSlotLink + "?person_id=:personId",{}).get({ personId: id}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					'getGroupSignupSlots' : function(groupId,callback){
						gcLoaderBus.add();
                        $resource(standardSignupSlotLink + "?group_id=:groupId",{}).get({ groupId: groupId}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					'save' : _resource.save,
					'delete' : function(signupSlotId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id: signupSlotId}, callback );
					}
                };
            }
        };
    })
	.provider("EventOccationBooking", function SignupSlotsRepositoryProvider(){
        var _resource;
		
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus,linkBuilder,gcLoaderBusInterceptor){
                _resource = $resource(linkBuilder.createAdminLink("eventOccationBooking") + "/:id",{},{	
					'get'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'createMany'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} }
                });
				
                return {
					'create'	: _resource.save,
					'createMany': function(dataArray,callback){ return _resource.createMany({},dataArray,callback,callback);},
					'delete'	: _resource.delete
                };
            }
        };
    })
	.directive("eventList",function($templateCache){
		return {
			controller : function($scope){

			},
			scope : {
				events : "="
			},
			replace : true,
			templateUrl : "event/templates/list.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventOccationSchedule",function($templateCache,EventOccation,ngDialog){
		return {
			controller : function($scope,Event){
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				var inSelectionMode = false;
				var selectedElements = [];
				
				var conventionStartsAt;
				var conventionEndsAt;
				
				$scope.enterSelectionMode = function($event){
					for(var key in selectedElements){
						angular.element(selectedElements[key]).removeClass("selected");
						
					}
					selectedElements.length = 0;
					$event.stopPropagation();
					$event.preventDefault();
					console.log("entering selection mode");
					inSelectionMode = true;
					$scope.select($event);
				};
				$scope.select = function($event){
					$event.stopPropagation();
					$event.preventDefault();
					if(inSelectionMode){
						console.log("selecting");
						selectedElements.push($event.target);
						console.log(selectedElements);
						angular.element($event.target).addClass("selected");
					}
				};
				$scope.leaveSelectionMode = function($event){
					$event.stopPropagation();
					$event.preventDefault();					
					inSelectionMode = false;
					console.log(selectedElements);
				};
				
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					
					
					
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addEvents(eventTypes,conventionStartsAt){
					var eventTypeBoxes = [];
					for(var key in eventTypes){
						
						var eventType = eventTypes[key];
						var eventTypeBox = {
							name : eventType.name,
							id : eventType.id,
							events : []
						};
						for(var eventKey in eventType.events){
							var event = eventType.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.eventOccations){
								var eventOccation = event.eventOccations[occationKey];
								if(eventOccation.timespan){
									var occationBox = {
										name : eventOccation.name || eventOccation.timespan.name,
										id : eventOccation.id,
										width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
										offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
									};
									eventBox.eventOccations.push(occationBox);
								}
							}
							eventTypeBox.events.push(eventBox);
						}
						eventTypeBoxes.push(eventTypeBox);
					}
					$scope.eventTypes = eventTypeBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				EventOccation.scheduleData(function(response){
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.data.publicTimespans;
					var eventTypes = response.data.data.events;
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);
						
						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addEvents(eventTypes,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
					
					
					
				});
				
				$scope.deleteEventOccation = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort passet? Alla anmälningar och resursbokningar kommer att försvinna.")){
						EventOccation.delete(eventOccationBox.id,function(response){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};
				
				
				
				$scope.createEventOccation = function($event,event,timespan){
					$event.preventDefault();
					var scope = $scope.$new(true);
					scope.timespan = timespan;
					scope.event = event;
					
					ngDialog.open({
						template : '<div><event-occation-dialogue event="event" timespan="timespan"></event-occation-dialogue></div>',
						plain:	true,
						className: 'ngdialog-theme-default',
						scope: scope
					});
					
//					if(confirm("Vill du skapa ett arrangemangspass här?")){
//						Event.createEventOccation(event.id,{id:0, isHidden:false, name:"",timespan: {id:timespan.id, isPublic: true} },function(response){
//							var eventOccation = response.data.data;
//							var occationBox = {
//								name : eventOccation.name || eventOccation.timespan.name,
//								id : eventOccation.id,
//								width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
//								offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
//							};
//							event.eventOccations.push(occationBox);
//							$scope.showNewForm = false;							
//						});
//					}
				};
			},
			scope : {
				
			},
			replace : true,
			templateUrl : "event/templates/eventOccationSchedule.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventDetails",function($templateCache){
		return {
			controller : function($scope,People,GroupDataProvider,Event,$location){
				$scope.selectOrganizer = function(selection){
					if($scope.currentEvent.organizer){
						$scope.currentEvent.organizer.group = null;
						$scope.currentEvent.organizer.person = null;
					}else{
						$scope.currentEvent.organizer = { 
							id : 0, 
							person:null, 
							group:null 
						};
					}
					
					if(selection.length && selection[0].type ==="person"){
						People.get(selection[0].id,function(response){
							$scope.currentEvent.organizer.person = {
								firstName: response.data.firstName, 
								lastName:response.data.lastName, 
								id: response.data.id
							};
						});
					}
					if(selection.length && selection[0].type ==="group"){
						GroupDataProvider.getById(selection[0].id).then(function(response){
							$scope.currentEvent.organizer.group = {
								name: response.name, 
								id: response.id
							};
						});
					}
				};
				$scope.save = function(){
					var event =  {};
					angular.copy($scope.currentEvent,event);
					event.eventOccations = undefined;
					if(event.organizer){
						if(event.organizer.person){
							event.organizer.personId = event.organizer.person.id;
							event.organizer.person = undefined;
						}
						if(event.organizer.group){
							event.organizer.groupId = event.organizer.group.id;
							event.organizer.group = undefined;
						}
						
					}
					if(event.eventtype){
						event.eventtypeId = event.eventtype.id;
						event.eventtype = undefined;
					}
					Event.save(event,function(response){
						$scope.currentEvent = response.data.data;
						if($scope.currentEvent.id){
							$location.path("event/" + $scope.currentEvent.id);
						}
					});
				};
				$scope.selectOrganizerLabel = ($scope.currentEvent && $scope.currentEvent.id) ? "ändra..." : "välj arrangör...";
				$scope.hasOrganizer = function(){
					return $scope.currentEvent && $scope.currentEvent.organizer && ($scope.currentEvent.organizer.person || $scope.currentEvent.organizer.group);
				};
			},
			scope : {
				currentEvent : "=",
				eventTypes : "="
			},
			replace : true,
			templateUrl : "event/templates/details.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventOccation",function($templateCache){
		return {
			controller : function($scope,EventOccation,Resource,SignupSlot,EventOccationBooking){
				$scope.showEditForm = false;
				$scope.showNewForm = false;
				
				$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
				
				$scope.$on("eventOccationFormEvent",function(event,args){
					if(args.type && args.type === "cancelClicked"){
						$scope.showEditForm = false;
						event.preventDefault();
					}					
					if(args.type && args.type === "eventOccationUpdate"){
						EventOccation.save(args.eventOccation,function(response){
							$scope.eventOccation = response.data.data;
							event.preventDefault();
							$scope.showEditForm = false;							
						});
					}
				});
				
				$scope.deleteResourceBooking = function (eventOccationBooking){
					if(confirm("Är du säker?")){
						var index = $scope.eventOccation.eventOccationBookings.indexOf(eventOccationBooking);
						Resource.deleteOccationBooking(eventOccationBooking.booking.id, function(){
							$scope.eventOccation.eventOccationBookings.splice(index,1);
						});
					}
				};
				
				$scope.saveResourceBooking = function (entities){
					var data = [];
					for(var key in entities){
						data.push({resourceId : entities[key].id, eventOccationId : $scope.eventOccation.id});
					}
					EventOccationBooking.createMany(data,function(response){
						for(var key in response.data.data){
							$scope.eventOccation.eventOccationBookings.push(response.data.data[key]);
						}
					});
				};
				
				$scope.$on("signupSlotFormEvent",function(event,args){
					if(args.type && args.type === "signupSlotCreate"){
						SignupSlot.save(args.signupSlot,function(response){
							$scope.eventOccation.signupSlots.push(response.data.data);
						});
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
					}
					if(args.type && args.type === "signupSlotDelete"){
						SignupSlot.delete(args.signupSlot.id,function(response){
							var index = $scope.eventOccation.signupSlots.indexOf(args.signupSlot);
							$scope.eventOccation.signupSlots.splice(index,1);
						});
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
					}
					if(args.type === "cancelClicked"){
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
						event.preventDefault();
					}
					event.preventDefault();
				});
			},
			scope : {
				eventOccation : "=",
				timespans :"=",
				slotTypes :"="
			},
			replace : true,
			templateUrl : "event/templates/eventOccation.tpl.html",
			restrict : 'E'
		};
	})
	.directive("eventOccationForm",function($templateCache){
		return {
			controller : function($scope,EventOccation){
				var backup;
				$scope.localTimespans = angular.copy($scope.timespans);
				var customTimespan = {startsAt: "", endsAt: "" , name: "Anpassatt", isPublic: false, showInSchedule: true, id:0};
				
				if($scope.eventOccation){
					backup = angular.copy($scope.eventOccation);
					
					if($scope.eventOccation.timespan && $scope.eventOccation.timespan.isPublic === false){
						customTimespan = $scope.eventOccation.timespan;
						$scope.startsAt = new Date(moment(customTimespan.startsAt).format("YYYY-MM-DD HH:mm"));
						$scope.endsAt = new Date(moment(customTimespan.endsAt).format("YYYY-MM-DD HH:mm"));
						$scope.showInSchedule = customTimespan.showInSchedule;
					}
					
					
					$scope.SaveLabel = "spara";
					$scope.onSaveClick = function(){
						if(!$scope.eventOccation.timespan.isPublic){
							$scope.eventOccation.timespan.startsAt =  moment($scope.startsAt).format("YYYY-MM-DD HH:mm");
							$scope.eventOccation.timespan.endsAt =  moment($scope.endsAt).format("YYYY-MM-DD HH:mm");
							$scope.eventOccation.timespan.showInSchedule = $scope.showInSchedule;
							$scope.eventOccation.timespan.name = "Anpassat";
						}
						$scope.$emit("eventOccationFormEvent", { type:"eventOccationUpdate", eventOccation : $scope.eventOccation });
						$scope.eventOccationForm.$setPristine();
					};
					$scope.onDeleteClick = function(){
						if(confirm("Är du säker?")){
							$scope.$emit("eventOccationFormEvent", { type:"eventOccationDelete", eventOccation : $scope.eventOccation });
							$scope.eventOccationForm.$setPristine();
						}
					};
				}
				else{
					$scope.eventOccation = { name : "", timespan : null, isHidden : false, id : 0, signupSlots : [] };
					backup = angular.copy($scope.eventOccation);
					$scope.SaveLabel = "skapa";
					$scope.onSaveClick = function(){
						var newOccation = angular.copy($scope.eventOccation);
						if(!newOccation.timespan.isPublic)  {
							// custom form
							newOccation.timespan = {
								startAt : moment($scope.startsAt).format("YYYY-MM-DD HH:mm"),
								endsAt : moment($scope.endsAt).format("YYYY-MM-DD HH:mm"),
								isPublic: false,
								showInSchedule : $scope.showInSchedule,
								name : "Anpassat"
							};
							
						}
						$scope.$emit("eventOccationFormEvent", { type:"eventOccationCreate", eventOccation : newOccation });
						angular.copy(backup,$scope.eventOccation);
						$scope.eventOccationForm.$setPristine();							
					};
				}
				$scope.localTimespans.splice(0,0,customTimespan);
				$scope.onCancelClick = function(){
					angular.copy(backup,$scope.eventOccation);
					$scope.eventOccationForm.$setPristine();
					$scope.$emit("eventOccationFormEvent", { type:"cancelClicked" });
				};
				$scope.eventOccation.isHidden = $scope.eventOccation.isHidden ? true : false;
				
				
			},
			scope : {
				eventOccation : "=?",
				timespans :"="
			},
			replace : true,
			templateUrl : "event/templates/eventOccationForm.tpl.html",
			restrict : 'E'
		};
	})
	.directive("signupSlot",function($templateCache){
		return {
			controller : function($scope,Signups,SignupSlot){
				$scope.toggleEdit = function($event){ $event.preventDefault();};
				$scope.toggleNewForm = function($event){ $event.preventDefault();};
				$scope.showEditForm = false;
				$scope.$on("signupSlotFormEvent",function(event,args){
					if(args.type === "cancelClicked"){
						$scope.showEditForm = false;
						event.preventDefault();
					}
					else if(args.type === "signupSlotUpdate"){
						SignupSlot.save($scope.signupSlot,function(response){
							$scope.signupSlot = response.data.data;
							$scope.showEditForm = false;
						});
						event.preventDefault();
					}
				});
				$scope.deleteSignup = function (signup){
					if(confirm("Är du säker?")){
						Signups.deleteSignup(signup.id,function(){
							var index = $scope.signupSlot.signups.indexOf(signup);
							$scope.signupSlot.signups.splice(index,1);
						});
					}
				};
				
				var savePersonalSignup = function(entities){
					Signups.savePersonalSignup(entities[0].id, $scope.signupSlot.id,function(response){
						$scope.signupSlot.signups.push(response.data);
					});
				};
				
				var saveGroupSignup = function(entities){
					Signups.saveGroupSignup(entities[0].id, $scope.signupSlot.id, function(response){
						$scope.signupSlot.signups.push(response.data);
					});
				};
				
				$scope.selectionScope = $scope.signupSlot.slotType.cardinality > 1 ? "groups" : "people";
				$scope.selectorHeader = "Sök efter " + $scope.signupSlot.slotType.name.toLowerCase()+ ".";
				$scope.onSelectEntity = function(entities){
					if($scope.signupSlot.slotType.cardinality > 1){
						saveGroupSignup(entities);
					}else{
						savePersonalSignup(entities);
					}
				}; 
			},
			scope : {
				signupSlot : "=",
				slotTypes :"="	
			},
			replace : true,
			templateUrl : "event/templates/signupSlot.tpl.html",
			restrict : 'E'
		};
	})
	.directive("signupSlotForm",function($templateCache){
		return {
			controller : function($scope){
				var backup;
				if($scope.signupSlot.id){
					$scope.saveLabel = "spara";
					$scope.onSaveClick = function(){
						$scope.$emit("signupSlotFormEvent", { type:"signupSlotUpdate", signupSlot : $scope.signupSlot });
						$scope.signupSlotForm.$setPristine();
					};
					$scope.onDeleteClick = function(){
						if(confirm("Är du säker?")){
							$scope.$emit("signupSlotFormEvent", { type:"signupSlotDelete", signupSlot : $scope.signupSlot });
						}
					};
				}else{
					$scope.saveLabel = "skapa";
					$scope.onSaveClick = function(){
						$scope.$emit("signupSlotFormEvent", { type:"signupSlotCreate", signupSlot : $scope.signupSlot });
						$scope.signupSlot = backup;
						$scope.signupSlotForm.$setPristine();
					};
				}
				backup = angular.copy($scope.signupSlot);
				
				$scope.onCancelClick = function(){
					$scope.signupSlot = backup;
					$scope.signupSlotForm.$setPristine();
					$scope.$emit("signupSlotFormEvent", { type:"cancelClicked" });
				};
				
			},
			scope : {
				slotTypes :"=",
				signupSlot : "=",
			},
			replace : true,
			templateUrl : "event/templates/signupSlotForm.tpl.html",
			restrict : 'E'
		};
	})
	.directive("eventSignups",function($templateCache){
		return {
			controller : function($scope,Event,EventOccation){
				$scope.showNewForm = $scope.currentEvent.eventOccations.length === 0;
				$scope.$on("eventOccationFormEvent",function(event,args){
					if(args.type && args.type === "eventOccationCreate"){
						Event.createEventOccation($scope.currentEvent.id,args.eventOccation,function(response){
							$scope.currentEvent.eventOccations.push(response.data.data);
							event.preventDefault();
							$scope.showNewForm = false;							
						});
					}else if(args.type && args.type === "cancelClicked"){
						$scope.showNewForm = false;
						event.preventDefault();
					}else if(args.type && args.type === "eventOccationDelete"){
						EventOccation.delete(args.eventOccation.id,function(response){
							var index = $scope.currentEvent.eventOccations.indexOf(args.eventOccation);
							$scope.currentEvent.eventOccations.splice(index,1);
							event.preventDefault();
						});
					}
				});
			},
			scope : {
				currentEvent : "=",
				timespans :"=",
				slotTypes :"="
			},
			replace : true,
			templateUrl : "event/templates/signups.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventBookingsSchedule",function(urls){
		return {
			controller : function($scope,Event,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addBookings(events,conventionStartsAt){
					var eventBoxes = [];

					angular.forEach(events,function(event){
						var eventBox = {
							name : event.name,
							id : event.id,
							resources : []
						};
						
						// add resources to the event box
						angular.forEach(event.resources,function(resource){
							
							// create a resource
							var resourceBox = {
								name : resource.name,
								id: resource.id,
								bookings : []
							};
							// add bookings to the resource
							angular.forEach(resource.bookings,function(booking){
								resourceBox.bookings.push({
									name : booking.name,
									id : booking.id,
									width: getOccationWidth(booking.startsAt,booking.endsAt),
									offset: getOccationOffset(booking.startsAt,conventionStartsAt)
								});
							});
							// add the resource to the event
							eventBox.resources.push(resourceBox);
						});
						// add the event box to the main event list
						eventBoxes.push(eventBox);
					});
					$scope.events = eventBoxes;
				}
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				Event.getResourceBookingScheduleData(function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.data.publicTimespans;
					var events = response.data.data.events;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addBookings(events,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "event/templates/eventBookingsSchedule.tpl.html",
            restrict : 'E'
		};
	})
	.directive("eventOccationDialogue",function(){
		return {
			controller : function($scope){
				console.log($scope.event);
				console.log($scope.timespan);
			},
			scope : {
				timespan : "=",
				event : "="
			},
			replace : false,
			templateUrl : "event/templates/eventOccationDialogue.tpl.html",
			restrict : 'E'
		};
	})
	
	.directive("event",function(){
		var extractIdRegex = /^\/event\/(\d+|new|bookingSchedule)(\.|\/|$)/;

		var signupsRegex = /^\/event\/(\d+)([^/]*)\/signups$/;	
		var deleteRegex = /^\/event\/(\d+)([^/]*)\/signups$/;	

		var initialData;
		return {
			controller : function($scope,$location,gcTabsBus,Event,$element,$compile){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.formMetaData = {
						eventTypes : [],
						slotTypes : [],
						timespans : []
					};
					Event.formMetaData(function(response){
						$scope.formMetaData.eventTypes = response.data.data.eventTypes;
						$scope.formMetaData.slotTypes = response.data.data.slotTypes;
						$scope.formMetaData.timespans = response.data.data.timespans;
					});
					
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
					
					$scope.$on('$locationChangeSuccess',onLocationChange);

				}

				function onLocationChange(){
					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						onShowNewForm();
						return;
					}
					else if(id === "bookingSchedule")
					{
						onShowBookingSchedule();
						return;
					}
					else{
						onShowDetails(id);
						return;
					}

				}

				function onShowBookingSchedule(){
					gcTabsBus.setTabs([]);
					$scope.title = "Arrangemangslista";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);					
					$scope.eventList = [];
					$scope.currentEvent = undefined;
					$scope.view = "<event-bookings-schedule></event-bookings-schedule>";
					return;
				}
				
				function onShowList(){
					gcTabsBus.setTabs([]);
					$scope.title = "Arrangemangslista";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);					
					Event.query(
						function(response){
							$scope.eventList = response.data.data;
							$scope.currentEvent = undefined;
							$scope.view = "<event-list events='eventList'></event-list>";
						}
					);
					return;
				}

				function onShowNewForm(){
					$scope.currentEvent = {};
					$scope.title = "Nytt arrangemang";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);
					$scope.view = "<event-details current-event='currentEvent' event-types='formMetaData.eventTypes'/>";
				}

				function onShowDetails(id){
					var newView = "";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'event/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'}, 
						{ id : 'event/' + id + '/delete', 'label' : 'Ta  bort'}]);
					switch(true){
						case signupsRegex.test($location.path()) :
							newView = "<event-signups current-event='currentEvent' timespans='formMetaData.timespans' slot-types='formMetaData.slotTypes'/>";
							break;
						case deleteRegex.test($location.path()):
							newView = "<event-delete current-event='currentEvent'/>";
							break;
						default:
							newView = "<event-details current-event='currentEvent' event-types='formMetaData.eventTypes'/>";
					}

					if(!$scope.currentEvent || "" + $scope.currentEvent.id !== "" + id){
						$scope.title = "Hämtar detaljer för arrangemang # " + id + "...";
						Event.get(id,function(response){
							$scope.currentEvent = response.data.data;
							$scope.title = "Detaljer för " + $scope.currentEvent.name;
							$scope.view = newView;
						});
					}else {
						if($scope.currentEvent){
							$scope.title = "Detaljer för " + $scope.currentEvent.name;
						}
						$scope.view = newView;
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "event/templates/event.tpl.html",
			restrict : 'E'				
		};
	});
}());