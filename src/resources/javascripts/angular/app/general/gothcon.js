(function(){
/*jshint strict:false */
"use strict";
	
angular.module("GothCon",['angularMoment','ui.bootstrap','Person', 'Resource','Group','Event','EventType','Timespan','ui.bootstrap.datetimepicker'])
	.provider("urls", function urlsProvider(){
		var angularAppUrl 	=  "resources/javascripts/angular/app/";
		var templatesUrl 	=  "resources/javascripts/angular/app/templates/";
		var directivesUrl 	=  "resources/javascripts/angular/app/directives/";
		var applicationUrl 	=  "/webreg/";
		
        return{
			setApplicationUrl: function(url){
				applicationUrl = url;
			},			
			getApplicationUrl : function(){
				return applicationUrl;
			},	
			setAngularAppUrl: function(url){
				angularAppUrl = url;
			},			
			getAngularAppUrl : function(){
				return angularAppUrl;
			},	
			setTemplatesUrl : function(url){
				templatesUrl = url;
			},			
			getTemplatesUrl : function(){
				return applicationUrl + templatesUrl;
			},			
			setDirectivesUrl : function(url){
				directivesUrl = url;
			},			
			getDirectivesUrl : function(){
				return applicationUrl + directivesUrl;
			},
            $get : function urlsFactory(){
                return {
					getAngularAppUrl : function(){
						return angularAppUrl;
					},
					getApplicationUrl : function(){
						return applicationUrl;
					},
					getDirectivesUrl : function(){
						return applicationUrl + directivesUrl;
					},
					getTemplatesUrl : function(){
						return applicationUrl + templatesUrl;
					}
				};
            }
        };
    })
    .run(function(amMoment,urls) {
        amMoment.changeLanguage('sv');
		tinyMCE.baseURL = urls.getApplicationUrl() + 'resources/javascripts/library/tiny_mce4';
		tinyMCE.suffix = '.min';
		tinyMCE.plugins = "pagebreak,table,image,link,preview,contextmenu,advlist";
		tinyMCE.toolbar = "undo,redo,|,link,unlink,anchor,|,bold,italic,blockquote,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,|,table";
		tinyMCE.menubar = false;
    })
    .config(function($locationProvider){
        $locationProvider.html5Mode(true);
    })
    .directive("gcTabs", function(urls){
        return {
            controller : function($scope,gcTabsBus,$location){
                $scope.$watch(gcTabsBus.getTabs(),function(){
                   $scope.tabs = gcTabsBus.getTabs(); 
                });

                $scope.isTabActive = function(tab){
                    return  $location.path() === "/" + tab.id || $location.path()+ "/" ===  "/" + tab.id ;
                };
            },
            templateUrl : "directives/tabs/tabs.tpl.html",
            restrict : 'AE'
        };
    })
	.provider("gcTabsBus", function PeopleRepositoryProvider(){

        function Tab(_id,_label){
                this.id = _id;
                this.label = _label;
                return this;
            }    

        return{
            $get : function PeopleViewsFactory(){
                var _tabs = [];
                return{
                    getTabs : function(){
                        return _tabs;
                    },
                    setTabs : function(tabs){
                        _tabs.length = 0;
                        for(var key in tabs){
                            _tabs.push(new Tab(tabs[key].id, tabs[key].label));
                        }
                    }
                };
            }
        };
    })
    .provider("gcMessageBus", function gcMessageBusProvider(){
        return{
            $get : function gcMessageBus(){
                var _message = "";
                return{
                    getMessages : function(){
                        return [_message];
                    },
                    addMessage : function(message){
                        _message = message;
                    }
                };
            }
        };
    })
	.provider("linkBuilder", function LoaderBusRepository(urlsProvider){
        return {
            $get: function LoaderBusFactory(){
                return {
                    createAdminLink : function(path){
						return urlsProvider.getApplicationUrl() + "administrator/" + path;
                    },
                    createOrganizerLink : function(path){
						return urlsProvider.getApplicationUrl() + "organizer/" + path;
                    },
                    createPublicLink : function(path){
						return urlsProvider.getApplicationUrl() + path;
                    }
                };
            }
        };
    })
	.directive("gcMenu",function(){
		return {
			link : function(scope,elem){
				elem.find("a").attr("target", "_self");
			},
			controller : function(){
            },
			replace : false,
            restrict : 'A'
        };
	})
	.directive("sortableCollection",function($filter){
		var orderByFilter = $filter("orderBy");

		return {
			controller : function($scope){
				
				this.currentlySortedBy = "";
				this.reverseSort = false;
				
				this.sort = function(collectionName,sortBy){
					var properties = sortBy.split(",");
					if(sortBy === this.currentlySortedBy){
						this.reverseSort = !this.reverseSort;
					}else{
						this.reverseSort = false;
					}
					this.currentlySortedBy = sortBy;
				    var getSortValue = function(element){
						var values = [];
						for(var pkey in properties){
							var currentValue = element;
							var propertyPath = properties[pkey].split(".");
							for(var pathSegmentKey in propertyPath){
								var pathSegment = propertyPath[pathSegmentKey];
								currentValue = currentValue[pathSegment];
							}
							values.push(currentValue);
						}
						return (values.length === 1) ? values[0] : values.toString();
					};
					$scope[collectionName] = orderByFilter($scope[collectionName],getSortValue,this.reverseSort);
					$scope.$apply();
				};
            },
			
			replace : false,
            restrict : 'A'
        };
	})
	.directive("sortOnClick",function(){
		var regexp = /^([a-zA-Z]*) by ([a-zA-Z, _.]*)$/;
		return {
            link : function(scope,elem,attrs,controllerInstance){
				var splitParams = regexp.exec(attrs.sortOnClick);
				var collectionName = splitParams[1];
				var collectionSortBy = splitParams[2];
				
				elem.bind("click",function(){
					controllerInstance.sort(collectionName,collectionSortBy);
				});
				var arrowElement = angular.element("<i></i>");
				if(!elem.hasClass("clickable")){
					elem.attr("class", elem.attr("class") + " clickable");
				}
				elem.append(arrowElement);

				scope.isSortedByThisProperty = function(){
					return collectionSortBy === controllerInstance.currentlySortedBy;
				};
				
				scope.isSortedInReverse = function(){
				 	return controllerInstance.reverseSort;
				};
				
				function updateSortDirectionArrow(){
					if(scope.isSortedByThisProperty()){
						if(scope.isSortedInReverse()){
							arrowElement.attr("class","fa fa-arrow-up");
						}else{
							arrowElement.attr("class","fa fa-arrow-down");
						}
						arrowElement.show();
					}else{
						arrowElement.hide();
					}
				}
				
				scope.$watch(scope.isSortedByThisProperty, function(){
					updateSortDirectionArrow();
				});

				scope.$watch(scope.isSortedInReverse, function(){
					updateSortDirectionArrow();
				});

				
			},
			controller : function(){
				
            },
			scope : {},
			require : '^sortableCollection',
			replace : false,
            restrict : 'A'
        };
	})
	.directive('gcMustMatch',function(){
        return{
            require : 'ngModel',
            link: function(scope,elem,attr,ctrl){
                var theForm = elem[0].form;
                if(!theForm && elem.parent()){
                    var parent = elem.parent();
                    while(parent[0]){
                        if(parent.attr('ng-form') !== undefined || parent.hasClass('ng-form')){
                            theForm = scope[parent.attr("name")];
                            break;
                        }
                        parent = parent.parent();
                    }
                }else{
                    theForm = scope[angular.element(theForm).attr("name")];
                }
                if(!theForm){
                    throw("Cannot find the parent form");
                }

                var theOther;

                ctrl.$parsers.unshift(function(value) {
                    if(!theOther){
                        theOther = theForm[attr.gcMustMatch];
                    }
                    ctrl.$setValidity('match', theOther && value === theOther.$modelValue);
                    return value;
                });

                ctrl.$formatters.unshift(function(value) {
                    if(!theOther){
                        theOther = theForm[attr.gcMustMatch];
                    }
                    ctrl.$setValidity('match', theOther && value === theOther.$modelValue);
                    return value;
                });

                var addParserToOther = function(){
                        theOther = theForm[attr.gcMustMatch];
                        theOther.$parsers.unshift(function(value) {
                                ctrl.$setValidity('match', value === ctrl.$modelValue);
                                return value;
                        });
                        theOther.$formatters.unshift(function(value) {
                                ctrl.$setValidity('match', value === ctrl.$modelValue);
                                return value;
                        });
                };

                var unwatch = scope.$watch(theForm.$name +"."+ attr.mustMatch, function(){
                        addParserToOther();
                        unwatch();
                });

            }
        };
    })
	.directive("gcLoader", function(){
            return {
            controller : function gcLoaderController($scope,gcLoaderBus){
                $scope.isLoading = function(){
                    return gcLoaderBus.instances() > 0;
                };
            }, 
            template : "<div id='loader-container'><div id='loader' ng-show='isLoading()'>jobbar...</div></div>",
            restrict : 'EA',
            replace : true
       };
    })
    .provider("gcLoaderBus", function LoaderBusRepository(){
        var instances = 0;
        return {
            $get: function LoaderBusFactory(){
                return {
                    add : function(){
                        instances++;
                    },
                    remove : function(){
                        instances--;
                    },
                    instances : function(){
                        return instances;
                    },
                    isLoading : function(){
                        return instances.length > 0;
                    }
                };
            }
        };
    })
	.directive("gcSearch",function(){
		return {
			link: function(scope,elem,attr){
				elem.find('input')[0].focus();
			},
			controller : function($scope,People,$attrs){
				$scope.selectionScope = $scope.selectionScope || "people";
				$scope.searchState = "";
				$scope.$watch("searchTerm",function(newVal,oldVal){
					if(newVal !== undefined && $scope.selectForm && $scope.selectForm.$valid){
						$scope.searchState = "söker efter \"" + $scope.searchTerm + "\"...";
						People.search($scope.searchTerm,$scope.selectionScope, function(response){
							if(response.data.data.length === 0){
								$scope.searchState = "hittade inga träffar";
							}
							else{
								$scope.searchState = "hittade " + response.data.data.length + " träffar";
								$scope.searchResult = response.data.data;
							}
						});
					}
				});
			},
			scope : {
				searchResult : "=?",
				searchState : "=?",
				selectionScope : "=?"
			},
			template : "<span><input ng-form='selectForm' type='text' placeholder='namn...' ng-model='searchTerm' ng-model-options=\"{ debounce: {'default': 500, 'blur': 0} }\" /><br/><span class='search-status'>{{searchState}}</span></span>",
			restrict : "E",
			replace: true
		};		
	})
//	.controller("peopleSearchController",function($scope,People){
//		$scope.noResults = false;
//		$scope.searchTerm = "";
//		var timer = null;
//		
//		$scope.$watch("searchTerm",function(newVal,oldVal){
//			if($scope.form.$valid){
//				if(timer !== null){
//					clearTimeout(timer);
//				}
//				timer = setTimeout( function(){
//						People.search($scope.searchTerm,function(response){
//							if(response.data.data.length === 0){
//								$scope.noResults = true;
//							}else{
//								$scope.noResults = false;
//								$scope.people = response.data.data;
//							}
//						});
//					},300);
//			}
//		});
//		
//		$scope.handleGetAll = function(){
//			People.search($scope.searchTerm,function(response){
//				if(response.data.data.length === 0){
//					$scope.noResults = true;
//				}else{
//					$scope.noResults = false;
//					$scope.people = response.data.data;
//				}
//			});
//		};
//	})
	.directive("gcSelector",function(urls){
		return {
			link : function(scope,element,attributes){
				if(attributes.multiSelect !== undefined && attributes.multiSelect !== "false"){
					scope.selectMultiple = true;
					scope.listItemContent = urls.getDirectivesUrl() + 'peopleSelector/checkbox.tpl.html';
				}else{
					scope.selectMultiple = false;
					scope.listItemContent = urls.getDirectivesUrl() + 'peopleSelector/radio.tpl.html';
				}
				scope.dialogueTemplate = urls.getDirectivesUrl() + 'peopleSelector/selectorDialogue.tpl.html';
			},
            controller : function($scope,ngDialog,$element){
				
				var selectedEntities = [];
				$scope.selectionScope = $scope.selectionScope || "people,groups";
				$scope.dialogHeader = $scope.dialogHeader || "Sök efter person";
				
				$scope.onSaveSelection = $scope.onSaveSelection || function(){ return true; };
				$element.bind("click",function(){
					
					if($scope.selectMultiple){
						$scope.handleClick = function($event,entity){
							var index = selectedEntities.indexOf(entity);

							if($event.srcElement.checked && index < 0){
								selectedEntities.push(entity);
							}
							else if(!$event.srcElement.checked && index >= 0){
								selectedEntities.splice(index,1);
							}
						};
					}
					else{
						$scope.handleClick = function($event,entity){
							selectedEntities = [entity];
						};
					}
					
					$scope.saveSelection = function(){
						$scope.onSaveSelection({ selection : selectedEntities});
						selectedEntities = [];
					};
					
					ngDialog.open({
						template : $scope.dialogueTemplate,
						className: 'ngdialog-theme-default',
						scope : $scope
					});
				});
            },
			scope : {
				onSaveSelection : "&onSaveSelection",
				dialogHeader : "=?",
				selectionScope : "=?"
			},
			replace : false,
            restrict : 'A'
        };
	})
	.factory("gcLoaderBusInterceptor", function($q, gcLoaderBus) {
		var errorNo = 0;
		
		return {
			'request': function(config) {
			  gcLoaderBus.add();
			  return config;
			},
		  
			'requestError': function(rejection) {
			   // do something on error
			   // if (canRecover(rejection)) {
			   // 	return responseOrNewPromise;
			   // }
			   return $q.reject(rejection);
			 },

			'response': function(response) {
			  gcLoaderBus.remove();
			  // do something on success
			  return response;
			},

		   'responseError': function(rejection) {
				gcLoaderBus.remove();
				// do something on error
				//alert("Something went wrong. Please look in the console for more details (error " + ++errorNo + ")");
				alert(rejection.data);
				console.log("error (" + errorNo + ")");
				console.log(rejection);
				return $q.reject(rejection);
			}
		};
	  });
}());