(function(){
/*jshint strict:false */
"use strict";

angular.module("Timespan",['GothCon'])
	.provider("TimespanRepository", function TimespanRepositoryProvider(){
		var _resource;

		return{
			$get : function TimespanRepository($resource,linkBuilder,gcLoaderBusInterceptor){
				_resource = $resource(linkBuilder.createAdminLink("timespan") + "/:id",{},{	
					'get'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'delete'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor }
				});

				return {
					'get'	: _resource.get,
					'delete': function(timespanId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id : timespanId},callback);
					},
					'save'	: function(timespanData,callback){
						callback = callback || angular.noop;
						var urlParams = {};
						if(timespanData.id){
							urlParams.id = timespanData.id;
						}
						return _resource.save(urlParams,timespanData,callback);
					},
					'getAll': _resource.get,
					'createNew' : function(){
						var now = moment().format("YYYY-MM-DD HH:mm");
						
						return {
							startsAt : now ,endsAt : now,name : "", isPublic : true
						};
					}
				};
			}
		};
	})
	.directive("timespan",function(){
		var extractIdRegex = /^\/timespan\/(\d+|new)(\/|$)/;
		var initialData;
		return {
			controller : function($scope,$location,gcTabsBus,TimespanRepository,$element,$compile){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
					$scope.$on('$locationChangeSuccess',onLocationChange);
				}

				function onLocationChange(){
					var match = extractIdRegex.exec($location.path());
					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						onShowNew();
						return;
					}
					else{
						onShow(id);
						return;
					}

				}

				function onShowList(){
					$scope.title = "Intervall";
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/new', 'label' : 'Nytt intervall'}]);
					$scope.currentTimespan = undefined;
					$scope.timespanList = TimespanRepository.getAll({},function(response){
						$scope.timespanList = response.data.data;
						$scope.view = "<timespan-list timespans='timespanList'></group-list>";
					});
					return;
				}

				function onShowNew(){
					$scope.currentTimespan = TimespanRepository.createNew();
					$scope.title = "Nytt intervall";
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/new', 'label' : 'Nytt intervall'}]);
					$scope.view = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
				}

				function onShow(id){
					var newView;
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/' + id + '/', 'label' : 'Detaljer'}]);
					switch($location.path()){
						case '/timespan/' + id +"/bookings":
							newView = "<timespan-bookings current-timespan='currentTimespan'></timespan-bookings>";
							break;
						case '/timespan/' + id :
							newView = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
							break;
						default:
							newView = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
					}

					if(!$scope.currentTimespan || "" + $scope.currentTimespan.id !== "" + id){
						TimespanRepository.get({ id : id},function(response){	
							$scope.currentTimespan = response.data.data;
							$scope.title = $scope.currentTimespan.name;
							$scope.view = newView;
						});
					}else{
						$scope.view = newView;
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "timespan/templates/timespan.tpl.html",
			restrict : 'E'				
		};
	})
	.directive("timespanList",function(){
		return {
			controller : function($scope,$location,gcTabsBus,TimespanRepository,$element,$compile){

			},
			replace : true,
			templateUrl : "timespan/templates/list.tpl.html",
			restrict : 'AE',
			scope : { 
				timespans : "=?"
			}
		};
	})
	.directive("timespanForm",function(){
		return {
			controller : function($scope,TimespanRepository,$location){
				$scope.startsAt = new Date($scope.currentTimespan.startsAt);
				$scope.endsAt = new Date($scope.currentTimespan.endsAt);
				$scope.saveLabel = $scope.currentTimespan.id ? "Uppdatera" : "Skapa";
				$scope.onSaveClick = function(){
					var timespanData = angular.copy($scope.currentTimespan);
					timespanData.startsAt = moment($scope.startsAt).format("YYYY-MM-DD HH:mm");
					timespanData.endsAt = moment($scope.endsAt).format("YYYY-MM-DD HH:mm");
					
					TimespanRepository.save(timespanData,function saveTimespanCallback(response){
						if(!timespanData.id){
							$location.url("/timespan");
						}
						console.log(response);
					});
				};
				if($scope.currentTimespan && $scope.currentTimespan.id){
					$scope.onDeleteClick = function(){
						TimespanRepository.delete($scope.currentTimespan.id,function deleteTimespanCallback(response){
						
						});
					};
					$scope.onCancelClick = function(){
						
					};
				}else{
					$scope.currentTimespan = TimespanRepository.createNew();
				}
			},
			replace : true,
			templateUrl : "timespan/templates/form.tpl.html",
			restrict : 'AE',
			scope : { 
				currentTimespan : "=?"
			}
		};
	});
}());