/*! GothCon - v1.0,0 - 2016-09-17
* Copyright (c) 2016 Joakim Ekeblad; Licensed  */
(function($){

	var defaultOptions = {
		createMembershipUrl : "/admin/membership/new?format=json",
		triggerSelector : ".addMembershipButton",
		onSaveCallback : function(){}
	};

	$.fn.groupMembershipSignup = function(optionsParam){
		
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
	
		var options = $.extend({},defaultOptions,optionsParam);
		var group_id = options.group_id;
		var createMembershipUrl = options.createMembershipUrl;
		
		$this.each(function(index,container){
			
			var $container = $(container);
			var $trigger = $container.find(options.triggerSelector);
			var repository_type = "people";
			var searchUrl = options.searchUrl;

			function onSelect(data){
				var entityIDs = [];
				var i =0;
				for(var key in data){
					entityIDs[i++] = data[key].id;
				}
				$.ajax({
					  url: createMembershipUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"group_id" : group_id , "entityIDs" : entityIDs, "signup_type" : repository_type},
					  success: options.onSaveCallback
				});
			}
			
			$trigger.ajaxSelector({"serviceUrl" : searchUrl, "callback" : onSelect, "multiSelect" : false});
			
		});
		return $this;
	};
}(jQuery));
(function($){

    var defaultOptions = {
        serviceUrl: '/ajax.php',
        searchDelay: 200,
        maxNoOfResult: 10,
        mininmumQueryLength : 0,
        callback: function(){},
    	queryToShortCallback : function(){}
    };


	$.fn.ajaxChecker = function(optionsParam){

		var $this = $(this);

		if($this.length === 0){
			return;
		}

		var options = $.extend({}, defaultOptions, optionsParam );

		function _init(){
			// gå igenom alla som matchar
			 $this.each(function(key,item){
			 	 var $checkBox = $(item);

			 	 $checkBox.bind("click",_doAjaxCall);
			 });
		}

		function _doAjaxCall(event){
			var $checkbox = $(event.target);
			var anmalning_id = $checkbox.val();
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
  				  method : 'post',
				  data: {"anmalning_id" : anmalning_id},
				  success: (function(caller){
				  	return function(responseData){
						console.log("This is the default response handler of ajax checker");
						console.log(responseData);
					};
				  }($checkbox))
			};
			var $call = $.ajax(argumentObject);
		}

		_init();
	};
})(jQuery);
(function($){
	
	var defaultOptions = {returnDataType:"html", deleteQuestion :"Are U sure?"}	;

	$.fn.ajaxDelete = function(optionsParam){
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
		var options = $.extend({},defaultOptions,optionsParam);
		
		var processLink = function($link){
		
			var url = $link.attr("href");
			if(url.indexOf("format=json")=== -1){
				url += (url.indexOf("?") === -1 ? "?" : "&") + "format=json";
			}
			$link.removeProp("onclick");
			$link.bind("click",function(event){
				event.preventDefault();
				event.stopPropagation();
				if(confirm(options.deleteQuestion)){
					$.ajax({
						  url: url,
						  dataType: options.returnDataType,
						  success: options.deleteCallback,
						  type: "get"
					});
				}
				return false;
			});
		};
		
		function init(){
			
			$this.each(function(key,obj){
				var $obj = $(obj);
				if($obj[0].tagName === "A" || $obj[0].tagName === "a"){
					processLink($obj);
				}else{
					$obj.find("a").each(function(innerKey,innerObj){
						processLink($(innerObj));
					});
				}
			});
		}
		init();
	};
}(jQuery));
(function($){
	var defaultOptions = { 
		"deleteCallback" : function(data){
			console.log("done deleting");
		},
		"deleteUrlTemplate" : "/admin/resourcebooking/{:resource_booking_id}/delete?format=json",
		"deleteAlertMessage" : $.translate("resources/deleteBookingWarning")
	};
	$.fn.ajaxDeleteResourceBooking= function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $button = $(obj).hide();
			var pattern = new RegExp("/(\\d+)/", "g");
			
			var resource_booking_id = pattern.exec($button.attr("href"))[1];
			var $tr = $button.closest("tr");
			$tr.hover(function(){
				$button.show();
			},function(){
				$button.hide();
			});
			$button.prop("onclick",null);
			$button.click(function(event){
				event.preventDefault();
				event.stopPropagation();
				if(confirm(options.deleteAlertMessage)){
					var deleteUrl = options.deleteUrlTemplate.replace("{:resource_booking_id}",resource_booking_id);
					
					console.log(deleteUrl);
					$.ajax({
						  url: deleteUrl,
						  success: function(){
							$tr.remove();
						  },
						  type:"get"
					});
				}else{
					console.log("no deletion");
				}
				return false;	
			});
		});
		return $(this);
	};
}(jQuery));



(function($){
	
	var defaultOptions = {
		"ajaxStatusBoxSelector" : "#ajaxStatusBox",
		"saveCallback" : function($responseObject,$form){},
		"preventBuiltInCallback" : false,
		"submitButtonSelector" : "input[type=submit]",
		"returnDataType" : "json"
	};	
	var helperFunctions = {};
	


	$.fn.ajaxForm = function(optionsParam){
		var globalLogger = $.logger.create();
		var $all = $(this);
		if($all.length < 1){
			return $all;
		}
		var options = $.extend({},defaultOptions,optionsParam);
		var $ajaxStatusBox = $(options.ajaxStatusBoxSelector);
		var statusBoxIsHidden = true;
		function init(){
			$all.each(function(key,obj){
				var $form = $(obj);
				var $submitButton = $form.find(options.submitButtonSelector);
				$submitButton.prop("onclick", null);
				$submitButton.unbind("click.ajaxForm");
				$submitButton.bind("click.ajaxForm",createSubmitClickHandler($form));
			});
		}
		
		function doSaveAjaxCall($form,data){
			
			var url = ($form.attr("action") || document.location.href);
			if(url.indexOf("format=json")=== -1){
				url += (url.indexOf("?") === -1 ? "?" : "&") + "format=json";
			}
			
			var callback = function(data){
				if(!options.preventBuiltInCallback){
					$.log(data.message);
				}
				options.saveCallback($form,data);
			};
			$.ajax({
				  url: url,
				  dataType: options.returnDataType,
				  data: data,
				  success: callback,
				  type: $form.attr("method") || "post"
			});
		}
		
		
		
		function createSubmitClickHandler($form){
			return function(event){
				event.preventDefault();
				event.stopPropagation();
				var data = {};
				
				$($form[0].elements).each(function(key,input){
					var $input = $(input);
					if($input.attr("disabled")){
						return 1;
					}
					var type = $input.attr("type");
					var name = $input.attr("name");
					var value;
					if(name){
						switch(type){
							case "checkbox":
								if($input.attr('checked')?true:false){
									value = $input.val();
								}else{
									value = null;
								}
								break;
							case "radio":
								if(!(name in data)){
									value = null;
								}
								if($input.attr('checked')?true:false){
									value = $input.val();
								}else{
									value = undefined;
								}
								break;
							case "select":
								if($input.checked){
									value = $input.val();
								}
								break;
							case "submit":
							case "button":
							break;
							default:
								value = $input.val();
							break;
						}
						if(value !== undefined){
							if(name && name.indexOf("[]") !== -1){
								if(!(name in data)){
									data[name] = [];
								}
								data[name][data[name].length] = value;
							}else{
								data[name] = value;
							}
						}
					}
				});
				
				doSaveAjaxCall($form,data);
				return false;
			};
		}
		
		init();
		return $all;
	};
}(jQuery));
(function($){

    var defaultOptions = {
        serviceUrl: '/ajax.php',
        searchDelay: 200,
        maxNoOfResult: 10,
        minimumQueryLength : 2,
        callback: function(){},
    	queryToShortCallback : function(){}
    };


	$.fn.ajaxSearcher = function(optionsParam){
		
		var $this = $(this);
		var recentSearchData;
		var recentSearchTerm;
		
		if($this.length === 0){
			return;
		}

		var options = $.extend({}, defaultOptions, optionsParam );
		var timer = null;

		function _init(){
			_addChangeHandler();
			if(options.minimumQueryLength < 1){
				var initialValue = $this.val();
				$this.val("");
				var timer = setTimeout(function(){_doAjaxCall(); $this.val(initialValue);},options.searchDelay);
			}
			// _doAjaxCall();
		}

		function _doAjaxCall(){
			
			var value = $this.val();
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
				  data: {"q" : value , "max" : options.maxNoOfResults, "sender" : $this.selector},
				  success: function(data){
					  recentSearchTerm = value;
					  recentSearchData = data;
					  options.callback(data);
				  }
			};
			var $call = $.ajax(argumentObject);
		}


		function _addChangeHandler(){
			$this.keyup(function(event){
				
				if((event.keyCode < 48 || (event.keyCode > 90 && event.keyCode < 96) || event.keyCode > 105) && event.keyCode !== 8){
					event.preventDefault();
					return;
				}else if(recentSearchData && $this.val().indexOf(recentSearchTerm) !== -1){
					//console.log("refine search");
					// refine search eg. search the already fetched data
					recentSearchTerm = $this.val();
					var pattern = new RegExp("(^| )" + recentSearchTerm,"i");
					var newSearchData = [];
					for(var key in recentSearchData.data){
						if(pattern.test(recentSearchData.data[key].label)){
							newSearchData[newSearchData.length] = recentSearchData.data[key];
						}
					}
					recentSearchData.data = newSearchData;
					options.callback(recentSearchData);
				}else if($this.val().length >= options.minimumQueryLength){
					if(timer != null){
						clearTimeout(timer);
					}
					timer = setTimeout(_doAjaxCall,options.searchDelay);
				}else if(recentSearchData){
					options.callback({data:[],message:null,status:-1});
				}else{
					//console.log("do nothing");
				}
			});
		}
		_init();
	};
})(jQuery);
(function($){

	var getCount = (function(){
		var _count = -1;
		return function(){
			_count +=1;
			return _count;
		};
	}());

	var defaultOptions = {
		callback : function(){},
		multiSelect : false,
		searchTerm : $.translate("search/search"),
		serviceUrl : "/ajax.php",
		maxNoOfResults:100,
		hideId : false,
		minimumQueryLength : 2,
		populateOnStart : true,
		hideSearchField: false
	};

	$.fn.ajaxSelector =  function(optionsParam){
		if(!optionsParam.serviceUrl){
			console.log("service url not set");
		}
		var options = $.extend( {} , defaultOptions, optionsParam );
		var $trigger = $(this);

		if($trigger.length === 0){
			return;
		}

		var id = "objectSelector_mainContainer_no" + getCount();
		var $container = $("<div id='"+ id +"' class='ajaxSelector_mainContainer'></div>");
		var $content = $("<div class='ajaxSelector_mainContent'></div>");
		
		function ajaxCallback(data){
			$list.html("");
			if(data.status === 0){
				objects = [];
				for(var key in data.data){
					var objectdata = data.data[key];
					objects[objectdata.id] = data.data[key];
					var idString = options.hideId ? "" : "" + objectdata.id + ". ";
					var inputType = options.multiSelect ? "checkbox" : "radio";
					var label = idString + objectdata.label + ((objectdata.type) ? " ("+objectdata.type + "s)" : "");
					var $listItem = $("<li><input type='"+inputType+"' name='ajaxSelector_objectid' value='" + objectdata.id + "'/>"+ label +"</li>").appendTo($list);
					$listItem.data("type",objectdata.type);
				}
				if(data.responsecode === 2){
					$list.append("<li>" + $.translate("search/moreThanXHits").replace("%S",options.maxNoOfResults) + "</li>");
				}
			}
		}
		
		
		if(!options.hideSearchField){
			var $searchField = $("<input type='text' class='ajaxSelector_searchField' value='"+options.searchTerm+"'/>");
			$searchField.bind("focus",function(){
				if($searchField.val() === options.searchTerm){
					$searchField.val("");
				}
			}).bind("blur",function(){
				if($searchField.val() === ""){
					$searchField.val(options.searchTerm);
				}
			});
			$content.append($searchField);
			
			var ajaxOptions = {
				minimumQueryLength : options.minimumQueryLength,
				serviceUrl : options.serviceUrl,
				maxNoOfResults : options.maxNoOfResults,
				callback : ajaxCallback,
				queryToShortCallback : function(){
					$list.html("");
				}
			};
			
			$searchField.ajaxSearcher(ajaxOptions);
		}
		
		var $list = $("<ul></ul>");
		var $okButton = $("<input class='ajaxSelector_okButton' type='button' value='ok'>");
		var $cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>");

		$okButton.click(function(){
			var selected = [];
			var selectorType = (options.multiSelect) ? "checkbox": "radio";

			$content.find("[type="+ selectorType +"]:checked").each(function(index,value){
				var val = $(value).val();
				selected[selected.length] = objects[val];
			});
			options.callback(selected,$trigger);
			$trigger.closeDOMWindow();
		});

		$cancelButton.click(function(){
			$trigger.closeDOMWindow();
		});

		$content.append($list).append($okButton).append($cancelButton);
		$container.append($content);
		$('body').append($container);

		var objects = [];

		$trigger.click( function(event){
			event.preventDefault();
			event.stopPropagation();
			$container.openDOMWindow({modal : true, width:350, height:450, windowSourceID : "#"+id });
		});
		
		if(options.populateOnStart){
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
				  success: ajaxCallback
			};
			var $call = $.ajax(argumentObject);
		}
		return $trigger;
	};
})(jQuery);
(function($){
	$.fn.autoSubmitOnEvent = function(eventName){
		eventName += ".autoSubmit";
		var $this = $(this);
		if($this.length > 0){
			$this.each(function(key,obj){
				var $obj = $(obj);
				var $submitButton = $obj.closest("form").find("input[type=submit]");
				$obj.unbind(eventName);
				$obj.bind(eventName,function(event){
					$submitButton.trigger("click");
				});
			});
		}
		return $this;
	};
}(jQuery));
(function($){
	var defaultOptions = {"headerIdentifier" : "h5", "fieldsetIdentifier" : ".formFieldset"};
	$.fn.eventOccationForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $formContainer = $(obj);
			var $header = $formContainer.find("h3");
			
			var $form = $formContainer.find("form.occationForm");
			
			if($form.length === 0){
				return true;
			}
			
			// ajaxify the forms
			$form.ajaxForm(
				{
					saveCallback:function(form,data){
						var name = data.data.name;
						$form.closest("li.pass").find("h3.passnamn").html(name);
					}
				}
			);
			// ajaxify the delete buttons
			var $deleteButton = $form.find(".deleteButton");
			$deleteButton.prop("onclick", null);
			$deleteButton.click(function(event){
				event.preventDefault();
				var url = $deleteButton.attr("href");
				url += (((url.indexOf("?") === -1) ? "?" : "&") + "format=json");
				$.ajax({
					  url: url,
					  dataType: 'json',
					  success: function(data){
						$form.closest("li").remove();
					  },
					  error:function(){
						console.log("something went wrong!!!");
					  },
					  type: "get"
				});
			});
			
			
			var $container = $($form.closest(".sektion")).hide();
			var $editLink = $("<a href='#' class='editLink'>"+$.translate("general/edit")+"</a>");
			$editLink.insertAfter($header);
			
			var enterEditState = function(event){
				event.preventDefault();
				$container.show();
				$editLink.unbind("click");
				$editLink.click(leaveEditState);
			};
			
			var leaveEditState = function(event){
				event.preventDefault();
				$container.hide();
				$editLink.unbind("click");
				$editLink.click(enterEditState);
			};
			
			$editLink.click(enterEditState);
		});
	};
}(jQuery));
(function($){

	var defaultOptions = {
		validateSignupUrlTemplate : "/admin/signup/{:signup_id}/save",
		saveUrlTemplate : "/admin/signup/new?format=json",
		deleteUrlTemplate : "/admin/signup/{:signup_id}/delete?format=json",
		searchUrlTemplate : "/admin/search/?r={:repository}&format=json",
		personLinkTemplate : "/admin/person/{:person_id}#Anmalningar_och_lagmedlemskap",
		triggerSelector : ".addSignupButton",
		tableSelector : ".signupTable",
		searchRepositoryInputSelector : "input[name=searchRepository]",
		signupSlotIdInputSelector : "input[name=signup_slot_id]",
		deleteButtonSelector : ".submitButtonLink",
		deleteAlertMessage : "Detta kommer att ta bort anmälningen, är du säker på att du vill fortsätta?",
		preventBuiltInCallback : false,
		ajaxStatusBoxSelector : "#ajaxStatusBox",
		deleteCallback : function(){}
	};
	
	var helperFunctions = {};

	//	saveSignupUrl : ,
	//	listUrl : ,
	/*
	eventSignup(
		"person",
		123,
		523,
		"signups_table_1",
		{}
	)
	*/
	$.fn.eventSignup = function(optionsParam){
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
		
		function createSignupUrl(urlTemplate){
			return urlTemplate;
		}
		
		function createSearchUrl(repository_type,urlTemplate){
			return urlTemplate.replace("{:repository}",repository_type);
		}

		function doDeleteAjaxCall(id,callback){
			var deleteUrl = options.deleteUrlTemplate.replace("{:signup_id}",id);
			$.ajax({
				  url: deleteUrl,
				  dataType: 'json',
				  data: {"signup_id" : id },
				  success: callback,
				  type:"delete"
			});
		}
		
		var options = $.extend({},defaultOptions,optionsParam);
		
		var saveSignupUrl = options.saveUrlTemplate;
		var $ajaxStatusBox = $(options.ajaxStatusBoxSelector);
		var statusBoxIsHidden = true;
		
		$this.each(function(index,container){
			var $container = $(container);
			var $trigger = $container.find(options.triggerSelector);
			var $signupTable = $container.find(options.tableSelector);
			var repository_type = $container.find(options.searchRepositoryInputSelector).val();
			var signup_slot_id = $container.find(options.signupSlotIdInputSelector).val();
			var searchUrl = createSearchUrl(repository_type,options.searchUrlTemplate);
			
			$container.find(options.deleteButtonSelector).each(function(key,obj){
				var $link = $(obj);
				var pattern = /(\d+)/g;
				var signupId = pattern.exec($link.attr("href"))[1];
				$link.prop("onclick", null);
				$link.click(createDeleteBtnClickCallback(signupId,$link));
			});
			
			$container.find(".signupForm").each(function(key,obj){
				var $this = $(obj); 
				var $submitButton = $this.find(".submitButton").hide();
				var $radio = $this.find("[name=is_approved],[name=is_paid]");
				
				$radio.each(function(key,obj){
					var $radio = $(obj);
					var $form = $radio.closest("form");
					$radio.change(function(event){
						$radio.closest("form").submit();
					});
				});
			});
			
			function createSignupRow(signup,slot,label){
				if(label === undefined){
					if(signup.group_id != null){
						label = signup.group.id  + ". " + signup.group.name;
					}else{
						label = signup.person.id + ". " + signup.person.first_name + " " + signup.person.last_name;
					}
				}
				var $tableRow = $("<tr></tr>");
				var $labelCell = $("<td>"+label+"</td>").appendTo($tableRow);
			
				var validationSaveUrl = options.validateSignupUrlTemplate.replace("{:signup_id}", signup.id);
			
				if(slot.requires_approval === "1"){
					var $validationCell = $("<td class='approvalCol'></td>").appendTo($tableRow);
					var $form = $("<form action='"+ validationSaveUrl +"' method='post' class='signupForm ajaxForm'>").appendTo($validationCell);
					var $checkbox = $("<input style='float:none; display:inline' type='checkbox' name='is_approved' value='1'/>").appendTo($form);
					$checkbox.change(function(event){
						$form.submit();
					});
				}else{
					$("<td class='approvalCol'> - </td>").appendTo($tableRow);
				}
				
				if(slot.signup_slot_article.article_id){
					var $paymentCell = $("<td class='approvalCol'></td>").appendTo($tableRow);
					var $paymentForm = $("<form action='"+ validationSaveUrl +"' method='post' class='signupForm ajaxForm'>").appendTo($paymentCell);
					var $paymentCheckbox = $("<input style='float:none; display:inline' type='checkbox' name='is_paid' value='1'/>").appendTo($paymentForm);
					$paymentCheckbox.change(function(){
						$paymentForm.submit();
					});
				}else{
					$("<td class='approvalCol'> - </td>").appendTo($tableRow);
				}
				
				var $buttonCell = $("<td></td>").appendTo($tableRow);
				var $deleteButton = $("<a href='#' class='buttonLink deleteButton'>"+$.translate("general/delete")+"<span class='end'></span></a>");
				$deleteButton.hide();
				$tableRow.bind("mouseover",function(){
					$deleteButton.show();
				}).bind("mouseout",function(){
					$deleteButton.hide();
				});
				$deleteButton.click(createDeleteBtnClickCallback(signup.id,$deleteButton)).appendTo($buttonCell);
				
				return $tableRow;
			}
			
			function onSaveSignup(response){
				var slot = response.data.signup_slot;
				var signups = response.data.signups;
				for(var key in signups){
					var $tableRow = createSignupRow(response.data.signups[key],slot).appendTo($signupTable);
				}
			}			
			
			function onSelect(data){
				var entityIDs = [];
				var i =0;
				for(var key in data){
					entityIDs[i++] = data[key].id;
				}
				
				var callback = function(response){
					onSaveSignup(data,response);
				};
				
				$.ajax({
					  url: saveSignupUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"signup_slot_id" : signup_slot_id , "entityIDs" : entityIDs, "signup_type" : repository_type},
					  success: onSaveSignup
				});
			}
			
			function createDeleteBtnClickCallback(signupId,$deleteButton){
				return function(event){
					if(confirm(options.deleteAlertMessage)){
						doDeleteAjaxCall(signupId,function(response){
							$deleteButton.closest("tr").remove();
							if(!options.preventBuiltInCallback){
								$.logger.log(response.message);
							}
							options.deleteCallback({},response);
						});
					}else{
						console.log("no deletion");
					}
					
					event.preventDefault();
					event.stopPropagation();
					return false;	
				};
			}
			
			$trigger.ajaxSelector({"serviceUrl" : searchUrl, "callback" : onSelect, "multiSelect" : false});
			
		});
	};
}(jQuery));
(function($){
	var defaultOptions = {
		saveUrl : "/admin/eventtype/{0}/save/?format=json",
		newUrl : "/admin/eventtype/new/?format=json",
		deleteUrl : "/admin/eventtype/{0}/delete?format=json"
	};	
	$.fn.eventTypeList = function(optionsParam){
		var $this = this;
		if($this.length < 1){
			return $this;
		}

		var options = $.extend({},defaultOptions,optionsParam);

		function doAjaxSave(requestUrl,data,callback){
			$.ajax({
				  url: requestUrl,
				  dataType: 'json',
				  type : 'POST',
				  data: data,
				  success: callback
			});
		}

		function doAjaxDelete(requestUrl,resourceTypeId,callback){
			$.ajax({
				  url: requestUrl,
				  dataType: 'json',
				  type : 'POST',
				  data: { "id" : resourceTypeId},
				  success: callback
			});
		}

		function transformRow($tr){
			var $idCell = $($tr.children()[0]);
			var $nameCell = $($tr.children()[1]);
			var $buttonCell = $($tr.children()[2]);

			var $idLink = $($idCell.find("a")[0]);
			var id = $idLink.text();
			var $idSpan = createIdSpan(id);
			$idLink.replaceWith($idSpan);

			var $nameLink = $($nameCell.find("a")[0]);
			var name = $nameLink.text();
			var $nameSpan = $("<span class='nameSpan'>"+name+"</span>");
			$nameLink.replaceWith($nameSpan);

			var $editButton = $($buttonCell.find(".editButton")[0]).removeProp("click");
			var $deleteButton = $($buttonCell.find(".deleteButton")[0]).hide().removeProp("click");
			var $saveButton = createSaveButton().hide().insertAfter($editButton);
			var $abortButton = createAbortButton().insertAfter($saveButton).hide();
			var $nameInput = createNameInput(name).appendTo($nameCell).hide();

			var leaveEditState = function(){
				$deleteButton.hide();
				$saveButton.hide();
				$abortButton.hide();
				$editButton.show();
				$nameInput.hide();
				$nameSpan.show();
			};

			var enterEditState = function(){
				$editButton.hide();
				$deleteButton.show();
				$abortButton.show();
				$saveButton.show();
				$nameSpan.hide();
				$nameInput.show();
			};

			$abortButton.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				leaveEditState();
				return false;
			});

			$editButton.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				enterEditState();
				return false;
			});

			$deleteButton.click(createDeleteButtonClick($tr,id));

			$saveButton.click(createSaveButtonClick($tr,"save",function(data){
				$nameSpan.text(data.data.name);
				$idSpan.text(data.data.id);
				leaveEditState();
				console.log(data.message);
			}));
		}

		function transformRows($table){
			var $rows = $table.find("tr"); 
			$rows.each(function(key,row){
				if(key === 0 || key === $rows.length-1){
					return true;
				}
				transformRow($(row));
			});
		}

		function createSaveButton(label){
			label = label || $.translate("general/save"); 
			var $saveButton = $("<a href='#spara' class='buttonLink saveButton'>" + label + "<span class='end'></span></a>");
			return $saveButton;
		}						

		function createAbortButton(){
			var label = $.translate("general/abort"); 
			var $button = $("<a href='#avbryt' class='buttonLink abortButton'>" + label + "<span class='end'></span></a>");
			return $button;
		}

		function createEditButton(){
			var label = $.translate("general/edit"); 
			var $button = $("<a href='#redigera' class='buttonLink editButton'>" + label + "<span class='end'></span></a>");
			return $button;
		}

		function createDeleteButton(){
			var $deleteButton = $("<a href='#' class='buttonLink deleteButton'>Ta bort<span class='end'></span></a>");
			return $deleteButton;
		}

		function createNameInput(name){
			name = name || "";
			return $("<input type='text' name='name' class='nameInput' value='"+name+"'/>");
		}

		function createIdSpan(innerText){
			return $("<span class='idSpan'>" + (innerText || "") + "</span>");
		}

		function createNameSpan(){
			return $("<span class='nameSpan'></span>");
		}

		function createSaveButtonClick($tr,operation,ajaxCallback){

			operation = operation || "save";
			return function(event){
				var data = getData($tr);
				var saveUrl = (operation === "new") ? options.newUrl: options.saveUrl.replace("{0}",data.id);
				doAjaxSave(saveUrl,data,ajaxCallback);
				event.stopPropagation();
				event.preventDefault();
				return false;
			};
		}

		function createNewButtonClick($table){
			return function(event){
				var $ajaxCallback = function(data){
					// create row
					var $tr = $("<tr>");
					var $td1 = $("<td>");
					var $td2 = $("<td>");
					var $td3 = $("<td>");
					$table.append($tr.append($td1).append($td2).append($td3));

					// create the input field and name textspan
					var $nameInput = $("<input type='text' value='"+ data.data.name +"'>");
					var $nameSpan = $("<span>"+data.data.name+"</span>");
					// create the save button
					var $saveButton = createSaveButton($.translate("general/save"));
					$saveButton.click(createSaveButtonClick($nameInput));
					// create the delete button
					var $deleteButton = createDeleteButton().click(createDeleteButtonClick($tr,data.data.id));
					$table.append($tr.append($td1).append($td2.append($nameSpan)).append($td3.append($saveButton)));
				};
			};
		}

		function createDeleteButtonClick($tr,typeId){

			var ajaxCallback = function(data){
				$tr.remove();
			};
			var deleteUrl = options.deleteUrl.replace("{0}",typeId);

			return function(event){
				doAjaxDelete(deleteUrl,typeId,ajaxCallback);
				event.stopPropagation();
				event.preventDefault();
				return false;
			};
		}



		function getData($tr){
			var id = $($tr.find(".idSpan")[0]).text();
			var name = $($tr.find(".nameInput")[0]).val();
			var data =  {"id" : id, "name" : name};
			return data;
		}

		function addNewRow($table,data){
			var $idSpan = createIdSpan($.translate("general/new"));
			var $nameSpan = createNameSpan().hide();
			var $nameInput = createNameInput();
			var $saveButton = createSaveButton();
			var $abortButton = createAbortButton();
			var $editButton = createEditButton();
			var $deleteButton = createDeleteButton();
			var $idCell = $("<td></td>").append($idSpan);
			var $nameCell = $("<td></td>").append($nameInput).append($nameSpan);
			var $buttonCell = $("<td></td>").append($deleteButton.hide()).append($saveButton).append($abortButton.hide()).append($editButton.hide());
			var $tr = $("<tr></tr>").append($idCell).append($nameCell).append($buttonCell).hide(); 

			var leaveEditState = function(){
				$nameInput.hide();
				$nameSpan.show();
				$saveButton.hide();
				$abortButton.hide();
				$deleteButton.hide();
				$editButton.show();
			};

			var enterEditState = function(){
				$nameInput.show();
				$nameSpan.hide();
				$saveButton.show();
				$abortButton.show();
				$deleteButton.show();
				$editButton.hide();
			};

			$saveButton.click(createSaveButtonClick($tr,"new",function(data){
				$nameSpan.text(data.data.name);
				$idSpan.text(data.data.id);
				leaveEditState();
				$saveButton.click(createSaveButtonClick($tr,"save",function(data){
					$nameSpan.text(data.name);
					$idSpan.text(data.id);
					leaveEditState();
					addNewRow($table,data);
				}));
			}));

			if(data){
				$deleteButton.click(createDeleteButtonClick($tr,data.data.id));
			}

			$editButton.click(enterEditState);
			$abortButton.click(leaveEditState);
			$tr.appendTo($table);
			return $tr;
		}

		function transformNewTypeLink($link,$row){
			$link.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				$row.show();
				return false;
			});
		}

		function _transformRow($tr){

		}

		function _addHandlersAndCallbacks($tr,$table){

		}

		function _transformNewTypeLink($table,selector){



		}

		function addNewTypeFormRow($table){

		}

		function createRow(type){
			return $("<tr><td>"+type.id+"</td><td>"+type.name+"</td><td><a href='#redigera' class='buttonLink editButton'>"+$.translate("general/edit") +"<span class='end'></span></a><a href='#' class='buttonLink deleteButton'>"+$.translate("general/delete") +"<span class='end'></span></a></td></tr>");
		}


		return $this.each(function(key,obj){
			var $table = $(obj);
			var $link = $($table.siblings()[0]);
			var $newRow = addNewRow($table);

			transformRows($table);
			transformNewTypeLink($link,$newRow);

		});
	};
})(jQuery);


(function($){
	
	var defaultOptions = {
		"newSignupUrl" : "/admin/registrering/ny?format=json",
		"eventTreeUrlTemplate" : "/admin/arrangemang/?format=json&mode=tree&cardinality=group&group_id={:group_id}",
		"deleteSignupUrlTemplate" : "/admin/registrering/{:signup_id}/delete"
	};
	
	$.fn.groupEventSignup = function(group_id,optionsParam){
	
		var options = $.extend({},defaultOptions,optionsParam);

		function createDeleteButtonClickHandler(deleteHref,onDeleteSuccess){
			return function(event){
				event.preventDefault();
				event.stopPropagation();
				$.ajax({
					url: deleteHref,
					success : onDeleteSuccess
				});
			};
		}

		function transformTable($table){
			$table.find("tr").each(function(key,obj){
				noOfTableRows++;
				if(key === 0){
					return true;
				}
				transformTableRow($(obj));
			});
		}

		function transformTableRow($tr){
			var $deleteButton = $($tr.find(".deleteButton")[0]);
			$deleteButton.removeProp("onclick");
			var onDeleteSuccess = (function($tr){
				return function(data){
					$.logger.log(data.message);
					$tr.remove();
				};
			}($tr));
			$deleteButton.click(createDeleteButtonClickHandler($deleteButton.attr("href") + "&format=json",onDeleteSuccess));
		}

		function transformLink($link){
			var $content = $("<div id='signupTree'></div>").insertAfter($link);
			$link.removeProp("onclick");
			$link.click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$.openDOMWindow({modal : false, width:600, height:400, functionCallOnOpen: onWindowOpen, windowBGColor : "#ddd" });
				return false;
			});

		}

		function onWindowOpen(){
			$.ajax({
				url: "/admin/grupp/?format=json",
				data : {'id' : group_id},
				success : function(data){ 
					var groups = data.data;
					populateWindow();
				}
			});
		}

		function populateWindow(){
			$.ajax({
				  url: options.eventTreeUrlTemplate.replace("{:group_id}",group_id),
				  dataType: 'json',
				  success: function(data){
					var $domWindow = $("#DOMWindow");
					var $outerContainer = $("<div class='domOuterContainer'></div>").appendTo($domWindow);
					var $container = $("<div class='domInnerContainer'></div>").appendTo($outerContainer);
					$("<a href='' class='buttonLink' id='closeBtn'>X<span class='end'></a>").appendTo($outerContainer).bind("click",function(event){
						event.preventDefault();
						event.stopPropagation();
						$.closeDOMWindow();
						return false;
					});

					var $eventTypeList = $("<ul class='eventTypeList'></ul>").appendTo($container);
					var eventTypes = data.data;
					for(var eventTypeKey in eventTypes){
						var eventType = eventTypes[eventTypeKey];
						createEventTypeListItem(eventType).appendTo($eventTypeList);
					}
				  }
			});						
		}

		function createEventTypeListItem(eventType){
			var $eventTypeListItem = $("<li><span class='label'>"+eventType.name+"</span></li>"); 
			var $eventList = $("<ul class='eventList'></ul>").appendTo($eventTypeListItem);
			var events = eventType.events; 
			for(var eventKey in events){
				createEventListItem(events[eventKey]).appendTo($eventList);
			}
			return $eventTypeListItem;
		}

		function createEventListItem(event){
			var $eventListItem = $("<li><span class='label'>"+event.name+"</span></li>"); 
			var $occationList = $("<ul class='eventOccationList'></ul>").appendTo($eventListItem);
			var event_occations = event.event_occations;
			for(var occationKey in event_occations){
				var occation = event_occations[occationKey];
				createOccationListItem(occation).appendTo($occationList);
			}
			return $eventListItem;
		}

		function createOccationListItem(occation){

			var $occationListItem = $("<li><span class='label'>"+ occation.full_name +"</span></li>"); 
			var $slotList = $("<ul class='signupSlotList'></ul>").appendTo($occationListItem);
			var signup_slots = occation.signup_slots;
			for(var slotKey in signup_slots){
				var slot = signup_slots[slotKey];
				createSignupSlotListItem(slot).appendTo($slotList);
			}
			return $occationListItem;
		}

		function createSignupSlotListItem(slot){
			var $slotListItem = $("<li>"+slot.slot_type_name+"</li>"); 
			if(slot.slot_status !== "alreadySignedUp"){
				var $signupButton = $("<input type='button' value='anmäl'/>").appendTo($slotListItem);
				$signupButton.click(createSignupButtonClickHandler(slot.id,group_id,$signupButton));
			}
			return $slotListItem;
		}

		function createSignupButtonClickHandler(slot_id,group_id,$button,signupType,$table){

			signupType = "group";

			return function(event){

				$button.attr("disabled","disabled");
				$.ajax({
					url: options.newSignupUrl,
					data : {
						'signup_type' : signupType,
						'entityIDs' :  [group_id],
						'signup_slot_id' : slot_id
					},
					success : createSignupSuccessHandler($button,$table),
					type : "POST"
				});
			};
		}

		function createSignupSuccessHandler($button){
			return function(data){
				$button.fadeOut(300);
				var signup = data.data.signups[0];
				$.logger.log(data.message);
				createNewRow(signup, noOfTableRows % 2 ? "odd" : "even" ).appendTo($table);
				noOfTableRows--;
			};
		}

		function createNewRow(signup, rowCssClass){
			var $row = $("<tr class='"+ rowCssClass +"'>");
			var $cells = [];
			for(var i=0; i < 5; i++){
				$cells[i] = $("<td>");
				$row.append($cells[i]);
			}
			var slot = signup.signupslot;
			var slotType = signup.signupslot.slot_type;
			var occation = signup.signupslot.event_occation;
			var timespan = signup.signupslot.event_occation.timespan;
			var event = signup.signupslot.event_occation.event;

			$cells[0].html(timespan.asString);
			$cells[1].html(event.name);
			$cells[2].html(slotType.name);
			$cells[3].html();
			$cells[4].html("<a class='deleteButton buttonLink' href='"+options.deleteSignupUrlTemplate.replace("{:signup_id}",signup.id)+"'>Ta bort<span class=\"end\"/></span></a></a>");
			transformTableRow($row);
			return $row;
		}

		var $table = $(this);
		var noOfTableRows = 0;
		transformTable($table);
		transformLink($(".addNewSignupLink"));
		
	};
}(jQuery));

(function($){
	var defaultOptions = {
		membershipTableSelector : "#groupMembershipTable",
		groupUrlTemplate : "/admin/group/{:group_id}",
		serviceUrl:"/admin/search/?format=json&r=groups",
		yesLabel : "yes",
		noLabel : "no",
		deleteLabel : "delete",
		deleteMembershipUrlTemplate : "/admin/groupMembership/{:membership_id}/delete"
	};
	$.fn.groupSelector =  function(person_id,optionsParam){

		var options = $.extend( {} , defaultOptions, optionsParam );
		
		var $trigger = $(this);
		var $table = $(options.membershipTableSelector);
		
		
		function createButton(label,link){
			label = label || "Button";
			link = link || "#";
			return $("<a class='deleteButton buttonLink' href='"+link+"'>"+label+"<span class=\"end\"/></a>");
		}
		
		function createTableRow(membership){
			var link = options.groupUrlTemplate.replace("{:group_id}",membership.group.id);
			var deleteUrl = options.deleteMembershipUrlTemplate.replace("{:group_membership_id}",membership.id);
			
			var $tr = $("<tr></tr>");
			$("<td><a href='"+link+"'>"+membership.group.id+"</a></td>").appendTo($tr);
			$("<td><a href='"+link+"'>"+membership.group.name+"</a></td>").appendTo($tr);
			$("<td>"+ (membership.is_leader === 1 ? options.yesLabel : options.noLabel) +"</td>").appendTo($tr);
			createButton(options.deleteLabel,deleteUrl).appendTo($tr);
			return $tr;
		}
		
		$trigger.ajaxSelector({
			callback : function (data){
				console.log(data);
				$.gc.ajax.saveMembership(data[0].id,person_id,function(data){
					createTableRow(data.data[0]).appendTo($table);
				});
			},
			multiselect : false,
			serviceUrl:options.serviceUrl
		});
	};
	

})(jQuery);
/*!
 * jQuery hashchange event - v1.2 - 2/11/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery hashchange event
//
// *Version: 1.2, Last updated: 2/11/2010*
// 
// Project Home - http://benalman.com/projects/jquery-hashchange-plugin/
// GitHub       - http://github.com/cowboy/jquery-hashchange/
// Source       - http://github.com/cowboy/jquery-hashchange/raw/master/jquery.ba-hashchange.js
// (Minified)   - http://github.com/cowboy/jquery-hashchange/raw/master/jquery.ba-hashchange.min.js (1.1kb)
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrate one way
// in which this plugin can be used.
// 
// hashchange event - http://benalman.com/code/projects/jquery-hashchange/examples/hashchange/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with, what browsers it has been tested in, and where the unit tests
// reside (so you can test it yourself).
// 
// jQuery Versions - 1.3.2, 1.4.1, 1.4.2pre
// Browsers Tested - Internet Explorer 6-8, Firefox 2-3.7, Safari 3-4, Chrome, Opera 9.6-10.1.
// Unit Tests      - http://benalman.com/code/projects/jquery-hashchange/unit/
// 
// About: Known issues
// 
// While this jQuery hashchange event implementation is quite stable and robust,
// there are a few unfortunate browser bugs surrounding expected hashchange
// event-based behaviors, independent of any JavaScript window.onhashchange
// abstraction. See the following examples for more information:
// 
// Chrome: Back Button - http://benalman.com/code/projects/jquery-hashchange/examples/bug-chrome-back-button/
// Firefox: Remote XMLHttpRequest - http://benalman.com/code/projects/jquery-hashchange/examples/bug-firefox-remote-xhr/
// WebKit: Back Button in an Iframe - http://benalman.com/code/projects/jquery-hashchange/examples/bug-webkit-hash-iframe/
// Safari: Back Button from a different domain - http://benalman.com/code/projects/jquery-hashchange/examples/bug-safari-back-from-diff-domain/
// 
// About: Release History
// 
// 1.2   - (2/11/2010) Fixed a bug where coming back to a page using this plugin
//         from a page on another domain would cause an error in Safari 4. Also,
//         IE6/7 Iframe is now inserted after the body (this actually works),
//         which prevents the page from scrolling when the event is first bound.
//         Event can also now be bound before DOM ready, but it won't be usable
//         before then in IE6/7.
// 1.1   - (1/21/2010) Incorporated document.documentMode test to fix IE8 bug
//         where browser version is incorrectly reported as 8.0, despite
//         inclusion of the X-UA-Compatible IE=EmulateIE7 meta tag.
// 1.0   - (1/9/2010) Initial Release. Broke out the jQuery BBQ event.special
//         window.onhashchange functionality into a separate plugin for users
//         who want just the basic event & back button support, without all the
//         extra awesomeness that BBQ provides. This plugin will be included as
//         part of jQuery BBQ, but also be available separately.

(function($,window,undefined){
  '$:nomunge'; // Used by YUI compressor.
  
  // Method / object references.
  var fake_onhashchange,
    jq_event_special = $.event.special,
    
    // Reused strings.
    str_location = 'location',
    str_hashchange = 'hashchange',
    str_href = 'href',
    
    // IE6/7 specifically need some special love when it comes to back-button
    // support, so let's do a little browser sniffing..
    browser = $.browser,
    mode = document.documentMode,
    is_old_ie = browser.msie && ( mode === undefined || mode < 8 ),
    
    // Does the browser support window.onhashchange? Test for IE version, since
    // IE8 incorrectly reports this when in "IE7" or "IE8 Compatibility View"!
    supports_onhashchange = 'on' + str_hashchange in window && !is_old_ie;
  
  // Get location.hash (or what you'd expect location.hash to be) sans any
  // leading #. Thanks for making this necessary, Firefox!
  function get_fragment( url ) {
    url = url || window[ str_location ][ str_href ];
    return url.replace( /^[^#]*#?(.*)$/, '$1' );
  }
  
  // Property: jQuery.hashchangeDelay
  // 
  // The numeric interval (in milliseconds) at which the <hashchange event>
  // polling loop executes. Defaults to 100.
  
  $[ str_hashchange + 'Delay' ] = 100;
  
  // Event: hashchange event
  // 
  // Fired when location.hash changes. In browsers that support it, the native
  // window.onhashchange event is used (IE8, FF3.6), otherwise a polling loop is
  // initialized, running every <jQuery.hashchangeDelay> milliseconds to see if
  // the hash has changed. In IE 6 and 7, a hidden Iframe is created to allow
  // the back button and hash-based history to work.
  // 
  // Usage:
  // 
  // > $(window).bind( 'hashchange', function(e) {
  // >   var hash = location.hash;
  // >   ...
  // > });
  // 
  // Additional Notes:
  // 
  // * The polling loop and Iframe are not created until at least one callback
  //   is actually bound to 'hashchange'.
  // * If you need the bound callback(s) to execute immediately, in cases where
  //   the page 'state' exists on page load (via bookmark or page refresh, for
  //   example) use $(window).trigger( 'hashchange' );
  // * The event can be bound before DOM ready, but since it won't be usable
  //   before then in IE6/7 (due to the necessary Iframe), recommended usage is
  //   to bind it inside a $(document).ready() callback.
  
  jq_event_special[ str_hashchange ] = $.extend( jq_event_special[ str_hashchange ], {
    
    // Called only when the first 'hashchange' event is bound to window.
    setup: function() {
      // If window.onhashchange is supported natively, there's nothing to do..
      if ( supports_onhashchange ) { return false; }
      
      // Otherwise, we need to create our own. And we don't want to call this
      // until the user binds to the event, just in case they never do, since it
      // will create a polling loop and possibly even a hidden Iframe.
      $( fake_onhashchange.start );
    },
    
    // Called only when the last 'hashchange' event is unbound from window.
    teardown: function() {
      // If window.onhashchange is supported natively, there's nothing to do..
      if ( supports_onhashchange ) { return false; }
      
      // Otherwise, we need to stop ours (if possible).
      $( fake_onhashchange.stop );
    }
    
  });
  
  // fake_onhashchange does all the work of triggering the window.onhashchange
  // event for browsers that don't natively support it, including creating a
  // polling loop to watch for hash changes and in IE 6/7 creating a hidden
  // Iframe to enable back and forward.
  fake_onhashchange = (function(){
    var self = {},
      timeout_id,
      iframe,
      set_history,
      get_history;
    
    // Initialize. In IE 6/7, creates a hidden Iframe for history handling.
    function init(){
      // Most browsers don't need special methods here..
      set_history = get_history = function(val){ return val; };
      
      // But IE6/7 do!
      if ( is_old_ie ) {
        
        // Create hidden Iframe after the end of the body to prevent initial
        // page load from scrolling unnecessarily.
        iframe = $('<iframe src="javascript:0"/>').hide().insertAfter( 'body' )[0].contentWindow;
        
        // Get history by looking at the hidden Iframe's location.hash.
        get_history = function() {
          return get_fragment( iframe.document[ str_location ][ str_href ] );
        };
        
        // Set a new history item by opening and then closing the Iframe
        // document, *then* setting its location.hash.
        set_history = function( hash, history_hash ) {
          if ( hash !== history_hash ) {
            var doc = iframe.document;
            doc.open().close();
            doc[ str_location ].hash = '#' + hash;
          }
        };
        
        // Set initial history.
        set_history( get_fragment() );
      }
    }
    
    // Start the polling loop.
    self.start = function() {
      // Polling loop is already running!
      if ( timeout_id ) { return; }
      
      // Remember the initial hash so it doesn't get triggered immediately.
      var last_hash = get_fragment();
      
      // Initialize if not yet initialized.
      if(!set_history){
		init();
      }
      // This polling loop checks every $.hashchangeDelay milliseconds to see if
      // location.hash has changed, and triggers the 'hashchange' event on
      // window when necessary.
      (function loopy(){
        var hash = get_fragment(),
          history_hash = get_history( last_hash );
        
        if ( hash !== last_hash ) {
          set_history( last_hash = hash, history_hash );
          
          $(window).trigger( str_hashchange );
          
        } else if ( history_hash !== last_hash ) {
          window[ str_location ][ str_href ] = window[ str_location ][ str_href ].replace( /#.*/, '' ) + '#' + history_hash;
        }
        
        timeout_id = setTimeout( loopy, $[ str_hashchange + 'Delay' ] );
      })();
    };
    
    // Stop the polling loop, but only if an IE6/7 Iframe wasn't created. In
    // that case, even if there are no longer any bound event handlers, the
    // polling loop is still necessary for back/next to work at all!
    self.stop = function() {
      if ( !iframe ) {
		if(timeout_id){
			clearTimeout( timeout_id );
		}
        timeout_id = 0;
      }
    };
    
    return self;
  })();
  
})(jQuery,this);

(function($){
	var defaultOptions = {
		"eventTreeUrlTemplate" : "/admin/event/?format=json&mode=tree&person_id={:person_id}",
		"newSignupUrl" : "/admin/signup/new?format=json",
		"deleteSignupUrlTemplate" : "/admin/signup/{:signup_id}/delete",
		"personUrlTemplate" : "/admin/person/{:person_id}/",
		"signupButtonLabel" : "sign up",
		"deleteLabel" : "delete",
		"eventUrlTemplate":"/admin/arrangemang/{:event_id}",
		"eventTypeUrlTemplate" : "/admin/arrangemangstyp/{:event_type_id}"
	};
	
	$.fn.individualEventSignup = function(person_id,optionsParam){

		var options = $.extend( {} , defaultOptions, optionsParam );

		var groups;
		var noOfTableRows = 0;
		$(this).each(function(key,obj){

			function createDeleteButtonClickHandler(deleteHref,onDeleteSuccess){
				return function(event){
					event.preventDefault();
					event.stopPropagation();
					$.ajax({
						url: deleteHref,
						data : {'id' : person_id},
						success : onDeleteSuccess
					});
				};
			}

			function transformTable($table){
				$table.find("tr").each(function(key,obj){
					noOfTableRows++;
					if(key === 0){
						return true;
					}
					transformTableRow($(obj));
					return true; 
				});
			}

			function transformTableRow($tr){

				var $deleteButton = $($tr.find(".deleteButton")[0]);
				$deleteButton.removeProp("onclick");

				var onDeleteSuccess = (function($tr){
					return function(){
						$tr.remove();
					};
				}($tr));

				var onDeleteButtonClick = (createDeleteButtonClickHandler($deleteButton.attr("href") + "?format=json",onDeleteSuccess));	

				$deleteButton.click(onDeleteButtonClick);
			}

			function transformLink($link){
				$("<div id='signupTree'></div>").insertAfter($link);
				$link.removeProp("onclick");


				$link.click(function(event){
					event.preventDefault();
					event.stopPropagation();
					$.openDOMWindow({modal : false, width:600, height:400, functionCallOnOpen: onWindowOpen, windowBGColor : "#ddd" });
					return false;
				});

			}

			function onWindowOpen(){
				populateWindow();
			}

			function populateWindow(){
				$.ajax({
					  url:  options.eventTreeUrlTemplate.replace("{:person_id}",person_id),
					  dataType: 'json',
					  success: function(data){
						var $domWindow = $("#DOMWindow");
						var $outerContainer = $("<div class='domOuterContainer'></div>").appendTo($domWindow);
						var $container = $("<div class='domInnerContainer'></div>").appendTo($outerContainer);
						$("<a href='' class='buttonLink' id='closeBtn'>X<span class='end'></a>").appendTo($outerContainer).bind("click",function(event){
							event.preventDefault();
							event.stopPropagation();
							$.closeDOMWindow();
							return false;
						});
						var $eventTypeList = $("<ul class='eventTypeList'></ul>").appendTo($container);
						var eventTypes = data.data;
						for(var eventTypeKey in eventTypes){
							var eventType = eventTypes[eventTypeKey];
							createEventTypeListItem(eventType).appendTo($eventTypeList);
						}
					  }
				});						
			}

			function createEventTypeListItem(eventType){
				var $eventTypeListItem = $("<li><span class='label'>"+eventType.name+"</span></li>"); 
				var $eventList = $("<ul class='eventList'></ul>").appendTo($eventTypeListItem);
				var events = eventType.events; 
				for(var eventKey in events){
					createEventListItem(events[eventKey]).appendTo($eventList);
				}
				return $eventTypeListItem;
			}

			function createEventListItem(event){
				var $eventListItem = $("<li><span class='label'>"+event.name+"</span></li>"); 
				var $occationList = $("<ul class='eventOccationList'></ul>").appendTo($eventListItem);
				var event_occations = event.event_occations;
				for(var occationKey in event_occations){
					var occation = event_occations[occationKey];
					createOccationListItem(occation).appendTo($occationList);
				}
				return $eventListItem;
			}

			function createOccationListItem(occation){
				// var timespanName = occation.timespan.name == "" ? occation.timespan.starts_at + " - " + occation.timespan_ends_at : occation.timespan.name;
				var timespanName = occation.full_name;
				var $occationListItem = $("<li><span class='label'>"+ timespanName +"</span></li>"); 
				var $slotList = $("<ul class='signupSlotList'></ul>").appendTo($occationListItem);
				var signup_slots = occation.signup_slots;
				for(var slotKey in signup_slots){
					var slot = signup_slots[slotKey];
					if(slot.slot_type_cardinality !== 1){
						for(var groupKey in groups){
							createGroupSignupSlotListItem(slot).appendTo($slotList);
						}
					}else{
						createSignupSlotListItem(slot).appendTo($slotList);
					}	
				}
				return $occationListItem;
			}

			function createGroupSignupSlotListItem(slot){
				var group = groups[groupKey].group;
				var $slotListItem = $("<li>"+slot.slot_type_name+": <em>"+ group.name +"</em></li>"); 
				var $signupButton = $("<input type='button' value='anmäl'/>").appendTo($slotListItem);
				$signupButton.click(createSignupButtonClickHandler(slot.id,group.id,$signupButton,"group"));
				return $slotListItem;
			}

			function createSignupSlotListItem(slot){
				var $slotListItem = $("<li>"+slot.slot_type_name+"</li>"); 
				if(slot.slot_status !== "alreadySignedUp"){
					var $signupButton = $("<input type='button' value='"+options.signupButtonLabel+"'/>").appendTo($slotListItem);
					$signupButton.click(createSignupButtonClickHandler(slot.id,person_id,$signupButton));
				}	
				return $slotListItem;
			}

			function createSignupButtonClickHandler(slot_id,group_id,$button,signupType,$table){

				if(signupType !== "group"){
					signupType = "people";
				}

				return function(event){

					$button.attr("disabled","disabled");
					$.ajax({
						url: options.newSignupUrl,
						data : {
							'signup_type' : signupType,
							'entityIDs' :  [group_id],
							'signup_slot_id' : slot_id
						},
						success : createSignupSuccessHandler($button,$table),
						type : "POST"
					});
				};
			}

			function createSignupSuccessHandler($button){
				return function(data){
					$button.fadeOut(300);
					signup = data.data.signups[0];
					createNewRow(signup, noOfTableRows % 2 ? "odd" : "even" ).appendTo($table);
					noOfTableRows--;
				};
			}

			function createNewRow(signup, rowCssClass){
				var $row = $("<tr class='"+ rowCssClass +"'></tr>");
				var $cells = [];
				for(i=0; i < 5; i++){
					$cells[i] = $("<td></td>");
					$row.append($cells[i]);
				}
				var slotType = signup.signupslot.slot_type;
				var timespan = signup.signupslot.event_occation.timespan;
				var event = signup.signupslot.event_occation.event;
				var $eventLink = $("<a href='"+ options.eventUrlTemplate.replace("{:event_id}",event.id)+"'>"+event.name+"</a>");
				var $eventOccationLink =  $("<a href='"+ options.eventOccationUrlTemplate.replace("{:event_id}",event.id)+"'>"+timespan.asString+"</a>");

				$cells[0].append($eventOccationLink);
				$cells[1].append($eventLink);
				$cells[2].html(slotType.name);
				$cells[3].html();
				$cells[4].html("<a class='deleteButton buttonLink' href='"+options.deleteSignupUrlTemplate.replace("{:signup_id}",signup.id)+"'>"+options.deleteLabel+"<span class=\"end\"/></a>");
				transformTableRow($row);
				return $row;
			}


			var $table = $(obj);
			transformTable($table);
			transformLink($(".addNewSignupLink"));
		});
		
		return $(this);
		
	};
}(jQuery));
	(function($){
		
		if(!$.logger){
			$.logger = {};
			
			$.logger.log = function(message,defaultType){
				if(!$.logger.globalLogger){
					$.logger.globalLogger = $.logger.create(); 
				}
				$.logger.globalLogger.log(message,defaultType);
			}
			
			$.log = $.logger.log;
			
			$.logger.create = function(){

				var returnObject = {};
				
				var $ajaxInfoList = $("#infoList");
				var $ajaxWarningList = $("#warningList");
				var $ajaxErrorList = $("#errorList");
				
				if($ajaxInfoList.length == 0){
					$ajaxInfoList = $("<ul id='infoList'></ul>").prependTo("#mainContent").hide();
				}
				
				if($ajaxWarningList.length == 0){
					$ajaxWarningList = $("<ul id='warningList'></ul>").prependTo("#mainContent").hide();
				}
				
				if($ajaxErrorList.length == 0){
					$ajaxErrorList = $("<ul id='errorList'></ul>").prependTo("#mainContent").hide();
				}
				
				
				
				
				
				
				function isAnArray(subj){
					return Object.prototype.toString.call(subj) === "[object Array]";
				}
				
				function isAString(subj){
					return Object.prototype.toString.call(subj) === "[object String]";
				}
				
				function isAnObject(subj){
					return Object.prototype.toString.call(subj) === "[object Object]";
				}
				
				returnObject.log = function(messages,type){
					
					type = type || "info";
					
					if(messages && messages.length > 0){

						if(isAString(messages)){
							messages = [{message:messages,type:type}];
						}
						else if(isAnObject(messages)){
							messages = [messages];
						}

						var clearInfoList = true;
						var clearWarningList = true;
						var clearErrorList = true;

						for(var key in messages){
							var message = messages[key];
							if(isAString(messages)){
								message = {message:message,type:type};
							}
							switch(message.type){
								
								case "info":
									if(clearInfoList){
										$ajaxInfoList.html("").show();
										clearInfoList = false;
										$ajaxWarningList.hide();
										$ajaxErrorList.hide();
									}
									$ajaxInfoList.append("<li>"+ message.message +"</li>")
									break;
									
								case "warning" :
									if(clearWarningList){
										$ajaxWarningList.html("").show();
										clearWarningList = false;
										$ajaxErrorList.hide();
										$ajaxInfoList.hide();
									}
									$ajaxWarningList.append("<li>"+ message.message +"</li>")
									break;
									
								case "error" : 
									if(clearErrorList){
										$ajaxErrorList.html("").show();
										clearErrorList = false;
										$ajaxWarningList.hide();
										$ajaxInfoList.hide();
									}
									$ajaxErrorList.append("<li>"+ message.message +"</li>")
									break;
							}
						}
					}
				}
				return returnObject;
			}
		
			$(function(){ $.logger.globalLogger = $.logger.create(); 
				var $ajaxInfoList = $("#infoList");
				var $ajaxWarningList = $("#warningList");
				var $ajaxErrorList = $("#errorList");
			
				$ajaxInfoList.append("<li class='close'>(Klicka för att stänga)</li>");
				$ajaxWarningList.append("<li class='close'>(Klicka för att stänga)</li>");
				$ajaxErrorList.append("<li class='close'>(Klicka för att stänga)</li>");
			
				$ajaxInfoList.click(function(){
					$ajaxInfoList.hide();
				});
				
				$ajaxWarningList.click(function(){
					$ajaxWarningList.hide();
				});
				
				$ajaxErrorList.click(function(){
					$ajaxErrorList.hide();
				});
			
			});

}
	}(jQuery));
//(function($){
//	$.fn.mainMenu = function(options){
//
//		$(this).each(function(key,obj){
//			var $mainMenu = $(obj);
//			$mainMenu.hide();
//			$mainMenu.find("ul").hide();
//			$mainMenu.find("li").each(function(key,obj){
//				var $li = $(obj);
//				var $innerList = $li.find("ul");
//				if($innerList.length > 0){
//					// $li.css({"cursor":"default"});
//					$li.bind("mouseout",function(){ $innerList.hide();});
//					$li.bind("mouseover",function(){ $innerList.show();});
//					$innerList.css({"position":"absolute", "z-index":"10"});
//				}
//			});
//			$mainMenu.show();	
//		});
//	}
//}(jQuery));
(function($){
	var defaultOptions = {"headerIdentifier" : "h5", "fieldsetIdentifier" : ".formFieldset"}
	$.fn.newSlotForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $slotFormContainer = $(obj).hide();
			var $sektionstitel = $($slotFormContainer.closest("li").find("h3.passnamn"));
			
			var $fieldset = $slotFormContainer.find(options.fieldsetIdentifier).hide();
			var $anchor =  $("<a href='/admin/event_occation/new' class='editLink'>"+$.translate("signupSlot/new")+"</a>").insertAfter($sektionstitel);
			// var $anchor = $header.find("a");
			var enterEditState = function(event){
				event.preventDefault();
				event.stopPropagation();
				$slotFormContainer.show();
				$fieldset.slideDown("800");
				$anchor.unbind("click");
				$anchor.click(leaveEditState);
				
			}
			var leaveEditState = function(event){
				event.preventDefault();
				event.stopPropagation();
				$fieldset.slideUp("800",function(){
					$slotFormContainer.hide();
				});
				$anchor.unbind("click");
				$anchor.click(enterEditState);
			}
			$anchor.click(enterEditState);
		});
	}
}(jQuery)); 
(function($){
	var defaultOptions = {
		// "serviceUrl" : "/admin/search/?r=resources&format=json",
		// "ajaxSaveUrl" : "/admin/resourcebooking/new?format=json",
		// "deleteUrlTemplate" : "/admin/resourcebooking/{:resource_booking_id}/delete?format=json",
		minimumQueryLength : 0,
		"dictionary" : {
				"delete" : $.translate("delete"),
				"error" : "something went wrong!!!"
			},
		
		"onSaveCallback" : function(data){
			console.log(data);
		}, 
		"multiSelect" : false						
	}
	$.fn.resourceBooker = function(optionsParam){ 
		var options = $.extend({},defaultOptions,optionsParam);
		var dictionary = options.dictionary;
		$(this).each(function(key,obj){
			var $trigger = $(obj);
			
			var pattern = /event_occation_id=(\d+)/g;
			var event_occation_id = pattern.exec($trigger.attr("href"))[1];
			
			var $table = $trigger.siblings("table");
			
			$table.find("a.deleteButton").ajaxDeleteResourceBooking({
				"deleteUrlTemplate" : options.deleteUrlTemplate
			});
			
			options.callback = function(data,$trigger){
				var postData = {"resource_id" : data[0].id, "event_occation_id" : event_occation_id};
				$.ajax({
					url: options.ajaxSaveUrl,
					dataType: 'json',
					data: postData ,
					success: function(data){
						console.log(options);
						options.onSaveCallback(data);
					},
					error:function(){
						console.log(dictionary["error"]);
					},
					type: 'POST'
				});

			};
			$trigger.ajaxSelector(options);						
		});
	}
}(jQuery));

(function($){
	var defaultOptions = {
		"headerIdentifier" : "h4", 
		"slotFormIdentifier" : ".signupSlotForm", 
		"submitButtonIdentifier" : ".submitButton",
		"fieldsetIdentifier" : ".formFieldset", 
		"signupTableIdentifier" : ".signupTable",
		"deleteSignupButtonIdentifier" : ".deleteButton"
	};
	$.fn.signupSlotForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			
			// update the form target (the action)
			var $form = $(obj).find(options.slotFormIdentifier);
			//var pattern = /\/signup_slot\/(\d+)\/save/g
			//var matches = pattern.exec($form.attr("action"));
			//if(matches != null){
		//		$form.attr("action","/admin/signup_slot/"+matches[1] +"/save");
		//	}else{
		//		$form.attr("action","/admin/signup_slot/new");
		//	}
		$form.hide();
			
			$form.ajaxForm({
				saveCallback:function(data){
					leaveEditState()
				}
			});
			
			
			var $saveButton = $form.find(options.submitButtonIdentifier);
			var requires_approval = $form.find("input[name=requires_approval]:checked").val() == 1;
			var maximum_signup_count = $form.find("input[name=maximum_signup_count]").val();
			var maximum_spare_signup_count = $form.find("input[name=maximum_spare_signup_count]").val();
			var infoString = "";
			if(requires_approval)
				infoString += "Behöver godkännande. ";
			if(maximum_signup_count > 0)
				infoString += "Begränsat antal platser: " + maximum_signup_count +". ";
			if(maximum_spare_signup_count > 0)
				infoString += "Begränsat antal reservplatser: " + maximum_spare_signup_count +". ";
			var $infoSpan = $("<span class='info'>" + infoString + "</span>");
			$infoSpan.insertBefore($form);
			var $header = $(obj).find(options.headerIdentifier);
			var $editLink = $("<a href='#' class='editLink'>redigera</a>");
			
			var enterEditState = function(clickEvent){
				if(clickEvent){
					clickEvent.preventDefault();
					clickEvent.stopPropagation();
				}
				$form.slideDown("300");
				$editLink.unbind("click");
				$editLink.click(leaveEditState);
			};
			
			var leaveEditState = function(clickEvent){
				if(clickEvent){
					clickEvent.preventDefault();
					clickEvent.stopPropagation();
				}
				$form.slideUp("300",function(){
				// $infoSpan.show();
				$editLink.unbind("click");
				$editLink.click(enterEditState);
				});
			};
			
			
			$editLink.click(enterEditState);
			
			$editLink.insertAfter($header);
			
			var $signupContainer = $(obj);
			$signupContainer.find(options.signupTableIdentifier).each(function(key,table){
				var $signupTable = $(table);
				$signupTable.find("tr").each(function(key,row){
					var $row = $(row);
					$row.find(options.deleteSignupButtonIdentifier).each(function(key,button){
						var $deleteButton = $(button).hide();
						$(row).hover(function(event){
							$deleteButton.show();
						},function(event){
							$deleteButton.hide();
						});
					});
				});
			});
			
			
		});					
	}
}(jQuery));
				
(function($){

	var getCount = (function(){
		var _count = -1;
		return function(){
			_count +=1;
			return _count;
		}
	}());

	var defaultOptions = {
		callback : function(){},
		multiSelect : false,
		searchTerm : "Sök",
		serviceUrl : "/ajax.php",
		maxNoOfResults:100,
		hideId : false
	}

	$.fn.suggestionInput =  function(optionsParam){
		var options = $.extend( {} , defaultOptions, optionsParam );
		var $input = $(this);
		var $list = $("<ul class=\"suggestions\"></ul>");
		var hasValues = false;
		var inititalValue;
		
		var selectedListItem = -1;
		var listTopOffset = 26;
		$list.insertAfter($input).css({"position" : "absolute"}).hide();
		$input.parent().css({"position":"relative"});
		var objects = [];
		
		function onKeyUp(event){
			event.preventDefault();
			event.stopPropagation();
			if(event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 9){
				if(event.keyCode == 9){
					signalNewValue();
					inactivateSuggestions();
				}else{
					event.preventDefault();
					event.stopPropagation();
					if(event.keyCode == 13 || event.keyCode == 9){
						signalNewValue();
						inactivateSuggestions();
					}
					else if(event.keyCode == 27){
						resetValue();
						inactivateSuggestions();
					}else if(hasValues){
						if(event.keyCode == 38){
							// arrow up
							selectedListItem = selectedListItem > 0 ? selectedListItem -1 : selectedListItem;
						}else if(event.keyCode == 40){
							// arrow down
							selectedListItem = selectedListItem < $list.children().length-1 ? selectedListItem +1 : selectedListItem;
						}
						if(selectedListItem >= 0){
							$input.val($($list.children()[selectedListItem]).html());
						}
						$list.css({"top" : listTopOffset -((selectedListItem+1) * 19)+"px"});
					}
					return false;
				}
			}
			return true;
		};
		
		function activateSuggestions(){
			$list.show();
			$input.unbind("keyup.suggestionInput");
			$input.bind("keyup.suggestionInput",onKeyUp);
			selectedListItem = 0;
			$list.css({"top" : listTopOffset -(selectedListItem * 19)+"px"});
		}
		
		function inactivateSuggestions(){
			$input.unbind("keydown.suggestionInput");
			$list.hide();
			hasValues = false;
		}
		
		function signalNewValue(){
			if($input.val() == ""){
				options.callback(0);
			}
			else if(selectedListItem == -1){
				$input.val($input[0].defaultValue);
				options.callback(-1);
			}else{
				options.callback($($list.children()[selectedListItem]).data("id"));
			}
		}
		
		function resetValue(){
			$input.val($input[0].defaultValue);
		}
		
		function ajaxCallback(data){
			$list.html("")
			if(data.status == 0){
				hasValues = data.data.length > 0;
				objects = [];
				for(key in data.data){
					objectdata = data.data[key];
					objects[objectdata.id] = data.data[key];
					var idString = options.hideId ? "" : "" + objectdata.id + ". ";
					var $listItem = $("<li>" + objectdata.label + " ("+ objectdata.id+")</li>");
					$listItem.data("type",objectdata.type);
					$listItem.data("label",idString + objectdata.label );
					$listItem.data("id",objectdata.id);
					$list.append($listItem);
				}
				selectedListItem = -1;
				if(data.responsecode == 2)
				$list.append("<li>Mer än " + options.maxNoOfResults +  " träffar </li>");
			}
			if(hasValues){
				activateSuggestions();
			}
		}

		var ajaxOptions = {
			serviceUrl : options.serviceUrl,
			maxNoOfResults : options.maxNoOfResults,
			callback : ajaxCallback,
			queryToShortCallback : function(){
				$list.html("");
			}
		}

		$input.ajaxSearcher(ajaxOptions);
		return $input;
	}
})(jQuery);
(function($){
	
	
	$.fn.tabbedContainer = function(parameters){
		var $tabbedContainer = $(this);
		var $tabs = $("<div class='tabs'></div>");
		var $sections = $tabbedContainer.find(".section");
		var initialHash = replace_swedish_chars(document.location.hash);
		var tabsShortCut = [];
		var defaultTab; 

		var onHashChange = function(event) {
			var cleanedHash = replace_swedish_chars(document.location.hash);
			if(tabsShortCut[cleanedHash] != undefined){
				tabsShortCut[cleanedHash].click();
			}else{
				defaultTab.click();
			}
		}

		function replace_swedish_chars(subject){
			var from = ["å","ä","ö","Å","Ä","Ö"," "];
			var to = ["a","a","o","A","A","O","_"];
			for(key in from){
				subject = subject.replace(RegExp(from[key],"g"),to[key]);
			}
			return subject;
		}

		if($sections.length > 0){
			$tabbedContainer.before($tabs);
			var $visibleSection = null;
			$sections.each(function(key,obj){
				var $obj = $(obj);
				var $label = $obj.find(".sectionHeader").hide();
				var tabHref = "#" + replace_swedish_chars($label.html());
				var $tab = $("<a class='tab' href='" + tabHref +"'></a>");
				tabsShortCut[tabHref] = $tab;
				if(key==0){
					defaultTab = $tab;
				}
				$tab.append($label.html() +"<span class='tabEnd'></span>");
				$obj.data("tab",$tab);
				$tabs.append($tab);
				$tab.click(function(){
					if($visibleSection != null){
						$visibleSection.data("tab").removeClass("active");
						$visibleSection.hide();
					}
					$visibleSection = $obj;
					$obj.data("tab").addClass("active");
					$obj.show();
				});
				$obj.hide();
			});
			var initialTabToClick = $tabs.find("[href=" + initialHash + "]");
			if (initialHash === "" || initialTabToClick.length == 0) {
				$sections.first().show();
				$visibleSection = $sections.first();
				$visibleSection.data("tab").addClass("active");
			}else{
				$(initialTabToClick).click();
			}
			
			$(window).bind('hashchange',onHashChange)
			
		}
	}
}(jQuery));
(function($){
	var saveMembershipUrl = "/admin/membership/new?format=json";



	$.gc = {
			ajax:{
				saveMembership : function(group_id,person_id,callback){
					console.log(group_id);
					$.ajax({
					  url: saveMembershipUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"group_id" : group_id , "entityIDs" : [person_id]},
					  success: callback
					});
				},
				setUrls : function(urls){
					saveMembershipUrl = urls.saveMembershipUrl;
				}
			}
		};
})(jQuery);