(function($){
	var defaultOptions = {
		"headerIdentifier" : "h4", 
		"slotFormIdentifier" : ".signupSlotForm", 
		"submitButtonIdentifier" : ".submitButton",
		"fieldsetIdentifier" : ".formFieldset", 
		"signupTableIdentifier" : ".signupTable",
		"deleteSignupButtonIdentifier" : ".deleteButton"
	};
	$.fn.signupSlotForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			
			// update the form target (the action)
			var $form = $(obj).find(options.slotFormIdentifier);
			//var pattern = /\/signup_slot\/(\d+)\/save/g
			//var matches = pattern.exec($form.attr("action"));
			//if(matches != null){
		//		$form.attr("action","/admin/signup_slot/"+matches[1] +"/save");
		//	}else{
		//		$form.attr("action","/admin/signup_slot/new");
		//	}
		$form.hide();
			
			$form.ajaxForm({
				saveCallback:function(data){
					leaveEditState()
				}
			});
			
			
			var $saveButton = $form.find(options.submitButtonIdentifier);
			var requires_approval = $form.find("input[name=requires_approval]:checked").val() == 1;
			var maximum_signup_count = $form.find("input[name=maximum_signup_count]").val();
			var maximum_spare_signup_count = $form.find("input[name=maximum_spare_signup_count]").val();
			var infoString = "";
			if(requires_approval)
				infoString += "Behöver godkännande. ";
			if(maximum_signup_count > 0)
				infoString += "Begränsat antal platser: " + maximum_signup_count +". ";
			if(maximum_spare_signup_count > 0)
				infoString += "Begränsat antal reservplatser: " + maximum_spare_signup_count +". ";
			var $infoSpan = $("<span class='info'>" + infoString + "</span>");
			$infoSpan.insertBefore($form);
			var $header = $(obj).find(options.headerIdentifier);
			var $editLink = $("<a href='#' class='editLink'>redigera</a>");
			
			var enterEditState = function(clickEvent){
				if(clickEvent){
					clickEvent.preventDefault();
					clickEvent.stopPropagation();
				}
				$form.slideDown("300");
				$editLink.unbind("click");
				$editLink.click(leaveEditState);
			};
			
			var leaveEditState = function(clickEvent){
				if(clickEvent){
					clickEvent.preventDefault();
					clickEvent.stopPropagation();
				}
				$form.slideUp("300",function(){
				// $infoSpan.show();
				$editLink.unbind("click");
				$editLink.click(enterEditState);
				});
			};
			
			
			$editLink.click(enterEditState);
			
			$editLink.insertAfter($header);
			
			var $signupContainer = $(obj);
			$signupContainer.find(options.signupTableIdentifier).each(function(key,table){
				var $signupTable = $(table);
				$signupTable.find("tr").each(function(key,row){
					var $row = $(row);
					$row.find(options.deleteSignupButtonIdentifier).each(function(key,button){
						var $deleteButton = $(button).hide();
						$(row).hover(function(event){
							$deleteButton.show();
						},function(event){
							$deleteButton.hide();
						});
					});
				});
			});
			
			
		});					
	}
}(jQuery));
				