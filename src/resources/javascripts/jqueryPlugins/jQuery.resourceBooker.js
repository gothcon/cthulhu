(function($){
	var defaultOptions = {
		// "serviceUrl" : "/admin/search/?r=resources&format=json",
		// "ajaxSaveUrl" : "/admin/resourcebooking/new?format=json",
		// "deleteUrlTemplate" : "/admin/resourcebooking/{:resource_booking_id}/delete?format=json",
		minimumQueryLength : 0,
		"dictionary" : {
				"delete" : $.translate("delete"),
				"error" : "something went wrong!!!"
			},
		
		"onSaveCallback" : function(data){
			console.log(data);
		}, 
		"multiSelect" : false						
	}
	$.fn.resourceBooker = function(optionsParam){ 
		var options = $.extend({},defaultOptions,optionsParam);
		var dictionary = options.dictionary;
		$(this).each(function(key,obj){
			var $trigger = $(obj);
			
			var pattern = /event_occation_id=(\d+)/g;
			var event_occation_id = pattern.exec($trigger.attr("href"))[1];
			
			var $table = $trigger.siblings("table");
			
			$table.find("a.deleteButton").ajaxDeleteResourceBooking({
				"deleteUrlTemplate" : options.deleteUrlTemplate
			});
			
			options.callback = function(data,$trigger){
				var postData = {"resource_id" : data[0].id, "event_occation_id" : event_occation_id};
				$.ajax({
					url: options.ajaxSaveUrl,
					dataType: 'json',
					data: postData ,
					success: function(data){
						console.log(options);
						options.onSaveCallback(data);
					},
					error:function(){
						console.log(dictionary["error"]);
					},
					type: 'POST'
				});

			};
			$trigger.ajaxSelector(options);						
		});
	}
}(jQuery));
