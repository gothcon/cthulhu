(function($){
	var defaultOptions = {"headerIdentifier" : "h5", "fieldsetIdentifier" : ".formFieldset"};
	$.fn.eventOccationForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $formContainer = $(obj);
			var $header = $formContainer.find("h3");
			
			var $form = $formContainer.find("form.occationForm");
			
			if($form.length === 0){
				return true;
			}
			
			// ajaxify the forms
			$form.ajaxForm(
				{
					saveCallback:function(form,data){
						var name = data.data.name;
						$form.closest("li.pass").find("h3.passnamn").html(name);
					}
				}
			);
			// ajaxify the delete buttons
			var $deleteButton = $form.find(".deleteButton");
			$deleteButton.prop("onclick", null);
			$deleteButton.click(function(event){
				event.preventDefault();
				var url = $deleteButton.attr("href");
				url += (((url.indexOf("?") === -1) ? "?" : "&") + "format=json");
				$.ajax({
					  url: url,
					  dataType: 'json',
					  success: function(data){
						$form.closest("li").remove();
					  },
					  error:function(){
						console.log("something went wrong!!!");
					  },
					  type: "get"
				});
			});
			
			
			var $container = $($form.closest(".sektion")).hide();
			var $editLink = $("<a href='#' class='editLink'>"+$.translate("general/edit")+"</a>");
			$editLink.insertAfter($header);
			
			var enterEditState = function(event){
				event.preventDefault();
				$container.show();
				$editLink.unbind("click");
				$editLink.click(leaveEditState);
			};
			
			var leaveEditState = function(event){
				event.preventDefault();
				$container.hide();
				$editLink.unbind("click");
				$editLink.click(enterEditState);
			};
			
			$editLink.click(enterEditState);
		});
	};
}(jQuery));