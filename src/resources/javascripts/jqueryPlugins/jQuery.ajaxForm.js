


(function($){
	
	var defaultOptions = {
		"ajaxStatusBoxSelector" : "#ajaxStatusBox",
		"saveCallback" : function($responseObject,$form){},
		"preventBuiltInCallback" : false,
		"submitButtonSelector" : "input[type=submit]",
		"returnDataType" : "json"
	};	
	var helperFunctions = {};
	


	$.fn.ajaxForm = function(optionsParam){
		var globalLogger = $.logger.create();
		var $all = $(this);
		if($all.length < 1){
			return $all;
		}
		var options = $.extend({},defaultOptions,optionsParam);
		var $ajaxStatusBox = $(options.ajaxStatusBoxSelector);
		var statusBoxIsHidden = true;
		function init(){
			$all.each(function(key,obj){
				var $form = $(obj);
				var $submitButton = $form.find(options.submitButtonSelector);
				$submitButton.prop("onclick", null);
				$submitButton.unbind("click.ajaxForm");
				$submitButton.bind("click.ajaxForm",createSubmitClickHandler($form));
			});
		}
		
		function doSaveAjaxCall($form,data){
			
			var url = ($form.attr("action") || document.location.href);
			if(url.indexOf("format=json")=== -1){
				url += (url.indexOf("?") === -1 ? "?" : "&") + "format=json";
			}
			
			var callback = function(data){
				if(!options.preventBuiltInCallback){
					$.log(data.message);
				}
				options.saveCallback($form,data);
			};
			$.ajax({
				  url: url,
				  dataType: options.returnDataType,
				  data: data,
				  success: callback,
				  type: $form.attr("method") || "post"
			});
		}
		
		
		
		function createSubmitClickHandler($form){
			return function(event){
				event.preventDefault();
				event.stopPropagation();
				var data = {};
				
				$($form[0].elements).each(function(key,input){
					var $input = $(input);
					if($input.attr("disabled")){
						return 1;
					}
					var type = $input.attr("type");
					var name = $input.attr("name");
					var value;
					if(name){
						switch(type){
							case "checkbox":
								if($input.attr('checked')?true:false){
									value = $input.val();
								}else{
									value = null;
								}
								break;
							case "radio":
								if(!(name in data)){
									value = null;
								}
								if($input.attr('checked')?true:false){
									value = $input.val();
								}else{
									value = undefined;
								}
								break;
							case "select":
								if($input.checked){
									value = $input.val();
								}
								break;
							case "submit":
							case "button":
							break;
							default:
								value = $input.val();
							break;
						}
						if(value !== undefined){
							if(name && name.indexOf("[]") !== -1){
								if(!(name in data)){
									data[name] = [];
								}
								data[name][data[name].length] = value;
							}else{
								data[name] = value;
							}
						}
					}
				});
				
				doSaveAjaxCall($form,data);
				return false;
			};
		}
		
		init();
		return $all;
	};
}(jQuery));