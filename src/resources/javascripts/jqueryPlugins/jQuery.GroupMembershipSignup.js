(function($){

	var defaultOptions = {
		createMembershipUrl : "/admin/membership/new?format=json",
		triggerSelector : ".addMembershipButton",
		onSaveCallback : function(){}
	};

	$.fn.groupMembershipSignup = function(optionsParam){
		
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
	
		var options = $.extend({},defaultOptions,optionsParam);
		var group_id = options.group_id;
		var createMembershipUrl = options.createMembershipUrl;
		
		$this.each(function(index,container){
			
			var $container = $(container);
			var $trigger = $container.find(options.triggerSelector);
			var repository_type = "people";
			var searchUrl = options.searchUrl;

			function onSelect(data){
				var entityIDs = [];
				var i =0;
				for(var key in data){
					entityIDs[i++] = data[key].id;
				}
				$.ajax({
					  url: createMembershipUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"group_id" : group_id , "entityIDs" : entityIDs, "signup_type" : repository_type},
					  success: options.onSaveCallback
				});
			}
			
			$trigger.ajaxSelector({"serviceUrl" : searchUrl, "callback" : onSelect, "multiSelect" : false});
			
		});
		return $this;
	};
}(jQuery));