(function($){
	var defaultOptions = { 
		"deleteCallback" : function(data){
			console.log("done deleting");
		},
		"deleteUrlTemplate" : "/admin/resourcebooking/{:resource_booking_id}/delete?format=json",
		"deleteAlertMessage" : $.translate("resources/deleteBookingWarning")
	};
	$.fn.ajaxDeleteResourceBooking= function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $button = $(obj).hide();
			var pattern = new RegExp("/(\\d+)/", "g");
			
			var resource_booking_id = pattern.exec($button.attr("href"))[1];
			var $tr = $button.closest("tr");
			$tr.hover(function(){
				$button.show();
			},function(){
				$button.hide();
			});
			$button.prop("onclick",null);
			$button.click(function(event){
				event.preventDefault();
				event.stopPropagation();
				if(confirm(options.deleteAlertMessage)){
					var deleteUrl = options.deleteUrlTemplate.replace("{:resource_booking_id}",resource_booking_id);
					
					console.log(deleteUrl);
					$.ajax({
						  url: deleteUrl,
						  success: function(){
							$tr.remove();
						  },
						  type:"get"
					});
				}else{
					console.log("no deletion");
				}
				return false;	
			});
		});
		return $(this);
	};
}(jQuery));