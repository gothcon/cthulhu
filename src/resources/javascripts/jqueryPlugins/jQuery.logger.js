	(function($){
		
		if(!$.logger){
			$.logger = {};
			
			$.logger.log = function(message,defaultType){
				if(!$.logger.globalLogger){
					$.logger.globalLogger = $.logger.create(); 
				}
				$.logger.globalLogger.log(message,defaultType);
			}
			
			$.log = $.logger.log;
			
			$.logger.create = function(){

				var returnObject = {};
				
				var $ajaxInfoList = $("#infoList");
				var $ajaxWarningList = $("#warningList");
				var $ajaxErrorList = $("#errorList");
				
				if($ajaxInfoList.length == 0){
					$ajaxInfoList = $("<ul id='infoList'></ul>").prependTo("#mainContent").hide();
				}
				
				if($ajaxWarningList.length == 0){
					$ajaxWarningList = $("<ul id='warningList'></ul>").prependTo("#mainContent").hide();
				}
				
				if($ajaxErrorList.length == 0){
					$ajaxErrorList = $("<ul id='errorList'></ul>").prependTo("#mainContent").hide();
				}
				
				
				
				
				
				
				function isAnArray(subj){
					return Object.prototype.toString.call(subj) === "[object Array]";
				}
				
				function isAString(subj){
					return Object.prototype.toString.call(subj) === "[object String]";
				}
				
				function isAnObject(subj){
					return Object.prototype.toString.call(subj) === "[object Object]";
				}
				
				returnObject.log = function(messages,type){
					
					type = type || "info";
					
					if(messages && messages.length > 0){

						if(isAString(messages)){
							messages = [{message:messages,type:type}];
						}
						else if(isAnObject(messages)){
							messages = [messages];
						}

						var clearInfoList = true;
						var clearWarningList = true;
						var clearErrorList = true;

						for(var key in messages){
							var message = messages[key];
							if(isAString(messages)){
								message = {message:message,type:type};
							}
							switch(message.type){
								
								case "info":
									if(clearInfoList){
										$ajaxInfoList.html("").show();
										clearInfoList = false;
										$ajaxWarningList.hide();
										$ajaxErrorList.hide();
									}
									$ajaxInfoList.append("<li>"+ message.message +"</li>")
									break;
									
								case "warning" :
									if(clearWarningList){
										$ajaxWarningList.html("").show();
										clearWarningList = false;
										$ajaxErrorList.hide();
										$ajaxInfoList.hide();
									}
									$ajaxWarningList.append("<li>"+ message.message +"</li>")
									break;
									
								case "error" : 
									if(clearErrorList){
										$ajaxErrorList.html("").show();
										clearErrorList = false;
										$ajaxWarningList.hide();
										$ajaxInfoList.hide();
									}
									$ajaxErrorList.append("<li>"+ message.message +"</li>")
									break;
							}
						}
					}
				}
				return returnObject;
			}
		
			$(function(){ $.logger.globalLogger = $.logger.create(); 
				var $ajaxInfoList = $("#infoList");
				var $ajaxWarningList = $("#warningList");
				var $ajaxErrorList = $("#errorList");
			
				$ajaxInfoList.append("<li class='close'>(Klicka för att stänga)</li>");
				$ajaxWarningList.append("<li class='close'>(Klicka för att stänga)</li>");
				$ajaxErrorList.append("<li class='close'>(Klicka för att stänga)</li>");
			
				$ajaxInfoList.click(function(){
					$ajaxInfoList.hide();
				});
				
				$ajaxWarningList.click(function(){
					$ajaxWarningList.hide();
				});
				
				$ajaxErrorList.click(function(){
					$ajaxErrorList.hide();
				});
			
			});

}
	}(jQuery));