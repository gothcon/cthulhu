(function($){
	
	
	$.fn.tabbedContainer = function(parameters){
		var $tabbedContainer = $(this);
		var $tabs = $("<div class='tabs'></div>");
		var $sections = $tabbedContainer.find(".section");
		var initialHash = replace_swedish_chars(document.location.hash);
		var tabsShortCut = [];
		var defaultTab; 

		var onHashChange = function(event) {
			var cleanedHash = replace_swedish_chars(document.location.hash);
			if(tabsShortCut[cleanedHash] != undefined){
				tabsShortCut[cleanedHash].click();
			}else{
				defaultTab.click();
			}
		}

		function replace_swedish_chars(subject){
			var from = ["å","ä","ö","Å","Ä","Ö"," "];
			var to = ["a","a","o","A","A","O","_"];
			for(key in from){
				subject = subject.replace(RegExp(from[key],"g"),to[key]);
			}
			return subject;
		}

		if($sections.length > 0){
			$tabbedContainer.before($tabs);
			var $visibleSection = null;
			$sections.each(function(key,obj){
				var $obj = $(obj);
				var $label = $obj.find(".sectionHeader").hide();
				var tabHref = "#" + replace_swedish_chars($label.html());
				var $tab = $("<a class='tab' href='" + tabHref +"'></a>");
				tabsShortCut[tabHref] = $tab;
				if(key==0){
					defaultTab = $tab;
				}
				$tab.append($label.html() +"<span class='tabEnd'></span>");
				$obj.data("tab",$tab);
				$tabs.append($tab);
				$tab.click(function(){
					if($visibleSection != null){
						$visibleSection.data("tab").removeClass("active");
						$visibleSection.hide();
					}
					$visibleSection = $obj;
					$obj.data("tab").addClass("active");
					$obj.show();
				});
				$obj.hide();
			});
			var initialTabToClick = $tabs.find("[href=" + initialHash + "]");
			if (initialHash === "" || initialTabToClick.length == 0) {
				$sections.first().show();
				$visibleSection = $sections.first();
				$visibleSection.data("tab").addClass("active");
			}else{
				$(initialTabToClick).click();
			}
			
			$(window).bind('hashchange',onHashChange)
			
		}
	}
}(jQuery));