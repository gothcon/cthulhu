(function($){

    var defaultOptions = {
        serviceUrl: '/ajax.php',
        searchDelay: 200,
        maxNoOfResult: 10,
        minimumQueryLength : 2,
        callback: function(){},
    	queryToShortCallback : function(){}
    };


	$.fn.ajaxSearcher = function(optionsParam){
		
		var $this = $(this);
		var recentSearchData;
		var recentSearchTerm;
		
		if($this.length === 0){
			return;
		}

		var options = $.extend({}, defaultOptions, optionsParam );
		var timer = null;

		function _init(){
			_addChangeHandler();
			if(options.minimumQueryLength < 1){
				var initialValue = $this.val();
				$this.val("");
				var timer = setTimeout(function(){_doAjaxCall(); $this.val(initialValue);},options.searchDelay);
			}
			// _doAjaxCall();
		}

		function _doAjaxCall(){
			
			var value = $this.val();
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
				  data: {"q" : value , "max" : options.maxNoOfResults, "sender" : $this.selector},
				  success: function(data){
					  recentSearchTerm = value;
					  recentSearchData = data;
					  options.callback(data);
				  }
			};
			var $call = $.ajax(argumentObject);
		}


		function _addChangeHandler(){
			$this.keyup(function(event){
				
				if((event.keyCode < 48 || (event.keyCode > 90 && event.keyCode < 96) || event.keyCode > 105) && event.keyCode !== 8){
					event.preventDefault();
					return;
				}else if(recentSearchData && $this.val().indexOf(recentSearchTerm) !== -1){
					//console.log("refine search");
					// refine search eg. search the already fetched data
					recentSearchTerm = $this.val();
					var pattern = new RegExp("(^| )" + recentSearchTerm,"i");
					var newSearchData = [];
					for(var key in recentSearchData.data){
						if(pattern.test(recentSearchData.data[key].label)){
							newSearchData[newSearchData.length] = recentSearchData.data[key];
						}
					}
					recentSearchData.data = newSearchData;
					options.callback(recentSearchData);
				}else if($this.val().length >= options.minimumQueryLength){
					if(timer != null){
						clearTimeout(timer);
					}
					timer = setTimeout(_doAjaxCall,options.searchDelay);
				}else if(recentSearchData){
					options.callback({data:[],message:null,status:-1});
				}else{
					//console.log("do nothing");
				}
			});
		}
		_init();
	};
})(jQuery);