(function($){
	var defaultOptions = {
		membershipTableSelector : "#groupMembershipTable",
		groupUrlTemplate : "/admin/group/{:group_id}",
		serviceUrl:"/admin/search/?format=json&r=groups",
		yesLabel : "yes",
		noLabel : "no",
		deleteLabel : "delete",
		deleteMembershipUrlTemplate : "/admin/groupMembership/{:membership_id}/delete"
	};
	$.fn.groupSelector =  function(person_id,optionsParam){

		var options = $.extend( {} , defaultOptions, optionsParam );
		
		var $trigger = $(this);
		var $table = $(options.membershipTableSelector);
		
		
		function createButton(label,link){
			label = label || "Button";
			link = link || "#";
			return $("<a class='deleteButton buttonLink' href='"+link+"'>"+label+"<span class=\"end\"/></a>");
		}
		
		function createTableRow(membership){
			var link = options.groupUrlTemplate.replace("{:group_id}",membership.group.id);
			var deleteUrl = options.deleteMembershipUrlTemplate.replace("{:group_membership_id}",membership.id);
			
			var $tr = $("<tr></tr>");
			$("<td><a href='"+link+"'>"+membership.group.id+"</a></td>").appendTo($tr);
			$("<td><a href='"+link+"'>"+membership.group.name+"</a></td>").appendTo($tr);
			$("<td>"+ (membership.is_leader === 1 ? options.yesLabel : options.noLabel) +"</td>").appendTo($tr);
			createButton(options.deleteLabel,deleteUrl).appendTo($tr);
			return $tr;
		}
		
		$trigger.ajaxSelector({
			callback : function (data){
				console.log(data);
				$.gc.ajax.saveMembership(data[0].id,person_id,function(data){
					createTableRow(data.data[0]).appendTo($table);
				});
			},
			multiselect : false,
			serviceUrl:options.serviceUrl
		});
	};
	

})(jQuery);