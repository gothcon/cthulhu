(function($){

	var defaultOptions = {
		validateSignupUrlTemplate : "/admin/signup/{:signup_id}/save",
		saveUrlTemplate : "/admin/signup/new?format=json",
		deleteUrlTemplate : "/admin/signup/{:signup_id}/delete?format=json",
		searchUrlTemplate : "/admin/search/?r={:repository}&format=json",
		personLinkTemplate : "/admin/person/{:person_id}#Anmalningar_och_lagmedlemskap",
		triggerSelector : ".addSignupButton",
		tableSelector : ".signupTable",
		searchRepositoryInputSelector : "input[name=searchRepository]",
		signupSlotIdInputSelector : "input[name=signup_slot_id]",
		deleteButtonSelector : ".submitButtonLink",
		deleteAlertMessage : "Detta kommer att ta bort anmälningen, är du säker på att du vill fortsätta?",
		preventBuiltInCallback : false,
		ajaxStatusBoxSelector : "#ajaxStatusBox",
		deleteCallback : function(){}
	};
	
	var helperFunctions = {};

	//	saveSignupUrl : ,
	//	listUrl : ,
	/*
	eventSignup(
		"person",
		123,
		523,
		"signups_table_1",
		{}
	)
	*/
	$.fn.eventSignup = function(optionsParam){
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
		
		function createSignupUrl(urlTemplate){
			return urlTemplate;
		}
		
		function createSearchUrl(repository_type,urlTemplate){
			return urlTemplate.replace("{:repository}",repository_type);
		}

		function doDeleteAjaxCall(id,callback){
			var deleteUrl = options.deleteUrlTemplate.replace("{:signup_id}",id);
			$.ajax({
				  url: deleteUrl,
				  dataType: 'json',
				  data: {"signup_id" : id },
				  success: callback,
				  type:"delete"
			});
		}
		
		var options = $.extend({},defaultOptions,optionsParam);
		
		var saveSignupUrl = options.saveUrlTemplate;
		var $ajaxStatusBox = $(options.ajaxStatusBoxSelector);
		var statusBoxIsHidden = true;
		
		$this.each(function(index,container){
			var $container = $(container);
			var $trigger = $container.find(options.triggerSelector);
			var $signupTable = $container.find(options.tableSelector);
			var repository_type = $container.find(options.searchRepositoryInputSelector).val();
			var signup_slot_id = $container.find(options.signupSlotIdInputSelector).val();
			var searchUrl = createSearchUrl(repository_type,options.searchUrlTemplate);
			
			$container.find(options.deleteButtonSelector).each(function(key,obj){
				var $link = $(obj);
				var pattern = /(\d+)/g;
				var signupId = pattern.exec($link.attr("href"))[1];
				$link.prop("onclick", null);
				$link.click(createDeleteBtnClickCallback(signupId,$link));
			});
			
			$container.find(".signupForm").each(function(key,obj){
				var $this = $(obj); 
				var $submitButton = $this.find(".submitButton").hide();
				var $radio = $this.find("[name=is_approved],[name=is_paid]");
				
				$radio.each(function(key,obj){
					var $radio = $(obj);
					var $form = $radio.closest("form");
					$radio.change(function(event){
						$radio.closest("form").submit();
					});
				});
			});
			
			function createSignupRow(signup,slot,label){
				if(label === undefined){
					if(signup.group_id != null){
						label = signup.group.id  + ". " + signup.group.name;
					}else{
						label = signup.person.id + ". " + signup.person.first_name + " " + signup.person.last_name;
					}
				}
				var $tableRow = $("<tr></tr>");
				var $labelCell = $("<td>"+label+"</td>").appendTo($tableRow);
			
				var validationSaveUrl = options.validateSignupUrlTemplate.replace("{:signup_id}", signup.id);
			
				if(slot.requires_approval === "1"){
					var $validationCell = $("<td class='approvalCol'></td>").appendTo($tableRow);
					var $form = $("<form action='"+ validationSaveUrl +"' method='post' class='signupForm ajaxForm'>").appendTo($validationCell);
					var $checkbox = $("<input style='float:none; display:inline' type='checkbox' name='is_approved' value='1'/>").appendTo($form);
					$checkbox.change(function(event){
						$form.submit();
					});
				}else{
					$("<td class='approvalCol'> - </td>").appendTo($tableRow);
				}
				
				if(slot.signup_slot_article.article_id){
					var $paymentCell = $("<td class='approvalCol'></td>").appendTo($tableRow);
					var $paymentForm = $("<form action='"+ validationSaveUrl +"' method='post' class='signupForm ajaxForm'>").appendTo($paymentCell);
					var $paymentCheckbox = $("<input style='float:none; display:inline' type='checkbox' name='is_paid' value='1'/>").appendTo($paymentForm);
					$paymentCheckbox.change(function(){
						$paymentForm.submit();
					});
				}else{
					$("<td class='approvalCol'> - </td>").appendTo($tableRow);
				}
				
				var $buttonCell = $("<td></td>").appendTo($tableRow);
				var $deleteButton = $("<a href='#' class='buttonLink deleteButton'>"+$.translate("general/delete")+"<span class='end'></span></a>");
				$deleteButton.hide();
				$tableRow.bind("mouseover",function(){
					$deleteButton.show();
				}).bind("mouseout",function(){
					$deleteButton.hide();
				});
				$deleteButton.click(createDeleteBtnClickCallback(signup.id,$deleteButton)).appendTo($buttonCell);
				
				return $tableRow;
			}
			
			function onSaveSignup(response){
				var slot = response.data.signup_slot;
				var signups = response.data.signups;
				for(var key in signups){
					var $tableRow = createSignupRow(response.data.signups[key],slot).appendTo($signupTable);
				}
			}			
			
			function onSelect(data){
				var entityIDs = [];
				var i =0;
				for(var key in data){
					entityIDs[i++] = data[key].id;
				}
				
				var callback = function(response){
					onSaveSignup(data,response);
				};
				
				$.ajax({
					  url: saveSignupUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"signup_slot_id" : signup_slot_id , "entityIDs" : entityIDs, "signup_type" : repository_type},
					  success: onSaveSignup
				});
			}
			
			function createDeleteBtnClickCallback(signupId,$deleteButton){
				return function(event){
					if(confirm(options.deleteAlertMessage)){
						doDeleteAjaxCall(signupId,function(response){
							$deleteButton.closest("tr").remove();
							if(!options.preventBuiltInCallback){
								$.logger.log(response.message);
							}
							options.deleteCallback({},response);
						});
					}else{
						console.log("no deletion");
					}
					
					event.preventDefault();
					event.stopPropagation();
					return false;	
				};
			}
			
			$trigger.ajaxSelector({"serviceUrl" : searchUrl, "callback" : onSelect, "multiSelect" : false});
			
		});
	};
}(jQuery));