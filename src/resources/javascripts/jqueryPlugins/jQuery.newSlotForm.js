(function($){
	var defaultOptions = {"headerIdentifier" : "h5", "fieldsetIdentifier" : ".formFieldset"}
	$.fn.newSlotForm = function(optionsParam){
		var options = $.extend({},defaultOptions,optionsParam);
		$(this).each(function(key,obj){
			var $slotFormContainer = $(obj).hide();
			var $sektionstitel = $($slotFormContainer.closest("li").find("h3.passnamn"));
			
			var $fieldset = $slotFormContainer.find(options.fieldsetIdentifier).hide();
			var $anchor =  $("<a href='/admin/event_occation/new' class='editLink'>"+$.translate("signupSlot/new")+"</a>").insertAfter($sektionstitel);
			// var $anchor = $header.find("a");
			var enterEditState = function(event){
				event.preventDefault();
				event.stopPropagation();
				$slotFormContainer.show();
				$fieldset.slideDown("800");
				$anchor.unbind("click");
				$anchor.click(leaveEditState);
				
			}
			var leaveEditState = function(event){
				event.preventDefault();
				event.stopPropagation();
				$fieldset.slideUp("800",function(){
					$slotFormContainer.hide();
				});
				$anchor.unbind("click");
				$anchor.click(enterEditState);
			}
			$anchor.click(enterEditState);
		});
	}
}(jQuery)); 