(function($){
	var defaultOptions = {
		"eventTreeUrlTemplate" : "/admin/event/?format=json&mode=tree&person_id={:person_id}",
		"newSignupUrl" : "/admin/signup/new?format=json",
		"deleteSignupUrlTemplate" : "/admin/signup/{:signup_id}/delete",
		"personUrlTemplate" : "/admin/person/{:person_id}/",
		"signupButtonLabel" : "sign up",
		"deleteLabel" : "delete",
		"eventUrlTemplate":"/admin/arrangemang/{:event_id}",
		"eventTypeUrlTemplate" : "/admin/arrangemangstyp/{:event_type_id}"
	};
	
	$.fn.individualEventSignup = function(person_id,optionsParam){

		var options = $.extend( {} , defaultOptions, optionsParam );

		var groups;
		var noOfTableRows = 0;
		$(this).each(function(key,obj){

			function createDeleteButtonClickHandler(deleteHref,onDeleteSuccess){
				return function(event){
					event.preventDefault();
					event.stopPropagation();
					$.ajax({
						url: deleteHref,
						data : {'id' : person_id},
						success : onDeleteSuccess
					});
				};
			}

			function transformTable($table){
				$table.find("tr").each(function(key,obj){
					noOfTableRows++;
					if(key === 0){
						return true;
					}
					transformTableRow($(obj));
					return true; 
				});
			}

			function transformTableRow($tr){

				var $deleteButton = $($tr.find(".deleteButton")[0]);
				$deleteButton.removeProp("onclick");

				var onDeleteSuccess = (function($tr){
					return function(){
						$tr.remove();
					};
				}($tr));

				var onDeleteButtonClick = (createDeleteButtonClickHandler($deleteButton.attr("href") + "?format=json",onDeleteSuccess));	

				$deleteButton.click(onDeleteButtonClick);
			}

			function transformLink($link){
				$("<div id='signupTree'></div>").insertAfter($link);
				$link.removeProp("onclick");


				$link.click(function(event){
					event.preventDefault();
					event.stopPropagation();
					$.openDOMWindow({modal : false, width:600, height:400, functionCallOnOpen: onWindowOpen, windowBGColor : "#ddd" });
					return false;
				});

			}

			function onWindowOpen(){
				populateWindow();
			}

			function populateWindow(){
				$.ajax({
					  url:  options.eventTreeUrlTemplate.replace("{:person_id}",person_id),
					  dataType: 'json',
					  success: function(data){
						var $domWindow = $("#DOMWindow");
						var $outerContainer = $("<div class='domOuterContainer'></div>").appendTo($domWindow);
						var $container = $("<div class='domInnerContainer'></div>").appendTo($outerContainer);
						$("<a href='' class='buttonLink' id='closeBtn'>X<span class='end'></a>").appendTo($outerContainer).bind("click",function(event){
							event.preventDefault();
							event.stopPropagation();
							$.closeDOMWindow();
							return false;
						});
						var $eventTypeList = $("<ul class='eventTypeList'></ul>").appendTo($container);
						var eventTypes = data.data;
						for(var eventTypeKey in eventTypes){
							var eventType = eventTypes[eventTypeKey];
							createEventTypeListItem(eventType).appendTo($eventTypeList);
						}
					  }
				});						
			}

			function createEventTypeListItem(eventType){
				var $eventTypeListItem = $("<li><span class='label'>"+eventType.name+"</span></li>"); 
				var $eventList = $("<ul class='eventList'></ul>").appendTo($eventTypeListItem);
				var events = eventType.events; 
				for(var eventKey in events){
					createEventListItem(events[eventKey]).appendTo($eventList);
				}
				return $eventTypeListItem;
			}

			function createEventListItem(event){
				var $eventListItem = $("<li><span class='label'>"+event.name+"</span></li>"); 
				var $occationList = $("<ul class='eventOccationList'></ul>").appendTo($eventListItem);
				var event_occations = event.event_occations;
				for(var occationKey in event_occations){
					var occation = event_occations[occationKey];
					createOccationListItem(occation).appendTo($occationList);
				}
				return $eventListItem;
			}

			function createOccationListItem(occation){
				// var timespanName = occation.timespan.name == "" ? occation.timespan.starts_at + " - " + occation.timespan_ends_at : occation.timespan.name;
				var timespanName = occation.full_name;
				var $occationListItem = $("<li><span class='label'>"+ timespanName +"</span></li>"); 
				var $slotList = $("<ul class='signupSlotList'></ul>").appendTo($occationListItem);
				var signup_slots = occation.signup_slots;
				for(var slotKey in signup_slots){
					var slot = signup_slots[slotKey];
					if(slot.slot_type_cardinality !== 1){
						for(var groupKey in groups){
							createGroupSignupSlotListItem(slot).appendTo($slotList);
						}
					}else{
						createSignupSlotListItem(slot).appendTo($slotList);
					}	
				}
				return $occationListItem;
			}

			function createGroupSignupSlotListItem(slot){
				var group = groups[groupKey].group;
				var $slotListItem = $("<li>"+slot.slot_type_name+": <em>"+ group.name +"</em></li>"); 
				var $signupButton = $("<input type='button' value='anmäl'/>").appendTo($slotListItem);
				$signupButton.click(createSignupButtonClickHandler(slot.id,group.id,$signupButton,"group"));
				return $slotListItem;
			}

			function createSignupSlotListItem(slot){
				var $slotListItem = $("<li>"+slot.slot_type_name+"</li>"); 
				if(slot.slot_status !== "alreadySignedUp"){
					var $signupButton = $("<input type='button' value='"+options.signupButtonLabel+"'/>").appendTo($slotListItem);
					$signupButton.click(createSignupButtonClickHandler(slot.id,person_id,$signupButton));
				}	
				return $slotListItem;
			}

			function createSignupButtonClickHandler(slot_id,group_id,$button,signupType,$table){

				if(signupType !== "group"){
					signupType = "people";
				}

				return function(event){

					$button.attr("disabled","disabled");
					$.ajax({
						url: options.newSignupUrl,
						data : {
							'signup_type' : signupType,
							'entityIDs' :  [group_id],
							'signup_slot_id' : slot_id
						},
						success : createSignupSuccessHandler($button,$table),
						type : "POST"
					});
				};
			}

			function createSignupSuccessHandler($button){
				return function(data){
					$button.fadeOut(300);
					signup = data.data.signups[0];
					createNewRow(signup, noOfTableRows % 2 ? "odd" : "even" ).appendTo($table);
					noOfTableRows--;
				};
			}

			function createNewRow(signup, rowCssClass){
				var $row = $("<tr class='"+ rowCssClass +"'></tr>");
				var $cells = [];
				for(i=0; i < 5; i++){
					$cells[i] = $("<td></td>");
					$row.append($cells[i]);
				}
				var slotType = signup.signupslot.slot_type;
				var timespan = signup.signupslot.event_occation.timespan;
				var event = signup.signupslot.event_occation.event;
				var $eventLink = $("<a href='"+ options.eventUrlTemplate.replace("{:event_id}",event.id)+"'>"+event.name+"</a>");
				var $eventOccationLink =  $("<a href='"+ options.eventOccationUrlTemplate.replace("{:event_id}",event.id)+"'>"+timespan.asString+"</a>");

				$cells[0].append($eventOccationLink);
				$cells[1].append($eventLink);
				$cells[2].html(slotType.name);
				$cells[3].html();
				$cells[4].html("<a class='deleteButton buttonLink' href='"+options.deleteSignupUrlTemplate.replace("{:signup_id}",signup.id)+"'>"+options.deleteLabel+"<span class=\"end\"/></a>");
				transformTableRow($row);
				return $row;
			}


			var $table = $(obj);
			transformTable($table);
			transformLink($(".addNewSignupLink"));
		});
		
		return $(this);
		
	};
}(jQuery));