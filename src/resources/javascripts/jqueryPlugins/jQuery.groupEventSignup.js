(function($){
	
	var defaultOptions = {
		"newSignupUrl" : "/admin/registrering/ny?format=json",
		"eventTreeUrlTemplate" : "/admin/arrangemang/?format=json&mode=tree&cardinality=group&group_id={:group_id}",
		"deleteSignupUrlTemplate" : "/admin/registrering/{:signup_id}/delete"
	};
	
	$.fn.groupEventSignup = function(group_id,optionsParam){
	
		var options = $.extend({},defaultOptions,optionsParam);

		function createDeleteButtonClickHandler(deleteHref,onDeleteSuccess){
			return function(event){
				event.preventDefault();
				event.stopPropagation();
				$.ajax({
					url: deleteHref,
					success : onDeleteSuccess
				});
			};
		}

		function transformTable($table){
			$table.find("tr").each(function(key,obj){
				noOfTableRows++;
				if(key === 0){
					return true;
				}
				transformTableRow($(obj));
			});
		}

		function transformTableRow($tr){
			var $deleteButton = $($tr.find(".deleteButton")[0]);
			$deleteButton.removeProp("onclick");
			var onDeleteSuccess = (function($tr){
				return function(data){
					$.logger.log(data.message);
					$tr.remove();
				};
			}($tr));
			$deleteButton.click(createDeleteButtonClickHandler($deleteButton.attr("href") + "&format=json",onDeleteSuccess));
		}

		function transformLink($link){
			var $content = $("<div id='signupTree'></div>").insertAfter($link);
			$link.removeProp("onclick");
			$link.click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$.openDOMWindow({modal : false, width:600, height:400, functionCallOnOpen: onWindowOpen, windowBGColor : "#ddd" });
				return false;
			});

		}

		function onWindowOpen(){
			$.ajax({
				url: "/admin/grupp/?format=json",
				data : {'id' : group_id},
				success : function(data){ 
					var groups = data.data;
					populateWindow();
				}
			});
		}

		function populateWindow(){
			$.ajax({
				  url: options.eventTreeUrlTemplate.replace("{:group_id}",group_id),
				  dataType: 'json',
				  success: function(data){
					var $domWindow = $("#DOMWindow");
					var $outerContainer = $("<div class='domOuterContainer'></div>").appendTo($domWindow);
					var $container = $("<div class='domInnerContainer'></div>").appendTo($outerContainer);
					$("<a href='' class='buttonLink' id='closeBtn'>X<span class='end'></a>").appendTo($outerContainer).bind("click",function(event){
						event.preventDefault();
						event.stopPropagation();
						$.closeDOMWindow();
						return false;
					});

					var $eventTypeList = $("<ul class='eventTypeList'></ul>").appendTo($container);
					var eventTypes = data.data;
					for(var eventTypeKey in eventTypes){
						var eventType = eventTypes[eventTypeKey];
						createEventTypeListItem(eventType).appendTo($eventTypeList);
					}
				  }
			});						
		}

		function createEventTypeListItem(eventType){
			var $eventTypeListItem = $("<li><span class='label'>"+eventType.name+"</span></li>"); 
			var $eventList = $("<ul class='eventList'></ul>").appendTo($eventTypeListItem);
			var events = eventType.events; 
			for(var eventKey in events){
				createEventListItem(events[eventKey]).appendTo($eventList);
			}
			return $eventTypeListItem;
		}

		function createEventListItem(event){
			var $eventListItem = $("<li><span class='label'>"+event.name+"</span></li>"); 
			var $occationList = $("<ul class='eventOccationList'></ul>").appendTo($eventListItem);
			var event_occations = event.event_occations;
			for(var occationKey in event_occations){
				var occation = event_occations[occationKey];
				createOccationListItem(occation).appendTo($occationList);
			}
			return $eventListItem;
		}

		function createOccationListItem(occation){

			var $occationListItem = $("<li><span class='label'>"+ occation.full_name +"</span></li>"); 
			var $slotList = $("<ul class='signupSlotList'></ul>").appendTo($occationListItem);
			var signup_slots = occation.signup_slots;
			for(var slotKey in signup_slots){
				var slot = signup_slots[slotKey];
				createSignupSlotListItem(slot).appendTo($slotList);
			}
			return $occationListItem;
		}

		function createSignupSlotListItem(slot){
			var $slotListItem = $("<li>"+slot.slot_type_name+"</li>"); 
			if(slot.slot_status !== "alreadySignedUp"){
				var $signupButton = $("<input type='button' value='anmäl'/>").appendTo($slotListItem);
				$signupButton.click(createSignupButtonClickHandler(slot.id,group_id,$signupButton));
			}
			return $slotListItem;
		}

		function createSignupButtonClickHandler(slot_id,group_id,$button,signupType,$table){

			signupType = "group";

			return function(event){

				$button.attr("disabled","disabled");
				$.ajax({
					url: options.newSignupUrl,
					data : {
						'signup_type' : signupType,
						'entityIDs' :  [group_id],
						'signup_slot_id' : slot_id
					},
					success : createSignupSuccessHandler($button,$table),
					type : "POST"
				});
			};
		}

		function createSignupSuccessHandler($button){
			return function(data){
				$button.fadeOut(300);
				var signup = data.data.signups[0];
				$.logger.log(data.message);
				createNewRow(signup, noOfTableRows % 2 ? "odd" : "even" ).appendTo($table);
				noOfTableRows--;
			};
		}

		function createNewRow(signup, rowCssClass){
			var $row = $("<tr class='"+ rowCssClass +"'>");
			var $cells = [];
			for(var i=0; i < 5; i++){
				$cells[i] = $("<td>");
				$row.append($cells[i]);
			}
			var slot = signup.signupslot;
			var slotType = signup.signupslot.slot_type;
			var occation = signup.signupslot.event_occation;
			var timespan = signup.signupslot.event_occation.timespan;
			var event = signup.signupslot.event_occation.event;

			$cells[0].html(timespan.asString);
			$cells[1].html(event.name);
			$cells[2].html(slotType.name);
			$cells[3].html();
			$cells[4].html("<a class='deleteButton buttonLink' href='"+options.deleteSignupUrlTemplate.replace("{:signup_id}",signup.id)+"'>Ta bort<span class=\"end\"/></span></a></a>");
			transformTableRow($row);
			return $row;
		}

		var $table = $(this);
		var noOfTableRows = 0;
		transformTable($table);
		transformLink($(".addNewSignupLink"));
		
	};
}(jQuery));
