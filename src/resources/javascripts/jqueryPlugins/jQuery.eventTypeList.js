(function($){
	var defaultOptions = {
		saveUrl : "/admin/eventtype/{0}/save/?format=json",
		newUrl : "/admin/eventtype/new/?format=json",
		deleteUrl : "/admin/eventtype/{0}/delete?format=json"
	};	
	$.fn.eventTypeList = function(optionsParam){
		var $this = this;
		if($this.length < 1){
			return $this;
		}

		var options = $.extend({},defaultOptions,optionsParam);

		function doAjaxSave(requestUrl,data,callback){
			$.ajax({
				  url: requestUrl,
				  dataType: 'json',
				  type : 'POST',
				  data: data,
				  success: callback
			});
		}

		function doAjaxDelete(requestUrl,resourceTypeId,callback){
			$.ajax({
				  url: requestUrl,
				  dataType: 'json',
				  type : 'POST',
				  data: { "id" : resourceTypeId},
				  success: callback
			});
		}

		function transformRow($tr){
			var $idCell = $($tr.children()[0]);
			var $nameCell = $($tr.children()[1]);
			var $buttonCell = $($tr.children()[2]);

			var $idLink = $($idCell.find("a")[0]);
			var id = $idLink.text();
			var $idSpan = createIdSpan(id);
			$idLink.replaceWith($idSpan);

			var $nameLink = $($nameCell.find("a")[0]);
			var name = $nameLink.text();
			var $nameSpan = $("<span class='nameSpan'>"+name+"</span>");
			$nameLink.replaceWith($nameSpan);

			var $editButton = $($buttonCell.find(".editButton")[0]).removeProp("click");
			var $deleteButton = $($buttonCell.find(".deleteButton")[0]).hide().removeProp("click");
			var $saveButton = createSaveButton().hide().insertAfter($editButton);
			var $abortButton = createAbortButton().insertAfter($saveButton).hide();
			var $nameInput = createNameInput(name).appendTo($nameCell).hide();

			var leaveEditState = function(){
				$deleteButton.hide();
				$saveButton.hide();
				$abortButton.hide();
				$editButton.show();
				$nameInput.hide();
				$nameSpan.show();
			};

			var enterEditState = function(){
				$editButton.hide();
				$deleteButton.show();
				$abortButton.show();
				$saveButton.show();
				$nameSpan.hide();
				$nameInput.show();
			};

			$abortButton.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				leaveEditState();
				return false;
			});

			$editButton.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				enterEditState();
				return false;
			});

			$deleteButton.click(createDeleteButtonClick($tr,id));

			$saveButton.click(createSaveButtonClick($tr,"save",function(data){
				$nameSpan.text(data.data.name);
				$idSpan.text(data.data.id);
				leaveEditState();
				console.log(data.message);
			}));
		}

		function transformRows($table){
			var $rows = $table.find("tr"); 
			$rows.each(function(key,row){
				if(key === 0 || key === $rows.length-1){
					return true;
				}
				transformRow($(row));
			});
		}

		function createSaveButton(label){
			label = label || $.translate("general/save"); 
			var $saveButton = $("<a href='#spara' class='buttonLink saveButton'>" + label + "<span class='end'></span></a>");
			return $saveButton;
		}						

		function createAbortButton(){
			var label = $.translate("general/abort"); 
			var $button = $("<a href='#avbryt' class='buttonLink abortButton'>" + label + "<span class='end'></span></a>");
			return $button;
		}

		function createEditButton(){
			var label = $.translate("general/edit"); 
			var $button = $("<a href='#redigera' class='buttonLink editButton'>" + label + "<span class='end'></span></a>");
			return $button;
		}

		function createDeleteButton(){
			var $deleteButton = $("<a href='#' class='buttonLink deleteButton'>Ta bort<span class='end'></span></a>");
			return $deleteButton;
		}

		function createNameInput(name){
			name = name || "";
			return $("<input type='text' name='name' class='nameInput' value='"+name+"'/>");
		}

		function createIdSpan(innerText){
			return $("<span class='idSpan'>" + (innerText || "") + "</span>");
		}

		function createNameSpan(){
			return $("<span class='nameSpan'></span>");
		}

		function createSaveButtonClick($tr,operation,ajaxCallback){

			operation = operation || "save";
			return function(event){
				var data = getData($tr);
				var saveUrl = (operation === "new") ? options.newUrl: options.saveUrl.replace("{0}",data.id);
				doAjaxSave(saveUrl,data,ajaxCallback);
				event.stopPropagation();
				event.preventDefault();
				return false;
			};
		}

		function createNewButtonClick($table){
			return function(event){
				var $ajaxCallback = function(data){
					// create row
					var $tr = $("<tr>");
					var $td1 = $("<td>");
					var $td2 = $("<td>");
					var $td3 = $("<td>");
					$table.append($tr.append($td1).append($td2).append($td3));

					// create the input field and name textspan
					var $nameInput = $("<input type='text' value='"+ data.data.name +"'>");
					var $nameSpan = $("<span>"+data.data.name+"</span>");
					// create the save button
					var $saveButton = createSaveButton($.translate("general/save"));
					$saveButton.click(createSaveButtonClick($nameInput));
					// create the delete button
					var $deleteButton = createDeleteButton().click(createDeleteButtonClick($tr,data.data.id));
					$table.append($tr.append($td1).append($td2.append($nameSpan)).append($td3.append($saveButton)));
				};
			};
		}

		function createDeleteButtonClick($tr,typeId){

			var ajaxCallback = function(data){
				$tr.remove();
			};
			var deleteUrl = options.deleteUrl.replace("{0}",typeId);

			return function(event){
				doAjaxDelete(deleteUrl,typeId,ajaxCallback);
				event.stopPropagation();
				event.preventDefault();
				return false;
			};
		}



		function getData($tr){
			var id = $($tr.find(".idSpan")[0]).text();
			var name = $($tr.find(".nameInput")[0]).val();
			var data =  {"id" : id, "name" : name};
			return data;
		}

		function addNewRow($table,data){
			var $idSpan = createIdSpan($.translate("general/new"));
			var $nameSpan = createNameSpan().hide();
			var $nameInput = createNameInput();
			var $saveButton = createSaveButton();
			var $abortButton = createAbortButton();
			var $editButton = createEditButton();
			var $deleteButton = createDeleteButton();
			var $idCell = $("<td></td>").append($idSpan);
			var $nameCell = $("<td></td>").append($nameInput).append($nameSpan);
			var $buttonCell = $("<td></td>").append($deleteButton.hide()).append($saveButton).append($abortButton.hide()).append($editButton.hide());
			var $tr = $("<tr></tr>").append($idCell).append($nameCell).append($buttonCell).hide(); 

			var leaveEditState = function(){
				$nameInput.hide();
				$nameSpan.show();
				$saveButton.hide();
				$abortButton.hide();
				$deleteButton.hide();
				$editButton.show();
			};

			var enterEditState = function(){
				$nameInput.show();
				$nameSpan.hide();
				$saveButton.show();
				$abortButton.show();
				$deleteButton.show();
				$editButton.hide();
			};

			$saveButton.click(createSaveButtonClick($tr,"new",function(data){
				$nameSpan.text(data.data.name);
				$idSpan.text(data.data.id);
				leaveEditState();
				$saveButton.click(createSaveButtonClick($tr,"save",function(data){
					$nameSpan.text(data.name);
					$idSpan.text(data.id);
					leaveEditState();
					addNewRow($table,data);
				}));
			}));

			if(data){
				$deleteButton.click(createDeleteButtonClick($tr,data.data.id));
			}

			$editButton.click(enterEditState);
			$abortButton.click(leaveEditState);
			$tr.appendTo($table);
			return $tr;
		}

		function transformNewTypeLink($link,$row){
			$link.click(function(event){
				event.stopPropagation();
				event.preventDefault();
				$row.show();
				return false;
			});
		}

		function _transformRow($tr){

		}

		function _addHandlersAndCallbacks($tr,$table){

		}

		function _transformNewTypeLink($table,selector){



		}

		function addNewTypeFormRow($table){

		}

		function createRow(type){
			return $("<tr><td>"+type.id+"</td><td>"+type.name+"</td><td><a href='#redigera' class='buttonLink editButton'>"+$.translate("general/edit") +"<span class='end'></span></a><a href='#' class='buttonLink deleteButton'>"+$.translate("general/delete") +"<span class='end'></span></a></td></tr>");
		}


		return $this.each(function(key,obj){
			var $table = $(obj);
			var $link = $($table.siblings()[0]);
			var $newRow = addNewRow($table);

			transformRows($table);
			transformNewTypeLink($link,$newRow);

		});
	};
})(jQuery);

