(function($){

	var getCount = (function(){
		var _count = -1;
		return function(){
			_count +=1;
			return _count;
		}
	}());

	var defaultOptions = {
		callback : function(){},
		multiSelect : false,
		searchTerm : "Sök",
		serviceUrl : "/ajax.php",
		maxNoOfResults:100,
		hideId : false
	}

	$.fn.suggestionInput =  function(optionsParam){
		var options = $.extend( {} , defaultOptions, optionsParam );
		var $input = $(this);
		var $list = $("<ul class=\"suggestions\"></ul>");
		var hasValues = false;
		var inititalValue;
		
		var selectedListItem = -1;
		var listTopOffset = 26;
		$list.insertAfter($input).css({"position" : "absolute"}).hide();
		$input.parent().css({"position":"relative"});
		var objects = [];
		
		function onKeyUp(event){
			event.preventDefault();
			event.stopPropagation();
			if(event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 9){
				if(event.keyCode == 9){
					signalNewValue();
					inactivateSuggestions();
				}else{
					event.preventDefault();
					event.stopPropagation();
					if(event.keyCode == 13 || event.keyCode == 9){
						signalNewValue();
						inactivateSuggestions();
					}
					else if(event.keyCode == 27){
						resetValue();
						inactivateSuggestions();
					}else if(hasValues){
						if(event.keyCode == 38){
							// arrow up
							selectedListItem = selectedListItem > 0 ? selectedListItem -1 : selectedListItem;
						}else if(event.keyCode == 40){
							// arrow down
							selectedListItem = selectedListItem < $list.children().length-1 ? selectedListItem +1 : selectedListItem;
						}
						if(selectedListItem >= 0){
							$input.val($($list.children()[selectedListItem]).html());
						}
						$list.css({"top" : listTopOffset -((selectedListItem+1) * 19)+"px"});
					}
					return false;
				}
			}
			return true;
		};
		
		function activateSuggestions(){
			$list.show();
			$input.unbind("keyup.suggestionInput");
			$input.bind("keyup.suggestionInput",onKeyUp);
			selectedListItem = 0;
			$list.css({"top" : listTopOffset -(selectedListItem * 19)+"px"});
		}
		
		function inactivateSuggestions(){
			$input.unbind("keydown.suggestionInput");
			$list.hide();
			hasValues = false;
		}
		
		function signalNewValue(){
			if($input.val() == ""){
				options.callback(0);
			}
			else if(selectedListItem == -1){
				$input.val($input[0].defaultValue);
				options.callback(-1);
			}else{
				options.callback($($list.children()[selectedListItem]).data("id"));
			}
		}
		
		function resetValue(){
			$input.val($input[0].defaultValue);
		}
		
		function ajaxCallback(data){
			$list.html("")
			if(data.status == 0){
				hasValues = data.data.length > 0;
				objects = [];
				for(key in data.data){
					objectdata = data.data[key];
					objects[objectdata.id] = data.data[key];
					var idString = options.hideId ? "" : "" + objectdata.id + ". ";
					var $listItem = $("<li>" + objectdata.label + " ("+ objectdata.id+")</li>");
					$listItem.data("type",objectdata.type);
					$listItem.data("label",idString + objectdata.label );
					$listItem.data("id",objectdata.id);
					$list.append($listItem);
				}
				selectedListItem = -1;
				if(data.responsecode == 2)
				$list.append("<li>Mer än " + options.maxNoOfResults +  " träffar </li>");
			}
			if(hasValues){
				activateSuggestions();
			}
		}

		var ajaxOptions = {
			serviceUrl : options.serviceUrl,
			maxNoOfResults : options.maxNoOfResults,
			callback : ajaxCallback,
			queryToShortCallback : function(){
				$list.html("");
			}
		}

		$input.ajaxSearcher(ajaxOptions);
		return $input;
	}
})(jQuery);