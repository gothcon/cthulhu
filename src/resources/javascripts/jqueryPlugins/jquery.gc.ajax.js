(function($){
	var saveMembershipUrl = "/admin/membership/new?format=json";



	$.gc = {
			ajax:{
				saveMembership : function(group_id,person_id,callback){
					console.log(group_id);
					$.ajax({
					  url: saveMembershipUrl,
					  dataType: 'json',
					  type : 'POST',
					  data: {"group_id" : group_id , "entityIDs" : [person_id]},
					  success: callback
					});
				},
				setUrls : function(urls){
					saveMembershipUrl = urls.saveMembershipUrl;
				}
			}
		};
})(jQuery);