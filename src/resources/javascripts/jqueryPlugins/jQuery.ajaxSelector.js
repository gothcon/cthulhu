(function($){

	var getCount = (function(){
		var _count = -1;
		return function(){
			_count +=1;
			return _count;
		};
	}());

	var defaultOptions = {
		callback : function(){},
		multiSelect : false,
		searchTerm : $.translate("search/search"),
		serviceUrl : "/ajax.php",
		maxNoOfResults:100,
		hideId : false,
		minimumQueryLength : 2,
		populateOnStart : true,
		hideSearchField: false
	};

	$.fn.ajaxSelector =  function(optionsParam){
		if(!optionsParam.serviceUrl){
			console.log("service url not set");
		}
		var options = $.extend( {} , defaultOptions, optionsParam );
		var $trigger = $(this);

		if($trigger.length === 0){
			return;
		}

		var id = "objectSelector_mainContainer_no" + getCount();
		var $container = $("<div id='"+ id +"' class='ajaxSelector_mainContainer'></div>");
		var $content = $("<div class='ajaxSelector_mainContent'></div>");
		
		function ajaxCallback(data){
			$list.html("");
			if(data.status === 0){
				objects = [];
				for(var key in data.data){
					var objectdata = data.data[key];
					objects[objectdata.id] = data.data[key];
					var idString = options.hideId ? "" : "" + objectdata.id + ". ";
					var inputType = options.multiSelect ? "checkbox" : "radio";
					var label = idString + objectdata.label + ((objectdata.type) ? " ("+objectdata.type + "s)" : "");
					var $listItem = $("<li><input type='"+inputType+"' name='ajaxSelector_objectid' value='" + objectdata.id + "'/>"+ label +"</li>").appendTo($list);
					$listItem.data("type",objectdata.type);
				}
				if(data.responsecode === 2){
					$list.append("<li>" + $.translate("search/moreThanXHits").replace("%S",options.maxNoOfResults) + "</li>");
				}
			}
		}
		
		
		if(!options.hideSearchField){
			var $searchField = $("<input type='text' class='ajaxSelector_searchField' value='"+options.searchTerm+"'/>");
			$searchField.bind("focus",function(){
				if($searchField.val() === options.searchTerm){
					$searchField.val("");
				}
			}).bind("blur",function(){
				if($searchField.val() === ""){
					$searchField.val(options.searchTerm);
				}
			});
			$content.append($searchField);
			
			var ajaxOptions = {
				minimumQueryLength : options.minimumQueryLength,
				serviceUrl : options.serviceUrl,
				maxNoOfResults : options.maxNoOfResults,
				callback : ajaxCallback,
				queryToShortCallback : function(){
					$list.html("");
				}
			};
			
			$searchField.ajaxSearcher(ajaxOptions);
		}
		
		var $list = $("<ul></ul>");
		var $okButton = $("<input class='ajaxSelector_okButton' type='button' value='ok'>");
		var $cancelButton = $("<input class='ajaxSelector_cancelButton' type='button' value='cancel'>");

		$okButton.click(function(){
			var selected = [];
			var selectorType = (options.multiSelect) ? "checkbox": "radio";

			$content.find("[type="+ selectorType +"]:checked").each(function(index,value){
				var val = $(value).val();
				selected[selected.length] = objects[val];
			});
			options.callback(selected,$trigger);
			$trigger.closeDOMWindow();
		});

		$cancelButton.click(function(){
			$trigger.closeDOMWindow();
		});

		$content.append($list).append($okButton).append($cancelButton);
		$container.append($content);
		$('body').append($container);

		var objects = [];

		$trigger.click( function(event){
			event.preventDefault();
			event.stopPropagation();
			$container.openDOMWindow({modal : true, width:350, height:450, windowSourceID : "#"+id });
		});
		
		if(options.populateOnStart){
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
				  success: ajaxCallback
			};
			var $call = $.ajax(argumentObject);
		}
		return $trigger;
	};
})(jQuery);