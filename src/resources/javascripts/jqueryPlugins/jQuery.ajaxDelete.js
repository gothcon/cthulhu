(function($){
	
	var defaultOptions = {returnDataType:"html", deleteQuestion :"Are U sure?"}	;

	$.fn.ajaxDelete = function(optionsParam){
		var $this = $(this);
		if($this.length < 1){
			return $this;
		}
		var options = $.extend({},defaultOptions,optionsParam);
		
		var processLink = function($link){
		
			var url = $link.attr("href");
			if(url.indexOf("format=json")=== -1){
				url += (url.indexOf("?") === -1 ? "?" : "&") + "format=json";
			}
			$link.removeProp("onclick");
			$link.bind("click",function(event){
				event.preventDefault();
				event.stopPropagation();
				if(confirm(options.deleteQuestion)){
					$.ajax({
						  url: url,
						  dataType: options.returnDataType,
						  success: options.deleteCallback,
						  type: "get"
					});
				}
				return false;
			});
		};
		
		function init(){
			
			$this.each(function(key,obj){
				var $obj = $(obj);
				if($obj[0].tagName === "A" || $obj[0].tagName === "a"){
					processLink($obj);
				}else{
					$obj.find("a").each(function(innerKey,innerObj){
						processLink($(innerObj));
					});
				}
			});
		}
		init();
	};
}(jQuery));