(function($){
	$.fn.autoSubmitOnEvent = function(eventName){
		eventName += ".autoSubmit";
		var $this = $(this);
		if($this.length > 0){
			$this.each(function(key,obj){
				var $obj = $(obj);
				var $submitButton = $obj.closest("form").find("input[type=submit]");
				$obj.unbind(eventName);
				$obj.bind(eventName,function(event){
					$submitButton.trigger("click");
				});
			});
		}
		return $this;
	};
}(jQuery));