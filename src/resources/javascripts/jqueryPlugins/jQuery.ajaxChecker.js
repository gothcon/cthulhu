(function($){

    var defaultOptions = {
        serviceUrl: '/ajax.php',
        searchDelay: 200,
        maxNoOfResult: 10,
        mininmumQueryLength : 0,
        callback: function(){},
    	queryToShortCallback : function(){}
    };


	$.fn.ajaxChecker = function(optionsParam){

		var $this = $(this);

		if($this.length === 0){
			return;
		}

		var options = $.extend({}, defaultOptions, optionsParam );

		function _init(){
			// gå igenom alla som matchar
			 $this.each(function(key,item){
			 	 var $checkBox = $(item);

			 	 $checkBox.bind("click",_doAjaxCall);
			 });
		}

		function _doAjaxCall(event){
			var $checkbox = $(event.target);
			var anmalning_id = $checkbox.val();
			var argumentObject = {
				  url: options.serviceUrl,
				  dataType: 'json',
  				  method : 'post',
				  data: {"anmalning_id" : anmalning_id},
				  success: (function(caller){
				  	return function(responseData){
						console.log("This is the default response handler of ajax checker");
						console.log(responseData);
					};
				  }($checkbox))
			};
			var $call = $.ajax(argumentObject);
		}

		_init();
	};
})(jQuery);