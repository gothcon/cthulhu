/*! GothCon - v1.0,0 - 2016-09-17
* Copyright (c) 2016 Joakim Ekeblad; Licensed  */
"undefined"!=typeof module&&"undefined"!=typeof exports&&module.exports===exports&&(module.exports="ui.router"),function(a,b,c){"use strict";function d(a,b){return M(new(M(function(){},{prototype:a})),b)}function e(a){return L(arguments,function(b){b!==a&&L(b,function(b,c){a.hasOwnProperty(c)||(a[c]=b)})}),a}function f(a,b){var c=[];for(var d in a.path){if(a.path[d]!==b.path[d])break;c.push(a.path[d])}return c}function g(a){if(Object.keys)return Object.keys(a);var c=[];return b.forEach(a,function(a,b){c.push(b)}),c}function h(a,b){if(Array.prototype.indexOf)return a.indexOf(b,Number(arguments[2])||0);var c=a.length>>>0,d=Number(arguments[2])||0;for(d=0>d?Math.ceil(d):Math.floor(d),0>d&&(d+=c);c>d;d++)if(d in a&&a[d]===b)return d;return-1}function i(a,b,c,d){var e,i=f(c,d),j={},k=[];for(var l in i)if(i[l].params&&(e=g(i[l].params),e.length))for(var m in e)h(k,e[m])>=0||(k.push(e[m]),j[e[m]]=a[e[m]]);return M({},j,b)}function j(a,b,c){if(!c){c=[];for(var d in a)c.push(d)}for(var e=0;e<c.length;e++){var f=c[e];if(a[f]!=b[f])return!1}return!0}function k(a,b){var c={};return L(a,function(a){c[a]=b[a]}),c}function l(a){var b={},c=Array.prototype.concat.apply(Array.prototype,Array.prototype.slice.call(arguments,1));for(var d in a)-1==h(c,d)&&(b[d]=a[d]);return b}function m(a,b){var c=K(a),d=c?[]:{};return L(a,function(a,e){b(a,e)&&(d[c?d.length:e]=a)}),d}function n(a,b){var c=K(a)?[]:{};return L(a,function(a,d){c[d]=b(a,d)}),c}function o(a,b){var d=1,f=2,i={},j=[],k=i,m=M(a.when(i),{$$promises:i,$$values:i});this.study=function(i){function n(a,c){if(s[c]!==f){if(r.push(c),s[c]===d)throw r.splice(0,h(r,c)),new Error("Cyclic dependency: "+r.join(" -> "));if(s[c]=d,I(a))q.push(c,[function(){return b.get(a)}],j);else{var e=b.annotate(a);L(e,function(a){a!==c&&i.hasOwnProperty(a)&&n(i[a],a)}),q.push(c,a,e)}r.pop(),s[c]=f}}function o(a){return J(a)&&a.then&&a.$$promises}if(!J(i))throw new Error("'invocables' must be an object");var p=g(i||{}),q=[],r=[],s={};return L(i,n),i=r=s=null,function(d,f,g){function h(){--u||(v||e(t,f.$$values),r.$$values=t,r.$$promises=r.$$promises||!0,delete r.$$inheritedValues,n.resolve(t))}function i(a){r.$$failure=a,n.reject(a)}function j(c,e,f){function j(a){l.reject(a),i(a)}function k(){if(!G(r.$$failure))try{l.resolve(b.invoke(e,g,t)),l.promise.then(function(a){t[c]=a,h()},j)}catch(a){j(a)}}var l=a.defer(),m=0;L(f,function(a){s.hasOwnProperty(a)&&!d.hasOwnProperty(a)&&(m++,s[a].then(function(b){t[a]=b,--m||k()},j))}),m||k(),s[c]=l.promise}if(o(d)&&g===c&&(g=f,f=d,d=null),d){if(!J(d))throw new Error("'locals' must be an object")}else d=k;if(f){if(!o(f))throw new Error("'parent' must be a promise returned by $resolve.resolve()")}else f=m;var n=a.defer(),r=n.promise,s=r.$$promises={},t=M({},d),u=1+q.length/3,v=!1;if(G(f.$$failure))return i(f.$$failure),r;f.$$inheritedValues&&e(t,l(f.$$inheritedValues,p)),M(s,f.$$promises),f.$$values?(v=e(t,l(f.$$values,p)),r.$$inheritedValues=l(f.$$values,p),h()):(f.$$inheritedValues&&(r.$$inheritedValues=l(f.$$inheritedValues,p)),f.then(h,i));for(var w=0,x=q.length;x>w;w+=3)d.hasOwnProperty(q[w])?h():j(q[w],q[w+1],q[w+2]);return r}},this.resolve=function(a,b,c,d){return this.study(a)(b,c,d)}}function p(a,b,c){this.fromConfig=function(a,b,c){return G(a.template)?this.fromString(a.template,b):G(a.templateUrl)?this.fromUrl(a.templateUrl,b):G(a.templateProvider)?this.fromProvider(a.templateProvider,b,c):null},this.fromString=function(a,b){return H(a)?a(b):a},this.fromUrl=function(c,d){return H(c)&&(c=c(d)),null==c?null:a.get(c,{cache:b,headers:{Accept:"text/html"}}).then(function(a){return a.data})},this.fromProvider=function(a,b,d){return c.invoke(a,null,d||{params:b})}}function q(a,b,e){function f(b,c,d,e){if(q.push(b),o[b])return o[b];if(!/^\w+(-+\w+)*(?:\[\])?$/.test(b))throw new Error("Invalid parameter name '"+b+"' in pattern '"+a+"'");if(p[b])throw new Error("Duplicate parameter name '"+b+"' in pattern '"+a+"'");return p[b]=new O.Param(b,c,d,e),p[b]}function g(a,b,c){var d=["",""],e=a.replace(/[\\\[\]\^$*+?.()|{}]/g,"\\$&");if(!b)return e;switch(c){case!1:d=["(",")"];break;case!0:d=["?(",")?"];break;default:d=["("+c+"|",")?"]}return e+d[0]+b+d[1]}function h(c,e){var f,g,h,i,j;return f=c[2]||c[3],j=b.params[f],h=a.substring(m,c.index),g=e?c[4]:c[4]||("*"==c[1]?".*":null),i=O.type(g||"string")||d(O.type("string"),{pattern:new RegExp(g)}),{id:f,regexp:g,segment:h,type:i,cfg:j}}b=M({params:{}},J(b)?b:{});var i,j=/([:*])([\w\[\]]+)|\{([\w\[\]]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,k=/([:]?)([\w\[\]-]+)|\{([\w\[\]-]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,l="^",m=0,n=this.segments=[],o=e?e.params:{},p=this.params=e?e.params.$$new():new O.ParamSet,q=[];this.source=a;for(var r,s,t;(i=j.exec(a))&&(r=h(i,!1),!(r.segment.indexOf("?")>=0));)s=f(r.id,r.type,r.cfg,"path"),l+=g(r.segment,s.type.pattern.source,s.squash),n.push(r.segment),m=j.lastIndex;t=a.substring(m);var u=t.indexOf("?");if(u>=0){var v=this.sourceSearch=t.substring(u);if(t=t.substring(0,u),this.sourcePath=a.substring(0,m+u),v.length>0)for(m=0;i=k.exec(v);)r=h(i,!0),s=f(r.id,r.type,r.cfg,"search"),m=j.lastIndex}else this.sourcePath=a,this.sourceSearch="";l+=g(t)+(b.strict===!1?"/?":"")+"$",n.push(t),this.regexp=new RegExp(l,b.caseInsensitive?"i":c),this.prefix=n[0],this.$$paramNames=q}function r(a){M(this,a)}function s(){function a(a){return null!=a?a.toString().replace(/\//g,"%2F"):a}function e(a){return null!=a?a.toString().replace(/%2F/g,"/"):a}function f(a){return this.pattern.test(a)}function i(){return{strict:t,caseInsensitive:p}}function j(a){return H(a)||K(a)&&H(a[a.length-1])}function k(){for(;x.length;){var a=x.shift();if(a.pattern)throw new Error("You cannot override a type's .pattern at runtime.");b.extend(v[a.name],o.invoke(a.def))}}function l(a){M(this,a||{})}O=this;var o,p=!1,t=!0,u=!1,v={},w=!0,x=[],y={string:{encode:a,decode:e,is:f,pattern:/[^/]*/},"int":{encode:a,decode:function(a){return parseInt(a,10)},is:function(a){return G(a)&&this.decode(a.toString())===a},pattern:/\d+/},bool:{encode:function(a){return a?1:0},decode:function(a){return 0!==parseInt(a,10)},is:function(a){return a===!0||a===!1},pattern:/0|1/},date:{encode:function(a){return this.is(a)?[a.getFullYear(),("0"+(a.getMonth()+1)).slice(-2),("0"+a.getDate()).slice(-2)].join("-"):c},decode:function(a){if(this.is(a))return a;var b=this.capture.exec(a);return b?new Date(b[1],b[2]-1,b[3]):c},is:function(a){return a instanceof Date&&!isNaN(a.valueOf())},equals:function(a,b){return this.is(a)&&this.is(b)&&a.toISOString()===b.toISOString()},pattern:/[0-9]{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2][0-9]|3[0-1])/,capture:/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/},json:{encode:b.toJson,decode:b.fromJson,is:b.isObject,equals:b.equals,pattern:/[^/]*/},any:{encode:b.identity,decode:b.identity,is:b.identity,equals:b.equals,pattern:/.*/}};s.$$getDefaultValue=function(a){if(!j(a.value))return a.value;if(!o)throw new Error("Injectable functions cannot be called at configuration time");return o.invoke(a.value)},this.caseInsensitive=function(a){return G(a)&&(p=a),p},this.strictMode=function(a){return G(a)&&(t=a),t},this.defaultSquashPolicy=function(a){if(!G(a))return u;if(a!==!0&&a!==!1&&!I(a))throw new Error("Invalid squash policy: "+a+". Valid policies: false, true, arbitrary-string");return u=a,a},this.compile=function(a,b){return new q(a,M(i(),b))},this.isMatcher=function(a){if(!J(a))return!1;var b=!0;return L(q.prototype,function(c,d){H(c)&&(b=b&&G(a[d])&&H(a[d]))}),b},this.type=function(a,b,c){if(!G(b))return v[a];if(v.hasOwnProperty(a))throw new Error("A type named '"+a+"' has already been defined.");return v[a]=new r(M({name:a},b)),c&&(x.push({name:a,def:c}),w||k()),this},L(y,function(a,b){v[b]=new r(M({name:b},a))}),v=d(v,{}),this.$get=["$injector",function(a){return o=a,w=!1,k(),L(y,function(a,b){v[b]||(v[b]=new r(a))}),this}],this.Param=function(a,b,d,e){function f(a){var b=J(a)?g(a):[],c=-1===h(b,"value")&&-1===h(b,"type")&&-1===h(b,"squash")&&-1===h(b,"array");return c&&(a={value:a}),a.$$fn=j(a.value)?a.value:function(){return a.value},a}function i(b,c,d){if(b.type&&c)throw new Error("Param '"+a+"' has two type configurations.");return c?c:b.type?b.type instanceof r?b.type:new r(b.type):"config"===d?v.any:v.string}function k(){var b={array:"search"===e?"auto":!1},c=a.match(/\[\]$/)?{array:!0}:{};return M(b,c,d).array}function l(a,b){var c=a.squash;if(!b||c===!1)return!1;if(!G(c)||null==c)return u;if(c===!0||I(c))return c;throw new Error("Invalid squash policy: '"+c+"'. Valid policies: false, true, or arbitrary string")}function p(a,b,d,e){var f,g,i=[{from:"",to:d||b?c:""},{from:null,to:d||b?c:""}];return f=K(a.replace)?a.replace:[],I(e)&&f.push({from:e,to:c}),g=n(f,function(a){return a.from}),m(i,function(a){return-1===h(g,a.from)}).concat(f)}function q(){if(!o)throw new Error("Injectable functions cannot be called at configuration time");return o.invoke(d.$$fn)}function s(a){function b(a){return function(b){return b.from===a}}function c(a){var c=n(m(w.replace,b(a)),function(a){return a.to});return c.length?c[0]:a}return a=c(a),G(a)?w.type.decode(a):q()}function t(){return"{Param:"+a+" "+b+" squash: '"+z+"' optional: "+y+"}"}var w=this;d=f(d),b=i(d,b,e);var x=k();b=x?b.$asArray(x,"search"===e):b,"string"!==b.name||x||"path"!==e||d.value!==c||(d.value="");var y=d.value!==c,z=l(d,y),A=p(d,x,y,z);M(this,{id:a,type:b,location:e,array:x,squash:z,replace:A,isOptional:y,value:s,dynamic:c,config:d,toString:t})},l.prototype={$$new:function(){return d(this,M(new l,{$$parent:this}))},$$keys:function(){for(var a=[],b=[],c=this,d=g(l.prototype);c;)b.push(c),c=c.$$parent;return b.reverse(),L(b,function(b){L(g(b),function(b){-1===h(a,b)&&-1===h(d,b)&&a.push(b)})}),a},$$values:function(a){var b={},c=this;return L(c.$$keys(),function(d){b[d]=c[d].value(a&&a[d])}),b},$$equals:function(a,b){var c=!0,d=this;return L(d.$$keys(),function(e){var f=a&&a[e],g=b&&b[e];d[e].type.equals(f,g)||(c=!1)}),c},$$validates:function(a){var b,c,d,e=!0,f=this;return L(this.$$keys(),function(g){d=f[g],c=a[g],b=!c&&d.isOptional,e=e&&(b||!!d.type.is(c))}),e},$$parent:c},this.ParamSet=l}function t(a,d){function e(a){var b=/^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(a.source);return null!=b?b[1].replace(/\\(.)/g,"$1"):""}function f(a,b){return a.replace(/\$(\$|\d{1,2})/,function(a,c){return b["$"===c?0:Number(c)]})}function g(a,b,c){if(!c)return!1;var d=a.invoke(b,b,{$match:c});return G(d)?d:!0}function h(d,e,f,g){function h(a,b,c){return"/"===p?a:b?p.slice(0,-1)+a:c?p.slice(1)+a:a}function m(a){function b(a){var b=a(f,d);return b?(I(b)&&d.replace().url(b),!0):!1}if(!a||!a.defaultPrevented){var e=o&&d.url()===o;if(o=c,e)return!0;var g,h=j.length;for(g=0;h>g;g++)if(b(j[g]))return;k&&b(k)}}function n(){return i=i||e.$on("$locationChangeSuccess",m)}var o,p=g.baseHref(),q=d.url();return l||n(),{sync:function(){m()},listen:function(){return n()},update:function(a){return a?void(q=d.url()):void(d.url()!==q&&(d.url(q),d.replace()))},push:function(a,b,e){d.url(a.format(b||{})),o=e&&e.$$avoidResync?d.url():c,e&&e.replace&&d.replace()},href:function(c,e,f){if(!c.validates(e))return null;var g=a.html5Mode();b.isObject(g)&&(g=g.enabled);var i=c.format(e);if(f=f||{},g||null===i||(i="#"+a.hashPrefix()+i),i=h(i,g,f.absolute),!f.absolute||!i)return i;var j=!g&&i?"/":"",k=d.port();return k=80===k||443===k?"":":"+k,[d.protocol(),"://",d.host(),k,j,i].join("")}}}var i,j=[],k=null,l=!1;this.rule=function(a){if(!H(a))throw new Error("'rule' must be a function");return j.push(a),this},this.otherwise=function(a){if(I(a)){var b=a;a=function(){return b}}else if(!H(a))throw new Error("'rule' must be a function");return k=a,this},this.when=function(a,b){var c,h=I(b);if(I(a)&&(a=d.compile(a)),!h&&!H(b)&&!K(b))throw new Error("invalid 'handler' in when()");var i={matcher:function(a,b){return h&&(c=d.compile(b),b=["$match",function(a){return c.format(a)}]),M(function(c,d){return g(c,b,a.exec(d.path(),d.search()))},{prefix:I(a.prefix)?a.prefix:""})},regex:function(a,b){if(a.global||a.sticky)throw new Error("when() RegExp must not be global or sticky");return h&&(c=b,b=["$match",function(a){return f(c,a)}]),M(function(c,d){return g(c,b,a.exec(d.path()))},{prefix:e(a)})}},j={matcher:d.isMatcher(a),regex:a instanceof RegExp};for(var k in j)if(j[k])return this.rule(i[k](a,b));throw new Error("invalid 'what' in when()")},this.deferIntercept=function(a){a===c&&(a=!0),l=a},this.$get=h,h.$inject=["$location","$rootScope","$injector","$browser"]}function u(a,e){function f(a){return 0===a.indexOf(".")||0===a.indexOf("^")}function l(a,b){if(!a)return c;var d=I(a),e=d?a:a.name,g=f(e);if(g){if(!b)throw new Error("No reference point given for path '"+e+"'");b=l(b);for(var h=e.split("."),i=0,j=h.length,k=b;j>i;i++)if(""!==h[i]||0!==i){if("^"!==h[i])break;if(!k.parent)throw new Error("Path '"+e+"' not valid for state '"+b.name+"'");k=k.parent}else k=b;h=h.slice(i).join("."),e=k.name+(k.name&&h?".":"")+h}var m=y[e];return!m||!d&&(d||m!==a&&m.self!==a)?c:m}function m(a,b){z[a]||(z[a]=[]),z[a].push(b)}function o(a){for(var b=z[a]||[];b.length;)p(b.shift())}function p(b){b=d(b,{self:b,resolve:b.resolve||{},toString:function(){return this.name}});var c=b.name;if(!I(c)||c.indexOf("@")>=0)throw new Error("State must have a valid name");if(y.hasOwnProperty(c))throw new Error("State '"+c+"'' is already defined");var e=-1!==c.indexOf(".")?c.substring(0,c.lastIndexOf(".")):I(b.parent)?b.parent:J(b.parent)&&I(b.parent.name)?b.parent.name:"";if(e&&!y[e])return m(e,b.self);for(var f in B)H(B[f])&&(b[f]=B[f](b,B.$delegates[f]));return y[c]=b,!b[A]&&b.url&&a.when(b.url,["$match","$stateParams",function(a,c){x.$current.navigable==b&&j(a,c)||x.transitionTo(b,a,{inherit:!0,location:!1})}]),o(c),b}function q(a){return a.indexOf("*")>-1}function r(a){var b=a.split("."),c=x.$current.name.split(".");if("**"===b[0]&&(c=c.slice(h(c,b[1])),c.unshift("**")),"**"===b[b.length-1]&&(c.splice(h(c,b[b.length-2])+1,Number.MAX_VALUE),c.push("**")),b.length!=c.length)return!1;for(var d=0,e=b.length;e>d;d++)"*"===b[d]&&(c[d]="*");return c.join("")===b.join("")}function s(a,b){return I(a)&&!G(b)?B[a]:H(b)&&I(a)?(B[a]&&!B.$delegates[a]&&(B.$delegates[a]=B[a]),B[a]=b,this):this}function t(a,b){return J(a)?b=a:b.name=a,p(b),this}function u(a,e,f,h,m,o,p){function s(b,c,d,f){var g=a.$broadcast("$stateNotFound",b,c,d);if(g.defaultPrevented)return p.update(),B;if(!g.retry)return null;if(f.$retry)return p.update(),C;var h=x.transition=e.when(g.retry);return h.then(function(){return h!==x.transition?u:(b.options.$retry=!0,x.transitionTo(b.to,b.toParams,b.options))},function(){return B}),p.update(),h}function t(a,c,d,g,i,j){var l=d?c:k(a.params.$$keys(),c),n={$stateParams:l};i.resolve=m.resolve(a.resolve,n,i.resolve,a);var o=[i.resolve.then(function(a){i.globals=a})];return g&&o.push(g),L(a.views,function(c,d){var e=c.resolve&&c.resolve!==a.resolve?c.resolve:{};e.$template=[function(){return f.load(d,{view:c,locals:n,params:l,notify:j.notify})||""}],o.push(m.resolve(e,n,i.resolve,a).then(function(f){if(H(c.controllerProvider)||K(c.controllerProvider)){var g=b.extend({},e,n);f.$$controller=h.invoke(c.controllerProvider,null,g)}else f.$$controller=c.controller;f.$$state=a,f.$$controllerAs=c.controllerAs,i[d]=f}))}),e.all(o).then(function(){return i})}var u=e.reject(new Error("transition superseded")),z=e.reject(new Error("transition prevented")),B=e.reject(new Error("transition aborted")),C=e.reject(new Error("transition failed"));return w.locals={resolve:null,globals:{$stateParams:{}}},x={params:{},current:w.self,$current:w,transition:null},x.reload=function(){return x.transitionTo(x.current,o,{reload:!0,inherit:!1,notify:!0})},x.go=function(a,b,c){return x.transitionTo(a,b,M({inherit:!0,relative:x.$current},c))},x.transitionTo=function(b,c,f){c=c||{},f=M({location:!0,inherit:!1,relative:null,notify:!0,reload:!1,$retry:!1},f||{});var g,j=x.$current,m=x.params,n=j.path,q=l(b,f.relative);if(!G(q)){var r={to:b,toParams:c,options:f},y=s(r,j.self,m,f);if(y)return y;if(b=r.to,c=r.toParams,f=r.options,q=l(b,f.relative),!G(q)){if(!f.relative)throw new Error("No such state '"+b+"'");throw new Error("Could not resolve '"+b+"' from state '"+f.relative+"'")}}if(q[A])throw new Error("Cannot transition to abstract state '"+b+"'");if(f.inherit&&(c=i(o,c||{},x.$current,q)),!q.params.$$validates(c))return C;c=q.params.$$values(c),b=q;var B=b.path,D=0,E=B[D],F=w.locals,H=[];if(!f.reload)for(;E&&E===n[D]&&E.ownParams.$$equals(c,m);)F=H[D]=E.locals,D++,E=B[D];if(v(b,j,F,f))return b.self.reloadOnSearch!==!1&&p.update(),x.transition=null,e.when(x.current);if(c=k(b.params.$$keys(),c||{}),f.notify&&a.$broadcast("$stateChangeStart",b.self,c,j.self,m).defaultPrevented)return p.update(),z;for(var I=e.when(F),J=D;J<B.length;J++,E=B[J])F=H[J]=d(F),I=t(E,c,E===b,I,F,f);var K=x.transition=I.then(function(){var d,e,g;if(x.transition!==K)return u;for(d=n.length-1;d>=D;d--)g=n[d],g.self.onExit&&h.invoke(g.self.onExit,g.self,g.locals.globals),g.locals=null;for(d=D;d<B.length;d++)e=B[d],e.locals=H[d],e.self.onEnter&&h.invoke(e.self.onEnter,e.self,e.locals.globals);return x.transition!==K?u:(x.$current=b,x.current=b.self,x.params=c,N(x.params,o),x.transition=null,f.location&&b.navigable&&p.push(b.navigable.url,b.navigable.locals.globals.$stateParams,{$$avoidResync:!0,replace:"replace"===f.location}),f.notify&&a.$broadcast("$stateChangeSuccess",b.self,c,j.self,m),p.update(!0),x.current)},function(d){return x.transition!==K?u:(x.transition=null,g=a.$broadcast("$stateChangeError",b.self,c,j.self,m,d),g.defaultPrevented||p.update(),e.reject(d))});return K},x.is=function(a,b,d){d=M({relative:x.$current},d||{});var e=l(a,d.relative);return G(e)?x.$current!==e?!1:b?j(e.params.$$values(b),o):!0:c},x.includes=function(a,b,d){if(d=M({relative:x.$current},d||{}),I(a)&&q(a)){if(!r(a))return!1;a=x.$current.name}var e=l(a,d.relative);return G(e)?G(x.$current.includes[e.name])?b?j(e.params.$$values(b),o,g(b)):!0:!1:c},x.href=function(a,b,d){d=M({lossy:!0,inherit:!0,absolute:!1,relative:x.$current},d||{});var e=l(a,d.relative);if(!G(e))return null;d.inherit&&(b=i(o,b||{},x.$current,e));var f=e&&d.lossy?e.navigable:e;return f&&f.url!==c&&null!==f.url?p.href(f.url,k(e.params.$$keys(),b||{}),{absolute:d.absolute}):null},x.get=function(a,b){if(0===arguments.length)return n(g(y),function(a){return y[a].self});var c=l(a,b||x.$current);return c&&c.self?c.self:null},x}function v(a,b,c,d){return a!==b||(c!==b.locals||d.reload)&&a.self.reloadOnSearch!==!1?void 0:!0}var w,x,y={},z={},A="abstract",B={parent:function(a){if(G(a.parent)&&a.parent)return l(a.parent);var b=/^(.+)\.[^.]+$/.exec(a.name);return b?l(b[1]):w},data:function(a){return a.parent&&a.parent.data&&(a.data=a.self.data=M({},a.parent.data,a.data)),a.data},url:function(a){var b=a.url,c={params:a.params||{}};if(I(b))return"^"==b.charAt(0)?e.compile(b.substring(1),c):(a.parent.navigable||w).url.concat(b,c);if(!b||e.isMatcher(b))return b;throw new Error("Invalid url '"+b+"' in state '"+a+"'")},navigable:function(a){return a.url?a:a.parent?a.parent.navigable:null},ownParams:function(a){var b=a.url&&a.url.params||new O.ParamSet;return L(a.params||{},function(a,c){b[c]||(b[c]=new O.Param(c,null,a,"config"))}),b},params:function(a){return a.parent&&a.parent.params?M(a.parent.params.$$new(),a.ownParams):new O.ParamSet},views:function(a){var b={};return L(G(a.views)?a.views:{"":a},function(c,d){d.indexOf("@")<0&&(d+="@"+a.parent.name),b[d]=c}),b},path:function(a){return a.parent?a.parent.path.concat(a):[]},includes:function(a){var b=a.parent?M({},a.parent.includes):{};return b[a.name]=!0,b},$delegates:{}};w=p({name:"",url:"^",views:null,"abstract":!0}),w.navigable=null,this.decorator=s,this.state=t,this.$get=u,u.$inject=["$rootScope","$q","$view","$injector","$resolve","$stateParams","$urlRouter","$location","$urlMatcherFactory"]}function v(){function a(a,b){return{load:function(c,d){var e,f={template:null,controller:null,view:null,locals:null,notify:!0,async:!0,params:{}};return d=M(f,d),d.view&&(e=b.fromConfig(d.view,d.params,d.locals)),e&&d.notify&&a.$broadcast("$viewContentLoading",d),e}}}this.$get=a,a.$inject=["$rootScope","$templateFactory"]}function w(){var a=!1;this.useAnchorScroll=function(){a=!0},this.$get=["$anchorScroll","$timeout",function(b,c){return a?b:function(a){c(function(){a[0].scrollIntoView()},0,!1)}}]}function x(a,c,d,e){function f(){return c.has?function(a){return c.has(a)?c.get(a):null}:function(a){try{return c.get(a)}catch(b){return null}}}function g(a,b){var c=function(){return{enter:function(a,b,c){b.after(a),c()},leave:function(a,b){a.remove(),b()}}};if(j)return{enter:function(a,b,c){var d=j.enter(a,null,b,c);d&&d.then&&d.then(c)},leave:function(a,b){var c=j.leave(a,b);c&&c.then&&c.then(b)}};if(i){var d=i&&i(b,a);return{enter:function(a,b,c){d.enter(a,null,b),c()},leave:function(a,b){d.leave(a),b()}}}return c()}var h=f(),i=h("$animator"),j=h("$animate"),k={restrict:"ECA",terminal:!0,priority:400,transclude:"element",compile:function(c,f,h){return function(c,f,i){function j(){l&&(l.remove(),l=null),n&&(n.$destroy(),n=null),m&&(r.leave(m,function(){l=null}),l=m,m=null)}function k(g){var k,l=z(c,i,f,e),s=l&&a.$current&&a.$current.locals[l];if(g||s!==o){k=c.$new(),o=a.$current.locals[l];var t=h(k,function(a){r.enter(a,f,function(){n&&n.$emit("$viewContentAnimationEnded"),(b.isDefined(q)&&!q||c.$eval(q))&&d(a)}),j()});m=t,n=k,n.$emit("$viewContentLoaded"),n.$eval(p)}}var l,m,n,o,p=i.onload||"",q=i.autoscroll,r=g(i,c);c.$on("$stateChangeSuccess",function(){k(!1)}),c.$on("$viewContentLoading",function(){k(!1)}),k(!0)}}};return k}function y(a,b,c,d){return{restrict:"ECA",priority:-400,compile:function(e){var f=e.html();return function(e,g,h){var i=c.$current,j=z(e,h,g,d),k=i&&i.locals[j];if(k){g.data("$uiView",{name:j,state:k.$$state}),g.html(k.$template?k.$template:f);var l=a(g.contents());if(k.$$controller){k.$scope=e;var m=b(k.$$controller,k);k.$$controllerAs&&(e[k.$$controllerAs]=m),g.data("$ngControllerController",m),g.children().data("$ngControllerController",m)}l(e)}}}}}function z(a,b,c,d){var e=d(b.uiView||b.name||"")(a),f=c.inheritedData("$uiView");return e.indexOf("@")>=0?e:e+"@"+(f?f.state.name:"")}function A(a,b){var c,d=a.match(/^\s*({[^}]*})\s*$/);if(d&&(a=b+"("+d[1]+")"),c=a.replace(/\n/g," ").match(/^([^(]+?)\s*(\((.*)\))?$/),!c||4!==c.length)throw new Error("Invalid state ref '"+a+"'");return{state:c[1],paramExpr:c[3]||null}}function B(a){var b=a.parent().inheritedData("$uiView");return b&&b.state&&b.state.name?b.state:void 0}function C(a,c){var d=["location","inherit","reload"];return{restrict:"A",require:["?^uiSrefActive","?^uiSrefActiveEq"],link:function(e,f,g,h){var i=A(g.uiSref,a.current.name),j=null,k=B(f)||a.$current,l=null,m="A"===f.prop("tagName"),n="FORM"===f[0].nodeName,o=n?"action":"href",p=!0,q={relative:k,inherit:!0},r=e.$eval(g.uiSrefOpts)||{};b.forEach(d,function(a){a in r&&(q[a]=r[a])});var s=function(c){if(c&&(j=b.copy(c)),p){l=a.href(i.state,j,q);var d=h[1]||h[0];return d&&d.$$setStateInfo(i.state,j),null===l?(p=!1,!1):void g.$set(o,l)}};i.paramExpr&&(e.$watch(i.paramExpr,function(a){a!==j&&s(a)},!0),j=b.copy(e.$eval(i.paramExpr))),s(),n||f.bind("click",function(b){var d=b.which||b.button;if(!(d>1||b.ctrlKey||b.metaKey||b.shiftKey||f.attr("target"))){var e=c(function(){a.go(i.state,j,q)});b.preventDefault();var g=m&&!l?1:0;b.preventDefault=function(){g--<=0&&c.cancel(e)}}})}}}function D(a,b,c){return{restrict:"A",controller:["$scope","$element","$attrs",function(b,d,e){function f(){g()?d.addClass(j):d.removeClass(j)}function g(){return"undefined"!=typeof e.uiSrefActiveEq?h&&a.is(h.name,i):h&&a.includes(h.name,i)}var h,i,j;j=c(e.uiSrefActiveEq||e.uiSrefActive||"",!1)(b),this.$$setStateInfo=function(b,c){h=a.get(b,B(d)),i=c,f()},b.$on("$stateChangeSuccess",f)}]}}function E(a){var b=function(b){return a.is(b)};return b.$stateful=!0,b}function F(a){var b=function(b){return a.includes(b)};return b.$stateful=!0,b}var G=b.isDefined,H=b.isFunction,I=b.isString,J=b.isObject,K=b.isArray,L=b.forEach,M=b.extend,N=b.copy;b.module("ui.router.util",["ng"]),b.module("ui.router.router",["ui.router.util"]),b.module("ui.router.state",["ui.router.router","ui.router.util"]),b.module("ui.router",["ui.router.state"]),b.module("ui.router.compat",["ui.router"]),o.$inject=["$q","$injector"],b.module("ui.router.util").service("$resolve",o),p.$inject=["$http","$templateCache","$injector"],b.module("ui.router.util").service("$templateFactory",p);var O;q.prototype.concat=function(a,b){var c={caseInsensitive:O.caseInsensitive(),strict:O.strictMode(),squash:O.defaultSquashPolicy()};return new q(this.sourcePath+a+this.sourceSearch,M(c,b),this)},q.prototype.toString=function(){return this.source},q.prototype.exec=function(a,b){function c(a){function b(a){return a.split("").reverse().join("")}function c(a){return a.replace(/\\-/,"-")}var d=b(a).split(/-(?!\\)/),e=n(d,b);return n(e,c).reverse()}var d=this.regexp.exec(a);if(!d)return null;b=b||{};var e,f,g,h=this.parameters(),i=h.length,j=this.segments.length-1,k={};if(j!==d.length-1)throw new Error("Unbalanced capture group in route '"+this.source+"'");for(e=0;j>e;e++){g=h[e];var l=this.params[g],m=d[e+1];for(f=0;f<l.replace;f++)l.replace[f].from===m&&(m=l.replace[f].to);m&&l.array===!0&&(m=c(m)),k[g]=l.value(m)}for(;i>e;e++)g=h[e],k[g]=this.params[g].value(b[g]);return k},q.prototype.parameters=function(a){return G(a)?this.params[a]||null:this.$$paramNames},q.prototype.validates=function(a){return this.params.$$validates(a)},q.prototype.format=function(a){function b(a){return encodeURIComponent(a).replace(/-/g,function(a){return"%5C%"+a.charCodeAt(0).toString(16).toUpperCase()})}a=a||{};var c=this.segments,d=this.parameters(),e=this.params;if(!this.validates(a))return null;var f,g=!1,h=c.length-1,i=d.length,j=c[0];for(f=0;i>f;f++){var k=h>f,l=d[f],m=e[l],o=m.value(a[l]),p=m.isOptional&&m.type.equals(m.value(),o),q=p?m.squash:!1,r=m.type.encode(o);if(k){var s=c[f+1];if(q===!1)null!=r&&(j+=K(r)?n(r,b).join("-"):encodeURIComponent(r)),j+=s;else if(q===!0){var t=j.match(/\/$/)?/\/?(.*)/:/(.*)/;j+=s.match(t)[1]}else I(q)&&(j+=q+s)}else{if(null==r||p&&q!==!1)continue;K(r)||(r=[r]),r=n(r,encodeURIComponent).join("&"+l+"="),j+=(g?"&":"?")+(l+"="+r),g=!0}}return j},r.prototype.is=function(){return!0},r.prototype.encode=function(a){return a},r.prototype.decode=function(a){return a},r.prototype.equals=function(a,b){return a==b},r.prototype.$subPattern=function(){var a=this.pattern.toString();return a.substr(1,a.length-2)},r.prototype.pattern=/.*/,r.prototype.toString=function(){return"{Type:"+this.name+"}"},r.prototype.$asArray=function(a,b){function d(a,b){function d(a,b){return function(){return a[b].apply(a,arguments)}}function e(a){return K(a)?a:G(a)?[a]:[]}function f(a){switch(a.length){case 0:return c;case 1:return"auto"===b?a[0]:a;default:return a}}function g(a){return!a}function h(a,b){return function(c){c=e(c);var d=n(c,a);return b===!0?0===m(d,g).length:f(d)}}function i(a){return function(b,c){var d=e(b),f=e(c);if(d.length!==f.length)return!1;for(var g=0;g<d.length;g++)if(!a(d[g],f[g]))return!1;return!0}}this.encode=h(d(a,"encode")),this.decode=h(d(a,"decode")),this.is=h(d(a,"is"),!0),this.equals=i(d(a,"equals")),this.pattern=a.pattern,this.$arrayMode=b}if(!a)return this;if("auto"===a&&!b)throw new Error("'auto' array mode is for query parameters only");return new d(this,a)},b.module("ui.router.util").provider("$urlMatcherFactory",s),b.module("ui.router.util").run(["$urlMatcherFactory",function(){}]),t.$inject=["$locationProvider","$urlMatcherFactoryProvider"],b.module("ui.router.router").provider("$urlRouter",t),u.$inject=["$urlRouterProvider","$urlMatcherFactoryProvider"],b.module("ui.router.state").value("$stateParams",{}).provider("$state",u),v.$inject=[],b.module("ui.router.state").provider("$view",v),b.module("ui.router.state").provider("$uiViewScroll",w),x.$inject=["$state","$injector","$uiViewScroll","$interpolate"],y.$inject=["$compile","$controller","$state","$interpolate"],b.module("ui.router.state").directive("uiView",x),b.module("ui.router.state").directive("uiView",y),C.$inject=["$state","$timeout"],D.$inject=["$state","$stateParams","$interpolate"],b.module("ui.router.state").directive("uiSref",C).directive("uiSrefActive",D).directive("uiSrefActiveEq",D),E.$inject=["$state"],F.$inject=["$state"],b.module("ui.router.state").filter("isState",E).filter("includedByState",F)}(window,window.angular);
/*jslint vars:true */

/**
 * @license angular-bootstrap-datetimepicker  version: 0.3.12
 * (c) 2013-2014 Knight Rider Consulting, Inc. http://www.knightrider.com
 * License: MIT
 */

/**
 *
 *    @author        Dale "Ducky" Lotts
 *    @since        2013-Jul-8
 */

(function (factory) {
  'use strict';
  /* istanbul ignore if */
  if (typeof define === 'function' && /* istanbul ignore next */ define.amd) {
    define(['angular', 'moment'], factory); // AMD
    /* istanbul ignore next */
  } else if (typeof exports === 'object') {
    module.exports = factory(require('angular'), require('moment')); // CommonJS
  } else {
    factory(window.angular, window.moment); // Browser global
  }
}(function (angular, moment) {
  'use strict';
  angular.module('ui.bootstrap.datetimepicker', [])
    .constant('dateTimePickerConfig', {
      dropdownSelector: null,
      minuteStep: 5,
      minView: 'minute',
      startView: 'day'
    })
    .directive('datetimepicker', ['$log', 'dateTimePickerConfig', function datetimepickerDirective($log, defaultConfig) {

      function DateObject() {

        var tempDate = new Date();
        var localOffset = tempDate.getTimezoneOffset() * 60000;
        this.utcDateValue = tempDate.getTime();
        this.selectable = true;

        this.localDateValue = function () {
          return this.utcDateValue + localOffset;
        };

        var validProperties = ['utcDateValue', 'localDateValue', 'display', 'active', 'selectable', 'past', 'future'];

        for (var prop in arguments[0]) {
          /* istanbul ignore else */
          //noinspection JSUnfilteredForInLoop
          if (validProperties.indexOf(prop) >= 0) {
            //noinspection JSUnfilteredForInLoop
            this[prop] = arguments[0][prop];
          }
        }
      }

      var validateConfiguration = function validateConfiguration(configuration) {
        var validOptions = ['startView', 'minView', 'minuteStep', 'dropdownSelector'];

        for (var prop in configuration) {
          //noinspection JSUnfilteredForInLoop
          if (validOptions.indexOf(prop) < 0) {
            throw ('invalid option: ' + prop);
          }
        }

        // Order of the elements in the validViews array is significant.
        var validViews = ['minute', 'hour', 'day', 'month', 'year'];

        if (validViews.indexOf(configuration.startView) < 0) {
          throw ('invalid startView value: ' + configuration.startView);
        }

        if (validViews.indexOf(configuration.minView) < 0) {
          throw ('invalid minView value: ' + configuration.minView);
        }

        if (validViews.indexOf(configuration.minView) > validViews.indexOf(configuration.startView)) {
          throw ('startView must be greater than minView');
        }

        if (!angular.isNumber(configuration.minuteStep)) {
          throw ('minuteStep must be numeric');
        }
        if (configuration.minuteStep <= 0 || configuration.minuteStep >= 60) {
          throw ('minuteStep must be greater than zero and less than 60');
        }
        if (configuration.dropdownSelector !== null && !angular.isString(configuration.dropdownSelector)) {
          throw ('dropdownSelector must be a string');
        }

        /* istanbul ignore next */
        if (configuration.dropdownSelector !== null && ((typeof jQuery === 'undefined') || (typeof jQuery().dropdown !== 'function'))) {
          $log.error('Please DO NOT specify the dropdownSelector option unless you are using jQuery AND Bootstrap.js. ' +
          'Please include jQuery AND Bootstrap.js, or write code to close the dropdown in the on-set-time callback. \n\n' +
          'The dropdownSelector configuration option is being removed because it will not function properly.');
          delete configuration.dropdownSelector;
        }
      };

      return {
        restrict: 'E',
        require: 'ngModel',
        template: '<div class="datetimepicker table-responsive">' +
        '<table class="table table-striped  {{ data.currentView }}-view">' +
        '   <thead>' +
        '       <tr>' +
        '           <th class="left" data-ng-click="changeView(data.currentView, data.leftDate, $event)" data-ng-show="data.leftDate.selectable"><i class="glyphicon glyphicon-arrow-left"/></th>' +
        '           <th class="switch" colspan="5" data-ng-show="data.previousViewDate.selectable" data-ng-click="changeView(data.previousView, data.previousViewDate, $event)">{{ data.previousViewDate.display }}</th>' +
        '           <th class="right" data-ng-click="changeView(data.currentView, data.rightDate, $event)" data-ng-show="data.rightDate.selectable"><i class="glyphicon glyphicon-arrow-right"/></th>' +
        '       </tr>' +
        '       <tr>' +
        '           <th class="dow" data-ng-repeat="day in data.dayNames" >{{ day }}</th>' +
        '       </tr>' +
        '   </thead>' +
        '   <tbody>' +
        '       <tr data-ng-if="data.currentView !== \'day\'" >' +
        '           <td colspan="7" >' +
        '              <span    class="{{ data.currentView }}" ' +
        '                       data-ng-repeat="dateObject in data.dates"  ' +
        '                       data-ng-class="{active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable}" ' +
        '                       data-ng-click="changeView(data.nextView, dateObject, $event)">{{ dateObject.display }}</span> ' +
        '           </td>' +
        '       </tr>' +
        '       <tr data-ng-if="data.currentView === \'day\'" data-ng-repeat="week in data.weeks">' +
        '           <td data-ng-repeat="dateObject in week.dates" ' +
        '               data-ng-click="changeView(data.nextView, dateObject, $event)"' +
        '               class="day" ' +
        '               data-ng-class="{active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable}" >{{ dateObject.display }}</td>' +
        '       </tr>' +
        '   </tbody>' +
        '</table></div>',
        scope: {
          onSetTime: '&',
          beforeRender: '&'
        },
        replace: true,
        link: function link(scope, element, attrs, ngModelController) {

          var directiveConfig = {};

          if (attrs.datetimepickerConfig) {
            directiveConfig = scope.$parent.$eval(attrs.datetimepickerConfig);
          }

          var configuration = {};

          angular.extend(configuration, defaultConfig, directiveConfig);

          validateConfiguration(configuration);

          var startOfDecade = function startOfDecade(unixDate) {
            var startYear = (parseInt(moment.utc(unixDate).year() / 10, 10) * 10);
            return moment.utc(unixDate).year(startYear).startOf('year');
          };

          var dataFactory = {
            year: function year(unixDate) {
              var selectedDate = moment.utc(unixDate).startOf('year');
              // View starts one year before the decade starts and ends one year after the decade ends
              // i.e. passing in a date of 1/1/2013 will give a range of 2009 to 2020
              // Truncate the last digit from the current year and subtract 1 to get the start of the decade
              var startDecade = (parseInt(selectedDate.year() / 10, 10) * 10);
              var startDate = moment.utc(startOfDecade(unixDate)).subtract(1, 'year').startOf('year');

              var activeYear = ngModelController.$modelValue ? moment(ngModelController.$modelValue).year() : 0;

              var result = {
                'currentView': 'year',
                'nextView': configuration.minView === 'year' ? 'setTime' : 'month',
                'previousViewDate': new DateObject({
                  utcDateValue: null,
                  display: startDecade + '-' + (startDecade + 9)
                }),
                'leftDate': new DateObject({utcDateValue: moment.utc(startDate).subtract(9, 'year').valueOf()}),
                'rightDate': new DateObject({utcDateValue: moment.utc(startDate).add(11, 'year').valueOf()}),
                'dates': []
              };

              for (var i = 0; i < 12; i += 1) {
                var yearMoment = moment.utc(startDate).add(i, 'years');
                var dateValue = {
                  'utcDateValue': yearMoment.valueOf(),
                  'display': yearMoment.format('YYYY'),
                  'past': yearMoment.year() < startDecade,
                  'future': yearMoment.year() > startDecade + 9,
                  'active': yearMoment.year() === activeYear
                };

                result.dates.push(new DateObject(dateValue));
              }

              return result;
            },

            month: function month(unixDate) {

              var startDate = moment.utc(unixDate).startOf('year');
              var previousViewDate = startOfDecade(unixDate);
              var activeDate = ngModelController.$modelValue ? moment(ngModelController.$modelValue).format('YYYY-MMM') : 0;

              var result = {
                'previousView': 'year',
                'currentView': 'month',
                'nextView': configuration.minView === 'month' ? 'setTime' : 'day',
                'previousViewDate': new DateObject({
                  utcDateValue: previousViewDate.valueOf(),
                  display: startDate.format('YYYY')
                }),
                'leftDate': new DateObject({utcDateValue: moment.utc(startDate).subtract(1, 'year').valueOf()}),
                'rightDate': new DateObject({utcDateValue: moment.utc(startDate).add(1, 'year').valueOf()}),
                'dates': []
              };

              for (var i = 0; i < 12; i += 1) {
                var monthMoment = moment.utc(startDate).add(i, 'months');
                var dateValue = {
                  'utcDateValue': monthMoment.valueOf(),
                  'display': monthMoment.format('MMM'),
                  'active': monthMoment.format('YYYY-MMM') === activeDate
                };

                result.dates.push(new DateObject(dateValue));
              }

              return result;
            },

            day: function day(unixDate) {

              var selectedDate = moment.utc(unixDate);
              var startOfMonth = moment.utc(selectedDate).startOf('month');
              var previousViewDate = moment.utc(selectedDate).startOf('year');
              var endOfMonth = moment.utc(selectedDate).endOf('month');

              var startDate = moment.utc(startOfMonth).subtract(Math.abs(startOfMonth.weekday()), 'days');

              var activeDate = ngModelController.$modelValue ? moment(ngModelController.$modelValue).format('YYYY-MMM-DD') : '';

              var result = {
                'previousView': 'month',
                'currentView': 'day',
                'nextView': configuration.minView === 'day' ? 'setTime' : 'hour',
                'previousViewDate': new DateObject({
                  utcDateValue: previousViewDate.valueOf(),
                  display: startOfMonth.format('YYYY-MMM')
                }),
                'leftDate': new DateObject({utcDateValue: moment.utc(startOfMonth).subtract(1, 'months').valueOf()}),
                'rightDate': new DateObject({utcDateValue: moment.utc(startOfMonth).add(1, 'months').valueOf()}),
                'dayNames': [],
                'weeks': []
              };


              for (var dayNumber = 0; dayNumber < 7; dayNumber += 1) {
                result.dayNames.push(moment.utc().weekday(dayNumber).format('dd'));
              }

              for (var i = 0; i < 6; i += 1) {
                var week = {dates: []};
                for (var j = 0; j < 7; j += 1) {
                  var monthMoment = moment.utc(startDate).add((i * 7) + j, 'days');
                  var dateValue = {
                    'utcDateValue': monthMoment.valueOf(),
                    'display': monthMoment.format('D'),
                    'active': monthMoment.format('YYYY-MMM-DD') === activeDate,
                    'past': monthMoment.isBefore(startOfMonth),
                    'future': monthMoment.isAfter(endOfMonth)
                  };
                  week.dates.push(new DateObject(dateValue));
                }
                result.weeks.push(week);
              }

              return result;
            },

            hour: function hour(unixDate) {
              var selectedDate = moment.utc(unixDate).startOf('day');
              var previousViewDate = moment.utc(selectedDate).startOf('month');

              var activeFormat = ngModelController.$modelValue ? moment(ngModelController.$modelValue).format('YYYY-MM-DD H') : '';

              var result = {
                'previousView': 'day',
                'currentView': 'hour',
                'nextView': configuration.minView === 'hour' ? 'setTime' : 'minute',
                'previousViewDate': new DateObject({
                  utcDateValue: previousViewDate.valueOf(),
                  display: selectedDate.format('ll')
                }),
                'leftDate': new DateObject({utcDateValue: moment.utc(selectedDate).subtract(1, 'days').valueOf()}),
                'rightDate': new DateObject({utcDateValue: moment.utc(selectedDate).add(1, 'days').valueOf()}),
                'dates': []
              };

              for (var i = 0; i < 24; i += 1) {
                var hourMoment = moment.utc(selectedDate).add(i, 'hours');
                var dateValue = {
                  'utcDateValue': hourMoment.valueOf(),
                  'display': hourMoment.format('LT'),
                  'active': hourMoment.format('YYYY-MM-DD H') === activeFormat
                };

                result.dates.push(new DateObject(dateValue));
              }

              return result;
            },

            minute: function minute(unixDate) {
              var selectedDate = moment.utc(unixDate).startOf('hour');
              var previousViewDate = moment.utc(selectedDate).startOf('day');
              var activeFormat = ngModelController.$modelValue ? moment(ngModelController.$modelValue).format('YYYY-MM-DD H:mm') : '';

              var result = {
                'previousView': 'hour',
                'currentView': 'minute',
                'nextView': 'setTime',
                'previousViewDate': new DateObject({
                  utcDateValue: previousViewDate.valueOf(),
                  display: selectedDate.format('lll')
                }),
                'leftDate': new DateObject({utcDateValue: moment.utc(selectedDate).subtract(1, 'hours').valueOf()}),
                'rightDate': new DateObject({utcDateValue: moment.utc(selectedDate).add(1, 'hours').valueOf()}),
                'dates': []
              };

              var limit = 60 / configuration.minuteStep;

              for (var i = 0; i < limit; i += 1) {
                var hourMoment = moment.utc(selectedDate).add(i * configuration.minuteStep, 'minute');
                var dateValue = {
                  'utcDateValue': hourMoment.valueOf(),
                  'display': hourMoment.format('LT'),
                  'active': hourMoment.format('YYYY-MM-DD H:mm') === activeFormat
                };

                result.dates.push(new DateObject(dateValue));
              }

              return result;
            },

            setTime: function setTime(unixDate) {
              var tempDate = new Date(unixDate);
              var newDate = new Date(tempDate.getTime() + (tempDate.getTimezoneOffset() * 60000));

              var oldDate = ngModelController.$modelValue;
              ngModelController.$setViewValue(newDate);

              if (configuration.dropdownSelector) {
                jQuery(configuration.dropdownSelector).dropdown('toggle');
              }

              scope.onSetTime({newDate: newDate, oldDate: oldDate});

              return dataFactory[configuration.startView](unixDate);
            }
          };

          var getUTCTime = function getUTCTime(modelValue) {
            var tempDate = (modelValue ? moment(modelValue).toDate() : new Date());
            return tempDate.getTime() - (tempDate.getTimezoneOffset() * 60000);
          };

          scope.changeView = function changeView(viewName, dateObject, event) {
            if (event) {
              event.stopPropagation();
              event.preventDefault();
            }

            if (viewName && (dateObject.utcDateValue > -Infinity) && dateObject.selectable && dataFactory[viewName]) {
              var result = dataFactory[viewName](dateObject.utcDateValue);

              var weekDates = [];
              if (result.weeks) {
                for (var i = 0; i < result.weeks.length; i += 1) {
                  var week = result.weeks[i];
                  for (var j = 0; j < week.dates.length; j += 1) {
                    var weekDate = week.dates[j];
                    weekDates.push(weekDate);
                  }
                }
              }

              scope.beforeRender({
                $view: result.currentView,
                $dates: result.dates || weekDates,
                $leftDate: result.leftDate,
                $upDate: result.previousViewDate,
                $rightDate: result.rightDate
              });

              scope.data = result;
            }
          };

          ngModelController.$render = function $render() {
            scope.changeView(configuration.startView, new DateObject({utcDateValue: getUTCTime(ngModelController.$viewValue)}));
          };
        }
      };
    }]);
}));

/**
 * Binds a TinyMCE widget to <textarea> elements.
 */
angular.module('ui.tinymce', [])
  .value('uiTinymceConfig', {})
  .directive('uiTinymce', ['uiTinymceConfig', function (uiTinymceConfig) {
    uiTinymceConfig = uiTinymceConfig || {};
    var generatedIds = 0;
    return {
      priority: 10,
      require: 'ngModel',
      link: function (scope, elm, attrs, ngModel) {
        var expression, options, tinyInstance,
          updateView = function () {
            ngModel.$setViewValue(elm.val());
            if (!scope.$root.$$phase) {
              scope.$apply();
            }
          };

        // generate an ID if not present
        if (!attrs.id) {
          attrs.$set('id', 'uiTinymce' + generatedIds++);
        }

        if (attrs.uiTinymce) {
          expression = scope.$eval(attrs.uiTinymce);
        } else {
          expression = {};
        }

        // make config'ed setup method available
        if (expression.setup) {
          var configSetup = expression.setup;
          delete expression.setup;
        }

        options = {
          // Update model when calling setContent (such as from the source editor popup)
          setup: function (ed) {
            var args;
            ed.on('init', function(args) {
              ngModel.$render();
              ngModel.$setPristine();
            });
            // Update model on button click
            ed.on('ExecCommand', function (e) {
              ed.save();
              updateView();
            });
            // Update model on keypress
            ed.on('KeyUp', function (e) {
              ed.save();
              updateView();
            });
            // Update model on change, i.e. copy/pasted text, plugins altering content
            ed.on('SetContent', function (e) {
              if (!e.initial && ngModel.$viewValue !== e.content) {
                ed.save();
                updateView();
              }
            });
            ed.on('blur', function(e) {
                elm.blur();
            });
            // Update model when an object has been resized (table, image)
            ed.on('ObjectResized', function (e) {
              ed.save();
              updateView();
            });
            if (configSetup) {
              configSetup(ed);
            }
          },
          mode: 'exact',
          elements: attrs.id
        };
        // extend options with initial uiTinymceConfig and options from directive attribute value
        angular.extend(options, uiTinymceConfig, expression);
        setTimeout(function () {
          tinymce.init(options);
        });

        ngModel.$render = function() {
          if (!tinyInstance) {
            tinyInstance = tinymce.get(attrs.id);
          }
          if (tinyInstance) {
            tinyInstance.setContent(ngModel.$viewValue || '');
          }
        };

        scope.$on('$destroy', function() {
          if (!tinyInstance) { tinyInstance = tinymce.get(attrs.id); }
          if (tinyInstance) {
            tinyInstance.remove();
            tinyInstance = null;
          }
        });
      }
    };
  }]);
angular.module("ui.bootstrap",["ui.bootstrap.tpls","ui.bootstrap.transition","ui.bootstrap.collapse","ui.bootstrap.accordion","ui.bootstrap.alert","ui.bootstrap.bindHtml","ui.bootstrap.buttons","ui.bootstrap.carousel","ui.bootstrap.dateparser","ui.bootstrap.position","ui.bootstrap.datepicker","ui.bootstrap.dropdown","ui.bootstrap.modal","ui.bootstrap.pagination","ui.bootstrap.tooltip","ui.bootstrap.popover","ui.bootstrap.progressbar","ui.bootstrap.rating","ui.bootstrap.tabs","ui.bootstrap.timepicker","ui.bootstrap.typeahead"]),angular.module("ui.bootstrap.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/carousel/carousel.html","template/carousel/slide.html","template/datepicker/datepicker.html","template/datepicker/day.html","template/datepicker/month.html","template/datepicker/popup.html","template/datepicker/year.html","template/modal/backdrop.html","template/modal/window.html","template/pagination/pager.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/progressbar/progressbar.html","template/rating/rating.html","template/tabs/tab.html","template/tabs/tabset.html","template/timepicker/timepicker.html","template/typeahead/typeahead-match.html","template/typeahead/typeahead-popup.html"]),angular.module("ui.bootstrap.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(a,b,c){function d(a){for(var b in a)if(void 0!==f.style[b])return a[b]}var e=function(d,f,g){g=g||{};var h=a.defer(),i=e[g.animation?"animationEndEventName":"transitionEndEventName"],j=function(){c.$apply(function(){d.unbind(i,j),h.resolve(d)})};return i&&d.bind(i,j),b(function(){angular.isString(f)?d.addClass(f):angular.isFunction(f)?f(d):angular.isObject(f)&&d.css(f),i||h.resolve(d)}),h.promise.cancel=function(){i&&d.unbind(i,j),h.reject("Transition cancelled")},h.promise},f=document.createElement("trans"),g={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},h={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return e.transitionEndEventName=d(g),e.animationEndEventName=d(h),e}]),angular.module("ui.bootstrap.collapse",["ui.bootstrap.transition"]).directive("collapse",["$transition",function(a){return{link:function(b,c,d){function e(b){function d(){j===e&&(j=void 0)}var e=a(c,b);return j&&j.cancel(),j=e,e.then(d,d),e}function f(){k?(k=!1,g()):(c.removeClass("collapse").addClass("collapsing"),e({height:c[0].scrollHeight+"px"}).then(g))}function g(){c.removeClass("collapsing"),c.addClass("collapse in"),c.css({height:"auto"})}function h(){if(k)k=!1,i(),c.css({height:0});else{c.css({height:c[0].scrollHeight+"px"});{c[0].offsetWidth}c.removeClass("collapse in").addClass("collapsing"),e({height:0}).then(i)}}function i(){c.removeClass("collapsing"),c.addClass("collapse")}var j,k=!0;b.$watch(d.collapse,function(a){a?h():f()})}}}]),angular.module("ui.bootstrap.accordion",["ui.bootstrap.collapse"]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(a,b,c){this.groups=[],this.closeOthers=function(d){var e=angular.isDefined(b.closeOthers)?a.$eval(b.closeOthers):c.closeOthers;e&&angular.forEach(this.groups,function(a){a!==d&&(a.isOpen=!1)})},this.addGroup=function(a){var b=this;this.groups.push(a),a.$on("$destroy",function(){b.removeGroup(a)})},this.removeGroup=function(a){var b=this.groups.indexOf(a);-1!==b&&this.groups.splice(b,1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",function(){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@",isOpen:"=?",isDisabled:"=?"},controller:function(){this.setHeading=function(a){this.heading=a}},link:function(a,b,c,d){d.addGroup(a),a.$watch("isOpen",function(b){b&&d.closeOthers(a)}),a.toggleOpen=function(){a.isDisabled||(a.isOpen=!a.isOpen)}}}}).directive("accordionHeading",function(){return{restrict:"EA",transclude:!0,template:"",replace:!0,require:"^accordionGroup",link:function(a,b,c,d,e){d.setHeading(e(a,function(){}))}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(a,b,c,d){a.$watch(function(){return d[c.accordionTransclude]},function(a){a&&(b.html(""),b.append(a))})}}}),angular.module("ui.bootstrap.alert",[]).controller("AlertController",["$scope","$attrs",function(a,b){a.closeable="close"in b,this.close=a.close}]).directive("alert",function(){return{restrict:"EA",controller:"AlertController",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"@",close:"&"}}}).directive("dismissOnTimeout",["$timeout",function(a){return{require:"alert",link:function(b,c,d,e){a(function(){e.close()},parseInt(d.dismissOnTimeout,10))}}}]),angular.module("ui.bootstrap.bindHtml",[]).directive("bindHtmlUnsafe",function(){return function(a,b,c){b.addClass("ng-binding").data("$binding",c.bindHtmlUnsafe),a.$watch(c.bindHtmlUnsafe,function(a){b.html(a||"")})}}),angular.module("ui.bootstrap.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).controller("ButtonsController",["buttonConfig",function(a){this.activeClass=a.activeClass||"active",this.toggleEvent=a.toggleEvent||"click"}]).directive("btnRadio",function(){return{require:["btnRadio","ngModel"],controller:"ButtonsController",link:function(a,b,c,d){var e=d[0],f=d[1];f.$render=function(){b.toggleClass(e.activeClass,angular.equals(f.$modelValue,a.$eval(c.btnRadio)))},b.bind(e.toggleEvent,function(){var d=b.hasClass(e.activeClass);(!d||angular.isDefined(c.uncheckable))&&a.$apply(function(){f.$setViewValue(d?null:a.$eval(c.btnRadio)),f.$render()})})}}}).directive("btnCheckbox",function(){return{require:["btnCheckbox","ngModel"],controller:"ButtonsController",link:function(a,b,c,d){function e(){return g(c.btnCheckboxTrue,!0)}function f(){return g(c.btnCheckboxFalse,!1)}function g(b,c){var d=a.$eval(b);return angular.isDefined(d)?d:c}var h=d[0],i=d[1];i.$render=function(){b.toggleClass(h.activeClass,angular.equals(i.$modelValue,e()))},b.bind(h.toggleEvent,function(){a.$apply(function(){i.$setViewValue(b.hasClass(h.activeClass)?f():e()),i.$render()})})}}}),angular.module("ui.bootstrap.carousel",["ui.bootstrap.transition"]).controller("CarouselController",["$scope","$timeout","$interval","$transition",function(a,b,c,d){function e(){f();var b=+a.interval;!isNaN(b)&&b>0&&(h=c(g,b))}function f(){h&&(c.cancel(h),h=null)}function g(){var b=+a.interval;i&&!isNaN(b)&&b>0?a.next():a.pause()}var h,i,j=this,k=j.slides=a.slides=[],l=-1;j.currentSlide=null;var m=!1;j.select=a.select=function(c,f){function g(){if(!m){if(j.currentSlide&&angular.isString(f)&&!a.noTransition&&c.$element){c.$element.addClass(f);{c.$element[0].offsetWidth}angular.forEach(k,function(a){angular.extend(a,{direction:"",entering:!1,leaving:!1,active:!1})}),angular.extend(c,{direction:f,active:!0,entering:!0}),angular.extend(j.currentSlide||{},{direction:f,leaving:!0}),a.$currentTransition=d(c.$element,{}),function(b,c){a.$currentTransition.then(function(){h(b,c)},function(){h(b,c)})}(c,j.currentSlide)}else h(c,j.currentSlide);j.currentSlide=c,l=i,e()}}function h(b,c){angular.extend(b,{direction:"",active:!0,leaving:!1,entering:!1}),angular.extend(c||{},{direction:"",active:!1,leaving:!1,entering:!1}),a.$currentTransition=null}var i=k.indexOf(c);void 0===f&&(f=i>l?"next":"prev"),c&&c!==j.currentSlide&&(a.$currentTransition?(a.$currentTransition.cancel(),b(g)):g())},a.$on("$destroy",function(){m=!0}),j.indexOfSlide=function(a){return k.indexOf(a)},a.next=function(){var b=(l+1)%k.length;return a.$currentTransition?void 0:j.select(k[b],"next")},a.prev=function(){var b=0>l-1?k.length-1:l-1;return a.$currentTransition?void 0:j.select(k[b],"prev")},a.isActive=function(a){return j.currentSlide===a},a.$watch("interval",e),a.$on("$destroy",f),a.play=function(){i||(i=!0,e())},a.pause=function(){a.noPause||(i=!1,f())},j.addSlide=function(b,c){b.$element=c,k.push(b),1===k.length||b.active?(j.select(k[k.length-1]),1==k.length&&a.play()):b.active=!1},j.removeSlide=function(a){var b=k.indexOf(a);k.splice(b,1),k.length>0&&a.active?j.select(b>=k.length?k[b-1]:k[b]):l>b&&l--}}]).directive("carousel",[function(){return{restrict:"EA",transclude:!0,replace:!0,controller:"CarouselController",require:"carousel",templateUrl:"template/carousel/carousel.html",scope:{interval:"=",noTransition:"=",noPause:"="}}}]).directive("slide",function(){return{require:"^carousel",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/carousel/slide.html",scope:{active:"=?"},link:function(a,b,c,d){d.addSlide(a,b),a.$on("$destroy",function(){d.removeSlide(a)}),a.$watch("active",function(b){b&&d.select(a)})}}}),angular.module("ui.bootstrap.dateparser",[]).service("dateParser",["$locale","orderByFilter",function(a,b){function c(a){var c=[],d=a.split("");return angular.forEach(e,function(b,e){var f=a.indexOf(e);if(f>-1){a=a.split(""),d[f]="("+b.regex+")",a[f]="$";for(var g=f+1,h=f+e.length;h>g;g++)d[g]="",a[g]="$";a=a.join(""),c.push({index:f,apply:b.apply})}}),{regex:new RegExp("^"+d.join("")+"$"),map:b(c,"index")}}function d(a,b,c){return 1===b&&c>28?29===c&&(a%4===0&&a%100!==0||a%400===0):3===b||5===b||8===b||10===b?31>c:!0}this.parsers={};var e={yyyy:{regex:"\\d{4}",apply:function(a){this.year=+a}},yy:{regex:"\\d{2}",apply:function(a){this.year=+a+2e3}},y:{regex:"\\d{1,4}",apply:function(a){this.year=+a}},MMMM:{regex:a.DATETIME_FORMATS.MONTH.join("|"),apply:function(b){this.month=a.DATETIME_FORMATS.MONTH.indexOf(b)}},MMM:{regex:a.DATETIME_FORMATS.SHORTMONTH.join("|"),apply:function(b){this.month=a.DATETIME_FORMATS.SHORTMONTH.indexOf(b)}},MM:{regex:"0[1-9]|1[0-2]",apply:function(a){this.month=a-1}},M:{regex:"[1-9]|1[0-2]",apply:function(a){this.month=a-1}},dd:{regex:"[0-2][0-9]{1}|3[0-1]{1}",apply:function(a){this.date=+a}},d:{regex:"[1-2]?[0-9]{1}|3[0-1]{1}",apply:function(a){this.date=+a}},EEEE:{regex:a.DATETIME_FORMATS.DAY.join("|")},EEE:{regex:a.DATETIME_FORMATS.SHORTDAY.join("|")}};this.parse=function(b,e){if(!angular.isString(b)||!e)return b;e=a.DATETIME_FORMATS[e]||e,this.parsers[e]||(this.parsers[e]=c(e));var f=this.parsers[e],g=f.regex,h=f.map,i=b.match(g);if(i&&i.length){for(var j,k={year:1900,month:0,date:1,hours:0},l=1,m=i.length;m>l;l++){var n=h[l-1];n.apply&&n.apply.call(k,i[l])}return d(k.year,k.month,k.date)&&(j=new Date(k.year,k.month,k.date,k.hours)),j}}}]),angular.module("ui.bootstrap.position",[]).factory("$position",["$document","$window",function(a,b){function c(a,c){return a.currentStyle?a.currentStyle[c]:b.getComputedStyle?b.getComputedStyle(a)[c]:a.style[c]}function d(a){return"static"===(c(a,"position")||"static")}var e=function(b){for(var c=a[0],e=b.offsetParent||c;e&&e!==c&&d(e);)e=e.offsetParent;return e||c};return{position:function(b){var c=this.offset(b),d={top:0,left:0},f=e(b[0]);f!=a[0]&&(d=this.offset(angular.element(f)),d.top+=f.clientTop-f.scrollTop,d.left+=f.clientLeft-f.scrollLeft);var g=b[0].getBoundingClientRect();return{width:g.width||b.prop("offsetWidth"),height:g.height||b.prop("offsetHeight"),top:c.top-d.top,left:c.left-d.left}},offset:function(c){var d=c[0].getBoundingClientRect();return{width:d.width||c.prop("offsetWidth"),height:d.height||c.prop("offsetHeight"),top:d.top+(b.pageYOffset||a[0].documentElement.scrollTop),left:d.left+(b.pageXOffset||a[0].documentElement.scrollLeft)}},positionElements:function(a,b,c,d){var e,f,g,h,i=c.split("-"),j=i[0],k=i[1]||"center";e=d?this.offset(a):this.position(a),f=b.prop("offsetWidth"),g=b.prop("offsetHeight");var l={center:function(){return e.left+e.width/2-f/2},left:function(){return e.left},right:function(){return e.left+e.width}},m={center:function(){return e.top+e.height/2-g/2},top:function(){return e.top},bottom:function(){return e.top+e.height}};switch(j){case"right":h={top:m[k](),left:l[j]()};break;case"left":h={top:m[k](),left:e.left-f};break;case"bottom":h={top:m[j](),left:l[k]()};break;default:h={top:e.top-g,left:l[k]()}}return h}}}]),angular.module("ui.bootstrap.datepicker",["ui.bootstrap.dateparser","ui.bootstrap.position"]).constant("datepickerConfig",{formatDay:"dd",formatMonth:"MMMM",formatYear:"yyyy",formatDayHeader:"EEE",formatDayTitle:"MMMM yyyy",formatMonthTitle:"yyyy",datepickerMode:"day",minMode:"day",maxMode:"year",showWeeks:!0,startingDay:0,yearRange:20,minDate:null,maxDate:null}).controller("DatepickerController",["$scope","$attrs","$parse","$interpolate","$timeout","$log","dateFilter","datepickerConfig",function(a,b,c,d,e,f,g,h){var i=this,j={$setViewValue:angular.noop};this.modes=["day","month","year"],angular.forEach(["formatDay","formatMonth","formatYear","formatDayHeader","formatDayTitle","formatMonthTitle","minMode","maxMode","showWeeks","startingDay","yearRange"],function(c,e){i[c]=angular.isDefined(b[c])?8>e?d(b[c])(a.$parent):a.$parent.$eval(b[c]):h[c]}),angular.forEach(["minDate","maxDate"],function(d){b[d]?a.$parent.$watch(c(b[d]),function(a){i[d]=a?new Date(a):null,i.refreshView()}):i[d]=h[d]?new Date(h[d]):null}),a.datepickerMode=a.datepickerMode||h.datepickerMode,a.uniqueId="datepicker-"+a.$id+"-"+Math.floor(1e4*Math.random()),this.activeDate=angular.isDefined(b.initDate)?a.$parent.$eval(b.initDate):new Date,a.isActive=function(b){return 0===i.compare(b.date,i.activeDate)?(a.activeDateId=b.uid,!0):!1},this.init=function(a){j=a,j.$render=function(){i.render()}},this.render=function(){if(j.$modelValue){var a=new Date(j.$modelValue),b=!isNaN(a);b?this.activeDate=a:f.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.'),j.$setValidity("date",b)}this.refreshView()},this.refreshView=function(){if(this.element){this._refreshView();var a=j.$modelValue?new Date(j.$modelValue):null;j.$setValidity("date-disabled",!a||this.element&&!this.isDisabled(a))}},this.createDateObject=function(a,b){var c=j.$modelValue?new Date(j.$modelValue):null;return{date:a,label:g(a,b),selected:c&&0===this.compare(a,c),disabled:this.isDisabled(a),current:0===this.compare(a,new Date)}},this.isDisabled=function(c){return this.minDate&&this.compare(c,this.minDate)<0||this.maxDate&&this.compare(c,this.maxDate)>0||b.dateDisabled&&a.dateDisabled({date:c,mode:a.datepickerMode})},this.split=function(a,b){for(var c=[];a.length>0;)c.push(a.splice(0,b));return c},a.select=function(b){if(a.datepickerMode===i.minMode){var c=j.$modelValue?new Date(j.$modelValue):new Date(0,0,0,0,0,0,0);c.setFullYear(b.getFullYear(),b.getMonth(),b.getDate()),j.$setViewValue(c),j.$render()}else i.activeDate=b,a.datepickerMode=i.modes[i.modes.indexOf(a.datepickerMode)-1]},a.move=function(a){var b=i.activeDate.getFullYear()+a*(i.step.years||0),c=i.activeDate.getMonth()+a*(i.step.months||0);i.activeDate.setFullYear(b,c,1),i.refreshView()},a.toggleMode=function(b){b=b||1,a.datepickerMode===i.maxMode&&1===b||a.datepickerMode===i.minMode&&-1===b||(a.datepickerMode=i.modes[i.modes.indexOf(a.datepickerMode)+b])},a.keys={13:"enter",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down"};var k=function(){e(function(){i.element[0].focus()},0,!1)};a.$on("datepicker.focus",k),a.keydown=function(b){var c=a.keys[b.which];if(c&&!b.shiftKey&&!b.altKey)if(b.preventDefault(),b.stopPropagation(),"enter"===c||"space"===c){if(i.isDisabled(i.activeDate))return;a.select(i.activeDate),k()}else!b.ctrlKey||"up"!==c&&"down"!==c?(i.handleKeyDown(c,b),i.refreshView()):(a.toggleMode("up"===c?1:-1),k())}}]).directive("datepicker",function(){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/datepicker.html",scope:{datepickerMode:"=?",dateDisabled:"&"},require:["datepicker","?^ngModel"],controller:"DatepickerController",link:function(a,b,c,d){var e=d[0],f=d[1];f&&e.init(f)}}}).directive("daypicker",["dateFilter",function(a){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/day.html",require:"^datepicker",link:function(b,c,d,e){function f(a,b){return 1!==b||a%4!==0||a%100===0&&a%400!==0?i[b]:29}function g(a,b){var c=new Array(b),d=new Date(a),e=0;for(d.setHours(12);b>e;)c[e++]=new Date(d),d.setDate(d.getDate()+1);return c}function h(a){var b=new Date(a);b.setDate(b.getDate()+4-(b.getDay()||7));var c=b.getTime();return b.setMonth(0),b.setDate(1),Math.floor(Math.round((c-b)/864e5)/7)+1}b.showWeeks=e.showWeeks,e.step={months:1},e.element=c;var i=[31,28,31,30,31,30,31,31,30,31,30,31];e._refreshView=function(){var c=e.activeDate.getFullYear(),d=e.activeDate.getMonth(),f=new Date(c,d,1),i=e.startingDay-f.getDay(),j=i>0?7-i:-i,k=new Date(f);j>0&&k.setDate(-j+1);for(var l=g(k,42),m=0;42>m;m++)l[m]=angular.extend(e.createDateObject(l[m],e.formatDay),{secondary:l[m].getMonth()!==d,uid:b.uniqueId+"-"+m});b.labels=new Array(7);for(var n=0;7>n;n++)b.labels[n]={abbr:a(l[n].date,e.formatDayHeader),full:a(l[n].date,"EEEE")};if(b.title=a(e.activeDate,e.formatDayTitle),b.rows=e.split(l,7),b.showWeeks){b.weekNumbers=[];for(var o=h(b.rows[0][0].date),p=b.rows.length;b.weekNumbers.push(o++)<p;);}},e.compare=function(a,b){return new Date(a.getFullYear(),a.getMonth(),a.getDate())-new Date(b.getFullYear(),b.getMonth(),b.getDate())},e.handleKeyDown=function(a){var b=e.activeDate.getDate();if("left"===a)b-=1;else if("up"===a)b-=7;else if("right"===a)b+=1;else if("down"===a)b+=7;else if("pageup"===a||"pagedown"===a){var c=e.activeDate.getMonth()+("pageup"===a?-1:1);e.activeDate.setMonth(c,1),b=Math.min(f(e.activeDate.getFullYear(),e.activeDate.getMonth()),b)}else"home"===a?b=1:"end"===a&&(b=f(e.activeDate.getFullYear(),e.activeDate.getMonth()));e.activeDate.setDate(b)},e.refreshView()}}}]).directive("monthpicker",["dateFilter",function(a){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/month.html",require:"^datepicker",link:function(b,c,d,e){e.step={years:1},e.element=c,e._refreshView=function(){for(var c=new Array(12),d=e.activeDate.getFullYear(),f=0;12>f;f++)c[f]=angular.extend(e.createDateObject(new Date(d,f,1),e.formatMonth),{uid:b.uniqueId+"-"+f});b.title=a(e.activeDate,e.formatMonthTitle),b.rows=e.split(c,3)},e.compare=function(a,b){return new Date(a.getFullYear(),a.getMonth())-new Date(b.getFullYear(),b.getMonth())},e.handleKeyDown=function(a){var b=e.activeDate.getMonth();if("left"===a)b-=1;else if("up"===a)b-=3;else if("right"===a)b+=1;else if("down"===a)b+=3;else if("pageup"===a||"pagedown"===a){var c=e.activeDate.getFullYear()+("pageup"===a?-1:1);e.activeDate.setFullYear(c)}else"home"===a?b=0:"end"===a&&(b=11);e.activeDate.setMonth(b)},e.refreshView()}}}]).directive("yearpicker",["dateFilter",function(){return{restrict:"EA",replace:!0,templateUrl:"template/datepicker/year.html",require:"^datepicker",link:function(a,b,c,d){function e(a){return parseInt((a-1)/f,10)*f+1}var f=d.yearRange;d.step={years:f},d.element=b,d._refreshView=function(){for(var b=new Array(f),c=0,g=e(d.activeDate.getFullYear());f>c;c++)b[c]=angular.extend(d.createDateObject(new Date(g+c,0,1),d.formatYear),{uid:a.uniqueId+"-"+c});a.title=[b[0].label,b[f-1].label].join(" - "),a.rows=d.split(b,5)},d.compare=function(a,b){return a.getFullYear()-b.getFullYear()},d.handleKeyDown=function(a){var b=d.activeDate.getFullYear();"left"===a?b-=1:"up"===a?b-=5:"right"===a?b+=1:"down"===a?b+=5:"pageup"===a||"pagedown"===a?b+=("pageup"===a?-1:1)*d.step.years:"home"===a?b=e(d.activeDate.getFullYear()):"end"===a&&(b=e(d.activeDate.getFullYear())+f-1),d.activeDate.setFullYear(b)},d.refreshView()}}}]).constant("datepickerPopupConfig",{datepickerPopup:"yyyy-MM-dd",currentText:"Today",clearText:"Clear",closeText:"Done",closeOnDateSelection:!0,appendToBody:!1,showButtonBar:!0}).directive("datepickerPopup",["$compile","$parse","$document","$position","dateFilter","dateParser","datepickerPopupConfig",function(a,b,c,d,e,f,g){return{restrict:"EA",require:"ngModel",scope:{isOpen:"=?",currentText:"@",clearText:"@",closeText:"@",dateDisabled:"&"},link:function(h,i,j,k){function l(a){return a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()})}function m(a){if(a){if(angular.isDate(a)&&!isNaN(a))return k.$setValidity("date",!0),a;if(angular.isString(a)){var b=f.parse(a,n)||new Date(a);return isNaN(b)?void k.$setValidity("date",!1):(k.$setValidity("date",!0),b)}return void k.$setValidity("date",!1)}return k.$setValidity("date",!0),null}var n,o=angular.isDefined(j.closeOnDateSelection)?h.$parent.$eval(j.closeOnDateSelection):g.closeOnDateSelection,p=angular.isDefined(j.datepickerAppendToBody)?h.$parent.$eval(j.datepickerAppendToBody):g.appendToBody;h.showButtonBar=angular.isDefined(j.showButtonBar)?h.$parent.$eval(j.showButtonBar):g.showButtonBar,h.getText=function(a){return h[a+"Text"]||g[a+"Text"]},j.$observe("datepickerPopup",function(a){n=a||g.datepickerPopup,k.$render()});var q=angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");q.attr({"ng-model":"date","ng-change":"dateSelection()"});var r=angular.element(q.children()[0]);j.datepickerOptions&&angular.forEach(h.$parent.$eval(j.datepickerOptions),function(a,b){r.attr(l(b),a)}),h.watchData={},angular.forEach(["minDate","maxDate","datepickerMode"],function(a){if(j[a]){var c=b(j[a]);if(h.$parent.$watch(c,function(b){h.watchData[a]=b}),r.attr(l(a),"watchData."+a),"datepickerMode"===a){var d=c.assign;h.$watch("watchData."+a,function(a,b){a!==b&&d(h.$parent,a)})}}}),j.dateDisabled&&r.attr("date-disabled","dateDisabled({ date: date, mode: mode })"),k.$parsers.unshift(m),h.dateSelection=function(a){angular.isDefined(a)&&(h.date=a),k.$setViewValue(h.date),k.$render(),o&&(h.isOpen=!1,i[0].focus())},i.bind("input change keyup",function(){h.$apply(function(){h.date=k.$modelValue})}),k.$render=function(){var a=k.$viewValue?e(k.$viewValue,n):"";i.val(a),h.date=m(k.$modelValue)};var s=function(a){h.isOpen&&a.target!==i[0]&&h.$apply(function(){h.isOpen=!1})},t=function(a){h.keydown(a)};i.bind("keydown",t),h.keydown=function(a){27===a.which?(a.preventDefault(),a.stopPropagation(),h.close()):40!==a.which||h.isOpen||(h.isOpen=!0)},h.$watch("isOpen",function(a){a?(h.$broadcast("datepicker.focus"),h.position=p?d.offset(i):d.position(i),h.position.top=h.position.top+i.prop("offsetHeight"),c.bind("click",s)):c.unbind("click",s)}),h.select=function(a){if("today"===a){var b=new Date;angular.isDate(k.$modelValue)?(a=new Date(k.$modelValue),a.setFullYear(b.getFullYear(),b.getMonth(),b.getDate())):a=new Date(b.setHours(0,0,0,0))}h.dateSelection(a)},h.close=function(){h.isOpen=!1,i[0].focus()};var u=a(q)(h);q.remove(),p?c.find("body").append(u):i.after(u),h.$on("$destroy",function(){u.remove(),i.unbind("keydown",t),c.unbind("click",s)})}}}]).directive("datepickerPopupWrap",function(){return{restrict:"EA",replace:!0,transclude:!0,templateUrl:"template/datepicker/popup.html",link:function(a,b){b.bind("click",function(a){a.preventDefault(),a.stopPropagation()})}}}),angular.module("ui.bootstrap.dropdown",[]).constant("dropdownConfig",{openClass:"open"}).service("dropdownService",["$document",function(a){var b=null;this.open=function(e){b||(a.bind("click",c),a.bind("keydown",d)),b&&b!==e&&(b.isOpen=!1),b=e},this.close=function(e){b===e&&(b=null,a.unbind("click",c),a.unbind("keydown",d))};var c=function(a){if(b){var c=b.getToggleElement();a&&c&&c[0].contains(a.target)||b.$apply(function(){b.isOpen=!1})}},d=function(a){27===a.which&&(b.focusToggleElement(),c())}}]).controller("DropdownController",["$scope","$attrs","$parse","dropdownConfig","dropdownService","$animate",function(a,b,c,d,e,f){var g,h=this,i=a.$new(),j=d.openClass,k=angular.noop,l=b.onToggle?c(b.onToggle):angular.noop;this.init=function(d){h.$element=d,b.isOpen&&(g=c(b.isOpen),k=g.assign,a.$watch(g,function(a){i.isOpen=!!a}))},this.toggle=function(a){return i.isOpen=arguments.length?!!a:!i.isOpen},this.isOpen=function(){return i.isOpen},i.getToggleElement=function(){return h.toggleElement},i.focusToggleElement=function(){h.toggleElement&&h.toggleElement[0].focus()},i.$watch("isOpen",function(b,c){f[b?"addClass":"removeClass"](h.$element,j),b?(i.focusToggleElement(),e.open(i)):e.close(i),k(a,b),angular.isDefined(b)&&b!==c&&l(a,{open:!!b})}),a.$on("$locationChangeSuccess",function(){i.isOpen=!1}),a.$on("$destroy",function(){i.$destroy()})}]).directive("dropdown",function(){return{controller:"DropdownController",link:function(a,b,c,d){d.init(b)}}}).directive("dropdownToggle",function(){return{require:"?^dropdown",link:function(a,b,c,d){if(d){d.toggleElement=b;var e=function(e){e.preventDefault(),b.hasClass("disabled")||c.disabled||a.$apply(function(){d.toggle()})};b.bind("click",e),b.attr({"aria-haspopup":!0,"aria-expanded":!1}),a.$watch(d.isOpen,function(a){b.attr("aria-expanded",!!a)}),a.$on("$destroy",function(){b.unbind("click",e)})}}}}),angular.module("ui.bootstrap.modal",["ui.bootstrap.transition"]).factory("$$stackedMap",function(){return{createNew:function(){var a=[];return{add:function(b,c){a.push({key:b,value:c})},get:function(b){for(var c=0;c<a.length;c++)if(b==a[c].key)return a[c]},keys:function(){for(var b=[],c=0;c<a.length;c++)b.push(a[c].key);return b},top:function(){return a[a.length-1]},remove:function(b){for(var c=-1,d=0;d<a.length;d++)if(b==a[d].key){c=d;break}return a.splice(c,1)[0]},removeTop:function(){return a.splice(a.length-1,1)[0]},length:function(){return a.length}}}}}).directive("modalBackdrop",["$timeout",function(a){return{restrict:"EA",replace:!0,templateUrl:"template/modal/backdrop.html",link:function(b,c,d){b.backdropClass=d.backdropClass||"",b.animate=!1,a(function(){b.animate=!0})}}}]).directive("modalWindow",["$modalStack","$timeout",function(a,b){return{restrict:"EA",scope:{index:"@",animate:"="},replace:!0,transclude:!0,templateUrl:function(a,b){return b.templateUrl||"template/modal/window.html"},link:function(c,d,e){d.addClass(e.windowClass||""),c.size=e.size,b(function(){c.animate=!0,d[0].querySelectorAll("[autofocus]").length||d[0].focus()}),c.close=function(b){var c=a.getTop();c&&c.value.backdrop&&"static"!=c.value.backdrop&&b.target===b.currentTarget&&(b.preventDefault(),b.stopPropagation(),a.dismiss(c.key,"backdrop click"))}}}}]).directive("modalTransclude",function(){return{link:function(a,b,c,d,e){e(a.$parent,function(a){b.empty(),b.append(a)})}}}).factory("$modalStack",["$transition","$timeout","$document","$compile","$rootScope","$$stackedMap",function(a,b,c,d,e,f){function g(){for(var a=-1,b=n.keys(),c=0;c<b.length;c++)n.get(b[c]).value.backdrop&&(a=c);return a}function h(a){var b=c.find("body").eq(0),d=n.get(a).value;n.remove(a),j(d.modalDomEl,d.modalScope,300,function(){d.modalScope.$destroy(),b.toggleClass(m,n.length()>0),i()})}function i(){if(k&&-1==g()){var a=l;j(k,l,150,function(){a.$destroy(),a=null}),k=void 0,l=void 0}}function j(c,d,e,f){function g(){g.done||(g.done=!0,c.remove(),f&&f())}d.animate=!1;var h=a.transitionEndEventName;if(h){var i=b(g,e);c.bind(h,function(){b.cancel(i),g(),d.$apply()})}else b(g)}var k,l,m="modal-open",n=f.createNew(),o={};return e.$watch(g,function(a){l&&(l.index=a)}),c.bind("keydown",function(a){var b;27===a.which&&(b=n.top(),b&&b.value.keyboard&&(a.preventDefault(),e.$apply(function(){o.dismiss(b.key,"escape key press")})))}),o.open=function(a,b){n.add(a,{deferred:b.deferred,modalScope:b.scope,backdrop:b.backdrop,keyboard:b.keyboard});var f=c.find("body").eq(0),h=g();if(h>=0&&!k){l=e.$new(!0),l.index=h;var i=angular.element("<div modal-backdrop></div>");i.attr("backdrop-class",b.backdropClass),k=d(i)(l),f.append(k)}var j=angular.element("<div modal-window></div>");j.attr({"template-url":b.windowTemplateUrl,"window-class":b.windowClass,size:b.size,index:n.length()-1,animate:"animate"}).html(b.content);var o=d(j)(b.scope);n.top().value.modalDomEl=o,f.append(o),f.addClass(m)},o.close=function(a,b){var c=n.get(a);c&&(c.value.deferred.resolve(b),h(a))},o.dismiss=function(a,b){var c=n.get(a);c&&(c.value.deferred.reject(b),h(a))},o.dismissAll=function(a){for(var b=this.getTop();b;)this.dismiss(b.key,a),b=this.getTop()},o.getTop=function(){return n.top()},o}]).provider("$modal",function(){var a={options:{backdrop:!0,keyboard:!0},$get:["$injector","$rootScope","$q","$http","$templateCache","$controller","$modalStack",function(b,c,d,e,f,g,h){function i(a){return a.template?d.when(a.template):e.get(angular.isFunction(a.templateUrl)?a.templateUrl():a.templateUrl,{cache:f}).then(function(a){return a.data})}function j(a){var c=[];return angular.forEach(a,function(a){(angular.isFunction(a)||angular.isArray(a))&&c.push(d.when(b.invoke(a)))}),c}var k={};return k.open=function(b){var e=d.defer(),f=d.defer(),k={result:e.promise,opened:f.promise,close:function(a){h.close(k,a)},dismiss:function(a){h.dismiss(k,a)}};if(b=angular.extend({},a.options,b),b.resolve=b.resolve||{},!b.template&&!b.templateUrl)throw new Error("One of template or templateUrl options is required.");var l=d.all([i(b)].concat(j(b.resolve)));return l.then(function(a){var d=(b.scope||c).$new();d.$close=k.close,d.$dismiss=k.dismiss;var f,i={},j=1;b.controller&&(i.$scope=d,i.$modalInstance=k,angular.forEach(b.resolve,function(b,c){i[c]=a[j++]}),f=g(b.controller,i),b.controllerAs&&(d[b.controllerAs]=f)),h.open(k,{scope:d,deferred:e,content:a[0],backdrop:b.backdrop,keyboard:b.keyboard,backdropClass:b.backdropClass,windowClass:b.windowClass,windowTemplateUrl:b.windowTemplateUrl,size:b.size})},function(a){e.reject(a)}),l.then(function(){f.resolve(!0)},function(){f.reject(!1)}),k},k}]};return a}),angular.module("ui.bootstrap.pagination",[]).controller("PaginationController",["$scope","$attrs","$parse",function(a,b,c){var d=this,e={$setViewValue:angular.noop},f=b.numPages?c(b.numPages).assign:angular.noop;this.init=function(f,g){e=f,this.config=g,e.$render=function(){d.render()},b.itemsPerPage?a.$parent.$watch(c(b.itemsPerPage),function(b){d.itemsPerPage=parseInt(b,10),a.totalPages=d.calculateTotalPages()}):this.itemsPerPage=g.itemsPerPage},this.calculateTotalPages=function(){var b=this.itemsPerPage<1?1:Math.ceil(a.totalItems/this.itemsPerPage);return Math.max(b||0,1)},this.render=function(){a.page=parseInt(e.$viewValue,10)||1},a.selectPage=function(b){a.page!==b&&b>0&&b<=a.totalPages&&(e.$setViewValue(b),e.$render())},a.getText=function(b){return a[b+"Text"]||d.config[b+"Text"]},a.noPrevious=function(){return 1===a.page},a.noNext=function(){return a.page===a.totalPages},a.$watch("totalItems",function(){a.totalPages=d.calculateTotalPages()}),a.$watch("totalPages",function(b){f(a.$parent,b),a.page>b?a.selectPage(b):e.$render()})}]).constant("paginationConfig",{itemsPerPage:10,boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last",rotate:!0}).directive("pagination",["$parse","paginationConfig",function(a,b){return{restrict:"EA",scope:{totalItems:"=",firstText:"@",previousText:"@",nextText:"@",lastText:"@"},require:["pagination","?ngModel"],controller:"PaginationController",templateUrl:"template/pagination/pagination.html",replace:!0,link:function(c,d,e,f){function g(a,b,c){return{number:a,text:b,active:c}}function h(a,b){var c=[],d=1,e=b,f=angular.isDefined(k)&&b>k;f&&(l?(d=Math.max(a-Math.floor(k/2),1),e=d+k-1,e>b&&(e=b,d=e-k+1)):(d=(Math.ceil(a/k)-1)*k+1,e=Math.min(d+k-1,b)));for(var h=d;e>=h;h++){var i=g(h,h,h===a);c.push(i)}if(f&&!l){if(d>1){var j=g(d-1,"...",!1);c.unshift(j)}if(b>e){var m=g(e+1,"...",!1);c.push(m)}}return c}var i=f[0],j=f[1];if(j){var k=angular.isDefined(e.maxSize)?c.$parent.$eval(e.maxSize):b.maxSize,l=angular.isDefined(e.rotate)?c.$parent.$eval(e.rotate):b.rotate;c.boundaryLinks=angular.isDefined(e.boundaryLinks)?c.$parent.$eval(e.boundaryLinks):b.boundaryLinks,c.directionLinks=angular.isDefined(e.directionLinks)?c.$parent.$eval(e.directionLinks):b.directionLinks,i.init(j,b),e.maxSize&&c.$parent.$watch(a(e.maxSize),function(a){k=parseInt(a,10),i.render()
});var m=i.render;i.render=function(){m(),c.page>0&&c.page<=c.totalPages&&(c.pages=h(c.page,c.totalPages))}}}}}]).constant("pagerConfig",{itemsPerPage:10,previousText:"« Previous",nextText:"Next »",align:!0}).directive("pager",["pagerConfig",function(a){return{restrict:"EA",scope:{totalItems:"=",previousText:"@",nextText:"@"},require:["pager","?ngModel"],controller:"PaginationController",templateUrl:"template/pagination/pager.html",replace:!0,link:function(b,c,d,e){var f=e[0],g=e[1];g&&(b.align=angular.isDefined(d.align)?b.$parent.$eval(d.align):a.align,f.init(g,a))}}}]),angular.module("ui.bootstrap.tooltip",["ui.bootstrap.position","ui.bootstrap.bindHtml"]).provider("$tooltip",function(){function a(a){var b=/[A-Z]/g,c="-";return a.replace(b,function(a,b){return(b?c:"")+a.toLowerCase()})}var b={placement:"top",animation:!0,popupDelay:0},c={mouseenter:"mouseleave",click:"click",focus:"blur"},d={};this.options=function(a){angular.extend(d,a)},this.setTriggers=function(a){angular.extend(c,a)},this.$get=["$window","$compile","$timeout","$document","$position","$interpolate",function(e,f,g,h,i,j){return function(e,k,l){function m(a){var b=a||n.trigger||l,d=c[b]||b;return{show:b,hide:d}}var n=angular.extend({},b,d),o=a(e),p=j.startSymbol(),q=j.endSymbol(),r="<div "+o+'-popup title="'+p+"title"+q+'" content="'+p+"content"+q+'" placement="'+p+"placement"+q+'" animation="animation" is-open="isOpen"></div>';return{restrict:"EA",compile:function(){var a=f(r);return function(b,c,d){function f(){D.isOpen?l():j()}function j(){(!C||b.$eval(d[k+"Enable"]))&&(s(),D.popupDelay?z||(z=g(o,D.popupDelay,!1),z.then(function(a){a()})):o()())}function l(){b.$apply(function(){p()})}function o(){return z=null,y&&(g.cancel(y),y=null),D.content?(q(),w.css({top:0,left:0,display:"block"}),D.$digest(),E(),D.isOpen=!0,D.$digest(),E):angular.noop}function p(){D.isOpen=!1,g.cancel(z),z=null,D.animation?y||(y=g(r,500)):r()}function q(){w&&r(),x=D.$new(),w=a(x,function(a){A?h.find("body").append(a):c.after(a)})}function r(){y=null,w&&(w.remove(),w=null),x&&(x.$destroy(),x=null)}function s(){t(),u()}function t(){var a=d[k+"Placement"];D.placement=angular.isDefined(a)?a:n.placement}function u(){var a=d[k+"PopupDelay"],b=parseInt(a,10);D.popupDelay=isNaN(b)?n.popupDelay:b}function v(){var a=d[k+"Trigger"];F(),B=m(a),B.show===B.hide?c.bind(B.show,f):(c.bind(B.show,j),c.bind(B.hide,l))}var w,x,y,z,A=angular.isDefined(n.appendToBody)?n.appendToBody:!1,B=m(void 0),C=angular.isDefined(d[k+"Enable"]),D=b.$new(!0),E=function(){var a=i.positionElements(c,w,D.placement,A);a.top+="px",a.left+="px",w.css(a)};D.isOpen=!1,d.$observe(e,function(a){D.content=a,!a&&D.isOpen&&p()}),d.$observe(k+"Title",function(a){D.title=a});var F=function(){c.unbind(B.show,j),c.unbind(B.hide,l)};v();var G=b.$eval(d[k+"Animation"]);D.animation=angular.isDefined(G)?!!G:n.animation;var H=b.$eval(d[k+"AppendToBody"]);A=angular.isDefined(H)?H:A,A&&b.$on("$locationChangeSuccess",function(){D.isOpen&&p()}),b.$on("$destroy",function(){g.cancel(y),g.cancel(z),F(),r(),D=null})}}}}}]}).directive("tooltipPopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(a){return a("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(a){return a("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("ui.bootstrap.popover",["ui.bootstrap.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$tooltip",function(a){return a("popover","popover","click")}]),angular.module("ui.bootstrap.progressbar",[]).constant("progressConfig",{animate:!0,max:100}).controller("ProgressController",["$scope","$attrs","progressConfig",function(a,b,c){var d=this,e=angular.isDefined(b.animate)?a.$parent.$eval(b.animate):c.animate;this.bars=[],a.max=angular.isDefined(b.max)?a.$parent.$eval(b.max):c.max,this.addBar=function(b,c){e||c.css({transition:"none"}),this.bars.push(b),b.$watch("value",function(c){b.percent=+(100*c/a.max).toFixed(2)}),b.$on("$destroy",function(){c=null,d.removeBar(b)})},this.removeBar=function(a){this.bars.splice(this.bars.indexOf(a),1)}}]).directive("progress",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",require:"progress",scope:{},templateUrl:"template/progressbar/progress.html"}}).directive("bar",function(){return{restrict:"EA",replace:!0,transclude:!0,require:"^progress",scope:{value:"=",type:"@"},templateUrl:"template/progressbar/bar.html",link:function(a,b,c,d){d.addBar(a,b)}}}).directive("progressbar",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",scope:{value:"=",type:"@"},templateUrl:"template/progressbar/progressbar.html",link:function(a,b,c,d){d.addBar(a,angular.element(b.children()[0]))}}}),angular.module("ui.bootstrap.rating",[]).constant("ratingConfig",{max:5,stateOn:null,stateOff:null}).controller("RatingController",["$scope","$attrs","ratingConfig",function(a,b,c){var d={$setViewValue:angular.noop};this.init=function(e){d=e,d.$render=this.render,this.stateOn=angular.isDefined(b.stateOn)?a.$parent.$eval(b.stateOn):c.stateOn,this.stateOff=angular.isDefined(b.stateOff)?a.$parent.$eval(b.stateOff):c.stateOff;var f=angular.isDefined(b.ratingStates)?a.$parent.$eval(b.ratingStates):new Array(angular.isDefined(b.max)?a.$parent.$eval(b.max):c.max);a.range=this.buildTemplateObjects(f)},this.buildTemplateObjects=function(a){for(var b=0,c=a.length;c>b;b++)a[b]=angular.extend({index:b},{stateOn:this.stateOn,stateOff:this.stateOff},a[b]);return a},a.rate=function(b){!a.readonly&&b>=0&&b<=a.range.length&&(d.$setViewValue(b),d.$render())},a.enter=function(b){a.readonly||(a.value=b),a.onHover({value:b})},a.reset=function(){a.value=d.$viewValue,a.onLeave()},a.onKeydown=function(b){/(37|38|39|40)/.test(b.which)&&(b.preventDefault(),b.stopPropagation(),a.rate(a.value+(38===b.which||39===b.which?1:-1)))},this.render=function(){a.value=d.$viewValue}}]).directive("rating",function(){return{restrict:"EA",require:["rating","ngModel"],scope:{readonly:"=?",onHover:"&",onLeave:"&"},controller:"RatingController",templateUrl:"template/rating/rating.html",replace:!0,link:function(a,b,c,d){var e=d[0],f=d[1];f&&e.init(f)}}}),angular.module("ui.bootstrap.tabs",[]).controller("TabsetController",["$scope",function(a){var b=this,c=b.tabs=a.tabs=[];b.select=function(a){angular.forEach(c,function(b){b.active&&b!==a&&(b.active=!1,b.onDeselect())}),a.active=!0,a.onSelect()},b.addTab=function(a){c.push(a),1===c.length?a.active=!0:a.active&&b.select(a)},b.removeTab=function(a){var e=c.indexOf(a);if(a.active&&c.length>1&&!d){var f=e==c.length-1?e-1:e+1;b.select(c[f])}c.splice(e,1)};var d;a.$on("$destroy",function(){d=!0})}]).directive("tabset",function(){return{restrict:"EA",transclude:!0,replace:!0,scope:{type:"@"},controller:"TabsetController",templateUrl:"template/tabs/tabset.html",link:function(a,b,c){a.vertical=angular.isDefined(c.vertical)?a.$parent.$eval(c.vertical):!1,a.justified=angular.isDefined(c.justified)?a.$parent.$eval(c.justified):!1}}}).directive("tab",["$parse",function(a){return{require:"^tabset",restrict:"EA",replace:!0,templateUrl:"template/tabs/tab.html",transclude:!0,scope:{active:"=?",heading:"@",onSelect:"&select",onDeselect:"&deselect"},controller:function(){},compile:function(b,c,d){return function(b,c,e,f){b.$watch("active",function(a){a&&f.select(b)}),b.disabled=!1,e.disabled&&b.$parent.$watch(a(e.disabled),function(a){b.disabled=!!a}),b.select=function(){b.disabled||(b.active=!0)},f.addTab(b),b.$on("$destroy",function(){f.removeTab(b)}),b.$transcludeFn=d}}}}]).directive("tabHeadingTransclude",[function(){return{restrict:"A",require:"^tab",link:function(a,b){a.$watch("headingElement",function(a){a&&(b.html(""),b.append(a))})}}}]).directive("tabContentTransclude",function(){function a(a){return a.tagName&&(a.hasAttribute("tab-heading")||a.hasAttribute("data-tab-heading")||"tab-heading"===a.tagName.toLowerCase()||"data-tab-heading"===a.tagName.toLowerCase())}return{restrict:"A",require:"^tabset",link:function(b,c,d){var e=b.$eval(d.tabContentTransclude);e.$transcludeFn(e.$parent,function(b){angular.forEach(b,function(b){a(b)?e.headingElement=b:c.append(b)})})}}}),angular.module("ui.bootstrap.timepicker",[]).constant("timepickerConfig",{hourStep:1,minuteStep:1,showMeridian:!0,meridians:null,readonlyInput:!1,mousewheel:!0}).controller("TimepickerController",["$scope","$attrs","$parse","$log","$locale","timepickerConfig",function(a,b,c,d,e,f){function g(){var b=parseInt(a.hours,10),c=a.showMeridian?b>0&&13>b:b>=0&&24>b;return c?(a.showMeridian&&(12===b&&(b=0),a.meridian===p[1]&&(b+=12)),b):void 0}function h(){var b=parseInt(a.minutes,10);return b>=0&&60>b?b:void 0}function i(a){return angular.isDefined(a)&&a.toString().length<2?"0"+a:a}function j(a){k(),o.$setViewValue(new Date(n)),l(a)}function k(){o.$setValidity("time",!0),a.invalidHours=!1,a.invalidMinutes=!1}function l(b){var c=n.getHours(),d=n.getMinutes();a.showMeridian&&(c=0===c||12===c?12:c%12),a.hours="h"===b?c:i(c),a.minutes="m"===b?d:i(d),a.meridian=n.getHours()<12?p[0]:p[1]}function m(a){var b=new Date(n.getTime()+6e4*a);n.setHours(b.getHours(),b.getMinutes()),j()}var n=new Date,o={$setViewValue:angular.noop},p=angular.isDefined(b.meridians)?a.$parent.$eval(b.meridians):f.meridians||e.DATETIME_FORMATS.AMPMS;this.init=function(c,d){o=c,o.$render=this.render;var e=d.eq(0),g=d.eq(1),h=angular.isDefined(b.mousewheel)?a.$parent.$eval(b.mousewheel):f.mousewheel;h&&this.setupMousewheelEvents(e,g),a.readonlyInput=angular.isDefined(b.readonlyInput)?a.$parent.$eval(b.readonlyInput):f.readonlyInput,this.setupInputEvents(e,g)};var q=f.hourStep;b.hourStep&&a.$parent.$watch(c(b.hourStep),function(a){q=parseInt(a,10)});var r=f.minuteStep;b.minuteStep&&a.$parent.$watch(c(b.minuteStep),function(a){r=parseInt(a,10)}),a.showMeridian=f.showMeridian,b.showMeridian&&a.$parent.$watch(c(b.showMeridian),function(b){if(a.showMeridian=!!b,o.$error.time){var c=g(),d=h();angular.isDefined(c)&&angular.isDefined(d)&&(n.setHours(c),j())}else l()}),this.setupMousewheelEvents=function(b,c){var d=function(a){a.originalEvent&&(a=a.originalEvent);var b=a.wheelDelta?a.wheelDelta:-a.deltaY;return a.detail||b>0};b.bind("mousewheel wheel",function(b){a.$apply(d(b)?a.incrementHours():a.decrementHours()),b.preventDefault()}),c.bind("mousewheel wheel",function(b){a.$apply(d(b)?a.incrementMinutes():a.decrementMinutes()),b.preventDefault()})},this.setupInputEvents=function(b,c){if(a.readonlyInput)return a.updateHours=angular.noop,void(a.updateMinutes=angular.noop);var d=function(b,c){o.$setViewValue(null),o.$setValidity("time",!1),angular.isDefined(b)&&(a.invalidHours=b),angular.isDefined(c)&&(a.invalidMinutes=c)};a.updateHours=function(){var a=g();angular.isDefined(a)?(n.setHours(a),j("h")):d(!0)},b.bind("blur",function(){!a.invalidHours&&a.hours<10&&a.$apply(function(){a.hours=i(a.hours)})}),a.updateMinutes=function(){var a=h();angular.isDefined(a)?(n.setMinutes(a),j("m")):d(void 0,!0)},c.bind("blur",function(){!a.invalidMinutes&&a.minutes<10&&a.$apply(function(){a.minutes=i(a.minutes)})})},this.render=function(){var a=o.$modelValue?new Date(o.$modelValue):null;isNaN(a)?(o.$setValidity("time",!1),d.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')):(a&&(n=a),k(),l())},a.incrementHours=function(){m(60*q)},a.decrementHours=function(){m(60*-q)},a.incrementMinutes=function(){m(r)},a.decrementMinutes=function(){m(-r)},a.toggleMeridian=function(){m(720*(n.getHours()<12?1:-1))}}]).directive("timepicker",function(){return{restrict:"EA",require:["timepicker","?^ngModel"],controller:"TimepickerController",replace:!0,scope:{},templateUrl:"template/timepicker/timepicker.html",link:function(a,b,c,d){var e=d[0],f=d[1];f&&e.init(f,b.find("input"))}}}),angular.module("ui.bootstrap.typeahead",["ui.bootstrap.position","ui.bootstrap.bindHtml"]).factory("typeaheadParser",["$parse",function(a){var b=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+([\s\S]+?)$/;return{parse:function(c){var d=c.match(b);if(!d)throw new Error('Expected typeahead specification in form of "_modelValue_ (as _label_)? for _item_ in _collection_" but got "'+c+'".');return{itemName:d[3],source:a(d[4]),viewMapper:a(d[2]||d[1]),modelMapper:a(d[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$timeout","$document","$position","typeaheadParser",function(a,b,c,d,e,f,g){var h=[9,13,27,38,40];return{require:"ngModel",link:function(i,j,k,l){var m,n=i.$eval(k.typeaheadMinLength)||1,o=i.$eval(k.typeaheadWaitMs)||0,p=i.$eval(k.typeaheadEditable)!==!1,q=b(k.typeaheadLoading).assign||angular.noop,r=b(k.typeaheadOnSelect),s=k.typeaheadInputFormatter?b(k.typeaheadInputFormatter):void 0,t=k.typeaheadAppendToBody?i.$eval(k.typeaheadAppendToBody):!1,u=i.$eval(k.typeaheadFocusFirst)!==!1,v=b(k.ngModel).assign,w=g.parse(k.typeahead),x=i.$new();i.$on("$destroy",function(){x.$destroy()});var y="typeahead-"+x.$id+"-"+Math.floor(1e4*Math.random());j.attr({"aria-autocomplete":"list","aria-expanded":!1,"aria-owns":y});var z=angular.element("<div typeahead-popup></div>");z.attr({id:y,matches:"matches",active:"activeIdx",select:"select(activeIdx)",query:"query",position:"position"}),angular.isDefined(k.typeaheadTemplateUrl)&&z.attr("template-url",k.typeaheadTemplateUrl);var A=function(){x.matches=[],x.activeIdx=-1,j.attr("aria-expanded",!1)},B=function(a){return y+"-option-"+a};x.$watch("activeIdx",function(a){0>a?j.removeAttr("aria-activedescendant"):j.attr("aria-activedescendant",B(a))});var C=function(a){var b={$viewValue:a};q(i,!0),c.when(w.source(i,b)).then(function(c){var d=a===l.$viewValue;if(d&&m)if(c.length>0){x.activeIdx=u?0:-1,x.matches.length=0;for(var e=0;e<c.length;e++)b[w.itemName]=c[e],x.matches.push({id:B(e),label:w.viewMapper(x,b),model:c[e]});x.query=a,x.position=t?f.offset(j):f.position(j),x.position.top=x.position.top+j.prop("offsetHeight"),j.attr("aria-expanded",!0)}else A();d&&q(i,!1)},function(){A(),q(i,!1)})};A(),x.query=void 0;var D,E=function(a){D=d(function(){C(a)},o)},F=function(){D&&d.cancel(D)};l.$parsers.unshift(function(a){return m=!0,a&&a.length>=n?o>0?(F(),E(a)):C(a):(q(i,!1),F(),A()),p?a:a?void l.$setValidity("editable",!1):(l.$setValidity("editable",!0),a)}),l.$formatters.push(function(a){var b,c,d={};return s?(d.$model=a,s(i,d)):(d[w.itemName]=a,b=w.viewMapper(i,d),d[w.itemName]=void 0,c=w.viewMapper(i,d),b!==c?b:a)}),x.select=function(a){var b,c,e={};e[w.itemName]=c=x.matches[a].model,b=w.modelMapper(i,e),v(i,b),l.$setValidity("editable",!0),r(i,{$item:c,$model:b,$label:w.viewMapper(i,e)}),A(),d(function(){j[0].focus()},0,!1)},j.bind("keydown",function(a){0!==x.matches.length&&-1!==h.indexOf(a.which)&&(-1!=x.activeIdx||13!==a.which&&9!==a.which)&&(a.preventDefault(),40===a.which?(x.activeIdx=(x.activeIdx+1)%x.matches.length,x.$digest()):38===a.which?(x.activeIdx=(x.activeIdx>0?x.activeIdx:x.matches.length)-1,x.$digest()):13===a.which||9===a.which?x.$apply(function(){x.select(x.activeIdx)}):27===a.which&&(a.stopPropagation(),A(),x.$digest()))}),j.bind("blur",function(){m=!1});var G=function(a){j[0]!==a.target&&(A(),x.$digest())};e.bind("click",G),i.$on("$destroy",function(){e.unbind("click",G),t&&H.remove()});var H=a(z)(x);t?e.find("body").append(H):j.after(H)}}}]).directive("typeaheadPopup",function(){return{restrict:"EA",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead-popup.html",link:function(a,b,c){a.templateUrl=c.templateUrl,a.isOpen=function(){return a.matches.length>0},a.isActive=function(b){return a.active==b},a.selectActive=function(b){a.active=b},a.selectMatch=function(b){a.select({activeIdx:b})}}}}).directive("typeaheadMatch",["$http","$templateCache","$compile","$parse",function(a,b,c,d){return{restrict:"EA",scope:{index:"=",match:"=",query:"="},link:function(e,f,g){var h=d(g.templateUrl)(e.$parent)||"template/typeahead/typeahead-match.html";a.get(h,{cache:b}).success(function(a){f.replaceWith(c(a.trim())(e))})}}}]).filter("typeaheadHighlight",function(){function a(a){return a.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(b,c){return c?(""+b).replace(new RegExp(a(c),"gi"),"<strong>$&</strong>"):b}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(a){a.put("template/accordion/accordion-group.html",'<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a href class="accordion-toggle" ng-click="toggleOpen()" accordion-transclude="heading"><span ng-class="{\'text-muted\': isDisabled}">{{heading}}</span></a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>\n')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(a){a.put("template/accordion/accordion.html",'<div class="panel-group" ng-transclude></div>')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(a){a.put("template/alert/alert.html",'<div class="alert" ng-class="[\'alert-\' + (type || \'warning\'), closeable ? \'alert-dismissable\' : null]" role="alert">\n    <button ng-show="closeable" type="button" class="close" ng-click="close()">\n        <span aria-hidden="true">&times;</span>\n        <span class="sr-only">Close</span>\n    </button>\n    <div ng-transclude></div>\n</div>\n')}]),angular.module("template/carousel/carousel.html",[]).run(["$templateCache",function(a){a.put("template/carousel/carousel.html",'<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel" ng-swipe-right="prev()" ng-swipe-left="next()">\n    <ol class="carousel-indicators" ng-show="slides.length > 1">\n        <li ng-repeat="slide in slides track by $index" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-left"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides.length > 1"><span class="glyphicon glyphicon-chevron-right"></span></a>\n</div>\n')}]),angular.module("template/carousel/slide.html",[]).run(["$templateCache",function(a){a.put("template/carousel/slide.html","<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item text-center\" ng-transclude></div>\n")}]),angular.module("template/datepicker/datepicker.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/datepicker.html",'<div ng-switch="datepickerMode" role="application" ng-keydown="keydown($event)">\n  <daypicker ng-switch-when="day" tabindex="0"></daypicker>\n  <monthpicker ng-switch-when="month" tabindex="0"></monthpicker>\n  <yearpicker ng-switch-when="year" tabindex="0"></yearpicker>\n</div>')}]),angular.module("template/datepicker/day.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/day.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{5 + showWeeks}}"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr>\n      <th ng-show="showWeeks" class="text-center"></th>\n      <th ng-repeat="label in labels track by $index" class="text-center"><small aria-label="{{label.full}}">{{label.abbr}}</small></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-show="showWeeks" class="text-center h6"><em>{{ weekNumbers[$index] }}</em></td>\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-muted\': dt.secondary, \'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/datepicker/month.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/month.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/datepicker/popup.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/popup.html",'<ul class="dropdown-menu" ng-style="{display: (isOpen && \'block\') || \'none\', top: position.top+\'px\', left: position.left+\'px\'}" ng-keydown="keydown($event)">\n	<li ng-transclude></li>\n	<li ng-if="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group pull-left">\n			<button type="button" class="btn btn-sm btn-info" ng-click="select(\'today\')">{{ getText(\'current\') }}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="select(null)">{{ getText(\'clear\') }}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="close()">{{ getText(\'close\') }}</button>\n	</li>\n</ul>\n')}]),angular.module("template/datepicker/year.html",[]).run(["$templateCache",function(a){a.put("template/datepicker/year.html",'<table role="grid" aria-labelledby="{{uniqueId}}-title" aria-activedescendant="{{activeDateId}}">\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="3"><button id="{{uniqueId}}-title" role="heading" aria-live="assertive" aria-atomic="true" type="button" class="btn btn-default btn-sm" ng-click="toggleMode()" tabindex="-1" style="width:100%;"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)" tabindex="-1"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows track by $index">\n      <td ng-repeat="dt in row track by dt.date" class="text-center" role="gridcell" id="{{dt.uid}}" aria-disabled="{{!!dt.disabled}}">\n        <button type="button" style="width:100%;" class="btn btn-default" ng-class="{\'btn-info\': dt.selected, active: isActive(dt)}" ng-click="select(dt.date)" ng-disabled="dt.disabled" tabindex="-1"><span ng-class="{\'text-info\': dt.current}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')}]),angular.module("template/modal/backdrop.html",[]).run(["$templateCache",function(a){a.put("template/modal/backdrop.html",'<div class="modal-backdrop fade {{ backdropClass }}"\n     ng-class="{in: animate}"\n     ng-style="{\'z-index\': 1040 + (index && 1 || 0) + index*10}"\n></div>\n')}]),angular.module("template/modal/window.html",[]).run(["$templateCache",function(a){a.put("template/modal/window.html",'<div tabindex="-1" role="dialog" class="modal fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog" ng-class="{\'modal-sm\': size == \'sm\', \'modal-lg\': size == \'lg\'}"><div class="modal-content" modal-transclude></div></div>\n</div>')}]),angular.module("template/pagination/pager.html",[]).run(["$templateCache",function(a){a.put("template/pagination/pager.html",'<ul class="pager">\n  <li ng-class="{disabled: noPrevious(), previous: align}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-class="{disabled: noNext(), next: align}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n</ul>')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(a){a.put("template/pagination/pagination.html",'<ul class="pagination">\n  <li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(1)">{{getText(\'first\')}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noPrevious()}"><a href ng-click="selectPage(page - 1)">{{getText(\'previous\')}}</a></li>\n  <li ng-repeat="page in pages track by $index" ng-class="{active: page.active}"><a href ng-click="selectPage(page.number)">{{page.text}}</a></li>\n  <li ng-if="directionLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(page + 1)">{{getText(\'next\')}}</a></li>\n  <li ng-if="boundaryLinks" ng-class="{disabled: noNext()}"><a href ng-click="selectPage(totalPages)">{{getText(\'last\')}}</a></li>\n</ul>')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-html-unsafe-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(a){a.put("template/tooltip/tooltip-popup.html",'<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(a){a.put("template/popover/popover.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/bar.html",'<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/progress.html",'<div class="progress" ng-transclude></div>')}]),angular.module("template/progressbar/progressbar.html",[]).run(["$templateCache",function(a){a.put("template/progressbar/progressbar.html",'<div class="progress">\n  <div class="progress-bar" ng-class="type && \'progress-bar-\' + type" role="progressbar" aria-valuenow="{{value}}" aria-valuemin="0" aria-valuemax="{{max}}" ng-style="{width: percent + \'%\'}" aria-valuetext="{{percent | number:0}}%" ng-transclude></div>\n</div>')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(a){a.put("template/rating/rating.html",'<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="{{range.length}}" aria-valuenow="{{value}}">\n    <i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < value && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')">\n        <span class="sr-only">({{ $index < value ? \'*\' : \' \' }})</span>\n    </i>\n</span>')}]),angular.module("template/tabs/tab.html",[]).run(["$templateCache",function(a){a.put("template/tabs/tab.html",'<li ng-class="{active: active, disabled: disabled}">\n  <a href ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')}]),angular.module("template/tabs/tabset.html",[]).run(["$templateCache",function(a){a.put("template/tabs/tabset.html",'<div>\n  <ul class="nav nav-{{type || \'tabs\'}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n')}]),angular.module("template/timepicker/timepicker.html",[]).run(["$templateCache",function(a){a.put("template/timepicker/timepicker.html",'<table>\n	<tbody>\n		<tr class="text-center">\n			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n		<tr>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n				<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td>:</td>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n				<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n		</tr>\n		<tr class="text-center">\n			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n	</tbody>\n</table>\n')}]),angular.module("template/typeahead/typeahead-match.html",[]).run(["$templateCache",function(a){a.put("template/typeahead/typeahead-match.html",'<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')
}]),angular.module("template/typeahead/typeahead-popup.html",[]).run(["$templateCache",function(a){a.put("template/typeahead/typeahead-popup.html",'<ul class="dropdown-menu" ng-show="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\'}" style="display: block;" role="listbox" aria-hidden="{{!isOpen()}}">\n    <li ng-repeat="match in matches track by $index" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)" role="option" id="{{match.id}}">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n')}]);
(function(){
/*jshint strict:false */
"use strict";





angular.module("Sleeping",['GothCon','Filters','ngDialog','HttpDataProviders'])
	.directive("accommodationSelection", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,People){
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        People.availableAccommodations(newVal,function(data){
                            
							$scope.availableAccommodations = data.data;
							for(var key in $scope.availableAccommodations){
								if($scope.availableAccommodations[key].isSelected){
									$scope.selectedSleepingResourceId = $scope.availableAccommodations[key].sleepingResourceId;
								}
							}
                        });
					}
					else{
						$scope.eventtypes = [];
					}
				});
				
				$scope.selectAccommodation = function(id){
					$scope.selectedSleepingResourceId = id;
				};
				
				$scope.selectedSleepingResourceId = "-1";
				$scope.saveAccommodation = function(){
					
					People.saveAccommodation($scope.currentPerson.id,$scope.selectedSleepingResourceId,function(){
						
					});
				};
            },
			scope : {
				currentPerson : "=",
			},
			replace : true,
            templateUrl : "directives/accommodation/accommodation.tpl.html",
            restrict : 'AE'
        };
    });

angular.module("Signups",['GothCon','Filters','ngDialog','HttpDataProviders','Event'])
	.controller("SignupSlotController", function($scope){
		$scope.saveSignup = function(signupSlot){
			$scope.signupButtonIsDisabled = true;
			return $scope.__proto__.saveSignup(signupSlot);
		};
    })
	.directive("signups", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,SignupSlot,Signups){
				var onShowSignupDialogue = angular.noop;
				
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        Signups.getSignupsByPerson(newVal,function(data){
                            $scope.signups = data.data;
                        });
						$scope.saveSignup = savePersonalSignup;
						onShowSignupDialogue = getPersonalSignupSlots;
					}else{
						$scope.eventtypes = [];
					}
				});

				$scope.$watch(function(){ return (!$scope.currentGroup) ? undefined : $scope.currentGroup.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
						
                        Signups.getSignupsByGroup(newVal,function(data){
                            $scope.signups = data.data;
                        });
						$scope.saveSignup = saveGroupSignup;
						onShowSignupDialogue = getGroupSignupSlots;
					}else{
						$scope.eventtypes = [];
					}
				});
				
				$scope.deleteSignup = function (signup){
					if(confirm("Är du säker?")){
						Signups.deleteSignup(signup.id,function(){
							$scope.signups.splice($scope.signups.indexOf(signup),1);
						});
					}
				};
				
				
				function createSaveResponseHandler(signupSlot){
					return function onSaveResponse(response){
						$scope.signups.push(response.data);
						for(var etKey in $scope.eventtypes){
							// event type
							var eventType = $scope.eventtypes[etKey];
							for(var eKey in eventType.events){
								// event
								var event = eventType.events[eKey];
								for(var eoKey in event.eventOccations){
									// event occation
									var eventOccation = event.eventOccations[eoKey];
									var index = eventOccation.signupSlots.indexOf(signupSlot);
									if(index >= 0){
										eventOccation.signupSlots.splice(index, 1);
									}
									if(eventOccation.signupSlots.length === 0){
										event.eventOccations.splice(eoKey, 1);
										break;
									}
								}
								if(event.eventOccations.length === 0){
									eventType.events.splice(eKey, 1);
									break;
								}
							}
							if(eventType.events.length === 0){
								$scope.eventtypes.splice(etKey, 1);
								break;
							}
						}
					};
				}
				
				var savePersonalSignup = function(signupSlot){
					Signups.savePersonalSignup($scope.currentPerson.id, signupSlot.id,createSaveResponseHandler(signupSlot));
				};
				
				var saveGroupSignup = function(signupSlot){
					Signups.saveGroupSignup($scope.currentGroup.id, signupSlot.id, createSaveResponseHandler(signupSlot));
				};
				
				var getPersonalSignupSlots = function(){
					SignupSlot.getPersonalSignupSlots($scope.currentPerson.id,function(response){
						$scope.eventtypes = response.data;
					});
				};

				var getGroupSignupSlots = function(){
					SignupSlot.getGroupSignupSlots($scope.currentGroup.id,function(response){
						$scope.eventtypes = response.data;
					});
				};
				
				$scope.showSignupDialogue=function(){
					onShowSignupDialogue();
					ngDialog.open({
					   template : urls.getDirectivesUrl() + 'signups/signupDialogue.tpl.html',
					   className: 'ngdialog-theme-default',
					   scope : $scope
					});
				}; 
				
            },
			scope : {
				currentPerson : "=?",
				currentGroup : "=?"
			},
			replace : true,
            templateUrl : urls.getDirectivesUrl() + "signups/signups.tpl.html",
            restrict : 'AE'
        };
    });
	

}());
(function(){
/*jshint strict:false */
"use strict";

angular.module("Event",['GothCon','Filters','ngDialog','Signups','Resource','HttpDataProviders','ui.tinymce'])
	.provider("Event",function EventRepositoryProvider(){
	   return {
			$get : function EventRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var _resource = $resource( linkBuilder.createAdminLink("event/:id/:svc/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'occations'	: {method:'GET'	, interceptor : gcLoaderBusInterceptor, isArray:true, params : {svc : 'svc' , operation : 'occations'} },
					'createEventOccation'				: {method:'POST', interceptor : gcLoaderBusInterceptor, isArray:false, params : {svc : 'svc' ,operation : 'createEventOccation'} },
					'getResourceBookingScheduleData'	: {method:'GET', interceptor : gcLoaderBusInterceptor, isArray:false, params : {svc : 'svc' ,operation : 'getResourceBookingScheduleData'} },
					'formMetaData'						: {method:'GET'	, interceptor : gcLoaderBusInterceptor, params : {svc: "svc", operation : 'formMetadata'} }
				});
				return {
					query : function(callback){
						callback = callback || angular.noop;
						return _resource.query(callback,callback);
					},
					occations : function(eventId,callback){
						callback = callback || angular.noop;
						return _resource.occations({'id' : eventId} ,callback,callback);
					},
					createEventOccation : function(eventId, eventOccation, callback){
						callback = callback || angular.noop;
						return _resource.createEventOccation({'id' : eventId}, eventOccation ,callback,callback);
					},
					formMetaData : function(callback){
						callback = callback || angular.noop;
						return _resource.formMetaData({} ,callback,callback);
					},
					get : function(id,callback){
						callback = callback || angular.noop;
						return _resource.get({id : id},callback,callback);
					},
					save : function(event,callback){
						callback = callback || angular.noop;
						return _resource.save({id : event.id},event,callback,callback);
					},
					getResourceBookingScheduleData : function(callback){
						return _resource.getResourceBookingScheduleData(callback,callback);
					}
				};
			}
	   } ;
	})
	.provider("EventOccation",function EventRepositoryProvider(){
	   return {
			$get : function EventRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var _resource = $resource( linkBuilder.createAdminLink("eventOccation/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'scheduleData' : {method:'GET', interceptor : gcLoaderBusInterceptor }
				});
				return {
					query : function(callback){
						callback = callback || angular.noop;
						return _resource.query(callback,callback);
					},
					get : function(id,callback){
						callback = callback || angular.noop;
						return _resource.get({id : id},callback,callback);
					},
					'delete' : function(id,callback){
						callback = callback || angular.noop;
						return _resource.delete({id : id},callback,callback);
					},
					save : function(eventOccation,callback){
						callback = callback || angular.noop;
						return _resource.save({id : eventOccation.id},eventOccation,callback,callback);
					},
					scheduleData : function(callback){
						callback = callback || angular.noop;
						return _resource.scheduleData({},callback,callback);
					}
				};
			}
	   } ;
	})
	.provider("SignupSlot", function SignupSlotsRepositoryProvider(){
        var _resource;
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus,linkBuilder,gcLoaderBusInterceptor){
				var standardSignupSlotLink = linkBuilder.createAdminLink("signupSlot");
				var standardPersonLink = linkBuilder.createAdminLink("person");
				
                _resource = $resource(standardSignupSlotLink + "/:id",{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} },
                    'all' : { method : "GET", isArray : true}
                });
				
                return {
                    'getPersonalSignupSlots' : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardSignupSlotLink + "?person_id=:personId",{}).get({ personId: id}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					'getGroupSignupSlots' : function(groupId,callback){
						gcLoaderBus.add();
                        $resource(standardSignupSlotLink + "?group_id=:groupId",{}).get({ groupId: groupId}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					'save' : _resource.save,
					'delete' : function(signupSlotId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id: signupSlotId}, callback );
					}
                };
            }
        };
    })
	.provider("EventOccationBooking", function SignupSlotsRepositoryProvider(){
        var _resource;
		
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus,linkBuilder,gcLoaderBusInterceptor){
                _resource = $resource(linkBuilder.createAdminLink("eventOccationBooking") + "/:id",{},{	
					'get'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'createMany'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} }
                });
				
                return {
					'create'	: _resource.save,
					'createMany': function(dataArray,callback){ return _resource.createMany({},dataArray,callback,callback);},
					'delete'	: _resource.delete
                };
            }
        };
    })
	.directive("eventList",function($templateCache){
		return {
			controller : function($scope){

			},
			scope : {
				events : "="
			},
			replace : true,
			templateUrl : "event/templates/list.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventOccationSchedule",function($templateCache,EventOccation,ngDialog){
		return {
			controller : function($scope,Event){
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				var inSelectionMode = false;
				var selectedElements = [];
				
				var conventionStartsAt;
				var conventionEndsAt;
				
				$scope.enterSelectionMode = function($event){
					for(var key in selectedElements){
						angular.element(selectedElements[key]).removeClass("selected");
						
					}
					selectedElements.length = 0;
					$event.stopPropagation();
					$event.preventDefault();
					console.log("entering selection mode");
					inSelectionMode = true;
					$scope.select($event);
				};
				$scope.select = function($event){
					$event.stopPropagation();
					$event.preventDefault();
					if(inSelectionMode){
						console.log("selecting");
						selectedElements.push($event.target);
						console.log(selectedElements);
						angular.element($event.target).addClass("selected");
					}
				};
				$scope.leaveSelectionMode = function($event){
					$event.stopPropagation();
					$event.preventDefault();					
					inSelectionMode = false;
					console.log(selectedElements);
				};
				
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					
					
					
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addEvents(eventTypes,conventionStartsAt){
					var eventTypeBoxes = [];
					for(var key in eventTypes){
						
						var eventType = eventTypes[key];
						var eventTypeBox = {
							name : eventType.name,
							id : eventType.id,
							events : []
						};
						for(var eventKey in eventType.events){
							var event = eventType.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.eventOccations){
								var eventOccation = event.eventOccations[occationKey];
								if(eventOccation.timespan){
									var occationBox = {
										name : eventOccation.name || eventOccation.timespan.name,
										id : eventOccation.id,
										width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
										offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
									};
									eventBox.eventOccations.push(occationBox);
								}
							}
							eventTypeBox.events.push(eventBox);
						}
						eventTypeBoxes.push(eventTypeBox);
					}
					$scope.eventTypes = eventTypeBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				EventOccation.scheduleData(function(response){
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.data.publicTimespans;
					var eventTypes = response.data.data.events;
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);
						
						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addEvents(eventTypes,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
					
					
					
				});
				
				$scope.deleteEventOccation = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort passet? Alla anmälningar och resursbokningar kommer att försvinna.")){
						EventOccation.delete(eventOccationBox.id,function(response){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};
				
				
				
				$scope.createEventOccation = function($event,event,timespan){
					$event.preventDefault();
					var scope = $scope.$new(true);
					scope.timespan = timespan;
					scope.event = event;
					
					ngDialog.open({
						template : '<div><event-occation-dialogue event="event" timespan="timespan"></event-occation-dialogue></div>',
						plain:	true,
						className: 'ngdialog-theme-default',
						scope: scope
					});
					
//					if(confirm("Vill du skapa ett arrangemangspass här?")){
//						Event.createEventOccation(event.id,{id:0, isHidden:false, name:"",timespan: {id:timespan.id, isPublic: true} },function(response){
//							var eventOccation = response.data.data;
//							var occationBox = {
//								name : eventOccation.name || eventOccation.timespan.name,
//								id : eventOccation.id,
//								width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
//								offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
//							};
//							event.eventOccations.push(occationBox);
//							$scope.showNewForm = false;							
//						});
//					}
				};
			},
			scope : {
				
			},
			replace : true,
			templateUrl : "event/templates/eventOccationSchedule.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventDetails",function($templateCache){
		return {
			controller : function($scope,People,GroupDataProvider,Event,$location){
				$scope.selectOrganizer = function(selection){
					if($scope.currentEvent.organizer){
						$scope.currentEvent.organizer.group = null;
						$scope.currentEvent.organizer.person = null;
					}else{
						$scope.currentEvent.organizer = { 
							id : 0, 
							person:null, 
							group:null 
						};
					}
					
					if(selection.length && selection[0].type ==="person"){
						People.get(selection[0].id,function(response){
							$scope.currentEvent.organizer.person = {
								firstName: response.data.firstName, 
								lastName:response.data.lastName, 
								id: response.data.id
							};
						});
					}
					if(selection.length && selection[0].type ==="group"){
						GroupDataProvider.getById(selection[0].id).then(function(response){
							$scope.currentEvent.organizer.group = {
								name: response.name, 
								id: response.id
							};
						});
					}
				};
				$scope.save = function(){
					var event =  {};
					angular.copy($scope.currentEvent,event);
					event.eventOccations = undefined;
					if(event.organizer){
						if(event.organizer.person){
							event.organizer.personId = event.organizer.person.id;
							event.organizer.person = undefined;
						}
						if(event.organizer.group){
							event.organizer.groupId = event.organizer.group.id;
							event.organizer.group = undefined;
						}
						
					}
					if(event.eventtype){
						event.eventtypeId = event.eventtype.id;
						event.eventtype = undefined;
					}
					Event.save(event,function(response){
						$scope.currentEvent = response.data.data;
						if($scope.currentEvent.id){
							$location.path("event/" + $scope.currentEvent.id);
						}
					});
				};
				$scope.selectOrganizerLabel = ($scope.currentEvent && $scope.currentEvent.id) ? "ändra..." : "välj arrangör...";
				$scope.hasOrganizer = function(){
					return $scope.currentEvent && $scope.currentEvent.organizer && ($scope.currentEvent.organizer.person || $scope.currentEvent.organizer.group);
				};
			},
			scope : {
				currentEvent : "=",
				eventTypes : "="
			},
			replace : true,
			templateUrl : "event/templates/details.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventOccation",function($templateCache){
		return {
			controller : function($scope,EventOccation,Resource,SignupSlot,EventOccationBooking){
				$scope.showEditForm = false;
				$scope.showNewForm = false;
				
				$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
				
				$scope.$on("eventOccationFormEvent",function(event,args){
					if(args.type && args.type === "cancelClicked"){
						$scope.showEditForm = false;
						event.preventDefault();
					}					
					if(args.type && args.type === "eventOccationUpdate"){
						EventOccation.save(args.eventOccation,function(response){
							$scope.eventOccation = response.data.data;
							event.preventDefault();
							$scope.showEditForm = false;							
						});
					}
				});
				
				$scope.deleteResourceBooking = function (eventOccationBooking){
					if(confirm("Är du säker?")){
						var index = $scope.eventOccation.eventOccationBookings.indexOf(eventOccationBooking);
						Resource.deleteOccationBooking(eventOccationBooking.booking.id, function(){
							$scope.eventOccation.eventOccationBookings.splice(index,1);
						});
					}
				};
				
				$scope.saveResourceBooking = function (entities){
					var data = [];
					for(var key in entities){
						data.push({resourceId : entities[key].id, eventOccationId : $scope.eventOccation.id});
					}
					EventOccationBooking.createMany(data,function(response){
						for(var key in response.data.data){
							$scope.eventOccation.eventOccationBookings.push(response.data.data[key]);
						}
					});
				};
				
				$scope.$on("signupSlotFormEvent",function(event,args){
					if(args.type && args.type === "signupSlotCreate"){
						SignupSlot.save(args.signupSlot,function(response){
							$scope.eventOccation.signupSlots.push(response.data.data);
						});
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
					}
					if(args.type && args.type === "signupSlotDelete"){
						SignupSlot.delete(args.signupSlot.id,function(response){
							var index = $scope.eventOccation.signupSlots.indexOf(args.signupSlot);
							$scope.eventOccation.signupSlots.splice(index,1);
						});
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
					}
					if(args.type === "cancelClicked"){
						$scope.signupSlotFormData = { slotType : null, isHidden : false, id : 0, requiresApproval: false, eventOccation : { id: $scope.eventOccation.id} };
						$scope.showNewForm = false;
						event.preventDefault();
					}
					event.preventDefault();
				});
			},
			scope : {
				eventOccation : "=",
				timespans :"=",
				slotTypes :"="
			},
			replace : true,
			templateUrl : "event/templates/eventOccation.tpl.html",
			restrict : 'E'
		};
	})
	.directive("eventOccationForm",function($templateCache){
		return {
			controller : function($scope,EventOccation){
				var backup;
				$scope.localTimespans = angular.copy($scope.timespans);
				var customTimespan = {startsAt: "", endsAt: "" , name: "Anpassatt", isPublic: false, showInSchedule: true, id:0};
				
				if($scope.eventOccation){
					backup = angular.copy($scope.eventOccation);
					
					if($scope.eventOccation.timespan && $scope.eventOccation.timespan.isPublic === false){
						customTimespan = $scope.eventOccation.timespan;
						$scope.startsAt = new Date(moment(customTimespan.startsAt).format("YYYY-MM-DD HH:mm"));
						$scope.endsAt = new Date(moment(customTimespan.endsAt).format("YYYY-MM-DD HH:mm"));
						$scope.showInSchedule = customTimespan.showInSchedule;
					}
					
					
					$scope.SaveLabel = "spara";
					$scope.onSaveClick = function(){
						if(!$scope.eventOccation.timespan.isPublic){
							$scope.eventOccation.timespan.startsAt =  moment($scope.startsAt).format("YYYY-MM-DD HH:mm");
							$scope.eventOccation.timespan.endsAt =  moment($scope.endsAt).format("YYYY-MM-DD HH:mm");
							$scope.eventOccation.timespan.showInSchedule = $scope.showInSchedule;
							$scope.eventOccation.timespan.name = "Anpassat";
						}
						$scope.$emit("eventOccationFormEvent", { type:"eventOccationUpdate", eventOccation : $scope.eventOccation });
						$scope.eventOccationForm.$setPristine();
					};
					$scope.onDeleteClick = function(){
						if(confirm("Är du säker?")){
							$scope.$emit("eventOccationFormEvent", { type:"eventOccationDelete", eventOccation : $scope.eventOccation });
							$scope.eventOccationForm.$setPristine();
						}
					};
				}
				else{
					$scope.eventOccation = { name : "", timespan : null, isHidden : false, id : 0, signupSlots : [] };
					backup = angular.copy($scope.eventOccation);
					$scope.SaveLabel = "skapa";
					$scope.onSaveClick = function(){
						var newOccation = angular.copy($scope.eventOccation);
						if(!newOccation.timespan.isPublic)  {
							// custom form
							newOccation.timespan = {
								startAt : moment($scope.startsAt).format("YYYY-MM-DD HH:mm"),
								endsAt : moment($scope.endsAt).format("YYYY-MM-DD HH:mm"),
								isPublic: false,
								showInSchedule : $scope.showInSchedule,
								name : "Anpassat"
							};
							
						}
						$scope.$emit("eventOccationFormEvent", { type:"eventOccationCreate", eventOccation : newOccation });
						angular.copy(backup,$scope.eventOccation);
						$scope.eventOccationForm.$setPristine();							
					};
				}
				$scope.localTimespans.splice(0,0,customTimespan);
				$scope.onCancelClick = function(){
					angular.copy(backup,$scope.eventOccation);
					$scope.eventOccationForm.$setPristine();
					$scope.$emit("eventOccationFormEvent", { type:"cancelClicked" });
				};
				$scope.eventOccation.isHidden = $scope.eventOccation.isHidden ? true : false;
				
				
			},
			scope : {
				eventOccation : "=?",
				timespans :"="
			},
			replace : true,
			templateUrl : "event/templates/eventOccationForm.tpl.html",
			restrict : 'E'
		};
	})
	.directive("signupSlot",function($templateCache){
		return {
			controller : function($scope,Signups,SignupSlot){
				$scope.toggleEdit = function($event){ $event.preventDefault();};
				$scope.toggleNewForm = function($event){ $event.preventDefault();};
				$scope.showEditForm = false;
				$scope.$on("signupSlotFormEvent",function(event,args){
					if(args.type === "cancelClicked"){
						$scope.showEditForm = false;
						event.preventDefault();
					}
					else if(args.type === "signupSlotUpdate"){
						SignupSlot.save($scope.signupSlot,function(response){
							$scope.signupSlot = response.data.data;
							$scope.showEditForm = false;
						});
						event.preventDefault();
					}
				});
				$scope.deleteSignup = function (signup){
					if(confirm("Är du säker?")){
						Signups.deleteSignup(signup.id,function(){
							var index = $scope.signupSlot.signups.indexOf(signup);
							$scope.signupSlot.signups.splice(index,1);
						});
					}
				};
				
				var savePersonalSignup = function(entities){
					Signups.savePersonalSignup(entities[0].id, $scope.signupSlot.id,function(response){
						$scope.signupSlot.signups.push(response.data);
					});
				};
				
				var saveGroupSignup = function(entities){
					Signups.saveGroupSignup(entities[0].id, $scope.signupSlot.id, function(response){
						$scope.signupSlot.signups.push(response.data);
					});
				};
				
				$scope.selectionScope = $scope.signupSlot.slotType.cardinality > 1 ? "groups" : "people";
				$scope.selectorHeader = "Sök efter " + $scope.signupSlot.slotType.name.toLowerCase()+ ".";
				$scope.onSelectEntity = function(entities){
					if($scope.signupSlot.slotType.cardinality > 1){
						saveGroupSignup(entities);
					}else{
						savePersonalSignup(entities);
					}
				}; 
			},
			scope : {
				signupSlot : "=",
				slotTypes :"="	
			},
			replace : true,
			templateUrl : "event/templates/signupSlot.tpl.html",
			restrict : 'E'
		};
	})
	.directive("signupSlotForm",function($templateCache){
		return {
			controller : function($scope){
				var backup;
				if($scope.signupSlot.id){
					$scope.saveLabel = "spara";
					$scope.onSaveClick = function(){
						$scope.$emit("signupSlotFormEvent", { type:"signupSlotUpdate", signupSlot : $scope.signupSlot });
						$scope.signupSlotForm.$setPristine();
					};
					$scope.onDeleteClick = function(){
						if(confirm("Är du säker?")){
							$scope.$emit("signupSlotFormEvent", { type:"signupSlotDelete", signupSlot : $scope.signupSlot });
						}
					};
				}else{
					$scope.saveLabel = "skapa";
					$scope.onSaveClick = function(){
						$scope.$emit("signupSlotFormEvent", { type:"signupSlotCreate", signupSlot : $scope.signupSlot });
						$scope.signupSlot = backup;
						$scope.signupSlotForm.$setPristine();
					};
				}
				backup = angular.copy($scope.signupSlot);
				
				$scope.onCancelClick = function(){
					$scope.signupSlot = backup;
					$scope.signupSlotForm.$setPristine();
					$scope.$emit("signupSlotFormEvent", { type:"cancelClicked" });
				};
				
			},
			scope : {
				slotTypes :"=",
				signupSlot : "=",
			},
			replace : true,
			templateUrl : "event/templates/signupSlotForm.tpl.html",
			restrict : 'E'
		};
	})
	.directive("eventSignups",function($templateCache){
		return {
			controller : function($scope,Event,EventOccation){
				$scope.showNewForm = $scope.currentEvent.eventOccations.length === 0;
				$scope.$on("eventOccationFormEvent",function(event,args){
					if(args.type && args.type === "eventOccationCreate"){
						Event.createEventOccation($scope.currentEvent.id,args.eventOccation,function(response){
							$scope.currentEvent.eventOccations.push(response.data.data);
							event.preventDefault();
							$scope.showNewForm = false;							
						});
					}else if(args.type && args.type === "cancelClicked"){
						$scope.showNewForm = false;
						event.preventDefault();
					}else if(args.type && args.type === "eventOccationDelete"){
						EventOccation.delete(args.eventOccation.id,function(response){
							var index = $scope.currentEvent.eventOccations.indexOf(args.eventOccation);
							$scope.currentEvent.eventOccations.splice(index,1);
							event.preventDefault();
						});
					}
				});
			},
			scope : {
				currentEvent : "=",
				timespans :"=",
				slotTypes :"="
			},
			replace : true,
			templateUrl : "event/templates/signups.tpl.html",
			restrict : 'AE'
		};
	})
	.directive("eventBookingsSchedule",function(urls){
		return {
			controller : function($scope,Event,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addBookings(events,conventionStartsAt){
					var eventBoxes = [];

					angular.forEach(events,function(event){
						var eventBox = {
							name : event.name,
							id : event.id,
							resources : []
						};
						
						// add resources to the event box
						angular.forEach(event.resources,function(resource){
							
							// create a resource
							var resourceBox = {
								name : resource.name,
								id: resource.id,
								bookings : []
							};
							// add bookings to the resource
							angular.forEach(resource.bookings,function(booking){
								resourceBox.bookings.push({
									name : booking.name,
									id : booking.id,
									width: getOccationWidth(booking.startsAt,booking.endsAt),
									offset: getOccationOffset(booking.startsAt,conventionStartsAt)
								});
							});
							// add the resource to the event
							eventBox.resources.push(resourceBox);
						});
						// add the event box to the main event list
						eventBoxes.push(eventBox);
					});
					$scope.events = eventBoxes;
				}
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				Event.getResourceBookingScheduleData(function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.data.publicTimespans;
					var events = response.data.data.events;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addBookings(events,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "event/templates/eventBookingsSchedule.tpl.html",
            restrict : 'E'
		};
	})
	.directive("eventOccationDialogue",function(){
		return {
			controller : function($scope){
				console.log($scope.event);
				console.log($scope.timespan);
			},
			scope : {
				timespan : "=",
				event : "="
			},
			replace : false,
			templateUrl : "event/templates/eventOccationDialogue.tpl.html",
			restrict : 'E'
		};
	})
	
	.directive("event",function(){
		var extractIdRegex = /^\/event\/(\d+|new|bookingSchedule)(\.|\/|$)/;

		var signupsRegex = /^\/event\/(\d+)([^/]*)\/signups$/;	
		var deleteRegex = /^\/event\/(\d+)([^/]*)\/signups$/;	

		var initialData;
		return {
			controller : function($scope,$location,gcTabsBus,Event,$element,$compile){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.formMetaData = {
						eventTypes : [],
						slotTypes : [],
						timespans : []
					};
					Event.formMetaData(function(response){
						$scope.formMetaData.eventTypes = response.data.data.eventTypes;
						$scope.formMetaData.slotTypes = response.data.data.slotTypes;
						$scope.formMetaData.timespans = response.data.data.timespans;
					});
					
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
					
					$scope.$on('$locationChangeSuccess',onLocationChange);

				}

				function onLocationChange(){
					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						onShowNewForm();
						return;
					}
					else if(id === "bookingSchedule")
					{
						onShowBookingSchedule();
						return;
					}
					else{
						onShowDetails(id);
						return;
					}

				}

				function onShowBookingSchedule(){
					gcTabsBus.setTabs([]);
					$scope.title = "Arrangemangslista";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);					
					$scope.eventList = [];
					$scope.currentEvent = undefined;
					$scope.view = "<event-bookings-schedule></event-bookings-schedule>";
					return;
				}
				
				function onShowList(){
					gcTabsBus.setTabs([]);
					$scope.title = "Arrangemangslista";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);					
					Event.query(
						function(response){
							$scope.eventList = response.data.data;
							$scope.currentEvent = undefined;
							$scope.view = "<event-list events='eventList'></event-list>";
						}
					);
					return;
				}

				function onShowNewForm(){
					$scope.currentEvent = {};
					$scope.title = "Nytt arrangemang";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/bookingSchedule', 'label' : 'Bokningsöversikt'},
						{ id : 'event/new', 'label' : 'Nytt arrangemang'}
					]);
					$scope.view = "<event-details current-event='currentEvent' event-types='formMetaData.eventTypes'/>";
				}

				function onShowDetails(id){
					var newView = "";
					gcTabsBus.setTabs([
						{ id : 'event/', 'label' : 'Arrangemangslista'},
						{ id : 'event/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'event/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'}, 
						{ id : 'event/' + id + '/delete', 'label' : 'Ta  bort'}]);
					switch(true){
						case signupsRegex.test($location.path()) :
							newView = "<event-signups current-event='currentEvent' timespans='formMetaData.timespans' slot-types='formMetaData.slotTypes'/>";
							break;
						case deleteRegex.test($location.path()):
							newView = "<event-delete current-event='currentEvent'/>";
							break;
						default:
							newView = "<event-details current-event='currentEvent' event-types='formMetaData.eventTypes'/>";
					}

					if(!$scope.currentEvent || "" + $scope.currentEvent.id !== "" + id){
						$scope.title = "Hämtar detaljer för arrangemang # " + id + "...";
						Event.get(id,function(response){
							$scope.currentEvent = response.data.data;
							$scope.title = "Detaljer för " + $scope.currentEvent.name;
							$scope.view = newView;
						});
					}else {
						if($scope.currentEvent){
							$scope.title = "Detaljer för " + $scope.currentEvent.name;
						}
						$scope.view = newView;
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "event/templates/event.tpl.html",
			restrict : 'E'				
		};
	});
}());
(function(){
/*jshint strict:false */


angular.module("EventType",['ngResource','GothCon','HttpDataProviders','Filters','ngDialog'])
		.provider("eventTypeRepository",function(){
			return {
				$get : function EventTypeRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
					var _resource = $resource( linkBuilder.createAdminLink("eventtype/:id/:svc/:operation?json"),{},{	
						'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
						'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor }
					});
					return {
						getList : function(callback){
							callback = callback || angular.noop;
							return _resource.query(callback,callback);
						},
						get : function(id,callback){
							callback = callback || angular.noop;
							return _resource.get({id : id},callback,callback);
						},
						save : function(eventtype,callback){
							callback = callback || angular.noop;
							return _resource.save({id : eventtype.id},eventtype,callback,callback);
						},
						create : function(eventtype,callback){
							callback = callback || angular.noop;
							return _resource.save({},eventtype,callback,callback);
						},
						delete : function(eventtypeId,callback){
							callback = callback || angular.noop;
							return _resource.delete({id : eventtypeId},callback,callback);
						}
					};
				}
			};
		})
		.controller("eventTypeItemController",function($scope,eventTypeRepository){
			console.log($scope);
			$scope.inEditMode = false;
			var backupValue = angular.copy($scope.eventtype);
			$scope.enterEditMode = function(){
				$scope.inEditMode = true;
			};
			$scope.leaveEditMode = function(){
				$scope.inEditMode = false;
			};			
			$scope.save = function(){
				eventTypeRepository.save($scope.eventtype,function(){
					backupValue = angular.copy($scope.eventtype);
					$scope.inEditMode = false;
				});
			};			
			$scope.cancel = function(){
				$scope.inEditMode = false;
				angular.copy(backupValue,$scope.eventtype);
			};
			$scope.remove = function(){
				if(confirm("Är du säker på att du vill ta bort arrangemangstypen?")){
					eventTypeRepository.delete($scope.eventtype.id,function(){
						var index = $scope.eventtypes.indexOf($scope.eventtype);
						$scope.eventtypes.splice(index,1);
						$scope.inEditMode = false;
					});
				}
			};
			$scope.create = function(){
				eventTypeRepository.create($scope.eventtype,function(response){
					$scope.eventtypes.push(response.data.data);
					angular.copy(backupValue,$scope.eventtype);
				});
			};
		})
		.directive("eventTypeList",function(){
		return {
			controller : function ($scope,eventTypeRepository){
				$scope.save = function(){};
				$scope.delete = function(){};
				$scope.title = "Arrangemangstyper";
				eventTypeRepository.getList(function(response){
					$scope.eventtypes = response.data.data;
				});
				$scope.eventtypes = [];
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "eventtypes/templates/list.tpl.html",
			restrict : 'E'			
		};
	});
}());
(function(){
/*jshint strict:false */
"use strict";

angular.module("Filters",['GothCon'])
	.filter('gcDate',function($filter){
		return function(input){
		    var date = moment(input.date ? input.date : input);
			var dateFilter = $filter("amDateFormat");
			return dateFilter(date,"dd D/M HH:mm");
		};
	})
	.filter('timespanLabel', function($filter) {
        return function(input,includeName) {
			if(input.startsAt && input.endsAt){
				var startsAt = moment(input.startsAt.date ? input.startsAt.date : input.startsAt );
				var endsAt = moment(input.endsAt.date ? input.endsAt.date : input.endsAt);
				var formatStart;
				var formatEnd;
				if(endsAt.year() !== startsAt.year()){
					// olika år
					formatStart = "dd D/M YYYY HH:mm";
					formatEnd = "dd D/M YYYY HH:mm";
				}
				else if(endsAt.month() !== startsAt.month()){
					// olika månader
					formatStart = "dd D/M HH:mm";
					formatEnd = "dd D/M HH:mm";
				}
				else if(endsAt.date() !== startsAt.date()){
					// olika dagar
					formatStart = "dd D/M HH:mm";
					formatEnd = "dd D/M  HH:mm";
				}
				else{
					// olika timmar
					formatStart = "dd D/M HH:mm";
					formatEnd = "HH:mm";
				}
				var dateFilter = $filter("amDateFormat");
				if(includeName && input.name){
					return input.name + " (" + dateFilter(startsAt,formatStart) + " - " + dateFilter(endsAt,formatEnd) + ")";
				}else{
					return dateFilter(startsAt,formatStart) + " - " + dateFilter(endsAt,formatEnd);
				}
			}else{
				return input.name;
			}
        };
    })
    .filter('occationLabel', function(){
        return function(eventOccation){
            return eventOccation.event.name + (eventOccation.name ? " - " + eventOccation.name : "");
        };
    })
	.filter('yesNoStatus', function(){
        return function(trueOrFalse,isApplicable){
			if(isApplicable !== undefined && !isApplicable){
				return "n/a";
			}
            return trueOrFalse ? "ja" : "nej";
        };
    })
	.filter('sleepingResourceStatus', function(){
        return function(resource){
			var sleepingResource = resource[0].sleepingResource;
			if(!sleepingResource  || !sleepingResource.isAvailable){
				return "nej";
			}
			else{
				var availableSlots = sleepingResource.noOfSlots - resource.sleepingSlotBookings;
				return "Ja, "  + availableSlots + " av "  +sleepingResource.noOfSlots + " platser är lediga"; 
			}
        };
    })
    .filter('orderStatus', function(){
        return function(statusInEnglish){
			switch(statusInEnglish){
				case 'cart':
					return 'öppen';
				case 'submitted':
					return 'beställd';
				case 'partially_paid':
					return 'delvis betald';
				case 'paid':
					return 'betald';
				case 'cancelled':
					return 'makulerad';
				case 'partially_delivered':
					return 'delvis levererad/utlämnad';
				case 'delivered':
					return 'levererad/utlämnad';
				default:
					return statusInEnglish;
			}
        };
    })
	.filter('signupStatus', function(){
        return function(signup){
			var statusParts = [];
			if(signup.signupOrderNumber){
				statusParts.push("Anmäld som nummer "+ signup.signupOrderNumber);
			}

			if(signup.signupSlot.requiresApproval){
				statusParts.push("behöver godkännas");
			}
			if(signup.signupSlot.signupSlotArticle){
				statusParts.push("behöver betalas");
			}
			
            var status =  statusParts.join(', ');
			return status + ".";
        };
    })
    .filter('eventOccationLabel', function($filter){
        return function(eventOccation, format){
            var label = "";
			if(format === undefined){
				var timespanFilter = $filter("timespanLabel");
				var timespan = eventOccation.timespan;
				if(!timespan){
					return "no timespan selected";
				}
				label = timespan.name ? timespan.name + " (" + timespanFilter(timespan) + ")" : timespanFilter(timespan);

				if(eventOccation.name){
					label += " - " + eventOccation.name;
				}
			}else if(format === "event"){
				label = eventOccation.timespan.name;
				if(eventOccation.name && eventOccation.name.length > 0){
					label += " : " + eventOccation.name;
				}
			}
            return label;
        };
    })
	.filter('personLabel', function(){
        return function(person){
			if(!person){
				return "";
			}
			return(person.firstName + " " + person.lastName);
        };
    })
	.filter('resourceLabel', function(){
        return function(resource){
			if(!resource){
				return "";
			}
			return(resource.id + ". " + resource.name + " (" + resource.resourceType.name + ")");
        };
    })
	.filter('resourceLabel', function(){
        return function(resource){
			if(!resource){
				return "";
			}
			return(resource.name + " (" + resource.resourceType.name + ")");
        };
    })
	.filter('resourceLink', function(linkBuilder){
        return function(resource){
			if(!resource){
				return "#";
			}
			return linkBuilder.createAdminLink("resource/" + resource.id);
        };
    })
	.filter('groupLabel', function(){
        return function(group){
			if(!group){
				return "";
			}
			return(group.id + ". " + group.name);
        };
    })
	.filter('eventOrganizerLabel', function($filter){
        return function(eventOrganizer){
			if(!eventOrganizer){
				return "ingen vald";
			}
			if(eventOrganizer.person){
				var labelFilter = $filter("personLabel");
				return labelFilter(eventOrganizer.person);
			}
			if(eventOrganizer.group){
				return eventOrganizer.group.id + ". " + eventOrganizer.group.name;
			}
			return "ingen vald";
        };
    })	
	.filter('eventOrganizerLink', function($filter){
        return function(eventOrganizer){
			var linkFilter;
			if(!eventOrganizer){
				return "#";
			}
			if(eventOrganizer.person){
				linkFilter = $filter("personLink");
				return linkFilter(eventOrganizer.person.id);
			}
			if(eventOrganizer.group){
				linkFilter = $filter("groupLink");
				return linkFilter(eventOrganizer.group.id);
			}
			return "#";
        };
    })
	.filter('signupSlotInfo', function(){
        return function(signupSlot){
			if(!signupSlot){
				return "";
			}
			var details = [];
			
			if(signupSlot.maximumSignupCount && signupSlot.maximumSignupCount >= 0){
				details.push(signupSlot.maximumSignupCount + " platser");
			}
			if(signupSlot.maximumSpareSignupCount && signupSlot.maximumSpareSignupCount >= 0){
				details.push(signupSlot.maximumSpareSignupCount + " reservplatser");
			}
			if(signupSlot.isHidden){
				details.push("Dold anmälningsmöjlighet");
			}
			if(signupSlot.requiresApproval){
				details.push("Kräver godkännande");
			}
			if(details.length){
				details.push("");
			}
			return details.join(". ");
        };
    })
	.filter('eventOccationInfo', function(){
        return function(eventOccation){
			if(!eventOccation){
				return "";
			}
			var details = [];
			
			if(eventOccation.isHidden){
				details.push("Dolt pass");
			}
			if(details.length){
				details.push("");
			}
			return details.join(". ");
        };
    })
	.filter('searchLabel', function(){
        return function(item){
			return(item.id + ". " + item.label);
        };
    })		
	.filter('personLink', function(linkBuilder){
        return function(personId){
			return linkBuilder.createAdminLink("person/" + personId);
        };
    })
	.filter('eventLink', function(linkBuilder){
        return function(eventId){
			return linkBuilder.createAdminLink("event/" + eventId);
        };
    })	
	.filter('eventTypeLink', function(linkBuilder){
        return function(eventTypeId){
			return linkBuilder.createAdminLink("eventtype/" + eventTypeId);
        };
    })
	.filter('registrationLink', function(linkBuilder){
        return function(personId){
			return linkBuilder.createAdminLink("ankomst/" + personId);
        };
    })
	.filter('groupLink',function(linkBuilder){
		return function(groupId){
			return linkBuilder.createAdminLink("group/" + groupId);
		};
	})
	.filter('timespanLink',function(linkBuilder){
		return function(timespanId){
			return linkBuilder.createAdminLink("timespan/" + timespanId);
		};
	})
	.filter('membershipLink',function(linkBuilder){
		return function(groupMembershipId){
			return linkBuilder.createAdminLink("groupmember/" + groupMembershipId);
		};
	})
	.filter('newOrderLink',function(linkBuilder){
		return function(personId){
			return linkBuilder.createAdminLink("order/ny/?p=" + personId);
		};
	})
	.filter('bookingType',function(){
		return function(bookingType){
			return bookingType;
		};
	})
	.filter('membershipStatus', function(){
        return function(membership){
			if(membership.isLeader){
				return "Gruppledare";
			}
			else{
				return "";
			}
        };
    });
}());
(function(){
/*jshint strict:false */
"use strict";
	
angular.module("GothCon",['angularMoment','ui.bootstrap','Person', 'Resource','Group','Event','EventType','Timespan','ui.bootstrap.datetimepicker'])
	.provider("urls", function urlsProvider(){
		var angularAppUrl 	=  "resources/javascripts/angular/app/";
		var templatesUrl 	=  "resources/javascripts/angular/app/templates/";
		var directivesUrl 	=  "resources/javascripts/angular/app/directives/";
		var applicationUrl 	=  "/webreg/";
		
        return{
			setApplicationUrl: function(url){
				applicationUrl = url;
			},			
			getApplicationUrl : function(){
				return applicationUrl;
			},	
			setAngularAppUrl: function(url){
				angularAppUrl = url;
			},			
			getAngularAppUrl : function(){
				return angularAppUrl;
			},	
			setTemplatesUrl : function(url){
				templatesUrl = url;
			},			
			getTemplatesUrl : function(){
				return applicationUrl + templatesUrl;
			},			
			setDirectivesUrl : function(url){
				directivesUrl = url;
			},			
			getDirectivesUrl : function(){
				return applicationUrl + directivesUrl;
			},
            $get : function urlsFactory(){
                return {
					getAngularAppUrl : function(){
						return angularAppUrl;
					},
					getApplicationUrl : function(){
						return applicationUrl;
					},
					getDirectivesUrl : function(){
						return applicationUrl + directivesUrl;
					},
					getTemplatesUrl : function(){
						return applicationUrl + templatesUrl;
					}
				};
            }
        };
    })
    .run(function(amMoment,urls) {
        amMoment.changeLanguage('sv');
		tinyMCE.baseURL = urls.getApplicationUrl() + 'resources/javascripts/library/tiny_mce4';
		tinyMCE.suffix = '.min';
		tinyMCE.plugins = "pagebreak,table,image,link,preview,contextmenu,advlist";
		tinyMCE.toolbar = "undo,redo,|,link,unlink,anchor,|,bold,italic,blockquote,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,|,table";
		tinyMCE.menubar = false;
    })
    .config(function($locationProvider){
        $locationProvider.html5Mode(true);
    })
    .directive("gcTabs", function(urls){
        return {
            controller : function($scope,gcTabsBus,$location){
                $scope.$watch(gcTabsBus.getTabs(),function(){
                   $scope.tabs = gcTabsBus.getTabs(); 
                });

                $scope.isTabActive = function(tab){
                    return  $location.path() === "/" + tab.id || $location.path()+ "/" ===  "/" + tab.id ;
                };
            },
            templateUrl : "directives/tabs/tabs.tpl.html",
            restrict : 'AE'
        };
    })
	.provider("gcTabsBus", function PeopleRepositoryProvider(){

        function Tab(_id,_label){
                this.id = _id;
                this.label = _label;
                return this;
            }    

        return{
            $get : function PeopleViewsFactory(){
                var _tabs = [];
                return{
                    getTabs : function(){
                        return _tabs;
                    },
                    setTabs : function(tabs){
                        _tabs.length = 0;
                        for(var key in tabs){
                            _tabs.push(new Tab(tabs[key].id, tabs[key].label));
                        }
                    }
                };
            }
        };
    })
    .provider("gcMessageBus", function gcMessageBusProvider(){
        return{
            $get : function gcMessageBus(){
                var _message = "";
                return{
                    getMessages : function(){
                        return [_message];
                    },
                    addMessage : function(message){
                        _message = message;
                    }
                };
            }
        };
    })
	.provider("linkBuilder", function LoaderBusRepository(urlsProvider){
        return {
            $get: function LoaderBusFactory(){
                return {
                    createAdminLink : function(path){
						return urlsProvider.getApplicationUrl() + "administrator/" + path;
                    },
                    createOrganizerLink : function(path){
						return urlsProvider.getApplicationUrl() + "organizer/" + path;
                    },
                    createPublicLink : function(path){
						return urlsProvider.getApplicationUrl() + path;
                    }
                };
            }
        };
    })
	.directive("gcMenu",function(){
		return {
			link : function(scope,elem){
				elem.find("a").attr("target", "_self");
			},
			controller : function(){
            },
			replace : false,
            restrict : 'A'
        };
	})
	.directive("sortableCollection",function($filter){
		var orderByFilter = $filter("orderBy");

		return {
			controller : function($scope){
				
				this.currentlySortedBy = "";
				this.reverseSort = false;
				
				this.sort = function(collectionName,sortBy){
					var properties = sortBy.split(",");
					if(sortBy === this.currentlySortedBy){
						this.reverseSort = !this.reverseSort;
					}else{
						this.reverseSort = false;
					}
					this.currentlySortedBy = sortBy;
				    var getSortValue = function(element){
						var values = [];
						for(var pkey in properties){
							var currentValue = element;
							var propertyPath = properties[pkey].split(".");
							for(var pathSegmentKey in propertyPath){
								var pathSegment = propertyPath[pathSegmentKey];
								currentValue = currentValue[pathSegment];
							}
							values.push(currentValue);
						}
						return (values.length === 1) ? values[0] : values.toString();
					};
					$scope[collectionName] = orderByFilter($scope[collectionName],getSortValue,this.reverseSort);
					$scope.$apply();
				};
            },
			
			replace : false,
            restrict : 'A'
        };
	})
	.directive("sortOnClick",function(){
		var regexp = /^([a-zA-Z]*) by ([a-zA-Z, _.]*)$/;
		return {
            link : function(scope,elem,attrs,controllerInstance){
				var splitParams = regexp.exec(attrs.sortOnClick);
				var collectionName = splitParams[1];
				var collectionSortBy = splitParams[2];
				
				elem.bind("click",function(){
					controllerInstance.sort(collectionName,collectionSortBy);
				});
				var arrowElement = angular.element("<i></i>");
				if(!elem.hasClass("clickable")){
					elem.attr("class", elem.attr("class") + " clickable");
				}
				elem.append(arrowElement);

				scope.isSortedByThisProperty = function(){
					return collectionSortBy === controllerInstance.currentlySortedBy;
				};
				
				scope.isSortedInReverse = function(){
				 	return controllerInstance.reverseSort;
				};
				
				function updateSortDirectionArrow(){
					if(scope.isSortedByThisProperty()){
						if(scope.isSortedInReverse()){
							arrowElement.attr("class","fa fa-arrow-up");
						}else{
							arrowElement.attr("class","fa fa-arrow-down");
						}
						arrowElement.show();
					}else{
						arrowElement.hide();
					}
				}
				
				scope.$watch(scope.isSortedByThisProperty, function(){
					updateSortDirectionArrow();
				});

				scope.$watch(scope.isSortedInReverse, function(){
					updateSortDirectionArrow();
				});

				
			},
			controller : function(){
				
            },
			scope : {},
			require : '^sortableCollection',
			replace : false,
            restrict : 'A'
        };
	})
	.directive('gcMustMatch',function(){
        return{
            require : 'ngModel',
            link: function(scope,elem,attr,ctrl){
                var theForm = elem[0].form;
                if(!theForm && elem.parent()){
                    var parent = elem.parent();
                    while(parent[0]){
                        if(parent.attr('ng-form') !== undefined || parent.hasClass('ng-form')){
                            theForm = scope[parent.attr("name")];
                            break;
                        }
                        parent = parent.parent();
                    }
                }else{
                    theForm = scope[angular.element(theForm).attr("name")];
                }
                if(!theForm){
                    throw("Cannot find the parent form");
                }

                var theOther;

                ctrl.$parsers.unshift(function(value) {
                    if(!theOther){
                        theOther = theForm[attr.gcMustMatch];
                    }
                    ctrl.$setValidity('match', theOther && value === theOther.$modelValue);
                    return value;
                });

                ctrl.$formatters.unshift(function(value) {
                    if(!theOther){
                        theOther = theForm[attr.gcMustMatch];
                    }
                    ctrl.$setValidity('match', theOther && value === theOther.$modelValue);
                    return value;
                });

                var addParserToOther = function(){
                        theOther = theForm[attr.gcMustMatch];
                        theOther.$parsers.unshift(function(value) {
                                ctrl.$setValidity('match', value === ctrl.$modelValue);
                                return value;
                        });
                        theOther.$formatters.unshift(function(value) {
                                ctrl.$setValidity('match', value === ctrl.$modelValue);
                                return value;
                        });
                };

                var unwatch = scope.$watch(theForm.$name +"."+ attr.mustMatch, function(){
                        addParserToOther();
                        unwatch();
                });

            }
        };
    })
	.directive("gcLoader", function(){
            return {
            controller : function gcLoaderController($scope,gcLoaderBus){
                $scope.isLoading = function(){
                    return gcLoaderBus.instances() > 0;
                };
            }, 
            template : "<div id='loader-container'><div id='loader' ng-show='isLoading()'>jobbar...</div></div>",
            restrict : 'EA',
            replace : true
       };
    })
    .provider("gcLoaderBus", function LoaderBusRepository(){
        var instances = 0;
        return {
            $get: function LoaderBusFactory(){
                return {
                    add : function(){
                        instances++;
                    },
                    remove : function(){
                        instances--;
                    },
                    instances : function(){
                        return instances;
                    },
                    isLoading : function(){
                        return instances.length > 0;
                    }
                };
            }
        };
    })
	.directive("gcSearch",function(){
		return {
			link: function(scope,elem,attr){
				elem.find('input')[0].focus();
			},
			controller : function($scope,People,$attrs){
				$scope.selectionScope = $scope.selectionScope || "people";
				$scope.searchState = "";
				$scope.$watch("searchTerm",function(newVal,oldVal){
					if(newVal !== undefined && $scope.selectForm && $scope.selectForm.$valid){
						$scope.searchState = "söker efter \"" + $scope.searchTerm + "\"...";
						People.search($scope.searchTerm,$scope.selectionScope, function(response){
							if(response.data.data.length === 0){
								$scope.searchState = "hittade inga träffar";
							}
							else{
								$scope.searchState = "hittade " + response.data.data.length + " träffar";
								$scope.searchResult = response.data.data;
							}
						});
					}
				});
			},
			scope : {
				searchResult : "=?",
				searchState : "=?",
				selectionScope : "=?"
			},
			template : "<span><input ng-form='selectForm' type='text' placeholder='namn...' ng-model='searchTerm' ng-model-options=\"{ debounce: {'default': 500, 'blur': 0} }\" /><br/><span class='search-status'>{{searchState}}</span></span>",
			restrict : "E",
			replace: true
		};		
	})
//	.controller("peopleSearchController",function($scope,People){
//		$scope.noResults = false;
//		$scope.searchTerm = "";
//		var timer = null;
//		
//		$scope.$watch("searchTerm",function(newVal,oldVal){
//			if($scope.form.$valid){
//				if(timer !== null){
//					clearTimeout(timer);
//				}
//				timer = setTimeout( function(){
//						People.search($scope.searchTerm,function(response){
//							if(response.data.data.length === 0){
//								$scope.noResults = true;
//							}else{
//								$scope.noResults = false;
//								$scope.people = response.data.data;
//							}
//						});
//					},300);
//			}
//		});
//		
//		$scope.handleGetAll = function(){
//			People.search($scope.searchTerm,function(response){
//				if(response.data.data.length === 0){
//					$scope.noResults = true;
//				}else{
//					$scope.noResults = false;
//					$scope.people = response.data.data;
//				}
//			});
//		};
//	})
	.directive("gcSelector",function(urls){
		return {
			link : function(scope,element,attributes){
				if(attributes.multiSelect !== undefined && attributes.multiSelect !== "false"){
					scope.selectMultiple = true;
					scope.listItemContent = urls.getDirectivesUrl() + 'peopleSelector/checkbox.tpl.html';
				}else{
					scope.selectMultiple = false;
					scope.listItemContent = urls.getDirectivesUrl() + 'peopleSelector/radio.tpl.html';
				}
				scope.dialogueTemplate = urls.getDirectivesUrl() + 'peopleSelector/selectorDialogue.tpl.html';
			},
            controller : function($scope,ngDialog,$element){
				
				var selectedEntities = [];
				$scope.selectionScope = $scope.selectionScope || "people,groups";
				$scope.dialogHeader = $scope.dialogHeader || "Sök efter person";
				
				$scope.onSaveSelection = $scope.onSaveSelection || function(){ return true; };
				$element.bind("click",function(){
					
					if($scope.selectMultiple){
						$scope.handleClick = function($event,entity){
							var index = selectedEntities.indexOf(entity);

							if($event.srcElement.checked && index < 0){
								selectedEntities.push(entity);
							}
							else if(!$event.srcElement.checked && index >= 0){
								selectedEntities.splice(index,1);
							}
						};
					}
					else{
						$scope.handleClick = function($event,entity){
							selectedEntities = [entity];
						};
					}
					
					$scope.saveSelection = function(){
						$scope.onSaveSelection({ selection : selectedEntities});
						selectedEntities = [];
					};
					
					ngDialog.open({
						template : $scope.dialogueTemplate,
						className: 'ngdialog-theme-default',
						scope : $scope
					});
				});
            },
			scope : {
				onSaveSelection : "&onSaveSelection",
				dialogHeader : "=?",
				selectionScope : "=?"
			},
			replace : false,
            restrict : 'A'
        };
	})
	.factory("gcLoaderBusInterceptor", function($q, gcLoaderBus) {
		var errorNo = 0;
		
		return {
			'request': function(config) {
			  gcLoaderBus.add();
			  return config;
			},
		  
			'requestError': function(rejection) {
			   // do something on error
			   // if (canRecover(rejection)) {
			   // 	return responseOrNewPromise;
			   // }
			   return $q.reject(rejection);
			 },

			'response': function(response) {
			  gcLoaderBus.remove();
			  // do something on success
			  return response;
			},

		   'responseError': function(rejection) {
				gcLoaderBus.remove();
				// do something on error
				//alert("Something went wrong. Please look in the console for more details (error " + ++errorNo + ")");
				alert(rejection.data);
				console.log("error (" + errorNo + ")");
				console.log(rejection);
				return $q.reject(rejection);
			}
		};
	  });
}());
(function(){
	function Group(promise){
		var that;
		if(promise){
			that = Object.create(promise);
		}else{
			that = this;
		}
		that.name = "";
		that.id = 0;
		that.emailAddress = "";
		that.members = [];
		that.signups = [];
		that.leader = null;

		return that;
	}
	angular.module("Group",['ngResource','ngSanitize','GothCon','HttpDataProviders','Filters','Signups','Sleeping','Orders','templates-angular_templates'])
		.provider("GroupDataProvider",function GroupDataProviderProvider(){
			var currentGroup = {};
			var baseUrl = "";
			var currentList = [];

			return {
				setBaseUrl : function(newBaseUrl){
					baseUrl = newBaseUrl;
				},
				$get : function GroupDataProviderFactory($q,$http){

					var groupPromise;
					var listPromise;

					function getById(id){
						var deferredJob = $q.defer();
						groupPromise = deferredJob.promise;
						if(!id){
							var group = getCurrent();
							deferredJob.resolve(group);
						}else{
							// load from http
							$http({
								url : baseUrl + "/admin/group/" + id,
								method: "get",
								responseType : "json"
							}).then(function(response){
								deferredJob.resolve(response.data.data);
							}, function(error){
								deferredJob.reject("error");
								console.log(error);
								throw "Failed to load group. See console for more information";
							});
						}
						return groupPromise;
					}
					function getCurrent(){

						if(!currentGroup){
							throw "No group loaded";
						}
						else{
							return currentGroup;
						}
					}
					function getAll(){
						var deferredJob = $q.defer();
						listPromise = deferredJob.promise;
						// load from http
						$http({
							url : baseUrl + "/admin/group",
							method: "get",
							responseType : "json"
						}).then(function(response){
							deferredJob.resolve(response.data.data);
						}, function(error){
							deferredJob.reject("error");
							console.log(error);
							throw "Failed to load groups. See console for more information";
						});
						currentList = deferredJob.promise;
						return currentList;
					}
					function save(groupData){
						var deferredJob = $q.defer();
						groupPromise = deferredJob.promise;
						var serviceUrl = baseUrl + "/admin/group";
						if(groupData.id){
							serviceUrl += "/" + groupData.id;
						}
						
						$http({
							url : serviceUrl,
							method: "post",
							responseType : "json",
							data: JSON.stringify(groupData)
						}).then(function(response){
							if(response.data.status === 0){
								deferredJob.resolve(response.data.data);
							}else{
								deferredJob.reject("Failed to save group. See console for more information");
								console.log(response.data);
							}
						}, function(error){
							deferredJob.reject("Failed to save group. See console for more information");
							console.log(error);
						});
						
						return groupPromise;
					}
					function createNew(){
						return new Group();
					}

					return {
						getCurrent : getCurrent,
						getById : getById,
						getAll : getAll,
						createNew : createNew,
						save : save
					};

				}
			};
		})
		.directive("group",function(){
			var extractIdRegex = /^\/group\/(\d+|new)(\/|$)/;
			var initialData;
			return {
				controller : function($scope,$location,gcTabsBus,GroupDataProvider,$element,$compile){
					
					function initialize(){
						var $content = angular.element($element.find("div")[0]);
						$scope.view = "";
						$scope.$watch("view",function(newValue,oldValue){
							if(newValue !== oldValue && newValue !== ""){
								$content.contents().remove();
								var $view = $compile(angular.element(newValue))($scope);
								$content.append($view);
							}
						});

						$scope.currentGroup = initialData;
						
						$scope.$on('$locationChangeSuccess',onLocationChange);
					}
					
					function onLocationChange(){
						var match = extractIdRegex.exec($location.path());

						var id = (match !== null && match.length > 1) ? match[1] : "";

						if(id === ""){
							// this is the list view
							onShowList();
							return;
						}
						else if(id === "new")
						{
							onShowNewForm();
							return;
						}
						else{
							onShowGroup(id);
							return;
						}
					}
					
					function onShowList(){
						gcTabsBus.setTabs([]);
						$scope.title = "Grupplista";
						gcTabsBus.setTabs([{ id : 'group/', 'label' : 'Grupplistning'},{ id : 'group/new', 'label' : 'Ny grupp'}]);
						$scope.currentGroup = undefined;
						$scope.groupList = GroupDataProvider.getAll().then(
							function(groups){
								$scope.groupList = groups;
								$scope.view = "<group-list groups='groupList'></group-list>";
							}
						);
						return;
					}

					function onShowNewForm(){
						$scope.currentGroup = GroupDataProvider.createNew();
						$scope.title = "Ny grupp";
						gcTabsBus.setTabs([{ id : 'group/', 'label' : 'Grupplistning'},{ id : 'group/new', 'label' : 'Ny grupp'}]);
						$scope.view = "<group-details current-group='currentGroup'></group-details>";
					}

					function onShowGroup(id){
						gcTabsBus.setTabs([
							{ id : 'group/', 'label' : 'Grupplistning'},
							{ id : 'group/' + id + '/', 'label' : 'Detaljer'},
							{ id : 'group/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'}]);
						switch($location.path()){
							case '/group/' + id +"/signups":
								$scope.view = "<group-signups-and-members current-group='currentGroup'></group-members-and-signups>";
								break;
							case '/group/' + id :
								$scope.view = "<group-details current-group='currentGroup'></group-details>";
								break;
							default:
								$scope.view = "<group-details current-group='currentGroup'></group-details>";
						}
						
						if(!$scope.currentGroup || "" + $scope.currentGroup.id !== "" + id){
							$scope.currentGroup = GroupDataProvider.getById(id).then(function(group){
								$scope.currentGroup = group;
								$scope.title = "Detaljer för " + $scope.currentGroup.name;
							});
						}
					}

					initialize();
					
				},
				scope : {
					
				},
				replace : true,
				templateUrl : "group/templates/group.tpl.html",
				restrict : 'AE'				
			};
		})
		.directive("groupDetails",function($templateCache){
			return {
				controller : function($scope,GroupDataProvider,$location){
					$scope.save = function(){
						GroupDataProvider.save($scope.currentGroup).then(function(createdGroup){
							$scope.currentGroup = createdGroup;
							$location.path('group/' + createdGroup.id + '/');
						},function(){
							
						});
					};
				},
				scope : {
					currentGroup : "="
				},
				replace : true,
				templateUrl : "group/templates/details.tpl.html",
				restrict : 'AE'
			};
		})
		.directive("groupList",function($templateCache){
			return {
				controller : function($scope){
					
				},
				scope : {
					groups : "="
				},
				replace : true,
				templateUrl : "group/templates/list.tpl.html",
				restrict : 'AE'
			};
		})
		.directive("groupSignupsAndMembers",function($templateCache){
			return {
				controller : function($scope,Groups){
					$scope.addMembers = function(member){
						if(member.length > 0){
							Groups.addMembership($scope.currentGroup.id,member[0].id,function(response){
								$scope.currentGroup.groupMembers.push(response.data);
							});
						}
					};
					$scope.deleteMembership = function(membership){
						if(confirm("Är du säker?")){
							Groups.deleteMembership(membership.id,function(response){
								$scope.currentGroup.groupMembers.splice($scope.currentGroup.groupMembers.indexOf(membership),1);
							});
						}
					};
				},
				scope : {
					currentGroup : "="
				},
				replace : true,
				templateUrl : "group/templates/members_and_signups.tpl.html",
				restrict : 'AE'
			};
		});
}());
(function(){
/*jshint strict:false */
"use strict";

angular.module("Orders",['GothCon','Filters','ngDialog','HttpDataProviders'])
	.directive("personalOrders", function(urls){
        return {
            controller : function($scope,ngDialog,gcLoaderBus,People){
				$scope.$watch(function(){ return (!$scope.currentPerson) ? undefined : $scope.currentPerson.id;},function(newVal,oldVal){
					if(newVal !== undefined && newVal > 0){
                        $scope.orders = People.orders(newVal);
					}
					else{
						$scope.orders = [];
					}
				});
            },
			scope : {
				currentPerson : "=",
			},
			replace : true,
            templateUrl : "orders/templates/orders.tpl.html",
            restrict : 'AE'
        };
    });

}());
(function(){
/*jshint strict:false */
"use strict";
	
angular.module("Person",['ngResource','GothCon','Filters','Signups','Sleeping','Orders'])
	.directive("people",function(){
		var extractIdRegex = /^\/person\/(\d+|new)(\/|$)/;
		return {
			templateUrl : 'person/templates/people.tpl.html',
			controller : function($scope,$location,gcTabsBus,People,gcUser,$compile,$element){

				function initialize(){
					$scope.currentUser = gcUser.current();
					$scope.disableSaveButton = false;
					$scope.overrideFormValidation = false;
					$scope.deletePerson = deletePerson;
					$scope.$on('$locationChangeSuccess',onLocationChange);
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
				}
			
				function onLocationChange(){

					$scope.deletePerson = angular.noop;
					$scope.savePerson = angular.noop;
					$scope.saveUser = angular.noop;

					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						// this is the "new" form
						onShowNewForm();
						return;
					}
					else{
						// this is the standard "view"
						var initialData = document.getElementById("initial-person-data");
						if(initialData && initialData.innerHTML.length > 0){
							$scope.person = JSON.parse(initialData.innerHTML);
							if($scope.person.user !== null){
								$scope.user = { username : $scope.person.user.username, level : $scope.person.user.level, isActive : $scope.person.user.isActive, password :"", password_repeat : ""};
							}

						}
						onShowPerson(id);
					}
				}

				function onUserChange(id){
					


				}

				function onShowList(){
					$scope.title = "Personlista";
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/new', 'label' : 'Ny person'}]);
					$scope.view = '<people-list people="people"></people-list>';
					$scope.people = People.all();
					return;
				}

				function onShowNewForm(){
					$scope.title = "Ny person";
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/new', 'label' : 'Ny person'}]);
					$scope.view =  '<people-form person="person"></people-form>';
					$scope.person = {};
					$scope.savePerson = function(){
						$scope.disableSaveButton = true;
						$scope.person = People.save($scope.person,function(response){
							var person = response.data;
							if(person.id > 0){
								$location.path("person/" + person.id);
							}else{
								$scope.disableSaveButton = false;
							}
						});
					};
				}

				function onShowPerson(id){
					
					gcTabsBus.setTabs([
						{ id : 'person/' , 'label' : 'Personlistan'},
						{ id : 'person/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'person/' + id + '/user', 'label' : 'Användaruppgifter'},
						{ id : 'person/' + id + '/signups', 'label' : 'Anmälningar och lagmedlemskap'},
						{ id : 'person/' + id + '/accommodation', 'label' : 'Sovning'},
						{ id : 'person/' + id + '/orders', 'label' : 'Förbeställningar'},
						{ id : 'person/' + id + '/arrival', 'label' : 'Ankomstregistrering'},
						{ id : 'person/' + id + '/delete', 'label' : 'Ta bort'}]);

					$scope.deletePerson = deletePerson;
					$scope.savePerson = savePerson;
					$scope.saveUser = saveUser;

					if($scope.person && "" + $scope.person.id === "" + id){
						$scope.title ="Detaljer för " + $scope.person.firstName + " " + $scope.person.lastName + " (" + $scope.person.id + ")";
					}
					else{	
						$scope.signups = undefined;
						$scope.password_repeat = "";
						$scope.disableSaveButton = true;
						$scope.user = { username : "", level : 0, isActive : false, password : "", password_repeat : ""};
						$scope.person = People.get(id,function(response){
							$scope.title ="Detaljer för " + $scope.person.firstName + " " + $scope.person.lastName + " (" + $scope.person.id + ")";
							$scope.disableSaveButton = false;
							if(response.user !== null){
								$scope.user = { username : $scope.person.user.username, level : $scope.person.user.level, isActive : $scope.person.user.isActive, password :"", password_repeat : ""};
							}
						});	
					}

					switch($location.path()){
						case "/person/" + id + "/signups":
							$scope.view = '<people-signups person="person"/>';
							break;
						case "/person/" + id + "/accommodation":
							$scope.view = '<people-accommodation person="person"/>';
							break;
						case "/person/" + id + "/orders":
							$scope.view = '<people-orders person="person"/>';
							break;
						case "/person/" + id + "/user":
							$scope.view = '<user person="person"/>';
							break;
						case "/person/" + id + "/delete":
							$scope.view = '<people-delete person="person"/>';
							break;                
						case "/person/" + id + "/arrival":
							$scope.view = '<people-arrival person="person"/>';
							break;
						default:
							$scope.view = '<people-form person="person"/>';
					}     
				}

				function savePerson(){
					$scope.disableSaveButton = true;
					People.save($scope.person,function(response){
						if(response.status === 200){
							$scope.person = response.data;
						}
						$scope.disableSaveButton = false;
					});
				}

				function saveUser(){

					$scope.disableSaveButton = true;
					People.saveUser($scope.person,$scope.user,function($response){

						$scope.disableSaveButton = false;
						// console.log($response); 
					});
				}

				function deletePerson(){
					if(confirm("Är du säker?")){
						People.remove($scope.person.id,function(){
							$scope.person = undefined;
							$location.path("person");
						});
					}
				}

				initialize();

			},
			restrict : 'E',
			replace : true
		};
	})
	.directive("peopleAccommodation",function(){
		return {
			templateUrl : 'person/templates/accommodation.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleArrival",function(){
		return {
			templateUrl : 'person/templates/arrival.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleDelete",function(){
		return {
			templateUrl : 'person/templates/delete.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleForm",function(){
		return {
			templateUrl : 'person/templates/form.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleList",function(){
		return {
			templateUrl : 'person/templates/list.tpl.html',
			controller : function($scope,$filter){
				
				
			},
			restrict : 'AE',
			scope : {
				people : "="
			}
		};
	})
	.directive("peopleOrders",function(){
		return {
			templateUrl : 'person/templates/orders.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("peopleSignups",function(){
		return {
			templateUrl : 'person/templates/signups.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("user",function(){
		return {
			templateUrl : 'person/templates/user.tpl.html',
			controller : function($scope){},
			restrict : 'AE'
		};
	})
	.directive("personalGroupMemberships", function(urls){
        return {
            controller : function($scope,gcLoaderBus,ngDialog,Groups){

				$scope.deleteMembership = function(membership){
					if(confirm("Är du säker?")){
						
						Groups.deleteMembership(membership.id,function(response){
							
							$scope.memberships.splice($scope.memberships.indexOf(membership),1);
						});
					}
				};

				$scope.addMember = function(group){
					
					Groups.addMembership(group.id,$scope.currentUserId,function(response){
						
						$scope.memberships.push(response.data);
						$scope.availableGroups.splice($scope.availableGroups.indexOf(group),1);
					});
				};
				
				$scope.showMembershipDialogue = function(){
					
					Groups.getAvailableGroups($scope.currentUserId,function(response){
						$scope.availableGroups = response.data;
						
					});
					ngDialog.open({
					   template : urls.getDirectivesUrl() + 'groupMemberships/groupMembershipDialogue.tpl.html',
					   className: 'ngdialog-theme-default',
					   scope : $scope
					});
				};
			},
			scope : {
				memberships : "=",
				currentUserId: "="
			},
			replace : true,
            templateUrl : urls.getDirectivesUrl() + "groupMemberships/groupMemberships.tpl.html",
            restrict : 'AE'
        };
    })
	.provider("gcUser", function UserRepositoryProvider(){
        
        var _currentUser;
        return{
            $get : function UserRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
                var _resource = $resource( linkBuilder.createAdminLink("user/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'current' 
							: {method : 'GET',params : { id : "current" } , interceptor : gcLoaderBusInterceptor }});
                return {
                    current : function(callback){
						callback = callback || angular.noop;
						if(_currentUser === undefined){
                            _resource.current(callback);
                        }else{
                            callback(_currentUser);
                        }
						return _currentUser;
                    }
                };
            }
        };
    })
	.provider("People", function PeopleRepositoryProvider(){
        var _resource;
        var currentPerson;
		
        return{
            $get : function PeopleRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var standardPersonLink = linkBuilder.createAdminLink("person/");
				var searchLink = linkBuilder.createAdminLink("search/");
                _resource = $resource( standardPersonLink + ":id/:operation?json",{},{	
					'search': {method:'GET'		, interceptor : gcLoaderBusInterceptor, url : searchLink },
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'orders': {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : { operation : "orders" }  },
					'saveUser'
							: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : { operation : "user" } },
					'saveAccommodation' 
							: {method : 'POST',params : { operation : "saveAccommodation" } , interceptor : gcLoaderBusInterceptor },
					'availableAccommodations' 
							: { method: "GET", isArray : true, params : { operation : "availableAccommodations" }, interceptor : gcLoaderBusInterceptor }
                });
                return {
					availableAccommodations : function(personId,callback){
						callback = callback || angular.noop;
						return _resource.availableAccommodations({ id: personId},{}, callback);
					},
					
					remove : function(personId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id: personId},callback);
					},
					saveAccommodation : function(personId, accommodationResourceId,callback){
						callback = callback || angular.noop;
						return _resource.saveAccommodation({ id: personId},{ accommodationResourceId: accommodationResourceId},callback,callback);
					},
					
					saveUser : function(person,user,callback){
						callback = callback || angular.noop;
						return _resource.saveUser ({ id: person.id},  user, callback,callback);
                    },
					
					save : function(person,callback){
						callback = callback || angular.noop;
						var currentPerson = _resource.save({},person,callback,callback);
						return currentPerson;
                    },
                    
					all : function(callback){
                        callback = callback || angular.noop;
						return _resource.query(callback,callback);
                    },
					orders : function(personId,callback){
                        callback = callback || angular.noop;
						return _resource.orders({ id: personId},callback,callback);
                    },
                    
					get : function(id,callback){
						callback = callback || angular.noop;
	                    if(currentPerson === undefined || "" + currentPerson.id !== "" + id){
							currentPerson = _resource.get({id : id},callback); 
                        }else{
                            callback(currentPerson);
                        }
                        return currentPerson;
                    },
					search : function(queryString, searchScope, callback){
						callback = callback || angular.noop;
						if(!queryString || queryString.length < 3){
							return false;
						}else{
							return _resource.search({ q : queryString, r : searchScope },callback);
						}
					}
                };
            }
        };
    });
}());
(function(){
/*jshint strict:false */
"use strict";
angular.module("HttpDataProviders",['ngResource','GothCon'])
	.provider("Signups",function SignupsRepositoryProvider(){
			return {
			   $get : function(linkBuilder,$resource,gcLoaderBus,gcLoaderBusInterceptor){
				   var standardSignupLink = linkBuilder.createAdminLink("signup/");
				   var _resource = $resource( standardSignupLink + ":signupId/:operation/",{},{	
						'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
						'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
						'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
						'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
						'getSignupsByPerson' 
								: {method : 'GET', isArray:true, interceptor : gcLoaderBusInterceptor },
						'getSignupsByGroup' 
								: {method : 'GET', isArray:true, interceptor : gcLoaderBusInterceptor }});
				   return {
						getSignupsByPerson : function(id,callback){
							return _resource.getSignupsByPerson({ person_id: id}, callback || angular.noop);
						},
						getSignupsByGroup : function(id,callback){
							return _resource.getSignupsByGroup({ group_id: id}, callback || angular.noop);
						},
						savePersonalSignup : function(personId,signupSlotId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + "?person_id=:personId").save(
								{personId : personId},
								{signupSlotId : signupSlotId },
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						},
						saveGroupSignup : function(personId,signupSlotId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + "?group_id=:groupId").save(
								{groupId : personId},
								{signupSlotId : signupSlotId },
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						},
						deleteSignup : function(signupId,callback){
							gcLoaderBus.add();
							$resource(standardSignupLink + ":signupId").delete(
								{signupId : signupId},
								{},
								function(response){
									gcLoaderBus.remove();
									if(callback){
										callback(response);
									}
								}
							);
						}
				   };
			   }
		   } ;
		})
	.provider("Groups", function GroupsRepositoryProvider(){
        var _resource;
        return{
            $get : function SignupSlotsRepositoryFactory($resource,gcLoaderBus, linkBuilder){
                
				var standardGroupMembershipLink = linkBuilder.createAdminLink("groupMembership/");
				var standardPersonLink = linkBuilder.createAdminLink("person/");
				
				_resource = $resource(standardPersonLink + ":id?json",{},{
                    all : { method : "GET", isArray : true}
                });

				return {
					deleteMembership : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardGroupMembershipLink + ":id",{}).delete({ id: id}, function internalCallback(response){
							gcLoaderBus.remove();
                            if(callback){
                                callback(response);
                            }
                        });
					},
					
                    getAvailableGroups : function(id,callback){
						gcLoaderBus.add();
                        $resource(standardPersonLink + ":personId/availableGroups",{}).get({ personId: id}, function internalCallback(response){
                            gcLoaderBus.remove();
							if(callback){
                                callback(response);
                            }
                        });
                    },
					addMembership : function(groupid,personid,callback){
						gcLoaderBus.add();
						$resource(standardPersonLink + ":personId/addMembership/",{}).save(
							{personId : personid},
							{group_id : groupid},
							function internalCallback(response){
								gcLoaderBus.remove();
								if(callback){
									callback(response);
								}
						});
					}
                };
            }
        };
    })
	.provider("Resource", function ResourceRepositoryProvider(){
        return{
            $get : function ResourceRepositoryFactory($resource,linkBuilder,gcLoaderBusInterceptor){
				var accommodationLink = linkBuilder.createAdminLink("sleepingSlotBooking/:id");
				var eventOccationLink = linkBuilder.createAdminLink("eventOccation/:id/:svc/:operation?format=json");
				var resourceBookingLink = linkBuilder.createAdminLink("resourceBooking/:id/:svc/:operation?format=json");
				var eventOccationBookingLink = linkBuilder.createAdminLink("eventOccationBooking/:id/?json");
                var _resource = $resource( linkBuilder.createAdminLink("resource/:id/:operation?json"),{},{	
					'get'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'remove': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'delete': {method:'DELETE'	, interceptor : gcLoaderBusInterceptor },
					'types'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params : {operation : 'availableResourceTypes'} },
					'addAccommodations'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : {operation : 'accommodation'} },
					'deleteAccommodation'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor, url : accommodationLink},
					'setSleepingOccations'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor, params : {operation : 'sleepingOccations'} },
					'getScheduleData'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, params : {operation : 'bookings'} },
					'getScheduleableEvents'	: {method:'GET'		, interceptor : gcLoaderBusInterceptor, params : {operation : 'availableEventOccations', svc : 'svc'} , url : resourceBookingLink },
					'addOccationBookings'	: {method:'POST'	, interceptor : gcLoaderBusInterceptor, url: eventOccationBookingLink, params : { multiple : "true", id : "ny"}, headers : {'Content-Type': 'application/x-www-form-urlencoded'} },
					'deleteOccationBooking'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor, url: eventOccationBookingLink },
					'bookings'				: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true, params  : {operation : "bookings" } }
				});
                return {
					'delete' : function(id,callback){
						callback = callback || angular.noop;
                        return _resource.delete({id : id},callback,callback);
                    },
					'query' : function(callback){
						callback = callback || angular.noop;
                        return _resource.query(callback,callback);
                    },
					'types' : function(callback){
						callback = callback || angular.noop;
                        return _resource.types(callback,callback);
                    },
					'get' : function(id,callback){
						callback = callback || angular.noop;
                        return _resource.get({id : id},callback,callback);
                    },
					'save' : function(data,callback){
						callback = callback || angular.noop;
                        return _resource.save({ 'id' : data.id },data,callback,callback);
					},
					'setSleepingOccations' : function(resourceId,occationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.setSleepingOccations({ 'id' : resourceId },occationSelectionData,callback,callback);
					},
					'addAccommodations' : function(resourceId,accommodationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.addAccommodations({ 'id' : resourceId },accommodationSelectionData,callback,callback);
					},
					'deleteAccommodation' : function(sleepingSlotBookingId,accommodationSelectionData,callback){
						callback = callback || angular.noop;
                        return _resource.deleteAccommodation({ 'id' : sleepingSlotBookingId },accommodationSelectionData,callback,callback);
					},
					'getScheduleData' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.getScheduleData({ 'id' : resourceId },callback,callback);
					},
					'getScheduleableEvents' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.getScheduleableEvents({ 'resourceId' : resourceId },callback,callback);
					},
					'addOccationBookings' : function(resourceId,data,callback){
						callback = callback || angular.noop;
                        return _resource.addOccationBookings({ 'resource_id' : resourceId },$.param({"eventOccation" : data}),callback,callback);
					},
					'deleteOccationBooking' : function(occationBookingId,callback){
						callback = callback || angular.noop;
                        return _resource.deleteOccationBooking({ 'id' : occationBookingId },callback,callback);
					},
					'bookings' : function(resourceId,callback){
						callback = callback || angular.noop;
                        return _resource.bookings({ 'id' : resourceId },callback,callback);
					}
					
                };
            }
        };
    });
}());
(function(){
/*jshint strict:false */


angular.module("Resource",['ngResource','GothCon','HttpDataProviders','Filters','ngDialog'])
	.provider("ResourceBookings",function(){
		var baseUrl = "";
		return {
			setBaseUrl : function(newBaseUrl){
				baseUrl = newBaseUrl;
			},
			$get : function GroupDataProviderFactory($q,$http){

				var promise;

				function getById(id){
					var deferredJob = $q.defer();
					promise = deferredJob.promise;
					
					// load from http
					$http({
						url : baseUrl + "/administrator/booking/svc/getBookingsByResource?resource_id=" + id,
						method: "get",
						responseType : "json"
					}).then(function(response){
						deferredJob.resolve(response.data.data);
					}, function(error){
						deferredJob.reject("error");
						console.log(error);
						throw "Failed to load bookings. See console for more information";
					});
					
					return promise;
				}
				function getScheduleData(){
					var deferredJob = $q.defer();
					promise = deferredJob.promise;
					
					// load from http
					$http({
						url : baseUrl + "/administrator/booking/svc/getScheduleData",
						method: "get",
						responseType : "json"
					}).then(function(response){
						deferredJob.resolve(response.data);
					}, function(error){
						deferredJob.reject("error");
						alert(error);
						throw "Failed to load bookings. See console for more information";
					});
					
					return promise;
				}
				
				return {
					getById : getById,
					getScheduleData : getScheduleData
				};

			}
		};
	})
	.controller("ResourceAppController", function($scope,$location,gcTabsBus,Resource,urls){

	})
	.directive("resource",function(){
		var extractIdRegex = /^\/resource\/(\d+|new|schedule)(\/|$)/;    

		return {
			controller : function($scope,$location,gcTabsBus,Resource,$element,$compile,urls){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);

					$scope.view = "";

					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});

					$scope.$on('$locationChangeSuccess',onLocationChange);

					$scope.currentResource = {};

					$scope.deleteAccommodation = function (accommodation){
						if(confirm("Är du säker?")){
							Resource.deleteAccommodation(accommodation.id,function(){
								$scope.resource.sleepingResource.sleepingSlotBookings.splice($scope.resource.sleepingResource.sleepingSlotBookings.indexOf(accommodation),1);
							});
						}
					};
				}

				function onLocationChange(){

					var match = extractIdRegex.exec($location.path());

					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						showList();
						return;
					}
					else if(id === "schedule")
					{
						// this is the "new" form
						showSchedule();
						return;
					}
					else if(id === "new")
					{
						// this is the "new" form
						showNewForm();
						return;
					}
					else{
						// this is the standard "view"
						showResource(id);
						return;
					}

				}

				function showList(){
					$scope.title = "Resurslista";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.currentResource = undefined;
					$scope.resources = Resource.query(function(groups){
						$scope.groupList = groups;
						$scope.view = "<resource-list resources='resources'></resource-list>";
					});
				}
				
				function showSchedule(){
					$scope.title = "Bokningsöversikt";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.currentResource = undefined;
					$scope.groupList = [];
					$scope.view = "<resource-bookings-overview></resource-bookings-overview>";
				}

				function showNewForm(){
					$scope.title = "Ny resurs";
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/schedule', 'label' : 'Bokningsöversikt'},
						{ id : 'resource/new', 'label' : 'Ny resurs'}]);
					$scope.view = "<resource-details/>";
				}

				function showResource(id){
					id = parseInt(id);
					gcTabsBus.setTabs([
						{ id : 'resource/', 'label' : 'Resurslista'},
						{ id : 'resource/' + id + '/', 'label' : 'Detaljer'},
						{ id : 'resource/' + id + '/accommodation', 'label' : 'Sovning'},
						{ id : 'resource/' + id + '/bookings', 'label' : 'Bokningslista'},
						{ id : 'resource/' + id + '/schedule', 'label' : 'Bokningsschema'},
						{ id : 'resource/' + id + '/delete', 'label' : 'Ta bort'}]);

					if(!$scope.resource || "" + $scope.currentResource.id !==  "" + id){
						
						$scope.title = "Laddar detaljer för resurs #" + id;
						Resource.get(id, function(response){
							
							switch($location.path()){
							case "/resource/" + id + "/accommodation":
								$scope.view = "<resource-accommodation resource='currentResource' sleeping-event-occations='sleepingEventOccations'/>";
								break;
							case "/resource/" + id + "/bookings":
								$scope.view = "<resource-bookings-list resource='currentResource'/>";
								break;
							case "/resource/" + id + "/schedule":
								$scope.view = "<resource-bookings-schedule resource='currentResource'/>";
								break;
							case "/resource/" + id + "/delete":
								$scope.view = "<resource-delete resource='currentResource'/>";
								break;
							default:
								$scope.view = "<resource-details resource='currentResource'/>";
							}		
							
							$scope.currentResource = response.data.resource;
							$scope.title = "Detaljer för " + $scope.currentResource.name;
							$scope.sleepingEventOccations = response.data.sleepingEventOccations;
						});
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "resource/templates/resource.tpl.html",
			restrict : 'E'				
		};
	})
	.directive("resourceList",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "resource/templates/list.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceAccommodation",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.onSaveAccommodations = function(accommodations){
					Resource.addAccommodations($scope.resource.id, accommodations,function(response){
						$scope.resource.sleepingResource.sleepingSlotBookings = response.data.data;
					});
				};
					
				$scope.updateOccationSelection = function($event,occationId){
					$scope.sleepingEventOccations[occationId].selected = $event.srcElement.checked;
				};

				$scope.updateOccations = function(){
					Resource.setSleepingOccations($scope.resource.id, $scope.sleepingEventOccations);
				};
			},
			scope : {
				resource : "=",
				sleepingEventOccations : "="
			},
			replace : true,
			templateUrl : "resource/templates/accommodation.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceBookingsList",function(){
		return {
			controller : function ($scope,Resource,ResourceBookings){
				$scope.saveEventOccationBookings = function(occations){
					Resource.addOccationBookings($scope.resource.id,occations, function(response){
						ResourceBookings.getById($scope.resource.id).then(function(data){
							$scope.bookings = data;
						});
					});
				};
				$scope.bookings = [];
				ResourceBookings.getById($scope.resource.id).then(function(data){
					$scope.bookings = data;
				});
				$scope.deleteBooking = function(booking){
					if(confirm("Är du säker?")){
						Resource.deleteOccationBooking(booking.id,function(){
							for(var bKey in  $scope.bookings){
								if($scope.bookings[bKey].id === booking.id){
									$scope.bookings.splice(bKey,1);
								}
							}
						});
					}
				};
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingslist.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceBookingsSchedule",function(urls){
		return {
			controller : function($scope,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				$scope.resourceId = $scope.resource.id;
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addEvents(eventTypes,conventionStartsAt){
					var eventTypeBoxes = [];
					for(var key in eventTypes){
						
						var eventType = eventTypes[key];
						var eventTypeBox = {
							name : eventType.name,
							id : eventType.id,
							events : []
						};
						for(var eventKey in eventType.events){
							var event = eventType.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.eventOccations){
								var eventOccation = event.eventOccations[occationKey];
								if(eventOccation.timespan){
									var occationBox = {
										name : eventOccation.name || eventOccation.timespan.name,
										id : eventOccation.id,
										bookingId : eventOccation.eventOccationBookings[0].booking.id,
										width: getOccationWidth(eventOccation.timespan.startsAt,eventOccation.timespan.endsAt),
										offset: getOccationOffset(eventOccation.timespan.startsAt,conventionStartsAt)
									};
									eventBox.eventOccations.push(occationBox);
								}
							}
							eventTypeBox.events.push(eventBox);
						}
						eventTypeBoxes.push(eventTypeBox);
					}
					$scope.eventTypes = eventTypeBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				Resource.getScheduleData($scope.resource.id, function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.publicTimespans;
					var eventTypes = response.data.bookings;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addEvents(eventTypes,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingschedule.tpl.html",
            restrict : 'E'
		};
	})
	.directive("resourceBookingsOverview",function(urls){
		return {
			controller : function($scope,ResourceBookings,Resource){
				
				var	conventionEndsAt;
				var conventionStartsAt;
				
				
				var PIXELS_PER_HOUR = 16;
				var PIXELS_PER_DAY = PIXELS_PER_HOUR * 24;
				
				$scope.schedule = {};
								
				function getOccationOffset(startsAtString, conventionStartsAt){
					var bookingStartsAt = moment(startsAtString);
					return bookingStartsAt.diff(conventionStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function getOccationWidth(startsAtString,endsAtString){
					var bookingStartsAt = moment(startsAtString);
					var bookingEndsAt = moment(endsAtString);
					return bookingEndsAt.diff(bookingStartsAt,'hour') * PIXELS_PER_HOUR;
				}
				
				function addDateBoxes(startsAt,endsAt){
					var lastDayStartsAt = moment(endsAt).startOf('day');

					var dateBoxes = [{ 
						width: moment(startsAt).startOf('day').add(1,'day').diff(startsAt,'h') * PIXELS_PER_HOUR, 
						label : startsAt.format('ddd DD/MM'),
						offset : 0 
					}];
					
					var currentDate = moment(startsAt).startOf('day').add(1,'day');
					var currentOffset = dateBoxes[0].width;
					while(currentDate.isBefore(lastDayStartsAt)){
						dateBoxes.push({ 
							width: PIXELS_PER_DAY, 
							label : currentDate.format('ddd DD/MM'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_DAY;
						currentDate.add(1,'d');
					}
					
					dateBoxes.push({ 
						width: endsAt.diff(lastDayStartsAt,'h')  * PIXELS_PER_HOUR, 
						label : endsAt.format('ddd DD/MM'),
						offset : currentOffset
					});
					
					$scope.dates = dateBoxes;
				}
				
				function addHourBoxes(startsAt,endsAt){
					var hourBoxes = [];
					var current = moment(startsAt);
					var currentOffset = 0;
					while(current.isBefore(endsAt)){
						hourBoxes.push({ 
							width: PIXELS_PER_HOUR, 
							label : current.format('HH'),
							offset : currentOffset
						});
						currentOffset += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.hours = hourBoxes;
				}
				
				function setScheduleWidth(startsAt,endsAt){
					var width=200;
					var current = moment(startsAt);
					while(current.isBefore(endsAt)){
						width += PIXELS_PER_HOUR;
						current.add(1,'h');
					}
					$scope.scheduleWidth = width;
				}
				
				function addBookings(resources,conventionStartsAt){
					var resourceBoxes = [];
					for(var key in resources){
						
						var resource = resources[key];
						var resourceBox = {
							name : resource.name,
							id : resource.id,
							events : []
						};
						for(var eventKey in resource.events){
							var event = resource.events[eventKey];
							var eventBox = {
								name : event.name,
								id : event.id,
								eventOccations : []
							};
							for(var occationKey in event.bookings){
								var eventOccation = event.bookings[occationKey];

								var occationBox = {
									name : eventOccation.name,
									bookingId : eventOccation.bookingId,
									width: getOccationWidth(eventOccation.startsAt,eventOccation.endsAt),
									offset: getOccationOffset(eventOccation.startsAt,conventionStartsAt)
								};
								eventBox.eventOccations.push(occationBox);

							}
							resourceBox.events.push(eventBox);
						}
						resourceBoxes.push(resourceBox);
					}
					$scope.resources = resourceBoxes;
				}
				
				function createPublicTimespansData(timespans,conventionStartsAt){
					var timespanBoxes = [];
					for(var key in timespans){
						var timespan = timespans[key];
						timespanBoxes.push({
							name : timespan.name,
							id: timespan.id,
							width : getOccationWidth(timespan.startsAt,timespan.endsAt),
							offset : getOccationOffset(timespan.startsAt,conventionStartsAt)
						});
					}
					$scope.publicTimespans = timespanBoxes;
				}
				
				$scope.deleteBooking = function($event,eventBox,eventOccationBox){
					$event.preventDefault();
					if(confirm("Är du säker på att du vill ta bort resursbokningen?")){
						Resource.deleteOccationBooking(eventOccationBox.bookingId,function(){
							var index = eventBox.eventOccations.indexOf(eventOccationBox);
							eventBox.eventOccations.splice(index,1);
						});
					}
				};

				ResourceBookings.getScheduleData().then(function(response){
					
					var startsAt;
					var endsAt;
					var publicTimespans = response.data.publicTimespans;
					var resources = response.data.resources;
					
					for(var i in publicTimespans){
						var timespan = publicTimespans[i];
						var _startsAt = moment(timespan.startsAt);
						var _endsAt = moment(timespan.endsAt);

						if(!startsAt || startsAt > _startsAt){
							startsAt = _startsAt;
						}
						if(!endsAt || endsAt < _endsAt ){
							endsAt = _endsAt;
						}
					}
					
					conventionEndsAt = endsAt;
					conventionStartsAt = startsAt;
					
					addDateBoxes(startsAt,endsAt);
					addHourBoxes(startsAt,endsAt);
					setScheduleWidth(startsAt,endsAt);
					addBookings(resources,startsAt);
					createPublicTimespansData(publicTimespans,startsAt);
				});
				
				
			},
			scope : {
				resource : "="
			},
			replace : true,
			templateUrl : "resource/templates/bookingoverview.tpl.html",
            restrict : 'E'
		};
	})
	.directive("resourceDelete",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.delete = function (){
						if(confirm("Är du säker?")){
							Resource.delete($scope.resource.id,function(){
								$scope.resource = undefined;
								$location.path("resource");
							});
						}
					};
			},
			scope : {
				resources : "="
			},
			replace : true,
			templateUrl : "resource/templates/delete.tpl.html",
			restrict : 'E'			
		};
	})
	.directive("resourceDetails",function(){
		return {
			controller : function ($scope,$location,gcTabsBus,Resource,$element,$compile){
				$scope.resourceTypes = [];
				Resource.types(function(response){
					$scope.resourceTypes = response.data;
				});
				$scope.saveResource = function(){
					Resource.save({
						id : $scope.resource.id || "new",
						name : $scope.resource.name,
						description : $scope.resource.description,
						resourceType : $scope.resource.resourceType.id,
						isAvailable : $scope.resource.available,
						sleepingIsAvailable : $scope.resource.sleepingResource.isAvailable,
						noOfSleepingSlots : $scope.resource.sleepingResource.noOfSlots
					},function(response){
						$scope.resource = response.data.resource;
						$scope.sleepingEventOccations = response.data.sleepingEventOccations;
						$location.path("/resource/" + $scope.resource.id);
					});
				};
				if(!$scope.resource){
					$scope.resource = {
						id : "new",
						name : "",
						description : "",
						resourceType : {id:0},
						available : true,
						sleepingResource : {
							isAvailable : false,
							noOfSlots : 0
						}
					};
				}
			},
			scope : {
				resource : "=?"
			},
			replace : true,
			templateUrl : "resource/templates/form.tpl.html",
			restrict : 'E'			
		};
	})	
	.directive("eventOccationSelector",function(urls){
		return {
            controller : function($scope,ngDialog,$element,Resource){
				
				$scope.formdata = {
					eventOccations : []
				};

				$scope.getOccationTrueValue = function(occation){
					if(occation.already_booked){
						return "";
					}
					else{
						return occation.id;
					}
				};

				$scope.getOccationFalseValue = function(){
					return "";
				};

				$scope.dialogueTemplate = "/" + urls.getAngularAppUrl() + 'resource/templates/eventOccationSelector/selectorDialogue.tpl.html';

				$scope.dialogHeader = $scope.dialogHeader || "Välj arrangemangstillfällen";
				
				$scope.onSaveSelection = $scope.onSaveSelection || function(){ return true; };
				
				$element.bind("click",function(){
					
					$scope.saveSelection = function(){
						var occations = [];
						for(var key in $scope.formdata.eventOccations){
							if($scope.formdata.eventOccations[key] && $scope.formdata.eventOccations[key] !== ""){
								occations.push($scope.formdata.eventOccations[key]);
							}
						}
						$scope.onSaveSelection({ occations : occations});
					};
					
					$scope.selectEventOccations = function(event,$event){
						var eoKey;
						var eventOccation;
						if($event.srcElement.checked){
							for(eoKey in event.eventOccations){
								eventOccation = event.eventOccations[eoKey];
								if(!eventOccation.already_booked){
									var eventOccationId = parseInt(eventOccation.id);
									$scope.formdata.eventOccations[eventOccationId] = eventOccationId;
								}
							}
						}else{
							for(eoKey in event.eventOccations){
								eventOccation = event.eventOccations[eoKey];
								if(!eventOccation.already_booked){
									$scope.formdata.eventOccations[eventOccation.id] = false;
								}
							}
						}
					};
					
					$scope.allOccationsAreBooked = function(event){
						for(var eoKey in event.eventOccations){
							var eventOccation = event.eventOccations[eoKey];
							if(!eventOccation.already_booked){
								return false;
							}
						}
						return true;
					};
					
					Resource.getScheduleableEvents($scope.resourceId,function(response){
						
						$scope.events = response.data.data;
						
						for(var key in response.data.data){
							var event = response.data.data[key];
							for(var eoKey in event.eventOccations){
								var eventOccation = event.eventOccations[eoKey];
								var eventOccationId = parseInt(eventOccation.id);
								eventOccation.already_booked = eventOccation.already_booked === "1";
								eventOccation.ok_to_book = eventOccation.ok_to_book === "1";
								$scope.formdata.eventOccations[eventOccationId] = false;
							}
						}
					});
					
					ngDialog.open({
						template : $scope.dialogueTemplate,
						className: 'ngdialog-theme-default',
						scope : $scope
					});
				});
            },
			scope : {
				onSaveSelection : "&",
				resourceId : "="
			},
			replace : false,
            restrict : 'A'
        };
	})
	.filter('bookingLabel', function($filter){
        return function(booking){
			var label = booking.eventOccationBooking.eventOccation.event.name;
			if(booking.eventOccationBooking.eventOccation.name.length){
				label += " - " + booking.eventOccationBooking.eventOccation.name;
			}
			return label;
        };
    })
	.filter('simpleTimespanLabel', function($filter){
        return function(booking){
			var timespanFilter = $filter("timespanLabel");
            return timespanFilter({startsAt: {date : booking.startsAt}, endsAt : {date : booking.endsAt} });
        };
    });

}());
angular.module('templates-angular_templates', ['directives/accommodation/accommodation.tpl.html', 'directives/groupMemberships/groupMembershipDialogue.tpl.html', 'directives/groupMemberships/groupMemberships.tpl.html', 'directives/peopleSelector/checkbox.tpl.html', 'directives/peopleSelector/radio.tpl.html', 'directives/peopleSelector/selectorDialogue.tpl.html', 'directives/signups/signupDialogue.tpl.html', 'directives/signups/signups.tpl.html', 'directives/tabs/tabs.tpl.html', 'event/templates/details.tpl.html', 'event/templates/event.tpl.html', 'event/templates/eventBookingsSchedule.tpl.html', 'event/templates/eventOccation.tpl.html', 'event/templates/eventOccationDialogue.tpl.html', 'event/templates/eventOccationForm.tpl.html', 'event/templates/eventOccationSchedule.tpl.html', 'event/templates/list.tpl.html', 'event/templates/signupSlot.tpl.html', 'event/templates/signupSlotForm.tpl.html', 'event/templates/signups.tpl.html', 'eventtypes/templates/list.tpl.html', 'group/templates/details.tpl.html', 'group/templates/group.tpl.html', 'group/templates/list.tpl.html', 'group/templates/members_and_signups.tpl.html', 'orders/templates/orders.tpl.html', 'person/templates/accommodation.tpl.html', 'person/templates/arrival.tpl.html', 'person/templates/delete.tpl.html', 'person/templates/form.tpl.html', 'person/templates/list.tpl.html', 'person/templates/orders.tpl.html', 'person/templates/people.tpl.html', 'person/templates/signups.tpl.html', 'person/templates/user.tpl.html', 'resource/templates/accommodation.tpl.html', 'resource/templates/bookingoverview.tpl.html', 'resource/templates/bookingschedule.tpl.html', 'resource/templates/bookingslist.tpl.html', 'resource/templates/delete.tpl.html', 'resource/templates/eventOccationSelector/bookingschedule.tpl.html', 'resource/templates/eventOccationSelector/checkbox.tpl.html', 'resource/templates/eventOccationSelector/radio.tpl.html', 'resource/templates/eventOccationSelector/selectorDialogue.tpl.html', 'resource/templates/form.tpl.html', 'resource/templates/list.tpl.html', 'resource/templates/resource.tpl.html', 'timespan/templates/form.tpl.html', 'timespan/templates/list.tpl.html', 'timespan/templates/timespan.tpl.html']);

angular.module("directives/accommodation/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/accommodation/accommodation.tpl.html",
    "<div ng-form>\n" +
    "	<h4>Sovning</h4>\n" +
    "	<ul class=\"sleepingResources\">\n" +
    "		<li>\n" +
    "			<h6><input type=\"radio\" name=\"accommodationId\" value=\"-1\" ng-model=\"selectedSleepingResourceId\"> Sover inte på konventet</h6>\n" +
    "		</li>\n" +
    "		<li ng-repeat=\"accommodationResource in availableAccommodations\">\n" +
    "			<h6><input type=\"radio\" name=\"accommodationId\" value=\"{{accommodationResource.sleepingResourceId}}\" ng-model=\"selectedSleepingResourceId\" ng-click=\"selectAccommodation(accommodationResource.sleepingResourceId)\"> {{accommodationResource.name}}</h6>\n" +
    "			<ul>\n" +
    "				<li ng-repeat=\"timespan in accommodationResource.eventOccations\">{{timespan|timespanLabel:true}}</li>\n" +
    "			</ul>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<button ng-click=\"saveAccommodation()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("directives/groupMemberships/groupMembershipDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/groupMemberships/groupMembershipDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h4>Gruppmedlemskap</h4>\n" +
    "	<table id=\"groupMembershipTable\">\n" +
    "		<tr class=\"odd\">\n" +
    "			<th>Id</th>\n" +
    "			<th>Namn</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"group in availableGroups\" ng-class-even=\"even\" ng-class-odd=\"odd\" >\n" +
    "			<td>{{group.id}}</td>\n" +
    "			<td>{{group.name}}</td>\n" +
    "			<td><button ng-click=\"addMember(group)\">Lägg till medlemskap</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "</div>");
}]);

angular.module("directives/groupMemberships/groupMemberships.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/groupMemberships/groupMemberships.tpl.html",
    "<div>\n" +
    "	<h4>Gruppmedlemskap</h4>\n" +
    "	<table id=\"groupMembershipTable\">\n" +
    "		<tr class=\"odd\">\n" +
    "			<th>Id</th>\n" +
    "			<th>Namn</th>\n" +
    "			<th>Noteringar</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"membership in memberships\">\n" +
    "			<td>{{membership.id}}</td>\n" +
    "			<td>{{membership.group.name}}</td>\n" +
    "			<td>{{membership|membershipStatus}}</td>\n" +
    "			<td><button ng-click=\"deleteMembership(membership)\">Avsluta medlemskap</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button ng-click=\"showMembershipDialogue()\">Nytt medlemskap</button>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("directives/peopleSelector/checkbox.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/checkbox.tpl.html",
    "<input type=\"checkbox\" ng-click=\"handleClick($event,entity)\"/>{{entity|searchLabel}}");
}]);

angular.module("directives/peopleSelector/radio.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/radio.tpl.html",
    "<input type=\"radio\" name=\"person\" ng-click=\"handleClick($event,entity)\"/>{{entity|searchLabel}}");
}]);

angular.module("directives/peopleSelector/selectorDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/peopleSelector/selectorDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h1>{{dialogHeader}}</h1>\n" +
    "	<div class=\"search-container\">\n" +
    "		<gc-search search-result=\"entities\" selection-scope=\"selectionScope\" search-status=\"searchStatus\"/></gc-search>\n" +
    "	</div>\n" +
    "	<div class=\"people-list-container\">\n" +
    "	<p>{{searchStatus}}</p>\n" +
    "	<ul class=\"people-list\" ng-hide=\"noResults\">\n" +
    " 		<li ng-repeat=\"entity in entities track by entity.type + entity.id\" id=\"multiSelection\">\n" +
    "			<label ng-include=\"listItemContent\"></label>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	</div>\n" +
    "	<button class=\"save-selection\" type=\"button\" ng-click=\"saveSelection() || closeThisDialog()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("directives/signups/signupDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/signups/signupDialogue.tpl.html",
    "<div class=\"dialog-contents\" style=\"\">\n" +
    "    <ul class=\"eventtypes\">\n" +
    "        <li ng-repeat=\"eventType in eventtypes\">\n" +
    "            <h1>{{eventType.name}}</h1>\n" +
    "            <ul class=\"events\">\n" +
    "                <li ng-repeat=\"event in eventType.events\">\n" +
    "                    <h2>{{event.name}}</h2>\n" +
    "                    <ul class=\"eventoccations\">\n" +
    "                        <li ng-repeat=\"eventOccation in event.eventOccations\">\n" +
    "                            <h3>{{eventOccation|eventOccationLabel}}</h3>\n" +
    "                            <ul class=\"signupslots\">\n" +
    "                                <li ng-repeat=\"signupSlot in eventOccation.signupSlots\" ng-controller=\"SignupSlotController\">\n" +
    "                                    {{signupSlot.slotType.name}}\n" +
    "                                    <button  ng-click=\"saveSignup(signupSlot)\" ng-disabled=\"signupButtonIsDisabled\" class=\"signupButton\" >Anmäl</button>\n" +
    "                                    <span class=\"clear\"></span>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>");
}]);

angular.module("directives/signups/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/signups/signups.tpl.html",
    "<div>\n" +
    "	<h4>Signups</h4>\n" +
    "	<table class=\"signupList\">\n" +
    "		<tr>\n" +
    "			<th>När</th>\n" +
    "			<th>Vad</th>\n" +
    "			<th></th>\n" +
    "			<th>Noteringar</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"signup in signups\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td>{{signup.signupSlot.eventOccation.timespan|timespanLabel}}</td>\n" +
    "			<td><a href=\"{{signup.signupSlot.eventOccation.event.id|eventLink}}/signups\" target=\"_self\">{{signup.signupSlot.eventOccation|occationLabel}}</a></td>\n" +
    "			<td></td>\n" +
    "			<td class=\"noteCol\">{{signup|signupStatus}}</td>	\n" +
    "			<td><button ng-click=\"deleteSignup(signup)\">Ta bort</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button ng-click=\"showSignupDialogue()\">Ny anmälning</button>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("directives/tabs/tabs.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("directives/tabs/tabs.tpl.html",
    "<div class=\"tabs\">\n" +
    "    <a ng-repeat=\"tab in tabs\" ng-class=\"{'tab' : true, 'active' : isTabActive(tab) }\" ng-href=\"{{tab.id}}\">{{tab.label}}<span class=\"tabEnd\"></span></a>\n" +
    "</div>\n" +
    "\n" +
    " ");
}]);

angular.module("event/templates/details.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/details.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"form\" name=\"groupform\">\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<label>Namn <input type=\"text\" name=\"name\" ng-model=\"currentEvent.name\" required=\"required\"/><span ng-show=\"currentEvent.name.$error.required\">*</span></label>\n" +
    "			<label>Typ <select ng-model=\"currentEvent.eventtype\" ng-options='t.name for t in eventTypes track by t.id'></select></label>\n" +
    "			<label>Arrangör \n" +
    "				<span class='buttonBox' >\n" +
    "					<a ng-show=\"hasOrganizer()\" href=\"{{currentEvent.organizer|eventOrganizerLink}}\" target=\"_self\">{{currentEvent.organizer|eventOrganizerLabel}}</a>\n" +
    "					<button type=\"button\" gc-selector dialog-header=\"'Sök upp arrangör'\" selection-scope=\"'people,groups'\" on-save-selection=\"selectOrganizer(selection)\">{{selectOrganizerLabel}}</button>\n" +
    "				</span>\n" +
    "			</label>\n" +
    "			<label>Namn i schemat<input type=\"text\" name=\"name\" ng-model=\"currentEvent.scheduleName\" required=\"required\"/></label>\n" +
    "			<label>Synlig i schemat?\n" +
    "				<span class=\"buttonBox\">\n" +
    "					<input type=\"radio\" name=\"visibleInSchedule\" ng-model=\"currentEvent.visibleInSchedule\" ng-value=\"true\"/> Ja\n" +
    "					<input type=\"radio\" name=\"visibleInSchedule\" ng-model=\"currentEvent.visibleInSchedule\" ng-value=\"false\"/> Nej\n" +
    "				</span>\n" +
    "			</label>\n" +
    "			<label>Publik?\n" +
    "				<span class=\"buttonBox\">\n" +
    "					<input type=\"radio\" name=\"visibleInPublicListing\" ng-model=\"currentEvent.visibleInPublicListing\" ng-value=\"true\"/> Ja\n" +
    "					<input type=\"radio\" name=\"visibleInPublicListing\" ng-model=\"currentEvent.visibleInPublicListing\" ng-value=\"false\"/> Nej\n" +
    "				</span>\n" +
    "			</label>\n" +
    "            <label>Intern information <textarea class=\"large\" ng-model=\"currentEvent.internalInfo\"></textarea></label>\n" +
    "	        <button type=\"submit\" ng-click=\"save()\" ng-disabled=\"groupform.$invalid\" ng-class=\"{'disabled' : groupform.$invalid }\"><i class=\"fa fa-save\"></i> {{currentEvent.id ? 'spara':'skapa'}}</button>\n" +
    "	        <span class=\"clear\"></span>\n" +
    "        </fieldset>\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "            <label>Arrangemangsinformation - Ingress <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.preamble\"></textarea></label>	\n" +
    "            <label>Arrangemangsinformation - Text <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.text\"></textarea></label>	\n" +
    "            <label>Arrangemangsinformation - Övrigt <textarea class=\"large\" ui-tinymce ng-model=\"currentEvent.info\"></textarea></label>\n" +
    "		</fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "    </form>\n" +
    "</div>");
}]);

angular.module("event/templates/event.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/event.tpl.html",
    "<div class=\"event\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("event/templates/eventBookingsSchedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventBookingsSchedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='event in events'>\n" +
    "			<h4>{{event.name}}</h4>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"resource in event.resources\">\n" +
    "					<div class=\"eventName\"><a href=\"{{resource.id|resourceLink}}\">{{resource.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"booking in resource.bookings\" \n" +
    "							ng-click=\"deleteBooking($event,resource,booking)\"\n" +
    "							style=\"width:{{booking.width}}px; left:{{booking.offset}}px\">{{booking.label}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccation.tpl.html",
    "<div class=\"event-occation\">\n" +
    "	<h4>{{eventOccation|eventOccationLabel}}<a href=\"#\" class=\"edit-link\" ng-click=\"showEditForm = !showEditForm\"><i class=\"fa fa-edit\"></i></a></h4>\n" +
    "	<span class=\"event-occation-info\">{{eventOccation|eventOccationInfo}}</span>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<event-occation-form ng-show=\"showEditForm\" event-occation=\"eventOccation\" timespans=\"timespans\"></event-occation-form>\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<div class=\"signup-slots\">\n" +
    "		<signup-slot ng-repeat=\"signupSlot in eventOccation.signupSlots\" class=\"signup-slot\" signup-slot=\"signupSlot\" slot-types=\"slotTypes\"></signup-slot><div class=\"resource-bookings\">\n" +
    "			<h5>Plats och resursbokningar</h5>\n" +
    "			<table class=\"signup-slots\">\n" +
    "				<tr><th>Var</th><th>Typ</th><th></th></tr>\n" +
    "				<tr class=\"resourceBookingsContainer\" ng-repeat=\"eventOccationBooking in eventOccation.eventOccationBookings track by eventOccationBooking.id\" >\n" +
    "					<td><a href=\"{{eventOccationBooking.booking.resource|resourceLink}}/schedule\" target=\"_self\">{{eventOccationBooking.booking.resource.name}}</a></td>\n" +
    "					<td>{{eventOccationBooking.booking.resource.resourceType.name}}</td>\n" +
    "					<td class=\"delete-button-col\"><button type='button' ng-click=\"deleteResourceBooking(eventOccationBooking)\"><i class=\"fa fa-times\"></i></button></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "			<button type='button' multi-select=\"true\" gc-selector dialog-header=\"'Sök efter plats eller resurs'\" selection-scope=\"'resources'\" on-save-selection=\"saveResourceBooking(selection)\"><i class=\"fa fa-plus\"></i> Ny bokning</button>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<button ng-hide=\"showNewForm\" type='button' ng-click=\"showNewForm = !showNewForm\"><i class=\"fa fa-plus\"></i> Ny anmälningstyp</button>\n" +
    "	<signup-slot-form ng-show=\"showNewForm\" slot-types=\"slotTypes\" signup-slot=\"signupSlotFormData\"></signup-slot-form>\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationDialogue.tpl.html",
    "<style>\n" +
    "	.eventOccationDialogue label{\n" +
    "		display:inline-block;\n" +
    "		float:none;\n" +
    "	}\n" +
    "	.eventOccationDialogue *{\n" +
    "		float:none;\n" +
    "	}\n" +
    "	\n" +
    "	\n" +
    "</style>\n" +
    "<div class=\"eventOccationDialogue\">\n" +
    "	<h3>Nytt pass</h3>\n" +
    "	<h4>{{event.name}} - {{timespan.name}}</h4>\n" +
    "	<label>\n" +
    "		Passnamn<br/><input type=\"text\"/>\n" +
    "	</label>\n" +
    "	<label>\n" +
    "		Dölj i listningen<br/><input type=\"checkbox\"/>\n" +
    "	</label>\n" +
    "	\n" +
    "	\n" +
    "	\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationForm.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationForm.tpl.html",
    "<div ng-form=\"eventOccationForm\" class=\"event-occation-form\">\n" +
    "	<label>Name <input type=\"text\" ng-model=\"eventOccation.name\" /></label>\n" +
    "	<label>Timespan <select required ng-model=\"eventOccation.timespan\" ng-options=\"timespan|timespanLabel:true for timespan in localTimespans track by timespan.id\"></select></label>\n" +
    "	<fieldset ng-hide=\"eventOccation.timespan.isPublic\">\n" +
    "		<label>Name <input type=\"text\" ng-mode=\"name\" /></label>\n" +
    "		<label>Börjar <input type=\"datetime-local\" ng-model=\"startsAt\"></label>\n" +
    "		<label>Slutar <input type=\"datetime-local\" ng-model=\"endsAt\"></label>\n" +
    "		<fieldset>\n" +
    "		Visa i schemat?\n" +
    "			<div class=\"input-box\">\n" +
    "				<label><input ng-model=\"showInSchedule\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "				<label><input ng-model=\"showInSchedule\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "			</div>\n" +
    "		</fieldset>\n" +
    "	</fieldset>\n" +
    "	<fieldset>\n" +
    "		Dold i listning?\n" +
    "		<div class=\"input-box\">\n" +
    "			<label><input ng-model=\"eventOccation.isHidden\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "			<label><input ng-model=\"eventOccation.isHidden\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	\n" +
    "	<span class=\"clear\"></span>\n" +
    "	<div style=\"display:table\">\n" +
    "		<div style=\"display: table-row\">\n" +
    "			<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"eventOccationForm.$pristine || eventOccationForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{SaveLabel}}</button></div>\n" +
    "			<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "			<div style=\"display: table-cell; width:50%\"><button type=\"button\" class=\"cancel-button right\" ng-click='onCancelClick()'><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "	<span class=\"clear\"></span>	\n" +
    "</div>");
}]);

angular.module("event/templates/eventOccationSchedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/eventOccationSchedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteEventOccation($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("event/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"event-list\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"events by id\">ID</th>\n" +
    "            <th sort-on-click=\"events by name\">Namn</th>\n" +
    "            <th sort-on-click=\"events by eventtype.name,name\">Arrangemangstyp</th>\n" +
    "            <th>Synligt för besökare</th>\n" +
    "            <th>Synligt i schemat</th>\n" +
    "            <th>Arrangör</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"event in events\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td><a href=\"event/{{event.id}}\">{{event.id}}</a></td>\n" +
    "			<td><a href=\"event/{{event.id}}\">{{event.name}}</td>\n" +
    "			<td><a href=\"{{event.eventtype.id|eventTypeLink}}\" target=\"_self\">{{event.eventtype.name}}</td>\n" +
    "			<td>{{event.visibleInPublicListing|yesNoStatus}}</td>\n" +
    "			<td>{{event.visibleInSchedule|yesNoStatus}}</td>\n" +
    "			<td><a href=\"{{event.organizer|eventOrganizerLink}}\" target=\"_self\">{{event.organizer|eventOrganizerLabel}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("event/templates/signupSlot.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signupSlot.tpl.html",
    "<div class=\"signup-slot\">\n" +
    "	<h5>{{signupSlot.slotType.name}}<a href=\"#\" class=\"edit-link\" ng-click=\"showEditForm = !showEditForm\"><i class=\"fa fa-edit\"></i></a></h5>\n" +
    "	<span class=\"signup-slot-info\">{{signupSlot|signupSlotInfo}}</span>\n" +
    "	<signup-slot-form ng-show=\"showEditForm\" signup-slot=\"signupSlot\" slot-types=\"slotTypes\"></signup-slot-form>\n" +
    "	<table>\n" +
    "		<tr>\n" +
    "			<th class=\"nameCol\">Vem</th><th class=\"approvalCol\">Godkänd</th><th class=\"approvalCol\">Har betalt</th><th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"signup in signupSlot.signups\">\n" +
    "			<td><span ng-show=\"signup.group\"><a href=\"{{signup.group.id|groupLink}}\" target=\"_self\">{{signup.group|groupLabel}}</a></span>\n" +
    "				<span ng-show=\"signup.person\"><a href=\"{{signup.person.id|personLink}}\" target=\"_self\">{{signup.person|personLabel:noId}}</a></span>\n" +
    "			</td>\n" +
    "			<td>{{signup.isApproved|yesNoStatus:signupSlot.requiresApproval}}</td>\n" +
    "			<td>{{signup.isPaid|yesNoStatus:signupSlot.signupSlotArticle}}</td>\n" +
    "			<td class=\"delete-button-col\"><button type='button' ng-click=\"deleteSignup(signup)\"><i class=\"fa fa-user-times\"></i></button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button type='button' gc-selector dialog-header=\"selectorHeader\" selection-scope=\"selectionScope\" on-save-selection=\"onSelectEntity(selection)\"><i class=\"fa fa-user-plus\"></i></button>	\n" +
    "</div>");
}]);

angular.module("event/templates/signupSlotForm.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signupSlotForm.tpl.html",
    "<div ng-form=\"signupSlotForm\" class=\"signup-slot-form\">\n" +
    "	<label>Anmälningstyp <select required ng-model=\"signupSlot.slotType\" ng-options=\"slotType.name for slotType in slotTypes track by slotType.id\"></select></label>\n" +
    "	<label>Begränsat antal platser <input type=\"number\" ng-model=\"signupSlot.maximumSignupCount\"/></label>\n" +
    "	<label>Begränsat antal reservplatser <input type=\"number\" ng-model=\"signupSlot.maximumSpareSignupCount\"/></label>\n" +
    "	<fieldset>Kräver godkännande? \n" +
    "		<div class=\"input-box\">\n" +
    "			<label>Ja <input type=\"radio\" ng-model=\"signupSlot.requiresApproval\" ng-value=\"true\"/></label>\n" +
    "			<label>Nej <input type=\"radio\" ng-model=\"signupSlot.requiresApproval\" ng-value=\"false\"/></label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	<fieldset>Dold?\n" +
    "		<div class=\"input-box\">\n" +
    "			<label>Ja <input type=\"radio\" ng-model=\"signupSlot.isHidden\" ng-value=\"true\"/></label>\n" +
    "			<label>Nej <input type=\"radio\" ng-model=\"signupSlot.isHidden\" ng-value=\"false\"/></label>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "	<div style=\"display: table-row\">\n" +
    "		<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"signupSlotForm.$pristine || signupSlotForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{saveLabel}}</button></div>\n" +
    "		<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "		<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onCancelClick()\" class=\"cancel-button right\"><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "	</div>\n" +
    "	<span class=\"clear\"></span>\n" +
    "</div>");
}]);

angular.module("event/templates/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("event/templates/signups.tpl.html",
    "<div class=\"sectionContent event-occation-view\">\n" +
    "	<h3 class=\"event-occation-header\">Arrangmangspass</h3>\n" +
    "	<button type='button' ng-hide=\"showNewForm\" ng-click=\"showNewForm = true\"><i class=\"fa fa-plus\"></i> Nytt pass</button>\n" +
    "	<div ng-show=\"showNewForm\">\n" +
    "		<h4>Nytt arrangemangspass</h4>\n" +
    "		<event-occation-form timespans=\"timespans\"/>\n" +
    "	</div>\n" +
    "	<div class=\"event-occations\">\n" +
    "		<event-occation ng-repeat=\"eventOccation in currentEvent.eventOccations\" event-occation=\"eventOccation\" timespans=\"timespans\" slot-types=\"slotTypes\"></event-occation>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("eventtypes/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("eventtypes/templates/list.tpl.html",
    "<div class=\"event-type\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"sectionContent content\">\n" +
    "		<table class=\"event-type-list\" sortable-collection>\n" +
    "			<tr class=\"even\">\n" +
    "				<th class=\"nameCol\" sort-on-click=\"eventtypes by name\">Name</th>\n" +
    "			</tr>\n" +
    "			<tr ng-repeat=\"eventtype in eventtypes\" ng-controller=\"eventTypeItemController\">\n" +
    "				<td>\n" +
    "					<a ng-hide='inEditMode' href=\"#\" ng-click=\"$event.preventDefault();enterEditMode();\">{{eventtype.name}} <i class=\"fa fa-edit\"></i></a>\n" +
    "					<span ng-show=\"inEditMode\"><input type=\"text\" ng-model=\"eventtype.name\" />\n" +
    "						<button title=\"spara\" type=\"button\" href=\"#\" ng-click=\"save();\"><i class=\"fa fa-save\"></i></button> \n" +
    "						<button title=\"avbryt\" type=\"button\" href=\"#\" ng-click=\"cancel();\"><i class=\"fa fa-ban\"></i></button>\n" +
    "						<button title=\"ta bort\" type=\"button\" href=\"#\" ng-click=\"remove();\"><i class=\"fa fa-times\"></i></button>\n" +
    "					</span>\n" +
    "				</td>\n" +
    "			</tr>\n" +
    "			<tr ng-controller=\"eventTypeItemController\" ng-init=\"{eventtype : { name: '', id : 0 }}\">\n" +
    "				<td>\n" +
    "					<h3>Skapa ny arrangemangstyp</h3>\n" +
    "					<span><input type=\"text\" ng-model=\"eventtype.name\" />\n" +
    "						<button title=\"skapa\" type=\"button\" href=\"#\" ng-click=\"create();\"><i class=\"fa fa-save\"></i></button> \n" +
    "					</span>\n" +
    "				</td>\n" +
    "			</tr>\n" +
    "		</table>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("group/templates/details.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/details.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"form\" name=\"groupform\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Namn <input type=\"text\" name=\"name\" ng-model=\"currentGroup.name\" required=\"required\"/><span ng-show=\"currentGroup.name.$error.required\">*</span></label>\n" +
    "            <label>Epostadress <input type=\"email\" name=\"email_address\" ng-model=\"currentGroup.emailAddress\" /></label>\n" +
    "            <label>Gruppledare <select ng-model=\"currentGroup.leaderMembershipId\" ng-options=\"value.id as value.person.firstName +' ' + value.person.lastName + ' ('+ value.person.id + ')' for value in currentGroup.groupMembers\"></select></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "             <label>Övriga noteringar <textarea class=\"large\" ng-model=\"currentGroup.note\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Intern information <textarea class=\"large\" ng-model=\"currentGroup.internal_note\"></textarea></label>	\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"save()\" ng-disabled=\"groupform.$invalid\" ng-class=\"{'disabled' : groupform.$invalid }\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "	<pre style=\"display:none\">{{currentGroup|json}}</pre>\n" +
    "</div>");
}]);

angular.module("group/templates/group.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/group.tpl.html",
    "<div class=\"group\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("group/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"personList\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"groups by id\">ID</th>\n" +
    "            <th sort-on-click=\"groups by name\">Gruppnamn</th>\n" +
    "            <th sort-on-click=\"groups by emailAddress\">Epostadress</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"group in groups\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.id}}</a></td>\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.name}}</a></td>\n" +
    "            <td><a href=\"group/{{group.id}}\">{{group.emailAddress}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("group/templates/members_and_signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("group/templates/members_and_signups.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "	<h3>Medlemmar</h3>\n" +
    "	<table id=\"personList\">\n" +
    "		<tr>\n" +
    "			<th class=\"idColumn\">ID</th>\n" +
    "			<th>Förnamn</th>\n" +
    "			<th>Efternamn</th>\n" +
    "			<th>Epostadress</th>\n" +
    "			<th>Gruppledare</th>\n" +
    "			<th></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"membership in currentGroup.groupMembers\">\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.id}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.firstName}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.lastName}}</a></td>\n" +
    "			<td><a href=\"{{membership.person.id|personLink}}\" target='_self'>{{membership.person.emailAddress}}</a></td>\n" +
    "			<td><span ng-show=\"membership.id == currentGroup.leaderMembershipId\">Gruppledare</span></td>\n" +
    "			<td><button type=\"button\" ng-click=\"deleteMembership(membership)\">Ta bort medlem</button></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<button type=\"button\" gc-selector on-save-selection=\"addMembers(selection)\">Lägg till medlemmar</button>\n" +
    "\n" +
    "	<h3>Anmälningar</h3>\n" +
    "	<signups current-group=\"currentGroup\"/>\n" +
    "</div>");
}]);

angular.module("orders/templates/orders.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("orders/templates/orders.tpl.html",
    "<div>\n" +
    "	<table class=\"resourceList\">\n" +
    "		<tr class=\"even\">\n" +
    "			<th class=\"idCol\">Id</th>\n" +
    "			<th>Datum</th>\n" +
    "			<th class=\"typeCol\">Ordertotal</th>\n" +
    "			<th>Orderstatus</th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"order in orders\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.id}}</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.submittedAt.date|amDateFormat:'YYYY-MM-DD HH:mm'}}</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.total}}:-</a></td>\n" +
    "			<td><a target=\"_self\" href=\"order/{{order.id}}\">{{order.status|orderStatus}}</a></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "	<a href=\"{{currentPerson.id|newOrderLink}}\" target=\"_self\">Ny förbeställning</a>\n" +
    "</div>");
}]);

angular.module("person/templates/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/accommodation.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<accommodation-selection class=\"sectionBox\" current-person=\"person\"></accommodation-selection>\n" +
    " </div>");
}]);

angular.module("person/templates/arrival.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/arrival.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "			<a ng-href=\"{{person.id|registrationLink}}\" target=\"_self\"><button type=\"button\">Till ankomstregistrering</button></a>\n" +
    "        </div>");
}]);

angular.module("person/templates/delete.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/delete.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "			<button ng-click=\"deletePerson()\" type=\"button\">Ta bort</button>\n" +
    "        </div>");
}]);

angular.module("person/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"person-form\" name=\"form\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Förnamn <input type=\"text\" name=\"firstName\" ng-model=\"person.firstName\" required=\"required\"/><span ng-show=\"form.firstName.$error.required\">*</span></label>\n" +
    "            <label>Efternamn <input type=\"text\" name=\"lastName\" ng-model=\"person.lastName\" required=\"required\"/><span ng-show=\"form.lastName.$error.required\">*</span></label>\n" +
    "            <label>Epostadress <input type=\"email\" name=\"emailAddress\" ng-model=\"person.emailAddress\" required=\"required\"/><span ng-show=\"form.emailAddress.$error.required\">*</span></label>\n" +
    "            <label>Privat epostadress? \n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"emailIsPublic\" ng-model=\"person.emailIsPublic\" ng-value=\"false\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"emailIsPublic\" ng-model=\"person.emailIsPublic\" ng-value=\"true\"/> Nej\n" +
    "                </span>\n" +
    "            </label>\n" +
    "            <label>Personnummer <input type=\"text\" ng-model=\"person.identification\" ng-pattern=\"/^[0-9]{6}( |-)?[0-9]{4}$/\"/></label>\n" +
    "            <label>Telefonnummer <input type=\"text\" ng-model=\"person.phoneNumber\"/></label>\n" +
    "            <label>Gatuadress <input type=\"text\" ng-model=\"person.streetAddress\"/></label>\n" +
    "            <label>Postnummer <input type=\"text\" ng-model=\"person.zip\"/></label>\n" +
    "            <label>Stad <input type=\"text\" ng-model=\"person.city\"/></label>\n" +
    "            <label>Utländsk adress <textarea rows=\"3\" ng-model=\"person.foreignAddress\"></textarea></label>\n" +
    "            <label>Ignorera formulärfel  <span class=\"buttonBox\"><input type=\"checkbox\" name=\"override\" value=\"override\" ng-model=\"override\"/></span></label>\n" +
    "       </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "             <label>Övriga noteringar <textarea class=\"large\" ng-model=\"person.note\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label for=\"internal_note\">Intern information <textarea class=\"large\" ng-model=\"person.internalNote\"></textarea></label>	\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"savePerson()\" ng-disabled=\"!(form.override.$modelValue || form.$valid) || disableSaveButton\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "    <script>\n" +
    "        $(\"#person-form input[type=radio]\").each(function(key,obj){\n" +
    "            var $obj = $(obj);\n" +
    "            var $parent =$obj.parent(\".buttonBox\");\n" +
    "            $obj.bind(\"focus\",function(){\n" +
    "                $parent.addClass(\"focus\");\n" +
    "            }).bind(\"blur\",function(){\n" +
    "                $parent.removeClass(\"focus\");\n" +
    "            });\n" +
    "        });\n" +
    "    </script>\n" +
    "</div>");
}]);

angular.module("person/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"personList\" sortable-collection >\n" +
    "        <tr>\n" +
    "            <th class=\"idColumn\" sort-on-click=\"people by id\">ID</th>\n" +
    "            <th sort-on-click=\"people by firstName,lastName\">Förnamn</th>\n" +
    "            <th sort-on-click=\"people by lastName,firstName\">Efternamn</th>\n" +
    "            <th sort-on-click=\"people by emailAddress\">Epostadress</th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"person in people\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.id}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.firstName}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.lastName}}</a></td>\n" +
    "            <td><a href=\"person/{{person.id}}\">{{person.emailAddress}}</a></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("person/templates/orders.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/orders.tpl.html",
    "<div  class=\"sectionContent\" >\n" +
    "	<personal-orders class=\"sectionBox\" current-person=\"person\"></personal-orders>\n" +
    "</div>");
}]);

angular.module("person/templates/people.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/people.tpl.html",
    "<div class=\"people\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("person/templates/signups.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/signups.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<signups class=\"sectionBox\" current-person=\"person\"></signups>\n" +
    "	<personal-group-memberships class=\"sectionBox\" memberships=\"person.groupMemberships\" current-user-id=\"person.id\"></group-memberships>\n" +
    "</div>");
}]);

angular.module("person/templates/user.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("person/templates/user.tpl.html",
    "<div class=\"sectionContent\" >\n" +
    "    <form name=\"userForm\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Användarnamn <input type=\"text\" ng-model=\"user.username\"/></label>\n" +
    "            <label>Lösenord <input type=\"password\" ng-model=\"user.password\" name=\"password\" /></label>\n" +
    "            <label>Upprepa lösenord <input type=\"password\" ng-model=\"user.password_repeat\" name=\"password_repeat\" gc-must-match=\"password\"/></label>\n" +
    "            <span class=\"clear\"></span>\n" +
    "            <label>Aktiv? <span class=\"buttonBox\"><input type=\"checkbox\" ng-model=\"user.isActive\"/></span></label>\n" +
    "            <label>Användarnivå \n" +
    "                <select ng-model=\"user.level\">\n" +
    "                    <option value=\"0\">Besökare</option>\n" +
    "                    <option value=\"1\">Användare</option>\n" +
    "                    <option value=\"20\">Arrangör</option>\n" +
    "                    <option value=\"30\">Arbetare</option>\n" +
    "                    <option value=\"40\">Ledning</option>\n" +
    "                    <option value=\"100\">Administratör</option>\n" +
    "                </select>\n" +
    "            </label>\n" +
    "        </fieldset>\n" +
    "        <span class=\"clear\"></span>\n" +
    "        <button ng-click=\"saveUser($event)\" ng-disabled=\"!userForm.$valid || disableSaveButton\">Skicka</button>\n" +
    "    </form>\n" +
    "</div>");
}]);

angular.module("resource/templates/accommodation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/accommodation.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<h3>Sovplatser och sovplatsbokningar</h3>\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "	<fieldset class=\"formBox\">\n" +
    "		<h4>Inbokade sovtillfällen</h4>\n" +
    "		<table>\n" +
    "			<thead>\n" +
    "				<tr>\n" +
    "					<th>Inbokad</th>\n" +
    "					<th>När</th>\n" +
    "				</tr>\n" +
    "			</thead>\n" +
    "			<tbody>\n" +
    "				<tr ng-repeat=\"occation in sleepingEventOccations\">\n" +
    "					<td><input type=\"checkbox\" value=\"{{occation.id}}\" ng-checked=\"occation.selected\" ng-click=\"updateOccationSelection($event,occation.id)\"/></td>\n" +
    "					<td>{{occation.timespan.name}} - ({{occation.timespan|timespanLabel}})</td>\n" +
    "				</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "		<button type=\"button\" ng-click=\"updateOccations()\">Uppdatera</button>\n" +
    "	</fieldset>\n" +
    "	<fieldset class=\"formBox\">\n" +
    "		<div  class=\"sleeperListContainer\" style=\"width:400px; float:left; margin-left:10px;\">\n" +
    "			<h4>Inbokade sovande</h4>\n" +
    "			<table>\n" +
    "				<colgroup>\n" +
    "					<col class=\"idCol\" />\n" +
    "					<col class=\"firstNameCol\"/>\n" +
    "					<col class=\"lastNameCol\"/>\n" +
    "					<col class=\"buttonsCol\"/>\n" +
    "				</colgroup>\n" +
    "				<tr class=\"even\">\n" +
    "					<th>Id</th>\n" +
    "					<th>Förnamn</th>\n" +
    "					<th>Efternamn</th>\n" +
    "					<th></th>\n" +
    "				</tr>\n" +
    "				<tr ng-repeat=\"accommodation in resource.sleepingResource.sleepingSlotBookings\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.id}}</a></td>\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.firstName}}</a></td>\n" +
    "					<td><a href=\"{{accommodation.person.id|personLink}}\" target=\"_self\">{{accommodation.person.lastName}}</a></td>\n" +
    "					<td><button type=\"button\" ng-click=\"deleteAccommodation(accommodation)\">Ta bort</button></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "			<button type=\"button\" people-selector multi-select on-people-select=\"onSaveAccommodations(people)\">Lägg till fler sovande</button>\n" +
    "		</div>\n" +
    "	</fieldset>\n" +
    "</div>\n" +
    "				\n" +
    "			");
}]);

angular.module("resource/templates/bookingoverview.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingoverview.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='resource in resources'>\n" +
    "			<h3><a href=\"resource/{{resource.id}}\">{{resource.name}}</a></h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in resource.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\" target=\"_self\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createBooking($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteBooking($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("resource/templates/bookingschedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingschedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\" target=\"_self\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createBooking($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteBooking($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("resource/templates/bookingslist.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/bookingslist.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<h4 class=\"sectionHeader\">Bokningslista</h4>\n" +
    "	<div>\n" +
    "		<table >\n" +
    "			<thead>\n" +
    "			<tr class=\"even\" sortable-collection>\n" +
    "				<th class=\"timeCol\">När</th>\n" +
    "				<th class=\"nameCol\">Vad</th>\n" +
    "				<th class=\"buttonsCol\"></th>\n" +
    "			</tr>\n" +
    "			</thead>\n" +
    "			<tbody>\n" +
    "				<tr ng-class-even=\"event\" ng-class-odd=\"odd\" ng-repeat=\"booking in bookings\">\n" +
    "					<td><a href=\"{{booking.eventOccationBooking.eventOccation.event.id|eventLink}}/signups\" target=\"_self\">{{booking.eventOccationBooking.eventOccation.timespan|simpleTimespanLabel}}</a></td>\n" +
    "					<td><a href=\"{{booking.eventOccationBooking.eventOccation.event.id|eventLink}}\" target=\"_self\">{{booking|bookingLabel}}</a></td>\n" +
    "					<td><button type=\"button\" ng-click=\"deleteBooking(booking)\">Ta bort</button></td>\n" +
    "				</tr>\n" +
    "			</tbody>\n" +
    "		</table>\n" +
    "		<button type=\"button\" resource-id=\"resource.id\" event-occation-selector on-save-selection='saveEventOccationBookings(occations)'>Lägg till bokning</button>\n" +
    "	</div>                   \n" +
    "</div>");
}]);

angular.module("resource/templates/delete.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/delete.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<button ng-click=\"delete()\" type=\"button\">Ta bort</button>\n" +
    "</div>");
}]);

angular.module("resource/templates/eventOccationSelector/bookingschedule.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/bookingschedule.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<div class=\"event-occation-schedule-container\" style=\"width:{{scheduleWidth}}px\">\n" +
    "		<ul class=\"date-list\">\n" +
    "			<li ng-repeat=\"date in dates\" style=\"left:{{date.offset}}px; width:{{date.width}}px\">{{date.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"hour-list\">\n" +
    "			<li ng-repeat=\"hour in hours\" ng-class-even=\"even\" style=\"left:{{hour.offset}}px; width:{{hour.width}}px\" ng-mousedown=\"enterSelectionMode($event)\" ng-mouseover=\"select($event)\" ng-mouseup=\"leaveSelectionMode($event)\" >{{hour.label}}</li>\n" +
    "		</ul>\n" +
    "		<ul class=\"main-timespan-list timespan-list\">\n" +
    "			<li ng-repeat=\"timespan in publicTimespans\" style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "		</ul>\n" +
    "		<div ng-repeat='eventType in eventTypes'>\n" +
    "			<h3>{{eventType.name}}</h3>\n" +
    "			<ul class=\"event-list\" >\n" +
    "				<li ng-repeat=\"event in eventType.events\">\n" +
    "					<div class=\"eventName\"><a href=\"{{event.id|eventLink}}/signups\">{{event.name}}</a></div>\n" +
    "					<ul class=\"timespan-list event-timespan-list\">\n" +
    "						<li ng-repeat=\"timespan in publicTimespans\" \n" +
    "							ng-click=\"createEventOccation($event,event,timespan)\"\n" +
    "							style=\"left:{{timespan.offset}}px; width:{{timespan.width}}px\">{{timespan.name}}</li>\n" +
    "					</ul>\n" +
    "					<ul class=\"timespan-list event-occation-list\">\n" +
    "						<li ng-repeat=\"eventOccation in event.eventOccations\" \n" +
    "							ng-click=\"deleteEventOccation($event,event,eventOccation)\"\n" +
    "							style=\"width:{{eventOccation.width}}px; left:{{eventOccation.offset}}px\">{{eventOccation.name}}</li>\n" +
    "					</ul>\n" +
    "				</li>\n" +
    "			</ul>\n" +
    "		</div>\n" +
    "	</div>\n" +
    "</div>");
}]);

angular.module("resource/templates/eventOccationSelector/checkbox.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/checkbox.tpl.html",
    "<input type=\"checkbox\" ng-click=\"handleClick($event,person)\"/>{{person|searchLabel}}");
}]);

angular.module("resource/templates/eventOccationSelector/radio.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/radio.tpl.html",
    "<input type=\"radio\" name=\"person\" ng-click=\"handleClick($event,person)\"/>{{person|searchLabel}}");
}]);

angular.module("resource/templates/eventOccationSelector/selectorDialogue.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/eventOccationSelector/selectorDialogue.tpl.html",
    "<div class=\"dialog-contents\">\n" +
    "	<h1>{{dialogHeader}}</h1>\n" +
    "	<div class=\"event-list-container\">\n" +
    "		<p ng-show=\"noResults\">Inga arrangemang kunde hittas</p>\n" +
    "		<ul class=\"event-list\" ng-hide=\"noResults\" >\n" +
    "			<li ng-repeat=\"event in events\" class=\"event\">\n" +
    "				<input type=\"checkbox\" value='person' ng-click='selectEventOccations(event,$event)' ng-disabled=\"allOccationsAreBooked(event)\"/>{{event.name}}\n" +
    "				<ul class=\"occation-list\">\n" +
    "					<li class=\"occation\" ng-repeat=\"occation in event.eventOccations\" ng-class=\"{'colliding' : !occation.ok_to_book}\" ng-form>\n" +
    "						<input ng-disabled=\"occation.already_booked\" type=\"checkbox\"\n" +
    "							   ng-model='formdata.eventOccations[occation.id]'\n" +
    "							   ng-true-value=\"{{occation.id}}\"\n" +
    "							   ng-false-value=\"false\"/>{{occation|eventOccationLabel}}\n" +
    "					</li>\n" +
    "				</ul>\n" +
    "			</li>\n" +
    "		</ul>\n" +
    "	</div>\n" +
    "	<button class=\"save-selection\" type=\"button\" ng-click=\"saveSelection() || closeThisDialog()\">Spara</button>\n" +
    "</div>");
}]);

angular.module("resource/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div class=\"message\" ng-show=\"message\" ng-bind=\"message\"></div>\n" +
    "    <form id=\"angular-form\" name=\"form\">\n" +
    "        <fieldset class=\"formBox\">\n" +
    "            <label>Namn <input type=\"text\" name=\"name\" ng-model=\"resource.name\" required=\"required\"/><span ng-show=\"form.name.$error.required\">*</span></label>\n" +
    "            <label>Type <select ng-model=\"resource.resourceType\" name=\"type\" required=\"required\" ng-options=\"type.name for type in resourceTypes track by type.id\">\n" +
    "                </select><span ng-show=\"form.type.$error.required\">*</span></label>\n" +
    "            <label>Tillgänglig?\n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"available\" ng-model=\"resource.available\" ng-value=\"true\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"available\" ng-model=\"resource.available\" ng-value=\"false\"/> Nej\n" +
    "                </span></label>\n" +
    "			<label>Användbar för sovning?\n" +
    "                <span class=\"buttonBox\">\n" +
    "                    <input type=\"radio\" name=\"sleepingResourceIsAvailable\" ng-model=\"resource.sleepingResource.isAvailable\" ng-value=\"true\"/> Ja\n" +
    "                    <input type=\"radio\" name=\"sleepingResourceIsAvailable\" ng-model=\"resource.sleepingResource.isAvailable\" ng-value=\"false\"/> Nej\n" +
    "                </span></label>\n" +
    "			<label ng-show=\"resource.sleepingResource.isAvailable\">Antal sovplatser <input type=\"text\" ng-model=\"resource.sleepingResource.noOfSlots\"/></label>\n" +
    "		</fieldset>\n" +
    "		<fieldset class=\"formBox\">\n" +
    "             <label>Beskrivning <textarea class=\"large\" ng-model=\"resource.description\"></textarea></label>\n" +
    "        </fieldset>\n" +
    "	    <span class=\"clear\"></span>\n" +
    "        <div>\n" +
    "            <button type=\"submit\" ng-click=\"saveResource()\" ng-disabled=\"!(form.override.$modelValue || form.$valid) || disableSaveButton\">Skicka</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "    <script>\n" +
    "        $(\"#person-form input[type=radio]\").each(function(key,obj){\n" +
    "            var $obj = $(obj);\n" +
    "            var $parent =$obj.parent(\".buttonBox\");\n" +
    "            $obj.bind(\"focus\",function(){\n" +
    "                $parent.addClass(\"focus\");\n" +
    "            }).bind(\"blur\",function(){\n" +
    "                $parent.removeClass(\"focus\");\n" +
    "            });\n" +
    "        });\n" +
    "    </script>\n" +
    "</div>");
}]);

angular.module("resource/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "	<table class=\"resourceList\">\n" +
    "		<tr class=\"even\">\n" +
    "			<th class=\"idCol\"><a href=\"\">Id</a></th>\n" +
    "			<th class=\"nameCol\"><a href=\"\">Namn</a></th>\n" +
    "			<th class=\"typeCol\"><a href=\"\">Typ</a></th>\n" +
    "			<th><a href=\"\">Tillgänglig</a></th>\n" +
    "			<th><a href=\"\">Sovsal</a></th>\n" +
    "		</tr>\n" +
    "		<tr ng-repeat=\"resource in resources\">\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].id}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].name}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].resourceType.name}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}\">{{resource[0].available|yesNoStatus}}</a></td>\n" +
    "			<td><a href=\"resource/{{resource[0].id}}/accommodation\">{{resource|sleepingResourceStatus}}</a></td>\n" +
    "		</tr>\n" +
    "	</table>\n" +
    "</div>");
}]);

angular.module("resource/templates/resource.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("resource/templates/resource.tpl.html",
    "<div class=\"resource\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

angular.module("timespan/templates/form.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/form.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <div ng-form=\"timespanForm\" class=\"timespan-form\">\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<h4 ng-show=\"currentTimespan.id\">Passdetaljer</h4>\n" +
    "			<label>Namn <input type=\"text\" ng-model=\"currentTimespan.name\" /></label>\n" +
    "			<label>Starts at<input ng-model=\"startsAt\" type=\"datetime-local\" /></label>\n" +
    "			<label>Ends at<input ng-model=\"endsAt\" type=\"datetime-local\"/></label>\n" +
    "			<fieldset>\n" +
    "				Visa i schemat?\n" +
    "				<div class=\"input-box\">\n" +
    "					<label><input ng-model=\"currentTimespan.showInSchedule\" type=\"radio\" ng-value=\"true\"/> ja</label>\n" +
    "					<label><input ng-model=\"currentTimespan.showInSchedule\" type=\"radio\" ng-value=\"false\"/> nej</label>\n" +
    "				</div>\n" +
    "			</fieldset>\n" +
    "			<span class=\"clear\"></span>\n" +
    "			<div style=\"display:table\">\n" +
    "				<div style=\"display: table-row\">\n" +
    "					<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-click=\"onSaveClick()\" ng-disabled=\"timespanForm.$pristine || timespanForm.$invalid\" class=\"save-button left\"><i class=\"fa fa-save\"></i> {{saveLabel}}</button></div>\n" +
    "					<div style=\"display: table-cell; white-space: nowrap\"><button type=\"button\" ng-show=\"onDeleteClick\" ng-click=\"onDeleteClick()\" class=\"left delete-button\"><i class=\"fa fa-times \"></i> Ta bort</button></div>\n" +
    "					<div style=\"display: table-cell; width:50%\"><button type=\"button\" ng-show=\"onCancelClick\" class=\"cancel-button right\" ng-click='onCancelClick()'><i class=\"fa fa-ban\"></i> avbryt</button></div>\n" +
    "				</div>\n" +
    "			</div>\n" +
    "			\n" +
    "		</fieldset>\n" +
    "		<fieldset class=\"wideFormBox\">\n" +
    "			<h4 ng-show=\"currentTimespan.id\">Inbokade pass</h4>\n" +
    "			 <table id=\"timespan-event-occations\" sortable-collection>\n" +
    "				<tr>\n" +
    "					<th>Arrangemang</th>\n" +
    "					<th>Passnamn</th>\n" +
    "					<th>Arrangemangstyp</th>\n" +
    "					<th></th>\n" +
    "				</tr>\n" +
    "				<tr ng-repeat=\"eventOccation in currentTimespan.eventOccations\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "					<td><a href=\"{{eventOccation.event.id|eventLink}}\">{{eventOccation.event.name}}</a></td>\n" +
    "					<td><a href=\"{{eventOccation.event.id|eventLink}}/signups\">{{eventOccation.name}}</a></td>\n" +
    "					<td><a href=\"{{eventOccation.event.eventtype.id|eventTypeLink}}\">{{eventOccation.event.eventtype.name}}</a></td>\n" +
    "					<td></td>\n" +
    "				</tr>\n" +
    "			</table>\n" +
    "		</fieldset>\n" +
    "	</div>	\n" +
    "</div>");
}]);

angular.module("timespan/templates/list.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/list.tpl.html",
    "<div class=\"sectionContent\">\n" +
    "    <table id=\"timespanList\" sortable-collection>\n" +
    "        <tr>\n" +
    "            <th sort-on-click=\"timespans by name\">Namn</th>\n" +
    "            <th sort-on-click=\"timespans by startsAt\">Startar</th>\n" +
    "            <th sort-on-click=\"timespans by endsAt\">Slutar</th>\n" +
    "			<th></th>\n" +
    "        </tr>\n" +
    "        <tr ng-repeat=\"timespan in timespans\" ng-class-even=\"even\" ng-class-odd=\"odd\">\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.name}}</a></td>\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.startsAt|date}}</a></td>\n" +
    "            <td><a href=\"{{timespan.id|timespanLink}}\">{{timespan.endsAt|date}}</a></td>\n" +
    "            <td></td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>");
}]);

angular.module("timespan/templates/timespan.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("timespan/templates/timespan.tpl.html",
    "<div class=\"group\">\n" +
    "	<h2>{{title}}</h2>\n" +
    "	<div class=\"content\"></div>\n" +
    "</div>");
}]);

(function(){
/*jshint strict:false */
"use strict";

angular.module("Timespan",['GothCon'])
	.provider("TimespanRepository", function TimespanRepositoryProvider(){
		var _resource;

		return{
			$get : function TimespanRepository($resource,linkBuilder,gcLoaderBusInterceptor){
				_resource = $resource(linkBuilder.createAdminLink("timespan") + "/:id",{},{	
					'get'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor },
					'save'		: {method:'POST'	, interceptor : gcLoaderBusInterceptor },
					'query'		: {method:'GET'		, interceptor : gcLoaderBusInterceptor, isArray:true },
					'delete'	: {method:'DELETE'	, interceptor : gcLoaderBusInterceptor }
				});

				return {
					'get'	: _resource.get,
					'delete': function(timespanId,callback){
						callback = callback || angular.noop;
						return _resource.delete({id : timespanId},callback);
					},
					'save'	: function(timespanData,callback){
						callback = callback || angular.noop;
						var urlParams = {};
						if(timespanData.id){
							urlParams.id = timespanData.id;
						}
						return _resource.save(urlParams,timespanData,callback);
					},
					'getAll': _resource.get,
					'createNew' : function(){
						var now = moment().format("YYYY-MM-DD HH:mm");
						
						return {
							startsAt : now ,endsAt : now,name : "", isPublic : true
						};
					}
				};
			}
		};
	})
	.directive("timespan",function(){
		var extractIdRegex = /^\/timespan\/(\d+|new)(\/|$)/;
		var initialData;
		return {
			controller : function($scope,$location,gcTabsBus,TimespanRepository,$element,$compile){

				function initialize(){
					var $content = angular.element($element.find("div")[0]);
					$scope.view = "";
					$scope.$watch("view",function(newValue,oldValue){
						if(newValue !== oldValue && newValue !== ""){
							$content.contents().remove();
							var $view = $compile(angular.element(newValue))($scope);
							$content.append($view);
						}
					});
					$scope.$on('$locationChangeSuccess',onLocationChange);
				}

				function onLocationChange(){
					var match = extractIdRegex.exec($location.path());
					var id = (match !== null && match.length > 1) ? match[1] : "";

					if(id === ""){
						// this is the list view
						onShowList();
						return;
					}
					else if(id === "new")
					{
						onShowNew();
						return;
					}
					else{
						onShow(id);
						return;
					}

				}

				function onShowList(){
					$scope.title = "Intervall";
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/new', 'label' : 'Nytt intervall'}]);
					$scope.currentTimespan = undefined;
					$scope.timespanList = TimespanRepository.getAll({},function(response){
						$scope.timespanList = response.data.data;
						$scope.view = "<timespan-list timespans='timespanList'></group-list>";
					});
					return;
				}

				function onShowNew(){
					$scope.currentTimespan = TimespanRepository.createNew();
					$scope.title = "Nytt intervall";
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/new', 'label' : 'Nytt intervall'}]);
					$scope.view = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
				}

				function onShow(id){
					var newView;
					gcTabsBus.setTabs([
						{ id : 'timespan/', 'label' : 'Intervallistning'},
						{ id : 'timespan/' + id + '/', 'label' : 'Detaljer'}]);
					switch($location.path()){
						case '/timespan/' + id +"/bookings":
							newView = "<timespan-bookings current-timespan='currentTimespan'></timespan-bookings>";
							break;
						case '/timespan/' + id :
							newView = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
							break;
						default:
							newView = "<timespan-form current-timespan='currentTimespan'></timespan-form>";
					}

					if(!$scope.currentTimespan || "" + $scope.currentTimespan.id !== "" + id){
						TimespanRepository.get({ id : id},function(response){	
							$scope.currentTimespan = response.data.data;
							$scope.title = $scope.currentTimespan.name;
							$scope.view = newView;
						});
					}else{
						$scope.view = newView;
					}
				}

				initialize();

			},
			scope : {

			},
			replace : true,
			templateUrl : "timespan/templates/timespan.tpl.html",
			restrict : 'E'				
		};
	})
	.directive("timespanList",function(){
		return {
			controller : function($scope,$location,gcTabsBus,TimespanRepository,$element,$compile){

			},
			replace : true,
			templateUrl : "timespan/templates/list.tpl.html",
			restrict : 'AE',
			scope : { 
				timespans : "=?"
			}
		};
	})
	.directive("timespanForm",function(){
		return {
			controller : function($scope,TimespanRepository,$location){
				$scope.startsAt = new Date($scope.currentTimespan.startsAt);
				$scope.endsAt = new Date($scope.currentTimespan.endsAt);
				$scope.saveLabel = $scope.currentTimespan.id ? "Uppdatera" : "Skapa";
				$scope.onSaveClick = function(){
					var timespanData = angular.copy($scope.currentTimespan);
					timespanData.startsAt = moment($scope.startsAt).format("YYYY-MM-DD HH:mm");
					timespanData.endsAt = moment($scope.endsAt).format("YYYY-MM-DD HH:mm");
					
					TimespanRepository.save(timespanData,function saveTimespanCallback(response){
						if(!timespanData.id){
							$location.url("/timespan");
						}
						console.log(response);
					});
				};
				if($scope.currentTimespan && $scope.currentTimespan.id){
					$scope.onDeleteClick = function(){
						TimespanRepository.delete($scope.currentTimespan.id,function deleteTimespanCallback(response){
						
						});
					};
					$scope.onCancelClick = function(){
						
					};
				}else{
					$scope.currentTimespan = TimespanRepository.createNew();
				}
			},
			replace : true,
			templateUrl : "timespan/templates/form.tpl.html",
			restrict : 'AE',
			scope : { 
				currentTimespan : "=?"
			}
		};
	});
}());