/*! GothCon - v1.0,0 - 2016-09-17
* Copyright (c) 2016 Joakim Ekeblad; Licensed  */
//(function(){
///*jshint strict:false */
//"use strict";
//angular.module("PublicPerson",[])
//	.provider("urls", function urlsProvider(){
//		var templatesUrl 	=  "resources/javascripts/angular/app/templates/";
//		var directivesUrl 	=  "resources/javascripts/angular/app/directives/";
//		var applicationUrl 	=  "/webreg/";
//		
//        return{
//			setApplicationUrl: function(url){
//				applicationUrl = url;
//			},			
//			getApplicationUrl : function(){
//				return applicationUrl;
//			},			
//			setTemplatesUrl : function(url){
//				templatesUrl = url;
//			},			
//			getTemplatesUrl : function(){
//				return applicationUrl + templatesUrl;
//			},			
//			setDirectivesUrl : function(url){
//				directivesUrl = url;
//			},			
//			getDirectivesUrl : function(){
//				return applicationUrl + directivesUrl;
//			},
//            $get : function urlsFactory(){
//                return {
//					getApplicationUrl : function(){
//						return applicationUrl;
//					},
//					getDirectivesUrl : function(){
//						return applicationUrl + directivesUrl;
//					},
//					getTemplatesUrl : function(){
//						return applicationUrl + templatesUrl;
//					}
//				};
//            }
//        };
//    })
//    .run(function() {
//        
//    })
//    .config(function($locationProvider){
//        $locationProvider.html5Mode(true);
//    })
//}());
//angular.module("PublicPerson")
//	.provider("PersonDataProvider",function PersonDataProviderProvider(){
//		return {
//			$get : function PersonDataProviderFactory(){
//				return {
//
//				};
//			}
//		} ;
//	});
angular.module('templates-angular_public_templates', []);

