<?php 

error_reporting(-1);
setlocale(LC_ALL, "sve");
date_default_timezone_set("Europe/Stockholm");

define("SESSION_STARTED",true);
session_start();

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../lib/ClassLib/ConfigurationManager.php");
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR."../../lib/SessionAccessors/LanguageStringAccessor.php");

header('Content-type: application/javascript'); ?>
$.translate = function(){

	var translationData = $($.parseXML("<?php 

		$absolutePathToConfigFile = dirname(__FILE__) . "/../../conf/Site.ini";
		$configurationManager = new ConfigurationManager($absolutePathToConfigFile);
		$langXML = fopen($configurationManager->getConfigurationValue("LanguageRoot").LanguageStringAccessor::getCurrentLanguage().".xml", "r");
		while(!feof($langXML)){
			$char = fread($langXML,1);
			if($char == '"'){
				echo "\\";
			}
			if(ord($char) == 10){
				continue;
			}

			echo $char;
		}
	?>"));
	return function(path){
			var originalPath = path; 
			path = path.replace(/\//g, " > ").replace(/^>/g,'');
			path = "terms > " + path;
			var translation = translationData.find(path).text();
			return translation ? translation : originalPath;
		};
}();