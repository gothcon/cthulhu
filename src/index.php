<?php 

	ini_set("output_buffering", true);
	ini_set("error_reporting", 6143);
	ini_set("session.use_only_cookies", true);
	ini_set("short_open_tag", false);
	ini_set("default_charset", "UTF-8");
	ini_set("html_errors",true);
	setlocale(LC_ALL, "swedish");
	date_default_timezone_set("Europe/Stockholm");

	require_once("lib/ClassLib/Application.php");
	
	$application = new Application(dirname(__FILE__) . "/conf");
	$application->handleRequest();

?>