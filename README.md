# README

## Installation

Follow this guide to get your VPS up to speed.

Install all needed packages.

```bash
apt-get install nginx mysql-server php-mysql php7.0 screen git
service nginx start
```

Set `cgi.fix_pathinfo=0` in `/etc/php/7.0/fpm/php.ini`.

```bash
useradd -m gothcon
mkdir -p /home/gothcon/www/cthulhu
chown -R gothcon.gothcon /home/gothcon
```

All the configuration files!

```bash
# /etc/php/7.0/fpm/pool.d/gc.conf

[gc.spelkonvent.se]

user   = gothcon
group  = gothcon

listen       = /var/run/fpm-$pool.sock
listen.owner = www-data
listen.group = www-data
listen.mode  = 0666

pm = dynamic

pm.max_children = 10
pm.start_servers = 1
pm.min_spare_servers = 1
pm.max_spare_servers = 1
;pm.process_idle_timeout = 300s;

access.log    = /var/log/$pool.access.log
access.format = %R - %u %t "%m %r%Q%q" %s %f %{mili}d %{kilo}M %C%%

chdir = /

catch_workers_output = yes
```

```bash
# /etc/nginx/conf.d/gc.spelkonvent.se.conf

server {
    listen 80;
    server_name gc.spelkonvent.se;
    root /home/gothcon/www/cthulhu;
    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ /index.php;
        autoindex off;
    }

    # http://www.binarytides.com/install-nginx-php-fpm-mariadb-debian/
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        # NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini

        # With php7.0-fpm:
        fastcgi_pass unix:/var/run/fpm-gc.spelkonvent.se.sock;
        fastcgi_index index.php;
        include /etc/nginx/fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
    }
}
```

Restart all the services and go!

```bash
service nginx restart
service php7.0-fpm restart
```

## Deploy Cthulhu

Change user to cthulhu, and create the needed git repository. We deploy by pushing our code to this repository.

```bash
su - cthulhu
mkdir -p /home/gothcon/repo.git
cd repo.git
git init --bare
cd
mkdir -p /home/.ssh/
```

Add your public SSH-keys to ~/.ssh/authorized keys to get access to the account, and to be able to deploy your code.

```bash
touch ~/repo.git/hooks/post-receive
chmod +x ~/repo.git/hooks/post-receive
```


## What to do on your development box

To get started, clone the repository, add the deploy remote and double check if your ssh key has been added to gothcon's .ssh/authorized_keys.

```bash
git clone git@gitlab.com:gothcon/cthulhu.git
git remote add deploy ssh://gothcon@gc.spelkonvent.se:/home/gothcon/repo.git
```

To deploy a branch or tag, push it to the deploy repository.

```bash
git push deploy origin
```

